package com.cybernetic.cnerp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnerpApplication {

	public static void main(String[] args) {
		SpringApplication.run(CnerpApplication.class, args);
	}

}
