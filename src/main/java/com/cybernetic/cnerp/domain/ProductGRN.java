package com.cybernetic.cnerp.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "productGRN")
public class ProductGRN {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="grnId", unique=true)
    private Long grnId;

    @Column(name="grn_number")
    private String grnNumber;

}
