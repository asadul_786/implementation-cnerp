package com.cybernetic.cnerp.domain;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by md. mithu sarker on 20/03/2022.
 */

@Data
@Entity
@Table(name = "store_intormation")
public class StoreInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", nullable=false, unique=true)
    private Long storeInfoId;

    @Column(name="name", nullable=false)
    private String name;

    @Column(name="code", nullable=false, unique=true)
    private String code;

    @Column(name="address", nullable=false)
    private String address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "district_id" , nullable=false)
    private District district;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "thana_id" , nullable=false)
    private Thana thana;

    @Column(name="store_type", nullable=false)
    private Integer storeType;

    @Column(name="description")
    private String description;

    @Column(name="store_id")
    private Long storeId;

    @Column(name="number_of_rack")
    private Integer numberOfRack;

    @Column(name="remark")
    private String remark;


    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updateBy;

    @Column(name = "updated_on")
    private Date updateOn;

}
