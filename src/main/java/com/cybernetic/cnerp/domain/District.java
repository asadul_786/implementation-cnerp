package com.cybernetic.cnerp.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by md. mithu sarker on 20/03/2022.
 */

@Data
@Entity
@Table(name = "district")
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="district_id", nullable=false, unique=true)
    private Long districtId;

    @Column(name = "district_NAME")
    private String districtName;

    @Column(name = "SHORT_NAME")
    private String shortName;


    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updateBy;

    @Column(name = "updated_on")
    private Date updateOn;
}
