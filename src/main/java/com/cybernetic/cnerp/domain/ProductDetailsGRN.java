package com.cybernetic.cnerp.domain;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "productDetailsGRN")
public class ProductDetailsGRN {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="pdId", unique=true)
    private Long pdId;

    @Column(name="grn_number")
    private String grnNumber;

    @Column(name="product_id")
    private String productId;

    @Column(name="unit")
    private String unit;

    @Column(name="order_qty")
    private Integer orderQty;

    @Column(name="receive_qty")
    private Integer receiveQty;

    @Column(name="damage_qty")
    private Integer damageQty;

    @Column(name="unit_price")
    private Float unitPrice;

    @Column(name="total_price")
    private Float totalPrice;

    @Column(name="vat")
    private Float vat;

    @Column(name="tax")
    private Float tax;

    @Column(name="net_total")
    private Float netTotal;

    @Column(name="deducted_product_amount_for_damage")
    private Integer deductedProductAmountForDamage;

    @Column(name="aferDeducted_netAmount")
    private Float aferDeductedNetAmount;

    @Column(name="sku")
    private String sku;

    @Column(name="expired_date")
    private Date expiredDate;

    @Column(name="manufacture_date")
    private Date manufactureDate;

    @Column(name="remarks")
    private String remarks;

    @Column(name="created_by")
    private String createdBy;

    @Column(name="created_on")
    private Date createdOn;

    @Column(name="updated_by")
    private String updatedBy;

    @Column(name="created_on")
    private Date updatedOn;


}
