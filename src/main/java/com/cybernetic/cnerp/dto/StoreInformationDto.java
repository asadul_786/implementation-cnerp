package com.cybernetic.cnerp.dto;

import com.cybernetic.cnerp.domain.District;
import com.cybernetic.cnerp.domain.Thana;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
public class StoreInformationDto {
    private Long storeInfoId;
    private String name;
    private String code;
    private String address;
    private Long districtId;
    private String districtName;
    private Long thanaId;
    private String thanaName;
    private Integer storeType;
    private String description;
    private Long storeId;
    private Integer numberOfRack;
    private String remark;
}
