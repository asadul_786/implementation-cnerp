package com.cybernetic.cnerp.services;

import com.cybernetic.cnerp.commands.ProductForm;
import com.cybernetic.cnerp.domain.Product;

import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface ProductService {

    List<Product> listAll();

    Product getById(Long id);

    Product saveOrUpdate(Product product);

    void delete(Long id);

    Product saveOrUpdateProductForm(ProductForm productForm);
}
