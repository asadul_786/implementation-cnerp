package com.cybernetic.cnerp.services.impl;

import com.cybernetic.cnerp.commands.ProductForm;
import com.cybernetic.cnerp.converters.ProductFormToProduct;
import com.cybernetic.cnerp.domain.District;
import com.cybernetic.cnerp.domain.Product;
import com.cybernetic.cnerp.domain.StoreInformation;
import com.cybernetic.cnerp.domain.Thana;
import com.cybernetic.cnerp.dto.StoreInformationDto;
import com.cybernetic.cnerp.repositories.DistrictRepository;
import com.cybernetic.cnerp.repositories.ProductRepository;
import com.cybernetic.cnerp.repositories.StoreInformationRepository;
import com.cybernetic.cnerp.repositories.ThanaRepository;
import com.cybernetic.cnerp.services.ProductService;
import com.cybernetic.cnerp.services.StoreInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by md. mithu sarker on 20/03/2022.
 */
@Service
public class StoreInformationServiceImpl implements StoreInformationService {

    @Autowired
    private StoreInformationRepository storeInformationRepository;
    @Autowired
    private DistrictRepository districtRepository;
    @Autowired
    private ThanaRepository thanaRepository;



    @Override
    public Integer storeInformationSave(StoreInformationDto storeInformationDto) {
        Integer returnStatus = 99;
        StoreInformation name = storeInformationRepository.findByCodeIgnoreCase(storeInformationDto.getCode());

        if (storeInformationDto.getStoreInfoId() == null) {
            if (name != null && storeInformationDto.getCode() == null){
                return returnStatus = 2;
            }else{
                StoreInformation storeInformation = new StoreInformation();
                storeInformation.setName(storeInformationDto.getName());
                storeInformation.setCode(storeInformationDto.getCode());
                storeInformation.setStoreType(storeInformationDto.getStoreType());
                storeInformation.setAddress(storeInformationDto.getAddress());
                storeInformation.setDescription(storeInformationDto.getDescription());
                storeInformation.setRemark(storeInformationDto.getRemark());
                storeInformation.setDistrict(setDistrictCode(storeInformationDto.getDistrictId()));
                storeInformation.setThana(setThanaCode(storeInformationDto.getThanaId()));
                storeInformation.setStoreId(11l);
                storeInformation.setNumberOfRack(22);
                storeInformation.setCreatedBy("admin");
                storeInformation.setCreatedOn(new Date());
                storeInformationRepository.save(storeInformation);
                return returnStatus = 0;
            }
        } else{
            StoreInformation obj  = storeInformationRepository.findByStoreInfoId(storeInformationDto.getStoreInfoId());
            if ((name == null && obj.getStoreInfoId().equals(storeInformationDto.getStoreInfoId())) || (name != null && name.getStoreInfoId().toString().equals(storeInformationDto.getStoreInfoId().toString()))){
                obj.setName(storeInformationDto.getName());
                obj.setCode(storeInformationDto.getCode());
                obj.setStoreType(storeInformationDto.getStoreType());
                obj.setAddress(storeInformationDto.getAddress());
                obj.setDescription(storeInformationDto.getDescription());
                obj.setRemark(storeInformationDto.getRemark());
                obj.setDistrict(setDistrictCode(storeInformationDto.getDistrictId()));
                obj.setThana(setThanaCode(storeInformationDto.getThanaId()));
                obj.setStoreId(11l);
                obj.setNumberOfRack(22);
                obj.setUpdateBy("admin");
                obj.setUpdateOn(new Date());
                storeInformationRepository.save(obj);
                return returnStatus = 1;
            }else {
                return returnStatus = 2;
            }
        }

        }


    private Thana setThanaCode(Long thanaId) {
        Thana thana = new Thana();
        thana.setThanaId(thanaId);
        return thana;
    }

    private District setDistrictCode(Long districtId) {
        District district = new District();
        district.setDistrictId(districtId);
        return district;
    }

    @Override
    public List<District> getDistrict() {
        return districtRepository.findAll();
    }

    @Override
    public List<Thana> getThana() {
        return thanaRepository.findAll();
    }

    @Override
    public List<StoreInformation> getStoreInformationData() {
        return storeInformationRepository.findAll();
    }


}
