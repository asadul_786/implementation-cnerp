package com.cybernetic.cnerp.services;

import com.cybernetic.cnerp.commands.ProductForm;
import com.cybernetic.cnerp.domain.District;
import com.cybernetic.cnerp.domain.Product;
import com.cybernetic.cnerp.domain.StoreInformation;
import com.cybernetic.cnerp.domain.Thana;
import com.cybernetic.cnerp.dto.StoreInformationDto;

import java.util.List;

/**
 * Created by md. mithu sarker on 20/03/2022.
 */

public interface StoreInformationService {
    Integer storeInformationSave(StoreInformationDto storeInformationDto);
    List<District> getDistrict();
    List<Thana> getThana();

    List<StoreInformation> getStoreInformationData();
}
