package com.cybernetic.cnerp.repositories;

import com.cybernetic.cnerp.domain.District;
import com.cybernetic.cnerp.domain.StoreInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by md. mithu sarker on 20/03/2022.
 */
public interface DistrictRepository extends JpaRepository<District, Long> {
}
