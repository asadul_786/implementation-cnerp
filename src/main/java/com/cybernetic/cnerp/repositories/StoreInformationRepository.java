package com.cybernetic.cnerp.repositories;

import com.cybernetic.cnerp.domain.Product;
import com.cybernetic.cnerp.domain.StoreInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by md. mithu sarker on 20/03/2022.
 */
public interface StoreInformationRepository extends JpaRepository<StoreInformation, Long> {
    StoreInformation findByStoreInfoId(Long storeInfoId);
    StoreInformation findByCodeIgnoreCase(String code);
}
