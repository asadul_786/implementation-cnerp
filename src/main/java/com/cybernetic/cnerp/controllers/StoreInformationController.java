package com.cybernetic.cnerp.controllers;

import com.cybernetic.cnerp.commands.ProductForm;
import com.cybernetic.cnerp.converters.ProductToProductForm;
import com.cybernetic.cnerp.domain.Product;
import com.cybernetic.cnerp.domain.StoreInformation;
import com.cybernetic.cnerp.dto.StoreInformationDto;
import com.cybernetic.cnerp.services.ProductService;
import com.cybernetic.cnerp.services.StoreInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by md. mithu sarker on 20/03/2022.
 */
@Controller
public class StoreInformationController {

    private StoreInformationService storeInformationService;

    @Autowired
    public void setProductToProductForm(StoreInformationService storeInformationService) {
        this.storeInformationService = storeInformationService;
    }
    @GetMapping("/form")
    public String showStoreInformationForm(Model model){
        model.addAttribute("getDistrict",storeInformationService.getDistrict());
        model.addAttribute("getThana",storeInformationService.getThana());
        model.addAttribute("getStoreInformationData",storeInformationService.getStoreInformationData());
        return "product/storeInformationForm";
    }

    @RequestMapping(value = "/storeInformation/save", method = RequestMethod.POST)
    @ResponseBody
    public Integer storeInformationSave(@RequestBody StoreInformationDto storeInformationDto) throws Exception {
        return storeInformationService.storeInformationSave(storeInformationDto);
    }


}
