package com.cybernetic.cnerp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/test-con")
public class TestController {

    @GetMapping("/prithun")
    public String prithunFunc(Model model) {
        //model.addAttribute("ddlChartOfAccount", objectConverter.objectsToDropDownList(chartAccountService.getHeadChartAcc()));
        return "/test/prithunhtml";
    }

}