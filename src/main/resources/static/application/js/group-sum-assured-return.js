
$(document).ready(function () {

    $("#pnl-info").hide();
    $("#pnl-list").show();

    $("#backbtn").click(function () {
        $("#pnl-info").hide();
        $("#pnl-list").show();
    });

    $("#addNewUnitBtnAdd").click(function () {
        $("#pnl-info").show();
        $("#pnl-list").hide();
    });

    $("#Save").click(function () {
        if (isValid()) {

            var storeInformation = {};
            storeInformation.storeInfoId = $("#storeInfoId").val();
            storeInformation.name = $("#name").val();
            storeInformation.code = $("#code").val();
            storeInformation.address = $("#address").val();
            storeInformation.districtId = $("#districtId").val();
            storeInformation.thanaId = $("#thanaId").val();
            storeInformation.storeType = $("#storeType").val();
            storeInformation.description = $("#description").val();
            storeInformation.remark = $("#remark").val();


            $.ajax({
                url: "/storeInformation/save",
                type: 'POST',
                data: JSON.stringify(storeInformation),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data == 0) {
                        alert("Successfully added.");
                        //showAlertByType("Successfully added.", 'S');
                        setInterval(function(){location.reload(true); }, 1500);
                    }
                    else if(data == 1) {
                        alert("Successfully updated.");
                        //showAlertByType("Successfully updated.", "S");
                        setInterval(function(){location.reload(true); }, 1500);
                    }else if(data == 2) {
                        alert("This Code Already Exists!!");
                        //showAlertByType("This Code Already Exists!!", "W");
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    alert("Something went wrong[xhr], Contact with IT team.");
                    //showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
    });

    function isValid() {

        $('.errorClass').text('');


        if($('#name').val() == ''){
            $('#error_name').text('This field is required');
            $('#name').focus();
            return false;
        }else if($('#name').val().length>50){
            $('#error_name').text('maximum 50 characters!!');
            $('#name').focus();
            return false;
        }

        else  if($('#code').val() == ''){
            $('#error_code').text('This field is required');
            $('#code').focus();
            return false;
        }else if($('#code').val().length>10){
            $('#error_code').text('maximum 10 characters!!');
            $('#code').focus();
            return false;
        }

        else  if($('#districtId').val() == ''){
            $('#error_districtId').text('This field is required');
            $('#districtId').focus();
            return false;
        }

        else  if($('#thanaId').val() == ''){
            $('#error_thanaId').text('This field is required');
            $('#thanaId').focus();
            return false;
        }

        else  if($('#storeType').val() == ''){
            $('#error_storeType').text('This field is required');
            $('#storeType').focus();
            return false;
        }

        else  if($('#address').val() == ''){
            $('#error_address').text('This field is required');
            $('#address').focus();
            return false;
        }else if($('#address').val().length>200){
            $('#error_address').text('maximum 200 characters!!');
            $('#address').focus();
            return false;
        }

        else if($('#description').val().length>200){
            $('#error_description').text('maximum 200 characters!!');
            $('#description').focus();
            return false;
        }

        else if($('#remark').val().length>50){
            $('#error_remark').text('maximum 50 characters!!');
            $('#remark').focus();
            return false;
        }


        return true;

    }

    //var table = $('#dataTable').DataTable({});

    $('#dataTable tbody').on('click', '#edit', function () {

        $("#pnl-info").show();
        $("#pnl-list").hide();

        var curRow = $(this).closest('tr');
        var storeInfoId = curRow.find('td:eq(0)').text();
        var name = curRow.find('td:eq(1)').text();
        var code = curRow.find('td:eq(2)').text();
        var storeType = curRow.find('td:eq(3)').text();
        var districtId = curRow.find('td:eq(4)').text();
        var thanaId = curRow.find('td:eq(5)').text();
        var address = curRow.find('td:eq(9)').text();
        var description = curRow.find('td:eq(10)').text();
        var remark = curRow.find('td:eq(11)').text();

        $('#storeInfoId').val(storeInfoId);
        $('#name').val(name);
        $('#code').val(code);
        $('#storeType').val(storeType);
        $('#districtId').val(districtId);
        $('#thanaId').val(thanaId);
        $('#address').val(address);
        $('#description').val(description);
        $('#remark').val(remark);

        $("#Save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });



});
