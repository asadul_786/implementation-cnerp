//REFRESH CLAIMANT STATEMENT
$('#refreshClaimantQuestionnair').on('click',  function () {
$('#drp-client-type').val(-1);
$('#clients-name').val("");
$('#fath-husb-name').val("");
$('#drp-relation-code').val(-1);
$('#profession').val("");
$('#clients-address').val("");
$('#attestation-place').val("");
$('#dt-attested-date').val("");
$('#attestation-by').val("");
$('#professional-address').val("");

})
$('#refreshQuestionnair').on('click',  function () {
    $('#drp-question-type').val(-1);
    $('#answer').val("");
    $('#drp-question').val(-1);


})
//REFRESH DOCTOR STATEMENT
$('#refreshdoctor').on('click',  function () {
    $('#doctors-name').val("");
    $('#signature-place').val("");
    $('#education').val("");
    $('#reg-no').val("");
    $('#address').val("");
    $('#jbc-identification-no').val("");
    $('#dt-signature-date').val("");
    $('#name').val("");
    $('#doc-witness-profession').val("");
    $('#doc-witness-address').val("");


})
$('#refreshdQuestionnair').on('click',  function () {
    $('#ddrp-question-type').val(-1);
    $('#danswer').val("");
    $('#dquestion').val(-1);


})
//REFRESH EMPLOYER STATEMENT
$('#refreshEmployer').on('click',  function () {

    $('#employer-name').val("");
    $('#designation').val("");
    $('#employer-address').val("");
    $('#date-signature-dt').val("");
    $('#place-of-signature').val("");
    $('#examined-by').val("");
    $('#created-by').val("");
    $('#exam-desig').val("");



})
$('#refreshEmployerQuestion').on('click',  function () {
    $('#edrp-question-type').val(-1);
    $('#eanswer').val("");
    $('#equestion').val(-1);


})

// Here asad code start
$( "#dt-application-date" ).datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1970:2020'
});

$( "#dt-attested-date" ).datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1970:2020'
});

$( "#dt-signature-date" ).datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1970:2020'
});


$( "#date-signature-dt" ).datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1970:2020'
});

$( "#dt-sig-date" ).datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1970:2020'
});
// end
//REFRESH IDENTIFIER STATEMENT
$('#refreshIdentification').on('click',  function () {

    $('#identified-by').val("");
    $('#desig').val("");
    $('#dt-sig-date').val("");
    $('#permanent-address').val("");
    $('#place-sig').val("");
    $('#id-name').val("");
    $('#id-witness-profession').val("");
    $('#id-witness-address').val("");



})
$('#refreshIdentificationQuestion').on('click',  function () {
    $('#idrp-question-type').val(-1);
    $('#ianswer').val("");
    $('#iquestion').val(-1);


})

//QUESTIONNAIRE TABLES
function tableForClaimant(pgid) {




    $.ajax({
        type: "GET",
        url: "/claimpayment/questionList/" + pgid+"/C",
        success: function (data) {

            var tableBody = "";

            $('#ClaimantTable tbody').empty();


            $.each(data, function (idx, elem) {
                var action =
                '<button class="btn btn-success " type="button" id="editClaimant"> ' +
                '<i class="fa fa-pencil"></i></button> ';

                tableBody += "<tr'>";
                tableBody += "<td hidden>" + elem[0] + "</td>";

                tableBody += "<td hidden>" + elem[1] + "</td>";
                tableBody += "<td hidden>" + elem[2] + "</td>";
                tableBody += "<td hidden>" + elem[3] + "</td>";
                tableBody += "<td>" + elem[4] + "</td>";
                tableBody += "<td hidden>" + elem[5] + "</td>";
                tableBody += "<td>" + elem[6] + "</td>";
                tableBody += "<td hidden>" + elem[7] + "</td>";
                tableBody += "<td hidden>" + elem[8] + "</td>";
                tableBody += "<td hidden>" + elem[9] + "</td>";
                tableBody += "<td hidden>" + elem[10] + "</td>";
                tableBody += "<td >" +action + "</td>";
                tableBody += "<tr>";
            });

            $('#ClaimantTable tbody').append(tableBody);

        }
    });
}
function tableForDoctor(pgid) {
    //  Table data
    $.ajax({
        type: "GET",
        url: "/claimpayment/questionList/" + pgid+"/"+'D',
        success: function (data) {

            var tableBody = "";


            $('#DoctortableData tbody').empty();

            $.each(data, function (idx, elem) {
                var action =
                    '<button class="btn btn-success " type="button" id="editDoctor"> ' +
                    '<i class="fa fa-pencil"></i></button> ';
                tableBody += "<tr'>";
                tableBody += "<td hidden>" + elem[0] + "</td>";
                tableBody += "<td hidden>" + elem[1] + "</td>";
                tableBody += "<td hidden>" + elem[2] + "</td>";
                tableBody += "<td hidden>" + elem[3] + "</td>";
                tableBody += "<td>" + elem[4] + "</td>";
                tableBody += "<td hidden>" + elem[5] + "</td>";
                tableBody += "<td>" + elem[6] + "</td>";
                tableBody += "<td hidden>" + elem[7] + "</td>";
                tableBody += "<td hidden>" + elem[8] + "</td>";
                tableBody += "<td hidden>" + elem[9] + "</td>";
                tableBody += "<td hidden>" + elem[10] + "</td>";
                tableBody += "<td >" +action + "</td>";
                tableBody += "<tr>";
            });

            $('#DoctortableData tbody').append(tableBody);
        }
    });
}
function tableForEmployer(pgid) {
    //  Table data
    $.ajax({
        type: "GET",
        url: "/claimpayment/questionList/" + pgid+"/"+'E',
        success: function (data) {

            var tableBody = "";



            $('#EmployertableData tbody').empty();


            $.each(data, function (idx, elem) {
                var action =
                    '<button class="btn btn-success " type="button" id="editEmpl"> ' +
                    '<i class="fa fa-pencil"></i></button> ';

                tableBody += "<tr'>";
                tableBody += "<td hidden>" + elem[0] + "</td>";
                tableBody += "<td hidden>" + elem[1] + "</td>";
                tableBody += "<td hidden>" + elem[2] + "</td>";
                tableBody += "<td hidden>" + elem[3] + "</td>";
                tableBody += "<td>" + elem[4] + "</td>";
                tableBody += "<td hidden>" + elem[5] + "</td>";
                tableBody += "<td>" + elem[6] + "</td>";
                tableBody += "<td hidden>" + elem[7] + "</td>";
                tableBody += "<td hidden>" + elem[8] + "</td>";
                tableBody += "<td hidden>" + elem[9] + "</td>";
                tableBody += "<td hidden>" + elem[10] + "</td>";
                tableBody += "<td >" +action + "</td>";
                tableBody += "<tr>";
            });

            $('#EmployertableData tbody').append(tableBody);

        }
    });
}
function tableForIdentification(pgid) {
    //  Table data
    $.ajax({
        type: "GET",
        url: "/claimpayment/questionList/" + pgid+"/"+'I',
        success: function (data) {

            var tableBody = "";

            $('#IdentificationtableData tbody').empty();

            $.each(data, function (idx, elem) {
                var action =
                    '<button class="btn btn-success " type="button" id="editIdent"> ' +
                    '<i class="fa fa-pencil"></i></button> ';

                tableBody += "<tr'>";
                tableBody += "<td hidden>" + elem[0] + "</td>";
                tableBody += "<td hidden>" + elem[1] + "</td>";
                tableBody += "<td hidden>" + elem[2] + "</td>";
                tableBody += "<td hidden>" + elem[3] + "</td>";
                tableBody += "<td>" + elem[4] + "</td>";
                tableBody += "<td hidden>" + elem[5] + "</td>";
                tableBody += "<td>" + elem[6] + "</td>";
                tableBody += "<td hidden>" + elem[7] + "</td>";
                tableBody += "<td hidden>" + elem[8] + "</td>";
                tableBody += "<td hidden>" + elem[9] + "</td>";
                tableBody += "<td hidden>" + elem[10] + "</td>";
                tableBody += "<td >" +action + "</td>";
                tableBody += "<tr>";
            });

            $('#IdentificationtableData tbody').append(tableBody);
        }
    });
}


function getId(id)
{
    var str = $("#employer-name").val();
    var eid= str.substring(str.indexOf("-") + 1);

    $.ajax({
        url: "/claimpayment/employeeDesig/" + eid,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var value_1 = "";

            $.each(response, function (i, l) {



                value_1 = l[0];


                //loading table




            });
            $("#designation").val(value_1)




            $("#err_empID").text("");


        },
        error: function (xhr, status, error) {


        }
    });
}

function getEID(id)
{
    var str = $("#examined-by").val();
    var eid= str.substring(str.indexOf("-") + 1);

    $.ajax({
        url: "/claimpayment/employeeDesig/" + eid,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var value_1 = "";

            $.each(response, function (i, l) {



                value_1 = l[0];


                //loading table




            });
            $("#exam-desig").val(value_1)




            $("#err_empID").text("");


        },
        error: function (xhr, status, error) {


        }
    });
}
$(document).ready(function () {
//DELETE
    $('#listTable tbody').on('click', '#delete', function () {
        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Selected record will be deleted.',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url:  "/claimpayment/deletedeathclaim/" + col1,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function(response) {

                            if(response === true){
                                table.row( curRow ).remove().draw( false );
                                showAlert("Deleted Successfully");
                            }
                            else {
                                showAlert("Unknown error");
                            }

                        },
                        error: function(xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });
    var table = $('#listTable').DataTable({

//                "columnDefs": [
//                    {"targets": [0], "searchable": false}
//                ]
    });
    // var table= $('#').DataTable();
    $('#DoctortableData').DataTable();
    $('#ClaimantTable').DataTable();
    $('#EmployertableData').DataTable();
    $('#IdentificationtableData').DataTable();

    //EDITING QUESTION TABLES
    $('#ClaimantTable tbody').on('click', '#editClaimant', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();

        $('#pgid').val(col1);
        $('#intimation-no').val(col2);
        $('#drp-question-type').val(col3);
        $('#drp-question').val(col4);
        $('#answer').val(col7);
        $('#iusr').val(col8);
        $('#idt').val(col9);


        // $('html, body').animate({
        //     scrollTop: $('#scroll').offset().top
        // }, 500);

    });
    $('#DoctortableData tbody').on('click', '#editDoctor', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();


        $('#pgid').val(col1);
        $('#intimation-no').val(col2);
        $('#ddrp-question-type').val(col3);
        $('#dquestion').val(col4);
        $('#danswer').val(col7);
        $('#iusr').val(col8);
        $('#idt').val(col9);


        // $('html, body').animate({
        //     scrollTop: $('#scroll').offset().top
        // }, 500);

    });
    $('#EmployertableData tbody').on('click', '#editEmpl', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();

        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();


        $('#pgid').val(col1);
        $('#intimation-no').val(col2);
        $('#edrp-question-type').val(col3);
        $('#equestion').val(col4);
        $('#eanswer').val(col7);
        $('#iusr').val(col8);
        $('#idt').val(col9);


        // $('html, body').animate({
        //     scrollTop: $('#scroll').offset().top
        // }, 500);

    });
    $('#IdentificationtableData tbody').on('click', '#editIdent', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();



        $('#pgid').val(col1);
        $('#intimation-no').val(col2);
        $('#idrp-question-typ').val(col3);
        $('#iquestion').val(col4);
        $('#ianswer').val(col7);
        $('#iusr').val(col8);
        $('#idt').val(col9);


        //scroll up


        // $('html, body').animate({
        //     scrollTop: $('#scroll').offset().top
        // }, 500);

    });



    //FINDING GID AND INITIALIZING QUESTION TABLES
    $(document).on("input", "#policy-no", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyno = $('#policy-no').val();


        if (policyno != "" && policyno.length < 16) {

            $.ajax({
                url: "/claimpayment/deathClaimApplicationDetails/" + policyno,
                type: 'GET',
                dataType: 'json',
                success: function (response) {

                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    var value_6 = "";
                    var value_7 ="";
                    var value_8 ="";
                    var value_9 ="";
                    var value_10 ="";
                    var value_11 ="";
                    var value_12 ="";
                    var value_13="";
                    var value_14="";
                    var value_15="";
                    var value_16="";
                    var value3="";
                    var value4="";
                    var value5="";
                    var value6="";
                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];
                        value_9 = l[8];
                        value_10 = l[9];
                        value_11 = l[10];
                        value_12 = l[11];
                        value_13 = l[12];
                        value_14 = l[13];
                        value_15 = l[14];
                        value_16 = l[15];

                        //loading table


                         value3 = getDateShow(value_3);
                         value4 = getDateShow(value_4);
                         value5 = getDateShow(value_5);
                         value6 = getDateShow(value_12);

                        function getDateShow(dateObject) {
                            var d = new Date(dateObject);
                            var day = d.getDate();
                            var month = d.getMonth() + 1;
                            var year = d.getFullYear();

                            if (day < 10) {
                                day = "0" + day;
                            }
                            if (month < 10) {
                                month = "0" + month;
                            }
                            var date = day + "/" + month + "/" + year;



                            return date;
                        };




                    });

                    $("#pgid").val(value_1);
                    $("#term").val(value_2);
                    $("#dt-commence-date").val(value3);
                    $("#dt-risk-date").val(value4);
                    $("#dt-maturity-date").val(value5);
                    $("#sum-assured").val(value_6);
                    $("#ser-off-cd").val(value_7);
                    $("#servicing-office").val(value_8);
                    $("#client-name").val(value_9);
                    $("#client-type").val(value_10);
                    $("#intimation-no").val(value_11);

                    $("#client-tp").val(value_12);

                    $("#dt-date-of-occu").val(value6);
                    $("#client-address").val(value_13);
                    $("#partyCd").val(value_15);

                    $("#dt-application-date").val($.datepicker.formatDate('dd/mm/yy', new Date()));



                    tableForClaimant(value_1);
                    tableForDoctor(value_1);
                    tableForEmployer(value_1);
                    tableForIdentification(value_1);



                    $("#err_empID").text("");


                },
                error: function (xhr, status, error) {

                    showAlertByType("Policy No. already applied","F");
                    var flag = false;
                }
            });

        } else {


        }

    });

    var flag = false;

    //INSERTING QUESTIONNAIR
    $("#saveQuestionnair").click(function () {


        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var questiontpcd = $('#drp-question-type').val();
            var questioncd = $('#drp-question').val();
            var answerdesc = $('#answer').val();
            var questionnairfor = 'C';
            var iusr = $('#iusr').val();
            var idt = $('#idt').val();

            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.questiontpcd = questiontpcd;
            suppben.questioncd = questioncd;
            suppben.answerdesc = answerdesc;
            suppben.questionnairfor = questionnairfor;

            suppben.iusr = iusr;
            suppben.idt = idt;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addClmQuestionnair",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;

                    $('#drp-question-type').val(-1);
                   $('#drp-question').val(-1);
                    $('#answer').val("");

                    tableForClaimant(pgid);
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });
    $("#saveDoctorQuestion").click(function () {


        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var questiontpcd = $('#ddrp-question-type').val();
            var questioncd = $('#dquestion').val();
            var answerdesc = $('#danswer').val();
            var questionnairfor = 'D';
            var iusr = $('#iusr').val();
            var idt = $('#idt').val();

            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.questiontpcd = questiontpcd;
            suppben.questioncd = questioncd;
            suppben.answerdesc = answerdesc;
            suppben.questionnairfor = questionnairfor;
            suppben.iusr = iusr;
            suppben.idt = idt;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addDoctorQuestion",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;
                    $('#ddrp-question-type').val(-1);
                    $('#dquestion').val(-1);
                    $('#danswer').val("");
                    tableForDoctor(pgid);
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });
    $("#saveEmployerQuestion").click(function () {


        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var questiontpcd = $('#edrp-question-type').val();
            var questioncd = $('#equestion').val();
            var answerdesc = $('#eanswer').val();
            var questionnairfor = 'E';
            var iusr = $('#iusr').val();
            var idt = $('#idt').val();

            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.questiontpcd = questiontpcd;
            suppben.questioncd = questioncd;
            suppben.answerdesc = answerdesc;
            suppben.questionnairfor = questionnairfor;
            suppben.iusr = iusr;
            suppben.idt = idt;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addDoctorQuestion",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;
                    $('#edrp-question-type').val(-1);
                    $('#equestion').val(-1);
                    $('#eanswer').val("");
                    tableForEmployer(pgid);
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });
    $("#saveIdentificationQuestion").click(function () {


        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var questiontpcd = $('#idrp-question-type').val();
            var questioncd = $('#iquestion').val();
            var answerdesc = $('#ianswer').val();
            var questionnairfor = 'I';
            var iusr = $('#iusr').val();
            var idt = $('#idt').val();

            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.questiontpcd = questiontpcd;
            suppben.questioncd = questioncd;
            suppben.answerdesc = answerdesc;
            suppben.questionnairfor = questionnairfor;
            suppben.iusr = iusr;
            suppben.idt = idt;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addDoctorQuestion",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;
                    $('#idrp-question-type').val(-1);
                    $('#iquestion').val(-1);
                    $('#ianswer').val("");
                    tableForIdentification(pgid);
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });

    //ADD DOCTOR
    $("#saveDoctor").click(function () {



        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var doctornm = $('#doctors-name').val();
            var education = $('#education').val();
            var regno = $('#reg-no').val();
            // var idno = $('#iusr').val();
            var address = $('#address').val();
            var signplace = $('#signature-place').val();
            var signdate = $('#idt').val();

            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.doctornm = doctornm;
            suppben.education = education;
            suppben.regno = regno;
            // suppben.idno = iusr;
            suppben.address = address;
            suppben.signplace = signplace;
            suppben.signdate = signdate;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addDoctorStatement",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {

                    flag = true;
                    // $('#doctors-name').val("");
                    // $('#education').val("");
                    // $('#reg-no').val("");
                    // // var idno = $('#iusr').val();
                    // $('#address').val("");
                    // $('#signature-place').val("");


                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }
        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var witnessfor = "D";
            var witnessnm = $('#name').val();
            var witnessprof = $('#doc-witness-profession').val();
            var witnessadd = $('#doc-witness-address').val();


            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.witnessfor = witnessfor;
            suppben.witnessnm = witnessnm;
            suppben.witnessprof = witnessprof;
            suppben.witnessadd = witnessadd;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addWitness",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;
                    // $('#name').val("");
                    // $('#doc-witness-profession').val("");
                    // $('#doc-witness-address').val("");
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });

    //ADD CLAIMENT
    $("#saveClaimantQuestionnair").click(function () {


        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var partycd = $('#drp-client-type').val();
            var clientnm = $('#clients-name').val();
            var fatherhusband = $('#fath-husb-name').val();
            var age = $('#age').val();
            var profression = $('#profession').val();
            var relationcd = $('#drp-relation-code').val();
            var clientaddress = $('#clients-address').val();


            var claimant = {};
            claimant.pgid = pgid;
            claimant.intimationno = intimationno;
            claimant.partycd = partycd;
            claimant.clientnm = clientnm;
            claimant.fatherhusband = fatherhusband;
            claimant.age = age;
            claimant.profression = profression;
            claimant.relationcd = relationcd;
            claimant.clientaddress = clientaddress;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addClaimant",
                data: JSON.stringify(claimant),
                dataType: 'json',
                success: function (data) {
                    flag = true;
                    // $('#drp-client-type').val(-1);
                    // $('#clients-name').val("");
                    // $('#fath-husb-name').val("");
                    // $('#age').val("");
                    // $('#profession').val("");
                    // $('#drp-relation-code').val(-1);
                    // $('#clients-address').val("");
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }

        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var attestplace = $('#attestation-place').val();
            var attestdate = $('#dt-attested-date').val();
            var attestby = $('#attestation-by').val();
            var profadd = $('#professional-address').val();


            var attestation = {};
            attestation.pgid = pgid;
            attestation.intimationno = intimationno;
            attestation.attestplace = attestplace;
            attestation.attestdate = attestdate;
            attestation.attestby = attestby;
            attestation.profadd = profadd;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addAttestation",
                data: JSON.stringify(attestation),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;
                    // $('#attestation-place').val("");
                    // $('#dt-attested-date').val("");
                    // $('#attestation-by').val("");
                    // $('#professional-address').val("");
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });

    //ADD EMPLOYER
    $("#saveEmployer").click(function () {



        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var employernm = $('#employer-name').val();
            var designation = $('#designation').val();
            var address = $('#employer-address').val();
            var signplace = $('#place-of-signature').val();
            var signdate = $('#date-signature-dt').val();
            var createdby = $('#created-by').val();
            var examineby = $('#examined-by').val();
            var examinedesig = $('#exam-desig').val();


            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.employernm = employernm;
            suppben.designation = designation;
            suppben.address = address;
            suppben.signplace = signplace;
            suppben.signdate = '2020-12-12';
            suppben.createdby = createdby;
            suppben.examineby = examineby;
            suppben.examinedesig = examinedesig;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addEmployer",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;
                    // $('#employer-name').val("");
                    // $('#designation').val("");
                    // $('#employer-address').val("");
                    // $('#place-of-signature').val("");
                    // $('#date-signature-dt').val("");
                    // $('#created-by').val("");
                    // $('#examined-by').val("");
                    // $('#exam-desig').val("");
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }



    });

    //ADD IDENTIFIER
    $("#saveIdentification").click(function () {



        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var witnessfor = "I";
            var witnessnm = $('#id-name').val();
            var witnessprof = $('#id-witness-profession').val();
            var witnessadd = $('#id-witness-address').val();


            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.witnessfor = witnessfor;
            suppben.witnessnm = witnessnm;
            suppben.witnessprof = witnessprof;
            suppben.witnessadd = witnessadd;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addWitness",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {

                    flag = true;

                   // $('#id-name').val("");
                   // $('#id-witness-profession').val("");
                   // $('#id-witness-address').val("");
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }

        if (1) {

            var pgid = $('#pgid').val();
            var intimationno = $('#intimation-no').val();
            var identifyby = $('#identified-by').val();
            var designation = $('#desig').val();
            var profaddress = $('#prof-address').val();
            var permanentadd = $('#permanent-address').val();
            var signplace = $('#place-sig').val();
            var signdate = $('#dt-sig-date').val();



            var suppben = {};
            suppben.pgid = pgid;
            suppben.intimationno = intimationno;
            suppben.identifyby = identifyby;
            suppben.designation = designation;
            suppben.profaddress = profaddress;
            suppben.permanentadd = permanentadd;
            suppben.signplace = signplace;
            suppben.signdate = signdate;



            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addIdentification",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;
                     // $('#identified-by').val("");
                     // $('#desig').val("");
                     // $('#prof-address').val("");
                     // $('#permanent-address').val("");
                     // $('#place-sig').val("");
                     // $('#dt-sig-date').val("");
                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }



    });


    //ADD MASTER
    $("#save").click(function () {


        if (1) {

            var pgid = $('#pgid').val();
            var intimationNo = $('#intimation-no').val();

            var deathPartCd = $('#partyCd').val();
            var applicationDt = $('#dt-application-date').val();
            var appSlNo ='1';
            var dod = $('#dt-date-of-occu').val();


            var clmApplication = {};
            clmApplication.pgid = pgid;
            clmApplication.intimationNo = intimationNo;

            clmApplication.deathPartCd = deathPartCd;
            clmApplication.applicationDt = applicationDt;
            clmApplication.appSlNo = appSlNo;
            clmApplication.dod = dod;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/saveClmApplication",
                data: JSON.stringify(clmApplication),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;

                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });

});