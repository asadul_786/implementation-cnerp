
function getClm(drclmcd){


    var json = {
        "drclmcd": drclmcd

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/claimpayment/clm/"
        + drclmcd,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#drclmdetlcd').empty();
            $.each(data, function(key, value) {

                $('#drclmdetlcd').append('<option value="' + key + '">' + value + '</option>');

            });

        },
        error: function (e) {

        }
    });


}


function edits() {

    $('#tableData tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();
        var col10 = curRow.find('td:eq(9)').text();
        var col11 = curRow.find('td:eq(10)').text();
        var col12 = curRow.find('td:eq(11)').text();
        var col13 = curRow.find('td:eq(12)').text();
        var col14 = curRow.find('td:eq(13)').text();
        var col15 = curRow.find('td:eq(14)').text();


        $('#slno').val(col1);
        $('#paytype').val(col2);
        $('#percentage').val(col3);
        $('#paybasiscd').val(col4);
        $('#paymenttype').val(col5);
        $('#installmenttype').val(col6);
        $('#paydependson').val(col7);
        $('#paymentyear').val(col8);
        $('#additionalben').val(col9);
        $('#intpercentage').val(col10);
        $('#addbencalon').val(col11);
        $('#timeframe').val(col12);
        $('#totaldays').val(col13);
        $('#premiumpayment').val(col14);
        $('#iusr').val(col15);

        //scroll up
        if (col12 == 1) {
            $("#timeframe").prop('checked', true);

        } else {

            $("#timeframe").prop('checked', false);

        }

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

}
function getList() {
    var suppbencd = $('#suppbencd').val();
    var clmcausecd = $('#clmcausecd').val();
    var clmdetlcd = $('#clmdetlcd').val();
    var benpartycode = $('#benpartycode').val();
    var educationTable = $('#tableData');
    $
        .ajax({
            type: "GET",
            url: "/claimpayment/listInfo/" + suppbencd + "/" + clmcausecd + "/" + clmdetlcd + "/" + benpartycode,
            success: function (data) {
                var no = 1;
                var tableBody = "";
                $('#tableData tbody').empty();
                $.each(data, function (idx, elem) {
                    var action =
                        '<button id="edit" onclick="edits()" type="button" class="btn fa fa-edit" style="text-align:center;vertical-align: middle;font-size:20px;"></button>';
                    tableBody += "<tr'>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td >" + elem[1] + "</td>";
                    tableBody += "<td>" + elem[2] + "</td>";
                    tableBody += "<td>" + elem[3] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td>" + elem[5] + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + elem[7] + "</td>";
                    tableBody += "<td >" + elem[8] + "</td>";
                    tableBody += "<td >" + elem[9] + "</td>";
                    tableBody += "<td >" + elem[10] + "</td>";
                    tableBody += "<td >" + elem[11] + "</td>";
                    tableBody += "<td >" + elem[12] + "</td>";
                    tableBody += "<td >" + elem[13] + "</td>";
                    tableBody += "<td >" + elem[14] + "</td>";
                    tableBody += "<td >" + elem[15] + "</td>";
                    tableBody += "<td>" + action + "</td>"
                    tableBody += "<tr>";
                });
                educationTable.append(tableBody);
            }
        });
}

$( "#dtintimationdate" ).datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1970:2020'
});



$(document).ready(function () {
    var table = $('#dataTable').DataTable({


    });

    //DELETE
    $('#dataTable tbody').on('click', '#delete', function () {
        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Selected record will be deleted.',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url:  "/claimpayment/intimationDelete/" + col1,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function(response) {

                            if(response === true){
                                table.row( curRow ).remove().draw( false );
                                showAlert("Deleted Successfully");
                            }
                            else {
                                showAlert("Unknown error");
                            }

                        },
                        error: function(xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });


    $("#H_EMPLOYEE_GID").select2();

    $(document).on("input", "#policy-no", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyno = $('#policy-no').val();


        if (policyno != "" && policyno.length < 16) {
            $.ajax({
                url: "/claimpayment/claimDetails/" + policyno,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    var value_6 = "";
                    var value_7 ="";
                    var value_8 ="";
                    var value_9 ="";
                    var value_10 ="";
                    var value_11 ="";
                    var value_12 ="";
                    var value_13="";
                    var value_14 ="";
                    var date1="";
                    var value3= "";
                    var value4= "";
                    var value5= "";
                    var value6= "";
                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];
                        value_9 = l[8];
                        value_10 = l[9];
                        value_11 = l[10];
                        value_12 = l[11];
                        value_13 = l[12];
                        value_14 = l[13];



                         value3 = getDateShow(value_3);
                         value4 = getDateShow(value_4);
                         value5 = getDateShow(value_5);
                         value6 = getDateShow(value_6);

                        function getDateShow(dateObject) {
                            var d = new Date(dateObject);
                            var day = d.getDate();
                            var month = d.getMonth() + 1;
                            var year = d.getFullYear();
                            if (day < 10) {
                                day = "0" + day;
                            }
                            if (month < 10) {
                                month = "0" + month;
                            }
                            var date = day + "/" + month + "/" + year;
                             date1 = year[2]+year[3]+month+day;


                            return date;
                        };





                    });

                    $("#pgid").val(value_1);

                    $("#term").val(value_2);
                    $("#dt-commence-date").val(value3);
                    $("#dt-risk-date").val(value4);
                    $("#dt-maturity-date").val(value5);
                    $("#dt-last-paid-date").val(value6);
                    $("#policy-status").val(value_7);
                    $("#option").val(value_8);
                    $("#sum-assured").val(value_9);
                    $("#product-name").val(value_10);
                    $("#client-name").val(value_11);
                    $("#client-type").val(value_12);
                    $("#partycd").val(value_13);



                  //  $("#client-type").val(value_14);

                    $("#intimation-person").val(value_11);

                    $("#err_empID").text("");


                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

    });
    var flag = false;




    //for insert

    $("#emp_master_add").click(function () {


        if (dataValidation()) {

            var intimationno = $('#intimationno').val();
            var dtintimationdate = $('#dtintimationdate').val();
            var informant = $('#informant').val();
            var relation = $('#relation').val();
            var address = $('#address').val();
            var pgid = $('#pgid').val();
            var uusr = $('#uusr').val();
            var udt = $('#udt').val();
            var drpinformedmedia = $('#drpinformedmedia').val();
            var dtdateofocc = $('#dtdateofocc').val();
            var drclmdetlcd = $('#drclmdetlcd').val();
            var condolclient = $('#client-tp').val();
            var chchangebenef = $('#chchangebenef').val();
            var intimationcause = $('#drclmcd').val();
            var partycd = $('#condolclient').val();


            if(chchangebenef == "on")
                chchangebenef = 1;
            else
                chchangebenef = 0;
            var chclosepolicy = $('#chclosepolicy').val();
            if(chclosepolicy == "on")
                chclosepolicy = 1;
            else
                chclosepolicy = 0;


            var suppben = {};
            suppben.intimationno = intimationno;
            suppben.dtintimationdate = dtintimationdate;
            suppben.informant = informant;
            suppben.relation = relation;
            suppben.address = address;
            suppben.pgid = pgid;
            suppben.uusr = uusr;
            suppben.udt = udt;
            suppben.drpinformedmedia = drpinformedmedia;
            suppben.dtdateofocc = dtdateofocc;
            suppben.clmdetlcd = drclmdetlcd;
            suppben.intimationcause = intimationcause;
            suppben.condolclient = condolclient;
            suppben.chchangebenef = chchangebenef;
            suppben.chclosepolicy = chclosepolicy;
            suppben.partycd = partycd;



            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addDeathClaimIn",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;




                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });


    function dataValidation() {
        var informant = $('#informant').val();
        var dtintimationdate = $('#dtintimationdate').val();
        var policyno = $('#policy-no').val();


        if (policyno.length <1) {
            $("#err_policy_no").text("Empty field found!!");
            $("#policy-no").focus();
            var policyno = false;

        } else if (informant.length <1) {
            $("#err_informant").text("Empty field found!!");
            $("#informant").focus();
            var status = false;

        }  else if (dtintimationdate.length<1) {
            $('#dtintimationdate').after('<span class="error">This field is required</span>');
            $("#err_dtintimationdate").text("Empty field found!!");
            $("#dtintimationdate").focus();
            var status = false;

        } else {
            var status = true;
        }

        return status;
    }

    $("#addDtls").click(function () {


        if (dataValidationDtls()) {

            var suppbencd = $('#suppbencd').val();
            var clmcausecd = $('#clmcausecd').val();
            var clmdetlcd = $('#clmdetlcd').val();
            var benpartycode = $('#benpartycode').val();


            var paybasiscd = $('#paybasiscd').val();
            var percentage = $('#percentage').val();
            var installmenttype = $('#installmenttype').val();
            var paymentyear = $('#paymentyear').val();
            var addbencalon = $('#addbencalon').val();
            var totaldays = $('#totaldays').val();
            var paytype = $('#paytype').val();
            var paymenttype = $('#paymenttype').val();
            var paydependson = $('#paydependson').val();
            var intpercentage = $('#intpercentage').val();
            var timeframe = $('#timeframe').val();
            var additionalben = $('#additionalben').val();

            if (timeframe == "on")
                timeframe = '1';
            else
                timeframe = '0';

            var suppbendtls = {};
            suppbendtls.suppbencd = suppbencd;
            suppbendtls.clmcausecd = clmcausecd;
            suppbendtls.clmdetlcd = clmdetlcd;

            suppbendtls.benpartycode = benpartycode;
            suppbendtls.paybasiscd = paybasiscd;
            suppbendtls.percentage = percentage;
            suppbendtls.installmenttype = installmenttype;
            suppbendtls.paymentyear = paymentyear;
            suppbendtls.addbencalon = addbencalon;
            suppbendtls.totaldays = totaldays;
            suppbendtls.paytype = paytype;
            suppbendtls.paymenttype = paymenttype;
            suppbendtls.paydependson = paydependson;
            suppbendtls.intpercentage = intpercentage;
            suppbendtls.timeframe = timeframe;
            suppbendtls.additionalben = additionalben;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addSuppBenDtls",
                data: JSON.stringify(suppbendtls),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;
                    $("#emp_tab").show();
                    getList();

                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }

    });

});