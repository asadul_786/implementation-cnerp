$(document).ready(function () {

    $("#drp-bank-name").select2();
    $("#drp-branch-name").select2();

    $(document).on("input", "#policy-no", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyno = $('#policy-no').val();


        if (policyno != "" && policyno.length < 16) {
            $.ajax({
                url: "/claimpayment/claimDetails/" + policyno,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    var value_6 = "";
                    var value_7 ="";
                    var value_8 ="";
                    var value_9 ="";
                    var value_10 ="";
                    var value_11 ="";
                    var value_12 ="";
                    var value_13="";
                    var value_14 ="";
                    var value_15 ="";
                    var date1="";
                    var value3 ="";
                    var value4 ="";
                    var value5 ="";
                    var value6 ="";
                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];
                        value_9 = l[8];
                        value_10 = l[9];
                        value_11 = l[10];
                        value_12 = l[11];
                        value_13 = l[12];
                        value_14 = l[13];
                        value_15 = l[14];



                         value3 = getDateShow(value_3);
                         value4 = getDateShow(value_4);
                         value5 = getDateShow(value_5);
                         value6 = getDateShow(value_6);

                        function getDateShow(dateObject) {
                            var d = new Date(dateObject);
                            var day = d.getDate();
                            var month = d.getMonth() + 1;
                            var year = d.getFullYear();
                            if (day < 10) {
                                day = "0" + day;
                            }
                            if (month < 10) {
                                month = "0" + month;
                            }
                            var date = day + "/" + month + "/" + year;
                            date1 = year[2]+year[3]+month+day;


                            return date;
                        };





                    });

                    $("#pgid").val(value_1);

                    $("#term").val(value_2);
                    $("#dt-commence-date").val(value3);
                    $("#dt-risk-date").val(value4);
                    $("#dt-maturity-date").val(value5);
                    $("#dt-last-paid-date").val(value6);
                    $("#policy-status").val(value_7);
                    $("#option").val(value_8);
                    $("#sum-assured").val(value_9);
                    $("#product-name").val(value_10);
                    $("#client-name").val(value_11);
                    $("#client-type").val(value_12);
                    $("#partycd").val(value_13);
                    $("#PARTYID").val(value_15);



                    //  $("#client-type").val(value_14);

                    $("#intimation-person").val(value_11);
                    $("#client-tp").val(value_12);
                    $("#err_empID").text("");


                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

    });
    var flag = false;




    //for insert


    $("#save").click(function () {


        if (dataValidation()) {


            var pgid = $('#pgid').val();
            var partyid = $('#PARTYID').val();
            var partycd = $('#partycd').val();
            var accountno = $('#account-no').val();
            var bankcd = $('#drp-bank-name').val();
            var brcd = $('#drp-branch-name').val();
            var address = $('#branch-address').val();




            var suppben = {};
            suppben.pgid = pgid;
            suppben.partyid = partyid;
            suppben.partycd = partycd;
            suppben.accountno = accountno;
            suppben.bankcd = bankcd;
            suppben.brcd = brcd;
            suppben.address = address;



            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addClientAccountInfo",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;




                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });


    function dataValidation() {

        var policyno = $('#policy-no').val();


        if (policyno.length <1) {
            $("#err_policy_no").text("Empty field found!!");
            $("#policy-no").focus();
            var policyno = false;

        } else {
            var status = true;
        }

        return status;
    }



});

