function loadData(policyno) {


    var policyno = $('#policy-no').val();


    if (policyno != "" && policyno.length < 16) {
        $.ajax({
            url: "/claimpayment/claimDetails/" + policyno,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var value_1 = "";
                var value_2 = "";
                var value_3 = "";
                var value_4 = "";
                var value_5 = "";
                var value_6 = "";
                var value_7 ="";
                var value_8 ="";
                var value_9 ="";
                var value_10 ="";
                var value_11 ="";
                var value_12 ="";
                var value_13="";
                var value_14 ="";
                var date1="";
                $.each(response, function (i, l) {



                    value_1 = l[0];
                    value_2 = l[1];
                    value_3 = l[2];
                    value_4 = l[3];
                    value_5 = l[4];
                    value_6 = l[5];
                    value_7 = l[6];
                    value_8 = l[7];
                    value_9 = l[8];
                    value_10 = l[9];
                    value_11 = l[10];
                    value_12 = l[11];
                    value_13 = l[12];
                    value_14 = l[13];



                    value3 = getDateShow(value_3);
                    value4 = getDateShow(value_4);
                    value5 = getDateShow(value_5);
                    value6 = getDateShow(value_6);

                    function getDateShow(dateObject) {
                        var d = new Date(dateObject);
                        var day = d.getDate();
                        var month = d.getMonth() + 1;
                        var year = d.getFullYear();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        var date = day + "/" + month + "/" + year;
                        date1 = year[2]+year[3]+month+day;


                        return date;
                    };





                });

                $("#pgid").val(value_1);

                //INTIMATION DETAILS
                $.ajax({
                    url: "/claimpayment/penCommDetails/" + $("#pgid").val(),
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var value_1 = "";
                        var value_2 = "";
                        var value_3 = "";
                        var value_4 = "";
                       var appDate = ""

                        $.each(response, function (i, l) {



                            value_1 = l[0];
                            value_2 = l[1];
                            value_3 = l[2];
                            value_4 = l[3];





                            appDate = getDateShow(value_2);



                            function getDateShow(dateObject) {
                                var d = new Date(dateObject);
                                var day = d.getDate();
                                var month = d.getMonth() + 1;
                                var year = d.getFullYear();
                                if (day < 10) {
                                    day = "0" + day;
                                }
                                if (month < 10) {
                                    month = "0" + month;
                                }
                                var date = day + "/" + month + "/" + year;
                                date1 = year[2]+year[3]+month+day;


                                return date;
                            };





                        });



                        $("#application-serial").val(value_1);
                        $("#dt-application-date").val(appDate);

                        $("#commutation-perc").val(value_3);
                        $("#remarks").val(value_4);





                        //  $("#client-type").val(value_14);




                    },
                    error: function (xhr, status, error) {


                    }
                });

                $("#term").val(value_2);
                $("#dt-commence-date").val(value3);
                $("#dt-risk-date").val(value4);
                $("#dt-maturity-date").val(value5);
                $("#dt-last-paid-date").val(value6);
                $("#policy-status").val(value_7);
                $("#option").val(value_8);
                $("#sum-assured").val(value_9);
                $("#product-name").val(value_10);
                $("#client-name").val(value_11);
                $("#client-type").val(value_12);
                $("#partycd").val(value_13);



                //  $("#client-type").val(value_14);

                $("#intimation-person").val(value_11);
                $("#client-tp").val(value_12);
                $("#err_empID").text("");


            },
            error: function (xhr, status, error) {


            }
        });
    } else {


    }
}
$(document).ready(function () {
    $('#tbl-awaiting-apprval-lst').DataTable({

        // "sDom": "lfrti",
        "dom": '<"pull-left"f><"pull-right"l>tip',
        // paging:         true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: ""
        },
        "info":     false,
        "scrollY":        "600px",
        "scrollCollapse": true,
        "paging":         false

    }) ;


    $('.dataTables_filter').addClass('pull-left');

    $('.proposal-list-tr').css("cursor", "pointer");


    $(document).on("click", ".proposal-list-tr", function () {


        $(this).addClass("selected").siblings().removeClass("selected");
        $('#policy-no').val($('td', this).eq(0).find('span').html())
        loadData($('td', this).eq(0).find('span').html())

    });

    $('#err_policy_no').hide();


    $("#save").click(function () {

        if (1) {

            var pgid = $('#pgid').val();
            var appSl = $('#application-serial').val();

            var suppben = {};
            suppben.pgid = pgid;
            suppben.appSl = appSl;








            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/approvePenCommAprrv",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {

                    if(data==true)
                        showAlert("Successfully Approved.");
                    else
                        showAlertByType("Approval privilege limit exceeding for the amount: "+$('#appv-amt').val(),'F');
                    flag = true;

                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });


});