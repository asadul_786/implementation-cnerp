
$(document).ready(function () {
    var agentTable = $('#tableData');
    var agentTables = $('#agentTable').DataTable({


    });

    $(document).on('input', '#policyNo', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');

        var policyNo = $("#policyNo").val();


        if (policyNo != "" && policyNo.length < 16) {
            $.ajax({
                url: "/claimpayment/getPartyName/" + policyNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];





                    });

                    $("#partyName").val(value_1);



                },
                error: function (xhr, status, error) {


                }
            });
        } else {

        }
            });


    $('#tableData tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(10)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(1)').text();


        $.confirm({
            title: 'Confirm',
            content: 'Are your sure?',
            buttons: {
                ok: function () {
                    var invalidateClaimCalc = {};
                    invalidateClaimCalc.approveFlag = col2;
                    invalidateClaimCalc.payableNo = col1;
                    invalidateClaimCalc.pgid = col3;
                    invalidateClaimCalc.applSl = col4;


                    $.ajax({
                        contentType: 'application/json',
                        url:  "invalidateCalculation",
                        type: 'POST',
                        data: JSON.stringify(invalidateClaimCalc),
                        dataType: 'json',
                        success: function (commonMsg) {
                            // console.log(commonMsg);
                            if (commonMsg.isSucceed) {
                                showAlertByType( commonMsg.remarks +'['+ commonMsg.msgCode+']', "S");
                                // customAlert(alertTypes.SUCCESS, 'Success[' + commonMsg.msgCode + ']', 'Generated Successfully.');
                                $("#btnReset").trigger("click");

                            }
                            else {
                                showAlertByType( commonMsg.remarks +'['+ commonMsg.msgCode+']', "W");
                                // customAlert(alertTypes.FAILED, 'Unsuccessful[' + commonMsg.msgCode + ']', commonMsg.msg + '[' + commonMsg.remarks + ']');
                            }
                            agentTables.row( curRow ).remove().draw( false );
                        },
                        error: function(xhr, status, error) {
                            showAlert("Something went wrong !!");
                        },
                        complete: function () {
                        }
                    });
                },
                cancel: function () {

                }
            }
        });



        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    $('#demandTable').hide();


    //FILTERING DATA
    $("#btnapplyfiter").click(function () {

        // var flag = dataValidationEmployeeAll();



        if (1) {

            var policyNo = $('#policyNo').val();



            var invalidateClaimCalc = {};
            invalidateClaimCalc.policyNo = policyNo;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/invalidateClaimCalcFiltering",
                data: JSON.stringify(invalidateClaimCalc),
                dataType: 'json',
                success: function (data) {
                    if (data == "") {
                        showAlertByType('Data not found', "W");
                        $('#demandTable').hide();

                    }else {
                        agentTable.dataTable({
                            paging: true,
                            searching: true,
                            destroy: true,
                            data: data,
                            columns: [{
                                "data": "payableNo"
                            }, {
                                "data": "applSl"
                            }, {
                                "data": "pgid"
                            }, {
                                "data": "grossPayableAmt"
                            }, {
                                "data": "bonusPayableAmt"
                            }, {
                                "data": "suspenseAmt"
                            }, {
                                "data": "loanDed"
                            }, {
                                "data": "otherDedAmt"
                            }, {
                                "data": "netPayableAmt"
                            },{
                                "data": "paymentDt"
                            },{
                                "data": "approveFlag"
                            },{
                                "data": "paymentStatus"
                            },{
                                "data": "clmDetlNm"
                            }, {
                                "data": "partyNm"
                            },{
                                "data": "policyNo",
                                "render": function (data,
                                                    type,
                                                    row) {
                                    return "<a class='btn btn-success' id='edit'>"
                                        + '<i class="fas fa-comment-alt-dots"></i>'
                                        + 'Invalidate Claim'
                                        + "</a>";
                                }


                            }]
                        });
                        $('#demandTable').show();



                    }

                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");

                }

            });

        }

    });
})