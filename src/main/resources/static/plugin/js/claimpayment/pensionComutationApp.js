
$(document).ready(function () {
    // var table = $('#dataTable').DataTable({
    //
    //
    // });
    //
    // //DELETE
    // $('#dataTable tbody').on('click', '#delete', function () {
    //     var curRow = $(this).closest('tr');
    //     var col1 = curRow.find('td:eq(0)').text();
    //
    //     $.confirm({
    //         title: 'Confirm',
    //         content: 'Selected record will be deleted.',
    //         buttons: {
    //             ok: function () {
    //                 $.ajax({
    //                     contentType: 'application/json',
    //                     url:  "/claimpayment/intimationDelete/" + col1,
    //                     type: 'POST',
    //                     //async: false,
    //                     //data: JSON.stringify(answerDto),
    //                     dataType: 'json',
    //                     success: function(response) {
    //
    //                         if(response === true){
    //                             table.row( curRow ).remove().draw( false );
    //                             showAlert("Deleted Successfully");
    //                         }
    //                         else {
    //                             showAlert("Unknown error");
    //                         }
    //
    //                     },
    //                     error: function(xhr, status, error) {
    //                         showAlert("Unknown error");
    //                     }
    //                 });
    //             },
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    //
    // });




    $(document).on("input", "#policy-no", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyno = $('#policy-no').val();


        if (policyno != "" && policyno.length < 16) {
            $.ajax({
                url: "/new-business/claim-payment/policyDetails/" + policyno,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    var value_6 = "";
                    var value_7 ="";
                    var value_8 ="";
                    var value_9 ="";
                    var value_10 ="";
                    var value_11 ="";

                    $.each(response, function (i, l) {

                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];
                        value_9 = l[8];
                        value_10 = l[9];
                        value_11 = l[10];


                    });

                    $("#pgid").val(value_1);
                    $("#term").val(value_2);
                    $("#dt-commence-date").val(value_3);
                    $("#dt-risk-date").val(value_4);
                    $("#policy-status").val(value_5);
                    $("#dt-maturity-date").val(value_6);
                    $("#sum-assured").val(value_7);
                    $("#client-name").val(value_8);
                    $("#client-type").val(value_9);

                    $("#product-name").val(value_11);


                    $("#partyId").val(value_10);
                    $.ajax({
                        url: "/new-business/claim-payment/appSl/" + policyno,
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            var value_1 = "";


                            $.each(response, function (i, l) {

                                value_1 = l[0];



                            });

                            $("#application-serial").val(value_1);


                        },
                        error: function (xhr, status, error) {


                        }
                    });


                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

    });
    var flag = false;




    //for insert

    $("#save").click(function () {


        if (1) {

            var pgid = $('#pgid').val();
            var appSl = $('#application-serial').val();
            var appDate = $('#dt-application-date').val();
            var appPartyCd = '03';
            var appPartyId = $('#partyId').val();
            var commutationPct = $('#commutation-perc').val();
            var remarks = $('#remarks').val();




            var claimAppCommut = {};
            claimAppCommut.pgid = pgid;
            claimAppCommut.appSl = appSl;
            claimAppCommut.appDate = appDate;
            claimAppCommut.appPartyCd = appPartyCd;
            claimAppCommut.appPartyId = appPartyId;
            claimAppCommut.commutationPct = commutationPct;
            claimAppCommut.remarks = remarks;





            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/new-business/claim-payment/savePenCommApp",
                data: JSON.stringify(claimAppCommut),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;




                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });


    $( "#dt-application-date" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1970:2020'
    });


});