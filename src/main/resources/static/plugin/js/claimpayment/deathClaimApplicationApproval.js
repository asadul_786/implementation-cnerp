function loadData(policyno) {


    var policyno = $('#policy-no').val();


    if (policyno != "" && policyno.length < 16) {
        $.ajax({
            url: "/claimpayment/claimDetails/" + policyno,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var value_1 = "";
                var value_2 = "";
                var value_3 = "";
                var value_4 = "";
                var value_5 = "";
                var value_6 = "";
                var value_7 ="";
                var value_8 ="";
                var value_9 ="";
                var value_10 ="";
                var value_11 ="";
                var value_12 ="";
                var value_13="";
                var value_14 ="";
                var date1="";
                $.each(response, function (i, l) {



                    value_1 = l[0];
                    value_2 = l[1];
                    value_3 = l[2];
                    value_4 = l[3];
                    value_5 = l[4];
                    value_6 = l[5];
                    value_7 = l[6];
                    value_8 = l[7];
                    value_9 = l[8];
                    value_10 = l[9];
                    value_11 = l[10];
                    value_12 = l[11];
                    value_13 = l[12];
                    value_14 = l[13];



                    value3 = getDateShow(value_3);
                    value4 = getDateShow(value_4);
                    value5 = getDateShow(value_5);
                    value6 = getDateShow(value_6);

                    function getDateShow(dateObject) {
                        var d = new Date(dateObject);
                        var day = d.getDate();
                        var month = d.getMonth() + 1;
                        var year = d.getFullYear();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        var date = day + "/" + month + "/" + year;
                        date1 = year[2]+year[3]+month+day;


                        return date;
                    };





                });

                $("#pgid").val(value_1);

                //INTIMATION DETAILS
                $.ajax({
                    url: "/claimpayment/intimationDetails/" + $("#pgid").val(),
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var value_1 = "";
                        var value_2 = "";
                        var value_3 = "";
                        var value_4 = "";
                        var value_5 = "";
                        var value_6 = "";
                        var value_7 ="";
                        var value_8 ="";

                        $.each(response, function (i, l) {



                            value_1 = l[0];
                            value_2 = l[1];
                            value_3 = l[2];
                            value_4 = l[3];
                            value_5 = l[4];
                            value_6 = l[5];
                            value_7 = l[6];
                            value_8 = l[7];




                            value3 = getDateShow(value_2);
                            value4 = getDateShow(value_5);


                            function getDateShow(dateObject) {
                                var d = new Date(dateObject);
                                var day = d.getDate();
                                var month = d.getMonth() + 1;
                                var year = d.getFullYear();
                                if (day < 10) {
                                    day = "0" + day;
                                }
                                if (month < 10) {
                                    month = "0" + month;
                                }
                                var date = day + "/" + month + "/" + year;
                                date1 = year[2]+year[3]+month+day;


                                return date;
                            };





                        });



                        $("#intimationno").val(value_1);
                        $("#dtintimationdate").val(value3);
                        $("#cause").val(value_3);
                        $("#drclmdetlcd").val(value_4);
                        $("#dtdateofocc").val(value4);
                        $("#condolclient").val(value_6);
                        $("#informant").val(value_7);
                        $("#relation").val(value_8);



                        //  $("#client-type").val(value_14);
                        $.ajax({
                            url: "/claimpayment/netPayableAmount/" + $("#pgid").val(),
                            type: 'GET',
                            dataType: 'json',
                            success: function (response) {
                                var value_1 = "";


                                $.each(response, function (i, l) {

                                    value_1 = l[0];

                                });



                                $("#appv-amt").val(value_1);

                            },
                            error: function (xhr, status, error) {


                            }
                        });



                    },
                    error: function (xhr, status, error) {


                    }
                });

                $("#term").val(value_2);
                $("#dt-commence-date").val(value3);
                $("#dt-risk-date").val(value4);
                $("#dt-maturity-date").val(value5);
                $("#dt-last-paid-date").val(value6);
                $("#policy-status").val(value_7);
                $("#option").val(value_8);
                $("#sum-assured").val(value_9);
                $("#product-name").val(value_10);
                $("#client-name").val(value_11);
                $("#client-type").val(value_12);
                $("#partycd").val(value_13);



                //  $("#client-type").val(value_14);

                $("#intimation-person").val(value_11);
                $("#client-tp").val(value_12);
                $("#err_empID").text("");


            },
            error: function (xhr, status, error) {


            }
        });
    } else {


    }
}
$(document).ready(function () {
    $('#tbl-awaiting-apprval-lst').DataTable({

        // "sDom": "lfrti",
        "dom": '<"pull-left"f><"pull-right"l>tip',
        // paging:         true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: ""
        },
        "info":     false,
        "scrollY":        "600px",
        "scrollCollapse": true,
        "paging":         false

    }) ;


    $('.dataTables_filter').addClass('pull-left');

    $('.proposal-list-tr').css("cursor", "pointer");


    $(document).on("click", ".proposal-list-tr", function () {


        $(this).addClass("selected").siblings().removeClass("selected");
        $('#policy-no').val($('td', this).eq(0).find('span').html())
         loadData($('td', this).eq(0).find('span').html())

    });

    $('#err_policy_no').hide();
    $('#err_informant').hide();
    $('#err_dtintimationdate').hide();








    $("#drp-office-name").select2();


    $("#save").click(function () {

        if (1) {

            var pgid = $('#pgid').val();
            var slno = 1;
            var productcd = $('#partycd').val();
            var clmcausecd = $('#cause').val();
            var clmdetlcd = $('#drclmdetlcd').val();
            var remarks = $('#remarks').val();
            var benpartycd = $('#condolclient').val();
            var claimdt = $('#dt-approved-date').val();


            var intimationno = $('#intimationno').val();

            var sendheadoffice = $('#send-to-head-office').val();
            if(sendheadoffice == "on")
                sendheadoffice = 'Y';
            else
                sendheadoffice = 'N';

            var approveflag = $('#approve').val();
            if(approveflag == "on")
                approveflag = 'Y';
            else
                approveflag = 'N';
            var needinvestigation = $('#perform-investigation').val();


            if(needinvestigation == "on")
            {
                needinvestigation = 'Y';

            }
            else
                needinvestigation = 'N';





            var suppben = {};
            suppben.pgid = pgid;
            suppben.slno = slno;
            suppben.productcd = productcd;
            suppben.clmcausecd = clmcausecd;
            suppben.clmdetlcd = clmdetlcd;
            suppben.remarks = remarks;
            suppben.benpartycd = benpartycd;

            suppben.amout = $('#appv-amt').val();


            function getDateShow(dateObject) {
                var d = new Date(dateObject);
                var day = d.getDate();
                var month = d.getMonth() + 1;
                var year = d.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var date = day + "/" + month + "/" + year;
                // date = year[2]+year[3]+month+day;


                return date;
            };
            suppben.aprrvRjctDate = getDateShow(new Date());
            suppben.intimationno = intimationno;
            suppben.sendheadoffice = sendheadoffice;
            suppben.approveflag = 'Y';

            suppben.needinvestigation = needinvestigation;




            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/approveApplication",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully Approved.");


                    setTimeout(function () {
                        window.location.reload();
                    }, 1500);


                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });
    $("#reject").click(function () {


        if (1) {

            var pgid = $('#pgid').val();
            var slno = 1;
            var productcd = $('#partycd').val();
            var clmcausecd = $('#cause').val();
            var clmdetlcd = $('#drclmdetlcd').val();
            var remarks = $('#remarks').val();
            var benpartycd = $('#condolclient').val();
            var claimdt = $('#dt-approved-date').val();


            var intimationno = $('#intimationno').val();

            var sendheadoffice = $('#send-to-head-office').val();
            if(sendheadoffice == "on")
                sendheadoffice = 'N';
            else
                sendheadoffice = 'N';

            var approveflag = $('#approve').val();
            if(approveflag == "on")
                approveflag = 'N';
            else
                approveflag = 'N';
            var needinvestigation = $('#perform-investigation').val();


            if(needinvestigation == "on")
            {
                needinvestigation = 'Y';

            }
            else
                needinvestigation = 'N';





            var suppben = {};
            suppben.pgid = pgid;
            suppben.slno = slno;
            suppben.productcd = productcd;
            suppben.clmcausecd = clmcausecd;
            suppben.clmdetlcd = clmdetlcd;
            suppben.remarks = remarks;
            suppben.benpartycd = benpartycd;

            suppben.amout = $('#appv-amt').val();


            function getDateShow(dateObject) {
                var d = new Date(dateObject);
                var day = d.getDate();
                var month = d.getMonth() + 1;
                var year = d.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var date = day + "/" + month + "/" + year;
                // date = year[2]+year[3]+month+day;


                return date;
            };
            suppben.aprrvRjctDate = getDateShow(new Date());
            suppben.intimationno = intimationno;
            suppben.sendheadoffice = sendheadoffice;
            suppben.approveflag = 'N';

            suppben.needinvestigation = needinvestigation;




            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/approveApplication",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {


                    showAlertByType("Policy Rejected",'W');



                    setTimeout(function () {
                        window.location.reload();
                    }, 1500);
                    window.open('/claimpayment/rejectedPolicy.pdf?policyNo='+$('#policy-no').val(), '_blank');

                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });

});