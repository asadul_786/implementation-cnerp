


function getDisc() {

    var a = parseFloat($('#bonus-amt').val()),
        b = parseFloat($('#payable-amt').val());
    if($('#baseAmt').val()==''){
        $('#baseAmt').val(0);
        $('#net-pay-amt').val(a+b);
        $('#tot-payable-amt').val(a+b);
    }
    if($('#discFac').val()==''){
        $('#discFac').val(0);
        $('#net-pay-amt').val(a+b);
        $('#tot-payable-amt').val(a+b);

    }
    else{

        $('#tot-payable-amt').val(a+b);
        $('#net-pay-amt').val($('#tot-payable-amt').val()*($('#discFac').val()/$('#baseAmt').val()));

        var c = parseFloat($('#tot-payable-amt').val()),
            d = parseFloat($('#net-pay-amt').val());
        $('#disc-amt').val(c-d);
    }

}


function getPartyDetails()
{
    $.ajax({
        url: "/claimpayment/partyDetails/" + $('#pgid').val() + "/" + $('#drp-client-type').val(),
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var value_1 = "";
            var value_2 = "";




            $.each(response, function (i, l) {



                value_1 = l[0];
                value_2 = l[1];


            });

            $('#partyId').val(value_1);
            $('#client-name').val(value_2);


        },
        error: function (xhr, status, error) {
        }
    });
}

$( "#dt-appl-date" ).datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1970:2020'
});

$( "#dt-reqpay-date" ).datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1970:2020'
});



function diff_months(firstDate, secondDate) {


    firstDate = firstDate.split('/');
    secondDate = secondDate.split('/');

    firstDate = new Date(firstDate[2], firstDate[1], firstDate[0]);
    secondDate = new Date(secondDate[2], secondDate[1], secondDate[0]);

    var oneDay = 30*24*60*60*1000;
    var monDiff = Math.round(Math.abs((firstDate - secondDate) / oneDay));
    $('#month-diff').val(monDiff);

    $.ajax({
        url: "/claimpayment/getDiscount/"+monDiff,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var value_1 = "";
            var value_2 = "";



            $.each(response, function (i, l) {



                value_1 = l[0];
                value_2 = l[1];


            });


            $('#discFac').val(value_1);
            $('#baseAmt').val(value_2);





        },
        error: function (xhr, status, error) {


        }
    });
}

function paymentType(type)
{



    if(type=='M' && ($('#policyStatus').val()=='07'||$('#policyStatus').val()=='08')) {
        $.ajax({
            url: "/claimpayment/payableData/" + $('#productCd').val() + "/" + $('#drp-client-type').val(),
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var value_1 = "";
                var value_2 = "";
                var value_3 = "";
                var value_4 = "";



                $.each(response, function (i, l) {



                    value_1 = l[0];
                    value_2 = l[1];
                    value_3 = l[2];
                    value_4 = l[3];

                });

                $('#payable-amt').val((value_1*$('#sum-assured').val())/100);
                $.ajax({
                    url: "/claimpayment/getBonus/" + $('#pgid').val() + "/" + $('#term').val() + "/" + $('#productCd').val() + "/" + $('#term').val(),
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var value_1 = "";
                        $.each(response, function (i, l) {
                            value_1 = l[0];
                        });
                        $('#bonus-amt').val(value_1);
                        var a=$('#payable-amt').val();
                        var b=$('#bonus-amt').val();
                        var c = +a + +b;
                        $('#tot-payable-amt').val(c);
                        getDisc();
                    }, error: function (xhr, status, error) {

                    } });


            },
            error: function (xhr, status, error) {
            }
        });

    }else if(type=='S' && $('#policyStatus').val()=='09'){
        diff_months($('#dt-reqpay-date').val(),$('#dt-payable-date').val());
        if($('#drp-payment-type').val()=='S')
        {

            var date_string = moment($('#dt-reqpay-date').val(), "DD/MM/YYYY").format("MM/DD/YYYY");

            var ff = $.datepicker.formatDate( "dd-M-yy", new Date(date_string));

            $.ajax({
                url: "/claimpayment/stagePayDetails/" +ff + "/" + $('#pgid').val(),
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    var value_6 = "";
                    var value_7 = "";
                    var value_8 = "";
                    var value_9 = "";
                    var value_10 = "";
                    var value_11 = "";



                    $.each(response, function (i, l) {



                        value_1 = l[0];

                        value_2 = l[1];
                        value_3 = l[2];

                        value_4 = l[3];

                        value_5 = l[4];
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];
                        value_9 = l[8];
                        value_10 = l[9];
                        value_11 = l[10];

                    });


                    $("#dt-commence-date").val(value_1);
                    $("#policyStatus").val(value_2);
                    $("#policy-status").val(value_3);
                    $("#dt-last-paid-date").val(value_4);
                    $("#sum-assured").val(value_5);
                    $("#term").val(value_6);
                    $("#productCd").val(value_7);
                    $("#product-name").val(value_8);
                    $("#dt-payable-date").val(value_9);
                    $("#policyAgeYr").val(value_10);
                    $("#noOfPaidYr").val(value_11);


                    $.ajax({
                        url: "/claimpayment/stageCalDetails/" +$('#productCd').val() + "/" + $('#drp-client-type').val()+ "/" + $('#term').val()+ "/" + $('#policyAgeYr').val()+ "/" + $('#noOfPaidYr').val(),
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            var value_1 = "";
                            var value_2 = "";
                            var value_3 = "";
                            var value_4 = "";
                            var value_5 = "";




                            $.each(response, function (i, l) {



                                value_1 = l[0];

                                value_2 = l[1];
                                value_3 = l[2];

                                value_4 = l[3];
                                value_5 = l[4];


                            });


                            $("#payable-amt").val($('#sum-assured').val()*value_1/100);
                            $.ajax({
                                url: "/claimpayment/getBonus/" + $('#pgid').val() + "/" + $('#term').val() + "/" + $('#productCd').val() + "/" + $('#term').val(),
                                type: 'GET',
                                dataType: 'json',
                                success: function (response) {
                                    var value_1 = "";
                                    $.each(response, function (i, l) {
                                        value_1 = l[0];
                                    });
                                    $('#bonus-amt').val(value_1);
                                    var a=$('#payable-amt').val();
                                    var b=$('#bonus-amt').val();
                                    var c = +a + +b;
                                    $('#tot-payable-amt').val(c);
                                    getDisc();
                                }, error: function (xhr, status, error) {

                                } });

                            var dateString = moment($('#dt-commence-date').val(), "DD/MM/YYYY").format("MM/DD//YYYY");



                            var payDate = new Date(dateString).add(value_3*12).month();


                            $('#dt-payable-date').val($.datepicker.formatDate('dd/mm/yy', new Date(payDate)));






                        },
                        error: function (xhr, status, error) {
                            alert("errorrrr");
                        }
                    });



                },
                error: function (xhr, status, error) {
                }
            });
        }

    }

}

$(document).ready(function () {

    //DELETE
        $('#listTable tbody').on('click', '#delete', function () {
            var curRow = $(this).closest('tr');
            var col1 = curRow.find('td:eq(1)').text();

            $.confirm({
                title: 'Confirm',
                content: 'Selected record will be deleted.',
                buttons: {
                    ok: function () {
                        $.ajax({
                            contentType: 'application/json',
                            url:  "/claimpayment/deleteAdvPaymentApli/" + col1,
                            type: 'POST',
                            //async: false,
                            //data: JSON.stringify(answerDto),
                            dataType: 'json',
                            success: function(response) {

                                if(response === true){
                                    table.row( curRow ).remove().draw( false );
                                    showAlert("Deleted Successfully");
                                }
                                else {
                                    showAlert("Unknown error");
                                }

                            },
                            error: function(xhr, status, error) {
                                showAlert("Unknown error");
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });

        });
    var table = $('#listTable').DataTable({


    });
    //SAVE DATA
    var flag = true;

    $(document).on("click", "#save", function () {



        if (flag!= false)
        {
            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var clmAdvPaymentAppl = {};
                        clmAdvPaymentAppl.pgid = $("#pgid").val();

                        clmAdvPaymentAppl.applDt = $("#dt-appl-date").val();
                        clmAdvPaymentAppl.partyCd = $("#drp-client-type").val();
                        //clmAdvPaymentAppl.partyId = $("#pgid").val();
                        clmAdvPaymentAppl.paymentType = $("#drp-payment-type").val();
                        clmAdvPaymentAppl.reqPaymentDt = $("#dt-reqpay-date").val();
                        clmAdvPaymentAppl.netPayableAmt = $("#net-pay-amt").val();
                        clmAdvPaymentAppl.discFactor = $("#discFac").val();
                        clmAdvPaymentAppl.discAmt = $("#disc-amt").val();
                        //clmAdvPaymentAppl.approveBy = $("#pgid").val();
                        // clmAdvPaymentAppl.approveDt = $("#dt-appl-date").val();
                        clmAdvPaymentAppl.actualPayableAmt = $("#payable-amt").val();
                        clmAdvPaymentAppl.actualPayableDt = $("#dt-payable-date").val();
                        clmAdvPaymentAppl.actualBonusAmt = $("#bonus-amt").val();
                        clmAdvPaymentAppl.partyId = $("#partyId").val();


                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveClmAdvAppl",
                            type: 'POST',
                            data: JSON.stringify(clmAdvPaymentAppl),
                            dataType: 'json',
                            success: function(response) {

                                showAlert("Inserted Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }



    });


    //setting current date for Application Date
    var today  = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    today = dd+'/0'+mm+'/'+yyyy;
    $('#dt-appl-date').val(today)

    $(document).on("input", "#policy-no", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyno = $('#policy-no').val();


        if (policyno != "" && policyno.length < 16) {
            $.ajax({
                url: "/claimpayment/policyDetails/" + policyno,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    var value_6 = "";
                    var value_7 = "";
                    var value_8 = "";
                    var value_9 = "";
                    var value_10 = "";


                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];
                        value_9 = l[8];
                        value_10 = l[9];

                    });

                    $("#pgid").val(value_1);
                    $("#dt-commence-date").val(value_2);
                    $("#policyStatus").val(value_3);
                    $("#policy-status").val(value_4);
                    $("#dt-last-paid-date").val(value_5);
                    $("#sum-assured").val(value_6);
                    $("#term").val(value_7);
                    $("#productCd").val(value_8);
                    $("#product-name").val(value_9);
                    $("#dt-payable-date").val(value_10);

                    flag = true;

                    //  $("#client-type").val(value_14);


                },
                error: function (xhr, status, error) {
                    showAlertByType("Policy No. already applied","F");
                    flag = false;
                }
            });
        } else {


        }

    });






});