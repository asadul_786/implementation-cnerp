/**
 * Created by Saifuddin on 3/29/2020.
 */

$(document).ready(function () {

    $("#Report").click(function () {

        $(".error").text("");

        if (1) {
            var startdate = $('#start-date').val();
            var product = $('#product').val();
            var enddate = $('#end-date').val();
            var flag = 0;

            if (!startdate){
                $("#err_"+$('#start-date').attr("name")).text("This Field is Required!");
                flag = 1;
            }

            if (!enddate){
                $("#err_"+$('#end-date').attr("name")).text("This Field is Required!");
                flag = 1;
            }

            if (flag){
                return false;
            }

            var opera1 = startdate.split('/');
            var opera2 = enddate.split('/');

            if(new Date(opera1[2], opera1[0] - 1, opera1[1]) > new Date(opera2[2], opera2[0] - 1, opera2[1])) {
                showAlert("Start Date Must Be Less Than End Date!");
                return false;
            }

            else
            {
                window.open('/claimpayment/genClaimPaymentSummary?summary='+startdate+','+enddate+','+product, '_blank')
            }


        }


    });

});