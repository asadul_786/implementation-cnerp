$(document).ready(function () {

    // add the newbusinesss else nothing
    $("#btn_testadd").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var pgid = $('#pgid').val();
            var sumAssurd = $('#sumAssurd').val();
            var officeCD = $('#officeCD').val();
            var amount = $('#amount').val();


            var btest = {};
            btest.pgid = pgid;
            btest.sumAssurd = sumAssurd;
            btest.officeCD = officeCD;
            btest.amount = amount;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/security/addBTest",
                data: JSON.stringify(btest),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    clearform();
                },
                error: function (e) {
                    alert("Something Wrong!!!");
                }
            });

        }

    });

    $("#btnRefresh").click(function () {
        clearform();
    });

    function dataValidation() {
        var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
        var status = true;

        if ($("#pgid").val() == "") {
            status = false;
            $("#PGIDSpan").text("Empty field found!!");
            $("#pgid").focus();
        } else $("#PGIDSpan").text("");

        if ($("#sumAssurd").val() == "") {
            status = false;
            $("#sumassuredSpan").text("Empty field found!!");
            $("#sumAssurd").focus();
        } else $("#sumassuredSpan").text("");


        if ($("#officeCD").val() == "") {
            status = false;
            $("#officeCDSpan").text("Empty field found!!");
            $("#officeCD").focus();
        }
        else $("#officeCDSpan").text("");


        if ($("#amount").val() == "") {
            status = false;
            $("#amountSpan").text("Empty field found!!");
            $("#amount").focus();
        }
        else $("#amountSpan").text("");


        var str_amount = $('#amount').val();

        if (numberRegex.test(str_amount) == false
        ) {
            status = false;
            $("#amountSpan").text("Invalid Character!!");
            $("#amount").focus();

        } else
            $("#amountSpan").text("");


        var str_sumAssurd = $('#sumAssurd').val();

        if (numberRegex.test(str_sumAssurd) == false
        ) {
            status = false;
            $("#sumassuredSpan").text("Invalid Character!!");
            $("#sumAssurd").focus();
        } else
            $("#sumassuredSpan").text("");


        return status;
    }

    function clearform() {
        $('#pgid').val("");
        $('#sumAssurd').val("");
        $('#officeCD').val("");
        $('#amount').val("");
    }

    var showAlert = function (alertContent) {
        new PNotify({
            title: 'Info',
            text: alertContent,
            type: 'info',
            styling: 'bootstrap3'
        });
    }

});