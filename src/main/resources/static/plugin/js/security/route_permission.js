/**
 * Created by Geetanjali Oishe on 5/12/2019.
 */

$(document).ready(function () {



    $("#module-dropdown").change(function () {
        if ($("#role-dropdown :selected").val() != -1)
            moduleWiseTree();
    });

    $("#role-dropdown").change(function () {
        if ($("#module-dropdown :selected").val() != -1)
            moduleWiseTree();
    });

    function moduleWiseTree() {
        var moduleId = $("#module-dropdown").val();
        var roleId = $("#role-dropdown").val();
        $.get('/security/getModuleWiseTree', {
                moduleId: moduleId,
                roleId: roleId,
                ajax: true
            },
            function (data) {
                $("#tree").html(data);
                $("#submitButton").css("visibility", "visible");
                $("#submitButton").show();
                checkBoxFix();
            });
    }

    function checkBoxFix() {
        var checkedOptions = $(document).find("input:checkbox[name='routeStatus']:checked");  //find checked options

        if (checkedOptions.length > 0) {
            $('#moduleStatus').prop('checked', true);  //if any option checked, module is checked

            $(checkedOptions).each(function (index, value) {   // for any option checked, parent submodule checked
                var v = $(value).parents().eq(4).find("input:checkbox[name='subModuleStatus']").first().prop("checked", true);
            });
        }
        else {
            $( ":checkbox" ).prop('checked', false);
        }
    };

    $(document).on("change","#moduleStatus",function() {
        if (this.checked) {
            $( ":checkbox" ).prop('checked', true);  //check all Submodule and option
        }
        else {
            $( ":checkbox" ).prop('checked', false); //uncheck all Submodule and option
        }
    });

    $(document).on("change", "input[name='subModuleStatus']", function() {
        var subm = $(this).parentsUntil('li[data-type="subModule"]').parent();
        if (this.checked) {
            $(subm).children().find("input[type='checkbox']").prop("checked", true);   // check all options under this submodule
            $('#moduleStatus').prop('checked', true);                              // check module
        }
        else {
            $(subm).children().find("input[type='checkbox']").prop("checked", false); // uncheck all options under this submodule
            checkBoxFix();
        }
    });

    $(document).on("change", "input[name='routeStatus']", function () {
        if (this.checked) {
            routeCheck(this);
        }
        else {
            routeUncheck(this);
        }
    });

    $(document).on("change", "input[name='addPermission']", function (){
        if (this.checked)
            checkByPermission(this);
        else {
            var editPermission = $(this).siblings("input:checkbox[name='editPermission']:checked").length;
            var deletePermission = $(this).siblings("input:checkbox[name='deletePermission']:checked").length;
            var readPermission = $(this).siblings("input:checkbox[name='readPermission']:checked").length;
            var approvePermission = $(this).siblings("input:checkbox[name='approvePermission']:checked").length;
            var declinePermission = $(this).siblings("input:checkbox[name='declinePermission']:checked").length;
            var independentPermission = $(this).siblings("input:checkbox[name='independentPermission']:checked").length;
            var pageId = $(this).siblings("select[name='selectedPage']").val();


            //page id dropdown

            if (editPermission==0 && deletePermission==0 && deletePermission==0 && readPermission==0 &&
                approvePermission==0 && declinePermission==0 && independentPermission==0 && pageId == -1){
                $(this).siblings("input:checkbox[name='routeStatus']").prop("checked", false);
                routeUncheck(this);
            }
        }
    });

    $(document).on("change", "input[name='editPermission']", function (){
        if (this.checked)
            checkByPermission(this);
        else {
            var addPermission = $(this).siblings("input:checkbox[name='addPermission']:checked").length;
            var deletePermission = $(this).siblings("input:checkbox[name='deletePermission']:checked").length;
            var readPermission = $(this).siblings("input:checkbox[name='readPermission']:checked").length;
            var approvePermission = $(this).siblings("input:checkbox[name='approvePermission']:checked").length;
            var declinePermission = $(this).siblings("input:checkbox[name='declinePermission']:checked").length;
            var independentPermission = $(this).siblings("input:checkbox[name='independentPermission']:checked").length;
            var pageId = $(this).siblings("select[name='selectedPage']").val();

            if (addPermission==0 && deletePermission==0 && readPermission==0 &&
                approvePermission==0 && declinePermission==0 && independentPermission==0 && pageId == -1){
                $(this).siblings("input:checkbox[name='routeStatus']").prop("checked", false);
                routeUncheck(this);
            }
        }
    });

    $(document).on("change", "input[name='deletePermission']", function (){
        if (this.checked)
            checkByPermission(this);
        else {
            var addPermission = $(this).siblings("input:checkbox[name='addPermission']:checked").length;
            var editPermission = $(this).siblings("input:checkbox[name='editPermission']:checked").length;
            var readPermission = $(this).siblings("input:checkbox[name='readPermission']:checked").length;
            var approvePermission = $(this).siblings("input:checkbox[name='approvePermission']:checked").length;
            var declinePermission = $(this).siblings("input:checkbox[name='declinePermission']:checked").length;
            var independentPermission = $(this).siblings("input:checkbox[name='independentPermission']:checked").length;
            var pageId = $(this).siblings("select[name='selectedPage']").val();

            if (addPermission==0 && editPermission==0 && readPermission==0 &&
                approvePermission==0 && declinePermission==0 && independentPermission==0 && pageId == -1){
                $(this).siblings("input:checkbox[name='routeStatus']").prop("checked", false);
                routeUncheck(this);
            }
        }
    });

    $(document).on("change", "input[name='readPermission']", function (){
        if (this.checked)
            checkByPermission(this);
        else {
            var addPermission = $(this).siblings("input:checkbox[name='addPermission']:checked").length;
            var editPermission = $(this).siblings("input:checkbox[name='editPermission']:checked").length;
            var deletePermission = $(this).siblings("input:checkbox[name='deletePermission']:checked").length;
            var approvePermission = $(this).siblings("input:checkbox[name='approvePermission']:checked").length;
            var declinePermission = $(this).siblings("input:checkbox[name='declinePermission']:checked").length;
            var independentPermission = $(this).siblings("input:checkbox[name='independentPermission']:checked").length;
            var pageId = $(this).siblings("select[name='selectedPage']").val();

            if (addPermission==0 && editPermission==0 && deletePermission==0 &&
                approvePermission==0 && declinePermission==0 && independentPermission==0 && pageId == -1){
                $(this).siblings("input:checkbox[name='routeStatus']").prop("checked", false);
                routeUncheck(this);
            }
        }
    });

    $(document).on("change", "input[name='approvePermission']", function (){
        if (this.checked)
            checkByPermission(this);
        else {
            var addPermission = $(this).siblings("input:checkbox[name='addPermission']:checked").length;
            var editPermission = $(this).siblings("input:checkbox[name='editPermission']:checked").length;
            var deletePermission = $(this).siblings("input:checkbox[name='deletePermission']:checked").length;
            var readPermission = $(this).siblings("input:checkbox[name='readPermission']:checked").length;
            var declinePermission = $(this).siblings("input:checkbox[name='declinePermission']:checked").length;
            var independentPermission = $(this).siblings("input:checkbox[name='independentPermission']:checked").length;
            var pageId = $(this).siblings("select[name='selectedPage']").val();

            if (addPermission==0 && editPermission==0 && readPermission==0 &&
                deletePermission==0 && declinePermission==0 && independentPermission==0 && pageId == -1){
                $(this).siblings("input:checkbox[name='routeStatus']").prop("checked", false);
                routeUncheck(this);
            }
        }
    });

    $(document).on("change", "input[name='declinePermission']", function (){
        if (this.checked)
            checkByPermission(this);
        else {
            var addPermission = $(this).siblings("input:checkbox[name='addPermission']:checked").length;
            var editPermission = $(this).siblings("input:checkbox[name='editPermission']:checked").length;
            var deletePermission = $(this).siblings("input:checkbox[name='deletePermission']:checked").length;
            var readPermission = $(this).siblings("input:checkbox[name='readPermission']:checked").length;
            var approvePermission = $(this).siblings("input:checkbox[name='approvePermission']:checked").length;
            var independentPermission = $(this).siblings("input:checkbox[name='independentPermission']:checked").length;
            var pageId = $(this).siblings("select[name='selectedPage']").val();

            if (addPermission==0 && editPermission==0 && readPermission==0 &&
                approvePermission==0 && deletePermission==0 && independentPermission==0 && pageId == -1){
                $(this).siblings("input:checkbox[name='routeStatus']").prop("checked", false);
                routeUncheck(this);
            }
        }
    });

    $(document).on("change", "input[name='independentPermission']", function (){
        if (this.checked)
            checkByPermission(this);
        else {
            var addPermission = $(this).siblings("input:checkbox[name='addPermission']:checked").length;
            var editPermission = $(this).siblings("input:checkbox[name='editPermission']:checked").length;
            var deletePermission = $(this).siblings("input:checkbox[name='deletePermission']:checked").length;
            var readPermission = $(this).siblings("input:checkbox[name='readPermission']:checked").length;
            var approvePermission = $(this).siblings("input:checkbox[name='approvePermission']:checked").length;
            var declinePermission = $(this).siblings("input:checkbox[name='declinePermission']:checked").length;
            var pageId = $(this).siblings("select[name='selectedPage']").val();

            if (addPermission==0 && editPermission==0 && readPermission==0 &&
                approvePermission==0 && declinePermission==0 && deletePermission==0 && pageId == -1){
                $(this).siblings("input:checkbox[name='routeStatus']").prop("checked", false);
                routeUncheck(this);
            }
        }
    });

    $(document).on("change", "select[name='selectedPage']", function (){
        if ($(this).val() != -1)
            checkByPermission(this);
        else {
            var addPermission = $(this).siblings("input:checkbox[name='addPermission']:checked").length;
            var editPermission = $(this).siblings("input:checkbox[name='editPermission']:checked").length;
            var deletePermission = $(this).siblings("input:checkbox[name='deletePermission']:checked").length;
            var readPermission = $(this).siblings("input:checkbox[name='readPermission']:checked").length;
            var approvePermission = $(this).siblings("input:checkbox[name='approvePermission']:checked").length;
            var declinePermission = $(this).siblings("input:checkbox[name='declinePermission']:checked").length;
            var independentPermission = $(this).siblings("input:checkbox[name='independentPermission']:checked").length;


            if (addPermission==0 && editPermission==0 && readPermission==0 &&
                approvePermission==0 && declinePermission==0 && deletePermission==0 && independentPermission == 0){
                $(this).siblings("input:checkbox[name='routeStatus']").prop("checked", false);
                routeUncheck(this);
            }
        }
    });

    function checkByPermission (data) {
        $(data).siblings("input:checkbox[name='routeStatus']").prop("checked", true);
        $(data).parents().eq(4).find("input:checkbox[name='subModuleStatus']").first().prop("checked", true);
        $('#moduleStatus').prop('checked', true);
    }

    function routeUncheck(data) {
        $(data).parentsUntil('li[data-type="route"]').parent().children().find("input[type='checkbox']").prop("checked", false);

        $(data).parentsUntil('li[data-type="route"]').parent().children().find("select[name='selectedPage']").val(-1);

        var subMod = $(data).parentsUntil('li[data-type="subModule"]').parent().first();
        var checkedOptions = $(subMod).find("input:checkbox[name='routeStatus']:checked");

        if (checkedOptions.length == 0) {
            $(subMod).find("input:checkbox[name='subModuleStatus']:checked").prop('checked', false);
            checkBoxFix();
        }
    }

    function routeCheck(data) {
        $(data).parentsUntil('li[data-type="route"]').parent().children().find("input[type='checkbox']").prop("checked", true);
        $(data).parents().eq(4).find("input:checkbox[name='subModuleStatus']").first().prop("checked", true);
        $('#moduleStatus').prop('checked', true);
    }


    $('#submitButton').click(function (e) {
        // var options = $('#tree').find('li[data-type="option"]');
        // $('#checkboxId').is(':checked')
        // var opt = $(options).
        var options = $(document).find("input[name='routeStatus']");
        var add = $(document).find("input[name='addPermission']");
        var edit = $(document).find("input[name='editPermission']");
        var del = $(document).find("input[name='deletePermission']");
        var read = $(document).find("input[name='readPermission']");
        var approve = $(document).find("input[name='approvePermission']");
        var decline = $(document).find("input[name='declinePermission']");
        var pageId = $(document).find("select[name='selectedPage']").val();
        var independent = $(document).find("input[name='independentPermission']");


        $(options).each(function (index, value) {
            if (value.checked)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });

        $(add).each(function (index, value) {
            if (value.checked)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });

        $(edit).each(function (index, value) {
            if (value.checked)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });

        $(del).each(function (index, value) {
            if (value.checked)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });

        $(read).each(function (index, value) {
            if (value.checked)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });

        $(approve).each(function (index, value) {
            if (value.checked)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });

        $(decline).each(function (index, value) {
            if (value.checked)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });

        $(independent).each(function (index, value) {
            if (value.checked)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });

        $(pageId).each(function (index, value) {
            if (value != -1)
                $(value).prev().prop( "disabled", true );  //disable hidden value
        });


    });

    $('#refreshButton').click(function() {
        // $("#role-dropdown").empty();
        // $("#module-dropdown").empty();
        // getModuleList();
        // getRoleList();
        $("#submitButton").hide();
        $("#tree").empty();
    });



});