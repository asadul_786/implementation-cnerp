/**
 * Created by User on 06-Aug-19.
 */
$(document).ready(function () {

    //$("#orgDtlDiv").hide();

    $(document).on('input', '#total_pos', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });

    $(document).on('input', '#parent_org_dtl', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });

    $(document).on('input', '#extra_pos', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });
    
    function validateOrgDtl() {

        $("#err_desg").text("");
        $("#err_pos").text("");

        if($("#designation").val() == -1){
            $("#err_desg").text("Please select designation !!");
            return false;
        }

        if($("#total_pos").val() == ""){
            $("#err_pos").text("Required !!");
            return false;
        }

        return true;
    }

    function validateOrgMstr() {

        $("#err_off_catg").text("");
        $("#err_office").text("");
        $("#err_dept").text("");
        $("#err_is_active").text("");

        if($("#office_category").val() == -1){
            $("#err_off_catg").text("Please select office category !!");
            return false;
        }

        if($("#office_name").val() == -1){
            $("#err_office").text("Please select an office !!");
            return false;
        }

        if($("#department").val() == -1){
            $("#err_dept").text("Select department !!");
            return false;
        }

        if($("#is_active").val() == -1){
            $("#err_is_active").text("Required");
            return false;
        }

        return true;
    }

    $(document).on("change", "#office_category", function () {

        var catId = $("#office_category").val();

        if(catId != -1){

            $.get("get-office-by-category?offCatgId=" + catId,

             function (data, status) {

                 var offDrpDwn = $('#office_name');
                 offDrpDwn.empty();
                 offDrpDwn.append($('<option/>', {
                     value: "-1",
                     text: "-- Select Office --"
                 }));

                 $.each(data, function (index, office) {
                     offDrpDwn.append($('<option/>', {
                         value: office.office_CD,
                         text: office.office_NM
                     }));
                 });

             });
        }
        else{
            $('#office_name').empty();
            $('#office_name').append($('<option/>', {
                value: "-1",
                text: "-- Select Office --"
            }));
        }
    });

    /*$(document).on("change", "#office_name", function () {

        var officeId = $("#office_name").val();

        if(officeId != -1){

            $.get("get-available-departments?officeId=" + officeId,

                function (data, status) {

                    var deptDrpDwn = $('#department');
                    deptDrpDwn.empty();
                    deptDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Department --"
                    }));

                    $.each(data, function (index, department) {
                        deptDrpDwn.append($('<option/>', {
                            value: department.id,
                            text: department.departmentName
                        }));
                    });

                });
        }
        else{
            $('#department').empty();
            $('#department').append($('<option/>', {
                value: "-1",
                text: "-- Select Department --"
            }));
        }

    });*/

    var orgMstrId;
    var offId;

    $(document).on("click", "#save_org_mstr", function () {

        if(validateOrgMstr()){

            var orgMstr = {};
            orgMstr.officeCD = $("#office_name").val();
            orgMstr.deptCD = $("#department").val();
            orgMstr.isActive = $("#is_active").val();

            $.ajax({
                contentType: 'application/json',
                url:  "save-org-mstr",
                type: 'POST',
                //async: false,
                data: JSON.stringify(orgMstr),
                dataType: 'json',
                success: function(response) {

                    $("#orgMstrForm :input").prop("disabled", true);

                    if(response.exist == false){
                        showAlert("Saved successfully");
                    }

                    orgMstrId = response.orgMstr.id;
                    offId = response.orgMstr.officeId;

                    //load specific designations
                    getAvailDesg();
                    //load parent org detail
                    getParentOrgDtl();

                    $.get("get-org-dtl-by-org-mstr?orgMstrId=" + orgMstrId,

                        function (data, status) {

                            $.each(data, function (index, orgDtl) {

                                var newRow = "";
                                newRow += "<tr>";
                                newRow += "<td style='display: none'>" + orgDtl.id + "</td>";
                                newRow += "<td style='display: none'>" + orgDtl.desigCD + "</td>";
                                newRow += "<td style='text-align:center;vertical-align: middle'>" + (orgDtl.designation == null ? '' : orgDtl.designation.desig_NM) + "</td>";
                                newRow += "<td style='text-align:center'>" + (orgDtl.parentDetailId == null ? '' : orgDtl.organogramDetail.designation.desig_NM) + "</td>";
                                newRow += "<td style='text-align:center'>" + (orgDtl.totalPosition == null ? '' : orgDtl.totalPosition) + "</td>";
                                newRow += "<td style='text-align:center'>" + (orgDtl.extraPosition == null ? '' : orgDtl.extraPosition) + "</td>";
                                newRow += "<td style='text-align:center'>" + '<button id="edit" type="button" class="btn btn-success fa fa-pencil" style="text-align:center;vertical-align: middle;font-size:17px;"></button>' + "</td>";
                                newRow += "<td style='text-align:center'>" + '<button id="inactive" type="button" class="btn fa fa-trash-o" style="text-align:center;vertical-align: middle;font-size:22px;color:red;">' + "</td>";
                                newRow += "<td style='display: none'>" + (orgDtl.parentDetailId == null ? '' : orgDtl.parentDetailId) + "</td>";
                                newRow += "</tr>";
                                $("#orgDtlTable tbody").append(newRow);

                            });

                        });

                    $("#orgDtlDiv").show();
                },
                error: function(xhr, status, error) {
                    showAlert("Something went wrong !!");
                }
            });

        }

    });

    $(document).on("click", "#save_org_dtl", function () {

        if(validateOrgDtl()){

            var orgDtl = {};
            orgDtl.masterId = orgMstrId;
            orgDtl.id = orgDtlId;
            orgDtl.desigCD = $("#designation").val();
            orgDtl.totalPosition = $("#total_pos").val();
            orgDtl.parentDetailId = $("#parent_org_dtl").val();
            orgDtl.extraPosition = $("#extra_pos").val();
            orgDtl.isActive = 1;

            $.ajax({
                contentType: 'application/json',
                url:  "save-org-dtl",
                type: 'POST',
                //async: false,
                data: JSON.stringify(orgDtl),
                dataType: 'json',
                success: function(response) {

                    if(curRowEdit != ""){
                        //update
                        curRowEdit.attr("id", "newScroll");
                        // curRowEdit.find('td:eq(3)').text(response.parentDetailId == null ? '' : response.organogramDetail.designation.desig_NM);
                        curRowEdit.find('td:eq(3)').text(response.parentDetailId == null ? '' : response.parentDesgNm);
                        curRowEdit.find('td:eq(4)').text(response.totalPosition == null ? '' : response.totalPosition);
                        curRowEdit.find('td:eq(5)').text(response.extraPosition == null ? '' : response.extraPosition);
                        curRowEdit.find('td:eq(8)').text(response.parentDetailId == null ? '' : response.parentDetailId);
                        showAlert("Updated successfully");
                    }
                    else{
                        //add
                        var newRow = "";
                        newRow += "<tr id='newScroll'>";
                        newRow += "<td style='display: none'>" + response.id + "</td>";
                        newRow += "<td style='display: none'>" + response.desigCD + "</td>";
                        newRow += "<td style='text-align:center;vertical-align: middle'>" + (response.designation == null ? response.desgNm : response.designation.desig_NM) + "</td>";
                        // newRow += "<td style='text-align:center'>" + (response.parentDetailId == null ? response.parentDesgNm : response.organogramDetail.designation.desig_NM) + "</td>";
                        newRow += "<td style='text-align:center'>" + (response.parentDetailId == null ? '' : response.parentDesgNm) + "</td>";
                        newRow += "<td style='text-align:center'>" + (response.totalPosition == null ? '' : response.totalPosition) + "</td>";
                        newRow += "<td style='text-align:center'>" + (response.extraPosition == null ? '' : response.extraPosition) + "</td>";
                        newRow += "<td style='text-align:center'>" + '<button id="edit" type="button" class="btn btn-success fa fa-pencil" style="text-align:center;vertical-align: middle;font-size:17px;"></button>' + "</td>";
                        newRow += "<td style='text-align:center'>" + '<button id="inactive" type="button" class="btn fa fa-trash-o" style="text-align:center;vertical-align: middle;font-size:22px;color:red;">' + "</td>";
                        newRow += "<td style='display: none'>" + (response.parentDetailId == null ? '' : response.parentDetailId) + "</td>";
                        newRow += "</tr>";
                        $("#orgDtlTable tbody").append(newRow);

                        $("#designation option:selected").remove();

                        $('#parent_org_dtl').append($('<option/>', {
                            value: response.id,
                            text: response.desgNm
                        }));

                        showAlert("Saved successfully");
                    }

                    $('#newScroll').attr("style", "background-color: rgb(255, 245, 238);");
                    document.getElementById("newScroll").scrollIntoView();

                    setTimeout(function(){

                        $('#newScroll').removeAttr("style");
                        $('#newScroll').removeAttr("id");

                        $('html, body').animate({
                            scrollTop: $('#orgDtlDiv').offset().top
                        }, 500);

                    }, 3000);

                },
                error: function(xhr, status, error) {
                    showAlert("Something went wrong !!");
                }
            });

        }
    });

    $(document).on("click", "#reset_org_dtl", function () {

        getAvailDesg();
        getParentOrgDtl();

        $("#total_pos").val("");
        $("#parent_org_dtl").val("");
        $("#extra_pos").val("");
        $("#save_org_dtl").text("Add");

        orgDtlId = "";
        curRowEdit = "";

    });

    function getAvailDesg() {

        //load specific designations
        if(orgMstrId !== undefined){

            $('#designation').empty();
            $('#designation').append($('<option/>', {
                value: "-1",
                text: "Loading ..."
            }));

            $.get("get-available-designations?orgMstrId=" + orgMstrId,

                function (data, status) {

                    var desgDrpDwn = $('#designation');
                    desgDrpDwn.empty();
                    desgDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Designation --"
                    }));

                    $.each(data, function (index, designation) {
                        desgDrpDwn.append($('<option/>', {
                            value: designation.desig_CD,
                            text: designation.desig_NM
                        }));
                    });

                });
        }
    }

    function getParentOrgDtl() {

        //load specific parent org dtl
        if(orgMstrId !== undefined){

            $('#parent_org_dtl').empty();
            $('#parent_org_dtl').append($('<option/>', {
                value: "",
                text: "Loading ..."
            }));

            return $.get("get-parent-org-dtl?orgMstrId=" + orgMstrId,

                function (data, status) {

                    var parentOrgDrpDwn = $('#parent_org_dtl');
                    parentOrgDrpDwn.empty();
                    parentOrgDrpDwn.append($('<option/>', {
                        value: "",
                        text: "-- Select Parent --"
                    }));

                    $.each(data, function (index, orgDtl) {
                        parentOrgDrpDwn.append($('<option/>', {
                            value: orgDtl.id,
                            text: orgDtl.designation.desig_NM
                        }));
                    });

                });
        }
    }

    var orgDtlId = "";
    var curRowEdit = "";

    $('#orgDtlTable tbody').on('click', '#edit', function () {

        curRowEdit = $(this).closest('tr');
        var col1 = curRowEdit.find('td:eq(0)').text();
        var col2 = curRowEdit.find('td:eq(1)').text();
        var col3 = curRowEdit.find('td:eq(2)').text();
        var col4 = curRowEdit.find('td:eq(8)').text();
        var col5 = curRowEdit.find('td:eq(4)').text();
        var col6 = curRowEdit.find('td:eq(5)').text();

        orgDtlId = col1;

        var desgDrpDwn = $('#designation').empty();
        desgDrpDwn.empty();
        desgDrpDwn.append($('<option/>', {
            value: col2,
            text: col3
        }));

        $("#total_pos").val(col5);
        $("#extra_pos").val(col6);

        $("#save_org_dtl").text("Update");

        $('html, body').animate({
            scrollTop: $('#orgDtlDiv').offset().top
        }, 500);

        $.when(getParentOrgDtl()).done(function(){
            $("#parent_org_dtl option[value='"+col1+"']").remove();
            $("#parent_org_dtl").val(col4);
        });

    });

    $('#orgDtlTable tbody').on('click', '#inactive', function () {

        var orgDtl = {};
        var curRow = $(this).closest('tr');

        orgDtl.id = curRow.find('td:eq(0)').text();
        orgDtl.masterId = orgMstrId;
        orgDtl.desigCD = curRow.find('td:eq(1)').text();
        orgDtl.parentDetailId = curRow.find('td:eq(8)').text();
        orgDtl.totalPosition = curRow.find('td:eq(4)').text();
        orgDtl.extraPosition = curRow.find('td:eq(5)').text();
        orgDtl.isActive = 0;

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url:  "save-org-dtl",
                        type: 'POST',
                        //async: false,
                        data: JSON.stringify(orgDtl),
                        dataType: 'json',
                        success: function(response) {
                            curRow.remove();
                            showAlert("Removed successfully");
                            $("#reset_org_dtl").trigger("click");
                        },
                        error: function(xhr, status, error) {
                            showAlert("Something went wrong !!");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    });

});
