/**
 * Created by Geetanjali Oishe on 8/22/2019.
 */
$(document).ready(function () {


    var confirmUserSaveDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        var isActive;
                        if ($('#activeStatus').is(":checked")) {
                            isActive = 1;
                        }
                        else {
                            isActive = 0;
                        }

                        var userName = $('#user-name').val();
                        var password = $('#user-password').val();
                        var hrmEmpGid = $('#hrm-emp-id').val();
                        var hrmEmplId = $('#employee-id').val();
                        //var userId = $('#hidden-user-id').val();
                        var hrmEmpNm = $("#employee-name").val();


                        var user = {};

                        //user.userId = userId;
                        user.username = userName;
                        user.password = password;
                        user.employeeGid = hrmEmpGid;
                        user.employeeId = hrmEmplId;
                        user.employeeNm = hrmEmpNm;
                        user.isActive = isActive;


                        $.ajax({
                            contentType: 'application/json',
                            url:  "save-user",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(user),
                            dataType: 'json',
                            success: function(response) {
                                // String msgCode;
                                // String msg;
                                // String remarks;
                                // Boolean isSucceed ;

                                if(response.isSucceed == false){

                                    showAlertByType(response.msg, "F");
                                    console.log("User-setup"+response.remarks);


                                }
                                else{
                                    clearForm();
                                    showAlertByType("Successfully added.", "S");
                                }
                            },
                            error: function(xhr, status, error) {
                                showAlertByType("Something went wrong!", "F");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };


    $('#submitButton').on("click",function (e) {
       e.preventDefault();

       var isValid = userSetupValidation();
       if (isValid) {
           confirmUserSaveDialog("Are you sure to save this user?");
       }


    });

    function clearForm() {

        $(".err_msg").text("");
        $('#user-name').val("");
        $('#user-password').val("");
        $('#hrm-emp-id').val(-1);
        $('#hidden-user-id').val(-1);
        $('#employee-id').val("");
        $('#employee-name').val("");
        $('#activeStatus').prop("checked", false);
    }

    $('#refreshButton').on("click",function (e) {
        clearForm();
    });


    $('#emp_search_btn').click(function (event) {
            resetModal();

            $('#modal-search-empl').modal();
        });

    function resetModal() {
        $('#user_modal_empl_info').hide();
        $('#user_modal_empl_search').val("");
        $('#modal_span').text("");
        $('#user_modal_empl_tbody').empty();
        $('#err_user_modal_empl_search').text("");
    }

    $('#user_empl_search_btn').click(function (event) {
        event.preventDefault();
        var isVaild = validateUserEmplSearch();
        if(isVaild) {
            emplSearch();
        }

    });

    function validateUserEmplSearch() {
        $("#err_user_modal_empl_search").text("");

        if($('#user_modal_empl_search').val().length < 4) {
            $("#user_modal_empl_search").text("Enter at least 4 characters");
            return false;
        }

        else {
            return true;
        }

    }

    function emplSearch() {

        var tableData = $("#user_modal_empl_tbody");

        var nameOrId = $('#user_modal_empl_search').val();

        $.ajax({
            contentType: 'application/json',
            url:  "search-employee",
            type: 'POST',
            async: false,
            data: nameOrId,
            dataType: 'json',
            success: function(response) {
                $('#modal_span').text("");

                tableData.html("");
                html = "";
                $.each(response, function(idx, elem) {
                    if (idx < 10) {
                        html = "";
                        html += "<tr>";
                        html += "<td>" + elem.empId + "</td>";
                        html += "<td>" + elem.empNmEng + "</td>";
                        html += "<td>" + elem.officeName + "</td>";
                        html += "<td>" + elem.designationName + "</td>";
                        html += "<td>" + elem.joinDt + "</td>";
                        html += "<td>" + '<button type="button" class="btn btn-info user_empl_add_button" value="Add" button_id="' + elem.empGid + '"> ' +
                            '<i class="fa fa-plus"></i></button>' + "</td>";
                        html += "</tr>";
                        tableData.append(html);
                        $('#user_modal_empl_info').show();
                    }
                    else {
                        $('#modal_span').text("Result contains more than 10 data. Kindly Refine search parameters.");
                    }
                });

                if (response.length == 0) {
                    $('#modal_span').text('Search result empty.');
                }
            },
            error: function(xhr, status, error) {

            }
        });

    }


    $(document).on("click", ".user_empl_add_button", function (event) {
        $('#modal-search-empl').modal('hide')
        addEmployee(this);
    });

    function addEmployee(identifier) {
        var row_id = $(identifier).attr("button_id");
        console.log("row_id: " + row_id);

        $.ajax({
            contentType: 'application/json',
            url: "get-empl-by-id",
            type: 'POST',
            async: false,
            data: JSON.stringify(row_id),
            dataType: 'json',
            success: function (response) {
                $("#employee-id").val(response.empId); //not gid. empId
                $("#employee-name").val(response.empNmEng);
                // $("#nb_pro_emp_desig").val(response.designationName);
                // $("#nb_pro_emp_office").val(response.officeName);
                // $('#nb_pro_emp_join_dt').val(response.joinDt);
                $('#hrm-emp-id').val(row_id);
            },
            error: function (xhr, status, error) {
            }
        });

    }

    function userSetupValidation(){


        var userNmRegEx = /^(?=[a-z_\d]*[a-z])[a-z_\d]{6,30}$/i;

        // ***********  rules of username  *********
        // Minimum 6 characters
        // Maximum 30 characters
        // Only numbers are not allowed at least one character should be there
        // No special characters allowed except _
        // No space allowed
        // Character only is allowed

        var passRegEx = /^(?=[a-z_\d]*[a-z])[a-z_\d]{6,15}$/i;

        // ***********  rules of password  *********
        // Minimum 6 characters
        // Maximum 15 characters
        // Only numbers are not allowed at least one character should be there
        // No special characters allowed except _
        // No space allowed
        // Character only is allowed



        var isValid = true;
        $(".err_msg").text("");

        if ($('#user-name').val().length == 0)
            $('#err-user-name').text("Mandatory field");

        else if ($('#user-name').val().length < 6 || $('#user-name').val().length > 30)
            $('#err-user-name').text("Must be between 6 to 30 characters");

        else if (! userNmRegEx.test($('#user-name').val()))
            $('#err-user-name').text("Invalid format");


        if ($('#user-password').val().length == 0)
            $('#err-password').text("Mandatory field");

        else if ($('#user-password').val().length < 6 || $('#user-password').val().length > 15)
            $('#err-password').text("Must be between 6 to 15 characters");

        else if (! passRegEx.test($('#user-password').val()))
            $('#err-password').text("Invalid format[must be alphanumeric]");

        if ($('#employee-id').val().length == 0)
            $('#err-employee').text("Mandatory field");




            ///// span with error class
            // $('span.err_msg').each(function () {
            //     if($(this).hasClass("err_msg")) {
            //         if ($(this).text() != "") {
            //             return false;
            //         }
            //     }
            // });

        $("span[class='err_msg']").each( function () {
            if ($(this).text() != "") {
                isValid = false;
            }
            }
        )

            return isValid;
        }

     });