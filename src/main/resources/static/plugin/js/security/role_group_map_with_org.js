/**
 * Created by User on 22-Aug-19.
 */
$(document).ready(function () {

    function validateRoleMap() {

        $("#err_off_catg").text("");
        $("#err_office").text("");
        $("#err_dept").text("");
        $("#err_desg").text("");
        $("#err_role_group").text("");

        if($("#office_category").val() == -1){
            $("#err_off_catg").text("Please select office category !!");
            return false;
        }

        if($("#office_name").val() == -1){
            $("#err_office").text("Please select an office !!");
            return false;
        }

        if($("#department").val() == -1){
            $("#err_dept").text("Select department !!");
            return false;
        }

        if($("#designation").val() == -1){
            $("#err_desg").text("Select designation !!");
            return false;
        }

        if($("#role_group").val() == -1){
            $("#err_role_group").text("Please select a role !!");
            return false;
        }

        return true;
    }

    function saveRoleGroupMap() {

        var roleGroupMap = {};
        roleGroupMap.roleGrpId = $("#role_group").val();
        roleGroupMap.orgDtlId = $("#designation").val();
        //roleGroupMap.isActive = 1;

        return $.ajax({
            contentType: 'application/json',
            url:  "save-role-group-map",
            type: 'POST',
            //async: false,
            data: JSON.stringify(roleGroupMap),
            dataType: 'json',
            success: function(response) {

                $("#designation option:selected").remove();

                /*if(mstrId != ""){

                    //update
                    curRowEdit.find('td:eq(0)').text(response.roleGroup == null ? '' : response.roleGroup.roleGroupName);
                    curRowEdit.find('td:eq(1)').text(response.organogramDetail == null ? '' : response.organogramDetail.designation.designationName);
                    curRowEdit.find('td:eq(4)').text(response.id);
                    curRowEdit.find('td:eq(5)').text(response.roleGrpId);
                    curRowEdit.find('td:eq(6)').text(response.orgDtlId);

                    showAlert("Updated successfully !!");

                    $("#role_group").trigger("change");
                    $("#save_role_map").text("Save");

                }
                else{

                    //add
                    addRow(response);
                    showAlert("Saved successfully !!");

                }

                //reset
                mstrId = "";
                curRowEdit = "";*/

                //add
                addRow(response);
                showAlert("Saved successfully !!");

                $("#roleMapTable tr:last").attr("id", "newScroll");

                $('#newScroll').attr("style", "background-color: rgb(255, 245, 238);");
                document.getElementById("newScroll").scrollIntoView();

                setTimeout(function(){

                    $('#newScroll').removeAttr("style");
                    $('#newScroll').removeAttr("id");

                    $('html, body').animate({
                     scrollTop: $('#roleMapDiv').offset().top
                     }, 500);

                }, 3000);

            },
            error: function(xhr, status, error) {
                showAlert("Something went wrong !!");
            }
        });

    }

    $(document).on("change", "#office_category", function () {

        var catId = $("#office_category").val();

        if(catId != -1){

            $.get("get-office-by-category?offCatgId=" + catId,

                function (data, status) {

                    var offDrpDwn = $('#office_name');
                    offDrpDwn.empty();
                    offDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Office --"
                    }));

                    $.each(data, function (index, office) {
                        offDrpDwn.append($('<option/>', {
                            value: office.office_CD,
                            text: office.office_NM
                        }));
                    });

                });
        }
        else{
            $('#office_name').empty();
            $('#office_name').append($('<option/>', {
                value: "-1",
                text: "-- Select Office --"
            }));
        }
    });

    $(document).on("change", "#office_name", function () {

        var offId = $("#office_name").val();

        if(offId != -1){

            $.get("get-depts-by-office?offCD=" + offId,

                function (data, status) {

                    var deptDrpDwn = $('#department');
                    deptDrpDwn.empty();
                    deptDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Department --"
                    }));

                    $.each(data, function (index, orgDtl) {
                        deptDrpDwn.append($('<option/>', {
                            value: orgDtl.department.divdept_cd,
                            text: orgDtl.department.divdept_nm
                        }));
                    });

                });
        }
        else{
            $('#department').empty();
            $('#department').append($('<option/>', {
                value: "-1",
                text: "-- Select Department --"
            }));
        }
    });

    /*$(document).on("change", "#department", function () {

        var offId = $("#office_name").val();
        var deptId = $("#department").val();

        if(deptId != -1){

            $.get("get-desgs-by-dept?offId=" + offId + "&deptId=" + deptId,

                function (data, status) {

                    var desgDrpDwn = $('#designation');
                    desgDrpDwn.empty();
                    desgDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Designation --"
                    }));

                    $.each(data, function (index, orgDtl) {
                        desgDrpDwn.append($('<option/>', {
                            value: orgDtl.id,
                            text: orgDtl.designation.designationName
                        }));
                    });

                });
        }
        else{
            $('#designation').empty();
            $('#designation').append($('<option/>', {
                value: "-1",
                text: "-- Select Designation --"
            }));
        }
    });*/

    $(document).on("change", "#department", function () {

        var deptId = $("#department").val();

        if(deptId != -1){

            $.get("get-role-groups",

                function (data, status) {

                    var roleGrpDrpDwn = $('#role_group');
                    roleGrpDrpDwn.empty();
                    roleGrpDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Role Group --"
                    }));

                    $.each(data, function (index, roleGroup) {
                        roleGrpDrpDwn.append($('<option/>', {
                            value: roleGroup.id,
                            text: roleGroup.roleGroupName
                        }));
                    });

                });
        }
        else{
            $('#role_group').empty();
            $('#role_group').append($('<option/>', {
                value: "-1",
                text: "-- Select Role Group --"
            }));
        }
    });

    $(document).on("change", "#role_group", function () {

        var roleGrpId = $("#role_group").val();

        if(roleGrpId != -1){

            var offId = $("#office_name").val();
            var deptId = $("#department").val();

            $.get("get-desgs-by-dept?offCD=" + offId + "&deptCD=" + deptId,

                function (data, status) {

                    var desgDrpDwn = $('#designation');
                    desgDrpDwn.empty();
                    desgDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Designation --"
                    }));

                    $.each(data, function (index, orgDtl) {
                        desgDrpDwn.append($('<option/>', {
                            value: orgDtl.id,
                            text: orgDtl.designation.desig_NM
                        }));
                    });

                }).done(function () {

                    $.get("get-desgs-by-role-group?roleGrpId=" + roleGrpId,

                        function (data, status) {

                            $.each(data, function (index, roleMap) {

                                $("#designation option[value='"+roleMap.orgDtlId+"']").remove();

                            });

                            $("#roleMapTable tbody").empty();

                            $.each(data, function (index, roleMap) {

                                //load table data
                                addRow(roleMap);

                            });

                        });

            });

        }
        else{
            $('#designation').empty();
            $('#designation').append($('<option/>', {
                value: "-1",
                text: "-- Select Designation --"
            }));
        }

    });

    $(document).on("click", "#save_role_map", function () {

        if(validateRoleMap()){

            saveRoleGroupMap();

        }

    });

    //var mstrId = "";
    //var curRowEdit = "";

    /*$('#roleMapTable tbody').on('click', '#edit', function () {

        curRowEdit = $(this).closest('tr');
        //var desg = curRowEdit.find('td:eq(1)').text();
        var id = curRowEdit.find('td:eq(4)').text();
        var roleGrpId = curRowEdit.find('td:eq(5)').text();
        //var orgDtlId = curRowEdit.find('td:eq(6)').text();

        mstrId = id;

        $("#save_role_map").text("Update");

        //$('#role_group').val(roleGrpId);

        /!*$('#designation').append($('<option/>', {
            value: orgDtlId,
            text: desg
        }));

        $('#designation').val(orgDtlId);*!/

    });*/

    $('#roleMapTable tbody').on('click', '#inactive', function () {

        var curRow = $(this).closest('tr');
        var desg = curRow.find('td:eq(1)').text();
        var roleMapId = curRow.find('td:eq(3)').text();
        var orgDtlId = curRow.find('td:eq(5)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure?',
            buttons: {
                ok: function () {

                    $.ajax({
                        contentType: 'application/json',
                        url:  "del-role-group-map/" + roleMapId,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(roleMapId),
                        dataType: 'json',
                        success: function(response) {

                            curRow.remove();

                            $("#designation").append($('<option/>', {
                                value: orgDtlId,
                                text: desg
                            }));

                            showAlert("Removed successfully");

                        },
                        error: function(xhr, status, error) {
                            showAlert("Something went wrong !!");
                        }
                    });

                },
                cancel: function () {

                }
            }
        });

    });

    /*$(document).on("click", "#reset_role_map", function () {
        mstrId = "";
        curRowEdit = "";
        $("#save_role_map").text("Save");
    });*/

    function addRow(roleMap) {

        var newRow = "";
        newRow += "<tr>";
        newRow += "<td style='text-align:center'>" + (roleMap.roleGroup == null ? roleMap.roleGrpNmTmp  : roleMap.roleGroup.roleGroupName) + "</td>";
        newRow += "<td style='text-align:center'>" + (roleMap.organogramDetail == null ? roleMap.desigNmTmp : roleMap.organogramDetail.designation.desig_NM) + "</td>";
        //newRow += "<td style='text-align:center'>" + '<button id="edit" type="button" class="btn btn-success fa fa-pencil" style="text-align:center;vertical-align: middle;font-size:17px;"></button>' + "</td>";
        newRow += "<td style='text-align:center'>" + '<button id="inactive" type="button" class="btn fa fa-trash-o" style="text-align:center;vertical-align: middle;font-size:22px;color:red;">' + "</td>";
        newRow += "<td style='display: none'>" + (roleMap.id) + "</td>";
        newRow += "<td style='display: none'>" + (roleMap.roleGrpId) + "</td>";
        newRow += "<td style='display: none'>" + (roleMap.orgDtlId) + "</td>";
        newRow += "</tr>";

        $("#roleMapTable tbody").append(newRow);

    }

});
