/**
 * Created by Geetanjali Oishe on 5/8/2019.
 */
$(document).ready(function () {

    var confirmRoleMapSaveDialog = function (text, sendRoles) {

        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        saveData(sendRoles);
                    }
                },
                cancel: function () {

                }
            }
        });
    };

    function saveData(sendRoles) {
        var rolegroupId = Number($('#rolegroup-dropdown').val());
        var roles = sendRoles;
        $.post('/security/saveRoles', {
            selectedRoles: JSON.stringify(roles),
            roleGroupId: rolegroupId,
            ajax: true
        })

            .done( function (response) {
                    showAlertByType("Successfully saved", "S");
            })

            .fail( function(xhr, textStatus, errorThrown) {

                showAlertByType("Failed to save", "F");
            });


        // $('#rolegroup-role-map').submit();
    }



    $('#getMap').click(function () {
       // var selectedRoleGroup = $('#rolegroup-dropdown').val();

        $("#list").empty();
        $.getJSON('/security/getRoleList', {
                roleGroupId: Number($('#rolegroup-dropdown').val()),
                ajax: true
            },
            function (data) {

                populateList(data)
                $("#divRoleMap").show();

            });

    });

    function populateList(dt) {

        var available = new Array();
        var selected = new Array();
        var data = new Object();
        var a = dt[0];
        var s = dt[1];


        $.each( a, function( key, value ) {
            var obj1 = new Object();
            obj1 = {id: value.roleId, label: value.roleName, status: 'new'};
            available.push(obj1);
        });

        $.each( s, function( key, value ) {
            var obj2 = new Object();
            obj2 = {id: value.roleId, label: value.roleName, status: 'old'};
            selected.push(obj2);
        });

        data = {available: available, selected: selected};

        var instance = $('#list').pickList({
                data: data
            });
    }

    $('#submitButton').click(function (event) {
        event.preventDefault();

        // var test = $('.selected').find('option');
        var sendObj = new Object();
        var selected = $('.selected').find("option[data-status='new']");
        var available = $('.available').find("option[data-status='old']");
        var sendRoles = new Array();

        selected.each(function() {
            sendObj = {roleId: Number(this.value), roleStatus: $(this).data("status")};
            sendRoles.push(sendObj);
        });

        available.each(function() {
            sendObj = {roleId: Number(this.value), roleStatus: $(this).data("status")};
            sendRoles.push(sendObj);
        });


        if(true) {
            confirmRoleMapSaveDialog("Are you sure to save this setting?", sendRoles);
        }
    })


    $('#rolegroup-role-map').on("submit",function (e) {
        // var test = $('.selected').find('option');
        // var selectedRoles = new Array();
        //
        // test.each(function() {
        //     // var a = this.value;
        //     selectedRoles.push(this.value);
        // });
        //
        //
        // if(true) {
        //     confirmRoleMapSaveDialog("Are you sure to save this setting?", selectedRoles);
        // }

        // $(".error").remove();
        // var hasError = false;
        //
        // var name = $('#name').val();
        // var displayName = $('#display-name').val();
        //
        // if (name.length < 1) {
        //     $('#name').after('<span class="error">This field is required</span>');
        //     hasError = true;
        // }
        // if (displayName.length < 1) {
        //     $('#display-name').after('<span class="error">This field is required</span>');
        //     hasError = true;
        // }
        //
        // errorcheck


    });




});