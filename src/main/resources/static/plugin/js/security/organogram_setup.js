

$(document).ready(function () {
    var obj = {};
    var obj_value = {};
    var deptStack = [];



    function genDesigForDept(data) {
        for (var i=0; i<data.length; i++){
            var deptId = data[i][0];
            var array = obj[deptId];
            var arr = [data[i][3], data[i][4], data[i][5],
                data[i][6],data[i][8]]; // [desig_name, parent_detail_id, total_pos, extra_pos, detail_id]

            var arr_value = [data[i][2], data[i][4],
                data[i][5], data[i][6],data[i][8], data[i][7]]; //[desig_id, parent_detail_id, total_pos,
            // extra_pos,detail_id, master_id]

            if (array === undefined || array.length == 0) {

                obj[deptId] = new Array();
                obj[deptId].push(arr);

                obj_value[deptId] = new Array();
                obj_value[deptId].push(arr_value);
            }
            else {
                obj[deptId].push(arr);
                obj_value[deptId].push(arr_value);
            }


        }

    }

    function genDeptTableWithDesig(data) {
        $("#org_dept_body_div").show();
        $("#org_dept_table_div").show();

        var master_id = '';
        for (var i=0; i<data.length; i++) {
            master_id += data[i][7] + '-';
            var deptId = data[i][0];
            deptStack.push(deptId);


                var addDesig = "<button type='button' " + "dept_id=" + "'" + deptId + "'" +
                    "class='btn btn-info org-add-desig'>Add Designations</button>";

                var viewDesig = "<button type='button' " + "dept_id=" + "'" + deptId + "'" +
                    "class='btn btn-dark org-view-desig'>" +
                    "<i class='fa fa-plus'></i></button>";

                var deleteDept = "<button type='button' " + "dept_id=" + "'" + deptId + "'" +
                    "class='btn btn-danger dept-delete'>" +
                    "<i class='fa fa-trash'></i></button>";


                $("#org_dept_table_body_id").append(
                    "<tr>" +
                    "<td>" + viewDesig + "</td>" +
                    "<td>" + data[i][1] + "</td>" + //deparment name
                    "<td>" + data[i][1] + "</td>" +
                    "<td>" + addDesig + deleteDept + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    " <td id=" + "'" + 'td' + deptId + "'" +
                    "colspan='4'>" +
                    "</td>" +
                    "</tr>");



            //else deptStack.push(deptId);
        }

        $('#master_hidden_id').val(master_id);

    }

    function generateDeptTableBody(deptId) {
        var addDesig = "<button type='button' " + "dept_id=" + "'" + deptId + "'" + " dept_master_id=" + "'" + 0 + "'" +
            "class='btn btn-info org-add-desig'>Add Designations</button>";

        var viewDesig = "<button type='button' " + "dept_id=" + "'" + deptId + "'" +
            "class='btn btn-dark org-view-desig'>"+
            "<i class='fa fa-plus'></i></button>";

        var deleteDept = "<button type='button' " +  "dept_id=" + "'" + deptId + "'" +
            "class='btn btn-danger dept-delete'>" +
            "<i class='fa fa-trash'></i></button>";

        $("#org_dept_table_body_id").append(
            "<tr>" +
            "<td>" + viewDesig + "</td>"+
            "<td>"+ $("#org_dept_name option:selected").text() + "</td>" +
            "<td>" + $("#org_dept_short_name").val() + "</td>" +
            "<td>"+  addDesig + deleteDept +"</td>" +
            "</tr>" +
            "<tr>"+
            " <td id=" + "'" + 'td'+deptId + "'" +
            "colspan='4'>"+
            "</td>"+
            "</tr>");
    }

    function generateDesigView(deptId) {
        var deleteDesig = "<button type='button' class='btn btn-danger desig-delete'>" +
            "<i class='fa fa-trash'></i></button>";
        var tableBody = "<table class='table table-striped table-bordered bulk_action'>" +
            "<thead>" +
            "<tr>" +
            "<th width='20%'>Designation Name</th>" +
            "<th width='20%'>Parent Detail</th>" +
            "<th width='20%'>Total Position</th>" +
            "<th width='20%'>Extra Position</th>" +
            "<th width='20%'>Action</th>" +
            "<th style='display: none'></th>"+
            "</tr>" +
            "</thead>" +
            "<tbody>";
        var array = obj[deptId];
        for(var i=0; i<array.length; i++){

            if(array[i].length!=0){
                tableBody += "<tr>"+
                    "<td>" + array[i][0] + "</td>"+
                    "<td>" + array[i][1] + "</td>"+
                    "<td>" + array[i][2] + "</td>"+
                    "<td>" + array[i][3] + "</td>"+
                    "<td>" +
                    "<button type='button' class='btn btn-danger desig-delete'" + " index=" + "'" + i + "'" + " dept_id=" + "'" + deptId + "'" +">" +
                    "<i class='fa fa-trash'></i></button>" +
                    "</td>"+
                    "<td style='display: none'" + " id=" + "'" + array[i][4] + "'" + ">" +
                    "</td>"+
                    "</tr>";
            }


        }

        tableBody+= "</tbody>" +
            "</table>";

        return tableBody;
    }

    function generateDs(){
        var ds = {
            'name': 'general manager',
            'children': [

            ]
        };
    }



    $(document).on("change", "#office_category_id", function (e) {

        var catId = $("#office_category_id").val();

        $.get("get-office-by-category?offCatgId=" + catId,
            function (data, status) {
                var office = $('#org_office_id');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Office---"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices.office_CD,
                        text: offices.office_NM
                    }));
                });

            });
    });

    $(document).on("click", "#org_next", function (e) {
        var _officeCD = $("#org_office_id option:selected").val();
        $.ajax({
            contentType: 'application/json',
            url:  "get-org-by-office-cd",
            type: 'GET',
            async: false,
            data:  {officeCD : _officeCD},
            dataType: 'json',
            success: function(response) {
                if (!$.trim(response)){
                   // alert('blank');
                }
                else{
                    $('#save_mode').val('0');
                    $('#org_dept_table_body_id').html('');
                    genDeptTableWithDesig(response);
                    genDesigForDept(response);
                }

            },
            error: function(xhr, status, error) {
                alert("Something went wrong!");
            }
        });

        $("#office_category_id").prop('disabled', true);
        $("#org_office_id").prop('disabled', true);
        $("#org_next").prop('disabled', true);
        $("#refresh_button_org").prop('disabled', true);
        $("#org_dept_body_div").show();
    });

    $(document).on("click", "#org_dept_add", function (e) {
        if($('#org_dept_name').val() === '-1'){
            showAlert('Select a department!');
        }
        else{

            var deptId =  $("#org_dept_name option:selected").val();
            var deptIdInt = parseInt(deptId, 10);
            if(!deptStack.includes(deptIdInt)){
                obj[deptId] =  new Array();
                obj_value[deptId] = new Array();

                $("#org_dept_table_div").show();
                generateDeptTableBody(deptId);
                $("#org_dept_name").val('-1');
                $("#org_dept_short_name").val('');
                deptStack.push(deptIdInt);
            }
            else {
                showAlert('Department already added!');
            }
        }


    });

    $(document).on("click", ".org-view-desig", function (e) {

            var deptId = $(this).attr("dept_id");
            var str = '#td' + deptId;
            if ($(str).is(':empty')){
                $(this).children().remove();
                $(this).append("<i class='fa fa-minus'></i>");

                $(str).append(generateDesigView(deptId)); //generateDesigView()
            }
            else {
                $(this).children().remove();
                $(this).append("<i class='fa fa-plus'></i>");
                $(str).empty();
            }


      //  alert($(this).children().attr('class'))

    });

    $(document).on("click", ".org-add-desig", function (e) {
        var deptId = $(this).attr("dept_id");
        var masterId = $(this).attr("dept_master_id");
        $("#dept_id_hidden").val(deptId);
        $('#modal_org_desig').modal();
    });

     function checkDesigValidation() {
        var res= true;
        if($('#org_desig_id').val() === '-1'){
            showAlert('Select a designation');
            res = false;
        }
        if($('#org_parent_dtl').val() === '-1'){
            showAlert('Select parent detail');
            res = false;
        }

        if (!$.isNumeric($("#org_desig_total_position").val())){
            $("#org_desig_total_position").focus();
            showAlert('Enter numeric value');
            res = false;
        }

        if (!$.isNumeric($("#org_desig_extra_position").val())){
            $("#org_desig_extra_position").focus();
            showAlert('Enter numeric value');
            res = false;
        }

        return res;

    }
    $(document).on("click", "#save_desig", function (e) {
        var status = checkDesigValidation();
        if(status){
            var deptId = $("#dept_id_hidden").val();
            var arr = [
                $("#org_desig_id option:selected").text(),
                $('#org_parent_dtl option:selected').text(),
                $("#org_desig_total_position").val(),
                $('#org_desig_extra_position').val(),
                0

            ]; //empty check push

            var arr_value = [
                $("#org_desig_id option:selected").val(),
                $('#org_parent_dtl option:selected').val(),
                $("#org_desig_total_position").val(),
                $('#org_desig_extra_position').val(),
                0,
                0

            ]; //empty check push


            var array = obj[deptId];
            if (array === undefined || array.length == 0) {
                obj[deptId] = new Array();
                obj_value[deptId] = new Array();

                obj[deptId].push(arr);
                obj_value[deptId].push(arr_value);
            }
            else {
                obj[deptId].push(arr);
                obj_value[deptId].push(arr_value);
            }

            showAlert("Designations Added!");
            $('#org_parent_dtl').val(-1);
            $("#org_desig_id").val(-1);
            $("#org_desig_total_position").val('');
            $('#org_desig_extra_position').val('');
        }

        
    });

    $(document).on("click", ".dept-delete", function (e) {

        if($('#save_mode').val() === '1'){
            var deptId = $(this).attr("dept_id");
            var str = '#td' + deptId;
            $(str).empty();
            delete obj[deptId];
            delete obj_value[deptId];
            $(this).parent().parent().remove();
            showAlert('Successfully Deleted!')
        }
        else{
            var deptId = $(this).attr("dept_id");
            var str = '#td' + deptId;
            $(str).empty();
            delete obj[deptId];
            delete obj_value[deptId];

            $.ajax({
                contentType: 'application/json',
                url:  "delete-org-dept?deptCD=" + deptId + "&officeCD="+ $('#org_office_id option:selected').val(),
                type: 'DELETE',
                async: false,
                success: function(response) {
                    showAlert('Successfully Deleted!');
                },
                error: function(xhr, status, error) {
                    alert("Something went wrong!");
                }
            });

            $(this).parent().parent().remove();
        }



    });

    $(document).on("click", ".desig-delete", function (e) {
        if($('#save_mode').val()==='1'){
            var index = $(this).attr('index');
            var deptId = $(this).attr('dept_id');
            obj[deptId][index]= [];
            obj_value[deptId][index]= [];
            $(this).parent().parent().remove();
        }
        else{
            var index = $(this).attr('index');
            var deptId = $(this).attr('dept_id');
            obj[deptId][index]= [];
            obj_value[deptId][index]= [];
            if($(this).parent().parent()[0].lastElementChild.id != 0){
                $.ajax({
                    contentType: 'application/json',
                    url:  "delete-org-desig_by-dept?detailId=" + $(this).parent().parent()[0].lastElementChild.id,
                    type: 'DELETE',
                    async: false,
                    success: function(response) {
                        showAlert('Successfully Deleted!');
                    },
                    error: function(xhr, status, error) {
                        alert("Something went wrong!");
                    }
                });
            }

            $(this).parent().parent().remove();
        }

    });

    $(document).on("click", "#org_preview", function (e) {
        $('#modal_org_preview').modal();
        var ds = {
            'name': 'general manager',
            'children': [
                { 'name': 'Bo Miao', 'title': 'department manager' },
                { 'name': 'Su Miao', 'title': 'department manager',
                    'children': [
                        { 'name': 'Tie Hua', 'title': 'senior engineer' },
                        { 'name': 'Hei Hei', 'title': 'senior engineer',
                            'children': [
                                { 'name': 'Pang Pang', 'title': 'engineer' },
                                { 'name': 'Xiang Xiang', 'title': 'UE engineer' }
                            ]
                        }
                    ]
                },
                { 'name': 'Hong Miao', 'title': 'department manager' },
                { 'name': 'Chun Miao', 'title': 'department manager' }
            ]
        };

        $('#org_preview_container').html('<p></p>');
        $('#org_preview_container').orgchart({
            'visibleLevel': 10,
            'pan': true,
            'data' : ds,
            'nodeContent': 'title',
            'createNode': function($node, data) {
                $node.on('click', function(event) {

                    console.log(event);
                    /*if (!$(event.target).is('.edge, .toggleBtn')) {
                     var $this = $(this);
                     var $chart = $this.closest('.orgchart');
                     var newX = window.parseInt(($chart.outerWidth(true)/2) - ($this.offset().left - $chart.offset().left) - ($this.outerWidth(true)/2));
                     var newY = window.parseInt(($chart.outerHeight(true)/2) - ($this.offset().top - $chart.offset().top) - ($this.outerHeight(true)/2));
                     $chart.css('transform', 'matrix(1, 0, 0, 1, ' + newX + ', ' + newY + ')');
                     }*/
                });
            }
        });

    });

    $(document).on("click", "#org_save", function (e) {
        $.ajax({
            contentType: 'application/json',
            url:  "add-org-map?officeCD=" + $('#org_office_id option:selected').val()
            + "&masterIdEditMode=" + $('#master_hidden_id').val(),
            type: 'POST',
            async: false,
            data:  JSON.stringify(obj_value),
            dataType: 'json',
            success: function(response) {
              // alert(response);
               showAlert("Sucessfully saved!");
            },
            error: function(xhr, status, error) {
                alert("Something went wrong!");
            }
        });
    });



});





