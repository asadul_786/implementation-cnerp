
function edits() {

    $('#tableData tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();
        var col10 = curRow.find('td:eq(9)').text();
        var col11 = curRow.find('td:eq(10)').text();
        var col12 = curRow.find('td:eq(11)').text();
        var col13 = curRow.find('td:eq(12)').text();
        var col14 = curRow.find('td:eq(13)').text();
        var col15 = curRow.find('td:eq(14)').text();


        $('#slno').val(col1);
        $('#paytype').val(col2);
        $('#percentage').val(col3);
        $('#paybasiscd').val(col4);
        $('#paymenttype').val(col5);
        $('#installmenttype').val(col6);
        $('#paydependson').val(col7);
        $('#paymentyear').val(col8);
        $('#additionalben').val(col9);
        $('#intpercentage').val(col10);
        $('#addbencalon').val(col11);
        $('#timeframe').val(col12);
        $('#totaldays').val(col13);
        $('#premiumpayment').val(col14);
        $('#iusr').val(col15);

        //scroll up
        if (col12 == 1) {
            $("#timeframe").prop('checked', true);

        } else {

            $("#timeframe").prop('checked', false);

        }

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

}
function getList() {
    var suppbencd = $('#suppbencd').val();
    var clmcausecd = $('#clmcausecd').val();
    var clmdetlcd = $('#clmdetlcd').val();
    var benpartycode = $('#benpartycode').val();
    var educationTable = $('#tableData');
    $
        .ajax({
            type: "GET",
            url: "/claimpayment/listInfo/" +suppbencd +"/"+clmcausecd+"/"+clmdetlcd+"/"+benpartycode,
            success: function (data) {
                var no = 1;
                var tableBody = "";
                $('#tableData tbody').empty();
                $.each(data, function (idx, elem) {
                    var action =
                        '<button id="edit" onclick="edits()" type="button" class="btn fa fa-edit" style="text-align:center;vertical-align: middle;font-size:20px;"></button>';
                    tableBody += "<tr'>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td >" + elem[1] + "</td>";
                    tableBody += "<td>" + elem[2] + "</td>";
                    tableBody += "<td>" + elem[3] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td>" + elem[5] + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + elem[7] + "</td>";
                    tableBody += "<td >" + elem[8] + "</td>";
                    tableBody += "<td >" + elem[9] + "</td>";
                    tableBody += "<td >" + elem[10] + "</td>";
                    tableBody += "<td >" + elem[11] + "</td>";
                    tableBody += "<td >" + elem[12] + "</td>";
                    tableBody += "<td >" + elem[13] + "</td>";
                    tableBody += "<td >" + elem[14] + "</td>";
                    tableBody += "<td >" + elem[15] + "</td>";
                    tableBody += "<td>" + action + "</td>"
                    tableBody += "<tr>";
                });
                educationTable.append(tableBody);
            }
        });
}
function showpage() {
    $('#dtls').show();




}

function paytp() {
    if($('#paytype').val()=='F')
        document.getElementById("percentage").disabled = true;
    else
        document.getElementById("percentage").disabled = false;


}


function addben() {
    if($('#additionalben').val()=='03'){
        document.getElementById("intpercentage").disabled = true;
        document.getElementById("addbencalon").disabled = true;
    } else if($('#additionalben').val()=='01')
    {
        document.getElementById("intpercentage").disabled = true;
    }    else if($('#additionalben').val()=='02')
    {
        document.getElementById("intpercentage").disabled = false;
        document.getElementById("addbencalon").disabled = false;
    }

    else
    {
        document.getElementById("intpercentage").disabled = false;
        document.getElementById("addbencalon").disabled = false;
    }










}
$(document).ready(function () {



    var flag = false;

    if(flag== true){


    }
        var pathname = window.location.pathname;
        var short_path_name = pathname.split('/')[2];

        if (short_path_name === "supp-ben-claim") {
            $("#emp_gen_info_id").addClass('active');

    }



    //for insert

    $("#emp_master_add").click(function () {


        if (dataValidation()) {

            var suppbencd = $('#suppbencd').val();
            var clmcausecd = $('#clmcausecd').val();
            var clmdetlcd = $('#clmdetlcd').val();
            var benpartycode = $('#benpartycode').val();


            var suppben = {};
            suppben.suppbencd = suppbencd;
            suppben.clmcausecd = clmcausecd;
            suppben.clmdetlcd = clmdetlcd;
            suppben.benpartycode = benpartycode;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addSuppBen",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag =true;
                    document.getElementById("suppbencd").disabled = true;
                    document.getElementById("clmcausecd").disabled = true;
                    document.getElementById("clmdetlcd").disabled = true;
                    document.getElementById("benpartycode").disabled = true;
                    document.getElementById("emp_master_add").disabled = true;
                    document.getElementById("refresh_button_master").disabled = true;

                    $("#emp_tab").show();
                    getList();

                },
                error: function (e) {
                    alert("Sorry,Something Wrong!!");
                }
            });

        }


    });


    function dataValidation() {
        var suppbencd = $('#suppbencd').val();
        var clmcausecd = $('#clmcausecd').val();
        var clmdetlcd = $('#clmdetlcd').val();
        var benpartycode = $('#benpartycode').val();

        if (suppbencd == -1) {
            $('#suppbencd').after('<span class="error">This field is required</span>');
            var status = false;

        } else if (clmcausecd == -1) {
            $('#clmcausecd').after('<span class="error">This field is required</span>');
            var status = false;

        } else if (clmdetlcd == -1) {
            $('#clmdetlcd').after('<span class="error">This field is required</span>');
            var status = false;

        } else if (benpartycode == -1) {
            $('#benpartycode').after('<span class="error">This field is required</span>');
            var status = false;

        } else {
            var status = true;
        }

        return status;
    }

    function dataValidationDtls() {

        var paybasiscd = $('#paybasiscd').val();
        var additionalben = $('#additionalben').val();
        var paydependson = $('#paydependson').val();
        var installmenttype = $('#installmenttype').val();
        var addbencalon = $('#addbencalon').val();
        var paymenttype = $('#paymenttype').val();

        if (paybasiscd == -1) {

            $('#paybasiscd').after('<span class="error">This field is required</span>');
            var status2 = false;

        } else if (additionalben == -1) {
            $('#additionalben').after('<span class="error">This field is required</span>');
            var status2 = false;

        } else if (paydependson == -1) {
            $('#paydependson').after('<span class="error">This field is required</span>');
            var status2 = false;

        } else if (installmenttype == -1) {

            $('#installmenttype').after('<span class="error">This field is required</span>');
            var status2 = false;

        }else if (addbencalon == -1) {
            $('#addbencalon').after('<span class="error">This field is required</span>');
            var status2 = false;

        }else if (paymenttype == -1) {
            $('#paymenttype').after('<span class="error">This field is required</span>');
            var status2 = false;

        }else {

            var status2 = true;
        }

        return status2;
    }
    $("#addDtls").click(function () {


        if (dataValidationDtls()) {

            var suppbencd = $('#suppbencd').val();
            var clmcausecd = $('#clmcausecd').val();
            var clmdetlcd = $('#clmdetlcd').val();
            var benpartycode = $('#benpartycode').val();


            var paybasiscd = $('#paybasiscd').val();
            var percentage = $('#percentage').val();
            var installmenttype = $('#installmenttype').val();
            var paymentyear = $('#paymentyear').val();
            var addbencalon = $('#addbencalon').val();
            var totaldays = $('#totaldays').val();
            var paytype = $('#paytype').val();
            var paymenttype = $('#paymenttype').val();
            var paydependson = $('#paydependson').val();
            var intpercentage = $('#intpercentage').val();
            var timeframe = $('#timeframe').val();
            var additionalben = $('#additionalben').val();

            if(timeframe == "on")
                timeframe = '1';
            else
                timeframe = '0';

            var suppbendtls = {};
            suppbendtls.suppbencd = suppbencd;
            suppbendtls.clmcausecd = clmcausecd;
            suppbendtls.clmdetlcd = clmdetlcd;

            suppbendtls.benpartycode = benpartycode;
            suppbendtls.paybasiscd = paybasiscd;
            suppbendtls.percentage = percentage;
            suppbendtls.installmenttype = installmenttype;
            suppbendtls.paymentyear = paymentyear;
            suppbendtls.addbencalon = addbencalon;
            suppbendtls.totaldays = totaldays;
            suppbendtls.paytype = paytype;
            suppbendtls.paymenttype = paymenttype;
            suppbendtls.paydependson = paydependson;
            suppbendtls.intpercentage = intpercentage;
            suppbendtls.timeframe = timeframe;
            suppbendtls.additionalben = additionalben;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addSuppBenDtls",
                data: JSON.stringify(suppbendtls),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag =true;
                    $("#emp_tab").show();
                    getList();

                },
                error: function (e) {
                    alert("Sorry,Something Wrong!!");
                }
            });

        }

    });

});