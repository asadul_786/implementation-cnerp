/**
 * Created by golam nobi.
 */
function edits() {

    $('#tableData tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();
        var col10 = curRow.find('td:eq(9)').text();
        var col11 = curRow.find('td:eq(10)').text();
        var col12 = curRow.find('td:eq(11)').text();
        var col13 = curRow.find('td:eq(12)').text();
        var col14 = curRow.find('td:eq(13)').text();
        var col15 = curRow.find('td:eq(14)').text();


        $('#slno').val(col1);
        $('#paytype').val(col2);
        $('#percentage').val(col3);
        $('#paybasiscd').val(col4);
        $('#paymenttype').val(col5);
        $('#installmenttype').val(col6);
        $('#paydependson').val(col7);
        $('#paymentyear').val(col8);
        $('#additionalben').val(col9);
        $('#intpercentage').val(col10);
        $('#addbencalon').val(col11);
        $('#timeframe').val(col12);
        $('#totaldays').val(col13);
        $('#premiumpayment').val(col14);
        $('#iusr').val(col15);

        //scroll up
        if (col12 == 1) {
            $("#timeframe").prop('checked', true);

        } else {

            $("#timeframe").prop('checked', false);

        }

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

}
function getList() {
    var suppbencd = $('#suppbencd').val();
    var clmcausecd = $('#clmcausecd').val();
    var clmdetlcd = $('#clmdetlcd').val();
    var benpartycode = $('#benpartycode').val();
    var educationTable = $('#tableData');
    $
        .ajax({
            type: "GET",
            url: "/claimpayment/listInfo/" +suppbencd +"/"+clmcausecd+"/"+clmdetlcd+"/"+benpartycode,
            success: function (data) {
                var no = 1;
                var tableBody = "";
                $('#tableData tbody').empty();
                $.each(data, function (idx, elem) {
                    var action =
                        '<button id="edit" onclick="edits()" type="button" class="btn fa fa-edit" style="text-align:center;vertical-align: middle;font-size:20px;"></button>';
                    tableBody += "<tr'>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td >" + elem[1] + "</td>";
                    tableBody += "<td>" + elem[2] + "</td>";
                    tableBody += "<td>" + elem[3] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td>" + elem[5] + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + elem[7] + "</td>";
                    tableBody += "<td >" + elem[8] + "</td>";
                    tableBody += "<td >" + elem[9] + "</td>";
                    tableBody += "<td >" + elem[10] + "</td>";
                    tableBody += "<td >" + elem[11] + "</td>";
                    tableBody += "<td >" + elem[12] + "</td>";
                    tableBody += "<td >" + elem[13] + "</td>";
                    tableBody += "<td >" + elem[14] + "</td>";
                    tableBody += "<td >" + elem[15] + "</td>";
                    tableBody += "<td>" + action + "</td>"
                    tableBody += "<tr>";
                });
                educationTable.append(tableBody);
            }
        });
}

function showpage() {
    $('#dtls').show();
    $('#alt').hide();
    $('#addidtls').hide();
}


function addipage() {
    $('#addidtls').show();
    $('#alt').hide();
    $('#dtls').hide();
}
function alternativepage() {
    $('#alt').show();
    $('#dtls').hide();
    $('#addidtls').hide();
}


function paymenttp() {

    if ($('#paymenttype').val() != '01')
    {
        $('#installmenttype'). prop("disabled", true);

    }else {
        $('#installmenttype'). prop("disabled", false);
    }

}
function paytp() {

    if ($('#paytype').val() == 1)
    {
        $('#percentage'). prop("disabled", false);

    }else{
        $('#percentage'). prop("disabled", true);
    }

}
function addiben() {

    if ($('#additionalben').val() == "03")
    {
        $('#intpercentage').prop("disabled", true);
        $('#addibencalon').prop("disabled", true);

    }else if ($('#additionalben').val() == "01"){
        $('#intpercentage').prop("disabled", true);
        $('#addibencalon').prop("disabled", false);
    }else
    {
        $('#intpercentage').prop("disabled", false);
        $('#addibencalon').prop("disabled", false);
    }

}
function otclm() {

    if ($('#otherclaim').val() != 1)
    {
        $('#alternativeapplied').prop("disabled", true);

    }else{
        $('#alternativeapplied').prop("disabled", false);
    }

}
function refreshdtls() {

    $('#policytermfr').val("");
    $('#policytermto').val("");
    $('#paymenttype').val(-1);
    $('#installmenttype').val(-1);
    $('#paydependson').val(-1);
    $('#paymentyear').val("");
    $('#percentage').val("");
    $('#paybasiscd').val(-1);
    $('#commutationvalue').val("");
    $('#commutationper').val("");
    $('#additionalben').val(-1);
    $('#addibencalon').val(-1);
    $('#grandtedyr').val("");
    $('#intpercentage').val("");


}

function refreshalts() {

    $('#agefrom').val("");
    $('#ageto').val("");
    $('#partycd').val(-1);
    $('#paybasiscdalt').val(-1);
    $('#additionalbenalt').val(-1);
    $('#paypercentage').val("");
    $('#addbenpercentage').val("");
    $('#addibencalonalt').val(-1);



}

function refreshaddis() {
alert("sssssssssssssssss")
    $('#paybasisaddi').val(-1);
    $('#paypercentageaddi').val("");
    $('#additionalbenaddi').val(-1);
    $('#addbenpercentageaddi').val("");
    $('#addibencalonaddi').val(-1);



}



function age() {
    if ($('#ageof').val() != 1)
    {
        $('#partycd').prop("disabled", true);

    }else{
        $('#partycd').prop("disabled", false);
    }

}
$('#graduatedyrreq').click(function() {
    if (!$(this).is(':checked')) {
        $('#grandtedyr').prop("disabled", true);
    }else{

        $('#grandtedyr').prop("disabled", false);
    }
});
$('#percentagealt').click(function() {
    if (!$(this).is(':checked')) {
        $('#paypercentage').prop("disabled", true);
    }else{

        $('#paypercentage').prop("disabled", false);
    }
});

function addibenalt() {

    if ($('#additionalbenalt').val() == "03")
    {
        $('#addibencalonalt').prop("disabled", true);
        $('#addbenpercentage').prop("disabled", true);

    }else if ($('#additionalbenalt').val() == "01"){
        $('#addbenpercentage').prop("disabled", true);
        $('#addibencalonalt').prop("disabled", false);
    }else
    {
        $('#addbenpercentage').prop("disabled", false);
        $('#addibencalonalt').prop("disabled", false);
    }

}


function addibenaddi() {

    if ($('#additionalbenaddi').val() == "03")
    {
        $('#addbenpercentageaddi').prop("disabled", true);
        $('#addibencalonaddi').prop("disabled", true);

    }else if ($('#additionalbenaddi').val() == "01"){
        $('#addbenpercentageaddi').prop("disabled", true);
        $('#addibencalonaddi').prop("disabled", false);
    }else
    {
        $('#addbenpercentageaddi').prop("disabled", false);
        $('#addibencalonaddi').prop("disabled", false);
    }

}

$('#percentagealtaddi').click(function() {
    if (!$(this).is(':checked')) {
        $('#paypercentageaddi').prop("disabled", true);
    }else{

        $('#paypercentageaddi').prop("disabled", false);
    }
});



var agefromerror =0;
$(document).ready(function () {



    var flag = false;

    //INSERT PRODUCT WISE CLAIM
    $("#addPWC").click(function () {


        if (dataValidation()) {

            var productcd = $('#productcd').val();
            var clmcausecd = $('#clmcausecd').val();
            var clmdetlcd = $('#clmdetlcd').val();
            var benpartycd = $('#benpartycd').val();
            var condletterreq = $('#condletterreq').val();
            var appreq = $('#appreq').val();
            var intemationreq = $('#intemationreq').val();
            var noofcond = $('#noofcond').val();

            var noofapp = $('#noofapp').val();

            var noofintim = $('#noofintim').val();
            var certificatereq = $('#certificatereq').val();
            var certificatepreserve = $('#certificatepreserve').val();





            if(condletterreq == "on")
                condletterreq = '1';
            else
                condletterreq = '0'

            if(appreq == "on")
                appreq = '1';
            else
                appreq = '0'

            if(intemationreq == "on")
                intemationreq = '1';
            else
                intemationreq = '0'

            if(certificatereq == "on")
                certificatereq = '1';
            else
                certificatereq = '0';

            if(certificatepreserve == "on")
                certificatepreserve = '1';
            else
                certificatepreserve = '0';




            var suppben = {};
            suppben.productcd = productcd;
            suppben.clmcausecd = clmcausecd;
            suppben.clmdetlcd = clmdetlcd;
            suppben.benpartycd = benpartycd;
            suppben.condletterreq = condletterreq;
            suppben.appreq = appreq;
            suppben.intemationreq = intemationreq;
            suppben.noofcond = noofcond;
            suppben.noofapp = noofapp;
            suppben.noofintim = noofintim;
            suppben.certificatereq = certificatereq;
            suppben.certificatepreserve = certificatepreserve;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addProdWiseClaimMst",
                data: JSON.stringify(suppben),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag =true;
                    document.getElementById("productcd").disabled = true;
                    document.getElementById("clmcausecd").disabled = true;
                    document.getElementById("clmdetlcd").disabled = true;
                    document.getElementById("benpartycd").disabled = true;
                    document.getElementById("noofcond").disabled = true;
                    document.getElementById("noofapp").disabled = true;
                    document.getElementById("noofintim").disabled = true;
                    document.getElementById("addPWC").disabled = true;
                    document.getElementById("refresh_button_master").disabled = true;

                    $("#emp_tab").show();


                },
                error: function (e) {
                    alert("Sorry,Something Wrong!!");
                }
            });

        }


    });

    //INSERT PRODUCT WISE CLAIM DETAILS
    $("#addDtls").click(function () {

        var productcd = $('#productcd').val();
        var clmcausecd = $('#clmcausecd').val();
        var clmdetlcd = $('#clmdetlcd').val();
        var benpartycd = $('#benpartycd').val();


        var policytermto = $('#policytermto').val();
        var paymenttype = $('#paymenttype').val();
        var installmenttype = $('#installmenttype').val();
        var paydependson = $('#paydependson').val();
        var paymentyear = $('#paymentyear').val();
        var paytype = $('#paytype').val();
        var percentage = $('#percentage').val();
        var paybasiscd = $('#paybasiscd').val();
        var commutationvalue = $('#commutationvalue').val();
        var commutationper = $('#commutationper').val();
        var graduatedyrreq = $('#graduatedyrreq').val();
        var grandtedyr = $('#grandtedyr').val();
        var additionalben = $('#additionalben').val();
        var intpercentage = $('#intpercentage').val();
        var addibencalon = $('#addibencalon').val();
        var otherclaim = $('#otherclaim').val();
        var alternativeapplied = $('#alternativeapplied').val();


        var suppbendtls = {};
        suppbendtls.productcd = productcd;
        suppbendtls.clmcausecd = clmcausecd;
        suppbendtls.clmdetlcd = clmdetlcd;
        suppbendtls.benpartycd = benpartycd;
        suppbendtls.policytermto = policytermto;
        suppbendtls.paymenttype = paymenttype;
        suppbendtls.installmenttype = installmenttype;
        suppbendtls.paydependson = paydependson;
        suppbendtls.paymentyear = paymentyear;
        suppbendtls.paymentyear = paymentyear;
        suppbendtls.paytype = paytype;
        suppbendtls.percentage = percentage;
        suppbendtls.paybasiscd = paybasiscd;
        suppbendtls.commutationvalue = commutationvalue;
        suppbendtls.commutationper = commutationper;
        suppbendtls.grandtedyr = grandtedyr;
        suppbendtls.additionalben = additionalben;
        suppbendtls.intpercentage = intpercentage;
        suppbendtls.addibencalon = addibencalon;
        suppbendtls.otherclaim = otherclaim;
        suppbendtls.alternativeapplied = alternativeapplied;


        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/claimpayment/addProdWiseClaimDtls",
            data: JSON.stringify(suppbendtls),
            dataType: 'json',
            success: function (data) {
                showAlert("Successfully added.");
                flag =true;



            },
            error: function (e) {
                alert("Sorry,Something Wrong!!");
            }
        });



    });

    //INSERT PRODUCT WISE CLAIM DETIALS ALTERNATIVE
    $("#addDtlsalt").click(function () {
        var agefrom = $('#agefrom').val();
        if (agefrom == "") {
            $('#agefrom').after('<span class="error">This field is required</span>');

        } else {

            agefromerror=1;
        }
        if(agefromerror!=0){
            var productcd = $('#productcd').val();
            var clmcausecd = $('#clmcausecd').val();
            var clmdetlcd = $('#clmdetlcd').val();
            var benpartycd = $('#benpartycd').val();


            var agefrom = $('#agefrom').val();
            var ageto = $('#ageto').val();
            var ageof = $('#ageof').val();
            var partycd = $('#partycd').val();
            var paybasiscdalt = $('#paybasiscdalt').val();
            var percentagealt = $('#percentagealt').val();

            var paypercentage = $('#paypercentage').val();
            var additionalbenalt = $('#additionalbenalt').val();
            var addbenpercentage = $('#addbenpercentage').val();
            var bencalon = $('#bencalon').val();

            if(percentagealt == "on")
                percentagealt = '1';
            else
                percentagealt = '0';
            var suppbendtls = {};
            suppbendtls.productcd = productcd;
            suppbendtls.clmcausecd = clmcausecd;
            suppbendtls.clmdetlcd = clmdetlcd;
            suppbendtls.benpartycd = benpartycd;
            suppbendtls.agefrom = agefrom;

            suppbendtls.ageto = ageto;
            suppbendtls.ageof = ageof;
            suppbendtls.partycd = partycd;
            suppbendtls.paybasiscdalt = paybasiscdalt;
            suppbendtls.percentagealt = percentagealt;
            suppbendtls.paypercentage = paypercentage;
            suppbendtls.additionalbenalt = additionalbenalt;
            suppbendtls.addbenpercentage = addbenpercentage;
            suppbendtls.bencalon = bencalon;




            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addProdWiseClaimDtlsAlt",
                data: JSON.stringify(suppbendtls),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag =true;



                },
                error: function (e) {
                    alert("Sorry,Something Wrong!!");
                }
            });
        }




    });

    //INSERT PRODUCT WISE CLAIM DETIALS ADDITIONAL
    $("#addDtlsaddi").click(function () {

        var productcd = $('#productcd').val();
        var clmcausecd = $('#clmcausecd').val();
        var clmdetlcd = $('#clmdetlcd').val();
        var benpartycd = $('#benpartycd').val();


        var paybasisaddi = $('#paybasisaddi').val();
        if(paybasisaddi!='-1'){
            var percentagealtaddi = $('#percentagealtaddi').val();
            var paypercentageaddi = $('#paypercentageaddi').val();
            var additionalbenaddi = $('#additionalbenaddi').val();
            var addbenpercentageaddi = $('#addbenpercentageaddi').val();
            var bencalonaddi = $('#bencalonaddi').val();

            if(percentagealtaddi == "on")
                percentagealtaddi = '1';
            else
                percentagealtaddi = '0';

            var suppbendtls = {};
            suppbendtls.productcd = productcd;
            suppbendtls.clmcausecd = clmcausecd;
            suppbendtls.clmdetlcd = clmdetlcd;
            suppbendtls.benpartycd = benpartycd;

            suppbendtls.paybasisaddi = paybasisaddi;
            suppbendtls.percentagealtaddi = percentagealtaddi;
            suppbendtls.paypercentageaddi = paypercentageaddi;
            suppbendtls.additionalbenaddi = additionalbenaddi;
            suppbendtls.addbenpercentageaddi = addbenpercentageaddi;
            suppbendtls.bencalonaddi = bencalonaddi;




            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/claimpayment/addProdWiseClaimDtlsAddi",
                data: JSON.stringify(suppbendtls),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag =true;



                },
                error: function (e) {
                    alert("Sorry,Something Wrong!!");
                }
            });
        }else
        {
            $('#paybasisaddi').after('<span class="error">This field is required</span>');
        }




    });

    //PRODUCT WISE CLAIM VALIDATION
    function dataValidation() {
        var productcd = $('#productcd').val();
        var clmcausecd = $('#clmcausecd').val();
        var clmdetlcd = $('#clmdetlcd').val();
        var benpartycd = $('#benpartycd').val();

        if (productcd == -1) {
            $('#productcd').after('<span class="error">This field is required</span>');
            var status = false;

        } else if (clmcausecd == -1) {
            $('#clmcausecd').after('<span class="error">This field is required</span>');
            var status = false;

        } else if (clmdetlcd == -1) {
            $('#clmdetlcd').after('<span class="error">This field is required</span>');
            var status = false;

        } else if (benpartycd == -1) {
            $('#benpartycd').after('<span class="error">This field is required</span>');
            var status = false;

        } else {
            var status = true;
        }

        return status;
    }




});