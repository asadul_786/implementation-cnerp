/**
 * Created by md mithu sarker on 6/24/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();

    /*$(".code_generator").change(function () {

        var shortCatName = "";
        var categoryName = $("#category-name").val();
        var categoryNameLength = 0;

        if (!isEmptyString(categoryName)) {
            categoryName = categoryName.toUpperCase();
            categoryNameLength = categoryName.trim().length;

            if (categoryNameLength > 2) {
                shortCatName = categoryName.substring(0, 3);
            }
            else {
                categoryNameLength = categoryNameLength == 0 ? 0 : categoryNameLength - 1;
                shortCatName = categoryName.substring(0, categoryNameLength - 1);
            }


            if ($("#category-code").val().length < 1) {
                var d = new Date();

                var daySum = parseInt(d.getMonth()) + parseInt(d.getDate()) + parseInt(d.getFullYear());
                //console.log(parseInt(d.getMonth()));
                //console.log(parseInt(d.getDate()));
                //console.log(parseInt(d.getFullYear()));
                var catCode = shortCatName + '-' + daySum + '' + Math.floor(Math.random() * 90 + 10);


                //  if($("#category-short-name").val().length<3) {
                $("#category-short-name").val(shortCatName);
                $("#category-code").val(catCode);
                //  }
            }


        }

    });*/

    $("#currentRegisterId").on("change", function () {

        var currentRegisterId = $('#currentRegisterId').val();
        var selectedValue =$( "#currentRegisterId option:selected" ).text();

        if (currentRegisterId != "") {

            selectedValue = selectedValue.toUpperCase();
            //selectedValue = selectedValue.substring(0, 3);
            var splitValue = selectedValue.match(/(.+)\_(.+)/);

            var aliasName = splitValue[1];
            var aliasCode = splitValue[2];

            $('#itemNameAlias').val(aliasName);
            $('#itemCodeAlias').val(aliasCode);

        }else {
            if ($('#currentRegisterId').val() == "") {
                $('#itemNameAlias').val("");
                $('#itemCodeAlias').val("");
            }
            else {
                $("#error_UDForApproval").text("Utility Current Register Name Is Invalid !!!");
            }
        }

    });


    /*-------------- asset save function start ------------------------*/
    $("#Save").click(function () {
        saveData();
    });
    $("#Referesh").click(function () {
        clearForm();
    });

    function getFormData() {
        var utilityItems = {};
        utilityItems.itemNo = $("#itemNo").val();
        utilityItems.currentRegisterId = $("#currentRegisterId").val();
        utilityItems.itemNameAlias = $("#itemNameAlias").val();
        utilityItems.itemCodeAlias = $("#itemCodeAlias").val();
        utilityItems.insertUser = $('#insertUser').val();
        return utilityItems;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/utility-ajax/save-utility-items";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 1500);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#itemNo").val('');
        $("#currentRegisterId").val('');
        $("#itemNameAlias").val('');
        $("#itemCodeAlias").val('');
        $("#insertUser").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#currentRegisterId").val())) {

            valdiationMsgContent = "Current register is required!";
            $("#currentRegisterId").focus();
            return false;
        }
        else if (isEmptyString($("#itemNameAlias").val())) {

            valdiationMsgContent = "Item name alias is required!";
            $("#itemNameAlias").focus();
            return false;
        }
        else if ($("#itemNameAlias").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Item name alias maximum 50 characters!!";
            $("#itemNameAlias").focus();
            return false;
        }
        else if (isEmptyString($("#itemCodeAlias").val())) {

            valdiationMsgContent = "Item code alias is required!";
            $("#itemCodeAlias").focus();
            return false;
        }
        else if ($("#itemCodeAlias").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Item code alias maximum 50 characters!!";
            $("#itemCodeAlias").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var itemNo = curRow.find('td:eq(0)').text();
        var currentRegisterId = curRow.find('td:eq(1)').text();
        var itemNameAlias = curRow.find('td:eq(3)').text();
        var itemCodeAlias = curRow.find('td:eq(4)').text();
        var insertUser = curRow.find('td:eq(5)').text();

        $('#itemNo').val(itemNo);
        $('#currentRegisterId').val(currentRegisterId).trigger('change');
        $('#itemNameAlias').val(itemNameAlias);
        $('#itemCodeAlias').val(itemCodeAlias);
        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
