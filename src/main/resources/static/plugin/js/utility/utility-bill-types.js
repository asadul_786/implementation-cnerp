/**
 * Created by md mithu sarker on 6/24/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();



    /*-------------- asset save function start ------------------------*/
    $("#Save").click(function () {
        saveData();
    });
    $("#Referesh").click(function () {
        clearForm();
    });

    function getFormData() {
        var utilityBillTypes = {};
        utilityBillTypes.billTypeNO = $("#billTypeNO").val();
        utilityBillTypes.billTypeName = $("#billTypeName").val();
        utilityBillTypes.billTypeCode = $("#billTypeCode").val();
        utilityBillTypes.partialReceivable = $("#partialReceivable").val();
        utilityBillTypes.insertUser = $('#insertUser').val();
        return utilityBillTypes;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/utility-ajax/save-utility-bill-types";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 1500);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#billTypeNO").val('');
        $("#billTypeName").val('');
        $("#billTypeCode").val('');
        $("#partialReceivable").val('');
        $("#insertUser").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#billTypeName").val())) {

            valdiationMsgContent = "Bill type name is required!";
            $("#billTypeName").focus();
            return false;
        }
        else if ($("#billTypeName").val().length >30) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Bill type name maximum 30 characters!!";
            $("#billTypeName").focus();
            return false;
        }
        else if (isEmptyString($("#billTypeCode").val())) {

            valdiationMsgContent = "Bill type code is required!";
            $("#billTypeCode").focus();
            return false;
        }
        else if ($("#billTypeCode").val().length >10) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Bill type code maximum 10 characters!!";
            $("#billTypeCode").focus();
            return false;
        }
        else if (isEmptyString($("#partialReceivable").val())) {

            valdiationMsgContent = "Partial Receivable is required!";
            $("#partialReceivable").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var billTypeNO = curRow.find('td:eq(0)').text();
        var billTypeName = curRow.find('td:eq(1)').text();
        var billTypeCode = curRow.find('td:eq(2)').text();
        var partialReceivable = curRow.find('td:eq(3)').text();
        var insertUser = curRow.find('td:eq(5)').text();

        $('#billTypeNO').val(billTypeNO);
        $('#billTypeName').val(billTypeName);
        $('#billTypeCode').val(billTypeCode);
        $('#partialReceivable').val(partialReceivable).trigger('change');
        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
