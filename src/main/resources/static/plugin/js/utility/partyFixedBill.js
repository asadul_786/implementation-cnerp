$(document).ready(function () {

    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field!";
        valdiationMsgContent = "";
        if ($("#partyAgreementNo").val()=='-1') {

            valdiationMsgContent = "Party Agreement is required!";
            $("#partyAgreementNo").focus();
            return false;
        } else if($("#billTypeNo").val()=='-1') {
            valdiationMsgContent = "Bill Type is required!";
            $("#billTypeNo").focus();
            return false;
        }
        else if(isEmptyString($("#billAmount").val())) {
            valdiationMsgContent = "Bill Amount is required!";
            $("#billAmount").focus();
            return false;
        }else if(isEmptyString($("#applicableFrom").val())) {
            valdiationMsgContent = "Applicable From Date is required!";
            $("#applicableFrom").focus();
            return false;
        }else if(isEmptyString($("#applicableTo").val())) {
            valdiationMsgContent = "Applicable To Date is required!";
            $("#applicableTo").focus();
            return false;
        }else
         {
            return true;
        }

    }

    var table = $('#dataTable').DataTable({});

    $(document).on("click", "#save", function () {

        if(isValidated()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var partyFixedBills = {};
                        partyFixedBills.partyFixedBillNo = $("#key").val();
                        partyFixedBills.partyAgreementNo = $("#partyAgreementNo").val();
                        partyFixedBills.billTypeNo = $("#billTypeNo").val();
                        partyFixedBills.billAmount = $("#billAmount").val();
                        partyFixedBills.applicableFrom = $("#applicableFrom").val();
                        partyFixedBills.applicableTo = $("#applicableTo").val();





                        $.ajax({
                            contentType: 'application/json',
                            url:  "savePartyFixedBills",
                            type: 'POST',
                            data: JSON.stringify(partyFixedBills),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }else {
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }

    });



    $(document).on("click", "#edit", function () {



        var curRow = $(this).closest('tr');
        $("#key").val(curRow.find('td:eq(0)').text());
        $("#partyAgreementNo").val(curRow.find('td:eq(1)').text());
        $("#billTypeNo").val(curRow.find('td:eq(2)').text());
        $("#billAmount").val(curRow.find('td:eq(3)').text());
        $("#applicableFrom").val(curRow.find('td:eq(4)').text());
        $("#applicableTo").val(curRow.find('td:eq(5)').text());









        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });



});