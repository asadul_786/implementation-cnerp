function getUnit(item){

    var json = {
        "item": item

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/utility/getUnit/"
            + item,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#unitNo').empty();
            $.each(data, function(key, value) {

                $('#unitNo').append('<option value="-1"> Choose Option </option>');
                $('#unitNo').append('<option value="' + key + '">' + value + '</option>');

            });

        },
        error: function (e) {

        }
    });


}
function getPartyDetails(month){

    var item = $('#itemNo').val();
    var unit = $('#unitNo').val();
    var year = $('#billingYear').val();


    $.ajax({
        url: "/utility/party-details/" + item+"/"+unit+"/"+month+"/"+year,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var value_1 = "";
            var value_2 = "";
            var value_3 = "";


            $.each(response, function (i, l) {



                value_1 = l[0];
                value_2 = l[1];
                value_3 = l[2];






            });

            $("#partyNo").val(value_1);

            $("#partyName").val(value_2);
            $("#contactNo").val(value_3);





        },
        error: function (xhr, status, error) {


        }
    });



}


function getFixedBillDetails(billingTypeNo){

    var partyNo = $('#partyNo').val();
    var billingTypeNo = $('#billingTypeNo').val();
    var year = $('#billingYear').val();
    var month = $('#billingMonth').val();


    $.ajax({
        url: "/utility/fixed-bill-details/" + partyNo+"/"+billingTypeNo+"/"+month+"/"+year,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var value_1 = "";



            $.each(response, function (i, l) {



                value_1 = l[0];






            });

            $("#lastMonthRecord").val(value_1);






        },
        error: function (xhr, status, error) {
            alert(error)

        }
    });



}

$(document).ready(function () {

    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field!";
        valdiationMsgContent = "";
        if ($("#partyAgreementNo").val()=='-1') {

            valdiationMsgContent = "Party Agreement is required!";
            $("#partyAgreementNo").focus();
            return false;
        } else if($("#billTypeNo").val()=='-1') {
            valdiationMsgContent = "Bill Type is required!";
            $("#billTypeNo").focus();
            return false;
        }
        else if(isEmptyString($("#billAmount").val())) {
            valdiationMsgContent = "Bill Amount is required!";
            $("#billAmount").focus();
            return false;
        }else if(isEmptyString($("#applicableFrom").val())) {
            valdiationMsgContent = "Applicable From Date is required!";
            $("#applicableFrom").focus();
            return false;
        }else if(isEmptyString($("#applicableTo").val())) {
            valdiationMsgContent = "Applicable To Date is required!";
            $("#applicableTo").focus();
            return false;
        }else
        {
            return true;
        }

    }

    var table = $('#dataTable').DataTable({});

    $(document).on("click", "#save", function () {

        if(1){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var monthlyBill = {};
                        monthlyBill.partyFixedBillNo = $("#key").val();
                        monthlyBill.itemNo = $("#itemNo").val();
                        monthlyBill.unitNo = $("#unitNo").val();
                        monthlyBill.billingYear = $("#billingYear").val();
                        monthlyBill.billingMonth = $("#billingMonth").val();
                        monthlyBill.billingTypeNo = $("#billingTypeNo").val();
                        monthlyBill.lastMonthRecord = $("#lastMonthRecord").val();
                        monthlyBill.currentMonthRecord = $("#currentMonthRecord").val();
                        monthlyBill.billingAmount = $("#billingAmount").val();
                        monthlyBill.billGeneratedBy = $("#billGeneratedBy").val();
                        monthlyBill.generatedOn = $("#generatedOn").val();
                        if($("#isPaid").val()=='on')
                            monthlyBill.isPaid = 1;
                        else
                            monthlyBill.isPaid = 0;





                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveMonthlyBills",
                            type: 'POST',
                            data: JSON.stringify(monthlyBill),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Bill Invalidated");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }else {
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }

    });



    $(document).on("click", "#edit", function () {



        var curRow = $(this).closest('tr');

        var monthlyBill = {};
        monthlyBill.monthlyBillNo = curRow.find('td:eq(0)').text();
        monthlyBill.itemNo = curRow.find('td:eq(1)').text();
        monthlyBill.unitNo = curRow.find('td:eq(2)').text();
        monthlyBill.billingYear = curRow.find('td:eq(3)').text();
        monthlyBill.billingMonth = curRow.find('td:eq(4)').text();
        monthlyBill.billingTypeNo = curRow.find('td:eq(5)').text();
        monthlyBill.lastMonthRecord = curRow.find('td:eq(6)').text();
        monthlyBill.currentMonthRecord = curRow.find('td:eq(7)').text();
        monthlyBill.billingAmount = curRow.find('td:eq(8)').text();
        monthlyBill.billFileNum = curRow.find('td:eq(9)').text();
        monthlyBill.billGeneratedBy = curRow.find('td:eq(12)').text();
        monthlyBill.generatedOn = curRow.find('td:eq(13)').text();
        monthlyBill.isPaid = curRow.find('td:eq(14)').text();

        if(curRow.find('td:eq(14)').text())
        {
            showAlertByType('Bill Already Paid', 'F');
        }else{
            $.confirm({
                title: 'Invalidate',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {

                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveMonthlyBills",
                            type: 'POST',
                            data: JSON.stringify(monthlyBill),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Bill Invalidated");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }








        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });



});