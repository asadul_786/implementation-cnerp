/**
 * Created by md mithu sarker on 1/24/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();



    /*-------------- asset save function start ------------------------*/
    $("#Save").click(function () {
        saveData();
    });
    $("#Referesh").click(function () {
        clearForm();
    });

    function getFormData() {
        var utilityUnits = {};
        utilityUnits.utilityNo = $("#utilityNo").val();
        utilityUnits.itemNo = $("#itemNo").val();
        utilityUnits.utilityName = $("#utilityName").val();
        utilityUnits.utilityPosition = $("#utilityPosition").val();
        utilityUnits.remarkes = $("#remarkes").val();
        utilityUnits.insertUser = $('#insertUser').val();
        return utilityUnits;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/utility-ajax/save-utility-units";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 1500);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#utilityNo").val('');
        $("#itemNo").val('');
        $("#utilityName").val('');
        $("#utilityPosition").val('');
        $("#remarkes").val('');
        $("#insertUser").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#itemNo").val())) {

            valdiationMsgContent = "Item Name is required!";
            $("#itemNo").focus();
            return false;
        }
        else if (isEmptyString($("#utilityName").val())) {

            valdiationMsgContent = "Utility name is required!";
            $("#utilityName").focus();
            return false;
        }
        else if ($("#utilityName").val().length >100) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Utility name maximum 100 characters!!";
            $("#utilityName").focus();
            return false;
        }
        else if (isEmptyString($("#utilityPosition").val())) {

            valdiationMsgContent = "Utility position is required!";
            $("#utilityPosition").focus();
            return false;
        }
        else if ($("#utilityPosition").val().length >100) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Utility position maximum 100 characters!!";
            $("#utilityPosition").focus();
            return false;
        }
        else if (isEmptyString($("#remarkes").val())) {

            valdiationMsgContent = "Utility remarkes is required!";
            $("#remarkes").focus();
            return false;
        }
        else if ($("#remarkes").val().length >300) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Utility remarkes maximum 300 characters!!";
            $("#remarkes").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var utilityNo = curRow.find('td:eq(0)').text();
        var itemNo = curRow.find('td:eq(1)').text();
        var utilityName = curRow.find('td:eq(3)').text();
        var utilityPosition = curRow.find('td:eq(4)').text();
        var remarkes = curRow.find('td:eq(5)').text();
        var insertUser = curRow.find('td:eq(6)').text();

        $('#utilityNo').val(utilityNo);
        $('#itemNo').val(itemNo).trigger('change');
        $('#utilityName').val(utilityName);
        $('#utilityPosition').val(utilityPosition);
        $('#remarkes').val(remarkes);
        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
