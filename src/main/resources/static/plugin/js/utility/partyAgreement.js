$(document).ready(function () {
    //IMAGE
    $('#nominee_photo_view').attr("src", "/images/no-image.jpg");
    function readURLPhoto(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#nominee_photo_view').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#err_emp_photo_id").text("");
        }
    }
    $(document).on(
        "change",
        "#getImage",
        function (e) {
            if (this.files[0].size > 1048576) {//200KB
                $("#err_emp_photo_id").text(
                    "Photo size can't be greater than 1MB!");
            } else {

                $("#hasNewImage").val(true);
                readURLPhoto(this);
            }
        });
    $("#hasNewImage").val(false);

    function getPhotoExtension(photonm){
        if (isEmptyString(photonm)){
            return "pdf";
        }
        else{
            return photonm.split('.')[1];
        }
    }

    //VALIDATION
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field!";
        valdiationMsgContent = "";
        if ($("#partyTypeNo").val()=='-1') {

            valdiationMsgContent = "Party Type Name is required!";
            $("#partyTypeNo").focus();
            return false;
        }else if ($("#utlItemNo").val()=='-1') {

            valdiationMsgContent = "Item Type is required!";
            $("#utlItemNo").focus();
            return false;
        } else if ($("#utlUnitNo").val()=='-1') {

            valdiationMsgContent = "Unit Name is required!";
            $("#utlUnitNo").focus();
            return false;
        } else {
            return true;
        }

    }

    //DATATABLE
    var table = $('#dataTable').DataTable({});


    //SAVE
    $(document).on("click", "#save", function () {

        if(isValidated()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var driver = {};
                        driver.partyAgreementNo= $("#partyAgreementNo").val();
                        driver.partyTypeNo= $("#partyTypeNo").val();
                        driver.organizationName= $("#organizationName").val();
                        driver.name= $("#name").val();
                        driver.contactNumber= $("#contactNumber").val();
                        driver.permanentAddress= $("#permanentAddress").val();
                        driver.idType= $("#idType").val();
                        driver.idNumber= $("#idNumber").val();
                        driver.utlItemNo= $("#utlItemNo").val();
                        driver.utlUnitNo= $("#utlUnitNo").val();
                        driver.startDate= $("#startDate").val();
                        driver.endDate= $("#endDate").val();
                        driver.agreementDate= $("#agreementDate").val();
                        driver.monthlyRent= $("#monthlyRent").val();
                        driver.photoname = $("#photoname").val();
                        driver.photopath = $("#photopath").val();
                        var form = $('#SetupForm')[0];
                        var data = new FormData(form);
                        data.append("driverData", JSON.stringify(driver));

                        $.ajax({
                            type : "POST",
                            enctype : 'multipart/form-data',
                            url : "/utility/savePartyAgreement",
                            data : data,
                            processData : false,
                            contentType : false,
                            cache : false,
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }else {
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }

    });



    //EDIT
    $(document).on("click", "#edit", function () {



        var curRow = $(this).closest('tr');
        $("#key").val(curRow.find('td:eq(0)').text());
        $("#partyTypeNo").val(curRow.find('td:eq(1)').text());
        $("#organizationName").val(curRow.find('td:eq(2)').text());
        $("#name").val(curRow.find('td:eq(3)').text());
        $("#contactNumber").val(curRow.find('td:eq(4)').text());
        $("#permanentAddress").val(curRow.find('td:eq(5)').text());
        $("#idType").val(curRow.find('td:eq(6)').text());
        $("#idNumber").val(curRow.find('td:eq(7)').text());
        $("#utlItemNo").val(curRow.find('td:eq(8)').text());
        $("#utlUnitNo").val(curRow.find('td:eq(9)').text());
        $("#startDate").val(curRow.find('td:eq(10)').text());
        $("#endDate").val(curRow.find('td:eq(11)').text());
        $("#agreementDate").val(curRow.find('td:eq(12)').text());
        $("#monthlyRent").val(curRow.find('td:eq(13)').text());
        $("#agreementDetails").val(curRow.find('td:eq(14)').text());
        $("#agreementFile").val(curRow.find('td:eq(15)').text());







        $("#photoname").val(curRow.find('td:eq(16)').text());
        $("#photopath").val(curRow.find('td:eq(17)').text());

        $('#imagefiled').val(curRow.find('td:eq(16)').text());
        var photo64BitString = $(this).closest('tr').find('.nominee-photo').attr('nomineePhotoByte');
        console.log(photo64BitString);



        if (isEmptyString(photo64BitString)) {
            $("#hasNewImage").val(true);
            $("#nominee_photo_view").attr("src","/images/no-image.jpg");
        }
        else {
            $("#hasNewImage").val(false);
            $("#nominee_photo_view").attr("src", 'data:image/'+getPhotoExtension(curRow.find('td:eq(16)').text())+';base64,' + photo64BitString);
        }



        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    // $(document).on("click", "#clear_btn", function () {
    //     $("select").prop("disabled", false);
    //     $("select").val('-1');
    //     // clrErr();
    //     $("#save").text('Save');
    // });

    // function clrErr() {
    //
    //     $("#err_p_cd").text("");
    //     $("#err_p_status").text("");
    //     $("#err_appl_party").text("");
    //
    // }

    // $(document).on('input', '#surDiscFactForm', function(e){
    //     e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    // });

    // function validate() {
    //
    //     clrErr();
    //
    //     if($("#p_cd").val() == -1){
    //         $("#err_p_cd").text("Required !!");
    //         return;
    //     }
    //
    //     if($("#p_status").val() == -1){
    //         $("#err_p_status").text("Required !!");
    //         return;
    //     }
    //
    //     if($("#appl_party").val() == -1){
    //         $("#err_appl_party").text("Required !!");
    //         return;
    //     }
    //
    //     return true;
    //
    // }

    // $('#dataTable tbody').on('click', '#delete', function () {
    //     var curRow = $(this).closest('tr');
    //     var col1 = curRow.find('td:eq(0)').text();
    //
    //     $.confirm({
    //         title: 'Confirm',
    //         content: 'Selected record will be deleted.',
    //         buttons: {
    //             ok: function () {
    //                 $.ajax({
    //                     contentType: 'application/json',
    //                     url: "/vehicle/vehVehicleDelete/" + col1,
    //                     type: 'POST',
    //                     //async: false,
    //                     //data: JSON.stringify(answerDto),
    //                     dataType: 'json',
    //                     success: function (response) {
    //
    //                         if (response === true) {
    //                             table.row(curRow).remove().draw(false);
    //                             showAlert("Deleted Successfully");
    //                         } else {
    //                             showAlert("Unknown error");
    //                         }
    //
    //                     },
    //                     error: function (xhr, status, error) {
    //                         showAlert("Unknown error");
    //                     }
    //                 });
    //             },
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    //
    // });

});