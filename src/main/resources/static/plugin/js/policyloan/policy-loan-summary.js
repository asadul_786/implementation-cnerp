$(document).ready(function () {
    $("#Report").click(function () {

        var startDate = $('#startDate').val();
        var endDate = $('#endDate').val();
        var office = $('#office').val().trim();
        var flag=1;

        $(".err_mgs").text("");

        if (!startDate){
            $("#err_"+$('#startDate').attr("name")).text("This Field is Required!");
            flag = 0;
        }

        if (!endDate){
            $("#err_"+$('#endDate').attr("name")).text("This Field is Required!");
            flag = 0;
        }

        if (!flag){
            return false;
        }

        var opera1 = startDate.split('/');
        var opera2 = endDate.split('/');

        if(new Date(opera1[2], opera1[0] - 1, opera1[1]) > new Date(opera2[2], opera2[0] - 1, opera2[1])) {
            showAlert("Start Date Must Be Less Than End Date!");
            return false;
        }

        if (!office){
            office = null;
        }

        if (flag){
            window.open('/insLoan/policy-loan-summary-report?date='+startDate+','+endDate+','+office, '_blank')
        }

    });
});
