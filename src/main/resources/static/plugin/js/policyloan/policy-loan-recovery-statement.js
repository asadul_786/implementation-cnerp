$(document).ready(function () {
    $("#Report").click(function () {

        var startDate = $('#startDate').val();
        var endDate = $('#endDate').val();
        var office = $('#office').val().trim();
        var recoveryFrom = $('#recoveryFrom').val();
        var category = $('#category').val();
        var c_type = $('#c_type').val();
        var flag = 1;

        $(".err_mgs").text("");

        if (!startDate){
            $("#err_"+$('#startDate').attr("name")).text("This Field is Required!");
            flag = 0;
        }

        if (!endDate){
            $("#err_"+$('#endDate').attr("name")).text("This Field is Required!");
            flag = 0;
        }

        if (!flag){
            return false;
        }

        var opera1 = startDate.split('/');
        var opera2 = endDate.split('/');

        if(new Date(opera1[2], opera1[0] - 1, opera1[1]) > new Date(opera2[2], opera2[0] - 1, opera2[1])) {
            showAlert("Start Date Must Be Less Than End Date!");
            return false;
        }

        if (!office){
            office = null;
        }

        if(recoveryFrom != null){
            if (recoveryFrom === 'Loan Collection'){recoveryFrom=1;}
            else if (recoveryFrom === 'Loan Settlement'){recoveryFrom=2;}
            else if (recoveryFrom === 'Surrender'){recoveryFrom=3;}
            else if (recoveryFrom === 'Claim'){recoveryFrom=4;}
        }

        if(category != null){
            if (category === 'Online'){category=1;}
            else if (category === 'Offline'){category=2;}
        }

        if(c_type != null){
            if (c_type === 'Cash'){c_type=1;}
            else if (c_type === 'Cheque'){c_type=2;}
            else if (c_type === 'Others'){c_type=3;}
        }

        if (flag){
            window.open('/insLoan/policy-loan-recovery-report?details='+startDate+','+endDate+','+office+','+recoveryFrom+','+category+','+c_type, '_blank')
        }


    });
});
