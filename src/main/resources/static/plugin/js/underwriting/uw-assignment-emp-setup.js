$(document).ready(function () {

    $('.cust-disabled').attr('disabled','disabled');

    var table = $('#empTableId').DataTable({
        "columnDefs": [
            { "targets": [0], "searchable": true }
        ]
    });

    $(document).on("input", "#uwcode", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');



        var getUwcode = $('#uwcode').val();

        //alert(getUwcode);
        if (getUwcode != "") {
            $.ajax({
                type: "GET",
                // contentType: "application/json",
                url: "/underwriting/getUnderWriterEmpCode/" + getUwcode,
                dataType: 'json',
                //data: JSON.stringify(json),
                cache: false,
                timeout: 600000,
                success: function (data) {
                    //  alert(JSON.stringify(json));
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    var value_6 = "";
                    // var value_5 = "";
                    $.each(data, function (i, l) {
                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        value_6 = l[5];
                        // value_5 = l[4];
                    });
                    //$("#uwcode").val(value_1);
                    $("#sumassured").val(value_3);
                    $("#empId2").val(value_5);
                    $("#empName").val(value_6);
                },
                error: function (e) {

                    showAlert("Something wrong!!!");
                }
            });

        }else{

        }

    });

    $('#dataTable tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();
        var col7 = curRow.find('td:eq(6)').text();


        $('#uwcode').val(col1);
        $('#sumassured').val(col3);
        $('#empId2').val(col5);
        $('#empName').val(col6);
        //scroll up

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });
    //
    // $('#empId').on('change', function() {
    //
    //     var getEmpId = $('#empId').val();
    //     // var getAssuranceType = $('#assuranceType').val();
    //     var json = {
    //         "getAgentId": getEmpId ,
    //         "getAssuranceType":""
    //     };
    //
    //     if (getEmpId != "") {
    //         $.ajax({
    //             type: "POST",
    //             contentType: "application/json",
    //             url: "/underwriting/getUnderWriterEmpCode/" + getEmpId,
    //             //+ getAgentId + getAssuranceType,
    //             data: JSON.stringify(json),
    //             dataType: 'json',
    //             cache: false,
    //             timeout: 600000,
    //             success: function (data) {
    //                //  alert(data);
    //                 var value_1 = "";
    //                 var value_2 = "";
    //                 var value_3 = "";
    //                 var value_4 = "";
    //                 var value_5 = "";
    //                 // var value_5 = "";
    //                 $.each(data, function (i, l) {
    //                     value_1 = l[0];
    //                     value_2 = l[1];
    //                     value_3 = l[2];
    //                     value_4 = l[3];
    //                     value_5 = l[4];
    //                     // value_5 = l[4];
    //                 });
    //                 $("#empId2").val(value_2);
    //                 $("#empName").val(value_3);
    //                 $("#empDesign").val(value_4);
    //                 $("#empStatus").val(value_5);
    //             },
    //             error: function (e) {
    //
    //                 showAlert("Something wrong!!!");
    //             }
    //         });
    //
    //     }else {
    //
    //     }
    //
    // });

    $('#offCode').on('change', function() {

        var getOfficeId = $('#offCode').val();
        // var getAssuranceType = $('#assuranceType').val();
        var json = {
            "getOfficeId": getOfficeId ,
            "getAssuranceType":""
        };
        if (getOfficeId != "") {
            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/underwriting/getUnderWriterOffCode/" + getOfficeId,
                //+ getAgentId + getAssuranceType,
                data: JSON.stringify(json),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (data) {
                    //alert(data);
                    var value_1 = "";
                    var value_2 = "";
                    //var value_3 = "";
                    // var value_4 = "";
                    // var value_5 = "";
                    // var value_5 = "";
                    $.each(data, function (i, l) {
                        value_1 = l[0];
                        value_2 = l[1];
                        //value_3 = l[2];
                        // value_4 = l[3];
                        // value_5 = l[4];
                        // value_5 = l[4];
                    });
                  //   alert(value_1);
                   //  alert(value_2);

                    $("#offCode").val(value_1);
                    $("#offName").val(value_2);
                    //$("#empDesign").val(value_4);
                    //$("#empStatus").val(value_5);
                },
                error: function (e) {
                    showAlert("Something wrong!!!");
                }
            });
        }else{

        }

    });

//            $("#btn_uwassign").click(function (event) {
//                event.preventDefault();
//                var isVaild = validationProposalInfo();
//                if(isVaild) {
 //                  confirmProposalInfoDialog("Are you sure to save UnderWriting Assignment setup Info?");
//                }
//            });
//
//     $("#btn_uwassignOff").click(function (event) {
//         event.preventDefault();
//         var isVaild = validationEmpInfo();
//         if(isVaild) {
//             confirmEmpInfoDialog("Are you sure to save Assign Employee ?");
//         }
//     });

    $("#btn_uwassignOff").click(function (event) {
        event.preventDefault();
        var isVaild = validationOfficeInfo();
        if(isVaild) {
            confirmOfficeInfoDialog("Are you sure to save Assign Office ?");
        }
    });


    var confirmOfficeInfoDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        var uwAssigOffice = {};

                        uwAssigOffice.uwcode = $('#uwcode').val();
                        uwAssigOffice.offCode = $('#offCode').val();
                        //uwAssigOffice.offName =  $('#offName').val();
                        //uwAssigEmp.writingstatus = $('#empDesign').val();
                        //uwAssigEmp.writingstatus = $('#empStatus').val();

                        // alert(uwAssigOffice.uwcode);
                         // alert(uwAssigOffice.offCode);
                          //alert(uwAssigOffice.offName);

                        $.ajax({
                            contentType: 'application/json',
                            url: "/underwriting/addAssignEmpOffice",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(uwAssigOffice),
                            dataType: 'json',
                            success: function (response) {

                                //alert(response)
                                //console.log(proposal);
                                //console.log("here client family");
                                // updateClientFamilyTable();
                                //updateTableList();
                                resetOfficeForm();
                                showAlert("Successfully saved");

                                // $( "#refreshOff" ).trigger( "click" );
                                //  $('#btn_uwassign').attr('disabled','disabled');

                            },
                            error: function (xhr, status, error) {
                                showAlert("Something Wrong!!!");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };


    var confirmEmpInfoDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        var uwAssigEmp = {};

                        uwAssigEmp.uwcode = $('#uwcode').val();
                        uwAssigEmp.empId2 = $('#empId2').val();
                        uwAssigEmp.empName =  $('#empName').val();
                        uwAssigEmp.empStatus = $('#empStatus').val();
                        //uwAssigEmp.writingstatus = $('#empDesign').val();



                        //alert(uwAssigEmp.uwcode);
                       // alert(uwAssigEmp.empId2);
                       // alert(uwAssigEmp.empName);
                        //alert(uwAssigEmp.empStatus);

                        $.ajax({
                            contentType: 'application/json',
                            url: "/underwriting/addAssignEmp",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(uwAssigEmp),
                            dataType: 'json',
                            success: function (response) {

                                //alert(response)
                                //console.log(proposal);
                                //console.log("here client family");
                                // updateClientFamilyTable();
                                //updateTableList();
                                clearformEmp();
                                showAlert("Successfully saved");
                                $('#divUWOfficeinfo').css("display", "block");
                                //  $( "#refreshEmp" ).trigger( "click" );
                                // $('#btn_uwassign').attr('disabled','disabled');

                            },
                            error: function (xhr, status, error) {
                                showAlert("Something Wrong!!!");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };


    // var confirmProposalInfoDialog = function (text) {
    //     $.confirm({
    //         title: 'Confirm!',
    //         content: text,
    //         buttons: {
    //             confirm: {
    //                 btnClass: 'btn-info',
    //                 keys: ['enter'],
    //                 action: function () {
    //
    //                     var uwAssignment = {};
    //
    //                     uwAssignment.uwcode = $('#uwcode').val();
    //                     uwAssignment.officecode =  $('#officecode').val();
    //                     uwAssignment.writingstatus = $('#writingstatus').val();
    //
    //                     $.ajax({
    //                         contentType: 'application/json',
    //                         url: "/underwriting/addAssignmentUw",
    //                         type: 'POST',
    //                         async: false,
    //                         data: JSON.stringify(uwAssignment),
    //                         dataType: 'json',
    //                         success: function (response) {
    //
    //                             showAlert("Successfully saved");
    //                             //$('#divUWEmpInfo').css("display", "block");
    //                             // $('#divUWOfficeinfo').css("display", "block");
    //                             //$('#btn_uwassign').attr('disabled','disabled');
    //                             //$('.cust-disabled').attr('disabled','disabled');
    //
    //
    //                         },
    //                         error: function (xhr, status, error) {
    //                             showAlert("Something Wrong!!!");
    //                         }
    //                     });
    //                 }
    //
    //             },
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    // };

    $("#refresh").click(function () {
        clearform();
    });

    $("#refreshEmp").click(function () {
        clearformEmp();
    });

    $("#refreshOff").click(function () {
        resetOfficeForm();
    });

    function resetEmpForm(){
        $('#empId').val("-1");
        $('#empName').val("");
        $('#empDesign').val("");
        $('#empStatus').val("");
    }

    function clearformEmp() {
        $('#empId').val("-1");
        $('#empId2').val("");
        $('#empId').selectpicker('refresh');
        $('#empId2').selectpicker('refresh');
        $('#empName').val("");
        $('#empDesign').val("");
        $('#empStatus').val("");
       // $('#uwcode').val("");

        $('#officeserviceSpan').text("");


        // $('#divUWEmpInfo').css("display", "none");
        //   $( "#refreshEmp" ).trigger( "click" );
        // $('#btn_uwassign').attr('disabled','disabled');

    }

    function resetOfficeForm() {
        $('#offCode').val("-1");
        $('#offName').val("");
        $('#offCode').selectpicker('refresh');
        // $('#divUWEmpInfo').css("display", "none");
        //   $( "#refreshEmp" ).trigger( "click" );
        // $('#btn_uwassign').attr('disabled','disabled');
    }

    function clearform() {
        $('#uwcode').val("-1");
        $('#officecode').val("-1");
        $('#strength').val("");
        $('#sumassured').val("");
        $('#writingstatus').val("");
        $('#acceptancecode').val("");

        // $('#divUWEmpInfo').css("display", "none");
        // $('#divUWOfficeinfo').css("display", "none");
        // $( "#refreshEmp" ).trigger( "click" );
        //  $('#btn_uwassign').removeAttr('disabled');
        //   $('.cust-disabled').removeAttr('disabled');

    }

    function validationOfficeInfo(){

        var status= true;

        if ($("#offCode").val() == "-1"){
            status = false;
            $("#err_offCode").text("Required field found!!");
            $("#offCode").focus();
        } else $("#err_offCode").text("");
        if ($("#uwcode").val() == "") {
            status = false;
            $("#officeserviceSpan").text("Required field found !!!");
            $("#uwcode").focus();
        } else $("#officeserviceSpan").text("");


        return status;
    }

    function validationEmpInfo(){

        var status = true;
        var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;

        if ($("#uwcode").val() == "") {
            status = false;
            $("#officeserviceSpan").text("Required field found !!!");
            $("#uwcode").focus();
        } else $("#officeserviceSpan").text("");

        if ($("#empId").val() == "-1") {
            status = false;
            $("#err_empId").text("Empty field found!!");
            $("#uwcode").focus();
        } else $("#err_empId").text("");

        if ($("#empName").val() == "") {
            status = false;
            $("#err_empName").text("Empty field found!!");
            $("#empName").focus();
        } else $("#err_empName").text("");

        if ($("#empDesign").val() == "") {
            status = false;
            $("#err_empDesign").text("Empty field found!!");
            $("#empDesign").focus();
        } else $("#err_empDesign").text("");

        if ($("#empStatus").val() == "") {
            status = false;
            $("#err_empStatus").text("Empty field found!!");
            $("#empStatus").focus();
        } else $("#err_empStatus").text("");

        return status;
    }

    function validationProposalInfo(){
        var status = true;
        var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;

        if ($("#uwcode").val() == "-1") {
            status = false;
            $("#officeserviceSpan").text("Empty field found!!");
            $("#uwcode").focus();
        } else $("#officeserviceSpan").text("");

        if ($("#officecode").val() == "-1") {
            status = false;
            $("#offCodeSpan").text("Empty field found!!");
            $("#officecode").focus();
        } else $("#offCodeSpan").text("");

        if ($("#strength").val() == "") {
            status = false;
            $("#strengthSpan").text("Empty field found!!");
            $("#strength").focus();
        } else $("#strengthSpan").text("");

        if ($("#sumassured").val() == "") {
            status = false;
            $("#sumassuredSpan").text("Empty field found!!");
            $("#sumassured").focus();
        } else $("#sumassuredSpan").text("");

        if ($("#writingstatus").val() == "") {
            status = false;
            $("#err_writingstatus").text("Empty field found!!");
            $("#writingstatus").focus();
        } else $("#err_writingstatus").text("");

        if ($("#acceptancecode").val() == "") {
            status = false;
            $("#err_acceptancecode").text("Empty field found!!");
            $("#acceptancecode").focus();
        } else $("#err_acceptancecode").text("");

        return status;
    }

//            function updateTableList() {
//
//                var empTable = $('#empTableId');
//
//                $.ajax({
//                    type: "GET",
//                    url: "/underwriting/addEmpUnList",
//                    success: function (data) {
//                        var no = 1;
//                        var tableBody = "";
//                        $.each(data, function (idx, elem) {
//                            var action =
//                                '<button class="btn btn-success empInformationEdit" value="Edit" id="editEmp"> ' +
//                                '<i class="fa fa-pencil"></i></button> ';
//                            tableBody += "<tr'>";
//                            tableBody += "<td>" + elem[0] + "</td>";
//                            tableBody += "<td>" + elem[1] + "</td>";
//                            tableBody += "<td>" + elem[2] + "</td>";
//                            tableBody += "<td>" + elem[3] + "</td>";
//                            tableBody += "<td>" + elem[4] + "</td>";
//                            tableBody += "<td>" + action + "</td>"
//                            tableBody += "<tr>";
//                        });
//                        empTable.append(tableBody);
//                    }
//                });
//            }

});
