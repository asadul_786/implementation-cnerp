

$(document).ready(function () {

    var table = $('#dataTable').DataTable({
        "columnDefs": [
            { "targets": [0], "searchable": false }
        ]
    });

    $('#setupUw').submit(function (e) {

        e.preventDefault();
        var typeuw = $('#typeunderwriting').val();
        var typsts = $('#writingstatus').val();
        var sumass = $('#sumassured').val();
        var agelmt = $('#agelimit').val();
        //  alert("hi...");

        // $(".error").remove();
        //alert(typeuw);

        if (typeuw == -1) {
            // $('#err_writingstatus').after('<span class="error">Required field found</span>');
            $('#err_typeunderwriting').text("Required field found !! ");
        }else if (typsts == -1){
            $('#err_writingstatus').text("Required field found !! ");
        }else if (sumass == ""){
            $('#err_sumassured').text("Required field found !! ");
        }else if (agelmt == ""){
            $('#err_agelimit').text("Required field found !! ");
        }else {
            $('#setupUw').get(0).submit();
        }

    });

    $('#refresh').click(function () {
        //$('.error').text('');
        $('#err_typeunderwriting').text('');
        $('#err_writingstatus').text('');
        $('#err_sumassured').text('');
        $('#err_agelimit').text('');
    });


    //  $('uw_setupSave').attr('disabled',false)

    $('#dataTable tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();



        $('#code').val(col1);
        $('#typeunderwriting').val(col2);
        $('#strength').val(col3);
        $('#noacceptance').val(col4);
        $('#sumassured').val(col5);
        $('#agelimit').val(col6);
        $('#writingstatus').val(col7);
        $('#remark').val(col8);


        //scroll up

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    if(saveNotification != undefined)
        showAlert(saveNotification);
});

