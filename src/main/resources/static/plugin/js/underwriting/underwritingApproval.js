
$(document).ready(function () {
     //alert("hi");
    $('#unCodeApproval').submit(function (e) {
        e.preventDefault();
        var uwcode = $('#uwcode').val();
        var uwStrength = $('#uwStrength').val();
        var sumAssured = $('#sumAssured').val();
        var commDt = $('#commDt').val();
        var statusType = $('#statusType').val();
        var toDt = $('#toDt').val();

        $(".error").remove();

        if (uwcode.length == "") {
            $('#uwcodeSpan').text("Empty field found");
        } else {
            $('#unCodeApproval').get(0).submit();
        }
    });


    $(document).on("input", "#uwcode", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var uwcode = $('#uwcode').val();
        //$('#updateempgid').val("");


        if (uwcode != "") {
            $.ajax({
                url: "/underwriting/getUderWritingDetail/" + uwcode,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";

                    $.each(response, function (i, l) {
                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];


                    });
                    $("#uwStrength").val(value_2);
                    $("#sumAssured").val(value_3);
                   // $("#Designation").val(value_4);
                    //$("#OFFICE_NAME").val(value_5);
                   $("#err_uwcode").text("");
                },
                error: function (xhr, status, error) {
                    $("#uwStrength").val("");
                    $("#sumAssured").val("");
                    //$("#Designation").val("");
                    //$("#OFFICE_NAME").val("");
                    $("#err_uwcode").text("Sorry! UW Code not match!!");
                }
            });

        }
        else {

            if ($('#uwcode').val() == "") {
                $("#err_uwcode").text("UW Code required!");
                $("#uwStrength").val("");
                $("#sumAssured").val("");
            }
            else {
                $("#err_uwcode").text("Code maximum 4 characters!");
            }
        }

    });


});

