$(document).ready(function () {


    var table = $('#dataTable').DataTable({
        "columnDefs": [
            { "targets": [0], "searchable": false }
        ]
    });

    $('#commIdForm').submit(function (e) {

        e.preventDefault();
        var comId = $('#comments').val();
        //  var typsts = $('#writingstatus').val();
        //  var sumass = $('#sumassured').val();
        // var agelmt = $('#agelimit').val();
        //  alert("hi...");

        // $(".error").remove();
        //alert(typeuw);

        if (comId == "") {
            // $('#err_writingstatus').after('<span class="error">Required field found</span>');
            $('#err_comments').text("Required field found !! ");
        } else {
            $('#commIdForm').get(0).submit();
        }

    });

    $('#refresh').click(function () {
        //$('.error').text('');
        $('#err_comments').text('');
    });


    //  $('saveUwCom').attr('disabled',false)

    $('#dataTable tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();


        $('#code').val(col1);
        $('#comments').val(col2);
        $('#status').val(col3);
        $('#I_USR').val(col4);
        $('#idate').val(col5);
        //scroll up

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    if(saveNotification != undefined)
        showAlert(saveNotification);
});
