/**
 * Created by Mithun on 3/2/2020.
 */

//Custom message Simple Dialog


//String Manipulation Function

$(document).ready(function () {

    // customAlert(alertTypes.SUCCESS,'','');
    $('#btnGenerate').click(function () {

        var radioVal = $("input[name='rdbtn-process-date']:checked").attr('p-proc-tp');
        if (isEmptyString(radioVal)) {
            customAlert(alertTypes.WARNING, "Required field", "How to process the schedule, is first schedule or duplicate. Choose first!")
        }
        else if (radioVal != "0" && radioVal != "1") {
            customAlert(alertTypes.WARNING, "Required field", "Seems the value of radio button has being temped, reload the page again, press ctrl+F5! ")

        } else if (radioVal == "1" && isEmptyString($('.policy-no').val())) {
            customAlert(alertTypes.WARNING, "Required field", "Policy number is required!")
        } else if (radioVal == "0" && isEmptyString($('.dp-schedule-date').val())) {
            customAlert(alertTypes.WARNING, "Required field", "Schedule Date  is required!")
        }

        else {

            var policyScheduleDateAssignDto = {};
            policyScheduleDateAssignDto.policyNumber = $('.policy-no').val();
            policyScheduleDateAssignDto.scheduleDate = new Date(isEmptyString($('.dp-schedule-date').val() ? null : $('.dp-schedule-date').val()));
            policyScheduleDateAssignDto.isDuplicateSchedule = radioVal;

            const url = "/new-business/policy-document" + "/" + "process-policy-sch-date";
            var policyDocDateCreUrl = 0;
            $.ajax({
                url: policyDocDateCreUrl,
                type: 'POST',
                data: JSON.stringify(policyScheduleDateAssignDto),
                contentType: 'application/json',
                async: false,
                success: function (commonMsg) {
                    // console.log(commonMsg);
                    if (commonMsg.isSucceed) {
                        customAlert(alertTypes.SUCCESS, 'Success[' + commonMsg.msgCode + ']', 'Generated Successfully.');
                        $("#btnReset").trigger("click");
                    }
                    else {
                        customAlert(alertTypes.FAILED, 'Unsuccessful[' + commonMsg.msgCode + ']', commonMsg.msg + '[' + commonMsg.remarks + ']');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    customAlert(alertTypes.ERROR, 'Error', 'Something went wrong[xhr], Contact with IT team.');
                }

            });
        }
    });

    $('#btnReset').click(function () {
        $("input[name='rdbtn-process-date']").prop("checked", false);
        $('.policy-no').val("");
        $('.dp-schedule-date').val("");

    });
    // $('#policyNo').oncut = $('#policyNo').oncopy = $('#policyNo').onpaste =$('#policyNo').onchange= function(event) {
    //check policy number
    $('#policyNo').focusout(function () {
        const policyDocPolicyNoChkUrl = "/new-business/policy-document" + "/" + 'policy-number-validity-chk/' + $('#policyNo').val();
        $.ajax({
            url: policyDocPolicyNoChkUrl,
            contentType: 'application/json',
            dataType: 'json',
            type: 'GET',
            success: function (commonMsg) {
                // console.log(commonMsg);
                if (commonMsg.isSucceed) {
                    // customAlert(alertTypes.SUCCESS, 'Success['+commonMsg.msgCode+']', 'Generated Successfully.');
                    //$( "#btnReset" ).trigger( "click" );
                    $('#btnGenerate').removeAttr('disabled');
                    return true;
                }
                else {
                    customAlert(alertTypes.FAILED, 'Unsuccessful[' + commonMsg.msgCode + ']', commonMsg.msg );
                    //focus
                    //$('#policyNo').focus();
                    $('#btnGenerate').attr('disabled','disabled');
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                customAlert(alertTypes.ERROR, 'Error', 'Something went wrong[xhr], Contact with IT team.'+jqXhr);
               // $('#policyNo').focus();
            }
        });


    });
});
