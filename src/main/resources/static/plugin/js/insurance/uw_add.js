/**
 * Created by User on 05-May-19.
 */
$(document).ready(function () {
    $('#uwSetTable').DataTable();
    var model = {};
    $(document).on("click", "#uw_add", function (e) {

       // var officeCat = $('#officeCatId option:selected').val();

        model.officeCatId = $('#officeCatId option:selected').val();

        var bothDate = $("#reservation").val();
        var split = bothDate.split(" - ");
        var fromDate = split[0];
        var toDate = split[1];
        console.log(fromDate);
        console.log(toDate);
        model.fromDate = fromDate;
        model.toDate = toDate;

        model.maxLimit = $( "#maxLimit").val();
        model.departmentId = $('#departmentId option:selected').val();
        model.designationId = $('#designationId option:selected').val();
        model.policyTp = $("input[name='policyTp']:checked").val();
        model.lifeStd = $("input[name='lifeStd']:checked").val();

//            model.comm_date = $( "#commDate").val();
//            model.proposal_date = $( "#proposalDate").val();
//            model.agent_id = $( "#agentId").val();
//            model.org_setup = $( "#orgSetup").val();
//            model.service_region = $( "#serviceRegion").val();
//            model.policy_category = $( "#policyCategory").val();
//            proposal_master.propMasterId  = $("#proposalMasterNo").val();
//            proposal_master.policy_type = $("input[name='policyType']:checked").val();



            $.confirm({
                title: 'Confirm!',
                content: "Are you sure to save the data",
                buttons: {
                    confirm: {
                        btnClass: 'btn-info',
                        keys: ['enter'],
                        action: function () {
                            $.ajax({
                                contentType: 'application/json',
                                url:  "add-underwriter-set",
                                type: 'POST',
                                async: false,
                                data: JSON.stringify(model),
                                dataType: 'json',
                                success: function(response) {
                                    showAlert("okay");
                                    location.reload();
                                },
                                error: function(xhr, status, error) {
                                    alert("Something went wrong!");
                                }
                            });
                        }

                    },
                    cancel: function () {

                    }
                }
            });



    });

    $(document).on("click", ".delete_uw", function (e) {
          var id = $(this).attr("id");
          var str = '#' + id;

          $.confirm({
                  title: 'Confirm!',
                  content: "Are you sure to delete the data",
                  buttons: {
                      confirm: {
                          btnClass: 'btn-info',
                          keys: ['enter'],
                          action: function () {

                              $.ajax({
                                  contentType: 'application/json',
                                  url:  "underwriter-set/delete-uw?id=" + id,
                                  type: 'DELETE',
                                  async: false,
                                  success: function(response) {
                                      showAlert('Successfully Deleted!');
                                     location.reload();
                                  },
                                  error: function(xhr, status, error) {
                                      alert("Something went wrong!");
                                  }
                              });
                          }
                      },
                      cancel: function () {
                      }
                  }
              });

      });

});