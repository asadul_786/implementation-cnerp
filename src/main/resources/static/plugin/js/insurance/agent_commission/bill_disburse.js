/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#bill_disburse_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "dummyCall",
            "type": "GET",
            "data": function ( d ) {
                d.officeCD = $("#office_cd").val(),
                    d.billNoFrom = $.trim($("#bill_no_from").val()),
                    d.billNoTo = $.trim($("#bill_no_to").val()),
                    d.billDtFrom = $.trim($("#bill_dt_from").val()),
                    d.billDtTo = $.trim($("#bill_dt_to").val())
            },
            "dataType": "json"
        },
        "initComplete": function(settings, json) {
            $("#bill_disburse_table_processing").css({"top": "5px", "color": "green"});
        },
        "autoWidth": true,
        "columns": [
            { "data": "agentId", "name": "AGENT_ID" },
            { "data": "agentNM", "name": "AGENT_NM_ENG" },
            // { "data": "officeNM", "name": "OFFICE_NAME" },
            { "data": "billNo", "name": "BILL_NO" },
            { "data": "billDt", "name": "BILL_DATE" },
            { "data": "chqNo", "name": "CHEQUE_NO" },
            { "data": "comAmt", "name": "COM_PAID_AMT" },
            { "data": "bonusAmt", "name": "BONUS_PAID_AMT" },
            { "data": "pbcAmt", "name": "PBC_PAID_AMT" },
            { "data": "totalAmt", "name": "TOT_PAID_AMOUNT" },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<input type="checkbox" id="chk">';
                }
            }
        ]
    });

    $(document).on('input', '#bill_no_from', function(e){
        e.target.value = e.target.value.replace(/[^0-9, /]/g,'');
    });

    $(document).on('input', '#bill_no_to', function(e){
        e.target.value = e.target.value.replace(/[^0-9, /]/g,'');
    });

    $(document).on("change", "#bill_dt_from", function (e) {
        fromDt = $.trim($('#bill_dt_from').val());
        if(fromDt != ""){
            splitDt = fromDt.split("/");
            actFromDt = new Date(splitDt[2] + "-" + splitDt[1] + "-" + splitDt[0]);
            actToDt = actFromDt;
            actToDt.setMonth(actToDt.getMonth() + 2);
            $('#bill_dt_to').val(actToDt.getDate() + "/" + (actToDt.getMonth() + 1) + "/" + actToDt.getFullYear());
        }
    });

    $(document).on("click", "#processDisburse", function () {

        //console.log(disTransLst);
        if($("#bank_cd").val() == -1){
            $("#err_bank_cd").text("Required !!");
            return;
        }
        else {
            $("#err_bank_cd").text("");
        }

        if($("#br_cd").val() == -1){
            $("#err_br_cd").text("Required !!");
            return;
        }
        else {
            $("#err_br_cd").text("");
        }

        if($("#acc_no").val() == -1){
            $("#err_acc_no").text("Required !!");
            return;
        }
        else {
            $("#err_acc_no").text("");
        }

        mBillTransLst = [];
        mRowDltLst = [];

        for (var name in billTransLst) {
            mBillTransLst.push(name);
            mRowDltLst.push(billTransLst[name]);
        }

        if(mBillTransLst.length > 0){

            //console.log(mDisTransLst);
            //console.log(mRowDltLst);

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {

                        $.ajax({
                            contentType: 'application/json',
                            url:  "billTrans/" + $("#bank_cd").val() + "/" + $("#br_cd").val() + "/" + $("#acc_no").val(),
                            type: 'POST',
                            data: JSON.stringify(mBillTransLst),
                            //dataType: 'json',
                            success: function(response) {
                                showAlert(response + " bill disbursed successfully");
                                billTransLst = [];
                                mRowDltLst.forEach(function (rowJ) {
                                    table.row(rowJ).remove();
                                });
                                table.draw();
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }
        else
            showAlert("No Application selected !!");

    });

    $(document).on("click", "#search_bill_lst", function () {
        if(validate()){
            $("#err_office_cd").text("");
            //table.ajax.reload();
            table.ajax.url("billList").load();
            //$("#status").val() == 1 ? $("#processDisburse").prop("disabled", true) : $("#processDisburse").prop("disabled", false);
        }
    });

    var billTransLst = [];

    $("#bill_disburse_table tbody").on("click", "#chk", function () {

        var curRow = $(this).closest('tr');

        billTransLst[$.trim(curRow.find('td:eq(2)').text())] == undefined ?
            billTransLst[$.trim(curRow.find('td:eq(2)').text())] = curRow :
            delete billTransLst[$.trim(curRow.find('td:eq(2)').text())];

        //console.log(disTransLst);
        //alert('Row index: ' + $(this).closest('tr').index());
    });

    $(document).on("click", "#clear_btn", function () {
        $('#agentBillDisburseForm').trigger("reset");
        $('#office_cd').val(-1).select2().trigger('change');
        $("#err_office_cd").text("");
        $("#err_bank_cd").text("");
        $("#err_br_cd").text("");
        $("#err_acc_no").text("");
        //$("#processDisburse").prop("disabled", false);
        //table.ajax.reload();
        //table.ajax.url("").load();
    });

    $(document).on("change", "#bank_cd", function () {

        var bankCD = $("#bank_cd").val();

        if(bankCD != -1){

            $.get("getBranchByBank?bankCD=" + bankCD,

                function (data, status) {

                    var brDrpDwn = $('#br_cd');
                    brDrpDwn.empty();
                    brDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Branch --"
                    }));

                    $.each(data, function (index, branch) {
                        brDrpDwn.append($('<option/>', {
                            value: branch[0],
                            text: branch[1]
                        }));
                    });

                    $('#br_cd').prop("disabled", false);

                });
        }
        else{
            $('#br_cd').empty();
            $('#br_cd').append($('<option/>', {
                value: "-1",
                text: "-- Select Branch --"
            }));
            $('#br_cd').prop("disabled", true);
        }
    });

    $(document).on("change", "#br_cd", function () {

        var brCD = $("#br_cd").val();

        if(brCD != -1){

            $.get("getAccByBranch?branchCD=" + brCD,

                function (data, status) {

                    var accDrpDwn = $('#acc_no');
                    accDrpDwn.empty();
                    accDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Account --"
                    }));

                    $.each(data, function (index, acc) {
                        accDrpDwn.append($('<option/>', {
                            value: acc,
                            text: acc
                        }));
                    });

                    $('#acc_no').prop("disabled", false);

                });
        }
        else{
            $('#acc_no').empty();
            $('#acc_no').append($('<option/>', {
                value: "-1",
                text: "-- Select Account --"
            }));
            $('#acc_no').prop("disabled", true);
        }
    });

    function validate() {

        if($("#office_cd").val() == "-1"){
            $("#err_office_cd").text("Required !!");
            return;
        }

        return true;
    }

});