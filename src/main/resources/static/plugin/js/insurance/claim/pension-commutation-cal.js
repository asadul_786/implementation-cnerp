$(document).ready(function () {

    loadCalOption();

    $(document).on("change", "#calculationType", function (e) {

        var cal_option = $("#calculationType option:selected").val();

        if (cal_option == 'P') {
            $('#descendanthide').hide();
            $('#officeCode').hide();
            $('#PolicyNo').show();
            $('#officeCD').val('-1');
            $('#officeCD').selectpicker('refresh');
        } else if (cal_option == 'O') {
            resetDefault();
            $('#policyNoOne').val('');
            $('#descendanthide').show();
            $('#officeCode').show();
            $('#PolicyNo').hide();
        }
    });

    function loadCalOption() {
        resetDefault();
    }

    function resetDefault() {
        $('#descendanthide').show();
        $('#officeCode').show();
        $('#PolicyNo').show();
        $('#officeCD').val('-1');
        $('#officeCD').selectpicker('refresh');
    }


    $("#btnPensionSave").click(function () {
        var flag = dataValidation();
        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to process pension commutation calculation?");
        }
    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var officeCD = $('#officeCD').val();
                        var policyNoOne = $('#policyNoOne').val();
                        var applicationDate = $('#applicationDate').val();
                        var applicationDateTo = $('#applicationDateTo').val();
                        var decendentOfficeCheck = false;


                        if ($('#decendentOfficeCheck').prop("checked") == true) {
                            decendentOfficeCheck = true
                        } else {
                            decendentOfficeCheck = false;
                        }
                        if (applicationDate) {
                            applicationDate = applicationDate.split("/").reverse().join("/");
                            applicationDate = getFormateDate(applicationDate);
                        }
                        if (applicationDateTo) {
                            applicationDateTo = applicationDateTo.split("/").reverse().join("/");
                            applicationDateTo = getFormateDate(applicationDateTo);
                        }

                        if (officeCD == '-1') {
                            officeCD = "";
                        }

                        var pensionCommutationList = {};
                        pensionCommutationList.officeCD = officeCD;
                        pensionCommutationList.policyNoOne = policyNoOne;
                        pensionCommutationList.applicationDate = applicationDate;
                        pensionCommutationList.applicationDateTo = applicationDateTo;
                        pensionCommutationList.decendentOfficeCheck = decendentOfficeCheck;


                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/new-business/claim-payment/process-pension-calculation",
                            data: JSON.stringify(pensionCommutationList),
                            dataType: 'json',
                            success: function (data) {
                                var getkey = "";
                                var getValue = "";
                                $.each(data, function (key, val) {
                                    getkey = key;
                                    getValue = val;
                                });
                                if (getkey == 'Success') {
                                    //showAlert(getValue);
                                    confirmMessage("Pension commutation calculation Data Successfully Created!!", "S");
                                } else {
                                    //showAlertByType(getValue, 'W');
                                    confirmMessage(getValue, 'W');
                                }
                                clearform();
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", 'F');
                            }
                        });
                    }
                },
                cancel: function () {

                }
            }
        });
    };

    function dataValidation() {

        var status = true;

        if ($('#applicationDate').val() == "") {
            status = false;
            $("#err_applicationDate").text("Empty field found!!");
            $("#applicationDate").focus();

        } else if (isValidDate($("#applicationDate").val()) == false) {
            status = false;
            $("#err_applicationDate").text("Invalid date format!!");
            $("#applicationDate").focus();

        } else $("#err_applicationDate").text("");


        if ($('#applicationDateTo').val() == "") {
            status = false;
            $("#err_applicationDateTo").text("Empty field found!!");
            $("#applicationDateTo").focus();

        } else if (isValidDate($("#applicationDateTo").val()) == false) {
            status = false;
            $("#err_applicationDateTo").text("Invalid date format!!");
            $("#applicationDateTo").focus();

        } else if (lessthenOrderDate($("#applicationDate").val(), $("#applicationDateTo").val())) {
            status = false;
            $("#err_applicationDateTo").text("To date should be greater than From date!!");
            $("#applicationDateTo").focus();

        } else $("#err_applicationDateTo").text("");

            if ($('#policyNoOne').val() == "") {
                status = false;
                $("#err_policyNoOne").text("Empty field found!!");
                $("#policyNoOne").focus();
            } else if ($('#policyNoOne').val().length > 20) {
                status = false;
                $("#err_policyNoOne").text("Maximum 20 character allow!!");
                $("#policyNoOne").focus();
            } else $("#err_policyNoOne").text("");

            if ($('#officeCD').val() == "-1") {
                status = false;
                $("#err_officeCD").text("Empty field found!!");
                $("#officeCD").focus();
            } else $("#err_officeCD").text("");

        return status;
    }

    $("#btnPensionRefresh").click(function () {
        clearform();
    });

    function clearform() {
        resetDefault();
        $("#err_officeCD").text("");
        $("#err_policyNoOne").text("");
        $("#err_applicationDateTo").text("");
        $("#err_applicationDate").text("");
        $('#policyNoOne').val('');
        $('#officeCD').val('-1');
        $('#officeCD').selectpicker('refresh');
        $("#decendentOfficeCheck").prop('checked', false);
        $('#applicationDate').val('');
        $('#applicationDateTo').val('');
        $("#calculationType").val('P');
    }

    function getFormateDate(date) {
        var d = new Date(date);
        return d;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }


    var confirmMessage = function (text, messageType) {

        if (messageType == "S") {
            $.confirm({
                title: 'Success!',
                content: text,
                type: 'green',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        } else if (messageType == "F") {
            $.confirm({
                title: 'Error!',
                content: text,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        }
    };

    function lessthenOrderDate(fromdate, todate) {
        var lessStatus = false;
        //release date
        var date_r = fromdate.substring(0, 2);
        var month_r = fromdate.substring(3, 5);
        var year_r = fromdate.substring(6, 10);
        var fromdate_date = new Date(year_r, month_r - 1, date_r);
// orderDate
        var date_o = todate.substring(0, 2);
        var month_o = todate.substring(3, 5);
        var year_o = todate.substring(6, 10);
        var todate_date = new Date(year_o, month_o - 1, date_o);

        if (fromdate_date > todate_date) {
            lessStatus = true;
        }
        return lessStatus;
    }

    $( "#applicationDate" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1970:2020'
    });

});