/**
 * Created by Mithun on 3/17/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
$(document).ready(function () {
    $(".hideable-panel").css("display","none");


    $('.policy-no').on("keypress", function(e) {
        if (e.keyCode == 13) {

            checkPolicyNo();



        }
    });

    function  checkPolicyNo() {
        var claimCalcData = {};
        claimCalcData.policyNo = $('.policy-no').val();


        const chkPolicyNoUrl = "/new-business/claim-payment/chk-policno-get-data" ;
        $.ajax({
            url: chkPolicyNoUrl,
            type: 'POST',
            data: JSON.stringify(claimCalcData),
            contentType: 'application/json',
            async: false,
            success: function (data) {
                console.log(data);

                if(data.isSucceed){

                    showAlertByType('Found','S');
                    loadData(data);

                }
                else {
                    showAlertByType(data.msg,'W');
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlertByType('Something went wrong[xhr], Contact with IT team.','F');
            }

        });

    }
    function  loadData(data) {

        $("#pgid").val(data.pgid);
        $("#appsl-no").val(data.appSlNo);
        $("#ud-sl").val(data.ud_sl);

        $("#policy-no").val(data.policyNo);
        $("#risk-date").val(data.riskDate);
        $("#commence-date").val(data.commDate);
        $("#marurity-date").val(data.maturityDate);
        $("#policy-status").val(data.policyStatus);
        $("#summ-assured-amt").val(data.sumAssured);
        $("#product-name").val(data.productName);
        //   $("#tbl-client-lst").val(data.);
        $("#last-commutation").val(data.lastCommutationPct);

        var rows="";
        $.each( data.clientInfoList, function( i, l ){
            // alert( "Index #" + i + ": " + l );

            // l[0];l[1]

            rows+="<tr>" +

                "<td>"+(i+1)+"</td>" +
                "<td>"+l[0]+"</td>" +
                "<td>"+l[1]+"</td>" +
                "<td hidden>"+l[2]+"</td>" +

                "</tr>";


        });
        if(isEmptyString(rows)){
            rows="<tr>" +
                "<td colspan='3'>No client list was found</td>"+
                "</tr>>"
        }
        $("#tbl-client-lst-tbody").empty();
        $("#tbl-client-lst-tbody").append(rows);

        $(".hideable-panel").css("display","block");

    }
    function resetData() {
        $("#policy-no").val('');
        $("#risk-date").val('');
        $("#commence-date").val('');
        $("#marurity-date").val('');
        $("#policy-status").val('');
        $("#summ-assured-amt").val('');
        $("#product-name").val('');
        // $("#tbl-client-lst").empty();
        $("#tbl-client-lst-tbody").empty(); //.append(rows);
        $("#last-commutation").val('');
        $("#pgid").val('');
        $("#appsl-no").val('');

        $(".hideable-panel").css("display","none");
    }
    // $( ".application-date" ).datepicker({
    //     changeMonth: true,
    //     changeYear: true
    // });

    $("#policy-no").focus();
    //
    // <button type="button" id="btnRefreshPensionCommutation" class="btn btn-primary pull-right" >Refresh</button>
    //     <button type="button" id="btnSavePensionCommutation" class="btn btn-success pull-right">Save</button>

    $('#btnRefreshPensionCommutation').on("click", function(e) {

        resetData();
    });
    $('#btnSavePensionCommutation').on("click", function(e) {
        saveApplication();


    });

    function  saveApplication() {

        if (validatedDate()) {


            const chkPolicyNoUrl = "/new-business/claim-payment/appl-pension-commutation";
            $.ajax({
                url: chkPolicyNoUrl,
                type: 'POST',
                data: JSON.stringify(prepareDataSet()),
                contentType: 'application/json',
                success: function (data) {
                    console.log(data);

                    if (data.isSucceed) {

                        showAlertByType('Successfully applied.', 'S');


                    }
                    else {
                        showAlertByType(data.msg, 'W');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }

    }
    function  prepareDataSet()
    {
        var claimAppCommut = {};
        claimAppCommut.pgid= $("#pgid").val();
        claimAppCommut.appSlNo= parseNumber($("#appsl-no").val());
        claimAppCommut.applicationDate=Date.parse($('.application-date').val());
        claimAppCommut.clientType=$('#clientType').children("option:selected").val();

        claimAppCommut.ud_sl=$('#ud-sl').val();//+1
        claimAppCommut.commutationPct=parseFloat($('#commutation').val());
        claimAppCommut.remarks=$('#remarks').val();



        return claimAppCommut;
    }
    function validatedDate() {

        var totalCommutation=parseFloat($('#commutation').val())+parseFloat($('#last-commutation').val());
        if(totalCommutation>100){
            showAlertByType('Commutation PCT must be equal or less than 100%!','W');
            return false;
        }
        else if(isEmptyString($('#clientType').children("option:selected").val())){
            showAlertByType('Client type is required!','W');
            return false;
        }

        else if($('#tbl-client-lst tr > td:contains('+$('#clientType').children("option:selected").val()+')').length<= 0 ){
            showAlertByType('Selected client type is illigule, please select a valid client type!','W');
            return false;
        }
        else
        {
            return true;
        }

    }
    function parseNumber(str) {
        if(str==null|| str.trim()==""||str=="undefined"){
            return null;
        }
        else{
            var  intNum=parseInt(str);

            return intNum=="NaN"?null:intNum;
        }

    }
    function parseFloat(str) {
        if(str==null|| str.trim()==""||str=="undefined"){
            return null;
        }
        else{
            var  intNum=parseNumber(str);

            return intNum=="NaN"?null:intNum;
        }

    }
});

