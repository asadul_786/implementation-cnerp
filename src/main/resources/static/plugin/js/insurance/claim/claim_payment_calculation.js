/**
 * Created by golam on 3/5/2020.
 */
$(document).ready(function () {

    defaultFalse();
    loadCalOption();

    $("#btnCalculate").click(function () {
        var flag = dataValidation();
        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to process Claim collection?");
        }
    });


    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var claimType = $('#claimType').val();
                        var dateFrom = $('#dateFrom').val();
                        var dateTo = $('#dateTo').val();
                        var calcOption = $('#calcOption').val();
                        var isDescendantOffice = $('#isDescendantOffice').val();
                        var paymentType = $('#paymentType').val();
                        var policyNo = $('#policyNo').val();
                        var product = $('#product').val();
                        var office = $('#office').val();

                        if ($('#isDescendantOffice').prop("checked") == true) {
                            isDescendantOffice =true;
                        } else {
                            isDescendantOffice = false;
                        }

                        if (dateFrom) {
                            dateFrom = dateFrom.split("/").reverse().join("/");
                            dateFrom = getFormateDate(dateFrom);
                        }
                        if (dateTo) {
                            dateTo = dateTo.split("/").reverse().join("/");
                            dateTo = getFormateDate(dateTo);
                        }
                        function getFormateDate(date) {
                            var d = new Date(date);
                            return d;
                        }

                        var claimCal = {};
                        claimCal.claimType = claimType;
                        claimCal.paymentType = paymentType;
                        claimCal.dateFrom = dateFrom;
                        claimCal.dateTo = dateTo;
                        claimCal.calcOption = calcOption;
                        claimCal.product = product;
                        claimCal.office = office;
                        claimCal.policyNo = policyNo;
                        claimCal.isDescendantOffice = isDescendantOffice;
                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/new-business/claim-payment/process-claim-calculation",
                            data: JSON.stringify(claimCal),
                            dataType: 'json',
                            success: function (data) {
                                var getkey = "";
                                var getValue = "";
                                $.each(data, function (key, val) {
                                    getkey = key;
                                    getValue = val;
                                });
                                if (getkey == 'Success') {
                                    //showAlert(getValue);
                                    confirmMessage(getValue, "S");
                                } else {
                                    // showAlertByType(getValue, 'W');
                                    confirmMessage(getValue, "F");
                                }
                                clearform();
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", 'F');
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };

    $("#btnReset").click(function () {
        clearform();
    });

    function dataValidation() {

        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($('#claimType').val() == "" && status) {
            status = false;
            $("#err_claimType").text("Empty field found!!");
            $("#claimType").focus();
        } else  $("#err_claimType").text("");

        if ($('#paymentType').val() == "" && status) {
            status = false;
            $("#error_payment-type").text("Empty field found!!");
            $("#paymentType").focus();
        } else  $("#error_payment-type").text("");


        if ($('#office').val() == "") {
            status = false;
            $("#error_office").text("Empty field found!!");
            $("#office").focus();
        } else $("#error_office").text("");


        if ($('#dateFrom').val() == "" && status) {
            status = false;
            $("#err_date-from").text("Empty field found!!");
            $("#dateFrom").focus();

        } else if (isValidDate($("#dateFrom").val()) == false) {
            status = false;
            $("#err_date-from").text("Invalid Date!!");
            $("#dateFrom").focus();

        } else if (!dateCheck.test($("#dateFrom").val())) {
            status = false;
            $("#err_date-from").text("Invalid Date!!");
            $("#dateFrom").focus();

        } else $("#err_date-from").text("");


        if ($('#dateTo').val() == "" && status) {
            status = false;
            $("#error_date-to").text("Empty field found!!");
            $("#dateTo").focus();

        } else if (isValidDate($("#dateTo").val()) == false) {
            status = false;
            $("#error_date-to").text("Invalid Date!!");
            $("#dateTo").focus();

        } else if (lessthenOrderDate($("#dateFrom").val(), $("#dateTo").val())) {
            status = false;
            $("#error_date-to").text("To date should be greater than From date!!");
            $("#dateTo").focus();

        } else if (!dateCheck.test($("#dateTo").val())) {
            status = false;
            $("#error_date-to").text("Invalid Date!!");
            $("#dateTo").focus();

        } else $("#error_date-to").text("");

        return status;
    }


    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    function clearform() {
        $("#claimType").val("");
        $("#dateFrom").val("");
        $("#dateTo").val("");
        $("#policyNo").val("");
        $("#calcOption").val("");
        $("#paymentType").val("");
        $('#product').selectpicker('refresh');
        $('#office').selectpicker('refresh');
        $("#isDescendantOffice").prop('checked', false);
        //error clr
        $("#err_date-from").text("");
        $("#err_descendant-office").text("");
        $("#error_payment-type").text("");
        $("#error_date-to").text("");
        $("#error_policyNo").text("");
        $("#error_product").text("");
        $("#error_office").text("");
        $("#err_claimType").text("");
        defaultFalse();
    }

    function defaultFalse() {
        $('#policyNohide').hide();
        $('#producthide').hide();
        // $('#officehide').hide();
        // $('#descendanthide').hide();
        //
        $("#descendant-office").prop('checked', false);
        $('#policyNo').val("");
        $('#product').val('');
        $('#product').selectpicker('refresh');
        $('#office').val('');
        $('#office').selectpicker('refresh');

    }

    function loadCalOption() {

        $(document).on("change", "#calcOption", function (e) {

            var cal_option = $("#calcOption option:selected").val();

            if (cal_option == '') {
                defaultFalse();

            } else if (cal_option == 'P') {
                $('#policyNohide').show();
                $('#producthide').hide();
                //$('#officehide').hide();
                //$('#descendanthide').hide();
            } else if (cal_option == 'D') {
                $('#policyNohide').hide();
                $('#producthide').show();
                // $('#officehide').hide();
                //$('#descendanthide').hide();
                $('#product').val('');
                $('#product').selectpicker('refresh');
                $('#office').val('');
                $('#office').selectpicker('refresh');
            } else if (cal_option == 'O') {
                $('#policyNohide').hide();
                $('#producthide').hide();
                // $('#officehide').show();
                // $('#descendanthide').show();
                $('#product').val('');
                $('#product').selectpicker('refresh');
                $('#office').val('');
                $('#office').selectpicker('refresh');
            }

        });
    }

    var confirmMessage = function (text, messageType) {

        if (messageType == "S") {
            $.confirm({
                title: 'Success!',
                content: text,
                type: 'green',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        } else if (messageType == "F") {
            $.confirm({
                title: 'Error!',
                content: text,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        }
    };

    function lessthenOrderDate(fromdate, todate) {
        var lessStatus = false;
        //release date
        var date_r = fromdate.substring(0, 2);
        var month_r = fromdate.substring(3, 5);
        var year_r = fromdate.substring(6, 10);
        var fromdate_date = new Date(year_r, month_r - 1, date_r);
// orderDate
        var date_o = todate.substring(0, 2);
        var month_o = todate.substring(3, 5);
        var year_o = todate.substring(6, 10);
        var todate_date = new Date(year_o, month_o - 1, date_o);

        if (fromdate_date > todate_date) {
            lessStatus = true;
        }
        return lessStatus;
    }

});