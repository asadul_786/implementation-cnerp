/**
 * Created by Mithun on 3/19/2020.
 */
$(document).ready(function () {


    var table = $('#commutation-approval-list').DataTable( {
        //"ajax": "/ajax/objects.txt",

        data : [],
        "columns": [
            { "data": null, defaultContent: '' },
           // { "data": "???" },
            { "data": "msgCode" },
            { "data": "msg" },
            { "data": "remarks" },
            { "data": "msgCode" },
            { "data": "msg" },
            { "data": "remarks" },
            { "data": "msg" },
            { "data": "remarks" }


        ],
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
            }
        }],
        'order': [[1, 'asc']]
    } );
    $("#btnSearchPensionCommutationApproval").on("click", function (event) {
      loadDatableWithFilteredData();
    });
    $("#btnRefreshPensionCommutationApproval").on("click", function (event) {
        table.clear().draw();
    });
    $('.commutation-approval-list-select-all').on('click', function(){
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
    $('#frm-example').on('submit', function(e) {
        var form = this;

        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function () {
            // If checkbox doesn't exist in DOM
            if (!$.contains(document, this)) {
                // If checkbox is checked
                if (this.checked) {
                    // Create a hidden element
                    $(form).append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                    );
                }
            }
        });
    });



    function loadDatableWithFilteredData() {
        $.ajax({
            url: "/new-business/claim-payment/appl-pension-app-approval-search",
            type: "get"

        }).done(function (result) {
            table.clear().draw();
            table.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }
});

// String msgCode;
// String msg;
// String remarks;
// Boolean isSucceed ;