/**
 * Created by Mithun on 3/13/2020.
 */
$(document).ready(function () {

   $('#tbl-awaiting-apprval-lst').DataTable({

        // "sDom": "lfrti",
        "dom": '<"pull-left"f><"pull-right"l>tip',
        // paging:         true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: ""
        },
        "info":     false,
       "scrollY":        "600px",
       "scrollCollapse": true,
       "paging":         false

    }) ;

    $('.dataTables_filter').addClass('pull-left');
    $('.proposal-list-tr').css("cursor", "pointer");


    $(document).on("click", ".proposal-list-tr", function () {
        // alert( $('td', this).eq(1).find('span').html());
        $(this).addClass("selected").siblings().removeClass("selected");

       loadData($('td', this).eq(0).find('span').html(), $('td', this).eq(1).find('span').html())

    });

    function  loadData(pgid,applNo) {


 // @RequestMapping(value = "/adv-payment-app-approval-lst/{pgid}/{applNo}", method = RequestMethod.GET)
        const getListUrl = "/new-business/claim-payment/adv-payment-app-approval-lst/" + pgid+'/'+applNo;
        $.ajax({
            url: getListUrl,
            contentType: 'application/json',
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                // set form data
                console.log(data);
                clearFormData();
                $('.appl-no').val(pgid);
                $('.pggid').val(applNo);
                fillFormData(data);


            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlertByType('Something went wrong[xhr], Contact with IT team.' + jqXhr,'F');
            }
        });

    }
    function clearFormData(){
        $('.policy-no').val('');
        $('.commence-date').val('');

        $('.policy-status').val('');
        $('.lats-paid-date').val('');

        $('.summ-assured-amt').val('');
        $('.term').val('');

        $('.product-name').val('');

    // 2nd block
        $('.application-date').val('');
        $('.client-type').val('');

        $('.client-name').val('');

        $('.req-pay-date').val('');
        $('.payment-type').val('');

        $('.payable-date').val('');
        $('.month-differ').val('');

        $('.payable-amt').val('');
        $('.bonus-amt').val('');

        $('.total-payable-amt').val('');
        $('.discount-amt').val('');

        $('.net-payable-amt').val('');

        $('.payment-type-cd').val('');
        $('.client-type-cd').val('');
        $('.policy-status-cd').val( '');

        $('.product-cd').val('');

         $('.appl-no').val('');
      $('.pggid').val('');
        $('discount-factor').val();

    };
    function  fillFormData(data){

        $('.policy-no').val( data[0][1]);
        $('.commence-date').val( data[0][2]);

        $('.policy-status').val( data[0][3]);
        $('.lats-paid-date').val( data[0][4]);

        $('.summ-assured-amt').val( data[0][5]);
        $('.term').val( data[0][6]);


        $('.product-name').val( data[0][7]);

        // 2nd block
        $('.application-date').val( data[0][8]);
        $('.client-type').val(data[0][9]);

        $('.client-name').val(data[0][10]);

        $('.req-pay-date').val(data[0][11]);
        $('.payment-type').val(data[0][12]);

        $('.payable-date').val(data[0][13]);
        $('.month-differ').val(data[0][14]);

        $('.payable-amt').val(data[0][15]);
        $('.bonus-amt').val(data[0][16]);

        $('.total-payable-amt').val(data[0][17]);
        $('.discount-amt').val(data[0][18]);

        $('.net-payable-amt').val(data[0][19]);

        $('.payment-type-cd').val(data[0][20]);
        $('.client-type-cd').val(data[0][21]);
        $('.policy-status-cd').val( data[0][22]);
        $('.product-cd').val( data[0][23]);
        $('.discount-factor').val( data[0][24]);





    };



    $( "#btnReset" ).click(function() {
        clearFormData();

    });
    $("#btnApproved" ).click(function() {
        // showAlertByType("Not Implemented","W");
        $.confirm({
            // icon: 'ace-icon fa fa-smile-o',
            icon: 'ace-icon fa fa-lock',
            theme: 'material',
            animation: 'scale',
            type: 'orange',
            useBootstrap: true,
            title:  "Are you sure to proceed?",
            content: "Once approved, you will not be revert the approval process",
            typeAnimated: true,


            buttons: {
                Yes: {

                    text: 'Approve',
                    btnClass: 'btn-warning',
                    action: function () {
                        if(isValidate()){


                            const url = "/new-business/claim-payment" + "/" + "approve-adv-pay-app";
                            $.ajax({
                                url: url,
                                type: 'POST',
                                data: JSON.stringify( prepareData()),
                                contentType: 'application/json',
                                async: false,
                                success: function (commonMsg) {
                                    // console.log(commonMsg);
                                    if (commonMsg.isSucceed) {
                                        showAlertByType( commonMsg.remarks +'['+ commonMsg.msgCode+']', "S");
                                        // customAlert(alertTypes.SUCCESS, 'Success[' + commonMsg.msgCode + ']', 'Generated Successfully.');
                                        $("#btnReset").trigger("click");
                                    }
                                    else {
                                        showAlertByType( commonMsg.remarks +'['+ commonMsg.msgCode+']', "W");
                                        // customAlert(alertTypes.FAILED, 'Unsuccessful[' + commonMsg.msgCode + ']', commonMsg.msg + '[' + commonMsg.remarks + ']');
                                    }
                                },
                                error: function (jqXhr, textStatus, errorThrown) {
                                    showAlertByType('Something went wrong[xhr], Contact with IT team.', "F");
                                    // customAlert(alertTypes.ERROR, 'Error', 'Something went wrong[xhr], Contact with IT team.');
                                }

                            });
                        }
                        else
                        {
                            $.confirm({
                                icon: 'ace-icon fa fa-ban',
                                theme: 'material',
                                closeIcon: true,
                                animation: 'scale',
                                type: 'red',

                                title: 'Required data Validation',
                                content:  "Level, Approved by is required!",
                                typeAnimated: true,
                                buttons: {
                                    Yes: {
                                        text: 'Ok'
                                    }
                                }

                            });
                        }


                    }

                },
                Cancel: {
                    btnClass: 'btn-info',
                    text: 'Cancel',
                    action: function () {
                        return true;
                    }

                }
            }

        });


    });

    // $("#reject" ).click(function() {
    //     // showAlertByType("Not Implemented","W");
    //     $.confirm({
    //         // icon: 'ace-icon fa fa-smile-o',
    //         icon: 'ace-icon fa fa-lock',
    //         theme: 'material',
    //         animation: 'scale',
    //         type: 'orange',
    //         useBootstrap: true,
    //         title:  "Are you sure to proceed?",
    //         content: "Once approved, you will not be revert the approval process",
    //         typeAnimated: true,
    //
    //
    //         buttons: {
    //             Yes: {
    //
    //                 text: 'Approve',
    //                 btnClass: 'btn-warning',
    //                 action: function () {
    //                     if(isValidate()){
    //
    //
    //                         const url = "/new-business/claim-payment" + "/" + "approve-adv-pay-app";
    //                         $.ajax({
    //                             url: url,
    //                             type: 'POST',
    //                             data: JSON.stringify( prepareData()),
    //                             contentType: 'application/json',
    //                             async: false,
    //                             success: function (commonMsg) {
    //                                 // console.log(commonMsg);
    //                                 if (commonMsg.isSucceed) {
    //                                     showAlertByType( commonMsg.remarks +'['+ commonMsg.msgCode+']', "S");
    //                                     // customAlert(alertTypes.SUCCESS, 'Success[' + commonMsg.msgCode + ']', 'Generated Successfully.');
    //                                     $("#btnReset").trigger("click");
    //                                 }
    //                                 else {
    //                                     showAlertByType( commonMsg.remarks +'['+ commonMsg.msgCode+']', "W");
    //                                     // customAlert(alertTypes.FAILED, 'Unsuccessful[' + commonMsg.msgCode + ']', commonMsg.msg + '[' + commonMsg.remarks + ']');
    //                                 }
    //                             },
    //                             error: function (jqXhr, textStatus, errorThrown) {
    //                                 showAlertByType('Something went wrong[xhr], Contact with IT team.', "F");
    //                                 // customAlert(alertTypes.ERROR, 'Error', 'Something went wrong[xhr], Contact with IT team.');
    //                             }
    //
    //                         });
    //                     }
    //                     else
    //                     {
    //                         $.confirm({
    //                             icon: 'ace-icon fa fa-ban',
    //                             theme: 'material',
    //                             closeIcon: true,
    //                             animation: 'scale',
    //                             type: 'red',
    //
    //                             title: 'Required data Validation',
    //                             content:  "Level, Approved by is required!",
    //                             typeAnimated: true,
    //                             buttons: {
    //                                 Yes: {
    //                                     text: 'Ok'
    //                                 }
    //                             }
    //
    //                         });
    //                     }
    //
    //
    //                 }
    //
    //             },
    //             Cancel: {
    //                 btnClass: 'btn-info',
    //                 text: 'Cancel',
    //                 action: function () {
    //                     return true;
    //                 }
    //
    //             }
    //         }
    //
    //     });
    //
    //
    // });

    function  isValidate() {
        if(isEmptyString( $('.appl-no').val())||isEmptyString( $('.pggid').val())||isEmptyString( $('.policy-no').val())){
            return true;
        }
        else {
            return true;
        }

    }

    function  prepareData() {

        var advancePaymentApprovalData = {};



        advancePaymentApprovalData.levelName = $('#level').val();
        advancePaymentApprovalData.approvedBy = $('.approved-by').children("option:selected").val()
        advancePaymentApprovalData.approvedDate = $('.date').val();


        advancePaymentApprovalData.applNO = $('.pggid').val();
        advancePaymentApprovalData.pggid = $('.appl-no').val();
        advancePaymentApprovalData.policyNo = $('.policy-no').val();


        advancePaymentApprovalData.discountFactor = $('.discount-factor').val( );
        advancePaymentApprovalData.paymentTypeCd = $('.payment-type-cd').val();
        advancePaymentApprovalData.actualPayableDate = $('.payable-date').val();
        advancePaymentApprovalData.reqPayableDate = $('.payable-date').val();
        advancePaymentApprovalData.netPayableAmount = $('.net-payable-amt').val();

        return advancePaymentApprovalData;
    }

});
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}