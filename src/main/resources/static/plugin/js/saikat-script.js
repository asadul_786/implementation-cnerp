function setCookie(cname, cvalue, mins) {
	var d = new Date();
	d.setTime(d.getTime() + (mins*60*1000));
	var expires = "expires="+ d;
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
$(document).ready(function(){
	$("#loginMgs").hide();

	$("#btnLogin").on('click',function(){
		var url = "dashboard";
		var users = [{user:"jbcnb", pass:"123",access:1},{user:"jbcuw", pass:"123",access:2}];
		var username = $("#username").val().trim();
		var password = $("#password").val().trim();
		$("#loginMgs").hide();
		$.each(users,function(k,v){
			if(v.user == username && v.pass == password){
				setCookie('x', v.access, 50); 
				window.location.href = url; 
				return false;
			}
		});
	});
});