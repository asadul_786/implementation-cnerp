$(document).ready(function () {

    loadCollectionFrom();

    $(document).on("change", "#fiscalfromDate", function (e) {
        var s = $("#fiscalfromDate option:selected").text();
        s = s.substring(14, s.indexOf('.'));
        $('#fiscaltoDate').val(s);
    });

    if ($("#fiscalfromDate").val() != "") {
        var s = $("#fiscalfromDate option:selected").text();
        s = s.substring(14, s.indexOf('.'));
        $('#fiscaltoDate').val(s);
    }

    $(document).on("input", "#agentIdsearch", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var agentIdsearch = $('#agentIdsearch').val();


        if (agentIdsearch != "" && agentIdsearch.length < 9) {
            $.ajax({
                url: "/agentcommission/getAgentName/" + agentIdsearch,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var agentName = "";
                    $.each(response, function (i, l) {
                        agentName = l[0];
                    });
                    $("#agentNamedetsils").val(agentName);
                    $("#err_agentIdsearch").text("");
                },
                error: function (xhr, status, error) {
                    $("#err_agentIdsearch").text("Sorry! Agent ID not match!!");
                    $("#agentNamedetsils").val("");
                }
            });
        }
        else {
            if ($('#agentIdsearch').val() == "") {
                $("#err_agentIdsearch").text("Agent ID required!");
                $("#agentNamedetsils").val("");
            }
            else {
                $("#err_agentIdsearch").text("ID maximum 8 characters!");
            }
        }

    });


    $("#btnFirstprepareSave").click(function () {

        var flag = dataValidation();

        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to process first premium agent commission calculation?");
        }
    });

    $("#btnFirstprepareRefresh").click(function () {
        clearform();
    });

    $(document).on("change", "#calcalutionType", function (e) {
        loadCollectionFrom();
    });


    function loadCollectionFrom() {

        var calcalutionType = $("#calcalutionType option:selected").val();

        if (calcalutionType == 0) {
            $("#decendentOffice").show();
            $("#officeCode").show();
            $("#agentId").hide();
            $("#agentName").hide();
        } else {
            $("#agentId").show();
            $("#agentName").show();
            $("#decendentOffice").hide();
            $("#officeCode").hide();
        }
    }

    function clearform() {
        // var today = new Date();
        //  $("#fiscalfromDate").val(getDate(today));
        $("#fiscaltoDate").val("");
        $("#calcalutionType").val("1");
        $("#officeCodedetails").val("-1");
        $("#agentIdsearch").val("");
        $("#decendentOfficeCheck").prop('checked', false);
        $("#billintType").val("-1");
        $("#minimumbillingAmount").val("500.00");
        $("#deductionRate").val("5.00");
        $("#agentNamedetsils").val("");
        $("#billingDate").val("");
        loadCollectionFrom();
        //clear error from
        $("#err_billingDate").text("");
        $("#err_agentIdsearch").text("");
        $("#err_billintType").text("");
        $("#err_fiscalfromDate").text("");
        $("#err_officeCodedetails").text("");

        if ($("#fiscalfromDate").val() != "") {
            var s = $("#fiscalfromDate option:selected").text();
            s = s.substring(14, s.indexOf('.'));
            $('#fiscaltoDate').val(s);
        }
    }

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var fiscalfromDate = $('#fiscalfromDate').val();
                        var fiscaltoDate = $('#fiscaltoDate').val();
                        var billingofficecodeName = $('#billingofficecodeName').val();
                        var calcalutionType = $('#calcalutionType').val();
                        var officeCodedetails = $('#officeCodedetails').val();
                        var agentIdsearch = $('#agentIdsearch').val();
                        var billintType = $('#billintType').val();
                        var billingDate = $('#billingDate').val();
                        var minimumbillingAmount = '500';
                        var deductionRate = '5';
                        var agentNamedetsils = $('#agentNamedetsils').val();
                        var decendentOfficeCheck;


                        if ($('#decendentOfficeCheck').prop("checked") == true) {
                            decendentOfficeCheck = '1'
                        } else {
                            decendentOfficeCheck = '0'
                        }

                        if (fiscalfromDate) {
                            fiscalfromDate = fiscalfromDate.split("/").reverse().join("/");
                            fiscalfromDate = getFormateDate(fiscalfromDate);
                        }
                        if (fiscaltoDate) {
                            fiscaltoDate = fiscaltoDate.split("/").reverse().join("/");
                            fiscaltoDate = getFormateDate(fiscaltoDate);
                        }
                        if (billingDate) {
                            billingDate = billingDate.split("/").reverse().join("/");
                            billingDate = getFormateDate(billingDate);
                        }

                        function getFormateDate(date) {
                            var d = new Date(date);
                            return d;
                        }

                        var agentCommissionbill = {};
                        agentCommissionbill.fiscalfromDate = fiscalfromDate;
                        agentCommissionbill.fiscaltoDate = fiscaltoDate;
                        agentCommissionbill.billingofficecodeName = billingofficecodeName;
                        agentCommissionbill.calcalutionType = calcalutionType;
                        agentCommissionbill.officeCodedetails = officeCodedetails;
                        agentCommissionbill.agentIdsearch = agentIdsearch;
                        agentCommissionbill.billintType = billintType;
                        agentCommissionbill.billingDate = billingDate;
                        agentCommissionbill.minimumbillingAmount = minimumbillingAmount;
                        agentCommissionbill.deductionRate = deductionRate;
                        agentCommissionbill.agentNamedetsils = agentNamedetsils;
                        agentCommissionbill.decendentOfficeCheck = decendentOfficeCheck;

                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/agentcommission/saveFirstpremiumAgentCommission",
                            data: JSON.stringify(agentCommissionbill),
                            dataType: 'json',
                            success: function (data) {
                                var getStatus = "";
                                var getMsg = "";
                                $.each(data, function (key, val) {
                                    getStatus = data.getSts;
                                    getMsg = data.getMsg;
                                });
                                if (getStatus == 'Success') {
                                    // showAlert('First Premium Agent Commission Calculation,' + getStatus);
                                    confirmMessage(getMsg, "S");
                                } else {
                                    //showAlertByType(getStatus, 'F');
                                    confirmMessage(getMsg, "F");
                                }
                                clearform();
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", "F");
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };

    var confirmMessage = function (text, messageType) {

        if (messageType == "S") {
            $.confirm({
                title: 'Success!',
                content: text,
                type: 'green',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        } else if (messageType == "F") {

            $.confirm({
                title: 'Error!',
                content: text,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        }
    };

    function dataValidation() {

        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($('#fiscalfromDate').val() == "-1") {
            status = false;
            $("#err_fiscalfromDate").text("Empty field found!!");
            $("#fiscalfromDate").focus();
        } else $("#err_fiscalfromDate").text("");


        if ($('#billintType').val() == "-1") {
            status = false;
            $("#err_billintType").text("Empty field found!!");
            $("#billintType").focus();
        } else $("#err_billintType").text("");


        if ($('#calcalutionType').val() == 0) {
            if ($('#officeCodedetails').val() == "-1") {
                status = false;
                $("#err_officeCodedetails").text("Empty field found!!");
                $("#officeCodedetails").focus();
            } else $("#err_officeCodedetails").text("");

        } else {
            if ($('#agentIdsearch').val() == "") {
                status = false;
                $("#err_agentIdsearch").text("Empty field found!!");
                $("#agentIdsearch").focus();
            } else $("#err_agentIdsearch").text("");
        }

        if ($('#billingDate').val() == "") {
            status = false;
            $("#err_billingDate").text("Empty field found!!");
            $("#billingDate").focus();

        } else if (!dateCheck.test($("#billingDate").val())) {
            status = false;
            $("#err_billingDate").text("Invalid date format!!");
            $("#billingDate").focus();

        } else if (isValidDate($('#billingDate').val()) == false) {
            status = false;
            $("#err_billingDate").text("Invalid date format!!");
            $("#billingDate").focus();

        } else $("#err_billingDate").text("");

        return status;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var year = d.getFullYear();
        var day;
        var month;
        day = "0" + "1";
        month = "0" + "1";
        var date = day + "/" + month + "/" + year;
        return date;
    }

});