$(document).ready(function () {

    loadCollectionFrom();

    $(document).on("change", "#fiscalfromDate", function (e) {
        var s = $("#fiscalfromDate option:selected").text();
        s = s.substring(14, s.indexOf('.'));
        $('#fiscaltoDate').val(s);
    });

    if($("#fiscalfromDate").val()!=""){
        var s = $("#fiscalfromDate option:selected").text();
        s = s.substring(14, s.indexOf('.'));
        $('#fiscaltoDate').val(s);
    }

    $(document).on("input", "#agentIdsearch", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var agentIdsearch = $('#agentIdsearch').val();


        if (agentIdsearch != "" && agentIdsearch.length < 9) {
            $.ajax({
                url: "/agentcommission/getAgentName/" + agentIdsearch,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var agentName = "";
                    $.each(response, function (i, l) {
                        agentName = l[0];
                    });
                    $("#agentNamedetsils").val(agentName);
                    $("#err_agentIdsearch").text("");
                },
                error: function (xhr, status, error) {
                    $("#err_agentIdsearch").text("Sorry! Agent ID not match!!");
                    $("#agentNamedetsils").val("");
                }
            });
        }
        else {
            if ($('#agentIdsearch').val() == "") {
                $("#err_agentIdsearch").text("Agent ID required!");
                $("#agentNamedetsils").val("");
            }
            else {
                $("#err_agentIdsearch").text("ID maximum 8 characters!");
            }
        }

    });

    $("#btnspecialCommisionSave").click(function () {

        var flag = dataValidation();

        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to process Special Agent Commission Calculation?");
        }

    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var fiscalfromDate = $('#fiscalfromDate').val();
                        var fiscaltoDate = $('#fiscaltoDate').val();
                        var calcalutionType = $('#calcalutionType').val();
                        var agentIdsearch = $('#agentIdsearch').val();
                        var officeCodedetails = $('#officeCodedetails').val();
                        var adjustmentDate = $('#adjustmentDate').val();
                        var calculationFrom = $('#calculationFrom').val();
                        var dependencybonusCommision;
                        var decendentOfficeCheck;

                        if ($('#dependencybonusCommision').prop("checked") == true) {
                            dependencybonusCommision = '1'
                        } else {
                            dependencybonusCommision = '0';
                        }

                        if ($('#decendentOfficeCheck').prop("checked") == true) {
                            decendentOfficeCheck = '1'
                        } else {
                            decendentOfficeCheck = '0'
                        }

                        var collectionAdjustment = {};
                        collectionAdjustment.fiscalfromDate = fiscalfromDate;
                        collectionAdjustment.fiscaltoDate = fiscaltoDate;
                        collectionAdjustment.calcalutionType = calcalutionType;
                        collectionAdjustment.officeCodedetails = officeCodedetails;
                        collectionAdjustment.agentIdsearch = agentIdsearch;
                        collectionAdjustment.decendentOfficeCheck = decendentOfficeCheck;
                        collectionAdjustment.dependencybonusCommision = dependencybonusCommision;
                        collectionAdjustment.adjustmentDate = adjustmentDate;
                        collectionAdjustment.calculationFrom = calculationFrom;

                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/agentcommission/saveSpecial-Agent",
                            data: JSON.stringify(collectionAdjustment),
                            dataType: 'json',
                            success: function (data) {
                                var getStatus = "";
                                $.each(data, function (key, val) {
                                    getStatus = val;
                                });
                                showAlert(getStatus);
                                clearform();
                            },
                            error: function (e) {
                                showAlert("Sorry,Something Wrong!!");
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };

    $("#btnspecialCommisionRefresh").click(function () {
        clearform();
    });

    $(document).on("change", "#calcalutionType", function (e) {
        loadCollectionFrom();
    });


    function loadCollectionFrom() {

        var calcalutionType = $("#calcalutionType option:selected").val();

        if (calcalutionType == 0) {
            $("#decendentOffice").show();
            $("#officeCode").show();
            $("#agentId").hide();
            $("#agentName").hide();
        } else {
            $("#agentId").show();
            $("#agentName").show();
            $("#decendentOffice").hide();
            $("#officeCode").hide();
        }
    }

    function clearform() {
        // $("#fiscalfromDate").val("-1");
        $("#agentIdsearch").val("");
        $("#decendentOfficeCheck").prop('checked', false);
        $("#dependencybonusCommision").prop('checked', false);
        $("#fiscaltoDate").val("");
        $("#adjustmentDate").val("");
        $("#agentNamedetsils").val("");
        $("#officeCodedetails").val("-1");
        $("#calcalutionType").val("0");
        $("#calculationFrom").val("");
        loadCollectionFrom();
        //
        $("#err_adjustmentDate").text("");
        $("#err_agentIdsearch").text("");
        $("#err_fiscalfromDate").text("");
        $("#err_officeCodedetails").text("");
        $("#err_calculationFrom").text("");
    }


    function dataValidation() {
        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($('#fiscalfromDate').val() == "-1") {
            status = false;
            $("#err_fiscalfromDate").text("Empty field found!!");
            $("#fiscalfromDate").focus();
        } else $("#err_fiscalfromDate").text("");

        if ($('#calcalutionType').val() == 0) {
            if ($('#officeCodedetails').val() == "-1") {
                status = false;
                $("#err_officeCodedetails").text("Empty field found!!");
                $("#officeCodedetails").focus();
            } else $("#err_officeCodedetails").text("");

        } else {
            if ($('#agentIdsearch').val() == "") {
                status = false;
                $("#err_agentIdsearch").text("Empty field found!!");
                $("#agentIdsearch").focus();
            } else $("#err_agentIdsearch").text("");
        }

        if ($('#adjustmentDate').val() == "") {
            status = false;
            $("#err_adjustmentDate").text("Empty field found!!");
            $("#adjustmentDate").focus();

        } else if (!dateCheck.test($("#adjustmentDate").val())) {
            status = false;
            $("#err_adjustmentDate").text("Invalid Date!!");
            $("#adjustmentDate").focus();

        } else $("#err_adjustmentDate").text("");


        if ($('#calculationFrom').val() == "") {
            status = false;
            $("#err_calculationFrom").text("Empty field found!!");
            $("#calculationFrom").focus();

        } else if (!dateCheck.test($("#calculationFrom").val())) {
            status = false;
            $("#err_calculationFrom").text("Invalid Date!!");
            $("#calculationFrom").focus();

        } else $("#err_calculationFrom").text("");

        return status;
    }

});