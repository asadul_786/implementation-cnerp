$(document).ready(function () {

    $(document).on("input", "#agentId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var agentIdsearch = $('#agentId').val();

        if (agentIdsearch != "" && agentIdsearch.length < 9) {
            $.ajax({
                url: "/agentcommission/getAgentName/" + agentIdsearch,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $("#err_agentIdsearch").text("");
                },
                error: function (xhr, status, error) {
                    $("#err_agentIdsearch").text("Sorry! Agent ID not match!!");
                }
            });
        }
        else {
            if ($('#agentId').val() == "") {
                $("#err_agentIdsearch").text("Agent ID required!");

            }
            else {
                $("#err_agentIdsearch").text("ID maximum 8 characters!");
            }
        }

    });


    $(document).on("input", "#policyNumber", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyNo = $('#policyNumber').val();

        if (policyNo != "" && policyNo.length < 20) {
            $.ajax({
                url: "/policy-alteration/getPGID/" + policyNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $("#err_policyNumber").text("");
                },
                error: function (xhr, status, error) {
                    $("#err_policyNumber").text("Sorry! Policy NO not match!!");
                }
            });
        }
        else {
            if ($('#policyNumber').val() == "") {
                $("#err_policyNumber").text("Policy NO required!");
            }
            else {
                $("#err_policyNumber").text("Policy NO maximum 20 characters!");
            }
        }

    });

    $("#btnundosave").click(function () {

        var flag = dataValidation();

        if (flag == true) {
            confirmEmpJoinDialog("Are you sure want to save?");
        }

    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var premiumType = $('#premiumType').val();
                        var agentId = $('#agentId').val();
                        var policyNumber = $('#policyNumber').val();
                        var collectionType = $('#collectionType').val();
                        var billNumber = $('#billNumber').val();
                        var collectionDate = $('#collectionDate').val();
                        var requestedBy = $('#requestedBy').val();
                        var operationType = $('#operationType').val();

                        var billcommisionUndo = {};
                        billcommisionUndo.premiumType = premiumType;
                        billcommisionUndo.agentId = agentId;
                        billcommisionUndo.policyNumber = policyNumber;
                        billcommisionUndo.collectionType = collectionType;
                        billcommisionUndo.billNumber = billNumber;
                        billcommisionUndo.collectionDate = collectionDate;
                        billcommisionUndo.requestedBy = requestedBy;
                        billcommisionUndo.operationType = operationType;

                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/agentcommission/saveundoCollection",
                            data: JSON.stringify(billcommisionUndo),
                            dataType: 'json',
                            success: function (data) {
                                var getStatus = "";
                                $.each(data, function (key, val) {
                                    getStatus = val;
                                });
                                showAlert(getStatus);
                                //confirmMessage(getStatus, "S");
                                clearform();
                            },
                            error: function (e) {
                                showAlert("Sorry,Something Wrong!!");
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };


    function dataValidation() {

        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($('#operationType').val() == "-1") {
            status = false;
            $("#err_operationType").text("Empty field found!!");
            $("#operationType").focus();
        } else $("#err_operationType").text("");

        if ($('#agentId').val() == "") {
            status = false;
            $("#err_agentIdsearch").text("Empty field found!!");
            $("#agentId").focus();
        } else $("#err_agentIdsearch").text("");

        if ($('#policyNumber').val() == "") {
            status = false;
            $("#err_policyNumber").text("Empty field found!!");
            $("#policyNumber").focus();
        } else $("#err_policyNumber").text("");


        if ($('#collectionDate').val() != "") {
            if (!dateCheck.test($("#collectionDate").val())) {
                status = false;
                $("#err_collectionDate").text("Invalid Date!!");
                $("#collectionDate").focus();
            } else $("#err_collectionDate").text("");

        } else $("#err_collectionDate").text("");

        return status;
    }

    $("#btnundoRefresh").click(function () {
        clearform();
    });

    function clearform() {
        $("#premiumType").val("0");
        $("#operationType").val("-1");
        $("#agentId").val("");
        $("#policyNumber").val("");
        $("#billNumber").val("");
        $("#collectionDate").val("");
        $("#requestedBy").val("");
        //error
        $("#err_collectionDate").text("");
        $("#err_policyNumber").text("");
        $("#err_agentIdsearch").text("");
        $("#err_operationType").text("");
    }

});