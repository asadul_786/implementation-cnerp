/**
 * Created by Raj on 21-Aug-19.
 */
$(document).ready(function () {

    $("#advertisement_log_table").dataTable();

    //reloading the page
    $(document).on("click", "#ad_log_reload", function (e) {
        location.reload();
    });

    //validation
    function validate() {

        return true;
    }


    //buttons
    function generateActionBtns(id){

        var btn = "<button type='button' style='text-align:center;vertical-align: middle; cursor: hand' class='btn btn-primary view_details' id='"+id+"'>"+
            "View details</button>";
        return btn;
    }

    //List of Offices selected by office category id
    $(document).on("change","#ad_log_OC_in", function (e) {
        var catId = $("#ad_log_OC_in option:selected").val();
        $.get("/public-affairs/get-office?offCatgId=" + catId,
            function (data, status) {
                //REMOVING VALIDATION TEXT(IF EXISTS)
                $("#err_item_pending_ROC_in").text("");
                var office = $('#ad_log_O_in');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "-- Select Office --"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices.id,
                        text: offices.officeName
                    }));
                });

            });
    });


    //pressing save button
    $(document).on('click','#ad_log_save' ,function () {

        if(validate()) {

            var public_affairs_ad_log = {};
            public_affairs_ad_log.mediaType = $("#ad_log_MT_in").val();
            public_affairs_ad_log.mediaCategory = $("#ad_log_MC_in").val();
            public_affairs_ad_log.purposeOfAdvertise = $("#ad_log_POA_in").val();
            public_affairs_ad_log.mediaSpecificName = $("#ad_log_MSN_in").val();
            public_affairs_ad_log.publishDate = $("#ad_log_PD_in").val();
            public_affairs_ad_log.contractStartDate = $("#ad_log_CDF_in").val();
            public_affairs_ad_log.contractEndDate = $("#ad_log_CDT_in").val();
            public_affairs_ad_log.officeId = $("#ad_log_O_in").val();
            public_affairs_ad_log.quantity = $("#ad_log_Q_in").val();
            public_affairs_ad_log.costingInfo = $("#ad_log_CI_in").val();
            public_affairs_ad_log.billPaymentDate = $("#ad_log_BPD_in").val();
            public_affairs_ad_log.contractStatus = $("#fa_in_RD").val();
            if($("#ad_log_T_Yes_in").is(':checked')){
                public_affairs_ad_log.contractStatus = $("#ad_log_T_Yes_in").val();
            }
            else{
                public_affairs_ad_log.contractStatus = $("#ad_log_T_No_in").val();
            }

            $.ajax({
                contentType: 'application/json',
                url: "add-public-affairs-ad-log",
                type: 'POST',
                async: false,
                data: JSON.stringify(public_affairs_ad_log),
                dataType: 'json',
                success: function (response) {
                    var last_row  = Number($('#advertisement_log_table tr:last td:first-child').text());

                    var tr = "<tr id ='" + response + "'>" +
                        "<td style='text-align: center; vertical-align: middle;'>" + (++last_row) + "</td>" +
                        "<td style='text-align: center; vertical-align: middle;'>" + $('#ad_log_MT_in option:selected').text() + "</td>" +
                        "<td style='text-align: center; vertical-align: middle;'>" + $('#ad_log_MC_in option:selected').text() + "</td>" +
                        "<td style='text-align: center; vertical-align: middle;'>" + $('#ad_log_PD_in').val() + "</td>" +
                        "<td style='text-align: center; vertical-align: middle;'>" + generateActionBtns(response) + "</td>"
                    "</tr>";
                    $("#ad_log_table_body").append(tr);
                    showAlert("Successfully saved");

                },
                error: function (xhr, status, error) {
                    alert("Something went wrong!");
                }
            });

        }else{
            status = true;
        }
    });

    $(document).on('click', '.view_details', function () {
        var val = $(this).attr('id');

       $.ajax({
           contentType: 'application/json',
           url: "get-public-affairs-ad-log-details",
           type: 'GET',
           async: false,
           data: {id: val},
           dataType: 'json',
           success: function (response) {
               //showing the ad log details body
               $("#ad_log_details").show();
               //animate the window dowm
               var n = $(document).height();
               $('html, body').animate({ scrollTop: n }, 'slow');

               //adding data into the details labels
               var date = new Date(response.publishDate);
               var conStart = new Date(response.contractStartDate);
               var conEnd = new Date(response.contractEndDate);
                $("#ad_log_media_type").text(response.mediaType.name);
                if(response.mediaCatId == '1'){
                    $("#ad_log_media_cat").text("National");
                }
                else{
                    $("#ad_log_media_cat").text("International");
                }
               $("#ad_log_purpose_of_ad").text(response.adPurpose);
               $("#ad_log_media_spe_name").text(response.mediaName);
               $("#ad_log_publish_date").text(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear());
               $("#ad_log_office").text(response.officeId.officeName);
               $("#ad_log_quantity_info").text(response.quantity);
               $("#ad_log_costing_info").text(response.costingInfo);
               $("#ad_log_contract_duration").text(conStart.getDate() + "/" + (conStart.getMonth()+1) + "/" + conStart.getFullYear() + " - " + conEnd.getDate() + "/" + (conEnd.getMonth()+1) + "/" + conEnd.getFullYear());
               if(response.contractStatus == 0){
                   $("#ad_log_comtract_status").text("Active");
               }
               else{
                   $("#ad_log_comtract_status").text("Inactive");
               }
           },
           error: function (xhr, status, error) {
               alert("Something went wrong!");
           }
       })
    });
});
