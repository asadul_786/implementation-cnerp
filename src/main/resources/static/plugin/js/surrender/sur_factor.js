/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#sur_fact_table").DataTable({

        "processing": true,
        "serverSide": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "surFactorList",
            "type": "GET",
            "dataType": "json"
        },
        "autoWidth": true,
        "columns": [
            { "data": "policyHolderAge", "name": "POLICY_HOLDER_AGE" },
            { "data": "policyTerm", "name": "POLICY_TERM" },
            { "data": "premiumPaidYear", "name": "PREMIUM_PAID_YEAR" },
            { "data": "surrenderRate", "name": "SURRENDER_RATE" },
            { "data": "calPara", "name": "CAL_PARA" },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="editBtn" class="btn btn-info btn-xs">Edit</button>';

                }
            }
        ]
    });

    $(document).on("click", "#save_sur_factor", function () {

        if(validate()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var surFact = {};
                        surFact.policyHolderAge = $("#p_h_age").val();
                        surFact.policyTerm = $("#pol_term").val();
                        surFact.premiumPaidYear = $("#p_paid_year").val();
                        surFact.surrenderRate = $("#sur_rate").val();
                        surFact.calPara = $("#cal_para").val();

                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveSurFac",
                            type: 'POST',
                            data: JSON.stringify(surFact),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                $("#save_sur_factor").text('Save');
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }

    });
    
    function setDisOrEn(val) {
        $("#p_h_age").prop('disabled', val);
        $("#pol_term").prop('disabled', val);
        $("#p_paid_year").prop('disabled', val);
    }

    $(document).on("click", "#editBtn", function () {

        setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#p_h_age").val(curRow.find('td:eq(0)').text());
        $("#pol_term").val(curRow.find('td:eq(1)').text());
        $("#p_paid_year").val(curRow.find('td:eq(2)').text());
        $("#sur_rate").val(curRow.find('td:eq(3)').text());
        $("#cal_para").val(curRow.find('td:eq(4)').text());

        $("#save_sur_factor").text('Update');

        $('html, body').animate({
            scrollTop: $('#scrlDiv').offset().top
        }, 500);

    });

    $(document).on("click", "#clear_btn", function () {

        $("#p_h_age").val('');
        $("#pol_term").val('');
        $("#p_paid_year").val('');
        $("#sur_rate").val('');
        $("#cal_para").val('');

        setDisOrEn(false);

        clrErr();
        $("#save_sur_factor").text('Save');

    });
    
    function clrErr() {

        $("#err_p_h_age").text('');
        $("#err_pol_term").text('');
        $("#err_p_paid_year").text('');
        $("#err_sur_rate").text('');
        $("#err_cal_para").text('');
        
    }

    $(document).on('input', '#surFactForm', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    $("input[type='search']").on("input", function (e) {
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    function validate() {

        clrErr();

        if($.trim($("#p_h_age").val()) == ""){
            $("#err_p_h_age").text("Required !!");
            return;
        }

        if($.trim($("#pol_term").val()) == ""){
            $("#err_pol_term").text("Required !!");
            return;
        }

        if($.trim($("#p_paid_year").val()) == ""){
            $("#err_p_paid_year").text("Required !!");
            return;
        }

        if($.trim($("#sur_rate").val()) == ""){
            $("#err_sur_rate").text("Required !!");
            return;
        }

        if($.trim($("#cal_para").val()) == ""){
            $("#err_cal_para").text("Required !!");
            return;
        }

        return true;

    }
    
});
