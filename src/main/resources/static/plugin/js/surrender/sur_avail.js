/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#sur_avail_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "surAvailList",
            "type": "GET",
            "dataType": "json"
        },
        "autoWidth": true,
        "columns": [
            { "data": "productCD", "name": "PRODUCT_CD" },
            { "data": "policyStatusCD", "name": "POLICY_STATUS" },
            { "data": "applTypeCD", "name": "APPLICANT_TYPE" },
            { "data": "productNM", "name": "PRODUCT_NM" },
            { "data": "statusNM", "name": "STATUS_NM" },
            { "data": "partyNM", "name": "PARTY_NM" },
            {
                "data": "status", "name": "STATUS",
                "render": function (data) {
                    return data == 1 ? "Active" : "Inactive";

                }
            },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="editBtn" class="btn btn-info btn-xs">Edit</button>';

                }
            }
        ]
    });

    $(document).on("click", "#save_sur_avail", function () {

        if(validate()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var surAvail = {};
                        surAvail.productCode = $("#p_cd").val();
                        surAvail.policyStatus = $("#p_status").val();
                        surAvail.applType = $("#appl_party").val();
                        surAvail.status = $("#status").val();

                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveSurAvail",
                            type: 'POST',
                            data: JSON.stringify(surAvail),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }

    });

    /*function setDisOrEn(val) {
     $("#dur_month").prop('disabled', val);
     }*/

    $(document).on("click", "#status", function () {
        $(this).val() == 1 ? $(this).val('0') : $(this).val('1');
    });

    $(document).on("click", "#editBtn", function () {

        //setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#p_cd").val(curRow.find('td:eq(0)').text());
        $("#p_status").val(curRow.find('td:eq(1)').text());
        $("#appl_party").val(curRow.find('td:eq(2)').text());
        if($.trim(curRow.find('td:eq(6)').text()) == "Active"){
            $("#status").prop("checked", true);
            $("#status").val('1');
        }
        else{
            $("#status").prop("checked", false);
            $("#status").val('0');
        }

        $("select").prop("disabled", true);

        $("#save_sur_avail").text('Update');

        $('html, body').animate({
            scrollTop: $('#scrlDiv').offset().top
        }, 500);

    });

    $(document).on("click", "#clear_btn", function () {
        $("select").prop("disabled", false);
        $("select").val('-1');
        clrErr();
        $("#save_sur_avail").text('Save');
    });

    function clrErr() {

        $("#err_p_cd").text("");
        $("#err_p_status").text("");
        $("#err_appl_party").text("");

    }

    $(document).on('input', '#surDiscFactForm', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    function validate() {

        clrErr();

        if($("#p_cd").val() == -1){
            $("#err_p_cd").text("Required !!");
            return;
        }

        if($("#p_status").val() == -1){
            $("#err_p_status").text("Required !!");
            return;
        }

        if($("#appl_party").val() == -1){
            $("#err_appl_party").text("Required !!");
            return;
        }

        return true;

    }

});