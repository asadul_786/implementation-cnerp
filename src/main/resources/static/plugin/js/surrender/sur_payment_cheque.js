/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#sur_pay_cheque_info_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "dummyCall",
            "type": "GET",
            "data": function ( d ) {
                d.officeCD = $("#office_cd").val(),
                    d.policyNo = $.trim($("#policy_no").val()),
                    d.dateFrom = $.trim($("#dt_from").val()),
                    d.dateTo = $.trim($("#dt_to").val())
            },
            "dataType": "json"
        },
        "initComplete": function(settings, json) {
            $("#sur_pay_cheque_info_table_processing").css({"top": "5px", "color": "green"});
        },
        "autoWidth": true,
        "columns": [
            // { "data": "pgId", "name": "PGID", className: "dt-hd" },
            { "data": "policyNo", "name": "POLICY_NO" },
            { "data": "payOrderNo", "name": "PAY_ORDER_NO" },
            { "data": "payOrderDate", "name": "PAY_ORDER_DATE" },
            { "data": "accNo", "name": "ACCOUNT_NO" },
            { "data": "officeNM", "name": "OFFICE_NAME" },
            { "data": "partyNM", "name": "LOAN_PARTY_NAME" },
            { "data": "bankNM", "name": "BANK_NM" },
            { "data": "brNM", "name": "BR_NM" },
            { "data": "netDisAmt", "name": "NET_DISBURS_AMOUNT" },
            { "data": "amtInWord", "name": "IN_WORD" },
            { "data": "disAccNo", "name": "DIS_ACCOUNT_NO" },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button type="button" id="show_rpt" class="btn btn-success">View</button>';
                }
            }
        ]
    });

    $('#sur_pay_cheque_info_table tbody').on("click", "#show_rpt", function () {

        var curRow = $(this).closest('tr');
        window.open('/surrender/generatedSurrenderPaymentCheque.pdf?policyNo=' + $.trim(curRow.find('td:eq(0)').text()), '_blank');

    });

    $(document).on("click", "#search_sur_pay_chk_info", function () {
        if(validate()){
            $("#err_dt_from").text("");
            table.ajax.url("chequeInfoList").load();
        }
    });

    $(document).on("change", "#dt_from", function (e) {
        fromDt = $.trim($('#dt_from').val());
        if(fromDt != ""){
            splitDt = fromDt.split("/");
            actFromDt = new Date(splitDt[2] + "-" + splitDt[1] + "-" + splitDt[0]);
            actToDt = actFromDt;
            actToDt.setMonth(actToDt.getMonth() + 2);
            $('#dt_to').val(actToDt.getDate() + "/" + (actToDt.getMonth() + 1) + "/" + actToDt.getFullYear());
        }
    });

    $(document).on("click", "#clear_btn", function () {
        $('#surPayChequeForm').trigger("reset");
        $("#err_dt_from").text("");
        $('#office_cd').val(-1).select2().trigger('change');
    });

    function validate() {

        if($.trim($("#dt_from").val()) == ""){
            $("#err_dt_from").text("Required !!");
            return;
        }

        return true;
    }

});