/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#ad_pay_fact_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "adPayFactList",
            "type": "GET",
            "dataType": "json"
        },
        "autoWidth": true,
        "columns": [
            { "data": "durationMon", "name": "DURATION_MONTH" },
            { "data": "discVal", "name": "DISC_VALUE" },
            { "data": "calPara", "name": "CAL_PARA" },
            { "data": "sheetNo", "name": "SHEET_NO" },
            {
                "data": "status", "name": "STATUS",
                "render": function (data) {
                    return data == 1 ? "Active" : "Inactive";

                }
            },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="editBtn" class="btn btn-info btn-xs">Edit</button>';

                }
            }
        ]
    });

    $(document).on("click", "#save_ad_pay_fact", function () {

        if(validate()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var surDiscVal = {};
                        surDiscVal.durationMon = $("#dur_month").val();
                        surDiscVal.discVal = $("#disc_val").val();
                        surDiscVal.calPara = $("#cal_para").val();
                        surDiscVal.sheetNo = $("#sheet_no").val();
                        surDiscVal.status = $("#status").val();

                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveAdPayFact",
                            type: 'POST',
                            data: JSON.stringify(surDiscVal),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }

    });

    function setDisOrEn(val) {
        $("#dur_month").prop('disabled', val);
    }

    $(document).on("click", "#status", function () {
        $(this).val() == 1 ? $(this).val('0') : $(this).val('1');
    });

    $(document).on("click", "#editBtn", function () {

        setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#dur_month").val(curRow.find('td:eq(0)').text());
        $("#disc_val").val(curRow.find('td:eq(1)').text());
        $("#cal_para").val(curRow.find('td:eq(2)').text());
        $("#sheet_no").val(curRow.find('td:eq(3)').text());
        if($.trim(curRow.find('td:eq(4)').text()) == "Active"){
            $("#status").prop("checked", true);
            $("#status").val('1');
        }
        else{
            $("#status").prop("checked", false);
            $("#status").val('0');
        }

        $("#save_ad_pay_fact").text('Update');

        $('html, body').animate({
            scrollTop: $('#scrlDiv').offset().top
        }, 500);

    });

    $(document).on("click", "#clear_btn", function () {

        $("#dur_month").val('');
        $("#disc_val").val('');
        $("#cal_para").val('');
        $("#sheet_no").val('');

        setDisOrEn(false);

        clrErr();
        $("#save_ad_pay_fact").text('Save');

    });

    function clrErr() {

        $("#err_dur_month").text('');
        $("#err_disc_val").text('');

    }

    $(document).on('input', '#adPayFactForm', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    $("input[type='search']").on("input", function (e) {
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    function validate() {

        clrErr();

        if($.trim($("#dur_month").val()) == ""){
            $("#err_dur_month").text("Required !!");
            return;
        }

        if($.trim($("#disc_val").val()) == ""){
            $("#err_disc_val").text("Required !!");
            return;
        }

        return true;

    }

});