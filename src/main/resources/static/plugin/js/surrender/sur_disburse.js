/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#sur_disburse_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "dummyCall",
            "type": "GET",
            "data": function ( d ) {
                d.disStatus = $("#status").val(),
                d.officeCD = $("#office_cd").val(),
                d.dtFrom = $("#dt_from").val(),
                d.dtTo = $("#dt_to").val(),
                d.policyNo = $.trim($("#policy_no").val())
            },
            "dataType": "json"
        },
        "initComplete": function(settings, json) {
            $("#sur_disburse_table_processing").css({"top": "5px", "color": "green"});
        },
        "autoWidth": true,
        "columns": [
            { "data": "policyNo", "name": "POLICY_NO" },
            { "data": "applSlNo", "name": "APPL_SL_NO" },
            { "data": "applDt", "name": "APPL_DT" },
            { "data": "approveDt", "name": "I_DT" },
            { "data": "surAmount", "name": "APPROVED_AMOUNT" },
            { "data": "chkAmount", "name": "NET_DISBURS_AMOUNT" },
            { "data": "chkWrtnDt", "name": "CHEQUE_WRITTEN_DT" },
            { "data": "chkNo", "name": "PAY_ORDER_NO" },
            {
                "className": "dt-btn",
                "render": function () {
                    if($("#status").val() == 1){
                        return '<input type="checkbox" id="chk" checked disabled>';
                    }
                    return '<input type="checkbox" id="chk">';
                }
            }
        ]
    });

    $(document).on("change", "#dt_from", function (e) {
        fromDt = $.trim($('#dt_from').val());
        if(fromDt != ""){
            splitDt = fromDt.split("/");
            actFromDt = new Date(splitDt[2] + "-" + splitDt[1] + "-" + splitDt[0]);
            actToDt = actFromDt;
            actToDt.setMonth(actToDt.getMonth() + 2);
            $('#dt_to').val(actToDt.getDate() + "/" + (actToDt.getMonth() + 1) + "/" + actToDt.getFullYear());
        }
        else {
            $('#dt_to').val('');
        }
    });

    $(document).on("click", "#processDisburse", function () {

        //console.log(disTransLst);

        mDisTransLst = [];
        mRowDltLst = [];

        for (var name in disTransLst) {
            mDisTransLst.push(name);
            mRowDltLst.push(disTransLst[name]);
        }

        if(mDisTransLst.length > 0){

            //console.log(mDisTransLst);
            //console.log(mRowDltLst);

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {

                        $.ajax({
                            contentType: 'application/json',
                            url:  "disburseTrans",
                            type: 'POST',
                            data: JSON.stringify(mDisTransLst),
                            //dataType: 'json',
                            success: function(response) {
                                showAlert(response + " application disbursed successfully");
                                disTransLst = [];
                                mRowDltLst.forEach(function (rowJ) {
                                    table.row(rowJ).remove();
                                });
                                table.draw();
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }
        else
            showAlert("No Application selected !!");

    });

   $(document).on("click", "#search_disburse_lst", function () {
       if(validate()){
           $("#err_dt_from").text("");
           table.ajax.url("surApprvList").load();
           $("#status").val() == 1 ? $("#processDisburse").prop("disabled", true) : $("#processDisburse").prop("disabled", false);
       }
   });

    var disTransLst = [];

    $("#sur_disburse_table tbody").on("click", "#chk", function () {

        var curRow = $(this).closest('tr');

        disTransLst[$.trim(curRow.find('td:eq(1)').text())] == undefined ?
            //disTransLst[$.trim(curRow.find('td:eq(1)').text())] = $.trim(curRow.find('td:eq(1)').text()) :
            disTransLst[$.trim(curRow.find('td:eq(1)').text())] = curRow :
            delete disTransLst[$.trim(curRow.find('td:eq(1)').text())];

        //console.log(disTransLst);
        //alert('Row index: ' + $(this).closest('tr').index());
    });

    $(document).on("click", "#clear_btn", function () {
        $('#surDisburseForm').trigger("reset");
        $("#err_dt_from").text("");
        $("#processDisburse").prop("disabled", false);
        table.ajax.reload();
    });

    function validate() {

        if($.trim($("#dt_from").val()) == ""){
            $("#err_dt_from").text("Required !!");
            return;
        }

        return true;
    }

});