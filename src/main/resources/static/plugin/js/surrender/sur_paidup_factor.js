/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#sur_p_fact_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "surPaidupFactList",
            "type": "GET",
            "dataType": "json"
        },
        "autoWidth": true,
        "columns": [
            { "data": "rowKey", "name": "ROWKEY", className: "dt-hd" },
            { "data": "ageEntry", "name": "AGE_ENTRY" },
            { "data": "ageMaturity", "name": "AGE_MATURITY" },
            { "data": "ageWeighted", "name": "AGE_WEIGHTED" },
            { "data": "paidupFactor", "name": "PAIDUP_FACTOR" },
            { "data": "calPara", "name": "CAL_PARA" },
            { "data": "sheetNo", "name": "SHEET_NO" },
            {
                "data": "status", "name": "STATUS",
                "render": function (data) {
                    return data == 1 ? "Active" : "Inactive";

                }
            },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="editBtn" class="btn btn-info btn-xs">Edit</button>';

                }
            }
        ]
    });

    $(document).on("click", "#save_sur_p_fact", function () {

        if(validate()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var surPaidFac = {};
                        surPaidFac.rowKey = $("#key").val();
                        surPaidFac.ageEntry = $("#age_entry").val();
                        surPaidFac.ageMaturity = $("#age_maturity").val();
                        surPaidFac.ageWeighted = $("#w_age").val();
                        surPaidFac.paidupFactor = $("#p_fact").val();
                        surPaidFac.calPara = $("#para_val").val();
                        surPaidFac.sheetNo = $("#sheet_no").val();
                        surPaidFac.status = $("#status").val();

                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveSurPaidupFact",
                            type: 'POST',
                            data: JSON.stringify(surPaidFac),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                $("#key").val('');
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }

    });

    /*function setDisOrEn(val) {
        $("#dur_month").prop('disabled', val);
    }*/

    $(document).on("click", "#status", function () {
        $(this).val() == 1 ? $(this).val('0') : $(this).val('1');
    });

    $(document).on("click", "#editBtn", function () {

        //setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#key").val(curRow.find('td:eq(0)').text());
        $("#age_entry").val(curRow.find('td:eq(1)').text());
        $("#age_maturity").val(curRow.find('td:eq(2)').text());
        $("#w_age").val(curRow.find('td:eq(3)').text());
        $("#p_fact").val(curRow.find('td:eq(4)').text());
        $("#para_val").val(curRow.find('td:eq(5)').text());
        $("#sheet_no").val(curRow.find('td:eq(6)').text());
        if($.trim(curRow.find('td:eq(7)').text()) == "Active"){
            $("#status").prop("checked", true);
            $("#status").val('1');
        }
        else{
            $("#status").prop("checked", false);
            $("#status").val('0');
        }

        $("#save_sur_p_fact").text('Update');

        $('html, body').animate({
            scrollTop: $('#scrlDiv').offset().top
        }, 500);

    });

    $(document).on("click", "#clear_btn", function () {
        $("#key").val('');
        $('#surPaidupFactForm input[type="text"]').val("");
        clrErr();
        $("#save_sur_p_fact").text('Save');
    });

    function clrErr() {

        $("#err_age_entry").text('');
        $("#err_age_maturity").text('');
        $("#err_w_age").text('');
        $("#err_p_fact").text('');

    }

    $(document).on('input', '#surPaidupFactForm', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    /*$("input[type='search']").on("input", function (e) {
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });*/

    function validate() {

        clrErr();

        if($.trim($("#age_entry").val()) == ""){
            $("#err_age_entry").text("Required !!");
            return;
        }

        if($.trim($("#age_maturity").val()) == ""){
            $("#err_age_maturity").text("Required !!");
            return;
        }

        if($.trim($("#w_age").val()) == ""){
            $("#err_w_age").text("Required !!");
            return;
        }

        if($.trim($("#p_fact").val()) == ""){
            $("#err_p_fact").text("Required !!");
            return;
        }

        return true;

    }

});