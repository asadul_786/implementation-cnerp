/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#sur_appl_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "surApplList",
            "type": "GET",
            "dataType": "json"
        },
        "autoWidth": true,
        "columns": [
         { "data": "riskDate", "name": "RISK_DATE" },
         { "data": "sumAssured", "name": "SUM_ASSURED" },
         { "data": "partyNM", "name": "PARTY_NAME" },
         // { "data": "proNM", "name": "PRODUCT_NM" },
         { "data": "optNM", "name": "OPTION_NM" },
         { "data": "polStatusNM", "name": "STATUS_NM" },
         { "data": "servOffNM", "name": "OFFICE_NAME" },
         // { "data": "remarks", "name": "REMARKS" },
         { "data": "applDate", "name": "APPL_DT" },
         { "data": "applSlNo", "name": "APPL_SL_NO" },
         { "data": "policyNo", "name": "POLICY_NO" },
         /*{
         "className": "dt-btn",
         "render": function () {
         return '<button id="editBtn" class="btn btn-info btn-xs">Edit</button>';

         }
         },*/
         {
         "className": "dt-btn",
         "render": function () {
         return '<button id="dltBtn" class="btn fa fa-trash-o" style="text-align:center;vertical-align: middle;font-size:20px;color:red;"></button>';

         }
         }
         ]
    });

    $(document).on("click", "#save_sur_appl", function () {

        if(validate()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var surAppl = {};
                        surAppl.applSlNo = $("#appl_sl").val();
                        surAppl.policyNo = $("#policy_no").val();
                        surAppl.tmpApplDt = $("#appl_date").val();
                        surAppl.servicingOff = $("#serv_off_cd").val();
                        surAppl.partyCD = $("#party_cd").val();
                        surAppl.remarks = $("#remarks").val();
                        //surAppl.status = $("#status").val();

                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveSurAppl",
                            type: 'POST',
                            data: JSON.stringify(surAppl),
                            //dataType: 'json',
                            success: function(response) {

                                if(response == "-1"){
                                    showAlertByType("Unknown error occured", "F");
                                }
                                else if(response == "-2")
                                    showAlertByType('Not applicable for surrender of this policy !!', "F");
                                else{
                                    showAlert("Applied Successfully");
                                    table.row.add({"riskDate": $("#risk_date").val(), "sumAssured": $("#sum_assured").val(), "partyNM": $("#appl_name").val(),
                                        "optNM": $("#option").val(), "polStatusNM": $("#pol_status").val(), "servOffNM": $("#serv_off_cd").val(),
                                        "applDate": $("#appl_date").val(), "applSlNo": response, "policyNo": $("#policy_no").val(),
                                        "button": '<button id="dltBtn" class="btn fa fa-trash-o" style="text-align:center;vertical-align: middle;font-size:20px;color:red;"></button>'
                                    }).draw( false );
                                }

                            },
                            error: function(xhr, status, error) {
                                showAlertByType("Something went wrong !!", "F");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }

    });

    $(document).on("input", "#policy_no", function (e) {


        if($("#policy_no").val().length == 10){

            var policy_no = $.trim($("#policy_no").val());
            var res = getSurrCount(policy_no);

            if(res > 0){

                $("#err_policy_no").text("");
                tmp = $.trim($("#policy_no").val());
                $("input").val("");
                $("#policy_no").val(tmp);
                $("#serv_off").val('-1');
                $("#appl_party").val('-1');

                $("#serv_off_cd").val('');
                $("#party_cd").val('');


                $.ajax({
                    contentType: 'application/json',
                    url:  "getPolInfo",
                    type: 'GET',
                    data: {
                        policyNo: $.trim($("#policy_no").val())
                    },
                    dataType: 'json',
                    success: function(response) {
                        response.proNM == null ? $("#err_policy_no").text("Policy not found !!") :
                            $("#appl_date").val(response.applDate);
                        if(response.applDate == null){
                            $("#appl_date").prop("disabled", false);
                            $("#save_sur_appl").prop("disabled", false);
                        }
                        else{
                            $("#appl_date").prop("disabled", true);
                            $("#save_sur_appl").prop("disabled", true);
                            showAlert("Already applied for surrender of this policy !!");
                        }
                        $("#appl_name").val(response.partyNM);
                        $("#pro_name").val(response.proNM);
                        $("#pol_status").val(response.polStatusNM);
                        $("#option").val(response.optNM);
                        $("#risk_date").val(response.riskDate);
                        $("#sum_assured").val(response.sumAssured);
                        $("#serv_off").val(response.servOffCD);
                        $("#appl_party").val(response.partyCD);
                        //response.status == null ? $("#status").val('-1') : $("#status").val(response.status);
                        $("#remarks").val(response.remarks);
                        $("#appl_sl").val(response.applSlNo);
                        $("#serv_off_cd").val(response.servOffCD);
                        $("#party_cd").val(response.partyCD);
                    },
                    error: function(xhr, status, error) {
                        showAlert("Something went wrong !!");
                    },
                    complete: function () {
                    }
                });
            }else{
                //$("#policy_no").val('')
                showAlert("Application Failed Due to Servicing Office Mismatch !!");
            }
        }else{
            $("#err_policy_no").text("Enter a valid Policy Number !!");
            tmp = $.trim($("#policy_no").val());
            $("input").val("");
            $("#serv_off").val('-1');
            $("#appl_party").val('-1');
            $("#policy_no").val(tmp);
        }
    });

    /*$(document).on("click", "#editBtn", function () {

        //setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#policy_no").val($.trim(curRow.find('td:eq(8)').text()));
        $("#policy_no").trigger("input");

        $("#surAppForm input").prop("disabled", true);

        $('html, body').animate({
            scrollTop: $('#scrlDiv').offset().top
        }, 500);

    });*/

    $("#sur_appl_table tbody").on("click", "#dltBtn", function () {

        //setDisOrEn(true);

        var curRow = $(this).closest('tr');

        $.confirm({
            title: 'Confirm',
            content: 'Are your sure?',
            buttons: {
                ok: function () {

                    $.ajax({
                        contentType: 'application/json',
                        url:  "dltSurAppl",
                        type: 'DELETE',
                        data: $.trim(curRow.find('td:eq(7)').text()),
                        dataType: 'json',
                        success: function(response) {
                            if(response == "1"){
                                showAlert("Deleted Successfully");
                                table.row( curRow ).remove().draw( false );
                            }
                            else
                                showAlert("Cannot be deleted, child record found !!");
                        },
                        error: function(xhr, status, error) {
                            showAlert("Something went wrong !!");
                        },
                        complete: function () {
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

    $(document).on("click", "#clear_btn", function () {
        $("#serv_off").val('-1');
        $("#appl_party").val('-1');
        /*$("input").val("");
        $("textarea").val("");*/
        $("#appl_sl").val('');
        $("#serv_off_cd").val('');
        $("#party_cd").val('');
        $("#appl_date").prop("disabled", false);
        $("#save_sur_appl").prop("disabled", false);
        $('#surAppForm').trigger("reset");

        clrErr();
    });

    function clrErr() {

        $(".rmv").text("");

    }

    /*$(document).on('input', '#surDiscFactForm', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });*/

    function validate() {

        clrErr();

        if($.trim($("#policy_no").val()) == ""){
            $("#err_policy_no").text("Required !!");
            return;
        }

        if($.trim($("#appl_date").val()) == ""){
            $("#err_appl_date").text("Required !!");
            return;
        }

        /*if($("#status").val() == -1){
            $("#err_status").text("Required !!");
            return;
        }*/

        return true;

    }

});


//Mohasin
//21-may-2020
function getSurrCount(policyNo){

    var keyValue = "";
    var json = {
        "policyNo": policyNo
    };

    $.ajax({
        async: false,
        type: 'POST',
        contentType: 'application/json',
        url: "/surrender/getSurrCount/"+policyNo,
        data: JSON.stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            keyValue = data;
        },
        error: function (e) {
        }
    });
    return keyValue;
}

