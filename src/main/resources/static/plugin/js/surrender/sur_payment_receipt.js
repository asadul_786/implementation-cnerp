/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    // var table = $("#sur_pay_receipt_info_table").DataTable({
    //
    //     "processing": true,
    //     "language": {
    //         "processing": "Processing... please wait"
    //     },
    //     "pageLength": 25,
    //     ajax: {
    //         "url": "dummyCall",
    //         "type": "GET",
    //         "data": function ( d ) {
    //
    //                 d.applSlNo = $.trim($("#appl_no").val())
    //
    //         },
    //         "dataType": "json"
    //     },
    //     "initComplete": function(settings, json) {
    //         $("#sur_pay_receipt_info_table_processing").css({"top": "5px", "color": "green"});
    //     },
    //     "autoWidth": true,
    //     "columns": [
    //         // { "data": "pgId", "name": "PGID", className: "dt-hd" },
    //         { "data": "applSlNo", "name": "APPL_SL_NO" },
    //         { "data": "policyNo", "name": "POLICY_NO" },
    //         // { "data": "officeCD", "name": "OFFICE_CD" },
    //         { "data": "officeNM", "name": "OFFICE_NAME" },
    //         // { "data": "partyCD", "name": "PARTY_CD" },
    //         { "data": "preparedBy", "name": "PREPARED_BY" },
    //         { "data": "checkedBy", "name": "CHECKED_BY" },
    //         { "data": "surValue", "name": "SURRENDER_VALUE" },
    //         { "data": "payBonusAmt", "name": "PAY_BONUS_AMT" },
    //         { "data": "netSurAmt", "name": "NET_SURRENDER_AMT" },
    //         {
    //             "className": "dt-btn",
    //             "render": function () {
    //                 return '<button type="button" id="show_rpt" class="btn btn-success">View</button>';
    //             }
    //         }
    //     ]
    // });

    $(document).on("input", "#appl_no", function (e) {

        $("#err_policy_no").text("");

        if($("#appl_no").val().length == 30){

            $.ajax({
                contentType: 'application/json',
                url:  "getApplSlNo",
                type: 'GET',
                data: {
                    policyNo: $("#appl_no").val()
                },
                //dataType: 'json',
                success: function(response) {
                    response == "" ? $("#err_policy_no").text("Policy not found !!") :
                        alert("gg")
                },
                error: function(xhr, status, error) {
                    showAlert("Something went wrong !!");
                },
                complete: function () {
                }
            });
        }
        else{
            $("#err_policy_no").text("Enter a valid Policy Number !!");
        }
    });

    $(document).on("click", "#search_sur_pay_receipt_info", function () {

        var curRow = $(this).closest('tr');
        window.open('/surrender/generatedSurrenderPaymentReceipt.pdf?applSlNo=' + $('#appl_no').val(), '_blank');

    });

    // $(document).on("click", "#search_sur_pay_receipt_info", function () {
    //     if(1){
    //         $("#err_appl_no").text("");
    //         table.ajax.url("receiptInfoList").load();
    //     }
    // });

    $(document).on("change", "#dt_from", function (e) {
        fromDt = $.trim($('#dt_from').val());
        if(fromDt != ""){
            splitDt = fromDt.split("/");
            actFromDt = new Date(splitDt[2] + "-" + splitDt[1] + "-" + splitDt[0]);
            actToDt = actFromDt;
            actToDt.setMonth(actToDt.getMonth() + 2);
            $('#dt_to').val(actToDt.getDate() + "/" + (actToDt.getMonth() + 1) + "/" + actToDt.getFullYear());
        }
    });

    $(document).on("click", "#clear_btn", function () {
        $('#surPayChequeForm').trigger("reset");
        $("#err_dt_from").text("");
        $('#office_cd').val(-1).select2().trigger('change');
    });

    function validate() {

        $("#err_dt_from").text("");
        $("#err_appl_no").text("");

        if($.trim($("#dt_from").val()) == ""){
            $("#err_dt_from").text("Required !!");
            return;
        }

        /*if($.trim($("#appl_no").val()) == ""){
            $("#err_appl_no").text("Required !!");
            return;
        }*/

        return true;
    }

});