/**
 * Created by Geetanjali Oishe on 8/7/2019.
 */
$(document).ready(function () {


    dateSetter();
    function dateSetter() {
        var now = new Date();

        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);

        var today = (day) + "/" +(month)+ "/" + now.getFullYear() ;

        $('#transfer-join-dt').val(today);
    }


    $(document).on("click", ".empl_join_select_button", function (event) {
        getReleaseDetail(this);
        // $('#modal-transfer-release-empl').modal();
        $('#release-emp-div').show();
        $('#join-info-div').show();

        // updateTransferTable($('#hidden-emp-id').val());

        $('html, body').animate({
            scrollTop: $('#release-emp-div').offset().top
        }, 1000)

    });

    function getReleaseDetail(identifier) {
        var row_id = $(identifier).attr("id");
        console.log("row_id: " + row_id);

        $.ajax({
            contentType: 'application/json',
            url: "get-employee-transfer-history",
            type: 'POST',
            async: false,
            data: JSON.stringify(row_id),
            dataType: 'json',
            success: function (response) {
                $('#release-employee-id-modal').val(response.empId);
                $('#release-employee-office-modal').val(response.officeName);
                $('#release-employee-dept-modal').val(response.deptName);
                $('#release-employee-joining-dt-modal').val(response.joiningDate);
                $('#release-employee-name-modal').val(response.employeeName);
                $('#release-employee-desig-modal').val(response.designationName);
                $('#release-employee-file-no-modal').val(response.fileNo);
                $('#release-employee-pay-scale-modal').val(response.payScale);

                $('#release-note').val(response.releaseNote);
                $('#transfer-release-dt').val(response.releaseDate);



                $('#hidden-transfer-id').val(response.id);
                $('#hidden-employee-id').val(response.hrmEmployeeId)
                $('#hidden-desig-id').val(response.desigId);
                $('#hidden-dept-id').val(response.deptId);
                $('#hidden-office-id').val(response.officeId);


                // $('#release-info-div').show();
                $('#release-emp-div').show();



                // if (edit == "edit") {
                //     if (response.status == "Released") {
                //         $('#transfer-release').prop("checked", true);
                //         $('#release-note').val(response.releaseNote);
                //         $('#transfer-release-dt').val(response.releaseDate);
                //         //file retrieve left////////////////////////////////
                //
                //         $('#release-info-edit-btn').show();
                //         $('#release-info-save-btn').hide();
                //     }
                //     else {
                //         $('#transfer-release').prop("checked", false);
                //     }
                // }



            },
            error: function (xhr, status, error) {
            }
        });
    }

    $('#join-info-save-btn').click(function (event) {
        if (checkJoinedValidation()) {
            confirmEmpJoinDialog("Are you sure to save joining info?");
        }
    });

    function checkJoinedValidation() {
        return true;
    }

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        dateSetter();

                        var joinRemarks = $('#join-remarks').val();
                        var joiningDate = $('#transfer-join-dt').val();
                        var transferId = $('#hidden-transfer-id').val();
                        var hrmEmployeeId = $('#hidden-employee-id').val();
                        var officeId = $('#hidden-office-id').val();
                        var designationId = $('#hidden-desig-id').val();
                        var departmentId = $('#hidden-dept-id').val();

                        var employeeTransfer = {};

                        employeeTransfer.joinRemarks = joinRemarks;
                        employeeTransfer.joiningDate = joiningDate;
                        employeeTransfer.id = transferId;
                        employeeTransfer.officeId = officeId;
                        employeeTransfer.desigId = designationId;
                        employeeTransfer.deptId = departmentId;
                        employeeTransfer.hrmEmployeeId = hrmEmployeeId;

                        $.ajax({
                            contentType: 'application/json',
                            url: "save-employee-join-info",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(employeeTransfer),
                            dataType: 'json',
                            success: function (response) {


                                // updateTransferRequestTable();
                                // updateTransferReleaseTable();
                                clearForm();
                                $('#release-emp-div').hide();

                                showAlertByType("Successfully added", "S");
                            },
                            error: function (xhr, status, error) {
                                showAlertByType("Failed to add", "F");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };

    $('#join-filterBtn').click(function (event) {
        event.preventDefault();
        var isVaild = validateJoinEmplSearch();
        if(isVaild) {
            updateTransferReleaseTable();
        }
    });

    function validateJoinEmplSearch() {
        $(".err_msg").text("");

        if ($('#release-pay-level').find('option:selected').val() == -1 ) {
            var parent = $('#release-pay-scale-grade').find('option:selected').val();
            if (parent != -1){
                $('#release-pay-level').after('<span class="error err_msg">Select pay level</span>');
                return false;
            }
        }

        else {
            return true;
        }

        if ($('#release-office').find('option:selected').val() == -1 ) {
            var parent = $('#release-office-category').find('option:selected').val();
            if (parent != -1) {
                $('#release-office').after('<span class="error err_msg">Select office</span>');
                return false;
            }
        }

        else {
            return true;
        }

        return true;
    }

    function updateTransferReleaseTable() {
        $.ajax({
            contentType: 'application/json',
            url: "get-transfer-released",
            type: 'GET',
            async: false,
            dataType: 'json',
            success: function(res){
                var table=$('#released-tbl').dataTable({
                    "lengthChange": false,
                    "searching": false,
                    data: res,
                    columns:[
                        {
                            "data": "id",
                            'visible' : false
                        },
                        { "data": "empId" },
                        { "data": "employeeName" },
                        { "data": "designationName" },
                        { "data": "officeName" },
                        { "data": "deptName" },
                        // { "data": "releaseDate" },
                        {
                            "data": "id",
                            render:function(data, type, row) {
                                var v = row.id;
                                v = '<button type="button" class="btn btn-info empl_join_select_button" value="Select" id="' +
                                    v + '"> <i class="fa fa-check"></i></button>';
                                return v;
                            }
                        }
                    ],
                    bDestroy : true,
                    iDisplayLength : 15,
                    columnDefs: [
                        {
                            className: "dt-center"
                        }
                    ]
                });

                table.column(1).visible(false);
            }
        });
    }

});