$(document).ready(function () {

    $('#postingdataTable').DataTable();


    $("#btnupdatepostingInfo").hide();


    $("#btnupdatepostingInfo").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var empId = $('#empId').val();
            var empgid = $('#empgid').val();
            var empName = $('#empName').val();
            var officeName = $('#officeName').val();
            var empDesignation = $('#empDesignation').val();
            var empDepartment = $('#empDepartment').val();
//transfer info
            var authorityCd = $('#authorityCd').val();
            var officeCode = $('#officeCode').val();
            var releaseType = $('#releaseType').val();
            var OfficeOrderNo = $('#OfficeOrderNo').val();
            var departmentName = $('#departmentName').val();
            var officeorderDate = $('#officeorderDate').val();
            //release type
            var referenceNo = $('#referenceNo').val();
            var releaseDate = $('#releaseDate').val();
            var accountClearness = $('#accountClearness').val();
            //joing info
            var joiningDate = $('#joiningDate').val();
            var remarks = $('#remarks').val();
            var updateempgid = $('#updateempgid').val();

            var postingInfo = {};

            postingInfo.empId = empId;
            postingInfo.empgid = empgid;
            postingInfo.empName = empName;
            postingInfo.officeName = officeName;
            postingInfo.empDesignation = empDesignation;
            postingInfo.empDepartment = empDepartment;
            //transfser
            postingInfo.authorityCd = authorityCd;
            postingInfo.officeCode = officeCode;
            postingInfo.OfficeOrderNo = OfficeOrderNo;
            postingInfo.releaseType = releaseType;
            postingInfo.departmentName = departmentName;
            postingInfo.officeorderDate = officeorderDate;
//release type
            postingInfo.referenceNo = referenceNo;
            postingInfo.releaseDate = releaseDate;
            postingInfo.accountClearness = accountClearness;
            //joing info
            postingInfo.joiningDate = joiningDate;
            postingInfo.remarks = remarks;
            postingInfo.updateempgid = updateempgid;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/UpdteapprovalPostingInfo",
                data: JSON.stringify(postingInfo),
                dataType: 'json',
                success: function (data) {
                    showAlertByType("Joing accepted and employee data updated!!","S");
                    clearForm();
                    setInterval(function () {
                        location.reload(true);
                    }, 2000);
                },
                error: function (e) {
                    showAlertByType("Sorry,Something Wrong!!","F");
                }
            });

        }

    });


    $(document).on("input", "#empId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var empId = $('#empId').val();
        $('#updateempgid').val("");


        if (empId != "" && empId.length < 9) {
            $.ajax({
                url: "/hrm-admin/getEmployeeDetails/" + empId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    //
                    var value_6 = "";
                    var value_7 = "";
                    var value_8 = "";

                    $.each(response, function (i, l) {
                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        //
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];

                    });
                    $("#empgid").val(value_1);
                    $("#empName").val(value_2);
                    $("#officeName").val(value_3);
                    $("#empDesignation").val(value_4);
                    $("#empDepartment").val(value_5);
                    $("#err_empID").text("");
                    //
                    $("#vofficeName").val(value_6);
                    $("#vempDesignation").val(value_7);
                    $("#vempDepartment").val(value_8);


                },
                error: function (xhr, status, error) {
                    $("#empgid").val("");
                    $("#empName").val("");
                    $("#officeName").val("");
                    $("#empDesignation").val("");
                    $("#empDepartment").val("");
                    $("#err_empID").text("Sorry! Employee ID not match!!");
                    //
                    $("#vofficeName").val("");
                    $("#vempDesignation").val("");
                    $("#vempDepartment").val("");
                }
            });
        }
        else {

            if ($('#empId').val() == "") {
                $("#vofficeName").val("");
                $("#vempDesignation").val("");
                $("#vempDepartment").val("");
                $("#err_empID").text("Employee ID required!");
                $("#empgid").val("");
                $("#empName").val("");
                $("#officeName").val("");
                $("#empDesignation").val("");
                $("#empDepartment").val("");
            }
            else {
                $("#err_empID").text("ID maximum 8 characters!");
            }
        }

    });


    function dataValidation() {


        var status = true;


        if ($("#empId").val() == "") {
            status = false;
            $("#err_empID").text("Empty field found!!");
            $("#empId").focus();
        } else $("#err_empID").text("");


        if ($("#authorityCd").val() == -1) {
            status = false;
            $("#err_authorityCd").text("Empty field found!!");
            $("#authorityCd").focus();
        } else $("#err_authorityCd").text("");

        if ($("#officeCode").val() == -1) {
            status = false;
            $("#err_officeCode").text("Empty field found!!");
            $("#officeCode").focus();
        }
        else $("#err_officeCode").text("");

        if ($("#departmentName").val() == -1) {
            status = false;
            $("#err_departmentName").text("Empty field found!!");
            $("#departmentName").focus();
        }
        else $("#err_departmentName").text("");

        if ($("#officeorderDate").val() == "") {
            status = false;
            $("#err_officeorderDate").text("Empty field found!!");
            $("#officeorderDate").focus();
        }
        else $("#err_officeorderDate").text("");


        if ($("#accountClearness").val() == -1) {
            status = false;
            $("#err_accountClearness").text("Empty field found!!");
            $("#accountClearness").focus();
        }
        else $("#err_accountClearness").text("");


        if ($("#releaseType").val() == -1) {
            status = false;
            $("#err_releaseType").text("Empty field found!!");
            $("#releaseType").focus();
        }
        else $("#err_releaseType").text("");

        return status;
    }

    function clearForm() {

        $("#empgid").val("");
        $("#empId").val("");
        $("#empName").val("");
        $("#officeName").val("");
        $("#empDesignation").val("");
        $("#empDepartment").val("");
        $("#authorityCd").val("-1");
        $("#officeCode").val("-1");
        $("#releaseType").val("-1");
        $("#OfficeOrderNo").val("");
        $("#departmentName").val("");
        $("#officeorderDate").val("");
        $("#referenceNo").val("");
        $("#releaseDate").val("");
        $("#accountClearness").val("-1");
        $("#joiningDate").val("");
        $("#remarks").val("");
        $("#updateempgid").val("");
        //
        $("#vofficeName").val("");
        $("#vempDesignation").val("");
        $("#vempDepartment").val("");
    }


    $('#postingdataTable tbody').on('click', '#editpostingInfo', function () {


        $("#btnupdatepostingInfo").show();

        var curRow = $(this).closest('tr');
        var col11 = curRow.find('td:eq(13)').text();

        var col1 = curRow.find('td:eq(14)').text();
        var col2 = curRow.find('td:eq(15)').text();
        var col3 = curRow.find('td:eq(16)').text();
        var col4 = curRow.find('td:eq(17)').text();
        var col5 = curRow.find('td:eq(18)').text();

        var col6 = curRow.find('td:eq(19)').text();
        var col7 = curRow.find('td:eq(20)').text();
        var col8 = curRow.find('td:eq(21)').text();
        var col9 = curRow.find('td:eq(22)').text();
        var col10 = curRow.find('td:eq(23)').text();
        var col12 = curRow.find('td:eq(24)').text();

        $('#updateempgid').val(col12);

        $('#officeCode').val(col1);
        $('#authorityCd').val(col2);
        $('#officeorderDate').val(col3);
        $('#departmentName').val(col4);
        $('#releaseType').val(col5);

        //new
        $('#referenceNo').val(col6);
        $('#accountClearness').val(col7);
        $('#releaseDate').val(col8);
        $('#joiningDate').val(col9);
        $('#remarks').val(col10);

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

        getDateFor();

        $.ajax({
            url: "/hrm-admin/fetchEmployeeId/" + col11,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $("#empId").val(response);

                $.ajax({
                    url: "/hrm-admin/getEmployeeDetails/" + response,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var value_1 = "";
                        var value_2 = "";
                        var value_3 = "";
                        var value_4 = "";
                        var value_5 = "";
                        //
                        var value_6 = "";
                        var value_7 = "";
                        var value_8 = "";

                        $.each(response, function (i, l) {
                            value_1 = l[0];
                            value_2 = l[1];
                            value_3 = l[2];
                            value_4 = l[3];
                            value_5 = l[4];
                            //
                            value_6 = l[5];
                            value_7 = l[6];
                            value_8 = l[7];
                        });
                        $("#empgid").val(value_1);
                        $("#empName").val(value_2);
                        $("#officeName").val(value_3);
                        $("#empDesignation").val(value_4);
                        $("#empDepartment").val(value_5);
                        $("#err_empID").text("");
                        $("#vofficeName").val(value_6);
                        $("#vempDesignation").val(value_7);
                        $("#vempDepartment").val(value_8);
                    },
                    error: function (xhr, status, error) {
                        $("#empgid").val("");
                        $("#empName").val("");
                        $("#officeName").val("");
                        $("#empDesignation").val("");
                        $("#empDepartment").val("");
                        $("#err_empID").text("Sorry! Employee ID not match!!");
                        $("#vofficeName").val("");
                        $("#vempDesignation").val("");
                        $("#vempDepartment").val("");
                    }
                });
            },
            error: function (xhr, status, error) {
                showAlertByType("Sorry,Something Wrong!!","F");
            }
        });
    });

    function getDateFor() {
        var dateStr = $('#officeorderDate').val();
        var dateCom = getDate(dateStr);
        $('#officeorderDate').val(dateCom);

//
        var dateStr1 = $('#releaseDate').val();
        var dateCom1 = getDateReleaseDate(dateStr1);
        $('#releaseDate').val(dateCom1);
//
        var dateStr2 = $('#joiningDate').val();
        var dateCom2 = getDateJoiningDate(dateStr2);
        $('#joiningDate').val(dateCom2);

    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = month + "/" + day + "/" + year;

        return date;
    }

//release date
    function getDateReleaseDate(dateObject) {

        if (dateObject != "") {

            var d = new Date(dateObject);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = month + "/" + day + "/" + year;

            return date;
        }

    }

    function getDateJoiningDate(dateObject) {

        if (dateObject != "") {

            var d = new Date(dateObject);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = month + "/" + day + "/" + year;
            return date;
        }
    }

});
