/**
 * Created by golam nobi.
 */

$(document).ready(function () {

    $(document).on("change", "#mail_division_cd", function (e) {

        var divisionCD = $("#mail_division_cd option:selected").val();

        $.get("/hrm-admin/get-districtInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#mail_district_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select District---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });

    $(document).on("change", "#mail_district_cd", function (e) {

        var divisionCD = $("#mail_district_cd option:selected").val();

        $.get("/hrm-admin/get-thanaInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#mail_thana_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Thana---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });


    $(document).on("change", "#perm_division_cd", function (e) {

        var divisionCD = $("#perm_division_cd option:selected").val();

        $.get("/hrm-admin/get-districtInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#perm_district_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select District---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });

    $(document).on("change", "#perm_district_cd", function (e) {

        var divisionCD = $("#perm_district_cd option:selected").val();

        $.get("/hrm-admin/get-thanaInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#perm_thana_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Thana---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });


    var setPreDisabled = function () {
        var val_checked = $('input[name=sameAs]:checked').val();
        if (val_checked == 'on') {
            getMailingAddress();
        }else{
            removeMailingAddress();
        }
    };

    // setPreDisabled();

    $(document).on("click", ".same_as_class", function (event) {
        setPreDisabled();
    });

    $("#btnadd_Address").click(function () {

        var val_checked = $('input[name=sameAs]:checked').val();

        if (val_checked == 'on') {

            var flag_yes = dataValidationTrue();

            if (flag_yes == true) {

                var mailaddressOne = $('#mailaddressOne').val();
                var mail_phone = $('#mail_phone').val();
                var mail_zip_cd = $('#mail_zip_cd').val();
                var mail_thana_cd = $('#mail_thana_cd option:selected').val();
                var mail_district_cd = $('#mail_district_cd option:selected').val();
                var countrycd = $('#countrycd option:selected').val();
                var mail_division_cd = $('#mail_division_cd option:selected').val();

                var address = {};
                address.mailaddressOne = mailaddressOne;
                address.mail_phone = mail_phone;
                address.mail_zip_cd = mail_zip_cd;
                address.mail_thana_cd = mail_thana_cd;
                address.mail_district_cd = mail_district_cd;
                address.countrycd = countrycd;
                address.mail_division_cd = mail_division_cd;

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/hrm-admin/addAddressInfo",
                    data: JSON.stringify(address),
                    dataType: 'json',
                    success: function (data) {
                        showAlertByType('Save successfully!!', "S");
                    },
                    error: function (e) {
                        showAlertByType('Something Wrong!', "F");
                    }
                });
            }


        } else {

            var flag_false = dataValidationFalse();

            if (flag_false == true) {

                var mailaddressOne = $('#mailaddressOne').val();
                var mail_phone = $('#mail_phone').val();
                var mail_zip_cd = $('#mail_zip_cd').val();
                var mail_thana_cd = $('#mail_thana_cd option:selected').val();
                var mail_district_cd = $('#mail_district_cd option:selected').val();
                var countrycd = $('#countrycd option:selected').val();
                var mail_division_cd = $('#mail_division_cd option:selected').val();
                //get data from parmanet address
                var perm_addrOne = $('#perm_addrOne').val();
                var perm_mobile = $('#perm_mobile').val();
                var perm_zip_cd = $('#perm_zip_cd').val();
                var perm_thana_cd = $('#perm_thana_cd option:selected').val();
                var perm_district_cd = $('#perm_district_cd option:selected').val();
                var countrycd = $('#countrycd option:selected').val();
                var perm_division_cd = $('#perm_division_cd option:selected').val();


                var address = {};
                address.mailaddressOne = mailaddressOne;
                address.mail_phone = mail_phone;
                address.mail_zip_cd = mail_zip_cd;
                address.mail_thana_cd = mail_thana_cd;
                address.mail_district_cd = mail_district_cd;
                address.countrycd = countrycd;
                address.mail_division_cd = mail_division_cd;
                address.perm_addrOne = perm_addrOne;//for parmanet address
                address.perm_mobile = perm_mobile;
                address.perm_zip_cd = perm_zip_cd;
                address.perm_thana_cd = perm_thana_cd;
                address.perm_district_cd = perm_district_cd;
                address.countrycd = countrycd;
                address.perm_division_cd = perm_division_cd;

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/hrm-admin/addAddressInfo",
                    data: JSON.stringify(address),
                    dataType: 'json',
                    success: function (data) {
                        showAlertByType('Save successfully!!', "S");
                        //clearform();
                    },
                    error: function (e) {
                        showAlertByType('Something Wrong!', "F");
                    }
                });
            }
        }
    });


    function dataValidationTrue() {

        var status_yes = true;
        var regexMobile = /^(?:\+?88)?01[1,2,3,5,6,7,8,9]{1}[0-9]{8}$/;

        if ($('#mail_division_cd option:selected').val() == -1) {
            status_yes = false;
            $("#error_mail_division_cd").text("Empty field found!!");
            $("#mail_division_cd").focus();
        } else $("#error_mail_division_cd").text("");


        if ($("#mailaddressOne").val() == "") {
            status_yes = false;
            $("#error_MAIL_ADDR1").text("Empty field found!!");
            $("#mailaddressOne").focus();
        } else $("#error_MAIL_ADDR1").text("");

        if ($("#mail_thana_cd").val() == -1) {
            status_yes = false;
            $("#error_MAIL_THANA_CD").text("Empty field found!!");
            $("#mail_thana_cd").focus();
        } else $("#error_MAIL_THANA_CD").text("");

        if ($("#mail_phone").val() != "") {
            if (!regexMobile.test($("#mail_phone").val())) {
                status_yes = false;
                $("#error_mail_phone").text("Invalid contact number!!");
                $("#mail_phone").focus();
            } else $("#error_mail_phone").text("");

        } else if (/[^0-9]/g.test($("#mail_phone").val()) == true) {
            status_yes = false;
            $("#error_mail_phone").text("Sorry!!");
            $("#mail_phone").focus();
        } else $("#error_mail_phone").text("");


        if ($("#mail_district_cd").val() == -1) {
            status_yes = false;
            $("#error_MAIL_DISTRICT_CD").text("Empty field found!!");
            $("#mail_district_cd").focus();
        } else $("#error_MAIL_DISTRICT_CD").text("");


        if ($("#mail_zip_cd").val() != "") {
            if ($("#mail_zip_cd").val().length != 4) {
                status_yes = false;
                $("#error_mail_zip_cd").text("Allow maximum 4 character!!");
                $("#mail_zip_cd").focus();
            } else $("#error_mail_zip_cd").text("");
        } else $("#error_mail_zip_cd").text("");

        if ($("#countrycd").val() == -1) {
            status_yes = false;
            $("#error_mail_country").text("Empty field found!!");
            $("#countrycd").focus();
        } else $("#error_mail_country").text("");

        if ($("#map_country").val() == -1) {
            status_yes = false;
            $("#err_map_country").text("Empty field found!!");
            $("#map_country").focus();
        } else $("#err_map_country").text("");


        if ($("#perm_zip_cd").val() != "") {

            if ($("#perm_zip_cd").val().length != 4) {
                status_no = false;
                $("#err_perm_zip_cd").text("Allow maximum 4 character!!");
                $("#perm_zip_cd").focus();
            } else $("#err_perm_zip_cd").text("");

        } else  $("#err_perm_zip_cd").text("");


        if ($("#perm_mobile").val() != "") {

            if (!regexMobile.test($("#perm_mobile").val())) {
                status_no = false;
                $("#err_perm_mobile").text("Invalid contact number!!");
                $("#perm_mobile").focus();
            } else $("#err_perm_mobile").text("");

        } else if (/[^0-9]/g.test($("#perm_mobile").val()) == true) {
            status_no = false;
            $("#err_perm_mobile").text("Invalid contact number!!");
            $("#perm_mobile").focus();
        } else $("#err_perm_mobile").text("");

        return status_yes;
    }


    function dataValidationFalse() {

        var status_no = true;
        var regexMobile = /^(?:\+?88)?01[1,2,3,5,6,7,8,9]{1}[0-9]{8}$/;

        if ($("#mailaddressOne").val() == "") {
            status_no = false;
            $("#error_MAIL_ADDR1").text("Empty field found!!");
            $("#mailaddressOne").focus();
        } else $("#error_MAIL_ADDR1").text("");

        if ($("#mail_division_cd").val() == -1) {
            status_no = false;
            $("#error_mail_division_cd").text("Empty field found!!");
            $("#mail_division_cd").focus();
        } else $("#error_mail_division_cd").text("");

        if ($("#mail_thana_cd").val() == -1) {
            status_no = false;
            $("#error_MAIL_THANA_CD").text("Empty field found!!");
            $("#mail_thana_cd").focus();
        } else $("#error_MAIL_THANA_CD").text("");


        if ($("#mail_phone").val() != "") {
            if (!regexMobile.test($("#mail_phone").val())) {
                status_no = false;
                $("#error_mail_phone").text("Invalid contact number!!");
                $("#mail_phone").focus();
            } else $("#error_mail_phone").text("");

        } else if (/[^0-9]/g.test($("#mail_phone").val()) == true) {
            status_no = false;
            $("#error_mail_phone").text("Sorry!!");
            $("#mail_phone").focus();
        } else $("#error_mail_phone").text("");


        if ($("#perm_mobile").val() != "") {

            if (!regexMobile.test($("#perm_mobile").val())) {
                status_no = false;
                $("#err_perm_mobile").text("Invalid contact number!!");
                $("#perm_mobile").focus();
            } else $("#err_perm_mobile").text("");

        } else if (/[^0-9]/g.test($("#perm_mobile").val()) == true) {
            status_no = false;
            $("#err_perm_mobile").text("Invalid contact number!!");
            $("#perm_mobile").focus();
        } else $("#err_perm_mobile").text("");


        if ($("#mail_district_cd").val() == -1) {
            status_no = false;
            $("#error_MAIL_DISTRICT_CD").text("Empty field found!!");
            $("#mail_district_cd").focus();
        } else $("#error_MAIL_DISTRICT_CD").text("");


        if ($("#mail_zip_cd").val() != "") {
            if ($("#mail_zip_cd").val().length != 4) {
                status_no = false;
                $("#error_mail_zip_cd").text("Allow maximum 4 character!!");
                $("#mail_zip_cd").focus();
            } else $("#error_mail_zip_cd").text("");
        } else $("#error_mail_zip_cd").text("");


        if ($("#mail_country").val() == -1) {
            status_no = false;
            $("#error_mail_country").text("Empty field found!!");
            $("#mail_country").focus();
        } else $("#error_mail_country").text("");

//for permanent address

        if ($("#perm_addrOne").val() == "") {
            status_no = false;
            $("#err_PERM_ADDR1").text("Empty field found!!");
            $("#perm_addrOne").focus();
        } else $("#err_PERM_ADDR1").text("");

        if ($("#perm_division_cd").val() == -1) {
            status_no = false;
            $("#err_PERM_DIVISION_CD").text("Empty field found!!");
            $("#perm_division_cd").focus();
        } else $("#err_PERM_DIVISION_CD").text("");


        if ($("#perm_thana_cd").val() == -1) {
            status_no = false;
            $("#err_PERM_THANA_CD").text("Empty field found!!");
            $("#perm_thana_cd").focus();
        } else $("#err_PERM_THANA_CD").text("");


        if ($("#perm_district_cd").val() == -1) {
            status_no = false;
            $("#err_PERM_DISTRICT_CD").text("Invalid contact number!!");
            $("#perm_district_cd").focus();
        } else $("#err_PERM_DISTRICT_CD").text("");


        // if ($("#perm_zip_cd").val() == "") {
        //     status_no = false;
        //     $("#err_perm_zip_cd").text("Invalid contact number!!");
        //     $("#perm_zip_cd").focus();
        // } else $("#err_perm_zip_cd").text("");


        if ($("#perm_zip_cd").val() != "") {

            if ($("#perm_zip_cd").val().length != 4) {
                status_no = false;
                $("#err_perm_zip_cd").text("Allow maximum 4 character!!");
                $("#perm_zip_cd").focus();
            } else $("#err_perm_zip_cd").text("");

        } else  $("#err_perm_zip_cd").text("");

        if ($("#countrycd").val() == -1) {
            status_no = false;
            $("#err_COUNTRY_CD").text("Invalid contact number!!");
            $("#countrycd").focus();
        } else $("#err_COUNTRY_CD").text("");

        if ($("#countrycd").val() == -1) {
            status_yes = false;
            $("#error_mail_country").text("Empty field found!!");
            $("#countrycd").focus();
        } else $("#error_mail_country").text("");

        if ($("#map_country").val() == -1) {
            status_yes = false;
            $("#err_map_country").text("Empty field found!!");
            $("#map_country").focus();
        } else $("#err_map_country").text("");

        return status_no;
    }

    $("#btn_Refresh").click(function () {
        clearform();
    });

    function clearform() {
        $('#mail_phone').val("");
        $('#mail_zip_cd').val("");
        $('#mail_country').val('-1');
        $('#mail_division_cd').val('-1');
        //for permanet address clear
        $('#perm_addrOne').val("");
        $('#perm_mobile').val("");
        $('#perm_zip_cd').val("");
        $('#countrycd').val('001');
        $('#perm_division_cd').val('-1');
        $('#mailaddressOne').val('');
        $('#map_country').val('-1');
        //for yes
        $("#error_mail_country").text("");
        $("#error_mail_zip_cd").text("");
        $("#error_MAIL_DISTRICT_CD").text("");
        $("#error_mail_phone").text("");
        $("#error_MAIL_THANA_CD").text("");
        $("#error_MAIL_ADDR1").text("");
        $("#error_mail_division_cd").text("");
        //
        $("#err_PERM_ADDR1").text("");
        $("#err_PERM_DIVISION_CD").text("");
        $("#err_PERM_THANA_CD").text("");
        $("#err_PERM_DISTRICT_CD").text("");
        $("#err_perm_zip_cd").text("");
        $("#err_perm_zip_cd").text("");
        $("#err_COUNTRY_CD").text("");
        $("#err_map_country").text("");
        $("#err_perm_mobile").text("");

        $('#perm_thana_cd').empty();
        $('#perm_district_cd').empty();
        $('#mail_thana_cd').empty();
        $('#mail_district_cd').empty();

    }

    function getMailingAddress() {

        var getData = $('#mail_division_cd option:selected').val();

        //get data from parmanet address
        $('#perm_addrOne').val($('#mailaddressOne').val());
        $('#perm_mobile').val($('#mail_phone').val());
        $('#perm_zip_cd').val($('#mail_zip_cd').val());

        var permanetThana = $('#perm_thana_cd');
        permanetThana.empty();
        permanetThana.append($('<option/>', {
            value: $('#mail_thana_cd option:selected').val(),
            text: $('#mail_thana_cd option:selected').text()
        }));

        var permanetDistrict = $('#perm_district_cd');
        permanetDistrict.empty();
        permanetDistrict.append($('<option/>', {
            value: $('#mail_district_cd option:selected').val(),
            text: $('#mail_district_cd option:selected').text()
        }));

        $('#perm_division_cd').val(getData);
    }

    function removeMailingAddress() {
        $('#perm_addrOne').val("");
        $('#perm_mobile').val("");
        $('#perm_zip_cd').val("");
        $('#perm_division_cd').val("-1");

        var permanetThana = $('#perm_thana_cd');
        permanetThana.empty();
        permanetThana.append($('<option/>', {
            value: -1,
            text: "--Select Thana--"
        }));

        var permanetDistrict = $('#perm_district_cd');
        permanetDistrict.empty();
        permanetDistrict.append($('<option/>', {
            value: -1,
            text: "--Select District--"
        }));
    }

});