/**
 * Created by golam.
 */

$(document).ready(function () {

    if ($("#emp_master_id").val() != "") {

        spouse_loadTable();
        child_loadTable();

        $("#emp_save_spouse").click(function () {

            var flag = dataValidation();

            if (flag == true) {
                var spouse_nm = $('#spouse_nm').val();
                var birth_dt = $('#birth_dt').val();
                var occup_cd = $('#occup_cd option:selected').val();
                var designation = $('#designation').val();
                var organizations = $('#organizations').val();
                var spouse_gid = $('#spouse_gid').val();
                var spousesl_no = $('#spousesl_no').val();
                var spousei_usr = $('#spousei_usr').val();
                var spousei_dt = $('#spousei_dt').val();

                if (birth_dt) {
                    birth_dt = birth_dt.split("/").reverse().join("/");
                    birth_dt = getFormateDate(birth_dt);
                }

                var spouse = {};
                spouse.spouse_nm = spouse_nm;
                spouse.birth_dt = birth_dt;
                spouse.occup_cd = occup_cd;
                spouse.designation = designation;
                spouse.organizations = organizations;
                spouse.spouse_gid = spouse_gid;
                spouse.spousesl_no = spousesl_no;
                spouse.spousei_usr = spousei_usr;
                spouse.spousei_dt = spousei_dt;

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/hrm-admin/addSpouseInfo",
                    data: JSON.stringify(spouse),
                    dataType: 'json',
                    success: function (data) {
                        showAlertByType('Save successfully!!', "S");
                        clearForm();
                        spouse_loadTable();
                    },
                    error: function (e) {
                        showAlertByType('Sorry,Something Wrong!!', "F");
                    }
                });

            }

        });


        function dataValidation() {

            var status = true;
            var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

            if ($("#spouse_nm").val() == "") {
                status = false;
                $("#err_emp_spouse_name").text("Empty field found!!");
                $("#spouse_nm").focus();
            } else if ($("#spouse_nm").val().length > 50) {
                status = false;
                $("#err_emp_spouse_name").text("Allow maximum 50 character!!");
                $("#spouse_nm").focus();

            } else $("#err_emp_spouse_name").text("");

            if ($("#birth_dt").val() == "") {
                status = false;
                $("#err_emp_spouse_birth_date").text("Empty field found!!");
                $("#birth_dt").focus();

            } else if (checkvalid_JoinDate($("#birth_dt").val())) {
                status = false;
                $("#err_emp_spouse_birth_date").text("Birth date should not be greater than the current Date!!");
                $("#birth_dt").focus();

            } else if (isValidDate($("#birth_dt").val()) == false) {
                status = false;
                $("#err_emp_spouse_birth_date").text("Invalid date format!!");
                $("#birth_dt").focus();

            } else if (!dateCheck.test($("#birth_dt").val())) {
                status = false;
                $("#err_emp_spouse_birth_date").text("Invalid date format!!");
                $("#birth_dt").focus();
            }

            if ($("#occup_cd").val() == -1) {
                status = false;
                $("#err_emp_spouse_occ_id").text("Empty field found!!");
                $("#occup_cd").focus();
            }
            else $("#err_emp_spouse_occ_id").text("");

            if ($("#designation").val() != "") {
                if ($("#designation").val().length > 30) {
                    status = false;
                    $("#err_emp_spouse_desig_id").text("Allow maximum 30 characters!!");
                    $("#designation").focus();
                }
                else $("#err_emp_spouse_desig_id").text("");
            } else $("#err_emp_spouse_desig_id").text("");

            if ($("#organizations").val() != "") {
                if ($("#organizations").val().length > 30) {
                    status = false;
                    $("#err_emp_spouse_org_id").text("Allow maximum 30 characters!!");
                    $("#organizations").focus();
                }
                else $("#err_emp_spouse_org_id").text("");
            } else $("#err_emp_spouse_org_id").text("");


            return status;
        }

        function spouse_loadTable() {

            var emp_spouse_table = $('#emp_spouse_table');

            $
                .ajax({
                    type: "GET",
                    url: "/hrm-admin/emp_SpouseListInfo",
                    success: function (data) {
                        var tableBody = "";
                        var organization = "";
                        var designation = "";
                        var dateofbirth = "";
                        $('#emp_spouse_table tbody').empty();
                        $.each(data, function (idx, elem) {
                            var action =
                                '<button class="btn btn-success childrenInformationEdit" value="Edit" id="spouseEdit"> ' +
                                '<i class="fa fa-pencil"></i></button> ';

                            if (elem[3] == null) {
                                dateofbirth = '';
                            } else {
                                dateofbirth = getDate(elem[3]);
                            }
                            if (elem[5] == null) {
                                designation = '';
                            } else {
                                designation = elem[5];
                            }
                            if (elem[6] == null) {
                                organization = '';
                            } else {
                                organization = elem[6];
                            }
                            tableBody += "<tr'>";
                            tableBody += "<td>" + elem[2] + "</td>";
                            tableBody += "<td>" + dateofbirth + "</td>";
                            tableBody += "<td hidden>" + elem[4] + "</td>";
                            tableBody += "<td>" + designation + "</td>";
                            tableBody += "<td>" + organization + "</td>";
                            tableBody += "<td hidden>" + elem[0] + "</td>";
                            tableBody += "<td hidden>" + elem[1] + "</td>";
                            tableBody += "<td hidden>" + elem[7] + "</td>";
                            tableBody += "<td hidden>" + elem[8] + "</td>";
                            tableBody += "<td>" + action + "</td>"
                            tableBody += "<tr>";
                        });
                        emp_spouse_table.append(tableBody);
                    }
                });
        }


        //Insert  child table

        $("#emp_add_child").click(function () {

            var childFlag = dataValidationChild();

            if (childFlag == true) {

                var child_nm = $('#child_nm').val();
                var birth_dt_d = $('#birth_dt_d').val();
                var gender_cd = $('#gender_cd option:selected').val();
                var occup_cd1 = $('#occup_cd1 option:selected').val();
                var desgination = $('#desgination').val();
                var organizations1 = $('#organizations1').val();
                var marital_stat_cd = $('#marital_stat_cd').val();
                //
                var emp_gid = $('#emp_gid').val();
                var sl_no = $('#sl_no').val();
                var i_usr = $('#i_usr').val();
                var i_dt = $('#i_dt').val();

                if (birth_dt_d) {
                    birth_dt_d = birth_dt_d.split("/").reverse().join("/");
                    birth_dt_d = getFormateDate(birth_dt_d);
                }

                var child = {};
                child.child_nm = child_nm;
                child.birth_dt_d = birth_dt_d;
                child.gender_cd = gender_cd;
                child.occup_cd1 = occup_cd1;
                child.desgination = desgination;
                child.organizations1 = organizations1;
                child.marital_stat_cd = marital_stat_cd;
//
                child.emp_gid = emp_gid;
                child.sl_no = sl_no;
                child.i_usr = i_usr;
                child.i_dt = i_dt;

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/hrm-admin/addChildInfo",
                    data: JSON.stringify(child),
                    dataType: 'json',
                    success: function (data) {
                        showAlertByType('Save successfully!!', "S");
                        clearFormChild();
                        child_loadTable();
                    },
                    error: function (e) {
                        showAlertByType('Sorry,Something Wrong!!', "F");
                    }
                });

            }

        });

//for child validation
        function dataValidationChild() {

            var childstatus = true;
            var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

            if ($("#child_nm").val() == "") {
                childstatus = false;
                $("#err_emp_child_name_id").text("Empty field found!!");
                $("#child_nm").focus();

            } else if ($("#child_nm").val().length > 100) {
                childstatus = false;
                $("#err_emp_child_name_id").text("Allow maximum 100 character!!");
                $("#child_nm").focus();

            } else $("#err_emp_child_name_id").text("");


            if ($("#birth_dt_d").val() == "") {
                childstatus = false;
                $("#err_emp_child_birth_date_id").text("Empty field found!!");
                $("#birth_dt_d").focus();

            } else if (checkvalid_JoinDate($("#birth_dt_d").val())) {
                childstatus = false;
                $("#err_emp_child_birth_date_id").text("Birth date should not be greater than the current Date!!");
                $("#birth_dt_d").focus();

            } else if (isValidDate($("#birth_dt_d").val()) == false) {

                childstatus = false;
                $("#err_emp_child_birth_date_id").text("Invalid Date format!!");
                $("#birth_dt_d").focus();

            } else if (!dateCheck.test($("#birth_dt_d").val())) {
                childstatus = false;
                $("#err_emp_child_birth_date_id").text("Invalid Date format!!");
                $("#birth_dt_d").focus();
            } else {
                $("#err_emp_child_birth_date_id").text("");
            }

            if ($("#gender_cd").val() == -1) {
                childstatus = false;
                $("#err_emp_child_gender_id").text("Empty field found!!");
                $("#gender_cd").focus();
            }
            else $("#err_emp_child_gender_id").text("");

            if ($("#marital_stat_cd").val() == -1) {
                childstatus = false;
                $("#err_emp_child_material_id").text("Empty field found!!");
                $("#marital_stat_cd").focus();
            }
            else $("#err_emp_child_material_id").text("");


            if ($("#organizations1").val() != "") {
                if ($("#organizations1").val().length > 30) {
                    childstatus = false;
                    $("#err_emp_child_org_id").text("Allow maximum 30 characters!!");
                    $("#organizations1").focus();
                }
                else $("#err_emp_child_org_id").text("");
            } else $("#err_emp_child_org_id").text("");

            return childstatus;
        }


        function child_loadTable() {

            var emp_child_table = $('#emp_child_table');

            $
                .ajax({
                    type: "GET",
                    url: "/hrm-admin/emp_ChildListInfo",
                    success: function (data) {
                        var tableBody = "";
                        var organization = "";
                        var occuption = "";
                        var designation = "";
                        var dateofbirth = "";
                        $('#emp_child_table tbody').empty();
                        $.each(data, function (idx, elem) {
                            var action =
                                '<button class="btn btn-success childrenInformationEdit" value="Edit" id="editChildren"> ' +
                                '<i class="fa fa-pencil"></i></button> ';

                            if (elem[5] == null) {
                                organization = '';
                            } else {
                                organization = elem[5];
                            }
                            if (elem[3] == null) {
                                occuption = '';
                            } else {
                                occuption = elem[3];
                            }
                            if (elem[4] == null) {
                                designation = '';
                            } else {
                                designation = elem[4];
                            }
                            if (elem[1] == null) {
                                dateofbirth = '';
                            } else {
                                dateofbirth = getDate(elem[1]);
                            }
                            tableBody += "<tr'>";
                            tableBody += "<td>" + elem[0] + "</td>";
                            tableBody += "<td>" + dateofbirth + "</td>";
                            tableBody += "<td hidden>" + elem[2] + "</td>";
                            tableBody += "<td hidden>" + occuption + "</td>";
                            tableBody += "<td>" + designation + "</td>";
                            tableBody += "<td>" + organization + "</td>";
                            tableBody += "<td hidden>" + elem[6] + "</td>";
                            tableBody += "<td hidden>" + elem[7] + "</td>";
                            tableBody += "<td hidden>" + elem[8] + "</td>";
                            tableBody += "<td hidden>" + elem[9] + "</td>";
                            tableBody += "<td hidden>" + elem[10] + "</td>";
                            tableBody += "<td>" + action + "</td>"
                            tableBody += "<tr>";
                        });
                        emp_child_table.append(tableBody);
                    }
                });
        }


        function clearFormChild() {
            $('#child_nm').val("");
            $('#birth_dt_d').val("");
            $('#gender_cd').val('-1');
            $('#occup_cd').val('-1');
            $('#occup_cd1').val('');
            $('#marital_stat_cd').val('-1');
            $('#desgination').val("");
            $('#organizations1').val("");
            $("#emp_gid").val("");
            $("#sl_no").val("");
            $("#i_dt").val("");
            $("#i_usr").val("");
            $('#emp_add_child').val("Save");
        }

        function clearForm() {
            $('#spouse_nm').val("");
            $('#birth_dt').val("");
            $('#occup_cd').val('-1');
            $('#designation').val("");
            $('#organizations').val("");
            $("#spouse_gid").val("");
            $("#spousesl_no").val("");
            $("#spousei_usr").val("");
            $("#spousei_dt").val("");
            $('#emp_save_spouse').val("Save");
        }
    }

//children table

    $('#emp_child_table tbody').on('click', '#editChildren', function () {

        $('#emp_add_child').val("Update");
        $("#err_emp_child_org_id").text("");
        $("#err_emp_child_desig_id").text("");
        $("#err_emp_child_material_id").text("");
        $("#err_emp_child_gender_id").text("");
        $("#err_emp_child_birth_date_id").text("");
        $("#err_emp_child_name_id").text("");

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();
        var col10 = curRow.find('td:eq(9)').text();
        var col11 = curRow.find('td:eq(10)').text();
        //
        $('#child_nm').val(col1);
        $('#birth_dt_d').val(col2);
        $('#gender_cd').val(col3);
        $('#occup_cd1').val(col4);
        $('#desgination').val(col5);
        $('#organizations1').val(col6);
        $('#marital_stat_cd').val(col11);

        $('#emp_gid').val(col7);
        $('#sl_no').val(col8);
        $('#i_usr').val(col9);
        $('#i_dt').val(col10);

        $('html, body').animate({
            scrollTop: $('#cildrenScroll').offset().top
        }, 500);

        getDateFor();

    });


    $('#emp_spouse_table tbody').on('click', '#spouseEdit', function () {

        $("#err_emp_spouse_org_id").text("");
        $("#err_emp_spouse_desig_id").text("");
        $("#err_emp_spouse_occ_id").text("");
        $("#err_emp_spouse_birth_date").text("");
        $("#err_emp_spouse_name").text("");
        $('#emp_save_spouse').val("Update");

        var curRow = $(this).closest('tr');
        var spouseName = curRow.find('td:eq(0)').text();
        var birthDate = curRow.find('td:eq(1)').text();
        var OccuptionCD = curRow.find('td:eq(2)').text();
        var designation = curRow.find('td:eq(3)').text();
        var organization = curRow.find('td:eq(4)').text();
        var empGid = curRow.find('td:eq(5)').text();
        var slno = curRow.find('td:eq(6)').text();
        var i_usr = curRow.find('td:eq(7)').text();
        var i_dt = curRow.find('td:eq(8)').text();
        $('#spouse_nm').val(spouseName);
        $('#birth_dt').val(birthDate);
        $('#occup_cd').val(OccuptionCD);
        $('#designation').val(designation);
        $('#organizations').val(organization);
//for update
        $('#spouse_gid').val(empGid);
        $('#spousesl_no').val(slno);
        $('#spousei_usr').val(i_usr);
        $('#spousei_dt').val(i_dt);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);
        getDateForspouse();

    });

    function getDateForspouse() {
        var dateStr = $('#spusei_dt').val();
        var dateCom = getDate(dateStr);
        $('#spusei_dt').val(dateCom);

        var spouseDt = $('#spousei_dt').val();
        var dateComFrom1 = getDate(spouseDt);
        $('#spousei_dt').val(dateComFrom1);

    }

    function getDateFor() {
        var dateStr = $('#i_dt').val();
        var dateCom = getDate(dateStr);
        $('#i_dt').val(dateCom);

        // var dateStrFrom = $('#birth_dt_d').val();
        // var dateComFrom = getDate(dateStrFrom);
        // $('#birth_dt_d').val(dateComFrom);
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    }

    $("#emp_refresh_spouse").click(function () {
        clearForm();
        $("#err_emp_spouse_org_id").text("");
        $("#err_emp_spouse_desig_id").text("");
        $("#err_emp_spouse_occ_id").text("");
        $("#err_emp_spouse_birth_date").text("");
        $("#err_emp_spouse_name").text("");
    });

    $("#emp_refresh_child").click(function () {
        $('#child_nm').val("");
        $('#birth_dt_d').val("");
        $('#marital_stat_cd').val('-1');
        $('#occup_cd1').val('');
        $('#gender_cd').val('-1');
        $('#desgination').val("");
        $('#organizations1').val("");
        $("#err_emp_child_org_id").text("");
        $("#err_emp_child_desig_id").text("");
        $("#err_emp_child_material_id").text("");
        $("#err_emp_child_gender_id").text("");
        $("#err_emp_child_birth_date_id").text("");
        $("#err_emp_child_name_id").text("");

        $("#emp_gid").val("");
        $("#sl_no").val("");
        $("#i_dt").val("");
        $("#i_usr").val("");
        $('#emp_add_child').val("Save");
    });
    function checkvalid_JoinDate(enteredDate) {
        var trace = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate > today) {
            trace = true;
        }
        return trace;
    }

    function getFormateDate(date) {
        var d = new Date(date);
        return d;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

});