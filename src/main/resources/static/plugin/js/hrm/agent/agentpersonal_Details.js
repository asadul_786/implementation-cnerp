/**
 * Created by golam nobi on 1/30/2020.
 */

$(document).ready(function () {


    $("#btnpersonaldetailsInfo").click(function () {

        var flag = ValidationpersonalDetils();

        if (flag == true) {

            var father_nm = $('#father_nm').val();
            var mothername = $('#mothername').val();
            var iden_mark = $('#iden_mark').val();
            var brith_plc = $('#brith_plc').val();
            var blgp_cd = $('#blgp_cd').val();
            var height = $('#height').val();
            var weight = $('#weight').val();
            var hair_color = $('#hair_color').val();
            var eye_color = $('#eye_color').val();
            var relation_cd = $('#relation_cd').val();
            var relation_nm = $('#relation_nm').val();
            var driv_licen_no = $('#driv_licen_no').val();
            var national_id_no = $('#national_id_no').val();
            var pass_no = $('#pass_no').val();

            var id_issue_dt = $('#id_issue_dt').val();
            var licendt = $('#licendt').val();
            var pass_exp_dt = $('#pass_exp_dt').val();

            if (id_issue_dt) {
                id_issue_dt = id_issue_dt.split("/").reverse().join("/");
                id_issue_dt = getFormateDate(id_issue_dt);
            }
            if (licendt) {
                licendt = licendt.split("/").reverse().join("/");
                licendt = getFormateDate(licendt);
            }
            if (pass_exp_dt) {
                pass_exp_dt = pass_exp_dt.split("/").reverse().join("/");
                pass_exp_dt = getFormateDate(pass_exp_dt);
            }

            function getFormateDate(date) {
                var d = new Date(date);
                return d;
            }

            var agentpersonalDetails = {};
            agentpersonalDetails.father_nm = father_nm;
            agentpersonalDetails.mothername = mothername;
            agentpersonalDetails.iden_mark = iden_mark;
            agentpersonalDetails.brith_plc = brith_plc;
            agentpersonalDetails.blgp_cd = blgp_cd;
            //permanet
            agentpersonalDetails.height = height;
            agentpersonalDetails.weight = weight;
            agentpersonalDetails.hair_color = hair_color;
            agentpersonalDetails.eye_color = eye_color;
            agentpersonalDetails.relation_cd = relation_cd;
            agentpersonalDetails.relation_nm = relation_nm;
            agentpersonalDetails.driv_licen_no = driv_licen_no;
            //digital
            agentpersonalDetails.licendt = licendt;
            agentpersonalDetails.national_id_no = national_id_no;
            agentpersonalDetails.id_issue_dt = id_issue_dt;
            agentpersonalDetails.pass_no = pass_no;
            agentpersonalDetails.pass_exp_dt = pass_exp_dt;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-dev/agent_addagentDetailsInfo",
                data: JSON.stringify(agentpersonalDetails),
                dataType: 'json',
                success: function (data) {
                    showAlertByType('Save successfully!!', "S");
                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");
                }
            });

        }

    });

    function ValidationpersonalDetils() {

        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");
        var status = true;

        if ($("#relation_cd").val() == "-1") {
            status = false;
            $("#err_relation_cd").text("Empty field found!!");
            $("#relation_cd").focus();
        } else $("#err_relation_cd").text("");

        if ($("#relation_nm").val() == "") {
            status = false;
            $("#err_relation_nm").text("Empty field found!!");
            $("#relation_nm").focus();
        } else $("#err_relation_nm").text("");


        if ($("#blgp_cd").val() == -1) {
            status = false;
            $("#err_blgp_cd").text("Empty field found!!");
            $("#blgp_cd").focus();
        } else $("#err_blgp_cd").text("");


        if ($("#licendt").val() != "") {
            if (!dateCheck.test($("#licendt").val())) {
                status = false;
                $("#err_licendt").text("Invalid date format!!");
                $("#licendt").focus();

            } else if (isValidDate($("#licendt").val()) == false) {
                status = false;
                $("#err_licendt").text("Invalid date format!!");
                $("#licendt").focus();

            } else {
                $("#err_licendt").text("");
            }
        } else  $("#err_licendt").text("");

        if ($("#pass_exp_dt").val() != "") {
            if (!dateCheck.test($("#pass_exp_dt").val())) {
                status = false;
                $("#err_pass_exp_dt").text("Invalid date format!!");
                $("#pass_exp_dt").focus();

            } else if (isValidDate($("#pass_exp_dt").val()) == false) {
                status = false;
                $("#err_pass_exp_dt").text("Invalid date format!!");
                $("#pass_exp_dt").focus();

            } else {
                $("#err_pass_exp_dt").text("");
            }
        } else $("#err_pass_exp_dt").text("");

        if ($("#id_issue_dt").val() != "") {
            if (!dateCheck.test($("#id_issue_dt").val())) {
                status = false;
                $("#err_id_issue_dt").text("Invalid date format!!");
                $("#id_issue_dt").focus();

            } else if (isValidDate($("#id_issue_dt").val()) == false) {
                status = false;
                $("#err_id_issue_dt").text("Invalid date format!!");
                $("#id_issue_dt").focus();

            } else {
                $("#err_id_issue_dt").text("");
            }
        } else $("#err_id_issue_dt").text("");


        if ($("#national_id_no").val() != "") {

            if ($("#national_id_no").val().length == 10 || $("#national_id_no").val().length == 13 || $("#national_id_no").val().length == 17) {
                $("#err_national_id_no").text("");
            } else {
                status = false;
                $("#err_national_id_no").text("NID allow only 10/13/17 digit!!");
                $("#national_id_no").focus();
            }
        } else $("#err_national_id_no").text("");


        if ($("#height").val() != "") {
            if ($("#height").val().length > 30) {
                status = false;
                $("#err_height").text("Maximum 30 character allow!!");
                $("#height").focus();
            } else  $("#err_height").text("");
        }

        if ($("#weight").val() != "") {
            if ($("#weight").val().length > 30) {
                status = false;
                $("#err_weight").text("Maximum 30 character allow!!");
                $("#weight").focus();
            } else  $("#err_weight").text("");
        }

        return status;
    }


    $("#brnpersonalDetilsRefresh").click(function () {
        $('#father_nm').val("");
        $('#mothername').val("");
        $('#iden_mark').val("");
        $('#brith_plc').val("");
        $('#blgp_cd').val("-1");
        $('#height').val("");
        $('#weight').val("");
        $('#hair_color').val("");
        $('#eye_color').val("");
        $('#relation_nm').val("");
        $('#driv_licen_no').val("");
        $('#licendt').val("");
        $('#national_id_no').val("");
        $('#id_issue_dt').val("");
        $('#pass_no').val("");
        $('#pass_exp_dt').val("");
        $("#relation_cd").val("-1");
//error text remove
        $("#err_relation_cd").text("");
        $("#err_relation_nm").text("");
        $("#err_blgp_cd").text("");
        $("#err_licendt").text("");
        $("#err_pass_exp_dt").text("");
        $("#err_id_issue_dt").text("");

    });

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }
});