/**
 * Created by golam nobi
 */

$(document).ready(function () {

    $("#btnapplyfiter").click(function () {

        var flag = dataValidationEmployeeAll();

        var agentTable = $('#tableData');

        if (flag == true) {

            var empNmEng = $('#empNmEng').val();
            var employeeId = $('#employeeId').val();
            var desigCd = $('#desigCd').val();
            var employmentTypeCd = $('#employmentTypeCd').val();
            var officeCd_one = $('#officeCd_one').val();
            var divdeptCd = $('#divdeptCd').val();

            var empployeefilter = {};
            empployeefilter.empNmEng = empNmEng;
            empployeefilter.employeeId = employeeId;
            empployeefilter.desigCd = desigCd;
            empployeefilter.employmentTypeCd = employmentTypeCd;
            empployeefilter.officeCd_one = officeCd_one;
            empployeefilter.divdeptCd = divdeptCd;
            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/filteringEmp",
                data: JSON.stringify(empployeefilter),
                dataType: 'json',
                success: function (data) {
                    if (data == "") {
                        showAlertByType('Your searching data not found', "W");
                    }
                    agentTable.dataTable({
                        paging: true,
                        searching: true,
                        destroy: true,
                        data: data,
                        columns: [{
                            "data": "empName"
                        }, {
                            "data": "empId"
                        }, {
                            "data": "office"
                        }, {
                            "data": "department"
                        }, {
                            "data": "designation"
                        }, {
                            "data": "employeementType"
                        }, {
                            "data": "employeeType"
                        }, {
                            "data": "empGid",
                            "render": function (data,
                                                type,
                                                row) {
                                return "<a class='btn btn-sm btn-info' href=\"/hrm-admin/updateemployee-setup?empGID="
                                    + encodeURIComponent(data)
                                    + "\">"
                                    + '<i class="fa fa-pencil"></i>'
                                    + ' Edit'
                                    + "</a>";
                            }

                        }, {
                            "data": "empGid",
                            "render": function (data,
                                                type,
                                                row) {
                                return "<a class='btn btn-sm btn-warning' href=\"/hrm-dev/dm-preview-details?empGID="
                                    + encodeURIComponent(data)
                                    + "\">"
                                    + '<i class="fa fa-eye"></i>'
                                    + ' Display'
                                    + "</a>";
                            }
                        }]
                    });

                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");
                }
            });
        }
    });


    $("#refreshButtonfiltering").click(function () {

        $("#empNmEng").val('');
        $("#employeeId").val('');
        $("#officeCd_one").val('0').select2();
        $("#divdeptCd").val('0').select2();
        $("#desigCd").val('0').select2();

        var agentTable = $('#tableData');

        var empNmEng = $('#empNmEng').val();
        var employeeId = $('#employeeId').val();
        var desigCd = $('#desigCd').val();
        var employmentTypeCd = $('#employmentTypeCd').val();
        var officeCd_one = $('#officeCd_one').val();
        var divdeptCd = $('#divdeptCd').val();

        var empployeefilter = {};
        empployeefilter.empNmEng = empNmEng;
        empployeefilter.employeeId = employeeId;
        empployeefilter.desigCd = desigCd;
        empployeefilter.employmentTypeCd = employmentTypeCd;
        empployeefilter.officeCd_one = officeCd_one;
        empployeefilter.divdeptCd = divdeptCd;
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/hrm-admin/filteringEmp",
            data: JSON.stringify(empployeefilter),
            dataType: 'json',
            success: function (data) {
                agentTable.dataTable({
                    paging: true,
                    searching: true,
                    destroy: true,
                    data: data,
                    columns: [{
                        "data": "empName"
                    }, {
                        "data": "empId"
                    }, {
                        "data": "office"
                    }, {
                        "data": "department"
                    }, {
                        "data": "designation"
                    }, {
                        "data": "employeementType"
                    }, {
                        "data": "employeeType"
                    }, {
                        "data": "empGid",
                        "render": function (data,
                                            type,
                                            row) {
                            return "<a class='btn btn-sm btn-info' href=\"/hrm-admin/updateemployee-setup?empGID="
                                + encodeURIComponent(data)
                                + "\">"
                                + '<i class="fa fa-pencil"></i>'
                                + 'Edit'
                                + "</a>";
                        }

                    }, {
                        "data": "empGid",
                        "render": function (data,
                                            type,
                                            row) {
                            return "<a class='btn btn-sm btn-warning' href=\"/hrm-dev/dm-preview-details?empGID="
                                + encodeURIComponent(data)
                                + "\">"
                                + '<i class="fa fa-eye"></i>'
                                + 'Display'
                                + "</a>";
                        }
                    }]
                });
            },
            error: function (e) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    });

    function dataValidationEmployeeAll() {
        var status = true;
        var empNmEng = $('#empNmEng').val();
        var employeeId = $('#employeeId').val();
        var desigCd = $('#desigCd').val();
        var employmentTypeCd = $('#employmentTypeCd').val();
        var officeCd_one = $('#officeCd_one').val();
        var divdeptCd = $('#divdeptCd').val();

        if ($('#empNmEng').val() == '' && $('#employeeId').val() == '' && $('#desigCd').val() == '0' && $('#officeCd_one').val() == '0' && $('#divdeptCd').val() == '0') {
            showAlertByType('Please select minimum one value!!', "W");
            status = false;
        }
        return status;
    }

});







