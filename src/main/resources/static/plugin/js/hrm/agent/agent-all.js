/**
 * Created by golam nobi on 1/30/2020.
 */

$(document).ready(function () {

    $("#btnFilter").click(function () {

        var flag = dataValidationAgentAll();

        if (flag == true) {
            loadagentFilteringTabel();
        }

    });


    function loadagentFilteringTabel() {

        var agentTable = $('#agentTable');
        var agent_nm_eng = $('#agent_nm_eng').val();
        var agentId = $('#agentId').val();
        var office_cd = $('#office_cd').val();
        var agentfilter = {};
        agentfilter.agent_nm_eng = agent_nm_eng;
        agentfilter.agentId = agentId;
        agentfilter.office_cd = office_cd;

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/hrm-dev/filteringAgent",
            data: JSON.stringify(agentfilter),
            dataType: 'json',
            success: function (data) {
                if (data == "") {
                    showAlertByType('Your searching data not found!!', "W");
                }
                agentTable.dataTable({
                    paging: true,
                    searching: true,
                    destroy: true,
                    data: data,
                    columns: [{
                        "data": "agnetId"
                    }, {
                        "data": "name"
                    }, {
                        "data": "validToDate",
                        "render": function (data,
                                            type,
                                            row) {
                            return data == null ? '' : getDate(data);
                        }
                    }, {
                        "data": "joinDate",
                        "render": function (data,
                                            type,
                                            row) {
                            return data == null ? '' : getDate(data);
                        }
                    }, {
                        "data": "devOffice"
                    },
                        {
                            "data": "ecriptagnetId",
                            "render": function (data,
                                                type,
                                                row) {

                                return "<a class='btn btn-sm btn-info' href=\"/hrm-dev/updateagent-setup?agentID="
                                    + encodeURIComponent(data)
                                    + "\">"
                                    + '<i class="fa fa-pencil"></i>'
                                    + ' Edit'
                                    + "</a>";
                            }

                        },]
                });
            },
            error: function (e) {
                showAlertByType('Something Wrong!', "F");
            }
        });

    }


    $("#refreshButton").click(function () {
        $('#agent_nm_eng').val("");
        $('#agentId').val("");
        $('#office_cd').val("-1").select2();
        $('#err_office_cd').text("");

        var agentTable = $('#agentTable');

        var agent_nm_eng = $('#agent_nm_eng').val();
        var agentId = $('#agentId').val();
        var office_cd = $('#office_cd').val();
        var agentfilter = {};
        agentfilter.agent_nm_eng = agent_nm_eng;
        agentfilter.agentId = agentId;
        agentfilter.office_cd = office_cd;

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/hrm-dev/filteringAgent",
            data: JSON.stringify(agentfilter),
            dataType: 'json',
            success: function (data) {
                agentTable.dataTable({
                    paging: true,
                    searching: true,
                    destroy: true,
                    data: data,
                    columns: [{
                        "data": "agnetId"
                    }, {
                        "data": "name"
                    }, {
                        "data": "validToDate"
                    }, {
                        "data": "joinDate"
                    }, {
                        "data": "devOffice"
                    },
                        {
                            "data": "agnetId",
                            "render": function (data,
                                                type,
                                                row) {

                                return "<a class='btn btn-sm btn-info' href=\"/updateagent-setup?agentID="
                                    + encodeURIComponent(data)
                                    + "\">"
                                    + '<i class="fa fa-pencil"></i>'
                                    + 'Edit'
                                    + "</a>";
                            }

                        },]
                });

            },
            error: function (e) {
                showAlertByType('Something Wrong!', "F");
            }
        });

    });

    function dataValidationAgentAll() {
        var status = true;
        if ($('#agent_nm_eng').val() == '' && $('#agentId').val() == '' && $("#office_cd").val() == '-1') {
            showAlertByType('Please select minimum one value!!', "W");
            status = false;
        }
        return status;
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;

        return date;
    }

});