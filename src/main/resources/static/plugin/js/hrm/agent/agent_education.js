/**
 * Created by golam nobi on 1/30/2020.
 */

$(document).ready(function () {

    $("#degree_cd").select2();
    $("#subject_cd").select2();
    $("#result_cd").select2();
    $("#board_cd").select2();

    if ($("#updateaqgentid").val() != "") {
        refresh();
    }

    $('#degree_cd').attr('disabled', false);


    $("#btnagentEducation").click(function () {

        var flag = dataValidationEducation();

        if (flag == true) {

            var degree_cd = $('#degree_cd').val();
            var subject_cd = $('#subject_cd').val();
            var board_cd = $('#board_cd').val();
            var result_cd = $('#result_cd').val();
            var tmarks = $('#tmarks').val();
            var passingyear = $('#passingyear').val();
            var updateeduId = $('#updateeduId').val();

            var agentEducation = {};

            agentEducation.degree_cd = degree_cd;
            agentEducation.subject_cd = subject_cd;
            agentEducation.board_cd = board_cd;
            agentEducation.result_cd = result_cd;
            agentEducation.tmarks = tmarks;
            agentEducation.passingyear = passingyear;
            agentEducation.updateeduId = updateeduId;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-dev/addagentEducationInfo",
                data: JSON.stringify(agentEducation),
                dataType: 'json',
                success: function (data) {
                    refresh();
                    showAlertByType('Save successfully!!', "S");
                    clearFrom();
                    $('#btnagentEducation').text('Save');
                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");
                }
            });

        }

    });

    //validation
    function dataValidationEducation() {

        var status = true;

        if ($("#degree_cd").val() == -1) {
            status = false;
            $("#err_agent_degree_cd").text("Empty field found!!");
            $("#degree_cd").focus();
        } else $("#err_agent_degree_cd").text("");


        if ($("#result_cd").val() == -1) {
            status = false;
            $("#err_agent_result_cd").text("Empty field found!!");
            $("#result_cd").focus();
        } else $("#err_agent_result_cd").text("");

        if ($("#passingyear").val().length > 4) {
            status = false;
            $("#err_emp_passing_year").text("Year maximum 4 characters!!");
            $("#passingyear").focus();

        } else if (/[^0-9]/g.test($("#passingyear").val()) == true) {
            status = false;
            $("#err_emp_passing_year").text("Sorry,Illegal characters!!");
            $("#passingyear").focus();

        } else {
            $("#err_emp_passing_year").text("");
        }

        if ($("#tmarks").val().length > 10) {
            status = false;
            $("#err_agent_tmarks").text("TMarks_CGPA_GPA maximum 10 characters!!");
            $("#tmarks").focus();
        } else $("#err_agent_tmarks").text("");

        return status;
    }

    function refresh() {

        var educationTable = $('#edu_agent_tbl_id');

        $
            .ajax({
                type: "GET",
                url: "/hrm-dev/educationListInfo",
                success: function (data) {
                    var no = 1;
                    var tableBody = "";
                    var subjectEmpty = "";
                    var boardEmpty = "";
                    var gpamarksEmpty = "";
                    $('#edu_agent_tbl_id tbody').empty();
                    $.each(data, function (idx, elem) {
                        var action =
                            '<button class="btn btn-success childrenInformationEdit" value="Edit" id="editEducation"> ' +
                            '<i class="fa fa-pencil"></i></button> ';

                        if (elem[4] == null) {
                            subjectEmpty = '';
                        } else {
                            subjectEmpty = elem[4];
                        }
                        if (elem[8] == null) {
                            boardEmpty = '';
                        } else {
                            boardEmpty = elem[8];
                        }
                        if (elem[9] == null) {
                            gpamarksEmpty = '';
                        } else {
                            gpamarksEmpty = elem[9];
                        }

                        tableBody += "<tr'>";
                        tableBody += "<td>" + elem[2] + "</td>";
                        tableBody += "<td>" + subjectEmpty + "</td>";
                        tableBody += "<td>" + elem[6] + "</td>";

                        tableBody += "<td>" + boardEmpty + "</td>";
                        tableBody += "<td>" + gpamarksEmpty + "</td>";

                        tableBody += "<td>" + elem[10] + "</td>";
                        tableBody += "<td hidden>" + elem[0] + "</td>";
                        tableBody += "<td hidden>" + elem[1] + "</td>";
                        tableBody += "<td hidden>" + elem[3] + "</td>";
                        tableBody += "<td hidden>" + elem[5] + "</td>";
                        tableBody += "<td hidden>" + elem[7] + "</td>";
                        tableBody += "<td>" + action + "</td>"
                        tableBody += "<tr>";
                    });
                    educationTable.append(tableBody);
                }
            });
    }


    $('#edu_agent_tbl_id tbody').on('click', '#editEducation', function () {

        $('#degree_cd').attr('disabled', true);
        $('#btnagentEducation').text('Update');

        var curRow = $(this).closest('tr');
        var marks = curRow.find('td:eq(4)').text();
        var year = curRow.find('td:eq(5)').text();
        var agentId = curRow.find('td:eq(6)').text();
        var degreeCD = curRow.find('td:eq(7)').text();
        var subjectCD = curRow.find('td:eq(8)').text();
        var resultCD = curRow.find('td:eq(9)').text();
        var universityCD = curRow.find('td:eq(10)').text();

        $('#updateeduId').val(agentId);
        $('#degree_cd').val(degreeCD).select2();
        $('#subject_cd').val(subjectCD).select2();
        $('#result_cd').val(resultCD).select2();
        $('#board_cd').val(universityCD).select2();
        $('#tmarks').val(marks);
        $('#updateeduId').val(agentId);
        $('#passingyear').val(year);

    });

    $("#btnagentEducationRefresh").click(function () {
        clearFrom();
    });

    function clearFrom() {
        $('#updateeduId').val("");
        $('#degree_cd').val("-1").select2();
        $('#subject_cd').val("0").select2();
        $('#result_cd').val("-1").select2();
        $('#board_cd').val("0").select2();
        $('#tmarks').val("");
        $('#updateeduId').val("");
        $('#passingyear').val("");
        $('#degree_cd').attr('disabled', false);
        //
        $("#err_agent_degree_cd").text("");
        $("#err_agent_subject_cd").text("");
        $("#err_agentboard_cd").text("");
        $("#err_agent_result_cd").text("");
        $("#err_emp_passing_year").text("");
        $("#err_agent_tmarks").text("");
        $('#btnagentEducation').text('Save');
    }


    $(document).change("#degree_cd", function (e) {

        var degreeCD = $("#degree_cd option:selected").val();

        if ($('#updateeduId').val() == "") {

            $
                .ajax({
                    type: "GET",
                    url: "/hrm-dev/checkEducationId/" + degreeCD,
                    success: function (data) {
                        if (data == true) {
                            $('#btnagentEducation').attr("disabled", true);
                            $("#err_agent_degree_cd").text("Data Already Exit!!");
                        } else {
                            $('#btnagentEducation').attr("disabled", false);
                            $("#err_agent_degree_cd").text("");
                        }
                    },
                    error: function (e) {
                        showAlert("Sorry,Something Wrong!!");
                    }

                });
        }

    });


});