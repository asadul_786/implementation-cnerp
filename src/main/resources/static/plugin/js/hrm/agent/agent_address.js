/**
 * Created by golam nobi on 1/29/2020.
 */

$(document).ready(function () {

    $(document).on("change", "#mail_divion_cd", function (e) {

        var divisionCD = $("#mail_divion_cd option:selected").val();

        $.get("/hrm-admin/get-districtInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#mail_district_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select District---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });


            });
    });

    $(document).on("change", "#mail_district_cd", function (e) {

        var divisionCD = $("#mail_district_cd option:selected").val();

        $.get("/hrm-admin/get-thanaInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#mail_thana_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Thana---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });

    });


    //for parmanet

    $(document).on("change", "#perm_division_cd", function (e) {

        var divisionCD = $("#perm_division_cd option:selected").val();

        $.get("/hrm-admin/get-districtInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#perm_district_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select District---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });


    $(document).on("change", "#perm_district_cd", function (e) {

        var divisionCD = $("#perm_district_cd option:selected").val();

        $.get("/hrm-admin/get-thanaInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#perm_thana_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Thana---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });

//insert

    $("#btn_agentAddress").click(function () {

        var flag = dataValidationAddress();

        if (flag == true) {

            var mail_addr1 = $('#mail_addr1').val();
            var mail_divion_cd = $('#mail_divion_cd').val();
            var mail_district_cd = $('#mail_district_cd').val();
            var mail_thana_cd = $('#mail_thana_cd').val();
            var mobile = $('#mobile').val();
            //for per
            var perm_addr1 = $('#perm_addr1').val();
            var perm_division_cd = $('#perm_division_cd').val();
            var perm_district_cd = $('#perm_district_cd').val();
            var perm_thana_cd = $('#perm_thana_cd').val();
            var p_phone = $('#p_phone').val();
            var country_cd = $('#country_cd').val();
            var nationality_cd = $('#nationality_cd').val();
            //for digital
            var fax = $('#fax').val();
            var t_phone = $('#t_phone').val();
            var email = $('#email').val();
            var url = $('#url').val();

            var agentaddress = {};

            agentaddress.mail_addr1 = mail_addr1;
            agentaddress.mail_divion_cd = mail_divion_cd;
            agentaddress.mail_district_cd = mail_district_cd;
            agentaddress.mail_thana_cd = mail_thana_cd;
            agentaddress.mobile = mobile;
            //permanet
            agentaddress.perm_addr1 = perm_addr1;
            agentaddress.perm_division_cd = perm_division_cd;
            agentaddress.perm_district_cd = perm_district_cd;
            agentaddress.perm_thana_cd = perm_thana_cd;
            agentaddress.p_phone = p_phone;
            agentaddress.country_cd = country_cd;
            agentaddress.nationality_cd = nationality_cd;
            //digital
            agentaddress.fax = fax;
            agentaddress.t_phone = t_phone;
            agentaddress.email = email;
            agentaddress.url = url;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-dev/addagentDetailsInfo",
                data: JSON.stringify(agentaddress),
                dataType: 'json',
                success: function (data) {
                    showAlertByType('Save successfully!!', "S");
                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");
                }
            });
        }
    });
    //validation
    function dataValidationAddress() {

        var checkEmail = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        var regexMobile = /^(?:\+?88)?01[1,2,3,5,6,7,8,9]{1}[0-9]{8}$/;

        var status = true;

        if ($("#mail_addr1").val() == "") {
            status = false;
            $("#err_mail_addr1").text("Empty field found!!");
            $("#mail_addr1").focus();
        } else $("#err_mail_addr1").text("");


        if ($("#perm_addr1").val() == "") {
            status = false;
            $("#err_perm_addr1").text("Empty field found!!");
            $("#perm_addr1").focus();
        } else $("#err_perm_addr1").text("");

        if ($("#nationality_cd").val() == -1) {
            status = false;
            $("#err_nationality_cd").text("Empty field found!!");
            $("#nationality_cd").focus();
        } else $("#err_nationality_cd").text("");


        if ($("#t_phone").val() != "") {

            if (!regexMobile.test($("#t_phone").val())) {
                status = false;
                $("#err_t_phone").text("Invalid Cell Number!!");
                $("#t_phone").focus();

            } else if (/[^0-9]/g.test($("#t_phone").val()) == true) {
                status = false;
                $("#err_t_phone").text("Invalid Cell Number!!");
                $("#t_phone").focus();
            } else {
                $("#err_t_phone").text("");
            }

        } else  $("#err_t_phone").text("");


        if ($("#p_phone").val() != "") {

            if (!regexMobile.test($("#p_phone").val())) {
                status = false;
                $("#err_p_phone").text("Invalid Contact Number!!");
                $("#p_phone").focus();

            } else if (/[^0-9]/g.test($("#p_phone").val()) == true) {
                status = false;
                $("#err_p_phone").text("Invalid Contact Number!!");
                $("#p_phone").focus();
            } else {
                $("#err_p_phone").text("");
            }

        } else  $("#err_p_phone").text("");


        if ($("#mobile").val() != "") {

                if (!regexMobile.test($("#mobile").val())) {
                    status = false;
                    $("#err_mailmobile").text("Invalid Contact Number!!");
                    $("#mobile").focus();

                } else if (/[^0-9]/g.test($("#mobile").val()) == true) {
                    status = false;
                    $("#err_mailmobile").text("Invalid Contact Number!!");
                    $("#mobile").focus();
                } else {
                    $("#err_mailmobile").text("");
                }

            } else $("#err_mailmobile").text("");

            if ($("#email").val() != "") {

                if (!checkEmail.test($("#email").val())) {
                    status = false;
                    $("#err_email").text("Invalid email address!!");
                    $("#email").focus();
                } else $("#err_email").text("");

            } else $("#err_email").text("");

            return status;

        }
        //refresh button
        $("#btn_agentAddressRefresh").click(function () {

            $('#mail_addr1').val("");
            $('#mail_divion_cd').val("-1");
            $('#mobile').val("");
            $('#perm_addr1').val("");
            $('#perm_division_cd').val("-1");
            $('#p_phone').val("");
            $('#country_cd').val("001");
            $('#nationality_cd').val("-1");
            $('#fax').val("");
            $('#t_phone').val("");
            $('#email').val("");
            $('#url').val("");
//error remove
            $("#err_email").text("");
            $("#err_mailmobile").text("");
            $("#err_p_phone").text("");
            $("#err_t_phone").text("");
            $("#err_nationality_cd").text("");
            $("#err_perm_addr1").text("");
            $("#err_mail_addr1").text("");
            //
            $('#perm_district_cd').empty();
            $('#perm_thana_cd').empty();
            $('#mail_district_cd').empty();
            $('#mail_thana_cd').empty();

        });

    }

    );