/**
 * Created by golam nobi on 1/29/2020.
 */
$(document).ready(function () {

    $("#nomineeacc_bank").select2();
    $("#nomineeacc_bank_br").select2();


    if ($("#updateaqgentid").val() != "") {
        refresh();
    }

    $(document).on("change", "#nomineeacc_bank", function (e) {

        var bankCD = $("#nomineeacc_bank option:selected").val();

        $.get("/hrm-admin/get-branchBank?bankCD=" + bankCD,

            function (data, status) {

                var office = $('#nomineeacc_bank_br');
                office.empty();
                office.append($('<option/>', {
                    value: "0",
                    text: "---Select Branch Bank---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });

//agent save

    $("#btnaddagentNominee").click(function () {

        var flag = dataValidationNominee();

        if (flag == true) {

            var nominee_nm = $('#nominee_nm').val();
            var accountno = $('#accountno').val();
            var percentages = $('#percentages').val();
            var relation_cd = $('#relation_cd').val();
            var acc_tp = $('#acc_tp').val();
            var nomineeacc_bank = $('#nomineeacc_bank').val();
            var nomineeacc_bank_br = $('#nomineeacc_bank_br').val();
            var updateeduId = $('#updateeduId').val();
            var slno = $('#slno').val();

            if (nomineeacc_bank_br == '0') {
                nomineeacc_bank_br = '';
            }

            if (nomineeacc_bank == '0') {
                nomineeacc_bank = '';
            }

            if (acc_tp == '0') {
                acc_tp = '';
            }

            var agentNominee = {};
            agentNominee.nominee_nm = nominee_nm;
            agentNominee.accountno = accountno;
            agentNominee.percentages = percentages;
            agentNominee.relation_cd = relation_cd;
            agentNominee.acc_tp = acc_tp;
            agentNominee.nomineeacc_bank = nomineeacc_bank;
            agentNominee.nomineeacc_bank_br = nomineeacc_bank_br;
            agentNominee.updateeduId = updateeduId;
            agentNominee.slno = slno;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-dev/addagentNomineeInfo",
                data: JSON.stringify(agentNominee),
                dataType: 'json',
                success: function (data) {
                    if (data == false) {
                        showAlertByType('Nominee Percentage can not accessed maximum 100%!!', "W");
                    } else {
                        refresh();
                        showAlertByType('Save successfully!!', "S");
                        clearFrom();
                        $('#btnaddagentNominee').text("Save");
                    }
                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");
                }
            });
        }

    });

    //validation
    function dataValidationNominee() {

        var status = true;

        // if ($("#acc_tp").val() == "0") {
        //     status = false;
        //     $("#err_acc_tp").text("Empty field found!!");
        //     $("#acc_tp").focus();
        // } else $("#err_acc_tp").text("");

        if ($("#nominee_nm").val() == "") {
            status = false;
            $("#err_NOMINEE_NM").text("Empty field found!!");
            $("#nominee_nm").focus();

        } else if ($("#nominee_nm").val().length > 50) {
            status = false;
            $("#err_NOMINEE_NM").text("Name maximum 50 characters!!");
            $("#nominee_nm").focus();
        } else {
            $("#err_NOMINEE_NM").text("");
        }

        if ($("#percentages").val() == "") {
            status = false;
            $("#err_PERCENTAGES").text("Empty field found!!");
            $("#percentages").focus();
        } else if ($("#percentages").val().length > 5) {
            status = false;
            $("#err_PERCENTAGES").text("Percentages maximum 5 characters!!");
            $("#percentages").focus();

        } else if ($("#percentages").val() > 100) {
            status = false;
            $("#err_PERCENTAGES").text("Nominee Percentage can not accessed maximum 100%!!");
            $("#percentages").focus();

        } else if ($("#percentages").val() == '0') {
            status = false;
            $("#err_PERCENTAGES").text("Nominee Percentage can not be Zero!!");
            $("#percentages").focus();

        } else if (/[^0-9]/g.test($("#percentages").val()) == true) {
            status = false;
            $("#err_PERCENTAGES").text("Sorry,Illegal characters!!");
            $("#percentages").focus();
        } else {
            $("#err_PERCENTAGES").text("");
        }

        if ($("#relation_cd").val() == -1) {
            status = false;
            $("#err_RELATION_CD").text("Empty field found!!");
            $("#relation_cd").focus();
        } else $("#err_RELATION_CD").text("");

        if ($("#accountno").val().length > 30) {
            status = false;
            $("#err_ACCOUNT_NO").text("Account No maximum 30 characters!!");
            $("#accountno").focus();
        } else $("#err_ACCOUNT_NO").text("");

        return status;
    }

    function refresh() {

        var educationTable = $('#nominee_tbl_id');

        $
            .ajax({
                type: "GET",
                url: "/hrm-dev/nomineeListInfo",
                success: function (data) {
                    var bankName = "";
                    var bank_br_Name = "";
                    var accountNo = "";
                    var accountType = "";
                    var no = 1;
                    var tableBody = "";
                    $('#nominee_tbl_id tbody').empty();
                    $.each(data, function (idx, elem) {
                        var action =
                            '<button class="btn btn-success childrenInformationEdit" value="Edit" id="editNominee"> ' +
                            '<i class="fa fa-pencil"></i></button> ';

                        if (elem[5] == null) {
                            accountNo = '';
                        } else {
                            accountNo = elem[5];
                        }

                        if (elem[6] == null) {
                            accountType = '';
                        } else {
                            accountType = elem[6];
                        }

                        if (elem[8] == null) {
                            bankName = '';
                        } else {
                            bankName = elem[8];
                        }

                        if (elem[10] == null) {
                            bank_br_Name = '';
                        } else {
                            bank_br_Name = elem[10];
                        }
                        tableBody += "<tr'>";
                        tableBody += "<td>" + elem[1] + "</td>";
                        tableBody += "<td>" + elem[3] + "</td>";
                        tableBody += "<td>" + elem[4] + "</td>";
                        tableBody += "<td>" + accountNo + "</td>";
                        tableBody += "<td>" + accountType + "</td>";
                        tableBody += "<td>" + bankName + "</td>";
                        tableBody += "<td>" + bank_br_Name + "</td>";
                        tableBody += "<td hidden>" + elem[0] + "</td>";
                        tableBody += "<td hidden>" + elem[2] + "</td>";
                        tableBody += "<td hidden>" + elem[7] + "</td>";
                        tableBody += "<td hidden>" + elem[9] + "</td>";
                        tableBody += "<td hidden>" + elem[11] + "</td>";
                        tableBody += "<td>" + action + "</td>"
                        tableBody += "<tr>";
                    });
                    educationTable.append(tableBody);
                }
            });
    }

    $('#nominee_tbl_id tbody').on('click', '#editNominee', function () {

        $('#btnaddagentNominee').text("Update");

        var curRow = $(this).closest('tr');
        var agentId = curRow.find('td:eq(7)').text();
        var releationCD = curRow.find('td:eq(8)').text();
        var percentage = curRow.find('td:eq(2)').text();
        var accountNo = curRow.find('td:eq(3)').text();
        var slNo = curRow.find('td:eq(11)').text();
        var nomineeName = curRow.find('td:eq(0)').text();
        var accountType = curRow.find('td:eq(4)').text();
        var bankCd = curRow.find('td:eq(9)').text();
        var branceCD = curRow.find('td:eq(10)').text();
        var branceName = curRow.find('td:eq(6)').text();

        if (bankCd == 'null') {
            bankCd = '0';
        }
        if (branceCD == 'null') {
            branceCD = '0';
        }

        if (accountType == '') {
            accountType = '0';
        }

        $('#updateeduId').val(agentId);
        $('#slno').val(slNo);
        $('#accountno').val(accountNo);
        $('#relation_cd').val(releationCD);
        $('#percentages').val(percentage);
        $('#acc_tp').val(accountType);

        $('#nomineeacc_bank').val(bankCd).select2();
        $('#nomineeacc_bank_br').val(branceCD).select2();

        var getData = $('#nomineeacc_bank_br');
        getData.empty();
        getData.append($('<option/>', {
            value: branceCD,
            text: branceName
        }));
        $('#nominee_nm').val(nomineeName);

    });

    $("#btnagentNomineeRefresh").click(function () {

        clearFrom();

    });

    function clearFrom() {
        $('#updateeduId').val("");
        $('#slno').val("");
        $('#accountno').val("");
        $('#relation_cd').val("-1");
        $('#percentages').val("");
        $('#acc_tp').val("0");
        $('#nomineeacc_bank').val("0").select2();
        $('#nomineeacc_bank_br').val("").select2();
        $('#nominee_nm').val("");
        //error remove
        $("#err_acc_tp").text("");
        $("#err_ACC_BANK_CD").text("");
        $("#err_ACC_BR_CD").text("");
        $("#err_NOMINEE_NM").text("");
        $("#err_PERCENTAGES").text("");
        $("#err_RELATION_CD").text("");
        $("#err_ACCOUNT_NO").text("");
        $('#btnaddagentNominee').text("Save");

    }

});