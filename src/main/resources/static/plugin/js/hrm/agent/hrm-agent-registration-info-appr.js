$(document).ready(function () {

    $("#btnlicenseSave").hide();
    $("#btnlicenseRefresh").hide();
    $("#btnlicenseReject").hide();

    var table = $("#tblAgentRegistration").DataTable({
        "processing": true,
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>',
        },
        "pageLength": 10,
        ajax: {
            "url": "/hrm-dev/getLicenseList",
            "type": "GET",
            "dataType": "json"
        },
        "autoWidth": true,
        "columns": [
            {
                "data": "agentId",
                "name": "AGENT_ID"
            },
            {"data": "slNo", "name": "SL_NO"},
            {
                "data": "licenseTp",
                "name": "LICENSE_TP",
                "render": function (data,
                                    type,
                                    row) {
                    return data == 'e' ? '' : data;
                }
            },
            {"data": "applicationNo", "name": "APPL_NO"},
            {"data": "moneyReceipt", "name": "MR_VR_REF_NO"},
            {
                "data": "applicationDT",
                "name": "APPL_DT",
                "render": function (data,
                                    type,
                                    row) {
                    return data == null ? '' : dateFormat(data);
                }
            },
            {
                "data": "licenseDtFrom",
                "name": "LICENSE_DT_FROM",
                "render": function (data,
                                    type,
                                    row) {
                    return data == null ? '' : dateFormat(data);
                }
            },
            {
                "data": "licenseDtTo",
                "name": "LICENSE_DT_TO",
                "render": function (data,
                                    type,
                                    row) {
                    return data == null ? '' : dateFormat(data);
                }
            },
            {"data": "approveStatus", "name": "APPRV_STATUS"},
            {
                "data": "licenseNo",
                "name": "LICENSE_NO",
                "visible": false
            },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="editBtnLicense" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></button>';
                }
            }
        ],
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
            var values = Object.values(aData);
            // $('td:eq(0)', nRow).attr("data-id", values[0]);
            $('td:eq(8)', nRow).attr("data-id", values[9]);
        }
    });

    $("#btnlicenseReject").click(function () {

        if (dataValidation()) {
            var slNo = $('#slNo').val();
            var licenseTp = $('#licenseTp').val();
            var applicationNo = $('#applicationNo').val();
            var agentId = $('#agentId').val();
            var fatherNm = $('#fatherNm').val();
            var birthDT = $('#birthDT').val();
            var religionCd = $('#religionCd').val();
            var licenseDateFrom = $('#licenseDateFrom').val();
            var mrvrRefNo = $('#mrvrRefNo').val();
            var appldt = $('#appldt').val();
            var agentNmEng = $('#agentNmEng').val();
            var motherNm = $('#motherNm').val();
            var sexCd = $('#sexCd').val();
            var nationality = $('#nationality').val();
            var licenseDateTO = $('#licenseDateTO').val();
            var licenseNo = $('#licenseNo').val();
            var remarks = $('#remarks').val();
            var uUSR;

            if (birthDT) {
                birthDT = birthDT.split("/").reverse().join("/");
                birthDT = getFormateDate(birthDT);
            }
            if (appldt) {
                appldt = appldt.split("/").reverse().join("/");
                appldt = getFormateDate(appldt);
            }
            if (licenseDateFrom) {
                licenseDateFrom = licenseDateFrom.split("/").reverse().join("/");
                licenseDateFrom = getFormateDate(licenseDateFrom);
            }
            if (licenseDateTO) {
                licenseDateTO = licenseDateTO.split("/").reverse().join("/");
                licenseDateTO = getFormateDate(licenseDateTO);
            }
            var licenseList = {};
            licenseList.slNo = slNo;
            licenseList.licenseTp = licenseTp;
            licenseList.applicationNo = applicationNo;
            licenseList.agentId = agentId;
            licenseList.fatherNm = fatherNm;
            licenseList.birthDT = birthDT;
            licenseList.religionCd = religionCd;
            licenseList.licenseDateFrom = licenseDateFrom;
            licenseList.mrvrRefNo = mrvrRefNo;
            licenseList.appldt = appldt;
            licenseList.agentNmEng = agentNmEng;
            licenseList.motherNm = motherNm;
            licenseList.sexCd = sexCd;
            licenseList.nationality = nationality;
            licenseList.licenseDateTO = licenseDateTO;
            licenseList.licenseNo = licenseNo;
            licenseList.remarks = remarks;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-dev/rejectLicensesInfo",
                data: JSON.stringify(licenseList),
                dataType: 'json',
                success: function (data) {
                    if (data != 0) {
                        showAlertByType('Agent Application Rejected!!', "S");
                        clrFrom();
                        table.ajax.reload();
                        var agentId = '';
                        var slNo = '';
                        agentId = data.agentId;
                        slNo = data.slNo;
                        //window.open('/hrm-dev/rejectedAgentLicense.pdf/' + agentId +"/"+slNo, '_blank');
                         var rejectAgentURL='/hrm-dev/rejectedAgentLicense.pdf?agentId=' + agentId + "&slNo=" + slNo;
                         window.open(encodeURIComponent(rejectAgentURL),'_blank');
                    } else {
                        showAlertByType("Sorry,Database problem!!", "F");
                    }
                },
                error: function (e) {
                    showAlertByType("Sorry,Something Wrong!!", "F");
                }
            });
        }
    });

    $("#btnlicenseSave").click(function () {

        if (dataValidation()) {

            var slNo = $('#slNo').val();
            var licenseTp = $('#licenseTp').val();
            var applicationNo = $('#applicationNo').val();
            var agentId = $('#agentId').val();
            var fatherNm = $('#fatherNm').val();
            var birthDT = $('#birthDT').val();
            var religionCd = $('#religionCd').val();
            var licenseDateFrom = $('#licenseDateFrom').val();
            var mrvrRefNo = $('#mrvrRefNo').val();
            var appldt = $('#appldt').val();
            var agentNmEng = $('#agentNmEng').val();
            var motherNm = $('#motherNm').val();
            var sexCd = $('#sexCd').val();
            var nationality = $('#nationality').val();
            var licenseDateTO = $('#licenseDateTO').val();
            var licenseNo = $('#licenseNo').val();
            var remarks = $('#remarks').val();

            if (birthDT) {
                birthDT = birthDT.split("/").reverse().join("/");
                birthDT = getFormateDate(birthDT);
            }
            if (appldt) {
                appldt = appldt.split("/").reverse().join("/");
                appldt = getFormateDate(appldt);
            }
            if (licenseDateFrom) {
                licenseDateFrom = licenseDateFrom.split("/").reverse().join("/");
                licenseDateFrom = getFormateDate(licenseDateFrom);
            }
            if (licenseDateTO) {
                licenseDateTO = licenseDateTO.split("/").reverse().join("/");
                licenseDateTO = getFormateDate(licenseDateTO);
            }
            var licenseList = {};
            licenseList.slNo = slNo;
            licenseList.licenseTp = licenseTp;
            licenseList.applicationNo = applicationNo;
            licenseList.agentId = agentId;
            licenseList.fatherNm = fatherNm;
            licenseList.birthDT = birthDT;
            licenseList.religionCd = religionCd;
            licenseList.licenseDateFrom = licenseDateFrom;
            licenseList.mrvrRefNo = mrvrRefNo;
            licenseList.appldt = appldt;
            licenseList.agentNmEng = agentNmEng;
            licenseList.motherNm = motherNm;
            licenseList.sexCd = sexCd;
            licenseList.nationality = nationality;
            licenseList.licenseDateTO = licenseDateTO;
            licenseList.licenseNo = licenseNo;
            licenseList.remarks = remarks;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-dev/updateLicensesInfo",
                data: JSON.stringify(licenseList),
                dataType: 'json',
                success: function (data) {
                    if (data != 0) {
                        showAlertByType('Agent Application Approved successfully!!', "S");
                        clrFrom();
                        table.ajax.reload();
                    } else {
                        showAlertByType("Sorry,Database problem!!", "F");
                    }
                    // table.row.add({
                    //     "agentId": $("#agentId").val(),
                    //     "slNo": data,
                    //     "licenseTp": $("#licenseTp").val(),
                    //     "licenseNo": $("#licenseNo").val(),
                    //     "applicationNo": $("#applicationNo").val(),
                    //     "moneyReceipt": $("#mrvrRefNo").val(),
                    //     "applicationDT": $("#appldt").val(),
                    //     "licenseDtFrom": dateFormat($("#licenseDateFrom").val()),
                    //     "licenseDtTo": dateFormat($("#licenseDateTO").val()),
                    //     "approveStatus": '0',
                    //     "button": '<button id="editBtnLicense" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></button>',
                    //     "button": '<button id="deleteLicense" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>'
                    // }).draw();
                },
                error: function (e) {
                    showAlertByType("Sorry,Something Wrong!!", "F");
                }
            });
        }
    });


    function dataValidation() {

        var status = true;

        if ($("#remarks").val() == "") {
            status = false;
            $("#error_remarks").text("Empty field found!!");
            $("#remarks").focus();
        } else if ($("#remarks").val().length > 200) {
            status = false;
            $("#error_remarks").text("Allow maximum 200 character!!");
            $("#remarks").focus();
        } else $("#error_remarks").text("");


        return status;
    }


    $("#btnlicenseRefresh").click(function () {
        clrFrom();
    });

    function clrFrom() {
        $("#btnlicenseSave").hide();
        $("#btnlicenseRefresh").hide();
        $("#btnlicenseReject").hide();
        $('#slNo').val("");
        $('#licenseTp').val("-1");
        $('#applicationNo').val("");
        $('#agentId').val("");
        $('#fatherNm').val("");
        $('#birthDT').val("");
        $('#religionCd').val("");
        $('#licenseDateFrom').val("");
        $('#mrvrRefNo').val("");
        $('#appldt').val("");
        $('#agentNmEng').val("");
        $('#motherNm').val("");
        $('#sexCd').val("");
        $('#nationality').val("");
        $('#licenseDateTO').val("");
        $('#licenseNo').val("");
        //new
        $("#religionName").val("");
        $("#sexName").val("");
        $("#nationalityName").val("");
        $('#remarks').val("");
        //clr error
        clrErrorMsg();
    }

    function getFormateDate(date) {
        var d = new Date(date);
        return d;
    }

    function dateFormat(dateObject) {

        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    function validationDateRange(fromdate, todate) {
        var lessStatus = false;
        //release date
        var date_r = fromdate.substring(0, 2);
        var month_r = fromdate.substring(3, 5);
        var year_r = fromdate.substring(6, 10);
        var fromdate_date = new Date(year_r, month_r - 1, date_r);
// orderDate
        var date_o = todate.substring(0, 2);
        var month_o = todate.substring(3, 5);
        var year_o = todate.substring(6, 10);
        var todate_date = new Date(year_o, month_o - 1, date_o);

        if (fromdate_date > todate_date) {
            lessStatus = true;
        }
        return lessStatus;
    }


    $('#tblAgentRegistration tbody').on('click', '#editBtnLicense', function () {
        $("#btnlicenseSave").show();
        $("#btnlicenseRefresh").show();
        $("#btnlicenseReject").show();
        clrErrorMsg();
        var curRow = $(this).closest('tr');
        var agentId = curRow.find('td:eq(0)').text();
        var licenseNo = curRow.find('td:eq(8)').attr("data-id");
        var slNo = curRow.find('td:eq(1)').text();
        var regType = curRow.find('td:eq(2)').text();
        var appNo = curRow.find('td:eq(3)').text();
        var moneyReceipt = curRow.find('td:eq(4)').text();
        var appDate = curRow.find('td:eq(5)').text();
        var startDate = curRow.find('td:eq(6)').text();
        var endDate = curRow.find('td:eq(7)').text();

        // var idx = table.row(curRow).index();
        // alert('row index= ' + idx);
        // var rowindex = curRow.index();
        // $('#countRow').val(rowindex);

        if (regType == '') {
            regType = '-1';
        }
        $('#agentId').val(agentId);
        $('#slNo').val(slNo);
        $('#licenseTp').val(regType);
        $('#applicationNo').val(appNo);
        $('#appldt').val(appDate);
        $('#licenseDateFrom').val(startDate);
        $('#licenseDateTO').val(endDate);
        $('#mrvrRefNo').val(moneyReceipt);
        $('#licenseNo').val(licenseNo);

        if (agentId != '') {

            $.ajax({
                url: "/hrm-dev/getAgentList/" + agentId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var agentName = "";
                    var birthDate = "";
                    var gender = "";
                    var religion = "";
                    var fatherName = "";
                    var motherName = "";
                    var countryName = "";
                    //
                    var genderName = "";
                    var religionName = "";
                    var nationalityName = "";

                    $.each(response, function (i, l) {
                        agentName = l[1];
                        birthDate = l[2];
                        gender = l[3];
                        religion = l[5];
                        fatherName = l[7];
                        motherName = l[8];
                        countryName = l[10];
                        //new
                        genderName = l[4];
                        religionName = l[6];
                        nationalityName = l[9];
                    });

                    if (agentName == null) {
                        agentName = "";
                    }
                    if (gender == null) {
                        gender = '';
                        genderName = '';
                    }
                    if (religion == null) {
                        religion = "";
                        religionName = "";
                    }
                    if (fatherName == null) {
                        fatherName = "";
                    }
                    if (motherName == null) {
                        motherName = "";
                    }
                    if (countryName == null) {
                        countryName = "";
                        nationalityName = "";
                    }
                    $("#agentNmEng").val(agentName);
                    $("#sexCd").val(gender);
                    $("#religionCd").val(religion);
                    $("#fatherNm").val(fatherName);
                    $("#motherNm").val(motherName);
                    $("#nationality").val(countryName);

                    $("#religionName").val(religionName);
                    $("#sexName").val(genderName);
                    $("#nationalityName").val(nationalityName);


                    if (birthDate == null || birthDate == "") {
                        $("#birthDT").val("");
                    } else {
                        $("#birthDT").val(dateFormat(birthDate));
                    }

                    $("#error_agentId").text("");
                },
                error: function (xhr, status, error) {
                    $("#agentNmEng").val("");
                    $("#birthDT").val("");
                    $("#sexCd").val("");
                    $("#religionCd").val("");
                    $("#fatherNm").val("");
                    $("#motherNm").val("");
                    $("#nationality").val("");
                    //new
                    $("#religionName").val("");
                    $("#sexName").val("");
                    $("#nationalityName").val("");
                    $("#error_agentId").text("Sorry! Agent ID not match!!");
                }
            });
        } else {
            showAlertByType('Sorry,Agent Id should not be empty!', "W");
        }

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });


    function clrErrorMsg() {
        $("#error_licenseDateTO").text("");
        $("#error_appldt").text("");
        $("#error_licenseDateFrom").text("");
        $("#error_MR_VR_REF_NO").text("");
        $("#error_applicationNo").text("");
        $("#error_licenseTp").text("");
        $("#error_agentId").text("");
        $("#error_licenseNo").text("");
        $("#error_remarks").text("");
        //
    }

});