/**
 * Created by golam nobi.
 */
$(document).ready(
    function () {
        function readURLPhoto(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#emp_photo_view').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $("#err-emp_photo_id").text("");
            }
        }

        function readURLSig(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#emp_sig_view').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $("#err-emp_sig_id").text("");
            }
        }

        $(document).on(
            "change",
            "#photofileName",
            function (e) {
                var file = this.files[0];
                var fileType = file["type"];
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    $("#err-emp_photo_id").text(
                        "Only gif/jpeg/png is allowed!");
                }
                else {
                    if (this.files[0].size > 1048576) {//200KB
                        $("#err-emp_photo_id").text(
                            "Photo size can't be greater than 1MB!");
                    } else {
                        $("#hasPhoto").val("false");
                        readURLPhoto(this);
                    }
                }
            });

        $("#signfilename").change(
            function () {
                var file = this.files[0];
                var fileType = file["type"];
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    $("#err-emp_sig_id").text(
                        "Only gif/jpeg/png is allowed!");
                }
                else {
                    if (this.files[0].size > 1048576) {
                        $("#err-emp_sig_id").text(
                            "Photo size can't be greater than 1MB!");
                    } else {
                        $("#hasSignature").val("false");
                        readURLSig(this);
                    }
                }
            });


        function genInfovalidate() {

            var status = true;

            if ($("#photofileName").val() == '' && $("#hasPhoto").val()=="false") {

                $("#err-emp_photo_id").text("Empty field found!!");
                return false;
            } else     if ($("#signfilename").val() == '' && !$("#hasSignature").val()=="false") {

                $("#err-emp_sig_id").text("Empty field found!!");
                return false;
            }
            else{
                $("#err-emp_photo_id").text('');
                return true;
            }
        }

        $(document).on("click", "#btnSaveImage", function (e) {
            var status = genInfovalidate();
            if (status) {
                $.ajax({
                    url: "agent-imageupload-file",
                    type: "POST",
                    data: new FormData($("#emp_general_form")[0]),
                    enctype: 'multipart/form-data',
                    async: false,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    cache: false,
                    success: function (res) {
                        showAlertByType('Save successfully!!', "S");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        showAlertByType('Something Wrong!', "F");
                    }
                });
            }

        });

        $("#btnRefreshImage").click(function () {
            $("#err-emp_photo_id").text("");
            $("#err-emp_sig_id").text("");
            $("#photofileName").val("");
            $("#signfilename").val("");
            $("#hasSignature").val(false);
            $("#hasSignature").val(false);
        });
    });