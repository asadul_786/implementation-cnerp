/**
 * Created by golam nobi on 2/02/2020.
 */
$(document).ready(function () {

    var getgid = $("#dev_emp_gid").val();

    if (getgid != '0' && getgid != '') {
        getHrmEmployeeDetailsbyGid($("#dev_emp_gid").val())
    }

    var pathname = window.location.pathname;
    var short_path_name = pathname.split('/')[2];

    if (short_path_name === "agent-address") {
        $("#agent_info_address1").addClass('active');
    }
    else if (short_path_name === "agent-personal-details") {
        $("#agent_personal-details1").addClass('active');
    }
    else if (short_path_name === "agent-education") {
        $("#agent_education").addClass('active');

    }
    else if (short_path_name === "agent-nominee") {
        $("#agent-nominee").addClass('active');
    }
    else if (short_path_name === "agent-image") {
        $("#agent-image").addClass('active');
    }

    if ($("#updateaqgentid").val() != "") {
        $("#emp_tab").show();
        $('#agentId').prop('readonly', true);
    }

    $("#agent_master_add").click(function () {

        var flag = dataValidationMaster();

        if (flag == true) {

            var agentId = $('#agentId').val();
            var salut = $('#salut').val();
            var agent_nm_eng = $('#agent_nm_eng').val();
            var agent_nm_bng = $('#agent_nm_bng').val();
            var empid = $('#empid').val();
            var office_cd = $('#office_cd').val();
            var religion_cd = $('#religion_cd').val();
            var marital_stat_cd = $('#marital_stat_cd').val();
            var activity_cd = $('#activity_cd').val();
            var license_no = $('#license_no').val();
            var account_no = $('#account_no').val();
            var acc_type = $('#acc_type').val();
            var acc_bank_cd = $('#acc_bank_cd').val();
            var acc_br_cd = $('#acc_br_cd').val();
            var dev_emp_gid = $('#dev_emp_gid').val();
            //date formate
            var birth_dt = $('#birth_dt').val();
            var join_dt = $('#join_dt').val();
            var license_valid_from_dt = $('#license_valid_from_dt').val();
            var license_valid_to_dt = $('#license_valid_to_dt').val();

            if (birth_dt) {
                birth_dt = birth_dt.split("/").reverse().join("/");
                birth_dt = getFormateDate(birth_dt);
            }

            if (join_dt) {
                join_dt = join_dt.split("/").reverse().join("/");
                join_dt = getFormateDate(join_dt);
            }

            if (license_valid_from_dt) {
                license_valid_from_dt = license_valid_from_dt.split("/").reverse().join("/");
                license_valid_from_dt = getFormateDate(license_valid_from_dt);
            }

            if (license_valid_to_dt) {
                license_valid_to_dt = license_valid_to_dt.split("/").reverse().join("/");
                license_valid_to_dt = getFormateDate(license_valid_to_dt);
            }

            function getFormateDate(date) {
                var d = new Date(date);
                return d;
            }

            var agentMaster = {};

            agentMaster.agentId = agentId;
            agentMaster.salut = salut;
            agentMaster.agent_nm_eng = agent_nm_eng;
            agentMaster.agent_nm_bng = agent_nm_bng;
            agentMaster.birth_dt = birth_dt;
            agentMaster.join_dt = join_dt;
            agentMaster.empid = empid;
            agentMaster.office_cd = office_cd;
            agentMaster.religion_cd = religion_cd;
            agentMaster.marital_stat_cd = marital_stat_cd;
            agentMaster.activity_cd = activity_cd;
            agentMaster.license_no = license_no;
            agentMaster.license_valid_from_dt = license_valid_from_dt;
            agentMaster.license_valid_to_dt = license_valid_to_dt;
            agentMaster.account_no = account_no;
            agentMaster.acc_type = acc_type;
            agentMaster.acc_bank_cd = acc_bank_cd;
            agentMaster.acc_br_cd = acc_br_cd;
            agentMaster.dev_emp_gid = dev_emp_gid;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-dev/addagentMasterInfo",
                data: JSON.stringify(agentMaster),
                dataType: 'json',
                success: function (data) {
                    $('#updateaqgentid').val(data);
                    $('#agentId').prop('readonly', true);
                    $("#emp_tab").show();
                    showAlertByType('Save successfully!!', "S");
                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");
                }
            });

        }

    });

    //validation
    function dataValidationMaster() {

        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($("#agentId").val() == "") {
            status = false;
            $("#err_agent_id").text("Empty field found!!");
            $("#agentId").focus();

        } else if ($("#agentId").val().length != 8) {
            status = false;
            $("#err_agent_id").text("ID must be 8 characters!!");
            $("#agentId").focus();
        } else {
            $("#err_agent_id").text("");
        }

        if ($("#agent_nm_eng").val() == "") {
            status = false;
            $("#err_agent_nm_eng").text("Empty field found!!");
            $("#agent_nm_eng").focus();
        } else $("#err_agent_nm_eng").text("");

        if ($("#empid").val() == "") {
            status = false;
            $("#err_empid").text("Empty field found!!");
            $("#empid").focus();
        } else $("#err_empid").text("");


        if ($("#account_no").val().length > 30) {
            status = false;
            $("#err_account_no").text("Account No maximum 30 characters!!");
            $("#account_no").focus();
        } else $("#err_account_no").text("");


        if ($("#license_no").val() != "") {
            if ($("#license_no").val().length > 20) {
                status = false;
                $("#err_license_no").text("Maximum 20 character allow!!");
                $("#license_no").focus();
            } else $("#err_license_no").text("");
        }

        if ($("#marital_stat_cd").val() == -1) {
            status = false;
            $("#err_marital_stat_cd").text("Empty field found!!");
            $("#marital_stat_cd").focus();
        } else $("#err_marital_stat_cd").text("");


        if ($("#license_valid_from_dt").val() != "") {

            if (!dateCheck.test($("#license_valid_from_dt").val())) {
                status = false;
                $("#err_license_valid_from_dt1").text("Invalid Date format!!");
                $("#license_valid_from_dt").focus();
            }

        } else $("#err_license_valid_from_dt1").text("");


        if ($("#license_valid_to_dt").val() != "") {

            if (!dateCheck.test($("#license_valid_to_dt").val())) {
                status = false;
                $("#err_license_valid_to_dt").text("Invalid Date format!!");
                $("#license_valid_to_dt").focus();

            } else if (isValidDate($("#license_valid_to_dt").val()) == false) {
                status = false;
                $("#err_license_valid_to_dt").text("Invalid date format!!");
                $("#license_valid_to_dt").focus();


            } else $("#err_license_valid_to_dt").text("");

        } else $("#err_license_valid_to_dt").text("");


        if ($("#license_valid_from_dt").val() != "" && $("#license_valid_to_dt").val() != "") {

            if (lessthenOrderDate($("#license_valid_from_dt").val(), $("#license_valid_to_dt").val())) {
                status = false;
                $("#err_license_valid_from_dt").text("From date should be less than To date!!");
                $("#license_valid_from_dt").focus();
            } else $("#err_license_valid_from_dt").text("");

        }

        if ($("#birth_dt").val() != "") {

            if (!dateCheck.test($("#birth_dt").val())) {
                status = false;
                $("#err_birth_dt").text("Invalid Date format!!");
                $("#birth_dt").focus();

            } else if (isValidDate($("#birth_dt").val()) == false) {
                status = false;
                $("#err_birth_dt").text("Invalid date format!!");
                $("#birth_dt").focus();

            } else if (checkvalid_JoinDate($("#birth_dt").val())) {
                status = false;
                $("#err_birth_dt").text("Birth date should not be greater than the current Date!!");
                $("#birth_dt").focus();

            } else $("#err_birth_dt").text("");

        } else $("#err_birth_dt").text("");

        if ($("#join_dt").val() != "") {
            if (!dateCheck.test($("#join_dt").val())) {
                status = false;
                $("#err_join_dt").text("Invalid Date format!!");
                $("#join_dt").focus();

            } else if (isValidDate($("#join_dt").val()) == false) {
                status = false;
                $("#err_join_dt").text("Invalid date format!!");
                $("#join_dt").focus();

            } else $("#err_join_dt").text("");

        } else $("#err_join_dt").text("");

        return status;
    }


    $(document).on("input", "#agentId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');
        var agentId = $('#agentId').val().trim();

        if (agentId != "" && agentId.length < 9) {

            $.ajax({
                url: "/hrm-dev/check-agent-id/" + agentId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    if (response == true) {
                        $("#err_agent_id").text("This Agent ID already exists!");
                        // $("#emp_master_add").prop('disabled', true);
                    } else {
                        $("#err_agent_id").text("");
                        // $("#emp_master_add").prop('disabled', false);
                    }
                },
                error: function (xhr, status, error) {
                    showAlertByType('Something Wrong!', "F");
                }
            });
        }
        else {

            if (agentId == "") {
                $("#err_agent_id").text("Agent ID required!");
            }
            else {
                $("#err_agent_id").text("ID maximum 8 characters!");
            }
        }

    });

    //search employee id

    $(document).on("input", "#empid", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var empid = $('#empid').val();

        if (empid != "" && empid.length < 9) {
            $.ajax({
                url: "/hrm-dev/gethrmAgentDetails/" + empid,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var empGid = "";
                    var empName = "";
                    var officeCd = "";
                    var officeName = "";
                    var designatinCD = "";
                    var designationName = "";
                    $.each(response, function (i, l) {
                        empGid = l[0];
                        empName = l[3];
                        officeCd = l[1];
                        officeName = l[9];
                        designatinCD = l[4];
                        designationName = l[11];
                    });
                    //set the office cd

                    $("#empName").val(empName);
                    $("#dev_emp_gid").val(empGid);

                    var officeCdNM = $('#office_cd');
                    officeCdNM.empty();
                    officeCdNM.append($('<option/>', {
                        value: officeCd,
                        text: officeName
                    }));

                    // set the designation
                    var designatinCDDetails = $('#emp_designation');
                    designatinCDDetails.empty();
                    designatinCDDetails.append($('<option/>', {
                        value: designatinCD,
                        text: designationName
                    }));
                    $("#err_empid").text("");
                },
                error: function (xhr, status, error) {
                    $("#err_empid").text("Sorry! Employee ID not match!!");

                    $("#empName").val("");

                    var officeCd = $('#office_cd');
                    officeCd.empty();
                    officeCd.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Office --"
                    }));
                    // set the designation
                    var designatinCDDetails = $('#emp_designation');
                    designatinCDDetails.empty();
                    designatinCDDetails.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Designation --"
                    }));

                }

            });
        }
        else {

            if ($('#empid').val() == "") {
                $("#err_empid").text("Employee ID required!");

                $("#empName").val("");

                var officeCd = $('#office_cd');
                officeCd.empty();
                officeCd.append($('<option/>', {
                    value: "-1",
                    text: "-- Select Office --"
                }));
                // set the designation
                var designatinCDDetails = $('#emp_designation');
                designatinCDDetails.empty();
                designatinCDDetails.append($('<option/>', {
                    value: "-1",
                    text: "-- Select Designation --"
                }));

            }
            else {
                $("#err_empid").text("ID maximum 8 characters!");
                $("#empName").val("");
                var officeCd = $('#office_cd');
                officeCd.empty();
                officeCd.append($('<option/>', {
                    value: "",
                    text: ""
                }));
                // set the designation
                var designatinCDDetails = $('#emp_designation');
                designatinCDDetails.empty();
                designatinCDDetails.append($('<option/>', {
                    value: "",
                    text: ""
                }));
            }
        }

    });

    //for bank

    $(document).on("change", "#acc_bank_cd", function (e) {

        var bankCD = $("#acc_bank_cd option:selected").val();

        $.get("/hrm-admin/get-branchBank?bankCD=" + bankCD,

            function (data, status) {

                var office = $('#acc_br_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Branch Bank---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });

    $("#refresh_button_master").click(function () {

        $('#updateaqgentid').val("");
        $('#agentId').val("");
        $('#salut').val("-1");
        $('#agent_nm_eng').val("");
        $('#agent_nm_bng').val("");
        $('#birth_dt').val("");
        $('#join_dt').val("");
        $('#religion_cd').val("-1");
        $('#marital_stat_cd').val("-1");
        $('#activity_cd').val("-1");
        $('#license_no').val("");
        $('#license_valid_from_dt').val("");
        $('#license_valid_to_dt').val("");
        $('#account_no').val("");
        $('#acc_type').val("-1");
        $('#acc_bank_cd').val("-1").select2();
        $('#acc_br_cd').val("-1").select2();


        //for remove error text
        $("#err_agent_id").text("");
        $("#err_agent_nm_eng").text("");
        $("#err_account_no").text("");
        $("#err_license_no").text("");
        $("#err_marital_stat_cd").text("");
        $("#err_license_valid_from_dt").text("");
        $("#err_license_valid_to_dt").text("");
        $("#err_birth_dt").text("");
        $("#err_join_dt").text("");
        $("#err_empid").text("");
        $("#err_license_valid_from_dt1").text("");
        var officeCd = $('#office_cd');
        officeCd.empty();
        officeCd.append($('<option/>', {
            value: "-1",
            text: "-- Select Office --"
        }));
        // set the designation
        var designatinCDDetails = $('#emp_designation');
        designatinCDDetails.empty();
        designatinCDDetails.append($('<option/>', {
            value: "-1",
            text: "-- Select Designation --"
        }));

        $("#empid").val("");
        $("#empName").val("");
    });

    function getHrmEmployeeDetailsbyGid(empgid) {

        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: "/hrm-dev/get-getHrmEmployeeDetailsbyGid/" + empgid,
            dataType: 'json',
            success: function (data) {
                var empID = "";
                var empName = "";
                var officeCd = "";
                var officeName = "";
                var designatinCD = "";
                var designationName = "";
                $.each(data, function (i, l) {
                    empID = l[0];
                    officeCd = l[1];
                    officeName = l[2];
                    empName = l[3];
                    designatinCD = l[4];
                    designationName = l[5];
                });

                $("#empName").val(empName);
                $("#empid").val(empID);

                var officeCdNM = $('#office_cd');
                officeCdNM.empty();
                officeCdNM.append($('<option/>', {
                    value: officeCd,
                    text: officeName
                }));

                // set the designation
                var designatinCDDetails = $('#emp_designation');
                designatinCDDetails.empty();
                designatinCDDetails.append($('<option/>', {
                    value: designatinCD,
                    text: designationName
                }));
            },
            error: function (e) {
                showAlertByType("Sorry,Something Wrong!!", "F");
            }
        });
    }

    function checkvalid_JoinDate(enteredDate) {
        var status = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate > today) {
            status = true;
        }
        return status;
    }

    function lessthenOrderDate(releaseDate, orderDate) {
        var lessStatus = false;
        //release date
        var date_r = releaseDate.substring(0, 2);
        var month_r = releaseDate.substring(3, 5);
        var year_r = releaseDate.substring(6, 10);
        var release_date = new Date(year_r, month_r - 1, date_r);
        var date_o = orderDate.substring(0, 2);
        var month_o = orderDate.substring(3, 5);
        var year_o = orderDate.substring(6, 10);
        var order_date = new Date(year_o, month_o - 1, date_o);

        if (release_date > order_date) {
            lessStatus = true;
        }
        return lessStatus;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }


});








