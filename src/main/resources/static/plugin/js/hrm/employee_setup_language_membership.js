/**
 * Created by golam
 */
$(document).ready(function () {

    loadLanguageTable();
    loadMembershipTable();

    $("#emp_lang_add_button").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var languageCD = $('#languageCD').val();
            var lang_read = '1';
            var lang_write = '1';
            var lang_speak = '1';
            var comments = $('#comments').val();
//for id
            var emp_gid = $('#emp_gid').val();
            var i_dt = $('#i_dt').val();
            var i_usr = $('#i_usr').val();
            var competencyCd = $('#competencyCd').val();

            var language = {};
            language.languageCD = languageCD;
            language.lang_read = lang_read;
            language.lang_write = lang_write;
            language.lang_speak = lang_speak;
            language.comments = comments;
            //new
            language.emp_gid = emp_gid;
            language.i_dt = i_dt;
            language.i_usr = i_usr;
            language.competencyCd = competencyCd;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/addlanguageInfo",
                data: JSON.stringify(language),
                dataType: 'json',
                success: function (data) {
                    if (data == "0") {
                        showAlertByType('Save successfully!!', "S");
                    }
                    clearForm();
                    loadLanguageTable();
                },
                error: function (e) {
                    showAlertByType('Sorry,Something Wrong!!', "F");
                }
            });

        }

    });

//validation

    function dataValidation() {

        var status = true;

        if ($("#languageCD").val() == -1) {
            status = false;
            $("#checkErrorlanguage").text("Empty field found!!");
            $("#languageCD").focus();
        } else $("#checkErrorlanguage").text("");

        if ($("#comments").val().length > 100) {
            status = false;
            $("#checkErrorcomments").text("Allow maximum 100 characters!!");
            $("#comments").focus();
        } else $("#checkErrorcomments").text("");


        if ($("#competencyCd").val() == -1) {
            status = false;
            $("#competencyCdError").text("Empty field found!!");
            $("#competencyCd").focus();
        } else $("#competencyCdError").text("");

        return status;
    }


    $("#emp_member_add_button").click(function () {

        var flag = dataValidationMembers();

        if (flag == true) {

            var mem_org_cd = $('#mem_org_cd').val();
            var member_cd = $('#member_cd').val();
            var country_cd = $('#country_cd').val();
            var remarks = $('#remarks').val();
            //new
            var emp_gidm = $('#emp_gidm').val();
            var i_usrm = $('#i_usrm').val();
            var i_dtm = $('#i_dtm').val();

            var members = {};
            members.mem_org_cd = mem_org_cd;
            members.member_cd = member_cd;
            members.country_cd = country_cd;
            members.remarks = remarks;
            //
            members.emp_gidm = emp_gidm;
            members.i_usrm = i_usrm;
            members.i_dtm = i_dtm;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/addMemberInfo",
                data: JSON.stringify(members),
                dataType: 'json',
                success: function (data) {
                    if (data == "0") {
                        showAlertByType('Save successfully!!', "S");
                    }
                    clearFormMembers();
                    loadMembershipTable();
                },
                error: function (e) {
                    showAlertByType('Sorry,Something Wrong!!', "F");
                }
            });
        }

    });


    function dataValidationMembers() {

        var status = true;

        if ($("#mem_org_cd").val() == -1) {
            status = false;
            $("#mem_org_cdError").text("Empty field found!!");
            $("#mem_org_cd").focus();
        } else $("#mem_org_cdError").text("");


        if ($("#country_cd").val() == -1) {
            status = false;
            $("#country_cdError").text("Empty field found!!");
            $("#country_cd").focus();
        } else $("#country_cdError").text("");

        if ($("#member_cd").val() == -1) {
            status = false;
            $("#member_cdError").text("Empty field found!!");
            $("#member_cd").focus();
        } else $("#member_cdError").text("");


        if ($("#remarks").val().length > 50) {
            status = false;
            $("#remarksError").text("Allow maximum 50 characters!!");
            $("#remarks").focus();
        } else $("#remarksError").text("");

        return status;
    }

    function clearFormMembers() {
        $('#mem_org_cd').val(-1);
        $('#member_cd').val(-1);
        $('#country_cd').val(-1);
        $('#remarks').val("");
        $("#checkErrorlanguage").text("");
        $("#checkErrorcomments").text("");
        $("#competencyCdError").text("");
        $('#mem_org_cd').attr("disabled", false);
        $('#emp_gidm').val("");
        $('#i_dtm').val("");
        $('#i_usrm').val("");
        $('#emp_member_add_button').text("Save");
    }

    function clearForm() {
        $('#languageCD').attr("disabled", false);
        $('#languageCD').val(-1);
        $('#lang_read').val("");
        $('#lang_write').val("");
        $('#lang_speak').val("");
        $('#comments').val("");
        $('#emp_gid').val("");
        $('#i_dt').val("");
        $('#i_usr').val("");
        $('#competencyCd').val("-1");
        $('#emp_lang_add_button').text("Save");
    }

    //for membership

    $('#emp_membership_tbl_id tbody').on('click', '#editMembership', function () {

        $("#mem_org_cdError").text("");
        $("#country_cdError").text("");
        $("#member_cdError").text("");

        $('#mem_org_cd').attr("disabled", true);

        $('#emp_member_add_button').text("Update");

        var curRow = $(this).closest('tr');
        var membershiporganizationCode = curRow.find('td:eq(0)').text();
        var membershipCode = curRow.find('td:eq(1)').text();
        var empGid = curRow.find('td:eq(2)').text();
        var i_usr = curRow.find('td:eq(3)').text();
        var i_dt = curRow.find('td:eq(4)').text();
        var countryCd = curRow.find('td:eq(5)').text();
        var remarks = curRow.find('td:eq(8)').text();
        //
        $('#mem_org_cd').val(membershiporganizationCode);
        $('#member_cd').val(membershipCode);
        $('#remarks').val(remarks);
        $('#country_cd').val(countryCd);
        //
        $('#emp_gidm').val(empGid);
        $('#i_usrm').val(i_usr);
        $('#i_dtm').val(i_dt);

        $('html, body').animate({
            scrollTop: $('#scroll1').offset().top
        }, 500);

        getDateFormember();

    });

    function getDateFormember() {
        var dateStr = $('#i_dtm').val();
        var dateCom = getDate(dateStr);
        $('#i_dtm').val(dateCom);
    }

    //for language
    $('#tableLangeuage tbody').on('click', '#editLanguage', function () {

        $('#languageCD').attr("disabled", true);
        $('#emp_lang_add_button').text("Update");
        $("#checkErrorlanguage").text("");
        $("#checkErrorcomments").text("");
        $("#competencyCdError").text("");

        var curRow = $(this).closest('tr');
        var langualgeCode = curRow.find('td:eq(0)').text();
        var ReadCode = curRow.find('td:eq(1)').text();
        var writeCode = curRow.find('td:eq(2)').text();
        var speackCode = curRow.find('td:eq(3)').text();
        var comment = curRow.find('td:eq(8)').text();
        var empGid = curRow.find('td:eq(9)').text();
        var i_usr = curRow.find('td:eq(10)').text();
        var i_dt = curRow.find('td:eq(11)').text();
        var comte = curRow.find('td:eq(12)').text();
        //
        $('#languageCD').val(langualgeCode);
        $('#comments').val(comment);
        //
        $('#emp_gid').val(empGid);
        $('#i_usr').val(i_usr);
        $('#i_dt').val(i_dt);
        $('#competencyCd').val(comte);

        // if (ReadCode == 1) {
        //     $("#lang_read").prop('checked', true);
        //     $("#lang_read").val(1);
        // } else {
        //     $("#lang_read").prop('checked', false);
        // }
        //
        // if (writeCode == 1) {
        //     $("#lang_write").prop('checked', true);
        //     $("#lang_write").val(1);
        // } else {
        //     $("#lang_write").prop('checked', false);
        // }
        //
        // if (speackCode == 1) {
        //     $("#lang_speak").prop('checked', true);
        //     $("#lang_speak").val(1);
        // } else {
        //     $("#lang_speak").prop('checked', false);
        // }

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

        getDateFor();

    });

    function getDateFor() {
        var dateStr = $('#i_dt').val();
        var dateCom = getDate(dateStr);
        $('#i_dt').val(dateCom);
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    };


    $("#emp_lang_refresh_button").click(function () {
        clearForm();
        $("#checkErrorlanguage").text("");
        $("#checkErrorcomments").text("");
        $("#competencyCdError").text("");
        $("#checkboxError").text("");
        $("#checkboxError").text("");
        $("#lang_read").prop('checked', false);
        $("#lang_write").prop('checked', false);
        $("#lang_speak").prop('checked', false);
        $('#languageCD').attr("disabled", false);
        $('#competencyCd').val("-1");
        $('#emp_lang_add_button').text("Save");
    });

    $("#emp_member_refresh_button").click(function () {
        clearFormMembers();
        $("#member_cdError").text("");
        $("#country_cdError").text("");
        $("#mem_org_cdError").text("");
        $("#remarksError").text("");
        $('#mem_org_cd').attr("disabled", false);
        $('#emp_member_add_button').text("Save");
    });

    function loadLanguageTable() {

        var tableLangeuage = $('#tableLangeuage');

        $
            .ajax({
                type: "GET",
                url: "/hrm-admin/emp_LanguageListInfo",
                success: function (data) {
                    var tableBody = "";
                    var can_read = "";
                    var can_write = "";
                    var can_speak = "";
                    var comments = "";
                    $('#tableLangeuage tbody').empty();
                    $.each(data, function (idx, elem) {
                        var action =
                            '<button class="btn btn-success childrenInformationEdit" value="Edit" id="editLanguage"> ' +
                            '<i class="fa fa-pencil"></i></button> ';

                        if (elem[1] == '1') {
                            can_read = 'Yes';
                        } else {
                            can_read = 'No';
                        }
                        if (elem[2] == '1') {
                            can_write = 'Yes';
                        } else {
                            can_write = 'No';
                        }
                        if (elem[3] == '1') {
                            can_speak = 'Yes';
                        } else {
                            can_speak = 'No';
                        }
                        if (elem[4] == null) {
                            comments = '';
                        } else {
                            comments = elem[4];
                        }
                        tableBody += "<tr'>";
                        tableBody += "<td hidden>" + elem[0] + "</td>";
                        tableBody += "<td hidden>" + elem[1] + "</td>";
                        tableBody += "<td hidden>" + elem[2] + "</td>";
                        tableBody += "<td hidden>" + elem[3] + "</td>";
                        tableBody += "<td>" + elem[8] + "</td>";
                        tableBody += "<td hidden>" + can_read + "</td>";
                        tableBody += "<td hidden>" + can_write + "</td>";
                        tableBody += "<td hidden>" + can_speak + "</td>";
                        tableBody += "<td>" + comments + "</td>";
                        tableBody += "<td hidden>" + elem[5] + "</td>";
                        tableBody += "<td hidden>" + elem[6] + "</td>";
                        tableBody += "<td hidden>" + elem[7] + "</td>";
                        tableBody += "<td hidden>" + elem[9] + "</td>";
                        tableBody += "<td>" + elem[10] + "</td>";
                        tableBody += "<td>" + action + "</td>"
                        tableBody += "<tr>";
                    });
                    tableLangeuage.append(tableBody);
                }
            });
    }

    function loadMembershipTable() {

        var membershipTable = $('#emp_membership_tbl_id');

        $
            .ajax({
                type: "GET",
                url: "/hrm-admin/emp_membershipListInfo",
                success: function (data) {
                    var remarks = "";
                    var tableBody = "";
                    $('#emp_membership_tbl_id tbody').empty();
                    $.each(data, function (idx, elem) {
                        var action =
                            '<button class="btn btn-success childrenInformationEdit" value="Edit" id="editMembership"> ' +
                            '<i class="fa fa-pencil"></i></button> ';

                        if (elem[2] == null) {
                            remarks = '';
                        } else {
                            remarks = elem[2];
                        }
                        tableBody += "<tr'>";
                        tableBody += "<td hidden>" + elem[0] + "</td>";
                        tableBody += "<td hidden>" + elem[1] + "</td>";
                        tableBody += "<td hidden>" + elem[3] + "</td>";
                        tableBody += "<td hidden>" + elem[4] + "</td>";
                        tableBody += "<td hidden>" + elem[5] + "</td>";
                        tableBody += "<td hidden>" + elem[6] + "</td>";
                        tableBody += "<td>" + elem[7] + "</td>";
                        tableBody += "<td>" + elem[8] + "</td>";
                        tableBody += "<td>" + remarks + "</td>";
                        tableBody += "<td>" + action + "</td>"
                        tableBody += "<tr>";
                    });
                    membershipTable.append(tableBody);
                }
            });
    }

});