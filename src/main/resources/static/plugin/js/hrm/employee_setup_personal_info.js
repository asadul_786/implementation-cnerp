/**
 * Created by golam nobi.
 */

$(document).ready(function () {

        $("#btnpersonalInfo").click(function () {

            var flag = dataValidation();

            if (flag == true) {

                var father_nm = $('#father_nm').val();
                var mother_nm = $('#mother_nm').val();
                var iden_mark = $('#iden_mark').val();
                var birth_plc = $('#birth_plc').val();
                var blgp_cd = $('#blgp_cd option:selected').val();
                var height = $('#height').val();
                var weight = $('#weight').val();
                var hair_color = $('#hair_color').val();
                var eye_color = $('#eye_color').val();
                var relation_cd = $('#relation_cd option:selected').val();
                var relation_nm = $('#relation_nm').val();
                var driv_licen_no = $('#driv_licen_no').val();
                var licen_exp_dt = $('#licen_exp_dt').val();
                var national_id_no = $('#national_id_no').val();
                var id_issue_dt = $('#id_issue_dt').val();
                var pass_no = $('#pass_no').val();
                var pass_exp_dt = $('#pass_exp_dt').val();
                var tin_no = $('#tin_no').val();
                var tax_pay_year = $('#tax_pay_year').val();
                var fathernm_bangla = $('#fathernm_bangla').val();
                var mothernm_bangla = $('#mothernm_bangla').val();
                var birthCertificateNo = $('#birthCertificateNo').val();

                if (licen_exp_dt) {
                    licen_exp_dt = licen_exp_dt.split("/").reverse().join("/");
                    licen_exp_dt = getFormateDate(licen_exp_dt);
                }
                if (id_issue_dt) {
                    id_issue_dt = id_issue_dt.split("/").reverse().join("/");
                    id_issue_dt = getFormateDate(id_issue_dt);
                }
                if (pass_exp_dt) {
                    pass_exp_dt = pass_exp_dt.split("/").reverse().join("/");
                    pass_exp_dt = getFormateDate(pass_exp_dt);
                }

                if (relation_cd == '-1') {
                    relation_cd = "";
                }

                function getFormateDate(date) {
                    var d = new Date(date);
                    return d;
                }

                var personalinfo = {};
                personalinfo.father_nm = father_nm;
                personalinfo.mother_nm = mother_nm;
                personalinfo.iden_mark = iden_mark;
                personalinfo.birth_plc = birth_plc;
                personalinfo.blgp_cd = blgp_cd;
                personalinfo.height = height;
                personalinfo.weight = weight;
                personalinfo.hair_color = hair_color;
                personalinfo.eye_color = eye_color;
                personalinfo.relation_cd = relation_cd;
                personalinfo.relation_nm = relation_nm;
                personalinfo.driv_licen_no = driv_licen_no;
                personalinfo.licen_exp_dt = licen_exp_dt;
                personalinfo.national_id_no = national_id_no;
                personalinfo.id_issue_dt = id_issue_dt;
                personalinfo.pass_no = pass_no;
                personalinfo.pass_exp_dt = pass_exp_dt;
                personalinfo.tin_no = tin_no;
                personalinfo.tax_pay_year = tax_pay_year;
                personalinfo.fathernm_bangla = fathernm_bangla;
                personalinfo.mothernm_bangla = mothernm_bangla;
                personalinfo.birthCertificateNo = birthCertificateNo;

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/hrm-admin/addDetailsInfo",
                    data: JSON.stringify(personalinfo),
                    dataType: 'json',
                    success: function (data) {
                        showAlertByType('Save successfully!!', "S");
                        //clearForm();
                    },
                    error: function (e) {
                        showAlertByType('Something Wrong!', "F");
                    }
                });

            }

        });


        function dataValidation() {

            var status = true;
            var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

            if ($("#father_nm").val() == "") {
                status = false;
                $("#err_father_nm").text("Empty field found!!");
                $("#father_nm").focus();
            } else $("#err_father_nm").text("");

            if ($("#mother_nm").val() == "") {
                status = false;
                $("#err_mother_nm").text("Empty field found!!");
                $("#mother_nm").focus();
            } else $("#err_mother_nm").text("");

            if ($("#blgp_cd").val() == -1) {
                status = false;
                $("#err_blgp_cd").text("Empty field found!!");
                $("#blgp_cd").focus();
            } else $("#err_blgp_cd").text("");


            if ($("#licen_exp_dt").val() != "") {
                if (!dateCheck.test($("#licen_exp_dt").val())) {
                    status = false;
                    $("#err_licen_exp_dt").text("Invalid date format!!");
                    $("#licen_exp_dt").focus();

                } else if (isValidDate($("#licen_exp_dt").val()) == false) {

                    status = false;
                    $("#err_licen_exp_dt").text("Invalid date format!!");
                    $("#licen_exp_dt").focus();

                } else $("#err_licen_exp_dt").text("");

            } else $("#err_licen_exp_dt").text("");


            if ($("#national_id_no").val() != "") {
                if (/[^0-9]/g.test($("#national_id_no").val()) == true) {
                    status = false;
                    $("#err_national_id_no").text("Invalid character found!!");
                    $("#national_id_no").focus();
                } else if ($("#national_id_no").val().length == 10 || $("#national_id_no").val().length == 13 || $("#national_id_no").val().length == 17) {
                    $("#err_national_id_no").text("");
                } else {
                    status = false;
                    $("#err_national_id_no").text("10,13 and 17 digit Only!!");
                    $("#national_id_no").focus();
                }
            }else $("#err_national_id_no").text("");

            if ($("#tin_no").val() != "") {

                if (/[^0-9]/g.test($("#tin_no").val()) == true) {
                    status = false;
                    $("#err_tin_no").text("Invalid character found!!");
                    $("#tin_no").focus();

                } else if ($("#tin_no").val().length > 12) {
                    status = false;
                    $("#err_tin_no").text("12 digit Only!!");
                    $("#tin_no").focus();

                } else  $("#err_tin_no").text("");

            } else $("#err_tin_no").text("");


            if ($("#id_issue_dt").val() != "") {
                if (!dateCheck.test($("#id_issue_dt").val())) {
                    status = false;
                    $("#err_id_issue_dt").text("Invalid date format!!");
                    $("#id_issue_dt").focus();

                } else if (isValidDate($("#id_issue_dt").val()) == false) {
                    status = false;
                    $("#err_id_issue_dt").text("Invalid date format!!");
                    $("#id_issue_dt").focus();

                } else $("#err_id_issue_dt").text("");

            } else $("#err_id_issue_dt").text("");


            if ($("#pass_exp_dt").val() != "") {
                if (!dateCheck.test($("#pass_exp_dt").val())) {
                    status = false;
                    $("#err_pass_exp_dt").text("Invalid date format!!");
                    $("#pass_exp_dt").focus();

                } else if (isValidDate($("#pass_exp_dt").val()) == false) {

                    status = false;
                    $("#err_pass_exp_dt").text("Invalid date format!!");
                    $("#pass_exp_dt").focus();

                } else $("#err_pass_exp_dt").text("");

            } else $("#err_pass_exp_dt").text("");


            if ($("#tax_pay_year").val() != '') {
                if ($("#tax_pay_year").val().length != 4) {
                    status = false;
                    $("#err_tax_pay_year").text("Invalid year!!");
                    $("#tax_pay_year").focus();
                } else $("#err_tax_pay_year").text("");
            } else $("#err_tax_pay_year").text("");


            if ($("#height").val() != '') {
                if ($("#height").val().length > 30) {
                    status = false;
                    $("#err_height").text("Maximum 30 character allow!!");
                    $("#height").focus();
                } else $("#err_height").text("");
            }

            if ($("#weight").val() != '') {
                if ($("#weight").val().length > 30) {
                    status = false;
                    $("#err_weight").text("Maximum 30 character allow!!");
                    $("#weight").focus();
                } else $("#err_weight").text("");
            }

            if ($("#birthCertificateNo").val() != '') {
                if ($("#birthCertificateNo").val().length > 30) {
                    status = false;
                    $("#err_birthCertificateNo").text("Maximum 30 character allow!!");
                    $("#birthCertificateNo").focus();
                } else $("#err_birthCertificateNo").text("");
            }else $("#err_birthCertificateNo").text("");

            return status;
        }

        function clearForm() {
            $('#father_nm').val("");
            $('#mother_nm').val("");
            $('#iden_mark').val("");
            $('#birth_plc').val("");
            $('#blgp_cd').val('-1');
            $('#height').val("");
            $('#weight').val("");
            $('#hair_color').val("");
            $('#eye_color').val("");
            $('#relation_cd').val('-1');
            $('#relation_nm').val("");
            $('#driv_licen_no').val("");
            $('#licen_exp_dt').val("");
            $('#national_id_no').val("");
            $('#id_issue_dt').val("");
            $('#pass_no').val("");
            $('#pass_exp_dt').val("");
            $('#tin_no').val("");
            $('#tax_pay_year').val("");
            $("#birthCertificateNo").val("");
        }

        $("#btnpersonalInfoRefresh").click(function () {
            clearForm();
            $("#err_tax_pay_year").text("");
            $("#err_pass_exp_dt").text("");
            $("#err_id_issue_dt").text("");
            $("#err_national_id_no").text("");
            $("#err_licen_exp_dt").text("");
            $("#err_relation_nm").text("");
            $("#err_relation_cd").text("");
            $("#err_blgp_cd").text("");
            $("#err_mother_nm").text("");
            $("#err_father_nm").text("");
            $("#err_tin_no").text("");
            $("#err_birthCertificateNo").text("");

        });

        function isValidDate(s) {
            var bits = s.split('/');
            var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
            return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
        }

    }
);