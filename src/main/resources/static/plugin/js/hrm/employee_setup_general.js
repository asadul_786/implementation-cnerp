/**
 * Created by Golam.
 */
$(document).ready(function () {

    if ($('#getRadioOption').val() != "") {

        if ($('#getRadioOption').val() == 1) {
            $('#bankAccountStatus').prop('checked', true);
        } else {
            $('#bankAccountStatusNo').prop('checked', true);
        }
    }

    $(document).on("change", "#bankName", function (e) {

        var bankCD = $("#bankName option:selected").val();

        $.get("/hrm-admin/get-branchBank?bankCD=" + bankCD,

            function (data, status) {

                var office = $('#bankBrCd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Branch Bank---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });

    $("#emp_general_info_btn").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var empNmBng = $('#empNmBng').val();
            var empFileNo = $('#empFileNo').val();
            var birthDate = $('#birthDate').val();
            var sexCd = $('#sexCd option:selected').val();
            var religionCd = $('#religionCd option:selected').val();
            var maritalStatCd = $('#maritalStatCd option:selected').val();
            var gradeCd = $('#gradeCd option:selected').val();
            var bankBrCd = $('#bankBrCd option:selected').val();
            var bankAccountNo = $('#bankAccountNo').val();
            var bankAccountStatus = $('input[name=bankAccountStatus]:checked', '#emp_general_form').val();
            var routingno = $('#routingno').val();
            var swiftcd = $('#swiftcd').val();
//new
            var munilocCd = $('#munilocCd').val();
            var effectDate = $('#effectDate').val();
            var targetBasis = $('#targetBasis').val();
            var targetAmount = $('#targetAmount').val();

            if (munilocCd == "-1") {
                munilocCd = "";
            }
            if (targetBasis == "-1") {
                targetBasis = "";
            }
            if (targetAmount == "0.0") {
                targetAmount = "";
            }
            if (birthDate) {
                birthDate = birthDate.split("/").reverse().join("/");
                birthDate = getFormateDate(birthDate);
            }
            if (effectDate) {
                effectDate = effectDate.split("/").reverse().join("/");
                effectDate = getFormateDate(effectDate);
            }

            function getFormateDate(date) {
                var d = new Date(date);
                return d;
            }

            var generalInfo = {};
            generalInfo.empNmBng = empNmBng;
            generalInfo.empFileNo = empFileNo;
            generalInfo.birthDate = birthDate;
            generalInfo.sexCd = sexCd;
            generalInfo.religionCd = religionCd;
            generalInfo.maritalStatCd = maritalStatCd;
            generalInfo.gradeCd = gradeCd;
            generalInfo.bankBrCd = bankBrCd;
            generalInfo.bankAccountNo = bankAccountNo;
            generalInfo.bankAccountStatus = bankAccountStatus;
            generalInfo.routingno = routingno;
            generalInfo.swiftcd = swiftcd;
            generalInfo.munilocCd = munilocCd;//new
            generalInfo.effectDate = effectDate;
            generalInfo.targetBasis = targetBasis;
            generalInfo.targetAmount = targetAmount;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/addGeneralInfo",
                data: JSON.stringify(generalInfo),
                dataType: 'json',
                success: function (data) {
                    showAlertByType('Save successfully!', "S");
                },
                error: function (e) {
                    showAlertByType('Sorry,Something Wrong!!', "F");
                }
            });
        }
    });


    function dataValidation() {

        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($("#empNmBng").val() == "") {
            status = false;
            $("#err_emp_bn_name_id").text("Empty field found!!");
            $("#empNmBng").focus();

        } else if ($("#empNmBng").val().length > 50) {
            status = false;
            $("#err_emp_bn_name_id").text("Name maximum 50 characters!!");
            $("#empNmBng").focus();

        } else $("#err_emp_bn_name_id").text("");

        if ($("#empFileNo").val().length > 8) {
            status = false;
            $("#err_emp_file_id").text("FileNo maximum 8 characters!!");
            $("#empFileNo").focus();
        } else $("#err_emp_file_id").text("");


        if ($("#birthDate").val() == "") {
            status = false;
            $("#err_emp_dob_id").text("Empty field found!!");
            $("#birthDate").focus();

        } else if (checkvalid_JoinDate($("#birthDate").val())) {
            status = false;
            $("#err_emp_dob_id").text("Birth date should not be greater than the current Date!!");
            $("#birthDate").focus();

        } else if (!dateCheck.test($("#birthDate").val())) {
            status = false;
            $("#err_emp_dob_id").text("Invalid date format!!");
            $("#birthDate").focus();

        } else if (isValidDate($("#birthDate").val()) == false) {

            status = false;
            $("#err_emp_dob_id").text("Invalid date format!!");
            $("#birthDate").focus();

        } else $("#err_emp_dob_id").text("");


        if ($("#sexCd").val() == -1) {
            status = false;
            $("#err_emp_sex_id").text("Empty field found!!");
            $("#sexCd").focus();
        }
        else $("#err_emp_sex_id").text("");

        if ($("#religionCd").val() == -1) {
            status = false;
            $("#err_emp_religion_id").text("Empty field found!!");
            $("#religionCd").focus();
        }
        else $("#err_emp_religion_id").text("");


        if ($("#maritalStatCd").val() == -1) {
            status = false;
            $("#err_emp_mar_id").text("Empty field found!!");
            $("#maritalStatCd").focus();
        }
        else $("#err_emp_mar_id").text("");


        if ($("#gradeCd").val() == -1) {
            status = false;
            $("#err_emp_grade_id").text("Empty field found!!");
            $("#gradeCd").focus();
        }
        else $("#err_emp_grade_id").text("");

        if ($("#bankName").val() == -1) {
            status = false;
            $("#err_emp_bank_name_id").text("Empty field found!!");
            $("#bankName").focus();
        }
        else $("#err_emp_bank_name_id").text("");

        if ($("#bankBrCd").val() == -1) {
            status = false;
            $("#err_emp_bankBrCd").text("Empty field found!!");
            $("#bankBrCd").focus();
        }
        else $("#err_emp_bankBrCd").text("");

        if ($("#bankAccountNo").val() == "") {
            status = false;
            $("#err_emp_bank_acc_id").text("Empty field found!!");
            $("#bankAccountNo").focus();

        } else if ($("#bankAccountNo").val().length > 25) {
            status = false;
            $("#err_emp_bank_acc_id").text("Allow maximum 25 characters!!");
            $("#bankAccountNo").focus();
        }
        else $("#err_emp_bank_acc_id").text("");


        if ($("#routingno").val() != "") {

            if ($("#routingno").val().length > 30) {
                status = false;
                $("#err_emp_routingno").text("Allow maximum 30 characters!!");
                $("#routingno").focus();
            } else  $("#err_emp_routingno").text("");

        } else  $("#err_emp_routingno").text("");

        if ($("#swiftcd").val() != "") {

            if ($("#swiftcd").val().length > 30) {
                status = false;
                $("#err_emp_swiftcd").text("Allow maximum 30 characters!!");
                $("#swiftcd").focus();
            } else  $("#err_emp_swiftcd").text("");
        } else  $("#err_emp_swiftcd").text("");


        if ($('#employmentTypeCd option:selected').val() != "-1") {

            var employement_Type = $('#employmentTypeCd option:selected').val();

            if (employement_Type == '02') {
                if ($("#targetBasis").val() == -1) {
                    status = false;
                    $("#err_emp_targetbasic").text("Empty field found!!");
                    $("#targetBasis").focus();
                }
                else $("#err_emp_targetbasic").text("");

                if ($("#targetAmount").val() == "") {
                    status = false;
                    $("#err_emp_targetamt").text("Empty field found!!");
                    $("#targetAmount").focus();

                } else if ($("#targetAmount").val().length > 10) {
                    status = false;
                    $("#err_emp_targetamt").text("Allow maximum 10 character!!");
                    $("#targetAmount").focus();
                }
                else $("#err_emp_targetamt").text("");

                if ($("#effectDate").val() == "") {
                    status = false;
                    $("#err_emp_effectiveDT").text("Empty field found!!");
                    $("#effectDate").focus();
                } else if (isValidDate($("#effectDate").val()) == false) {
                    status = false;
                    $("#err_emp_effectiveDT").text("Invalid date format!!");
                    $("#effectDate").focus();
                }
                else $("#err_emp_effectiveDT").text("");
            } else {
                if ($("#munilocCd").val() == -1) {
                    status = false;
                    $("#err_emp_muniloc").text("Empty field found!!");
                    $("#munilocCd").focus();
                }
                else $("#err_emp_muniloc").text("");
            }
        }
        return status;
    }


    $("#general_form_refresh").click(function () {
        clearform();
    });

    function clearform() {
        $('#empNmBng').val("");
        $('#empFileNo').val("");
        $('#birthDate').val("");
        $('#sexCd').val('-1');
        $('#religionCd').val('-1');
        $('#maritalStatCd').val('-1');
        $('#gradeCd').val('-1');
        $('#bankBrCd').val('-1');
        $('#bankName').val('-1');
        $('#bankAccountNo').val("");
        $('#bankAccountStatus').val("");
        $('#getRadioOption').val("");
        $('#swiftcd').val("");
        $('#routingno').val("");
        $('#bankAccountStatus').prop('checked', true);
        //Clear error field
        $("#err_emp_bank_acc_id").text("");
        $("#err_emp_bankBrCd").text("");
        $("#err_emp_bank_name_id").text("");
        $("#err_emp_bank_name_id").text("");
        $("#err_emp_grade_id").text("");
        $("#err_emp_bank_acc_id").text("");
        $("#err_emp_mar_id").text("");
        $("#err_emp_religion_id").text("");
        $("#err_emp_sex_id").text("");
        $("#err_emp_dob_id").text("");
        $("#err_emp_file_id").text("");
        $("#err_emp_bn_name_id").text("");
        $("#err_emp_routingno").text("");
        $("#err_emp_swiftcd").text("");
//new
        $("#err_emp_muniloc").text("");
        $("#err_emp_effectiveDT").text("");
        $("#err_emp_targetbasic").text("");
        $("#err_emp_targetamt").text("");

        $('#munilocCd').val("-1");
        $('#effectDate').val("");
        $('#targetBasis').val("-1");
        $('#targetAmount').val("");


        var office = $('#bankBrCd');
        office.empty();
        office.append($('<option/>', {
            value: "-1",
            text: "---Select Branch Bank---"
        }));

    }

    function checkvalid_JoinDate(enteredDate) {
        var status = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate > today) {
            status = true;
        }
        return status;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    //
    // //added by mithun
    // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    //     var target = $(e.target).attr("href") // activated tab
    //     if(target=="#tab_content7"){
    // var isActive=true;
    // if(isActive) {
    //     isActive=false;
    //     $("#emp_gen_info_id").trigger("click");
    // }
    //         $("#tab_content41").addClass("active in ");
    //         // $("#tab_content41").addClass("in");
    //     }
    //
    // });
});