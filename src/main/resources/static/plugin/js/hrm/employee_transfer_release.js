/**
 * Created by Geetanjali Oishe on 7/31/2019.
 */
$(document).ready(function () {

    $('#release-info-div').hide();
    $('#release-info-edit-btn').hide();
    $('#release-emp-div').hide();
    updateTransferReleaseTable();


    $('#trans-req-tbl').DataTable({
        "lengthChange": false,
        "searching": false,
        "columnDefs": [
            { "targets": [0], "searchable": false }
        ]
    });
    // $('#trans-release-tbl').DataTable({
    //     "columnDefs": [
    //         { "targets": [0], "searchable": false }
    //     ]
    // });

    $(document).on("click", ".empl_release_select_button", function (event) {
        getTransferDetail(this);
        // $('#modal-transfer-release-empl').modal();
        $('#release-emp-div').show();
        $('#release-info-div').show();

        dateSetter();

        // updateTransferTable($('#hidden-emp-id').val());

        $('html, body').animate({
            scrollTop: $('#release-info-div').offset().top
        }, 1000)

    });


    function getTransferDetail (identifier, edit) {
        var row_id = $(identifier).attr("id");
        console.log("row_id: " + row_id);

        $.ajax({
            contentType: 'application/json',
            url: "get-employee-transfer-history",
            type: 'POST',
            async: false,
            data: JSON.stringify(row_id),
            dataType: 'json',
            success: function (response) {
                $('#release-employee-id-modal').val(response.empId);
                $('#release-employee-office-modal').val(response.officeName);
                $('#release-employee-dept-modal').val(response.deptName);
                $('#release-employee-joining-dt-modal').val(response.joiningDate);
                $('#release-employee-name-modal').val(response.employeeName);
                $('#release-employee-desig-modal').val(response.designationName);
                $('#release-employee-file-no-modal').val(response.fileNo);
                $('#release-employee-pay-scale-modal').val(response.payScale);

                clearFields();

                $('#hidden-transfer-id').val(response.id);

                $('#release-info-div').show();
                $('#release-emp-div').show();



                if (edit == "edit") {
                    if (response.status == "Released") {
                        $('#transfer-release').prop("checked", true);
                        $('#release-note').val(response.releaseNote);
                        $('#transfer-release-dt').val(response.releaseDate);
                        //file retrieve left////////////////////////////////

                        $('#release-info-edit-btn').show();
                        $('#release-info-save-btn').hide();
                    }
                    else {
                        $('#transfer-release').prop("checked", false);
                    }
                }



            },
            error: function (xhr, status, error) {
            }
        });
    }

    // $('#release-modal-close-btn').click(function (event) {
    //     $('#release-info-div').show();
    //
    //     dateSetter();
    //
    //     // updateTransferTable($('#hidden-emp-id').val());
    //
    //
    //     $('html, body').animate({
    //         scrollTop: $('#release-info-div').offset().top
    //     }, 1000)
    // });

    function dateSetter() {
        var now = new Date();

        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);

        var today = (day) + "/" +(month)+ "/" + now.getFullYear() ;

        $('#transfer-release-dt').val(today);
    }


    $('#release-info-save-btn').click(function (event) {
        if (checkReleasedValidation()) {
            confirmEmpReleaseDialog("Are you sure to save release info?");
        }
    });

    function checkReleasedValidation() {
            return true;
    }

    var confirmEmpReleaseDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        dateSetter();

                        var isReleased = 0;

                        if($('#transfer-release').prop("checked") == true){
                            isReleased = 1;
                        }

                        var releaseNote = $('#release-note').val();
                        var releaseDate = $('#transfer-release-dt').val();
                        var transferId = $('#hidden-transfer-id').val();

                        var employeeRelease = {};


                        employeeRelease.isReleased = isReleased;
                        employeeRelease.releaseNote = releaseNote;
                        employeeRelease.releaseDate = releaseDate;
                        employeeRelease.transferId = transferId;

                        $.ajax({
                            contentType: 'application/json',
                            url: "save-employee-release-info",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(employeeRelease),
                            dataType: 'json',
                            success: function (response) {


                                updateTransferRequestTable();
                                updateTransferReleaseTable();
                                clearForm();
                                $('#release-emp-div').hide();

                                showAlertByType("Successfully added", "S");
                                

                            },
                            error: function (xhr, status, error) {
                                showAlertByType("Failed to add", "F");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };


    function updateTransferRequestTable() {
        // var radioValue = $("input[name='empl_type_for_transfer']:checked").val();

        var empId = $('#release-employee-id').val();

        var empName = $('#release-employee-name').val();
        var empOfc = $('#release-office').val();
        var empDept = $('#release-department').val();
        var empDesig = $('#release-designation').val();
        var empFileNo = $('#release-employee-file-no').val();

        var payScale = $('#release-pay-scale-grade').val();
        var payLevel = $('#release-pay-level').val();

        var transStatus = 0;

        var employeeTransferSearch = {};

        employeeTransferSearch.empNmEng = empName;
        employeeTransferSearch.officeId = empOfc;
        // employeeTransferSearch.employeeType = radioValue;
        employeeTransferSearch.empId = empId;

        employeeTransferSearch.payScale = payScale;
        employeeTransferSearch.payLevel = payLevel;

        employeeTransferSearch.transStatus = transStatus;

        // if (radioValue == '3') { // agentInsss
        //     employeeTransferSearch.agentId = empId;
        // }

        // if (radioValue != '3') { //admin //do-dm
            employeeTransferSearch.designationId = empDesig;
            employeeTransferSearch.deptId = empDept;
            employeeTransferSearch.fileNo = empFileNo;
        // }


        // var tableData = $("#transfer_empl_table_tbody");
        //
        // var nameOrId = $('#transfer-employee-name').val();

        // parameter values


        $.ajax({
            contentType: 'application/json',
            url: "get-transfer-pending",
            type: 'POST',
            async: false,
            data: JSON.stringify(employeeTransferSearch),
            dataType: 'json',
            success: function(res){
                var table=$('#trans-req-tbl').dataTable({
                    data: res,
                    columns:[
                        {
                            "data": "id",
                            'visible' : false
                        },
                        { "data": "empId" },
                        { "data": "employeeName" },
                        { "data": "designationName" },
                        { "data": "officeName" },
                        { "data": "deptName" },
                        {
                            "data": "id",
                            render:function(data, type, row) {
                                var v = row.id;
                                v =
                                '<button style="text-align:center;vertical-align: middle;" ' +
                                'type="button" class="btn btn-info empl_release_select_button" value="Select" id="' +
                               v + '"> <i class="fa fa-check"></i></button>';
                                return v;
                            }
                        }

                    ],
                    bDestroy : true,
                    iDisplayLength : 15,
                    columnDefs: [
                        {
                            className: "dt-center"
                        }
                    ]
                });

                table.column(1).visible(false);
            }
        });
    }

    function updateTransferReleaseTable() {
        $.ajax({
            contentType: 'application/json',
            url: "get-transfer-released",
            type: 'GET',
            async: false,
            dataType: 'json',
            success: function(res){
                var table=$('#trans-release-tbl').dataTable({
                    "lengthChange": false,
                    "searching": false,
                    data: res,
                    columns:[
                        {
                            "data": "id",
                            'visible' : false
                        },
                        { "data": "empId" },
                        { "data": "employeeName" },
                        { "data": "designationName" },
                        { "data": "officeName" },
                        { "data": "deptName" },
                        { "data": "releaseDate" },
                        {
                            "data": "id",
                            render:function(data, type, row) {
                                var v = row.id;
                                v = '<button type="button" class="btn btn-success empl_released_edit_button" value="Select" id="' +
                                    v + '"> <i class="fa fa-pencil"></i></button>';
                                return v;

                            }
                        }
                    ],
                    bDestroy : true,
                    iDisplayLength : 15,
                    columnDefs: [
                        {
                            className: "dt-center"
                        }
                    ]
                });

                table.column(1).visible(false);
            }
        });
    }

    $(document).on("click", ".empl_released_edit_button", function (event) {
        getTransferDetail(this, "edit");
    });

    $('#release-info-edit-btn').click(function (event) {
        if (checkReleasedValidation()) {
            confirmEmpReleaseDialog("Are you sure to edit release info?");
        }
    });

    function clearForm(){
        clearFields();
        $('#release-info-edit-btn').hide();
        $('#release-info-save-btn').show();
        $('#release-info-refresh-btn').show();
        $("#hidden-transfer-id").val('-1');
    };
    
    function clearFields() {
        $("#transfer-release").prop("checked", false);
        $("#release-note").val("");
        $("#transfer-release-dt").val("");
        $("#hidden-transfer-id").val('-1');
        $('#release-info-edit-btn').hide();
        $('#release-info-save-btn').show();
    }

    $('#release-filterBtn').click(function (event) {
        event.preventDefault();
        var isVaild = validateReleaseEmplSearch();
        if(isVaild) {
            updateTransferRequestTable();
        }
    });

    function validateReleaseEmplSearch() {
        $(".err_msg").text("");

        if ($('#release-pay-level').find('option:selected').val() == -1 ) {
            var parent = $('#release-pay-scale-grade').find('option:selected').val();
            if (parent != -1){
                $('#release-pay-level').after('<span class="error err_msg">Select pay level</span>');
                return false;
            }
        }

        else {
            return true;
        }

        if ($('#release-office').find('option:selected').val() == -1 ) {
            var parent = $('#release-office-category').find('option:selected').val();
            if (parent != -1) {
                $('#release-office').after('<span class="error err_msg">Select office</span>');
                return false;
            }
        }

        else {
            return true;
        }

        return true;
    }

    $(document).on("change", "#release-pay-scale-grade", function (e) {

        var scaleId = $("#release-pay-scale-grade option:selected").val();

        $.getJSON('/hrm-admin/get-level-by-scale', {
                scaleId: scaleId,
                ajax: true
            },
            function (data) {
                var payLevel = $('#release-pay-level');
                payLevel.empty();
                payLevel.append($('<option/>', {
                    value: "-1",
                    text: "---Select Pay Level---"
                }));
                $.each(data, function (index, offices) {
                    payLevel.append($('<option/>', {
                        value: payLevel.id,
                        text: offices.payLevel
                    }));
                });
            });
    });


    $('#release-refreshButton').click(function (event) {
        event.preventDefault();


        $('#release-employee-id').val("");
        $('#release-employee-file-no').val("");
        $('#release-employee-name').val("");
        $('#release-department').val(-1);
        $('#release-designation').val(-1);
        $('#release-pay-level').val(-1);
        $('#release-pay-scale-grade').val(-1);
        $('#release-office').val(-1)
        $('#release-office-category').val(-1);

        // resetModal();

        // $('#modal-transfer-empl').modal();

    });

});