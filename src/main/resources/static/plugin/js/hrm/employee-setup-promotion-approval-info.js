$(document).ready(function () {

    $("#btnpromotionStatusUpdate").hide();


    $('#promotationdataTable').DataTable();

    $(document).on("change", "#pay_level", function (e) {
        var s = $("#pay_level option:selected").text();
        s = s.substring(0, s.indexOf('('));
        $("#basic").val(s);
    });


    $(document).on("change", "#paysc_id", function (e) {

        var catId = $("#paysc_id option:selected").val();

        $.get("/hrm-admin/get-payScale?scaleId=" + catId,

            function (data, status) {

                var office = $('#pay_level');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Pay Level---"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices[0],
                        text: offices[1] + ' (Pay Level= ' + (offices[0]) + ')'
                    }));
                });

            });
    });


    $("#btnpromotionStatusUpdate").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var emp_id = $('#emp_id').val();
            var emp_gid = $('#emp_gid').val();
            var updateempgid = $('#updateempgid').val();
            var office_cd = $('#office_cd').val();
            var divdept_cd = $('#divdept_cd').val();
            var empName = $('#empName').val();
            var pre_desig_cd = $('#pre_desig_cd').val();
            var pre_paysc_id = $('#pre_paysc_id').val();
            var pre_pay_level = $('#pre_pay_level').val();
            var pre_basic = $('#pre_basic').val();
            var desig_cd = $('#desig_cd').val();
            var comments = $('#comments').val();
            var paysc_id = $('#paysc_id').val();
            var pay_level = $('#pay_level').val();
            var basic = $('#basic').val();
            //var prom_incre_dt = $('#prom_incre_dt').val();
            //var effect_dt_on_salary = $('#effect_dt_on_salary').val();
            var emp_type_cd = $('#emp_type_cd').val();


            var promotion = {};

            promotion.emp_id = emp_id;
            promotion.emp_gid = emp_gid;
            promotion.updateempgid = updateempgid;
            promotion.office_cd = office_cd;
            promotion.divdept_cd = divdept_cd;
            promotion.empName = empName;
            promotion.pre_desig_cd = pre_desig_cd;
            promotion.pre_paysc_id = pre_paysc_id;
            promotion.pre_pay_level = pre_pay_level;
            promotion.pre_basic = pre_basic;
            promotion.desig_cd = desig_cd;
            promotion.comments = comments;
            promotion.paysc_id = paysc_id;
            promotion.pay_level = pay_level;
            promotion.basic = basic;
            //promotion.prom_incre_dt = prom_incre_dt;
            //promotion.effect_dt_on_salary = effect_dt_on_salary;
            promotion.emp_type_cd = emp_type_cd;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/UpdatePromotionInfo",
                data: JSON.stringify(promotion),
                dataType: 'json',
                success: function (data) {
                    showAlertByType("Promotion Approved successfully!!","S");
                    clearForm();
                    setInterval(function () {
                        location.reload(true);
                    }, 2000);

                },
                error: function (e) {
                    showAlertByType("Sorry!Something Wrong!!","F");
                }
            });

        }
    });


    $(document).on("input", "#emp_id", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var emp_id = $('#emp_id').val();
        $('#updateempgid').val("");

        if (emp_id != "" && emp_id.length < 9) {
            $.ajax({
                url: "/hrm-admin/gethrmEmployeeDetails/" + emp_id,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    var value_6 = "";
                    var value_7 = "";
                    var value_8 = "";
                    var value_9 = "";
                    var value_10 = "";
                    var value_11 = "";
                    var value_12 = "";
                    $.each(response, function (i, l) {
                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];
                        value_9 = l[8];
                        value_10 = l[9];
                        value_11 = l[10];
                        value_12 = l[11];
                    });
                    $("#emp_gid").val(value_1);
                    $("#office_cd").val(value_2);
                    $("#divdept_cd").val(value_3);
                    $("#empName").val(value_4);
                    $("#pre_desig_cd").val(value_5);
                    $("#pre_paysc_id").val(value_6);
                    $("#pre_pay_level").val(value_7);
                    $("#pre_basic").val(value_8);
                    $("#emp_type_cd").val(value_9);
                    $("#err_empID").text("");
//
                    $("#office_cdName").val(value_10);
                    $("#divdept_cdName").val(value_11);
                    $("#pre_desig_cdName").val(value_12);

                },
                error: function (xhr, status, error) {
                    $("#emp_gid").val("");
                    $("#office_cd").val("");
                    $("#divdept_cd").val("");
                    $("#empName").val("");
                    $("#pre_desig_cd").val("");
                    $("#pre_paysc_id").val("");
                    $("#pre_pay_level").val("");
                    $("#pre_basic").val("");
                    $("#emp_type_cd").val("-1");
                    $("#err_empID").text("Sorry! Employee ID not match!!");
                    $("#desig_cd").val("-1");
                    $("#comments").val("");
                    $("#paysc_id").val("-1");
                    $("#pay_level").val("-1");
                    $("#basic").val("");
                    $("#prom_incre_dt").val("");
                    $("#effect_dt_on_salary").val("");
                    //
                    $("#office_cdName").val("");
                    $("#divdept_cdName").val("");
                    $("#pre_desig_cdName").val("");

                }
            });
        }
        else {

            if ($('#emp_id').val() == "") {
                $("#desig_cd").val("-1");
                $("#comments").val("");
                $("#paysc_id").val("-1");
                $("#pay_level").val("-1");
                $("#basic").val("");
                $("#prom_incre_dt").val("");
                $("#effect_dt_on_salary").val("");
                $("#err_empID").text("Employee ID required!");
                $("#emp_gid").val("");
                $("#office_cd").val("");
                $("#divdept_cd").val("");
                $("#empName").val("");
                $("#pre_desig_cd").val("");
                $("#pre_paysc_id").val("");
                $("#pre_pay_level").val("");
                $("#pre_basic").val("");
                $("#emp_type_cd").val("-1");
                //
                $("#office_cdName").val("");
                $("#divdept_cdName").val("");
                $("#pre_desig_cdName").val("");

            }
            else {
                $("#err_empID").text("ID maximum 8 characters!");
            }
        }

    });


    function dataValidation() {

        var status = true;

        if ($("#emp_id").val() == "") {
            status = false;
            $("#err_empID").text("Empty field found!!");
            $("#emp_id").focus();
        } else $("#err_empID").text("");


        if ($("#paysc_id").val() == -1) {
            status = false;
            $("#err_recentpaysc_id").text("Empty field found!!");
            $("#paysc_id").focus();
        } else $("#err_recentpaysc_id").text("");


        if ($("#pay_level").val() == -1) {
            status = false;
            $("#err_recentpay_level").text("Empty field found!!");
            $("#pay_level").focus();
        }
        else $("#err_recentpay_level").text("");


        if ($("#basic").val() == -1) {
            status = false;
            $("#err_recentbasic").text("Empty field found!!");
            $("#basic").focus();
        }
        else $("#err_recentbasic").text("");


        if ($("#prom_incre_dt").val() == "") {
            status = false;
            $("#err_recentprom_incre_dt").text("Empty field found!!");
            $("#prom_incre_dt").focus();
        }
        else $("#err_recentprom_incre_dt").text("");

        if ($("#effect_dt_on_salary").val() == "") {
            status = false;
            $("#err_recenteffect_dt_on_salary").text("Empty field found!!");
            $("#effect_dt_on_salary").focus();
        }
        else $("#err_recenteffect_dt_on_salary").text("");

        if ($("#desig_cd").val() == -1) {
            status = false;
            $("#err_desig_cd").text("Empty field found!!");
            $("#desig_cd").focus();
        }
        else $("#err_desig_cd").text("");

        return status;
    }

    function clearForm() {
        $("#emp_gid").val("");
        $("#updateempgid").val("");
        $("#emp_id").val("");
        $("#office_cd").val("");
        $("#divdept_cd").val("");
        $("#empName").val("");
        $("#pre_desig_cd").val("");
        $("#pre_paysc_id").val("");
        $("#pre_pay_level").val("");
        $("#pre_basic").val("");
        $("#desig_cd").val("-1");
        $("#comments").val("");
        $("#paysc_id").val("-1");
        $("#pay_level").val("");
        $("#basic").val("");
        $("#prom_incre_dt").val("");
        $("#effect_dt_on_salary").val("");
        $("#emp_type_cd").val("-1");
        //
        $("#office_cdName").val("");
        $("#divdept_cdName").val("");
        $("#pre_desig_cdName").val("");
    }

    $('#promotationdataTable tbody').on('click', '#editpromotionInfo', function () {

        $("#btnpromotionStatusUpdate").show();

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        $('#emp_id').val(col1);
        var col2 = curRow.find('td:eq(12)').text();
        $('#updateempgid').val(col2);
        var col3 = curRow.find('td:eq(5)').text();
        $('#desig_cd').val(col3);
        var col4 = curRow.find('td:eq(6)').text();
        $('#prom_incre_dt').val(col4);
        var col5 = curRow.find('td:eq(7)').text();
        $('#effect_dt_on_salary').val(col5);

        var col6 = curRow.find('td:eq(13)').text();
        $('#updateempgid').val(col6);

        var col7 = curRow.find('td:eq(15)').text();
        $('#comments').val(col7);

        var primarykeypayscale = curRow.find('td:eq(14)').text();

        //entry pay scale level
        var col8 = curRow.find('td:eq(9)').text();
        $('#pay_level option:selected').val(col8);

        var col9 = curRow.find('td:eq(11)').text();
        $('#basic').val(col9);

        var payscaledescription = curRow.find('td:eq(16)').text();


        var getData = $('#pay_level');
        getData.empty();
        getData.append($('<option/>', {
            value: col8,
            text: col9 + ' (Pay Level= ' + col8 + ')'
        }));
        // end pay scale level

        var getData = $('#paysc_id');
        getData.empty();
        getData.append($('<option/>', {
            value: primarykeypayscale,
            text: payscaledescription
        }));

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

        getDateFor();

        $.ajax({
            url: "/hrm-admin/gethrmEmployeeDetails/" + col1,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var value_1 = "";
                var value_2 = "";
                var value_3 = "";
                var value_4 = "";
                var value_5 = "";
                var value_6 = "";
                var value_7 = "";
                var value_8 = "";
                var value_9 = "";
                var value_10 = "";
                var value_11 = "";
                var value_12 = "";
                $.each(response, function (i, l) {
                    value_1 = l[0];
                    value_2 = l[1];
                    value_3 = l[2];
                    value_4 = l[3];
                    value_5 = l[4];
                    value_6 = l[5];
                    value_7 = l[6];
                    value_8 = l[7];
                    value_9 = l[8];
                    value_10 = l[9];
                    value_11 = l[10];
                    value_12 = l[11];
                });
                $("#emp_gid").val(value_1);
                $("#office_cd").val(value_2);
                $("#divdept_cd").val(value_3);
                $("#empName").val(value_4);
                $("#pre_desig_cd").val(value_5);
                $("#pre_paysc_id").val(value_6);
                $("#pre_pay_level").val(value_7);
                $("#pre_basic").val(value_8);
                $("#emp_type_cd").val(value_9);
                $("#err_empID").text("");
                //
                $("#office_cdName").val(value_10);
                $("#divdept_cdName").val(value_11);
                $("#pre_desig_cdName").val(value_12);
            },
            error: function (xhr, status, error) {

                $("#emp_gid").val("");
                $("#office_cd").val("");
                $("#divdept_cd").val("");
                $("#empName").val("");
                $("#pre_desig_cd").val("");
                $("#pre_paysc_id").val("");
                $("#pre_pay_level").val("");
                $("#pre_basic").val("");
                $("#emp_type_cd").val("-1");
                $("#err_empID").text("Sorry! Employee ID not match!!");
                $("#desig_cd").val("-1");
                $("#comments").val("");
                $("#paysc_id").val("-1");
                $("#pay_level").val("-1");
                $("#basic").val("");
                $("#prom_incre_dt").val("");
                $("#effect_dt_on_salary").val("");
                //
                $("#office_cdName").val("");
                $("#divdept_cdName").val("");
                $("#pre_desig_cdName").val("");
            }
        });

    });

    function getDateFor() {
        var dateStr = $('#prom_incre_dt').val();
        var dateCom = getDate(dateStr);
        $('#prom_incre_dt').val(dateCom);

        var dateStr1 = $('#effect_dt_on_salary').val();
        var dateCom1 = getDate(dateStr1);
        $('#effect_dt_on_salary').val(dateCom1);

    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    }

});
