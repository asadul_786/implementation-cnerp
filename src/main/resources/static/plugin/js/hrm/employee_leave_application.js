$(document).ready(function () {

    //$('.js-example-basic-single').select2();

    //year picker

    $(".yearpicker").yearpicker();


    /*var empInfoTable = $('#employee_information_table').DataTable({
        /!*"columnDefs": [
            { "targets": [0], "searchable": false }
        ]*!/
    });*/

    var leaveApplTable = $('#leave_application_table').DataTable({
        "columnDefs": [
            { "targets": [11, 12, 13, 14, 15, 16], "searchable": false }
        ]
    });



   /* $(document).on('input', '#no_of_days', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });*/


    $('#leave_type').on('click', function () {
        $('#err_leave_type').hide();
    });

    $('#from_date1').on('click', function () {
        $('#err_from_date1').hide();
    });

    $('#to_date').on('click', function () {
        $('#err_to_date').hide();
    });

    /*$('#no_of_days').keyup(function () {
        $('#err_no_of_days').hide();
    });*/

    $('#attachment').on('click', function () {
        $('#err_attachment').hide();
    });

    $('#office_category').on('click', function () {
        $('#err_office_category').hide();
    });

    $('#office_id').on('click', function () {
        $('#err_office_id').hide();
    });

    $('#designation_name_dropdown').on('click', function () {
        $('#err_designation_dropdown').hide();
    });


    function fieldValidation()
    {

        $('#err_leave_type').hide();
        $('#err_from_date1').hide();
        $('#err_to_date').hide();
        //$('#err_no_of_days').hide();
        $('#err_attachment').hide();
        $('#err_office_category').hide();
        $('#err_office_id').hide();
        $('#err_designation_name_dropdown').hide();


        if ($('#leave_type').val() == "-1")
        {
            $('#err_leave_type').text("Please enter leave type");
            $('#err_leave_type').show();

            return false;
        }
        else
        {
            $('#err_leave_type').hide();
        }


        if ($('#from_date1').val() == "")
        {
            $('#err_from_date1').text("Please enter from date");
            $('#err_from_date1').show();

            return false;
        }
        else
        {
            $('#err_from_date1').hide();
        }


        if ($('#to_date').val() == "")
        {
            $('#err_to_date').text("Please enter to date");
            $('#err_to_date').show();

            return false;
        }
        else
        {
            $('#err_to_date').hide();
        }


        /*if ($('#no_of_days').val() == "")
        {
            $('#err_no_of_days').text("Please enter no of days");
            $('#err_no_of_days').show();

            return false;
        }
        else
        {
            $('#err_no_of_days').hide();
        }*/


        if ($('#office_category').val() == "-1")
        {
            $('#err_office_category').text("Please enter office category");
            $('#err_office_category').show();

            return false;
        }
        else
        {
            $('#err_office_category').hide();
        }


        if ($('#office_id').val() == "-1")
        {
            $('#err_office_id').text("Please enter office");
            $('#err_office_id').show();

            return false;
        }
        else
        {
            $('#err_office_id').hide();
        }


        if ($('#designation_name_dropdown').val() == "-1")
        {
            $('#err_designation_name_dropdown').text("Please enter designation and name");
            $('#err_designation_name_dropdown').show();

            return false;
        }
        else
        {
            $('#err_designation_name_dropdown').hide();
        }

        return true;
    }



    $(document).on("click", "#employee_leave_info_load", function () {

        $.ajax({
            contentType: 'application/json',
            url: "/hrm-admin/get-leave-info-by-employee-id/" + $("#employee_id").val(),
            type: 'GET',
            dataType: 'json',
            success: function(leaveInfoResponse)
            {

                $("#employee_information_table tbody").empty();

                var li_response = 0;
                var leaveTakenByEmp = 0;
                var leaveBalnaceOfEmp = 0;
                var leaveCount = 0;
                var leaveBal = 0;
                var yearOfLeave = $("#year_picker").val();
                yearOfLeave = parseInt(yearOfLeave);
                var leaveYearNeeded = 0;


                $.ajax({
                    contentType: 'application/json',
                    url: "/hrm-admin/get-leave-types/",
                    type: 'GET',
                    dataType: 'json',
                    success: function(leaveTypeResponse)
                    {

                        for(var leaveTypeResp in leaveTypeResponse)
                        {
                            var lt_response = leaveTypeResponse[leaveTypeResp].id;
                            var lt_name = leaveTypeResponse[leaveTypeResp].leaveTypeName;
                            var lt_allocation = leaveTypeResponse[leaveTypeResp].noDays;

                            leaveCount = 0;
                            leaveBal = 0;

                            for(var leaveInfResp in leaveInfoResponse)
                            {
                                li_response = leaveInfoResponse[leaveInfResp].leaveTypeCode;
                                leaveTakenByEmp = leaveInfoResponse[leaveInfResp].leaveTaken;
                                leaveYearNeeded = leaveInfoResponse[leaveInfResp].leaveYear;
                                //alert(leaveTakenByEmp);
                                leaveBalnaceOfEmp = leaveInfoResponse[leaveInfResp].prevLeaveBal;

                                if((li_response == lt_response) && (leaveYearNeeded == yearOfLeave))
                                {
                                    leaveCount = leaveCount + leaveTakenByEmp;
                                    leaveBal = leaveBal + leaveBalnaceOfEmp;
                                }
                            }
                            var html = "<tr>" +
                                "<td>" + lt_name + "</td>" +
                                "<td>" + lt_allocation + "</td>" +
                                "<td>" + leaveCount + "</td>" +
                                "<td>" + leaveBal + "</td>" +
                                "</tr>";

                            $("#employee_information_table tbody").append(html);
                        }


                    },
                    error: function(xhr, status, error) {
                        alert(xhr.status);
                    }
                });


            },
            error: function(xhr, status, error) {
                alert(xhr.status);
            }
        });

    });



    $('#office_category').change(function () {

        var dropdown_val = $('#office_category').val();

        if(dropdown_val != "-1"){ //Dropdown value is not -- Select --

            $.ajax({
                contentType: 'application/json',
                url: "/hrm-admin/get-offices-by-category/" + dropdown_val,
                type: 'GET',
                dataType: 'json',
                success: function(response) {

                    console.log(response);
                    //$('#office_category').attr('disabled', false);
                    $("#office_id").empty();

                    if(response.length == 0)
                    {
                        $("#office_id").append($('<option></option>').val(-1).html("No offices available ..."));
                    }
                    else
                    {
                        $("#office_id").append($('<option></option>').val('-1').html('-- Select --'));
                        $.each( response, function( key, value )
                        {
                            $("#office_id").append($('<option></option>').val(value.id).html(value.officeName));
                        });
                    }

                },
                error: function(xhr, status, error) {
                    alert(xhr.status);
                }
            }).done(function(data){

                if(editModeEnabledOffId == 1) {
                    var offId = $.trim(curRowEditMode.find('td:eq(13)').text());
                    $('#office_id').val(offId);
                    $('#office_id').trigger('change');
                    editModeEnabledOffId = 0;
                }

            });
        }
        else
        {
            $("#office_id").empty();
            $("#office_id").append($('<option></option>').val('-1').html('-- Select --'));
        }

    });


    $('#office_id').change(function () {

        var dropdown_val = $('#office_id').val();

        if(dropdown_val != "-1"){ //Dropdown value is not -- Select --

            $.ajax({
                contentType: 'application/json',
                url: "/hrm-admin/get-name-designation-by-office/" + dropdown_val,
                type: 'GET',
                dataType: 'json',
                success: function(response) {

                    $("#designation_name_dropdown").empty();

                    if(response.length == 0)
                    {
                        $("#designation_name_dropdown").append($('<option></option>').val(-1).html("No boss available ..."));
                    }
                    else
                    {
                        $("#designation_name_dropdown").append($('<option></option>').val('-1').html('-- Select --'));
                        $.each( response, function( key, value )
                        {
                            if(value.designationId != null)
                            {
                                $("#designation_name_dropdown").append($('<option></option>').val(value.id).html(value.designation.designationName + " --- " + value.empNmEng));
                            }
                            else
                            {
                                $("#designation_name_dropdown").append($('<option></option>').val(value.id).html(null + " --- " + value.empNmEng));
                            }
                        });
                    }

                },
                error: function(xhr, status, error) {
                    alert(xhr.status);
                }
            }).done(function(data){

                if(editModeEnabledNameDesig == 1) {
                    var nameDesignation = $.trim(curRowEditMode.find('td:eq(15)').text());
                    $('#designation_name_dropdown').val(nameDesignation);
                    editModeEnabledNameDesig = 0;
                }

            });
        }
        else
        {
            $("#designation_name_dropdown").empty();
            $("#designation_name_dropdown").append($('<option></option>').val('-1').html('-- Select --'));
        }

    });

    var employeeLeaveApplication = {};

    $(document).on("click", "#employee_leave_application_save", function () {

        if (fieldValidation())
        {
            employeeLeaveApplication.id = $("#id").val();
            employeeLeaveApplication.empId = $("#employee_id").val();
            employeeLeaveApplication.officeCatId = $("#office_category").val();
            employeeLeaveApplication.officeId = $("#office_id").val();
            employeeLeaveApplication.leaveTypeCode = $("#leave_type").val();
            employeeLeaveApplication.leaveStartDate = $("#from_date1").val();
            employeeLeaveApplication.leaveEndDate = $("#to_date").val();
            //employeeLeaveApplication.totalDays = $("#no_of_days").val();
            employeeLeaveApplication.approvalStatus = 0;
            employeeLeaveApplication.purpose = $("#leave_purpose").val();
            employeeLeaveApplication.appliedToEmpId = $("#designation_name_dropdown").val();

            var leaveType = $("#leave_type option:selected").text();
            var officeName = $("#office_id option:selected").text();

            var editBtn = "<button id='edit' type='button' class='btn fa fa-edit' style='text-align:center;vertical-align: middle;font-size:20px;'></button>";
            var dltBtn = "<button id='delete' type='button' class='btn fa fa-trash-o' style='text-align:center;vertical-align: middle;font-size:20px;color:red;'></button>";

            $.ajax({
                contentType: 'application/json',
                url: "/hrm-admin/save-employee-leave-application",
                type: 'POST',
                data: JSON.stringify(employeeLeaveApplication),
                dataType: 'json',
                success: function (response) {
                    if(response)
                    {
                        var status = "";
                        if(response.approvalStatus == 0)
                        {
                            status = "Pending";
                        }
                        else if(response.approvalStatus == 1)
                        {
                            status = "Approved";
                        }
                        else
                        {
                            status = "Rejected";
                        }
                        if (employeeLeaveApplication.id != "")
                        {
                            var rowIndex = leaveApplTable.row( curRowEditMode ).index();
                            var updatedRow = leaveApplTable.row( rowIndex ).data( [officeName,
                                response.leaveYear,
                                leaveType,
                                response.applicationDate,
                                employeeLeaveApplication.leaveStartDate,
                                employeeLeaveApplication.leaveEndDate,
                                response.totalDays,
                                employeeLeaveApplication.purpose,
                                status,
                                editBtn,
                                dltBtn,
                                response.id,
                                employeeLeaveApplication.empId,
                                employeeLeaveApplication.officeId,
                                employeeLeaveApplication.leaveTypeCode,
                                employeeLeaveApplication.appliedToEmpId,
                                employeeLeaveApplication.officeCatId] ).invalidate();

                            updatedRow.nodes().to$().attr("style", "background-color: rgb(255, 245, 238);");
                            updatedRow.nodes().to$().attr("id", "updateScroll");
                            document.getElementById("updateScroll").scrollIntoView();
                            updatedRow.nodes().to$().removeAttr("id");

                            setTimeout(function(){
                                updatedRow.nodes().to$().removeAttr("style");
                            }, 3000);

                            showAlert("Updated successfully !");
                        }
                        else
                        {
                            var newRow = leaveApplTable.row.add([officeName,
                                response.leaveYear,
                                leaveType,
                                response.applicationDate,
                                employeeLeaveApplication.leaveStartDate,
                                employeeLeaveApplication.leaveEndDate,
                                response.totalDays,
                                employeeLeaveApplication.purpose,
                                status,
                                editBtn,
                                dltBtn,
                                response.id,
                                employeeLeaveApplication.empId,
                                employeeLeaveApplication.officeId,
                                employeeLeaveApplication.leaveTypeCode,
                                employeeLeaveApplication.appliedToEmpId,
                                employeeLeaveApplication.officeCatId]).draw();
                            var colCount = leaveApplTable.columns()[0].length;

                            /*newRow.nodes().to$().find("td:eq(0)").attr("style", "text-align:center;display: none");
                            newRow.nodes().to$().find("td:eq(1)").attr("style", "text-align:center;display: none");
                            newRow.nodes().to$().find("td:eq(2)").attr("style", "text-align:center;display: none");
                            newRow.nodes().to$().find("td:eq(3)").attr("style", "text-align:center;display: none");*/

                            newRow.nodes().to$().attr("style", "background-color: rgb(255, 245, 238);");
                            newRow.nodes().to$().attr("id", "newScroll");
                            var i;
                            for(i = 0; i < colCount - 6; i++){
                                newRow.nodes().to$().find("td:eq(" + i + ")").attr("style", "text-align:center;vertical-align: middle;");
                            }
                            for(i = colCount - 6; i < colCount; i++){
                                newRow.nodes().to$().find("td:eq(" + i + ")").attr("style", "text-align:center;vertical-align: middle; display: none");
                            }
                            newRow.show().draw(false);
                            document.getElementById("newScroll").scrollIntoView();
                            newRow.nodes().to$().find("tr").removeAttr("id");

                            setTimeout(function(){
                                newRow.nodes().to$().find("tr").removeAttr("style");
                            }, 3000);

                            showAlert("Saved successfully !");

                        }
                    }
                },
                error: function (xhr, status, error) {
                    showAlert("Something went wrong!");
                }
            });
        }

    });



    var curRowEditMode;
    var editModeEnabledOffId = 0;
    var editModeEnabledNameDesig= 0;

    $('#leave_application_table').on('click', '#edit', function () {

        $("#employee_leave_application_save").text("Update");

        editModeEnabledOffId = 1;
        editModeEnabledNameDesig = 1;

        curRowEditMode = $(this).closest('tr');
        var col1 = $.trim(curRowEditMode.find('td:eq(0)').text());
        var col2 = $.trim(curRowEditMode.find('td:eq(1)').text());
        var col3 = $.trim(curRowEditMode.find('td:eq(2)').text());
        var col4 = $.trim(curRowEditMode.find('td:eq(3)').text());
        var col5 = $.trim(curRowEditMode.find('td:eq(4)').text());
        var col6 = $.trim(curRowEditMode.find('td:eq(5)').text());
        var col7 = $.trim(curRowEditMode.find('td:eq(6)').text());
        var col8 = $.trim(curRowEditMode.find('td:eq(7)').text());
        var col9 = $.trim(curRowEditMode.find('td:eq(8)').text());

        var col12 = $.trim(curRowEditMode.find('td:eq(11)').text());
        var col13 = $.trim(curRowEditMode.find('td:eq(12)').text());
        var col14 = $.trim(curRowEditMode.find('td:eq(13)').text());
        var col15 = $.trim(curRowEditMode.find('td:eq(14)').text());
        var col16 = $.trim(curRowEditMode.find('td:eq(15)').text());
        var col17 = $.trim(curRowEditMode.find('td:eq(16)').text());



        $('#leave_type').val(col15);
        $('#from_date1').val(col5);
        $('#to_date').val(col6);
        $('#leave_purpose').val(col8);


        $('#office_category').val(col17);
        $('#office_category').trigger('change');

        $('#id').val(col12);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });


    $('#leave_application_table tbody').on('click', '#delete', function () {
        var curRow = $(this).closest('tr');
        var col12 = $.trim(curRow.find('td:eq(11)').text());

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url:  "/hrm-admin/delete/" + col12,
                        type: 'POST',
                        dataType: 'json',
                        success: function(response) {

                            leaveApplTable.row( curRow ).remove().draw( false );
                            showAlert("Deleted Successfully");
                        },
                        error: function(xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

});