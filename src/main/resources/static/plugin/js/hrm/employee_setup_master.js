/**
 * Created by golam nobi.
 */
$(document).ready(function () {

    var pathname = window.location.pathname;
    var short_path_name = pathname.split('/')[2];

    if (short_path_name === "employee-general-info") {
        $("#emp_gen_info_id").addClass('active');
    }
    else if (short_path_name === "employee-job-detail") {
        $("#emp_job_detail_id_li").addClass('active');
    }
    else if (short_path_name === "employee-setup-address") {
        $("#emp_setup_address_id").addClass('active');
    }
    else if (short_path_name === "employee-setup-personal-info") {
        $("#emp_setup_personal_info_id").addClass('active');
    }
    else if (short_path_name === "employee-setup-education") {
        $("#emp_setup_edu_id").addClass('active');
    }
    else if (short_path_name === "employee-setup-training") {
        $("#emp_setup_training_id").addClass('active');
    }
    else if (short_path_name === "employee-setup-language-membership") {
        $("#emp_setup_lang_membership_id").addClass('active');
    }
    else if (short_path_name === "employee-family") {
        $("#emp_family_id").addClass('active');
    }
    else if (short_path_name === "employee-nominee") {
        $("#emp_nom_id").addClass('active');
    }
    else if (short_path_name === "employee-image") {
        $("#emp_image_id").addClass('active');
    }


    if ($('#employmentTypeCd option:selected').val() != "-1") {

        var employement_Type = $('#employmentTypeCd option:selected').val();

        if (employement_Type == '02') {
            $("#IdtargetAmount").show();
            $("#IdtargetBasis").show();
            $("#IdeffectDate").show();
            $("#IdmunilocCd").hide();
        } else {
            $("#IdtargetAmount").hide();
            $("#IdtargetBasis").hide();
            $("#IdeffectDate").hide();
            $("#IdmunilocCd").show();
        }
    }


    if ($("#emp_master_id").val() != "") {
        $("#emp_tab").show();
        $('#empNmEng').prop('readonly', true);
        $('#employmentTypeCd').attr("disabled", true);
        $('#employeeId').prop('readonly', true);
        $('#employeeTypeCd').attr("disabled", true);
        $('#desigCd').attr("disabled", true);
        $('#officecategory').attr("disabled", true);
        $('#officeCd_one').attr("disabled", true);
        $('#divdeptCd').attr("disabled", true);
        $('#emp_master_add').attr("disabled", true);
        $('#refresh_button_master').attr("disabled", true);
    }

    $(".emp-tab-class").click(function (e) {
        window.location.href = $(this).find('a').attr('href');
    });

    function masterValidate() {

        var status = true;

        if ($("#empNmEng").val() == "") {
            status = false;
            $("#err_emp_name_id").text("Empty field found!!");
            $("#empNmEng").focus();

        } else if ($("#empNmEng").val().length > 50) {
            status = false;
            $("#err_emp_name_id").text("Allow Maximum 50 Character!!");
            $("#empNmEng").focus();
        }
        else $("#err_emp_name_id").text("");


        if ($("#employmentTypeCd option:selected").val() === "-1" && status) {
            status = false;
            $("#err_emp_type_id").text("Empty field found!!");
            $("#employmentTypeCd").focus();
        }
        else $("#err_emp_type_id").text("");


        if ($("#employeeId").val() == "") {
            status = false;
            $("#err_emp_id").text("Empty field found!!");
            $("#employeeId").focus();

        } else if ($("#employeeId").val().length != 8) {
            status = false;
            $("#err_emp_id").text("ID must be 8 characters!!");
            $("#employeeId").focus();
        }
        else $("#err_emp_id").text("");


        if ($("#employeeTypeCd option:selected").val() === "-1" && status) {
            status = false;
            $("#err_emp_cat_id").text("Empty field found!!");
            $("#employeeTypeCd").focus();
        }
        else $("#err_emp_cat_id").text("");


        if ($("#desigCd option:selected").val() === "-1" && status) {
            status = false;
            $("#err_emp_desig_id").text("Empty field found!!");
            $("#desigCd").focus();
        }
        else $("#err_emp_desig_id").text("");

        if ($("#officecategory option:selected").val() === "-1" && status) {
            status = false;
            $("#err_emp_office_cat").text("Empty field found!!");
            $("#officecategory").focus();
        }
        else $("#err_emp_office_cat").text("");

        if ($("#officeCd_one option:selected").val() === "-1" && status) {
            status = false;
            $("#err_emp_office_id").text("Empty field found!!");
            $("#officeCd_one").focus();
        }
        else $("#err_emp_office_id").text("");

        if ($("#divdeptCd option:selected").val() === "-1" && status) {
            status = false;
            $("#err_emp_divdept_id").text("Empty field found!!");
            $("#divdeptCd").focus();
        }
        else $("#err_emp_divdept_id").text("");

        return status;
    }


    $(document).on("change", "#officecategory", function (e) {

        var catId = $("#officecategory option:selected").val();

        $.get("/hrm-admin/get-office-by-category?offCatgId=" + catId,

            function (data, status) {

                var office = $('#officeCd_one');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Office---"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices[0],
                        text: offices[1]
                    }));
                });

            });
    });


    $(document).on("input", "#employeeId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');
        var empId = $('#employeeId').val();

        if (empId != "" && empId.length < 9) {

            $.ajax({
                url: "/hrm-admin/check-emp-id/" + empId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {

                    if (response == true) {

                        status = false;
                        $("#err_emp_id").text("This Employee ID already exists!");
                        $("#emp_master_add").prop('disabled', true);

                    } else {
                        status = true;
                        $("#err_emp_id").text("");
                        $("#emp_master_add").prop('disabled', false);
                    }
                },
                error: function (xhr, status, error) {
                    showAlertByType('Something Wrong!', "F");
                }
            });
        }
        else {

            if (empId == "") {
                $("#err_emp_id").text("Employee ID required!");
            }
            else {
                $("#err_emp_id").text("ID maximum 8 characters!");

            }
        }

    });

    var emp_master = {};
    $(document).on("click", "#emp_master_add", function (e) {

        var flag = masterValidate();

        if (flag == true) {
            emp_master.empNmEng = $('#empNmEng').val();
            emp_master.employmentTypeCd = $('#employmentTypeCd option:selected').val();
            emp_master.employeeId = $('#employeeId').val();
            emp_master.employeeTypeCd = $('#employeeTypeCd option:selected').val();
            emp_master.desigCd = $('#desigCd option:selected').val();
            emp_master.officecategory = $('#officecategory option:selected').val();
            emp_master.officeCd_one = $('#officeCd_one option:selected').val();
            emp_master.divdeptCd = $('#divdeptCd option:selected').val();
            //for update

            $.ajax({
                contentType: 'application/json',
                url: "/hrm-admin/add-employee-master",
                type: 'POST',
                async: false,
                data: JSON.stringify(emp_master),
                dataType: 'json',
                success: function (data) {
                    $("#emp_tab").show();
                    $('#emp_master_id').val(data);
                    $('#empNmEng').prop('readonly', true);
                    $('#employmentTypeCd').attr("disabled", true);
                    $('#employeeId').prop('readonly', true);
                    $('#employeeTypeCd').attr("disabled", true);
                    $('#desigCd').attr("disabled", true);
                    $('#officecategory').attr("disabled", true);
                    $('#officeCd_one').attr("disabled", true);
                    $('#divdeptCd').attr("disabled", true);
                    $('#emp_master_add').attr("disabled", true);
                    $('#refresh_button_master').attr("disabled", true);
                    showAlertByType('Save successfully!!', "S");
                },
                error: function (xhr, status, error) {
                    showAlertByType('Something Wrong!', "F");
                }
            });
        }
        else {
            status = true;
        }

    });

//for education
    function getDateFor() {
        var dateStr = $('#proposalDate').val();
        var dateCom = getDate(dateStr);
        $('#proposalDate').val(dateCom);

        var dateStrFrom = $('#dateto').val();
        var dateComFrom = getDate(dateStrFrom);
        $('#dateto').val(dateComFrom);
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    }

    $("#refresh_button_master").click(function () {
        $("#empNmEng").val('');
        $("#employeeId").val('');
        $("#employeeTypeCd").val('-1');
        $("#officecategory").val('-1');
        $("#desigCd").val('-1').select2();
        $("#officeCd_one").val('-1').select2();
        $("#divdeptCd").val('-1').select2();
        //
        $("#err_emp_name_id").text("");
        $("#err_emp_type_id").text("");
        $("#err_emp_id").text("");
        $("#err_emp_cat_id").text("");
        $("#err_emp_desig_id").text("");
        $("#err_emp_office_cat").text("");
        $("#err_emp_office_id").text("");
        $("#err_emp_divdept_id").text("");
    });


});