function table()
{
    var table = $('#tableData');
    //LIST DATA
    $.ajax({
        type: 'GET',
        url: "/hrm-admin/holidayList",
        dataType: 'json',
        success: function (data) {
            if (data == "") {
                showAlertByType('Data not found', "W");


            }else {
                table.dataTable({
                    paging: true,
                    searching: true,
                    destroy: true,
                    data: data,
                    columns: [{
                        "data": "rowkey"
                    }, {
                        "data": "holdidayTpCd"
                    }, {
                        "data": "holidayNm"
                    }, {
                        "data": "holyDate"
                    }, {
                        "data": "rowkey",
                        "render": function (data,
                                            type,
                                            row) {
                            return "<a class='btn btn-success' id='edit'>"
                                + '<i class="fas fa-comment-alt-dots"></i>'
                                + 'Edit'
                                + "</a>";
                        }


                    }]
                });



            }

        },
        error: function (e) {
            showAlertByType('Something Wrong!', "F");

        }

    });
}
$(document).ready(function () {
    table();
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    })

    $("#save").click(function () {


        if (1) {

            var holdidayTpCd = $('#drp-holiday-name').val();

            if(holdidayTpCd == '1')
            {
                holdidayTpCd = 'W';
            }else if (holdidayTpCd == '2')
            {
                holdidayTpCd = 'W';
            }else if (holdidayTpCd == '3')
            {
                holdidayTpCd = 'G';
            }else
            {
                holdidayTpCd = 'O';
            }
            var holidayDateFrom = moment($('#dt-date-frm').val()).format("DD-MMM-YYYY");
            var holidayDateTo = moment($('#dt-date').val()).format("DD-MMM-YYYY");
            var holidayNm = $('#holiday').val();


            var holidayDto = {};
            holidayDto.holdidayTpCd = holdidayTpCd;
            holidayDto.holidayDateFrom = holidayDateFrom;
            holidayDto.holidayDateTo = holidayDateTo;
            holidayDto.holidayNm = holidayNm;


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/saveHoliday",
                data: JSON.stringify(holidayDto),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    flag = true;

                    $('#drp-question-type').val(-1);
                    $('#drp-question').val(-1);
                    $('#answer').val("");
                    table();

                },
                error: function (e) {
                    showAlertByType("Something Wrong",'F');
                }
            });

        }


    });



    $('#tableData tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(1)').text();
        var col2 = curRow.find('td:eq(2)').text();
        var col3 = curRow.find('td:eq(3)').text();

        if(col1 == 'W' && col2 == 'FRIDAY')
        {
            $('#drp-holiday-name').val("1");

        }else if (col1 == 'W' && col2 == 'SATURDAY')
        {
            $('#drp-holiday-name').val("2");

        }else if (col1 == 'G')
        {
            $('#drp-holiday-name').val("3");
        }else
        {
            $('#drp-holiday-name').val("4");
        }
        $('#dt-date-frm').val(moment(col3).format("MM/DD/YYYY"))
        $('#holiday').val(col2);
        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });




})