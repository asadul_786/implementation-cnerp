$(document).ready(function () {

    $('#nominee_photo_view').attr("src", "/images/no-image.jpg");

    $('#hrmNomineeForm').submit(function (e) {
        e.preventDefault();

        var status = validationNomineeData();

        if (status) {

            $('#hrmNomineeForm').get(0).submit();
        }
    });

    function readURLPhoto(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#nominee_photo_view').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#err_emp_photo_id").text("");
        }
    }

    $(document).on(
        "change",
        "#getImage",
        function (e) {
            if (this.files[0].size > 1048576) {//200KB
                $("#err_emp_photo_id").text(
                    "Photo size can't be greater than 1MB!");
            } else {

                $("#hasNewImage").val(true);
                readURLPhoto(this);
            }
        });
    $("#hasNewImage").val(false);
    //end image

    $("#btnNomineeRefresh").click(function () {
        $('#emp_gid').val("");
        $('#inserDate').val("");
        $('#inserUserName').val("");
        $('#sl_no').val("");
        $('#imagefiled').val("");
        $('#name').val("");
        $('#nomineenid').val("");
        $('#brithdate').val("");
        $('#getImage').val("");
        $('#percentage').val("100");
        $('#relation').val("-1");
        $("#err_name").text("");
        $("#err_percentage").text("");
        $("#err_relation").text("");
        // ($("#nominee_photo_view")).attr("src", "");
        $('#btnNomineeID').text("Save");
        $("#hasNewImage").val(false);
    });

    function validationNomineeData() {

        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($('#name').val() == "") {
            status = false;
            $("#err_name").text("Empty field found!!");
            $("#name").focus();
        } else $("#err_name").text("");

        if ($("#nomineenid").val() != "") {

            if ($("#nomineenid").val().length == 10 || $("#nomineenid").val().length == 13 || $("#nomineenid").val().length == 17) {
                $("#err_nomineenid").text("");
            } else {
                status = false;
                $("#err_nomineenid").text("Invalid NID!!");
                $("#nomineenid").focus();
            }
        } else $("#err_nomineenid").text("");


        if ($('#brithdate').val() == '') {

            status = false;
            $("#err_brithdate").text("Empty field found!!");
            $("#brithdate").focus();


        } else if (!dateCheck.test($("#brithdate").val())) {
            status = false;
            $("#err_brithdate").text("Invalid date!!");
            $("#brithdate").focus();

        } else if (checkvalid_JoinDate($("#brithdate").val())) {
            status = false;
            $("#err_brithdate").text("Birth date should not be greater than the current Date!!");
            $("#brithdate").focus();


        } else $("#err_brithdate").text("");


        if ($('#percentage').val() == "") {
            status = false;
            $("#err_percentage").text("Empty field found!!");
            $("#percentage").focus();

        } else if ($('#percentage').val().length > 5) {
            status = false;
            $("#err_percentage").text("Percentage maximum 5 characters!!");
            $("#percentage").focus();

        } else if ($('#percentage').val() > 100) {
            status = false;
            $("#err_percentage").text("Add nominee between 100% should be included!!");
            $("#percentage").focus();

        } else if (/[^0-9]/g.test($("#percentage").val()) == true) {
            status = false;
            $("#err_percentage").text("Invalid characters!!!!");
            $("#percentage").focus();

        } else $("#err_percentage").text("");

        if ($('#relation').val() == "-1") {
            status = false;
            $("#err_relation").text("Empty field found!!");
            $("#relation").focus();
        } else $("#err_relation").text("");


        return status;
    }

    $('#percentage').val(100);
     var hasChangedImage=false;
    $('#tableData tbody').on('click', '#nomineeEdit', function () {
        $("#hasNewImage").val(false);
        $("#err_name").text("");
        $("#err_percentage").text("");
        $("#err_relation").text("");
        $('#btnNomineeID').text("Update");

        var curRow = $(this).closest('tr');
        var empGid = curRow.find('td:eq(0)').text();
        var nomineeName = curRow.find('td:eq(1)').text();
        var percentage = curRow.find('td:eq(3)').text();
        var relationCD = curRow.find('td:eq(6)').text();
        var insertDate = curRow.find('td:eq(4)').text();
        var inserUser = curRow.find('td:eq(5)').text();
        var slNo = curRow.find('td:eq(7)').text();

        var birthdate = curRow.find('td:eq(8)').text();
        var nid = curRow.find('td:eq(9)').text();
        var photonm = curRow.find('td:eq(10)').text();


        //var photoByte = curRow.find('td:eq(11)').text();

        $('#emp_gid').val(empGid);
        $('#name').val(nomineeName);
        $('#relation').val(relationCD);
        $('#percentage').val(percentage);
        $('#inserDate').val(insertDate);
        $('#inserUserName').val(inserUser);
        $('#sl_no').val(slNo);
        $('#brithdate').val(birthdate);
        $('#nomineenid').val(nid);
        $('#imagefiled').val(photonm);
        var photo64BitString = $(this).closest('tr').find('.nominee-photo').attr('nomineePhotoByte');
        console.log(photo64BitString);



        if (isEmptyString(photo64BitString)) {
            $("#hasNewImage").val(true);
            $("#nominee_photo_view").attr("src","/images/no-image.jpg");
        }
        else {
            $("#hasNewImage").val(false);
            $("#nominee_photo_view").attr("src", 'data:image/'+getPhotoExtension(photonm)+';base64,' + photo64BitString);
        }

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    function getDateForspouse() {
        var inserDate = $('#inserDate').val();
        var inserDate = getDate(inserDate);
        $('#inserDate').val(inserDate);
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    }

    if (saveNotification != undefined) {

        if(saveNotification=='Save Successfully'){
            showAlertByType(saveNotification, "S");
        }else{
            showAlertByType(saveNotification, "F");
        }
    }
 function getPhotoExtension(photonm){
        if (isEmptyString(photonm)){
            return "jpg";
        }
        else{
            return photonm.split('.')[1];
        }
 }
    function checkvalid_JoinDate(enteredDate) {
        var status = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate > today) {
            status = true;
        }
        return status;
    }

});
