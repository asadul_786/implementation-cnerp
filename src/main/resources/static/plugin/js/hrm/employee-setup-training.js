$(document).ready(function () {

    loadCountry();
    loadTrainingTable();

    $("#btnAdd").click(function () {

        var status = validationTrainingData();

        if (status) {
            var emp_gid = $('#emp_gid').val();
            var i_usr = $('#i_usr').val();
            var i_dt = $('#i_dt').val();
            var sl_no = $('#sl_no').val();
            var tr_type = $('#tr_type').val();
            var tr_topics = $('#tr_topics').val();
            var tr_duration = $('#tr_duration').val();
            var country_cd = $('#country_cd').val();
            var tr_dt_from = $('#tr_dt_from').val();
            var tr_dt_to = $('#tr_dt_to').val();
            var tr_inst_nm = $('#tr_inst_nm').val();

            if (tr_inst_nm == '-1') {
                tr_inst_nm = '';
            }

            if (tr_dt_from) {
                tr_dt_from = tr_dt_from.split("/").reverse().join("/");
                tr_dt_from = getFormateDate(tr_dt_from);
            }
            if (tr_dt_to) {
                tr_dt_to = tr_dt_to.split("/").reverse().join("/");
                tr_dt_to = getFormateDate(tr_dt_to);
            }
            function getFormateDate(date) {
                var d = new Date(date);
                return d;
            }

            var training = {};
            training.emp_gid = emp_gid;
            training.i_usr = i_usr;
            training.i_dt = i_dt;
            training.sl_no = sl_no;
            training.tr_type = tr_type;
            training.tr_topics = tr_topics;
            training.tr_duration = tr_duration;
            training.country_cd = country_cd;
            training.tr_dt_from = tr_dt_from;
            training.tr_dt_to = tr_dt_to;
            training.tr_inst_nm = tr_inst_nm;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/addTraining",
                data: JSON.stringify(training),
                dataType: 'json',
                success: function (data) {
                    if (data == '0') {
                        showAlertByType('Save successfully!!', "S");
                    } else {
                        showAlertByType('Save successfully!!', "S");
                    }
                    loadTrainingTable();
                    clearForm();
                },
                error: function (e) {
                    showAlertByType('Sorry,Something Wrong!!', "F");
                }
            });
        }
    });

    function loadTrainingTable() {

        var trainingTable = $('#training_tbl_id');

        $
            .ajax({
                type: "GET",
                url: "/hrm-admin/emp_trainingListInfo",
                success: function (data) {
                    var tableBody = "";
                    var date_from = "";
                    var date_to = "";
                    var duration = "";
                    $('#training_tbl_id tbody').empty();
                    $.each(data, function (idx, elem) {
                        var action =
                            '<button class="btn btn-success childrenInformationEdit" value="Edit" id="editTraining"> ' +
                            '<i class="fa fa-pencil"></i></button> ';

                        if (elem[2] == '0') {
                            duration = '';
                        } else {
                            duration = elem[2];
                        }
                        if (elem[3] == null) {
                            date_from = '';
                        } else {
                            date_from = convertDate(elem[3]);
                        }
                        if (elem[4] == null) {
                            date_to = '';
                        } else {
                            date_to = convertDate(elem[4]);
                        }

                        tableBody += "<tr'>";
                        tableBody += "<td hidden>" + elem[0] + "</td>";
                        tableBody += "<td>" + elem[1] + "</td>";
                        tableBody += "<td>" + duration + "</td>";
                        tableBody += "<td>" + date_from + "</td>";
                        tableBody += "<td>" + date_to + "</td>";
                        tableBody += "<td hidden>" + elem[5] + "</td>";
                        tableBody += "<td hidden>" + elem[6] + "</td>";
                        tableBody += "<td hidden>" + elem[7] + "</td>";
                        tableBody += "<td hidden>" + elem[8] + "</td>";
                        tableBody += "<td hidden>" + elem[9] + "</td>";
                        tableBody += "<td hidden>" + elem[11] + "</td>";
                        tableBody += "<td>" + action + "</td>"
                        tableBody += "<tr>";
                    });
                    trainingTable.append(tableBody);
                }
            });
    }

    $(document).on("change", "#tr_type", function (e) {
        loadCountry();
    });

    function loadCountry() {

        var trainigType = $("#tr_type option:selected").val();

        if (trainigType == '1') {
            var country = $('#country_cd');
            country.empty();
            country.append($('<option/>', {
                value: "001",
                text: "Bangladesh"
            }));

        } else {

            $.get("/hrm-admin/getCountryMap",

                function (data, status) {
                    var country = $('#country_cd');
                    country.empty();
                    country.append($('<option/>', {
                        value: "-1",
                        text: "---Select Country---"
                    }));
                    $.each(data, function (index, offices) {
                        country.append($('<option/>', {
                            value: index,
                            text: offices
                        }));
                    });

                });
        }
    }

    $("#btn_RefreshTraining").click(function () {
        clearForm();
    });


    function clearForm() {
        $('#emp_gid').val("");
        $('#i_dt').val("");
        $('#i_usr').val("");
        $('#sl_no').val("");
        $('#tr_type').val("1");
        $('#tr_topics').val("");
        $('#tr_duration').val("");
        $('#tr_dt_from').val("");
        $('#tr_dt_to').val("");
        $('#country_cd').val("-1");
        $('#tr_inst_nm').val("-1");
        loadCountry();
        $('#btnAdd').text("Save");
        //
        $("#err_country_cd").text("");
        $("#err_tr_dt_to").text("");
        $("#err_tr_dt_from").text("");
        $("#err_tr_topics").text("");
        $("#err_tr_duration").text("");
    }

    function validationTrainingData() {

        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");
        var status = true;

        if ($('#tr_topics').val() == "") {
            status = false;
            $("#err_tr_topics").text("Empty field found!!");
            $("#tr_topics").focus();
        } else $("#err_tr_topics").text("");

        if ($('#tr_dt_from').val() != "") {
            if (!dateCheck.test($("#tr_dt_from").val())) {
                status = false;
                $("#err_tr_dt_from").text("Invalid Date format!!");
                $("#tr_dt_from").focus();
            } else if (isValidDate($("#tr_dt_from").val()) == false) {
                status = false;
                $("#err_tr_dt_from").text("Invalid Date format!!");
                $("#tr_dt_from").focus();

            } else $("#err_tr_dt_from").text("");

        } else $("#err_tr_dt_from").text("");


        if ($('#tr_dt_to').val() != "") {

            if (!dateCheck.test($("#tr_dt_to").val())) {
                status = false;
                $("#err_tr_dt_to").text("Invalid Date format!!");
                $("#tr_dt_to").focus();
            } else if (isValidDate($("#tr_dt_to").val()) == false) {
                status = false;
                $("#err_tr_dt_to").text("Invalid Date format!!");
                $("#tr_dt_to").focus();

            } else if (lessthenOrderDate($("#tr_dt_from").val(), $("#tr_dt_to").val())) {
                status = false;
                $("#err_tr_dt_to").text("To date should be greater than From date!!");
                $("#tr_dt_to").focus();
            } else $("#err_tr_dt_to").text("");

        } else $("#err_tr_dt_to").text("");


        if ($("#tr_type option:selected").val() == '2') {

            if ($('#country_cd').val() == "-1") {
                status = false;
                $("#err_country_cd").text("Empty field found!!");
                $("#country_cd").focus();
            } else $("#err_country_cd").text("");
        }

        if ($('#tr_duration').val() != "") {
            if ($('#tr_duration').val().length > 10) {
                status = false;
                $("#err_tr_duration").text("Allow maximum 10 character!!");
                $("#tr_duration").focus();
            } else $("#err_tr_duration").text("");
        } else $("#err_tr_duration").text("");


        return status;
    }


    $('#training_tbl_id tbody').on('click', '#editTraining', function () {

        $("#err_country_cd").text("");
        $("#err_tr_dt_to").text("");
        $("#err_tr_dt_from").text();
        $("#err_tr_topics").text("");

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();
        var col7 = curRow.find('td:eq(6)').text();
        //
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();
        var col10 = curRow.find('td:eq(9)').text();
        var SlNo = curRow.find('td:eq(10)').text();

        if (col6 == 'null') {
            col6 = '-1';
        }
        $('#btnAdd').text("Update");
        $('#sl_no').val(SlNo);
        $('#tr_type').val(col1);
        $('#tr_topics').val(col2);
        $('#tr_duration').val(col3);
        $('#tr_dt_from').val(col4);
        $('#tr_dt_to').val(col5);
        $('#tr_inst_nm').val(col6);
        $('#emp_gid').val(col8);
        $('#i_dt').val(col10);
        $('#i_usr').val(col9);
        loadCountryData(col7);

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

        $('#proposalDate').val(col4);
        $('#dateto').val(col5);
    });


    function loadCountryData(getdata) {

        var trainigType = $("#tr_type option:selected").val();

        if (trainigType == '1') {
            var country = $('#country_cd');
            country.empty();
            country.append($('<option/>', {
                value: "001",
                text: "Bangladesh"
            }));

        } else {

            $.get("/hrm-admin/getCountryMap",

                function (data, status) {
                    var country = $('#country_cd');
                    country.empty();
                    country.append($('<option/>', {
                        value: "-1",
                        text: "---Select Country---"
                    }));
                    $.each(data, function (index, offices) {
                        country.append($('<option/>', {
                            value: index,
                            text: offices,
                            selected: index == getdata
                        }));
                    });
                });
        }
    }

    function lessthenOrderDate(fromdate, todate) {
        var lessStatus = false;
        //release date
        var date_r = fromdate.substring(0, 2);
        var month_r = fromdate.substring(3, 5);
        var year_r = fromdate.substring(6, 10);
        var fromdate_date = new Date(year_r, month_r - 1, date_r);
// orderDate
        var date_o = todate.substring(0, 2);
        var month_o = todate.substring(3, 5);
        var year_o = todate.substring(6, 10);
        var todate_date = new Date(year_o, month_o - 1, date_o);

        if (fromdate_date > todate_date) {
            lessStatus = true;
        }
        return lessStatus;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    function convertDate(dateObject) {

        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    }

});