
$(document).ready(function () {
    $('#postingdataTable tbody').on('click', '#getCurrentDate', function () {

        var curRow = $(this).closest('tr');
        var col11 = curRow.find('td:eq(13)').text();

        var col1 = curRow.find('td:eq(14)').text();
        var col2 = curRow.find('td:eq(15)').text();
        var col3 = curRow.find('td:eq(16)').text();
        var col4 = curRow.find('td:eq(17)').text();
        var col5 = curRow.find('td:eq(18)').text();
        var col6 = curRow.find('td:eq(19)').text();
        var col7 = curRow.find('td:eq(20)').text();
        var col8 = curRow.find('td:eq(21)').text();
        var col9 = curRow.find('td:eq(22)').text();
        var col10 = curRow.find('td:eq(23)').text();
        var col12 = curRow.find('td:eq(24)').text();
        var authcationName = curRow.find('td:eq(25)').text();
        var offficeorderNo = curRow.find('td:eq(26)').text();

        var authorityName = curRow.find('td:eq(2)').text();
        var postingOffice = curRow.find('td:eq(3)').text();

        $('#updateempgid').val(col12);
        // $('#officeCode').val(col1);
        // $('#authorityCd').val(col2);
        $('#officeorderDate').val(col3);
        $('#departmentName').val(col4);
        $('#releaseType').val(col5);
        //new
        $('#referenceNo').val(col6);
        $('#accountClearness').val(col7);
        $('#releaseDate').val(col8);

        var today = new Date();
        var date = (today.getDate()+1)+'/'+(today.getMonth()+1)+'/'+ today.getFullYear();

        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output =  (day<10 ? '0' : '') + day+ '/' +
            (month<10 ? '0' : '') + month + '/' +
            d.getFullYear() ;



        $('#joiningDate').val(output);

        $('#remarks').val(col10);
        $('#officeorderNo').val(offficeorderNo);



        getDateFor();

        var authorityCd = $('#authorityCd');
        authorityCd.empty();
        authorityCd.append($('<option/>', {
            value: col2,
            text: authcationName
        }));

        var officeCode = $('#officeCode');
        officeCode.empty();
        officeCode.append($('<option/>', {
            value: col1,
            text: postingOffice
        }));

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


        $.ajax({
            url: "/hrm-admin/fetchEmployeeId/" + col11,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $("#empId").val(response);

                $.ajax({
                    url: "/hrm-admin/getEmployeeDetails/" + response,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var value_1 = "";
                        var value_2 = "";
                        var value_3 = "";
                        var value_4 = "";
                        var value_5 = "";
                        //
                        var value_6 = "";
                        var value_7 = "";
                        var value_8 = "";

                        $.each(response, function (i, l) {
                            value_1 = l[0];
                            value_2 = l[1];
                            value_3 = l[2];
                            value_4 = l[3];
                            value_5 = l[4];
                            //
                            value_6 = l[5];
                            value_7 = l[6];
                            value_8 = l[7];
                        });
                        $("#empgid").val(value_1);
                        $("#empName").val(value_2);
                        $("#officeName").val(value_3);
                        $("#empDesignation").val(value_4);
                        $("#empDepartment").val(value_5);
                        $("#err_empID").text("");
                        $("#vofficeName").val(value_6);
                        $("#vempDesignation").val(value_7);
                        $("#vempDepartment").val(value_8);

                    },
                    error: function (xhr, status, error) {
                        $("#empgid").val("");
                        $("#empName").val("");
                        $("#officeName").val("");
                        $("#empDesignation").val("");
                        $("#empDepartment").val("");
                        $("#err_empID").text("Sorry! Employee ID not match!!");
                        $("#vofficeName").val("");
                        $("#vempDesignation").val("");
                        $("#vempDepartment").val("");

                    }
                });


            },
            error: function (xhr, status, error) {
                showAlertByType("Sorry,Something Wrong!!","F");

            }
        });


    });

    $('#postingdataTable').DataTable();


    $("#btnpostingSave").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var empId = $('#empId').val();
            var empgid = $('#empgid').val();
            var empName = $('#empName').val();
            var officeName = $('#officeName').val();
            var empDesignation = $('#empDesignation').val();
            var empDepartment = $('#empDepartment').val();
//transfer info
            var authorityCd = $('#authorityCd').val();
            var officeCode = $('#officeCode').val();
            var releaseType = $('#releaseType').val();
            var officeorderNo = $('#officeorderNo').val();
            var departmentName = $('#departmentName').val();
            var officeorderDate = $('#officeorderDate').val();
            //release type
            var referenceNo = $('#referenceNo').val();
            var releaseDate = $('#releaseDate').val();
            var accountClearness = $('#accountClearness').val();
            //joing info
            var joiningDate = $('#joiningDate').val();
            var remarks = $('#remarks').val();
            var updateempgid = $('#updateempgid').val();



            var postingInfo = {};

            postingInfo.empId = empId;
            postingInfo.empgid = empgid;
            postingInfo.empName = empName;
            postingInfo.officeName = officeName;
            postingInfo.empDesignation = empDesignation;
            postingInfo.empDepartment = empDepartment;
            //transfser
            postingInfo.authorityCd = authorityCd;
            postingInfo.officeCode = officeCode;
            postingInfo.officeorderNo = officeorderNo;
            postingInfo.releaseType = releaseType;
            postingInfo.departmentName = departmentName;
            postingInfo.officeorderDate = officeorderDate;
//release type
            postingInfo.referenceNo = referenceNo;
            postingInfo.releaseDate = releaseDate;
            postingInfo.accountClearness = accountClearness;
            //joing info
            postingInfo.joiningDate = joiningDate;
            postingInfo.remarks = remarks;
            postingInfo.updateempgid = updateempgid;



            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/addPostingInfo",
                data: JSON.stringify(postingInfo),
                dataType: 'json',
                success: function (data) {
                    setInterval(function () {
                        location.reload(true);
                    }, 2000);
                    showAlertByType("Successfully added.","S");
                    clearForm();
                },
                error: function (e) {
                    showAlertByType("Sorry,Something Wrong!!","F");
                }
            });

        }

    });

    $("#btnpostingReferesh").click(function () {
        clearForm();
    });


    $(document).on("input", "#empId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var empId = $('#empId').val();
        $('#updateempgid').val("");

        if (empId != "" && empId.length < 9) {
            $.ajax({
                url: "/hrm-admin/getEmployeeDetails/" + empId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    var value_5 = "";
                    //
                    var value_6 = "";
                    var value_7 = "";
                    var value_8 = "";

                    $.each(response, function (i, l) {
                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                        value_5 = l[4];
                        //
                        value_6 = l[5];
                        value_7 = l[6];
                        value_8 = l[7];
                    });
                    $("#empgid").val(value_1);
                    $("#empName").val(value_2);
                    $("#officeName").val(value_3);
                    $("#empDesignation").val(value_4);
                    $("#empDepartment").val(value_5);
                    $("#err_empID").text("");
                    //
                    $("#vofficeName").val(value_6);
                    $("#vempDesignation").val(value_7);
                    $("#vempDepartment").val(value_8);
                },
                error: function (xhr, status, error) {
                    $("#empgid").val("");
                    $("#empName").val("");
                    $("#officeName").val("");
                    $("#empDesignation").val("");
                    $("#empDepartment").val("");
                    $("#err_empID").text("Sorry! Employee ID not match!!");
                    //
                    $("#vofficeName").val("");
                    $("#vempDesignation").val("");
                    $("#vempDepartment").val("");
                }
            });
        }
        else {
            if ($('#empId').val() == "") {
                $("#vofficeName").val("");
                $("#vempDesignation").val("");
                $("#vempDepartment").val("");
                $("#err_empID").text("Employee ID required!");
                $("#empgid").val("");
                $("#empName").val("");
                $("#officeName").val("");
                $("#empDesignation").val("");
                $("#empDepartment").val("");
            }
            else {
                $("#err_empID").text("ID maximum 8 characters!");
            }
        }

    });


    function dataValidation() {
        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($("#empId").val() == "") {
            status = false;
            $("#err_empID").text("Empty field found!!");
            $("#empId").focus();
        } else $("#err_empID").text("");


        if ($("#authorityCd").val() == -1) {
            status = false;
            $("#err_authorityCd").text("Empty field found!!");
            $("#authorityCd").focus();
        } else $("#err_authorityCd").text("");

        if ($("#officeCode").val() == -1) {
            status = false;
            $("#err_officeCode").text("Empty field found!!");
            $("#officeCode").focus();
        }
        else $("#err_officeCode").text("");

        if ($("#departmentName").val() == -1) {
            status = false;
            $("#err_departmentName").text("Empty field found!!");
            $("#departmentName").focus();
        }
        else $("#err_departmentName").text("");

        if ($("#officeorderDate").val() == "") {
            status = false;
            $("#err_officeorderDate").text("Empty field found!!");
            $("#officeorderDate").focus();

        } else if ($("#officeorderDate").val() != "") {
            if (!dateCheck.test($("#officeorderDate").val())) {
                status = false;
                $("#err_officeorderDate").text("Invalid Date format!!");
                $("#officeorderDate").focus();
            } else if (checkDate($("#officeorderDate").val())) {
                status = false;
                $("#err_officeorderDate").text("Office order date can't be future date!!");
                $("#officeorderDate").focus();
            } else $("#err_officeorderDate").text("");

        } else $("#err_officeorderDate").text("");


        if ($("#releaseDate").val() != "") {
            if (!dateCheck.test($("#releaseDate").val())) {
                status = false;
                $("#err_releaseDate").text("Invalid Date format!!");
                $("#releaseDate").focus();
            } else $("#err_releaseDate").text("");

        } else $("#err_releaseDate").text("");


        if ($("#releaseDate").val() != "" && $("#officeorderDate").val() != "") {
            if (lessthenOrderDate($("#releaseDate").val(), $("#officeorderDate").val())) {
                status = false;
                $("#err_releaseDate").text("Release date should be less than Office order date!!");
                $("#releaseDate").focus();
            } else $("#err_releaseDate").text("");
        }

        if ($("#joiningDate").val() != "") {
            if (!dateCheck.test($("#joiningDate").val())) {

                status = false;
                $("#err_joiningDate").text("Invalid Date format!!");
                $("#joiningDate").focus();

            } else if (validationJoingDate($("#joiningDate").val())) {

                status = false;
                $("#err_joiningDate").text("Joining date should not be back date!!");
                $("#joiningDate").focus();

            } else $("#err_joiningDate").text("");

        } else $("#err_joiningDate").text("");


        if ($("#accountClearness").val() == -1) {
            status = false;
            $("#err_accountClearness").text("Empty field found!!");
            $("#accountClearness").focus();
        }
        else $("#err_accountClearness").text("");

        if ($("#releaseType").val() == -1) {
            status = false;
            $("#err_releaseType").text("Empty field found!!");
            $("#releaseType").focus();
        }
        else $("#err_releaseType").text("");

        return status;
    }



    function clearForm() {
        $("#empgid").val("");
        $("#empId").val("");
        $("#empName").val("");
        $("#officeName").val("");
        $("#empDesignation").val("");
        $("#empDepartment").val("");
        $("#authorityCd").val("-1");
        $("#officeCode").val("-1");
        $("#releaseType").val("-1");
        $("#officeorderNo").val("");
        $("#departmentName").val("-1");
        $("#officeorderDate").val("");
        $("#referenceNo").val("");
        $("#releaseDate").val("");
        $("#accountClearness").val("-1");
        $("#joiningDate").val("");
        $("#remarks").val("");
        $("#updateempgid").val("");
        //
        $("#vofficeName").val("");
        $("#vempDesignation").val("");
        $("#vempDepartment").val("");
        //error resolve
        $("#err_empID").text("");
        $("#err_authorityCd").text("");
        $("#err_officeCode").text("");
        $("#err_departmentName").text("");
        $("#err_officeorderDate").text("");
        $("#err_releaseDate").text("");
        $("#err_releaseDate").text("");
        $("#err_joiningDate").text("");
        $("#err_accountClearness").text("");
        $("#err_releaseType").text("");

        $("#Office1").val("-1");
        $("#OrderOffice").val("-1");
    }

    $('#postingdataTable tbody').on('click', '#editpostingInfo', function () {

        var curRow = $(this).closest('tr');
        var col11 = curRow.find('td:eq(13)').text();

        var col1 = curRow.find('td:eq(14)').text();
        var col2 = curRow.find('td:eq(15)').text();
        var col3 = curRow.find('td:eq(16)').text();
        var col4 = curRow.find('td:eq(17)').text();
        var col5 = curRow.find('td:eq(18)').text();
        var col6 = curRow.find('td:eq(19)').text();
        var col7 = curRow.find('td:eq(20)').text();
        var col8 = curRow.find('td:eq(21)').text();
        var col9 = curRow.find('td:eq(22)').text();
        var col10 = curRow.find('td:eq(23)').text();
        var col12 = curRow.find('td:eq(24)').text();
        var authcationName = curRow.find('td:eq(25)').text();
        var offficeorderNo = curRow.find('td:eq(26)').text();

        var authorityName = curRow.find('td:eq(2)').text();
        var postingOffice = curRow.find('td:eq(3)').text();

        $('#updateempgid').val(col12);
        // $('#officeCode').val(col1);
        // $('#authorityCd').val(col2);
        $('#officeorderDate').val(col3);
        $('#departmentName').val(col4);
        $('#releaseType').val(col5);
        //new
        $('#referenceNo').val(col6);
        $('#accountClearness').val(col7);
        $('#releaseDate').val(col8);
        $('#joiningDate').val(col9);
        $('#remarks').val(col10);
        $('#officeorderNo').val(offficeorderNo);



        getDateFor();

        var authorityCd = $('#authorityCd');
        authorityCd.empty();
        authorityCd.append($('<option/>', {
            value: col2,
            text: authcationName
        }));

        var officeCode = $('#officeCode');
        officeCode.empty();
        officeCode.append($('<option/>', {
            value: col1,
            text: postingOffice
        }));

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


        $.ajax({
            url: "/hrm-admin/fetchEmployeeId/" + col11,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $("#empId").val(response);

                $.ajax({
                    url: "/hrm-admin/getEmployeeDetails/" + response,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var value_1 = "";
                        var value_2 = "";
                        var value_3 = "";
                        var value_4 = "";
                        var value_5 = "";
                        //
                        var value_6 = "";
                        var value_7 = "";
                        var value_8 = "";

                        $.each(response, function (i, l) {
                            value_1 = l[0];
                            value_2 = l[1];
                            value_3 = l[2];
                            value_4 = l[3];
                            value_5 = l[4];
                            //
                            value_6 = l[5];
                            value_7 = l[6];
                            value_8 = l[7];
                        });
                        $("#empgid").val(value_1);
                        $("#empName").val(value_2);
                        $("#officeName").val(value_3);
                        $("#empDesignation").val(value_4);
                        $("#empDepartment").val(value_5);
                        $("#err_empID").text("");
                        $("#vofficeName").val(value_6);
                        $("#vempDesignation").val(value_7);
                        $("#vempDepartment").val(value_8);

                    },
                    error: function (xhr, status, error) {
                        $("#empgid").val("");
                        $("#empName").val("");
                        $("#officeName").val("");
                        $("#empDesignation").val("");
                        $("#empDepartment").val("");
                        $("#err_empID").text("Sorry! Employee ID not match!!");
                        $("#vofficeName").val("");
                        $("#vempDesignation").val("");
                        $("#vempDepartment").val("");

                    }
                });


            },
            error: function (xhr, status, error) {
                showAlertByType("Sorry,Something Wrong!!","F");
            }
        });


    });

    function getDateFor() {
        var dateStr = $('#officeorderDate').val();
        var dateCom = getDate(dateStr);
        $('#officeorderDate').val(dateCom);

//
        var dateStr1 = $('#releaseDate').val();
        var dateCom1 = getDateReleaseDate(dateStr1);
        $('#releaseDate').val(dateCom1);
//
        var dateStr2 = $('#joiningDate').val();
        var dateCom2 = getDateJoiningDate(dateStr2);


    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = month + "/" + day + "/" + year;
        return date;
    }

    function getDateReleaseDate(dateObject) {

        if (dateObject != "") {

            var d = new Date(dateObject);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = month + "/" + day + "/" + year;

            return date;
        }

    }

    function getDateJoiningDate(dateObject) {

        if (dateObject != "") {

            var d = new Date(dateObject);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = month + "/" + day + "/" + year;
            return date;
        }
    }

    function checkDate(enteredDate) {
        var datevalidationStatus = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate > today) {
            datevalidationStatus = true;
        }
        return datevalidationStatus;
    }

    function validationJoingDate(enteredDate) {
        var datestatus = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate >= today) {
            datestatus = false;
        } else {
            datestatus = true;
        }
        return datestatus;
    }

    function lessthenOrderDate(releaseDate, orderDate) {
        var lessStatus = false;
        //release date
        var date_r = releaseDate.substring(0, 2);
        var month_r = releaseDate.substring(3, 5);
        var year_r = releaseDate.substring(6, 10);
        var release_date = new Date(year_r, month_r - 1, date_r);
// orderDate
        var date_o = orderDate.substring(0, 2);
        var month_o = orderDate.substring(3, 5);
        var year_o = orderDate.substring(6, 10);
        var order_date = new Date(year_o, month_o - 1, date_o);

        if (release_date < order_date) {
            lessStatus = true;
        }
        return lessStatus;
    }


    $(document).on("change", "#OrderOffice", function (e) {

        var catId = $("#OrderOffice option:selected").val();

        $.get("/hrm-admin/get-office-by-category?offCatgId=" + catId,

            function (data, status) {

                var office = $('#authorityCd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select branch office---"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices[0],
                        text: offices[1]
                    }));
                });

            });
    });


    $(document).on("change", "#Office1", function (e) {

        var catId = $("#Office1 option:selected").val();

        $.get("/hrm-admin/get-office-by-category?offCatgId=" + catId,

            function (data, status) {

                var office = $('#officeCode');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select branch office---"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices[0],
                        text: offices[1]
                    }));
                });

            });
    });

});
