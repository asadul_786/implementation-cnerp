/**
 * Created by golam Nobi on 1/8/2020.
 */

$(document).ready(function () {

    loadEduTable();
    $('#degreeCd').select2();
    $('#subjectCd').select2();
    $('#boardCd').select2();
    $('#degreeCd').attr('disabled', false);

    $("#addEducation").click(function () {

        var status = validationEducationData();

        if (status) {

            var emp_gid = $('#emp_gid').val();
            var i_dt = $('#i_dt').val();
            var i_usr = $('#i_usr').val();
            var degreeCd = $('#degreeCd').val();
            var subjectCd = $('#subjectCd').val();
            var boardCd = $('#boardCd').val();
            var resultCd = $('#resultCd').val();
            var tmarks_cgpa_gpa = $('#tmarks_cgpa_gpa').val();
            var passing_yr = $('#passing_yr').val();

            if (subjectCd == '-1') {
                subjectCd = '';
            }
            if (boardCd == '-1') {
                boardCd = '';
            }

            var education = {};
            education.emp_gid = emp_gid;
            education.i_dt = i_dt;
            education.i_usr = i_usr;
            education.degreeCd = degreeCd;
            education.subjectCd = subjectCd;
            education.boardCd = boardCd;
            education.resultCd = resultCd;
            education.tmarks_cgpa_gpa = tmarks_cgpa_gpa;
            education.passing_yr = passing_yr;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/addEducation",
                data: JSON.stringify(education),
                dataType: 'json',
                success: function (data) {
                    if (data == '0') {
                        showAlertByType('Save successfully!!', "S");
                    } else {
                        showAlertByType('Save successfully!!', "S");
                    }
                    loadEduTable();
                    clearForm();
                },
                error: function (e) {
                    showAlertByType('Sorry,Something Wrong!!', "F");
                }
            });
        }
    });


    function loadEduTable() {

        var educationTable = $('#eduTable');

        $
            .ajax({
                type: "GET",
                url: "/hrm-admin/emp_educationListInfo",
                success: function (data) {
                    var no = 1;
                    var tableBody = "";
                    $('#eduTable tbody').empty();
                    var checkSubject = "";
                    var checkBoard = "";
                    var checkExamMarks = "";
                    var checkpassingyr = "";
                    var resultName = "";
                    $.each(data, function (idx, elem) {

                        var action =
                            '<button class="btn btn-success childrenInformationEdit" value="Edit" id="editEducation"> ' +
                            '<i class="fa fa-pencil"></i></button> ';

                        if (elem[4] == null) {
                            checkExamMarks = '';
                        } else {
                            checkExamMarks = elem[4];
                        }
                        if (elem[5] == '0' || elem[5] == null) {
                            checkpassingyr = '';
                        } else {
                            checkpassingyr = elem[5];
                        }
                        if (elem[10] == null) {
                            checkSubject = '';
                        } else {
                            checkSubject = elem[10];
                        }
                        if (elem[11] == null) {
                            checkBoard = '';
                        } else {
                            checkBoard = elem[11];
                        }
                        if (elem[12] == null) {
                            resultName = '';
                        } else {
                            resultName = elem[12];
                        }
                        tableBody += "<tr'>";
                        tableBody += "<td hidden>" + elem[0] + "</td>";
                        tableBody += "<td hidden>" + elem[1] + "</td>";
                        tableBody += "<td hidden>" + elem[2] + "</td>";
                        tableBody += "<td hidden>" + elem[3] + "</td>";
                        tableBody += "<td>" + checkExamMarks + "</td>";
                        tableBody += "<td>" + checkpassingyr + "</td>";
                        tableBody += "<td hidden>" + elem[6] + "</td>";
                        tableBody += "<td hidden>" + elem[7] + "</td>";
                        tableBody += "<td hidden>" + elem[8] + "</td>";
                        tableBody += "<td>" + elem[9] + "</td>";
                        tableBody += "<td>" + checkSubject + "</td>";
                        tableBody += "<td>" + checkBoard + "</td>";
                        tableBody += "<td>" + resultName + "</td>";
                        tableBody += "<td>" + action + "</td>"
                        tableBody += "<tr>";
                    });
                    educationTable.append(tableBody);
                }
            });
    }

    function clearForm() {
        $("#emp_gid").val("");
        $("#i_dt").val("");
        $("#i_usr").val("");
        $("#degreeCd").val("-1").select2();
        $("#subjectCd").val("-1").select2();
        $("#boardCd").val("-1").select2();
        $("#resultCd").val("-1");
        $("#tmarks_cgpa_gpa").val("");
        $("#passing_yr").val("");
        $('#addEducation').text('Save');
        //
        $('#degreeCd').attr('disabled', false);
        clrErrorMsg();
    }
    function clrErrorMsg() {
        $("#err_emp_degree").text("");
        $("#err_emp_result").text("");
        $("#err_emp_passing_year").text("");
        $("#err_emp_marks").text("");
    }

    $("#btnEduRefresh").click(function () {
        clearForm();
    });


    function validationEducationData() {

        var status = true;

        if ($('#degreeCd').val() == "-1") {
            status = false;
            $("#err_emp_degree").text("Empty field found!!");
            $("#degreeCd").focus();
        } else $("#err_emp_degree").text("");

        if ($('#resultCd').val() == "-1") {
            status = false;
            $("#err_emp_result").text("Empty field found!!");
            $("#resultCd").focus();
        } else $("#err_emp_result").text("");


        if($('#passing_yr').val() == ""){
            status = false;
            $("#err_emp_passing_year").text("Empty field found!!");
            $("#passing_yr").focus();
        }else if($('#passing_yr').val().length > 4){
            status = false;
            $("#err_emp_passing_year").text("Passing Year maximum 4 characters!!");
            $("#passing_yr").focus();

        }else if(/[^0-9]/g.test($("#passing_yr").val()) == true){
            status = false;
            $("#err_emp_passing_year").text("Invalid Year!!");
            $("#passing_yr").focus();
        }else{
            $("#err_emp_passing_year").text("");
        }

        if ($('#tmarks_cgpa_gpa').val() != "") {
            if ($('#tmarks_cgpa_gpa').val().length > 10) {
                status = false;
                $("#err_emp_marks").text("Exam mark maximum 10 characters!!");
                $("#tmarks_cgpa_gpa").focus();
            } else $("#err_emp_marks").text("");

        } else $("#err_emp_marks").text("");

        return status;
    }


    $('#eduTable tbody').on('click', '#editEducation', function () {

        $('#addEducation').text('Update');
        $('#degreeCd').attr('disabled', true);
        clrErrorMsg();

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();
        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();

        var col7 = curRow.find('td:eq(6)').text();
        var col8 = curRow.find('td:eq(7)').text();
        var col9 = curRow.find('td:eq(8)').text();
        //

        if (col2 == 'null') {
            col2 = '-1';
        }
        if (col3 == 'null') {
            col3 = '-1';
        }

        $('#degreeCd').val(col1).select2();
        $('#subjectCd').val(col2).select2();
        $('#boardCd').val(col3).select2();
        $('#resultCd').val(col4);
        $('#tmarks_cgpa_gpa').val(col5);
        $('#passing_yr').val(col6);
        //
        $('#emp_gid').val(col7);
        $('#i_dt').val(col9);
        $('#i_usr').val(col8);

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

});
