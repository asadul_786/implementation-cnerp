/**
 * Created by golam nobi.
 */
$(document).ready(function () {

    var getValue = $('#getpayscaleId').val();
    //var getValue1 = $('#getpayscaleDescription').val();


    if ($('#employmentTypeCd').val() != "") {
        if ($('#employmentTypeCd').val() != "-1") {
            payscaleMode($('#employmentTypeCd').val(), getValue);
        }
    }


    $("#emp_job_detailId").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var joinDate = $('#joinDate').val();
            var confirmDate = $('#confirmDate').val();
            var lprDate = $('#lprDate').val();
            var retireDt = $('#retireDt').val();
            var pfcDedStat = $('#pfcDedStat').val();
            var salPayStat = $('#salPayStat option:selected').val();
            var activityCd = $('#activityCd option:selected').val();
            var employementStCd = $('#employementStCd option:selected').val();
            var serviceCd = $('#serviceCd option:selected').val();
            var payscId = $('#payscId option:selected').val();
            var effectDtOnSalary = $('#effectDtOnSalary').val();
            var currBasic = $('#currBasic').val();
            var lastIncreDt = $('#lastIncreDt').val();
            var lastPromDt = $('#lastPromDt').val();
            var officeCdone = $('#officeCdone option:selected').val();
            var penPayStat = $('#penPayStat option:selected').val();
            var payLevel = $('#payLevel option:selected').val();

            if (joinDate) {
                joinDate = joinDate.split("/").reverse().join("/");
                joinDate = getFormateDate(joinDate);
            }
            if (confirmDate) {
                confirmDate = confirmDate.split("/").reverse().join("/");
                confirmDate = getFormateDate(confirmDate);
            }
            if (lprDate) {
                lprDate = lprDate.split("/").reverse().join("/");
                lprDate = getFormateDate(lprDate);
            }
            if (retireDt) {
                retireDt = retireDt.split("/").reverse().join("/");
                retireDt = getFormateDate(retireDt);
            }
            if (effectDtOnSalary) {
                effectDtOnSalary = effectDtOnSalary.split("/").reverse().join("/");
                effectDtOnSalary = getFormateDate(effectDtOnSalary);
            }
            if (lastIncreDt) {
                lastIncreDt = lastIncreDt.split("/").reverse().join("/");
                lastIncreDt = getFormateDate(lastIncreDt);
            }
            if (lastPromDt) {
                lastPromDt = lastPromDt.split("/").reverse().join("/");
                lastPromDt = getFormateDate(lastPromDt);
            }

            function getFormateDate(date) {
                var d = new Date(date);
                return d;
            }

            var jobdetails = {};

            jobdetails.joinDate = joinDate;
            jobdetails.confirmDate = confirmDate;
            jobdetails.lprDate = lprDate;
            jobdetails.retireDt = retireDt;
            jobdetails.pfcDedStat = pfcDedStat;
            jobdetails.salPayStat = salPayStat;
            jobdetails.activityCd = activityCd;
            jobdetails.employementStCd = employementStCd;
            jobdetails.serviceCd = serviceCd;
            jobdetails.payscId = payscId;
            jobdetails.effectDtOnSalary = effectDtOnSalary;
            jobdetails.currBasic = currBasic;
            jobdetails.lastIncreDt = lastIncreDt;
            jobdetails.lastPromDt = lastPromDt;
            jobdetails.officeCdone = officeCdone;
            jobdetails.penPayStat = penPayStat;
            jobdetails.payLevel = payLevel;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/hrm-admin/addjobDetails",
                data: JSON.stringify(jobdetails),
                dataType: 'json',
                success: function (data) {
                    showAlertByType('Save successfully!!', "S");
                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");
                }
            });
        }
    });


    $(document).on("change", "#payLevel", function (e) {
        var s = $("#payLevel option:selected").text();
        s = s.substring(0, s.indexOf('('));
        $("#currBasic").val(s);
    });


    $(document).on("change", "#payscId", function (e) {

        var catId = $("#payscId option:selected").val();

        $.get("/hrm-admin/get-payScale?scaleId=" + catId,

            function (data, status) {

                var office = $('#payLevel');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "--Select Pay Level--"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices[0],
                        text: offices[1] + ' (Pay Level= ' + (offices[0]) + ')'
                    }));
                });

            });
    });


    $(document).on("change", "#joinDate", function (e) {

        var join_date = $('#empdateofbirth').val();

        if (join_date != "") {

            var cal_join = $('#joinDate').val();

            var parts = join_date.split('/');
            var mydate = new Date(parts[2], parts[1], parts[0]); //year month day3
            var year = mydate.getFullYear();
            var month = mydate.getMonth();
            var day = mydate.getDate();

            //===========
            var cal_join_parts = cal_join.split('/');
            var cal_join_mydate = new Date(cal_join_parts[2], cal_join_parts[1], cal_join_parts[0]); //year month day3
            var cal_join_year = cal_join_mydate.getFullYear();
            var cal_join_month = cal_join_mydate.getMonth();
            var cal_join_day = cal_join_mydate.getDate();
            //===========

            var lpr = new Date(year + 59, month, day);
            var lpr_year = lpr.getFullYear();
            var lpr_month = lpr.getMonth();
            var lpr_day = lpr.getDate();

            var retire = new Date(year + 60, month, day);
            var retire_year = retire.getFullYear();
            var retire_month = retire.getMonth();
            var retire_day = retire.getDate();

            var confirm_date = new Date(cal_join_year, cal_join_month + 6, cal_join_day);
            var conf_year = confirm_date.getFullYear();
            var conf_month = confirm_date.getMonth();
            var conf_day = confirm_date.getDate();

            if($('#joinDate').val()!=""){
                $('#retireDt').val(retire_day + "/" + retire_month + "/" + retire_year);
                $('#confirmDate').val(conf_day + "/" + conf_month + "/" + conf_year);
                $('#lprDate').val(lpr_day + "/" + lpr_month + "/" + lpr_year);
            }else{
                $('#retireDt').val("");
                $('#confirmDate').val("");
                $('#lprDate').val("");
            }
        }

    });


    function dataValidation() {

        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($("#joinDate").val() == "") {
            status = false;
            $("#err_emp_join_date_id").text("Empty field found!!");
            $("#joinDate").focus();
        } else if (checkvalid_JoinDate($("#joinDate").val())) {
            status = false;
            $("#err_emp_join_date_id").text("Joining date should not be greater than the current Date!!");
            $("#joinDate").focus();

        } else if (!dateCheck.test($("#joinDate").val())) {
            status = false;
            $("#err_emp_join_date_id").text("Invalid date format!!");
            $("#joinDate").focus();

        } else if (isValidDate($("#joinDate").val()) == false) {
            status = false;
            $("#err_emp_join_date_id").text("Invalid date format!!");
            $("#joinDate").focus();

        } else {
            $("#err_emp_join_date_id").text("");
        }

        if ($("#effectDtOnSalary").val() != "") {

            if (!dateCheck.test($("#effectDtOnSalary").val())) {
                status = false;
                $("#err_effectDtOnSalaryhrm").text("Invalid date format!!");
                $("#effectDtOnSalary").focus();

            } else if (isValidDate($("#effectDtOnSalary").val()) == false) {

                status = false;
                $("#err_effectDtOnSalaryhrm").text("Invalid date format!!");
                $("#effectDtOnSalary").focus();

            } else  $("#err_effectDtOnSalaryhrm").text("");

        } else {
            $("#err_effectDtOnSalaryhrm").text("");
        }


        if ($("#pfcDedStat").val() == -1) {
            status = false;
            $("#err_pf_deduction_status_id").text("Empty field found!!");
            $("#pfcDedStat").focus();
        } else $("#err_pf_deduction_status_id").text("");

        if ($("#payLevel").val() == -1) {
            status = false;
            $("#err_emp_pay_level").text("Empty field found!!");
            $("#payLevel").focus();
        } else $("#err_emp_pay_level").text("");


        if ($("#payscId").val() == "-1") {
            status = false;
            $("#err_emp_payscId").text("Empty field found!!");
            $("#payscId").focus();
        }
        else $("#err_emp_payscId").text("");


        if ($("#lastIncreDt").val() != "") {
            if (!dateCheck.test($("#lastIncreDt").val())) {
                status = false;
                $("#err_lastincrement").text("Invalid Date format!!");
                $("#lastIncreDt").focus();

            } else if (isValidDate($("#lastIncreDt").val()) == false) {
                status = false;
                $("#err_lastincrement").text("Invalid Date format!!");
                $("#lastIncreDt").focus();


            } else $("#err_lastincrement").text("");
        } else $("#err_lastincrement").text("");


        if ($("#lastPromDt").val() != "") {
            if (!dateCheck.test($("#lastPromDt").val())) {
                status = false;
                $("#err_lastpropotion").text("Invalid Date format!!");
                $("#lastPromDt").focus();

            } else if (isValidDate($("#lastPromDt").val()) == false) {

                status = false;
                $("#err_lastpropotion").text("Invalid Date format!!");
                $("#lastPromDt").focus();

            } else $("#err_lastpropotion").text("");

        } else $("#err_lastpropotion").text("");


        if ($("#confirmDate").val() == "") {
            status = false;
            $("#err_emp_conf_date_id").text("Empty field found!!");
            $("#confirmDate").focus();
        } else {
            if (!dateCheck.test($("#confirmDate").val())) {
                status = false;
                $("#err_emp_conf_date_id").text("Invalid Date format!!");
                $("#confirmDate").focus();

            } else if (isValidDate($("#confirmDate").val()) == false) {

                status = false;
                $("#err_emp_conf_date_id").text("Invalid Date format!!");
                $("#confirmDate").focus();

            } else $("#err_emp_conf_date_id").text("");
        }

        if ($("#lprDate").val() == "") {
            status = false;
            $("#err_lprDate").text("Empty field found!!");
            $("#lprDate").focus();
        } else {
            if (!dateCheck.test($("#lprDate").val())) {
                status = false;
                $("#err_lprDate").text("Invalid Date format!!");
                $("#lprDate").focus();

            } else if (isValidDate($("#lprDate").val()) == false) {

                status = false;
                $("#err_lprDate").text("Invalid Date format!!");
                $("#lprDate").focus();

            } else $("#err_lprDate").text("");
        }

        if ($("#retireDt").val() == "") {
            status = false;
            $("#err_retireDt").text("Empty field found!!");
            $("#retireDt").focus();
        } else {
            if (!dateCheck.test($("#retireDt").val())) {
                status = false;
                $("#err_retireDt").text("Invalid Date format!!");
                $("#retireDt").focus();

            } else if (isValidDate($("#retireDt").val()) == false) {
                status = false;
                $("#err_retireDt").text("Invalid Date format!!");
                $("#retireDt").focus();

            } else $("#err_retireDt").text("");
        }

        if ($("#serviceCd").val() == -1) {
            status = false;
            $("#err_emp_service_status_id").text("Empty field found!!");
            $("#serviceCd").focus();
        } else $("#err_emp_service_status_id").text("");

        if ($("#penPayStat").val() == -1) {
            status = false;
            $("#err_pension_payment_status_id").text("Empty field found!!");
            $("#penPayStat").focus();
        } else $("#err_pension_payment_status_id").text("");

        if ($("#activityCd").val() == -1) {
            status = false;
            $("#err_emp_act_status_id").text("Empty field found!!");
            $("#activityCd").focus();
        } else $("#err_emp_act_status_id").text("");


        if ($("#employementStCd").val() == -1) {
            status = false;
            $("#err_emp_status_id").text("Empty field found!!");
            $("#employementStCd").focus();
        } else $("#err_emp_status_id").text("");


        if ($("#salPayStat").val() == -1) {
            status = false;
            $("#err_emp_salary_payment_status_id").text("Empty field found!!");
            $("#salPayStat").focus();
        } else $("#err_emp_salary_payment_status_id").text("");

        return status;
    }

    $("#job_detail_form_refresh").click(function () {
        clearform();
        $("#err_emp_salary_payment_status_id").text("");
        $("#err_emp_status_id").text("");
        $("#err_emp_act_status_id").text("");
        $("#err_pension_payment_status_id").text("");
        $("#err_emp_service_status_id").text("");
        $("#err_retireDt").text("");
        $("#err_lprDate").text("");
        $("#err_emp_conf_date_id").text("");
        $("#err_lastpropotion").text("");
        $("#err_lastincrement").text("");
        $("#err_emp_payscId").text("");
        $("#err_emp_pay_level").text("");
        $("#err_pf_deduction_status_id").text("");
        $("#err_emp_join_date_id").text("");
    });

    function clearform() {
        $('#retireDt').val("");
        $('#lprDate').val("");
        $('#confirmDate').val("");
        $('#joinDate').val("");
        $('#pfcDedStat').val('0');
        $('#salPayStat').val('00');
        $('#penPayStat').val('0');
        $('#employementStCd').val('-1');
        $('#activityCd').val('-1');
        $('#serviceCd').val('-1');
        $('#payscId').val("-1");
        $('#officeCdone').val('-1');
        $('#effectDtOnSalary').val("");
        $('#currBasic').val("");
        $('#lastIncreDt').val("");
        $('#lastPromDt').val("");
        $('#getpayscaleId').val("");

        var office = $('#payLevel');
        office.empty();
        office.append($('<option/>', {
            value: "",
            text: ""
        }));
    }

    function checkvalid_JoinDate(enteredDate) {
        var status = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate > today) {
            status = true;
        }
        return status;
    }

    function payscaleMode(employmentType, getValue) {

        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: "/hrm-admin/gethrmEmployeePayscaleCode/" + employmentType,
            dataType: 'json',
            success: function (data) {

                var office = $('#payscId');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Pay Scale Grade---"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices[0],
                        text: offices[2] + '-(' + offices[1] + ')',
                        selected: offices[0] == getValue
                    }));
                });
            },
            error: function (e) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

});
