/**
 * Created by Geetanjali Oishe on 7/18/2019.
 */

$(document).ready(function () {



    $('#transfer-info-edit-btn').hide();

    $('#search-tbl-div').hide();
    $('#transfer-info-div').hide();
    $('#trans-info-tbl-div').hide();


    $('#transfer-filterBtn').click(function (event) {
        event.preventDefault();
        var isVaild = validateTransferEmplSearch();
        if(isVaild) {
            emplSearch();
        }
    });


    function validateTransferEmplSearch() {
        $(".err_msg").text("");

        if(!$('#transfer-employee-id').val().length == 0) {
            return true;
        }

        if (!$('#transfer-employee-file-no').val().length == 0) {
            return true;
        }

        if (!$('#transfer-employee-name').val().length == 0) {
            return true;
        }

        if (!$('#transfer-department').find('option:selected').val() == -1) {
            return true;
        }

        if (!$('#transfer-designation').find('option:selected').val() == -1) {
            return true;
        }

        if ($('#transfer-pay-level').find('option:selected').val() == -1 ) {
            var parent = $('#transfer-pay-scale-grade').find('option:selected').val();
            if (parent != -1)
                $('#transfer-pay-level').after('<span class="error err_msg">Select pay level</span>');
        }

        else {
            return true;
        }

        if ($('#transfer-office').find('option:selected').val() == -1 ) {
            var parent = $('#transfer-office-category').find('option:selected').val();
            if (parent != -1)
                $('#transfer-office').after('<span class="error err_msg">Select office</span>');
        }

        else {
            return true;
        }

        return false;
    }


    function emplSearch() {

        var radioValue = $("input[name='empl_type_for_transfer']:checked").val();

        var empId = $('#transfer-employee-id').val();

        var empName = $('#transfer-employee-name').val();
        var empOfc = $('#transfer-office').val();
        var empDept = $('#transfer-department').val();
        var empDesig = $('#transfer-designation').val();
        var empFileNo = $('#transfer-employee-file-no').val();

        var payScale = $('#transfer-pay-scale-grade').val();
        var payLevel = $('#transfer-pay-level').val();

        var transStatus = -1;

        var employeeTransferSearch = {};

        employeeTransferSearch.empNmEng = empName;
        employeeTransferSearch.officeId = empOfc;
        employeeTransferSearch.employeeType = radioValue;
        employeeTransferSearch.empId = empId;

        employeeTransferSearch.payScale = payScale;
        employeeTransferSearch.payLevel = payLevel;

        employeeTransferSearch.transStatus = transStatus;

        // if (radioValue == '3') { // agentInsss
        //     employeeTransferSearch.agentId = empId;
        // }

        if (radioValue != '3') { //admin //do-dm
            employeeTransferSearch.designationId = empDesig;
            employeeTransferSearch.deptId = empDept;
            employeeTransferSearch.fileNo = empFileNo;
        }


        var tableData = $("#transfer_empl_table_tbody");

        var nameOrId = $('#transfer-employee-name').val();

        // parameter values

        $.ajax({
            contentType: 'application/json',
            url:  "search-employee-for-transfer",
            type: 'POST',
            async: false,
            data: JSON.stringify(employeeTransferSearch),
            dataType: 'json',
            success: function(response) {

                // employeeDto.setEmpId((String) obj[0]);
                // employeeDto.setHrmEmployeeId(((BigDecimal)obj[1]).longValue());
                // employeeDto.setEmpNmEng((String) obj[2]);
                // employeeDto.setOfficeId(((BigDecimal) obj[3]).longValue());
                // employeeDto.setOfficeName((String) obj[4]);
                // employeeDto.setEmployeeType(3L);
                // employeeDto.setDesignationName("Agent");



                $('#transafer_emp_table_span').text("");

                tableData.html("");
                html = "";
                $.each(response, function(idx, elem) {
                    if (idx < 10) {
                        html = "";
                        html += "<tr>";
                        html += "<td>" + elem.empId + "</td>";
                        html += "<td>" + elem.empNmEng + "</td>";
                        html += "<td>" + elem.designationName + "</td>";
                        html += "<td>" + elem.officeName + "</td>";
                        // html += "<td>" + elem.joinDt + "</td>";
                        html += "<td>" + '<button type="button" class="btn btn-info empl_transfer_select_button" value="Select" button_id="' + elem.hrmEmployeeId + '"> ' +
                            '<i class="fa fa-check"></i></button>' + "</td>";
                        html += "</tr>";
                        tableData.append(html);
                        $('#transfer_empl_table_div').show();
                    }
                    else {
                        $('#transafer_emp_table_span').text("Result contains more than 10 data. Kindly Refine search parameters.");
                    }
                });

                if (response.length == 0) {
                    $('#transafer_emp_table_span').text('Search result empty.');
                }


                $('#search-tbl-div').show();

            },
            error: function(xhr, status, error) {

            }
        });

    }



    //agentInsss process
    // $('#empl_type_agent').click(function () {
    //     $('#department-hide').css("visibility", "hidden");
    //     $('#designation-hide').css("visibility", "hidden");
    //     $('#payScale-hide').css("visibility", "hidden");
    //     $('#fileNo-hide').css('visibility', 'hidden');
    // });

    var confirmEmpTransferDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        var orderNo = $('#transfer-order-no').val();
                        var orderDate = $('#transfer-order-date').val();
                        var officeCatId = $('#transferred-office-category').val();
                        var officeId = $('#transferred-office').val();
                        var desigId = $('#transferred-designation').val();
                        var deptId = $('#transferred-department').val();
                        var joiningDt = $('#transfer-joining-dt').val();
                        var remarks = $('#transfer-remarks').val();
                        var empId = $('#hidden-emp-id').val() ;
                        var transferId = $('#hidden-transfer-id').val();

                        var employeeTransfer = {};


                        employeeTransfer.id = transferId;
                        employeeTransfer.hrmEmployeeId = empId;
                        // employeeTransfer.organogId = ;
                        employeeTransfer.orderNo = orderNo;
                        employeeTransfer.orderDate = orderDate;
                        employeeTransfer.officeId = officeId;
                        employeeTransfer.desigId = desigId;
                        employeeTransfer.deptId = deptId;
                        employeeTransfer.joiningDate = joiningDt;
                        employeeTransfer.remarks = remarks;


                        $.ajax({
                            contentType: 'application/json',
                            url: "save-employee-transfer-info",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(employeeTransfer),
                            dataType: 'json',
                            success: function (response) {
                                clearForm();
                                updateTransferTable(response);
                                showAlertByType("Successfully added", "S");

                            },
                            error: function (xhr, status, error) {
                                showAlertByType("Failed to add", "F");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };

    function updateTransferTable(hrmEmployeeId) {

        var tableData =  $('#transferred_empl_table_tbody');

        $.ajax({
            contentType: 'application/json',
            url:  "get-employee-transfer-table",
            type: 'POST',
            async: false,
            data: JSON.stringify(hrmEmployeeId),
            dataType: 'json',
            success: function(response) {

                if (response.length > 0) {
                    $('#trans-info-tbl-div').show();
                }

                tableData.html("");
                html = "";
                $.each(response, function(idx, elem) {

                    if (idx < 10) {
                        html = "";
                        html += "<tr>";
                        html += "<td>" + elem.orderNo  + "</td>";
                        html += "<td>" + elem.employeeName + "</td>";
                        html += "<td>" + elem.officeName + "</td>";
                        html += "<td>" + elem.deptName + "</td>";
                        html += "<td>" + elem.designationName + "</td>";
                        html += "<td>" + elem.joiningDate + "</td>";
                        html += "<td>" + elem.orderDate  + "</td>";
                        html += "<td>" + elem.status + "</td>";
                        // html += "<td>" + elem.joinDt + "</td>";

                        if (elem.status == "Pending") {
                            html += "<td>" + '<button type="button" class="btn btn-success empl_trans_history_edit" value="Select" button_id="' + elem.id + '"> ' +
                                '<i class="fa fa-pencil"></i></button>' + "</td>";
                        }
                        else {

                            html += "<td>" + '<button type="button" class="btn btn-success empl_trans_history_edit" value="Select" button_id="' + elem.id + '"disabled> ' +
                                '<i class="fa fa-pencil"></i></button>' + "</td>";

                        }



                        html += "</tr>";
                        tableData.append(html);
                        $('#transfer_empl_table_div').show();
                    }
                    else {
                        $('#transafer_emp_table_span').text("Result contains more than 10 data. Kindly Refine search parameters.");
                    }
                });

                if (response.length == 0) {
                    $('#transafer_emp_table_span').text('Search result empty.');
                }
            },
            error: function(xhr, status, error) {

            }
        });

    }

    $('#transfer-refreshButton').click(function (event) {
        event.preventDefault();


        $('#transfer-employee-id').val("");
        $('#transfer-employee-file-no').val("");
        $('#transfer-employee-name').val("");
        $('#transfer-department').val(-1);
        $('#transfer-designation').val(-1);
        $('#transfer-pay-level').val(-1);
        $('#transfer-pay-scale-grade').val(-1);
        $('#transfer-office').val(-1)
        $('#transfer-office-category').val(-1);
        // $('#hidden-emp-id').val(0);

        resetModal();

        // $('#modal-transfer-empl').modal();

    });

    
    function resetModal() {
        $('#transfer-employee-id-modal').val("");
        $('#transfer-employee-office-modal').val("");
        $('#transfer-employee-dept-modal').val("");
        $('#transfer-employee-joining-dt-modal').val("");
        $('#transfer-employee-name-modal').val("");
        $('#transfer-employee-desig-modal').val("");
        $('#transfer-employee-file-no-modal').val("");
        $('#transfer-employee-pay-scale-modal').val("");
    }
    
    $('#transfer-modal-close-btn').click(function (event) {
        $('#transfer-info-div').show();

        $('#transferred-emp-id').val($('#transfer-employee-id-modal').val());
        $('#transferred-emp-nm').val($('#transfer-employee-name-modal').val());

        updateTransferTable($('#hidden-emp-id').val());


        $('html, body').animate({
            scrollTop: $('#transfer-info-div').offset().top
        }, 1000)



    });

    $(document).on("click", ".empl_transfer_select_button", function (event) {
        getEmpDetail(this);
        $('#modal-transfer-empl').modal();

    });

    $(document).on("click", ".empl_trans_history_edit", function (event) {
        getTransferHistoryDtl(this);
    });



    function getEmpDetail(identifier) {
        var row_id = $(identifier).attr("button_id");
        console.log("row_id: " + row_id);

        $.ajax({
            contentType: 'application/json',
            url: "get-empl-details-for-trans",
            type: 'POST',
            async: false,
            data: JSON.stringify(row_id),
            dataType: 'json',
            success: function (response) {
                $('#transfer-employee-id-modal').val(response.empId);
                $('#transfer-employee-office-modal').val(response.officeName);
                $('#transfer-employee-dept-modal').val(response.deptName);
                $('#transfer-employee-joining-dt-modal').val(response.joinDt);
                $('#transfer-employee-name-modal').val(response.empNmEng);
                $('#transfer-employee-desig-modal').val(response.designationName);
                $('#transfer-employee-file-no-modal').val(response.fileNo);
                $('#transfer-employee-pay-scale-modal').val(response.payScale);
                // $('#nb_pro_save_empl').val(row_id);


                $('#hidden-emp-id').val(response.hrmEmployeeId);
            },
            error: function (xhr, status, error) {
            }
        });
    }

    function getTransferHistoryDtl (identifier) {
        var row_id = $(identifier).attr("button_id");
        console.log("row_id: " + row_id);

        $.ajax({
            contentType: 'application/json',
            url: "get-transfer-details-for-edit",
            type: 'POST',
            async: false,
            data: JSON.stringify(row_id),
            dataType: 'json',
            success: function (response) {
                $('#transfer-order-no').val(response.orderNo);
                $('#transfer-order-date').val(response.orderDate);
                $('#transferred-office-category').val(response.ofcCatId);
                updateOfcByCategory(response.officeId);
                $('#transferred-designation').val(response.desigId);
                $('#transferred-department').val(response.deptId);
                $('#transfer-joining-dt').val(response.joiningDate);
                $('#transfer-remarks').val(response.remarks);


                // $('#transferred-office').val(response.officeId);

                $('#hidden-transfer-id').val(response.id);

                $('#hidden-emp-id').val(response.hrmEmployeeId);

                $('#transfer-info-edit-btn').show();
                $('#transfer-info-save-btn').hide();

            },
            error: function (xhr, status, error) {
            }
        });
    }
    
    
    $('#transfer-info-save-btn').click(function (event) {
            if (checkTransferredValidation()) {
                confirmEmpTransferDialog("Are you sure to save transfer info?");
            }
        });

    $('#transfer-info-edit-btn').click(function (event) {
        if (checkTransferredValidation()) {
            confirmEmpTransferDialog("Are you sure to edit transfer info?");
        }
    });
    
     function checkTransferredValidation() {
         var isValid = true;
         $(".err_msg").text("");


         $('#transfer-info-div input').each(function () {
             if($(this).val().length == 0) {
                 $(this).after('<span class="error err_msg">This field is mandatory</span>');
             }
         });

         $('#transfer-info-div select').each(function () {
             if($(this).find('option:selected').val() == -1) {
                 $(this).after('<span class="error err_msg">Select option</span>');
             }
         });

         ///// span with error class
         $('#transfer-info-div span').each(function () {
             if($(this).hasClass("err_msg")) {
                 if ($(this).text() != "") {
                     isValid = false;
                 }
             }
         });


         return isValid;
     }

     function updateOfcByCategory(officeId) {
         var catId = $("#transferred-office-category option:selected").val();

         $.get("/hrm-admin/get-office-by-category?offCatgId=" + catId,
             function (data, status) {
                 var office = $('#transferred-office');
                 office.empty();
                 office.append($('<option/>', {
                     value: "-1",
                     text: "---Select Office---"
                 }));
                 $.each(data, function (index, offices) {
                     office.append($('<option/>', {
                         value: offices.id,
                         text: offices.officeName
                     }));
                 });

                 if (officeId != undefined) {
                     $('#transferred-office').val(officeId);
                 }
             });
     }


    $(document).on("change", "#transfer-office-category", function (e) {
        var catId = $("#transfer-office-category option:selected").val();

        $.get("/hrm-admin/get-office-by-category?offCatgId=" + catId,
            function (data, status) {
                var office = $('#transfer-office');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Office---"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices.id,
                        text: offices.officeName
                    }));
                });

                // if(officeId != undefined) {
                //     $('#transferred-office').val(officeId);
                // }


            });

    });


    $(document).on("change", "#transfer-pay-scale-grade", function (e) {

        var scaleId = $("#transfer-pay-scale-grade option:selected").val();



        $.getJSON('/hrm-admin/get-level-by-scale', {
                scaleId: scaleId,
                ajax: true
            },
            function (data) {
                var payLevel = $('#transfer-pay-level');
                payLevel.empty();
                payLevel.append($('<option/>', {
                    value: "-1",
                    text: "---Select Pay Level---"
                }));
                $.each(data, function (index, offices) {
                    payLevel.append($('<option/>', {
                        value: payLevel.id,
                        text: offices.payLevel
                    }));
                });
            });
    });


    $(document).on("change", "#transferred-office-category", function (e) {
       updateOfcByCategory();
    });


    function clearForm(){
        clearFields();
        $('#transfer-info-edit-btn').hide();
        $('#transfer-info-save-btn').show();
        $('#transfer-info-refresh-btn').show();
        $("#hidden-transfer-id").val('-1');
    };

    function clearFields(){
        $(".err_msg").text("");


        $("#transfer-info-div input[type='text']").each(function () {
            $(this).val("");
        });

        $('#transfer-info-div select').each(function () {
            $(this).val(-1);
        });

        $("#transfer-info-div input.hasDatepicker").each(function () {
            $(this).val("");
        });

        $('#transfer-remarks').text("");

    };


    $(document).on("click", "#transfer-info-refresh-btn", function () {
        clearForm();
    });



});