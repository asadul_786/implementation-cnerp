/**
 * Created by Administrator on 4/2/2019.
 */

$(document).ready(function () {
    $('#btnPrnt').click(function () {
        var printContents = $('#money-receipt').html();
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    });
});