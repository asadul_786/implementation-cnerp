$(document).ready(function () {

    getAccHeadLedger();

    $("#addNewThrdBtn").click(function () {

            var level1 = $('#level1').val();
            var level2 = $('#level2').val();
            var level3 = $('#level3').val();
            var level4 = $('#level4').val();

            if(level1 != '' && level2 != '' && level3 != '' && level4 != ''){

                var deCr = $('input[name="deCr"]:checked').val();
                var level4 = $('#level4').val();
                var newThread = $('#newThread').val();
                var oldGlAcNo = $('#oldGlAcNo').val();
                var level4Val = $('#level4Val').val();
                var postFlagVal = $('input[name="postFlagVal"]:checked').val();
                var glNature = $('#glNature').val();
                var status = $('#status').val();

                //if(deCr != undefined && level4 != '' && newThread != '' && oldGlAcNo != '' && postFlagVal != undefined && glNature != ''){

                    var chartOfAcc = {};

                    chartOfAcc.deCr = deCr;
                    chartOfAcc.level4 = level4;
                    chartOfAcc.newThread = newThread;
                    chartOfAcc.oldGlAcNo = oldGlAcNo;
                    chartOfAcc.level4Val = level4Val;
                    chartOfAcc.postFlagVal = postFlagVal;
                    chartOfAcc.glNature = glNature;
                    chartOfAcc.status = status;

                    $.confirm({
                        title: 'Message',
                        content: 'Are You Sure To Add These Records',
                        buttons: {
                            ok: function () {

                                $.ajax({
                                    type: 'POST',
                                    contentType: 'application/json',
                                    url: "/genAccounting/saveLedgerChartAcc",
                                    data: JSON.stringify(chartOfAcc),
                                    dataType: 'json',
                                    success: function (data) {

                                        if(data == true){
                                            showAlert("New Thread Added Successfully");
                                        }else{
                                            showAlert("Duplicate Entry Found!!!");
                                        }
                                    },
                                    error: function (e) {
                                        showAlert("Duplicate Entry Found!!!");
                                    }
                                });
                            },
                            cancel: function () {

                            },
                        }
                    });
               /* }else{
                    $.confirm({
                        title: 'Message',
                        content: 'Please Fill Up All Fields',
                        buttons: {
                            ok: function () {
                            },
                        }
                    });
                }*/
            }else{
                $.confirm({
                    title: 'Message',
                    content: 'Please Select All Levels',
                    buttons: {
                        ok: function () {
                        },
                    }
                });
            }
    });
});
function getFourthLevelLeafNodes(glAcNo){

    var table = $("#dataTable tbody");

    var json = {
        "glAcNo": glAcNo
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/genAccounting/getFourthLevelNodeList/"+glAcNo,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            table.empty();

            var glAcNo = "";
            var glAcNm = "";
            var oldGlacNo = "";
            var drCr = "";
            var controlAc = "";
            var glNature = "";

            $.each(data, function (i, l) {

                glAcNo = l[0];
                glAcNm = l[1];
                oldGlacNo = l[2]
                drCr = l[3];
                controlAc = l[4];
                glNature = l[5];

                table.append("<tr><td>"+'<span>'+glAcNo+'</span>'+"</td>" +
                    "<td>"+glAcNm+"</td>" +
                    "<td>"+oldGlacNo+"</td>"+
                    "<td>" + drCr + "</td>" +
                    "<td>" + controlAc + "</td>" +
                    "<td>" + glNature + "</td>" +
                    "<td>" +
                    '<button id="delete" type="submit" value='+glAcNo+' onclick="return deleteFourthLevelLeafNode(this,this.value);" class="btn btn-danger">'+
                    "Delete"
                    +'</button>' + "</td></tr>");
            });
            $("#dataTable").DataTable();
        },
        error: function (e) {
        }
    });

}
function deleteFourthLevelLeafNode(obj,glAcNo){

    var json = {
        "glAcNo": glAcNo,
    };

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Delete These Record',
        buttons: {
            ok: function () {

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/genAccounting/deleteFourthLevelLeafNode/"+glAcNo,
                    data: JSON.stringify(json),
                    dataType: 'json',
                    success: function (data) {
                        //showAlert("Successfully Deleted.");
                        if(data == 1){
                            $(obj).parent().parent().remove();
                            $.confirm({
                                title: 'Message',
                                content: 'Deleted Successfully',
                                buttons: {
                                    ok: function () {
                                    },
                                }
                            });
                        }else{
                            $.confirm({
                                title: 'Message',
                                content: 'Child Record Found!!',
                                buttons: {
                                    ok: function () {
                                    },
                                    cancel: function () {
                                    },
                                }
                            });
                        }
                    },
                    error: function (e) {
                        showAlert("Child Record Found!!");
                    }
                });
            },
            cancel: function () {

            },
        }
    });

}
function getAccHeadLedger(){

    var $select = $('#level1');

    var json = {
        "param": ""
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/genAccounting/getHeadChartAccount",
        data: JSON.stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var glAcNo = "";
            var glNm = "";

            $('.selectpicker').selectpicker({
                style: ''
            });
            $select.html('');
            $select.append('<option value="' + '' + '">' + 'Select Head' + '</option>');
            $.each(data, function (i, l) {
                glAcNo = l[0];
                glNm = l[1];
                $select.append('<option value="' + glAcNo + '">' + glNm + '</option>');
            });
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (e) {
        }
    });
}

function getSecLevelChartAcc(){

    var $select = $('#level2');

    var json = {
        "param": ""
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/genAccounting/getSecLevelChartAcc",
        data: JSON.stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var glAcNo = "";
            var glNm = "";

            $('.selectpicker').selectpicker({
                style: ''
            });
            $select.html('');
            $select.append('<option value="' + '' + '">' + 'Select Category' + '</option>');
            $.each(data, function (i, l) {
                glAcNo = l[0];
                glNm = l[1];
                $select.append('<option value="' + glAcNo + '">' + glNm + '</option>');
            });
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (e) {
        }
    });
}

function getThirdLevelChartAcc(glAcNo){

    var $select = $('#level3');

    var json = {
        "glAcNo": glAcNo
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/genAccounting/getThirdLevelChartAcc/"+glAcNo,
        data: JSON.stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var glAcNo = "";
            var glNm = "";

            $('.selectpicker').selectpicker({
                style: ''
            });
            $select.html('');
            $select.append('<option value="' + '' + '">' + 'Select Category' + '</option>');
            $.each(data, function (i, l) {
                glAcNo = l[0];
                glNm = l[1];
                $select.append('<option value="' + glAcNo + '">' + glNm + '</option>');
            });
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (e) {
        }
    });
}
function getFourthLevelChartAcc(glAcNo){

    var $select = $('#level4');

    var json = {
        "glAcNo": glAcNo
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/genAccounting/getFourthLevelChartAcc/"+glAcNo,
        data: JSON.stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var glAcNo = "";
            var glNm = "";

            $('.selectpicker').selectpicker({
                style: ''
            });
            $select.html('');
            $select.append('<option value="' + '' + '">' + 'Select Category' + '</option>');
            $.each(data, function (i, l) {
                glAcNo = l[0];
                glNm = l[1];
                $select.append('<option value="' + glAcNo + '">' + glNm + '</option>');
            });
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (e) {
        }
    });
}

/* var days = 10;
       var date = $('#trxTotalAmount').val();
       var datearray = date.split("/");
       var newdateR = datearray[1] + '/' + datearray[0] + '/' + datearray[2];

       var newdate = new Date(newdateR);

       newdate.setDate(newdate.getDate() + days);

       var dd = newdate.getDate();
       var mm = newdate.getMonth() + 1;
       var y = newdate.getFullYear();

       var someFormattedDate = dd + '/' + mm + '/' + y;

       alert(someFormattedDate)*/

