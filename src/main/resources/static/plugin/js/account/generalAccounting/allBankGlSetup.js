$(document).ready(function () {

    $('#rowkey').val(0);

    getBankGlSetupList();

    $("#addBtn").click(function () {

                var office = $('#office').val();
                var glCatCd = $('#glCatCd').val();
                var branch = $('#branch').val();
                var accTp = $('#accTp').val();
                var accNo = $('#accNo').val();
                var accNature = $('input[name="accNature"]:checked').val();
                var accFlag = $('input[name="accFlag"]:checked').val();
                var costCenter = $('#costCenter').val();
                var brGlAcNo = $('#brGlAcNo').val();
                var rowkey = $('#rowkey').val();

        /*rowkey = 10;
        office = "0075";
        glCatCd = 23;
        brGlAcNo = "150100007";
        branch = "008";
        accTp = "CD";
        accNo = "3444";
        costCenter = 4;
        accFlag = 1;
        accNature = "CHEQUE";*/



                    var bankGlSetup = {};

        bankGlSetup.rowkey = rowkey;
        bankGlSetup.office = office;
        bankGlSetup.glCatCd = glCatCd;
        bankGlSetup.branch = branch;
        bankGlSetup.accTp = accTp;
        bankGlSetup.accNo = accNo;
        bankGlSetup.accNature = accNature;
        bankGlSetup.accFlag = accFlag;
        bankGlSetup.costCenter = costCenter;
        bankGlSetup.brGlAcNo = brGlAcNo;

        //alert(JSON.stringify(bankGlSetup));

                    $.confirm({
                        title: 'Message',
                        content: 'Are You Sure To Add These Records',
                        buttons: {
                            ok: function () {

                                $.ajax({
                                    type: 'POST',
                                    contentType: 'application/json',
                                    url: "/genAccounting/saveBankGlSetup",
                                    data: JSON.stringify(bankGlSetup),
                                    dataType: 'json',
                                    success: function (data) {

                                        if(data == true){
                                            showAlert("Bank Gl Setup Added Successfully");
                                            getBankGlSetupList();
                                        }else{
                                            showAlert("Duplicate Entry Found!!!");
                                        }
                                    },
                                    error: function (e) {
                                        showAlert("Duplicate Entry Found!!!");
                                    }
                                });
                            },
                            cancel: function () {

                            },
                        }
                    });
    });
});


function getBankGlSetupList(){

    var table = $("#dataTable tbody");

    var json = {
        "param": ''
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/genAccounting/getBankGlSetupList",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            table.empty();

            var rowkey = "";
            var office = "";
            var officeCd = "";
            var glCatCd = "";
            var brGlAcNo = "";
            var branch = "";
            var brCd = "";
            var accTp = "";
            var accNo = "";
            var costCenter = "";
            var accNature = "";
            var aFlag = "";

            $.each(data, function (i, l) {

                rowkey = l[0];
                office = l[1];
                officeCd = l[2]
                glCatCd = l[3];
                brGlAcNo = l[4];
                branch = l[5];
                if(branch == null){
                    branch = "";
                }
                brCd = l[6];
                accTp = l[7];
                accNo = l[8]
                costCenter = l[9];
                accNature = l[10];
                aFlag = l[11];

                table.append("<tr><td>"+'<span>'+office+'</span>'+"</td>" +
                    "<td>"+glCatCd+"</td>" +
                    "<td>"+brGlAcNo+"</td>"+
                    "<td>" + branch + "</td>" +
                    "<td>" + accTp + "</td>" +
                    "<td>" + accNo + "</td>" +
                    "<td>" + costCenter + "</td>" +
                    "<td>" + accNature + "</td>" +
                    "<td>" + aFlag + "</td>" +
                    "<td>" +
                    '<button id="edit" type="submit" value='+rowkey+' class="btn fa fa-edit" onclick="setValue(this.value);" style="text-align:center;vertical-align: middle;font-size:20px;">'+'</button>' + "</td>" +
                    "<td>" +
                    '<button id="delete" type="submit" value='+rowkey+' class="btn fa fa-trash-o" onclick="deleteBankGlSetup(this,this.value);" style="text-align:center;vertical-align: middle;font-size:20px;color:red;">'+'</button>' + "</td>" +
                    "</tr>");
            });
            $("#dataTable").DataTable();
        },
        error: function (e) {
        }
    });

}

function deleteBankGlSetup(obj,rowkey){


   var json = {
        "rowkey": rowkey,
    };

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Delete These Record',
        buttons: {
            ok: function () {

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/genAccounting/deleteBankGlSetup/"+rowkey,
                    data: JSON.stringify(json),
                    dataType: 'json',
                    success: function (data) {

                        if(data == 1){
                            $(obj).parent().parent().remove();
                            $.confirm({
                                title: 'Message',
                                content: 'Deleted Successfully',
                                buttons: {
                                    ok: function () {
                                    },
                                }
                            });
                        }else{
                            $.confirm({
                                title: 'Message',
                                content: 'Child Record Found!!',
                                buttons: {
                                    ok: function () {
                                    },
                                    cancel: function () {
                                    },
                                }
                            });
                        }
                    },
                    error: function (e) {
                        showAlert("Child Record Found!!");
                    }
                });
            },
            cancel: function () {

            },
        }
    });

}

function setValue(rowkey){

    var json = {
        "rowkey": rowkey,
    };


               $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/genAccounting/getBankGlSetupInfo/"+rowkey,
                    data: JSON.stringify(json),
                    dataType: 'json',
                    success: function (data) {

                        var rowkey = "";
                        var office = "";
                        var officeCd = "";
                        var glCatCd = "";
                        var brGlAcNo = "";
                        var branch = "";
                        var brCd = "";
                        var accTp = "";
                        var accNo = "";
                        var costCenter = "";
                        var accNature = "";
                        var aFlag = "";

                        $.each(data, function (i, l) {

                            rowkey = l[0];
                            office = l[1];
                            officeCd = l[2]
                            glCatCd = l[3];
                            brGlAcNo = l[4];
                            branch = l[5];
                            if(branch == null){
                                branch = "";
                            }
                            brCd = l[6];
                            accTp = l[7];
                            accNo = l[8]
                            costCenter = l[9];
                            accNature = l[10];
                            aFlag = l[11];

                        });
                        $('#rowkey').val('');
                        $('#rowkey').val(rowkey);
                    },
                    error: function (e) {
                    }
                });

}