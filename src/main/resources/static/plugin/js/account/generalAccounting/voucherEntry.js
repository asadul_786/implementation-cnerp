function getVoucherTypeList(){

    var json = {
        "param": ""
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/genAccounting/getVoucherTypeList",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#voucherType').empty();
            $('#voucherType').append('<option value="0">' + "Select Voucher Type" + '</option>');
            $.each(data, function(key, value) {
                $('#voucherType').append('<option value="' + key + '">' + value + '</option>');
            });
        },
        error: function (e) {
        }
    });
}

function getVoucherNo(voucherCd){

    if(voucherCd == '0'){
        $('#voucherNo').val('');
    }else{
        var json = {
            "voucherCd": voucherCd
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/genAccounting/getVoucherNo/"+voucherCd,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                $('#voucherNo').val('');
                $('#voucherNo').val(data[0]);
            },
            error: function (e) {
            }
        });
    }
}
function getVoucherList(voucherNo){

    var table = $("#dataTable tbody");

        var json = {
            "voucherNo": voucherNo
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/genAccounting/getVoucherList/"+voucherNo,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                table.empty();
                var voucherNo = "";
                var voucherDate = "";
                var glacNo = "";
                var creditAmnt = "";
                var debAmnt = "";
                var totalCreAmnt = 0;
                var totalDebAmnt = 0;
                var slNo = "";
                $('#total').val('');
                $('#trxTotalAmount').val('');

                $.each(data, function (i, l) {
                    voucherNo = l[0];
                    voucherDate = l[1];
                    voucherDate = getDateShow(voucherDate);
                    glacNo = l[2];
                    creditAmnt = l[3];
                    debAmnt = l[4];
                    slNo = l[5];
                   /* if(creditAmnt == 0){
                        amnt = debAmnt;
                    }else{
                        amnt = creditAmnt;
                    }*/
                    totalCreAmnt = parseFloat(totalCreAmnt) + parseFloat(creditAmnt);
                    totalDebAmnt = parseFloat(totalDebAmnt) + parseFloat(debAmnt);
                    table.append("<tr><td>"+'<span>'+slNo+'</span>'+"</td>" +
                        "<td>"+voucherNo+"</td>" +
                        "<td>"+voucherDate+"</td>"+
                        "<td>" + glacNo + "</td>" +
                        "<td>" + voucherNo + "</td>" +
                        "<td>" + voucherNo + "</td>" +
                        "<td>" + creditAmnt + "</td>" +
                        "<td>" + debAmnt + "</td>" +
                        "<td>" +
                        '<button id="delete" type="submit" value='+slNo+' onclick="return deleteVoucherNo(this,this.value);" class="btn btn-danger">'+
                        "Delete"
                        +'</button>' + "</td></tr>");
                });
                $('#totalCreAmnt').val(totalCreAmnt);
                $('#totalDebAmnt').val(totalDebAmnt);
                $("#dataTable").DataTable();
            },
            error: function (e) {
            }
        });
}
function deleteVoucherNo(obj,slNo){

    var voucherNo = $('#voucherNo').val();
    var slNoStr = slNo;
    var valStr;

    valStr = voucherNo.concat(","+slNoStr);

    var json = {
        "valStr": valStr,
    };

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Delete These Record',
        buttons: {
            ok: function () {

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/genAccounting/deleteVoucherNo/"+valStr,
                    data: JSON.stringify(json),
                    dataType: 'json',
                    success: function (data) {
                        //showAlert("Successfully Deleted.");
                        if(data == 1){
                            $(obj).parent().parent().remove();
                            $.confirm({
                                title: 'Message',
                                content: 'Deleted Successfully',
                                buttons: {
                                    ok: function () {
                                    },
                                }
                            });
                        }else{
                            $.confirm({
                                title: 'Message',
                                content: 'Child Record Found!!',
                                buttons: {
                                    ok: function () {
                                    },
                                    cancel: function () {
                                    },
                                }
                            });
                        }
                    },
                    error: function (e) {
                        showAlert("Child Record Found!!");
                    }
                });
            },
            cancel: function () {

            },
        }
    });

}
function getTrxDetailHead(controlAcc){


        var json = {
            "param": controlAcc
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/genAccounting/getTrxDetailHead/"+controlAcc,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                $('#trxDtlHead').empty();
                $('#trxDtlHead').append('<option value="0">' + "Select Trx. Detail Head" + '</option>');
                $.each(data, function(key, value) {
                    $('#trxDtlHead').append('<option value="' + key + '">' + value + '</option>');
                });
            },
            error: function (e) {
            }
        });
}

function getTrxAccHead(){

    var genAccOfficeId = $('#genAccOfficeId').val();
    var trxAccHead = $('#trxAccHead').val();
    var trxMode = $('input[name="trxMode"]:checked').val();
    //var $select = $('#accCategory');

    if(trxMode == 'CS'){


        var json = {
            "param": ""
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/genAccounting/getCashTrxAccHead",
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                $('#trxAccHead').empty();
                $('#trxAccHead').append('<option value="0">' + "Select Trx. Account Head" + '</option>');
                $.each(data, function(key, value) {
                    $('#trxAccHead').append('<option value="' + key + '">' + value + '</option>');
                });
            },
            error: function (e) {
            }
        });
    }else{

        if(genAccOfficeId != '0'){
            if(trxMode === 'CH'){
                var json = {
                    "genAccOfficeId": genAccOfficeId
                };


                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: "/genAccounting/getBankTrxAccHead/"+genAccOfficeId,
                    data: JSON
                        .stringify(json),
                    dataType: 'json',
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        $('#trxAccHead').empty();
                        $('#trxAccHead').append('<option value="0">' + "Select Trx. Account Head" + '</option>');
                        $.each(data, function(key, value) {
                            $('#trxAccHead').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (e) {
                    }
                });
            }
        }else{
            //$select.html('');
            $.confirm({
                title: 'Message',
                content: 'Please Select Office First',
                buttons: {
                    ok: function () {
                    },
                }
            });
        }
    }
}

$(document).ready(function () {

    $("#chqNo").prop('disabled', true);
    $("#chqDate").prop('disabled', true);

    $('input[name="trxMode"]').click(function(){
        var trxMode = $('input[name="trxMode"]:checked').val();
        if(trxMode == 'CS'){
            $("#chqNo").prop('disabled', true);
            $("#chqDate").prop('disabled', true);
        }
        if(trxMode == 'CH'){
            var genAccOfficeId = $('#genAccOfficeId').val();
            $("#chqNo").prop('disabled', false);
            $("#chqDate").prop('disabled', false);
        }

    });

    $("#addBtn").prop('disabled', true);
    $("#submitBtn").prop('disabled', true);
    $("#delBtn").prop('disabled', true);
    $("#saveBtn").prop('disabled', false);
    $("#refreshBtn").prop('disabled', false);
    $("#voucherType").prop('disabled', false);
    $("#refBtn").prop('disabled', true);


    //$('#voEntryFormSecond').hide();

    $("#transactionDate").datepicker({ dateFormat: "dd/mm/yy"}).datepicker("setDate", new Date());
    getVoucherTypeList();

    $("#saveBtn").click(function () {

        //$('#voEntryFormSecond').show();

        var flag = dataValidation();

        if (flag == true) {

            var trxType = $('input[name="trxType"]:checked').val();
            var transactionDate = $('#transactionDate').val();
            var voucherType = $('#voucherType').val();
            var voucherNo = $('#voucherNo').val();
            var officeId = $('#genAccOfficeId').val();
            var docRef = $('#docRef').val();
            var narration = $('#narration').val();
            //var chqNo = $('#chqNo').val();
            //var chqDate = $('#chqDate').val();

            var voucherEntry = {};

            if(transactionDate){
                transactionDate = transactionDate.split("/").reverse().join("/");
                transactionDate = getDate(transactionDate);
            }

            voucherEntry.transactionDate = transactionDate;
            voucherEntry.voucherType = voucherType;
            voucherEntry.voucherNo = voucherNo;
            voucherEntry.officeId = officeId;
            voucherEntry.docRef = docRef;
            voucherEntry.narration = narration;
            //voucherEntry.chqNo = chqNo;
            /*if(chqDate){
                chqDate = chqDate.split("/").reverse().join("/");
                chqDate = getDate(chqDate);
            }
            voucherEntry.chqDate = chqDate;*/

            $.confirm({
                title: 'Message',
                content: 'Are You Sure To Add These Records',
                buttons: {
                    ok: function () {

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/genAccounting/saveVoucherEntryFirstPart",
                data: JSON.stringify(voucherEntry),
                dataType: 'json',
                success: function (data) {
                    if(trxType == '1'){
                        $('#crDrSecPart').val('');
                        $('#crDrSecPart').val("credit");
                    }
                    if(trxType == '2'){
                        $('#crDrSecPart').val('');
                        $('#crDrSecPart').val("debit");
                    }
                    showAlert("Successfully added.");
                    $("#addBtn").prop('disabled', false);
                    $("#submitBtn").prop('disabled', false);
                    $("#delBtn").prop('disabled', false);
                    $("#saveBtn").prop('disabled', true);
                    $("#refreshBtn").prop('disabled', true);
                    $("#voucherType").prop('disabled', true);
                    $("#refBtn").prop('disabled', false);
                },
                error: function (e) {
                    showAlert("Duplicate Entry Found!!!");
                }
               });
                    },
                    cancel: function () {

                    },
                }
            });
        }
    });

    $("#addBtn").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var controllAccHead = $('#controllAccHead').val();
            var voucherNo = $('#voucherNo').val();
            var trxDtlHead = $('#trxDtlHead').val();
            var particulars = $('#particulars').val();
            var trxAmount = $('#trxAmount').val();
            var crDrSecPart = $('#crDrSecPart').val();
            var chqNo = $('#chqNo').val();
            var chqDate = $('#chqDate').val();
            var genAccOfficeId = $('#genAccOfficeId').val();
            var chqTp = $('input[name="trxMode"]:checked').val();
            var transactionDate = $('#transactionDate').val();

            var voucherEntrySecPart = {};

            voucherEntrySecPart.conAccHead = controllAccHead;
            voucherEntrySecPart.voucherNo = voucherNo;
            voucherEntrySecPart.trxDtlHead = trxDtlHead;
            voucherEntrySecPart.particulars = particulars;
            voucherEntrySecPart.crDb = crDrSecPart;
            voucherEntrySecPart.trxAmount = trxAmount;
            voucherEntrySecPart.chqNo = chqNo;
            if(chqDate){
                chqDate = chqDate.split("/").reverse().join("/");
                chqDate = getDate(chqDate);
            }
            voucherEntrySecPart.chqDate = chqDate;
            voucherEntrySecPart.genAccOfficeId = genAccOfficeId;
            voucherEntrySecPart.chqTp = chqTp;
            if(transactionDate){
                transactionDate = transactionDate.split("/").reverse().join("/");
                transactionDate = getDate(transactionDate);
                voucherEntrySecPart.transactionDate = transactionDate;
            }

              $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/genAccounting/saveVoucherEntrySecPart",
                data: JSON.stringify(voucherEntrySecPart),
                dataType: 'json',
                success: function (data) {
                    if(data == true){
                        $.confirm({
                            title: 'Message',
                            content: 'Added Successfully',
                            buttons: {
                                ok: function () {
                                },
                            }
                        });
                        getVoucherList(voucherNo);
                    }else{
                        $.confirm({
                            title: 'Message',
                            content: 'Child Record Found!!',
                            buttons: {
                                ok: function () {
                                },
                            }
                        });
                    }
                },
                error: function (e) {
                    $.confirm({
                        title: 'Message',
                        content: 'Child Record Found!!',
                        buttons: {
                            ok: function () {
                            },
                        }
                    });
                }
            });
        }
    });
    $("#submitBtn").click(function () {


            //var controllAccHead = $('#controllAccHead').val();
            var voucherNo = $('#voucherNo').val();
            //var particulars = $('#particulars').val();
            //var trxAmount = $('#trxAmount').val();
            //var chqNo = $('#chqNo').val();
            //var chqDate = $('#chqDate').val();
            var trxAccHead = $('#trxAccHead').val();

            var voucherEntrySubmission = {};

            //voucherEntrySubmission.conAccHead = controllAccHead;
            voucherEntrySubmission.voucherNo = voucherNo;
            voucherEntrySubmission.trxAccHead = trxAccHead;
            //voucherEntrySubmission.particulars = particulars;
            //voucherEntrySubmission.chqNo = chqNo;
            //voucherEntrySubmission.trxAmount = trxAmount;
            //voucherEntrySubmission.chqDate = chqDate;

            $.confirm({
                title: 'Message',
                content: 'Are You Sure To Submit These Records',
                buttons: {
                    ok: function () {

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/genAccounting/submitVoucherEntry",
                data: JSON.stringify(voucherEntrySubmission),
                dataType: 'json',
                success: function (data) {
                    if(data == true){
                        showAlert("Submitted Successfully");
                        location.reload();
                    }else{
                        showAlert("Duplicate Found!!!");
                    }
                    getVoucherList(voucherNo);
                },
                error: function (e) {
                    showAlert("Duplicate Found!!!");
                }
            });
        },
        cancel: function () {

        },
    }
});
    });

    function dataValidation() {
        var status = true;

        if ($("#OfficeId").val() == "" || $("#OfficeId").val() == "0" ) {
            status = false;
            $("#officeIdSpan").text("Empty field found!!");
            $("#OfficeId").focus();
        } else $("#officeIdSpan").text("");

        return status;
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        return d;
    };


    /*function clearform(){
        $('#OfficeId').val("");
        $('#assuredName').val("");
        $('#proMobile').val("");
        $('#rowkey').val("");
        $('#proposalNo').val("");
        $('#pgid').val("");

    }
*/
});

function deAllAccTransMst(){

    var voucherNo = $('#voucherNo').val();

    if(voucherNo){

        var json = {
            "voucherNo": voucherNo
        };

        $.confirm({
            title: 'Message',
            content: 'Are You Sure To Delete All Records of This Voucher No',
            buttons: {
                ok: function () {
                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json',
                        url: "/genAccounting/delAllAccTransMstDtl/"+voucherNo,
                        data: JSON.stringify(json),
                        dataType: 'json',
                        success: function (data) {

                            if(data){
                                $.confirm({
                                    title: 'Message',
                                    content: 'Deleted Successfully',
                                    buttons: {
                                        ok: function () {
                                        },
                                        cancel: function () {
                                        },
                                    }
                                });
                            }else{
                                $.confirm({
                                    title: 'Message',
                                    content: 'Child Record Found!!',
                                    buttons: {
                                        ok: function () {
                                        },
                                        cancel: function () {
                                        },
                                    }
                                });
                            }
                        },
                        error: function (e) {
                            $.confirm({
                                title: 'Message',
                                content: 'Child Record Found!!',
                                buttons: {
                                    ok: function () {
                                    },
                                    cancel: function () {
                                    },
                                }
                            });
                        }
                    });
                },
                cancel: function () {

                },
            }
        });
    }else{
        $.confirm({
            title: 'Message',
            content: 'Please Create Voucher No at first',
            buttons: {
                ok: function () {
                },
            }
        });
    }
}

function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};