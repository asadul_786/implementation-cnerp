/**
 * Created by SIL_PC_73 on 6/19/2019.
 */

$(document).ready(function () {

    function generateActionBtns(id) {

        var action = "<button type='button' class='btn btn-info fiscal-ext-approve' id='" + id + "'>" +
            "<i class='fa fa-check'></i></button>" +
        "<button type='button' class='btn btn-danger fiscal-ext-decline' id='" + id + "'>" +
         "<i class='fa fa-close'></i></button>";

        return action;
    }

    $.get("/accounts/get-all-fiscal-year",
        function (data, status) {
            $.each(data, function (index, elm) {
                var start_date = new Date(elm.fiscalYear.startDate);
                var end_date = new Date(elm.fiscalYear.endDate);
                var ext_date = new Date(elm.extensionFor);
                var formatted_start_date = start_date.getDate() + "-" + (start_date.getMonth() + 1) + "-" + start_date.getFullYear();
                var formatted_end_date = end_date.getDate() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getFullYear();
                var formatted_ext_date = ext_date.getDate() + "-" + (ext_date.getMonth() + 1) + "-" + ext_date.getFullYear();

                var statusStr = "";
                if(elm.isActive == 0){
                    statusStr = "Inactive";
                } else statusStr = "Active";

                var extensionStr = "";
                if(elm.extensionFor) extensionStr = "Yes - " + formatted_ext_date;
                else extensionStr= "No";

                var extStateStr = "";
                if(elm.extState == 0){
                    extStateStr = "Not Approved";
                }
                else if(elm.extState == 1){
                    extStateStr = "Approved";
                }
                else extStateStr = "Declined";


                var extComment = "";
                if(elm.extComment){
                    extComment =elm.extComment;
                }
                var commentHtml = "<textarea id='txtArea' rows='3' cols='30'>" + extComment + "</textarea>";


                var html = "<tr id='" + elm.id + "'>" +
                    "<td>" + elm.office.officeName + "</td>" +
                    "<td>" + formatted_start_date + " to " + formatted_end_date + "</td>" +
                    "<td>" + statusStr + "</td>" +
                    "<td>" +  extensionStr + "</td>" +
                    "<td>" +  commentHtml + "</td>" +
                    "<td>" +  extStateStr + "</td>" +
                    "<td>" + generateActionBtns(elm.id) + "</td>" +

                    "</tr>";

                $("#fiscal_table_body").append(html);

            });

            $('#fiscal_table').DataTable();

        });

    var fiscal_ext_approve_body = {};
    var ajaxResponse = false;
    $(document).on("click", ".fiscal-ext-approve", function (e) {

        var request = $(this).closest('tr').find('td').eq(3).text().trim();
        if(request==='No'){
            showAlert('There is no extension request');
        }
        else{
            var comment = $(this).closest('tr').find('td').find("textarea").val();
            fiscal_ext_approve_body.comment = comment;
            fiscal_ext_approve_body.id = $(this).closest('tr').attr('id');
            $.ajax({
                url: "approve-fiscal-ext",
                type: 'POST',
                async: false,
                data: JSON.stringify(fiscal_ext_approve_body),
                dataType: 'json',
                contentType: 'application/json',
                success: function (response) {
                    if(response==true) {
                        ajaxResponse = true;
                        showAlert('Extension Approved');
                    }
                    else showAlert("Something went wrong!");
                },
                error: function (xhr, status, error) {
                    alert("Something went wrong!");
                }
            });

            if(ajaxResponse) $(this).closest('tr').find('td').eq(5).text('Approved');
        }


    });


    var fiscal_ext_decline_body = {};
    var ajaxResponseDecline = false;
    $(document).on("click", ".fiscal-ext-decline", function (e) {

        var request = $(this).closest('tr').find('td').eq(3).text().trim();
        if(request==='No'){
            showAlert('There is no extension request');
        }
        else{
            var comment = $(this).closest('tr').find('td').find("textarea").val();
            fiscal_ext_decline_body.comment = comment;
            fiscal_ext_decline_body.id = $(this).closest('tr').attr('id');

            $.ajax({
                url: "decline-fiscal-ext",
                type: 'POST',
                async: false,
                data: JSON.stringify(fiscal_ext_decline_body),
                dataType: 'json',
                contentType: 'application/json',
                success: function (response) {
                    if(response==true) {
                        ajaxResponseDecline = true;
                        showAlert('Extension Declined');
                    }
                    else showAlert("Something went wrong!");
                },
                error: function (xhr, status, error) {
                    alert("Something went wrong!");
                }
            });

            if(ajaxResponseDecline) $(this).closest('tr').find('td').eq(5).text('Declined');
        }


    });






});