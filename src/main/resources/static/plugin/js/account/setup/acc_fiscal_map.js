/**
 * Created by SIL_PC_73 on 6/13/2019.
 */

$(document).ready(function () {
    function validate() {
        var status = true;
        if($('#fiscal_office_cat_id option:selected').val()==="-1" && status){
            $('#err_fiscal_office_cat_id').text('Please select office category');
            $('#fiscal_office_cat_id').focus();
            status= false;
        }
        else $('#err_fiscal_office_cat_id').text('');

        if($('#fiscal_office_id option:selected').val()==="-1" && status){
            $('#err_fiscal_office_id').text('Please select office');
            $('#fiscal_office_id').focus();
            status= false;
        }
        else $('#err_fiscal_office_id').text('');

        if($('#fiscal_year_id option:selected').val()=== "-1" && status){
            $('#err_fiscal_year_id').text('Please select fiscal year');
            $('#fiscal_year_id').focus();
            status= false;
        }
        else $('#err_fiscal_year_id').text('');

        return status;
    }
    function generateActionBtns(id) {

        var action = "<button type='button' class='btn btn-info fiscal-edit' id='" + id + "'>" +
            "<i class='fa fa-pencil'></i></button>" ;
            /*"<button type='button' class='btn btn-danger fiscal-delete' id='" + id + "'>" +
            "<i class='fa fa-trash'></i></button>";*/

        return action;
    }

    $.get("/accounts/get-all-fiscal-year",
        function (data, status) {
            $.each(data, function (index, elm) {
                var start_date = new Date(elm.fiscalYear.startDate);
                var end_date = new Date(elm.fiscalYear.endDate);
                var formatted_start_date = start_date.getDate() + "-" + (start_date.getMonth() + 1) + "-" + start_date.getFullYear();
                var formatted_end_date = end_date.getDate() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getFullYear();

                var statusStr = "";
                if(elm.isActive == 0){
                    statusStr = "Inactive";
                } else statusStr = "Active";

                var extensionStr = "";
                if(elm.hasExtension==0) extensionStr = "No";
                else extensionStr= "Yes";


                var html = "<tr id='" + elm.id + "'>" +
                    "<td>" + elm.office.officeName + "</td>" +
                    "<td>" + formatted_start_date + " to " + formatted_end_date + "</td>" +
                    "<td>" + statusStr + "</td>" +
                    "<td>" +  extensionStr + "</td>" +
                    "<td>" + generateActionBtns(elm.id) + "</td>" +

                    "</tr>";

                $("#fiscal_table_body").append(html);

            });

            $('#fiscal_table').DataTable();

        });



    var fiscal_body = {};
    $(document).on("click", "#fiscal_map_btn", function (e) {

        if(validate()){
            var activeStr = "";
            fiscal_body.officeId = $('#fiscal_office_id').val();
            fiscal_body.fiscalYearId = $('#fiscal_year_id').val();

            if ($('#fiscal_active').is(":checked")) {
                fiscal_body.activeStatus = 1;
                activeStr = "Active";
            }
            else {
                fiscal_body.activeStatus = 0;
                activeStr = "Inactive";
            }
            fiscal_body.fiscalPk = $("#fiscal_pk").val();
            fiscal_body.extensionFor = $("#fiscal_ext_for").val();

            $.ajax({
                contentType: 'application/json',
                url: "add-fiscal-year",
                type: 'POST',
                async: false,
                data: JSON.stringify(fiscal_body),
                success: function (response) {

                    if($("#fiscal_pk") !="0" ){
                        var tr = '#'+response.id;
                        $(tr).remove();
                    }
                    var html = "<tr id='" + response.id + "'>" +
                        "<td>" + $('#fiscal_office_id option:selected').text() + "</td>" +
                        "<td>" + $('#fiscal_year_id option:selected').text() + "</td>" +
                        "<td>" + activeStr + "</td>" +
                        "<td>" + "No" + "</td>" +
                        "<td>" + generateActionBtns(response.id) + "</td>" +
                        "</tr>";
                    //alert(response.id);
                    showAlert("Successfully saved");
                    $("#fiscal_table_body").append(html);
                    //datatable.columns.adjust().draw();
                   // $('#fiscal_table').DataTable().columns.adjust().draw();


                    //location.reload();
        },
                error: function (xhr, status, error) {
                    showAlert("Fiscal Year exists for the selected office"); //try catch ora-00001 backend ~~ implement
                }
            });
        }
        else{

        }

    });

    $(document).on("click", ".fiscal-edit", function (e) {
       // alert($(this).closest('tr').attr('id'));

        $.ajax({
            url: "get-fiscal-year-by-id",
            type: 'GET',
            async: false,
            data: {id : $(this).closest('tr').attr('id')},
            dataType: 'json',
            success: function (response) {
                $('#fiscal_office_id').val(response.officeId);
                $('#fiscal_year_id').val(response.fiscalYearId);
                $('#fiscal_office_cat_id').val(response.officeCatId);

                if(response.status == 0) $('#fiscal_active').prop('checked', false);
                else $('#fiscal_active').prop('checked', true);
                $('#fiscal_pk').val(response.id);

                var office = $('#fiscal_office_id');
                office.empty();
                office.append($('<option/>', {
                    value: response.officeId,
                    text: response.officeName
                }));

                $('#fiscal_ext_for').val(response.extensionFor);


            },
            error: function (xhr, status, error) {
                alert("Something went wrong!");
            }
        });
        $('#fiscal_office_cat_id').prop("disabled", true);
        $('#fiscal_office_id').prop("disabled", true);
        $('#fiscal_year_id').prop("disabled", true);
        $('#fiscal_ext_div').show();
    });

    $(document).on("click", ".fiscal-delete", function (e) {
        alert($(this).closest('tr').find('td').eq(0).text().trim());

    });

    $(document).on("click", "#fiscal_map_reload", function (e) {
        location.reload();
    });

    $('#myform').submit(function(ev) {
        ev.preventDefault();
        // ajax stuff...
    });


});