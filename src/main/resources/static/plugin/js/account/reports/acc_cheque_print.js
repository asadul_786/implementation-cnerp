/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

/*
    var table = $("#sur_pay_cheque_info_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "dummyCall",
            "type": "GET",
            "data": function ( d ) {
                d.officeCD = $("#office_cd").val(),
                    d.policyNo = $.trim($("#policy_no").val()),
                    d.dateFrom = $.trim($("#dt_from").val()),
                    d.dateTo = $.trim($("#dt_to").val())
            },
            "dataType": "json"
        },
        "initComplete": function(settings, json) {
            $("#sur_pay_cheque_info_table_processing").css({"top": "5px", "color": "green"});
        },
        "autoWidth": true,
        "columns": [
            // { "data": "pgId", "name": "PGID", className: "dt-hd" },
            { "data": "policyNo", "name": "POLICY_NO" },
            { "data": "payOrderNo", "name": "PAY_ORDER_NO" },
            { "data": "payOrderDate", "name": "PAY_ORDER_DATE" },
            { "data": "accNo", "name": "ACCOUNT_NO" },
            { "data": "officeNM", "name": "OFFICE_NAME" },
            { "data": "partyNM", "name": "LOAN_PARTY_NAME" },
            { "data": "bankNM", "name": "BANK_NM" },
            { "data": "brNM", "name": "BR_NM" },
            { "data": "netDisAmt", "name": "NET_DISBURS_AMOUNT" },
            { "data": "amtInWord", "name": "IN_WORD" },
            { "data": "disAccNo", "name": "DIS_ACCOUNT_NO" },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button type="button" id="show_rpt" class="btn btn-success">View</button>';
                }
            }
        ]
    });
*/

    $(document).on("click", "#chk_print_rpt", function () {

        if(validate()){

            $.ajax({
                url:  "checkChqData",
                type: 'GET',
                data: {
                    officeCd: $.trim($("#office_cd").val()),
                    dateFrom: $.trim($("#dt_from").val()),
                    dateTo: $.trim($("#dt_to").val()),
                    trnsTp: $.trim($("#pay_for").val()),
                    trnsVal : $.trim($("#dyn_trn_no").val())
                },
                success: function(response) {
                    if(response == ""){
                        window.open('/accounts/reports/generatedCheque.pdf?tTp=' + $.trim($("#pay_for").val())
                                    + '&tV=' + $.trim($("#dyn_trn_no").val())
                                    + '&o=' + $.trim($("#office_cd").val())
                                    + '&dF=' + $.trim($("#dt_from").val())
                                    + '&dT=' + $.trim($("#dt_to").val())
                                    , '_blank');
                        //window.open('/accounts/reports/generatedCheque.pdf?v=' + response, '_blank');
                    }
                    else
                        showAlertByType(response, "F");
                },
                error: function(xhr, status, error) {
                    showAlertByType("Something went wrong !!", "F");
                },
                complete: function () {
                }
            });
        }

    });

    /*$(document).on("click", "#chk_print_rpt", function () {
        if(validate()){
            $("#err_dt_from").text("");
            table.ajax.url("chequeInfoList").load();
        }
    });*/

    $(document).on("change", "#pay_for", function () {
        spn = '<span class="err_msg"> *</span>';
        if($("#pay_for").val() == "03"){
            $("#dyn_lbl h5").text("Requisition No.");
            $("#dyn_lbl h5").append(spn);
            $("#dyn_trn_no").attr("placeholder", "Requisition No.");
        }
        else if($("#pay_for").val() == "04"){
            $("#dyn_lbl h5").text("Transaction No.");
            $("#dyn_lbl h5").append(spn);
            $("#dyn_trn_no").attr("placeholder", "Transaction No.");
        }
        else if($("#pay_for").val() == "08"){
            $("#dyn_lbl h5").text("Employee ID");
            $("#dyn_lbl h5").append(spn);
            $("#dyn_trn_no").attr("placeholder", "Employee ID");
        }
        else{
            $("#dyn_lbl h5").text("Policy Number");
            $("#dyn_lbl h5").append(spn);
            $("#dyn_trn_no").attr("placeholder", "Policy Number");
        }
    });

    /*$(document).on("change", "#dt_from", function (e) {
        fromDt = $.trim($('#dt_from').val());
        if(fromDt != ""){
            $("#dt_to").val('');
        }
    });*/

    $( "#dt_from" ).datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateStr)
        {
            $("#dt_to").datepicker("destroy");
            $("#dt_to").val(dateStr);
            $("#dt_to").datepicker({ minDate: dateStr, dateFormat: 'dd/mm/yy'});
        }
    });

    $(document).on("click", "#clear_btn", function () {
        $('#accChkPrnt').trigger("reset");
        $(".rmv").text("");
        $('#pay_for').val(-1).select2().trigger('change');
        $('#office_cd').val(-1).select2().trigger('change');
    });

    function validate() {

        $(".rmv").text("");

        if($("#pay_for").val() == "-1"){
            $("#err_pay_for").text("Required !!");
            return;
        }

        if($.trim($("#dyn_trn_no").val()) == ""){
            $("#err_dyn_trn_no").text("Required !!");
            return;
        }

        if($.trim($("#office_cd").val()) == "-1"){
            $("#err_office_cd").text("Required !!");
            return;
        }

        return true;
    }

});