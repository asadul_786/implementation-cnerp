
$(document).ready(function () {

// start

        //
        // function createPopup( data ) {
        //     var mywindow = window.open( "", "new div", "height=400,width=600" );
        //     mywindow.document.write( "<html><head><title></title>" );
        //     mywindow.document.write( "<link rel=\"stylesheet\" href=\"http://localhost:3001/theme/vendors/bootstrap/dist/css/bootstrap.css\" type=\"text/css\"/>" );
        //     mywindow.document.write( "<link rel=\"stylesheet\" href=\"http://localhost:3001/theme/build/css/custom.min.css\" type=\"text/css\"/>" );
        //     // http://localhost:3001/theme/build/js/custom.min.js
        //
        //     // mywindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\"/>" );
        //     mywindow.document.write( "</head><body >" );
        //     mywindow.document.write( data );
        //     mywindow.document.write( "</body></html>" );
        //
        //     mywindow.print();
        //     //mywindow.close();
        //
        //     return true;
        //
        // }
        // document.addEventListener( "DOMContentLoaded", function() {
        //     document.getElementById( "print" ).addEventListener( "click", function() {
        //         createPopup( document.getElementById( "printAreaFrame" ).innerHTML );
        //
        //     }, false );
        //
        // });
        //

    // end


    //hide gen button if fpr already exists
    if( $('#fprNo').text() != "" || $('#policyNo').text() != "") {
        $('#btnGenerateShow').html("")
            // $('#btnGenerate').hide();
    }



    $(document).on("click", "#btnPrnt", function (e) {
        var printContents = $('#printAreaFrame').html();

        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    });

    $(document).on("click", "#btnGenerate", function (e) {
        //console.log(fprRegisterDtoList);

        // if(fprRegisterDtoList == null)
        // {
        //     alert("FPR generation is not possible");
        // }
        // else
        // {
        //     if(fprRegisterDtoList.policyNo == null || fprRegisterDtoList.fprNo == null)
        //     {
        //         alert("FPR generation is not possible");
        //     }
        //     else
        //     {
        //         $.ajax({
        //             type: "POST",
        //             contentType: "application/json; charset=utf-8",
        //             url: "/collection/generated-fpr",
        //             data: JSON.stringify(fprRegisterDtoList),
        //             success: function (result) {
        //                 alert(result);
        //             },
        //             error: function(jqXHR)
        //             {
        //                 alert(jqXHR);
        //             }
        //         });
        //     }
        // }

        ///////////////////////////////////////////

        var proposalNo = $('#proposalNo').text();
        $.ajax({
            contentType: 'application/json',
            url: "get-generated-fpr",
            type: 'POST',
            async: false,
            data: JSON.stringify(proposalNo),
            dataType: 'json',
            success: function (response) {
                $('#proposalNo').text(response.policyNo);
                $('#fprNo').text(response.fprNo);
                $('#policyNo').text(response.policyNo);

                showAlertByType(response.message, response.messageStatus);

                if (response.messageStatus == "S" || response.messageStatus == "F") { //success
                    $('#btnGenerate').hide();
                }
                // else if (response.messageStatus == "W") { //failure
                //     showAlert(response.message);
                // }
                // else if (response.messageStatus == "F") { //warning
                //     showAlert(response.message);
                // }
            },
            error: function (xhr, status, error) {
            }
        });



    });


});