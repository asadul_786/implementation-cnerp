/*function setCollOffice(){

    var json = {
        "param": ''

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getCollOffice",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var value_1 = "";
            var value_2 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
                value_2 = l[1];
            });

            $("#collOffice").val(value_2);
            $("#collOfficeId").val(value_1);


        },
        error: function (e) {
        }
    });

}*/
$(document).ready(function () {
    //setCollOffice();
});

/*function setPgIdRecvIdToAdvCollForm(recvId){

    var proposalNo =  $("#proposalNo").val();
    var offId =  $("#offId").val();
    var values = recvId.concat(",").concat(proposalNo).concat(",").concat(offId);
    alert(values);

    var json = {
        "values": values

    };

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Adjust This Record',
        buttons: {
            ok: function () {
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: "/collection/getvaluesForAdvCollForm/"+values,
                    data: JSON
                        .stringify(json),
                    dataType: 'json',
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                         alert(data);
                        document.location.href="/collection/getAdvanceCollForAdjustment/"+data;


                    },
                    error: function (e) {
                    }
                });
            },
            cancel: function () {
            },
        }
    });
}*/

function getProposalCollAdjustmentList(){

    var table = $("#dataTable tbody");

    var propNo =  $("#propNo").val();
    var collOfficeId =  $("#collOfficeId").val();

    var values = propNo.concat(",").concat(collOfficeId);

    var json = {
        "values": values
    };

    if(propNo == ''){

        $.confirm({
            title: 'Message',
            content: 'Please Provide Valid Proposal No',
            buttons: {
                ok: function () {

                },
                cancel: function () {
                },
            }
        });
    }else{
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/collection/getProposalCollAdjustmentList/"+values,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                $("#propNo").val('');

                table.empty();

                var proposalNo = "";
                var collectionDate = "";
                var collectedAmount = "";
                var proposerName = "";
                var premAmount = "";
                var totalCollAmount = "";
                var totalDueAmount = "";
                var office = "";
                var pgid = "";
                var propoNo = "";
                var collAmStr = "";
                var premAmountStr = "";
                var totalCollAmountStr = "";
                var totalDueAmountStr = "";
                var collectionDateStr = "";

                $.each(data, function (i, l) {

                    proposalNo = l[2];
                    collectionDate = l[1];
                    collectionDateStr = JSON.stringify(collectionDate);
                    if(collectionDateStr == 'null'){
                        collectionDateStr = "";
                    }else{
                        collectionDateStr = getDateShow(collectionDate);;
                    }
                    collectedAmount = l[7];
                    collAmStr = JSON.stringify(collectedAmount);
                    //alert(typeof(collectedAmount));
                    if(collAmStr == 'null'){
                        collAmStr = "";
                    }else{
                        collAmStr = collAmStr;
                    }

                    proposerName = l[3];

                    premAmount = l[9];
                    premAmountStr = JSON.stringify(premAmount);
                    if(premAmountStr == 'null'){
                        premAmountStr = "";
                    }else{
                        premAmountStr = premAmountStr;
                    }

                    totalCollAmount = l[11];
                    totalCollAmountStr = JSON.stringify(totalCollAmount);
                    if(totalCollAmountStr == 'null'){
                        totalCollAmountStr = "";
                    }else{
                        totalCollAmountStr = totalCollAmountStr;
                    }

                    totalDueAmount = l[10];
                    totalDueAmountStr = JSON.stringify(totalDueAmount);
                    if(totalDueAmountStr == 'null'){
                        totalDueAmountStr = "";
                    }else{
                        totalDueAmountStr = totalDueAmountStr;
                    }

                    office = l[5];
                    pgid = l[6];
                    propoNo = l[2];

                    table.append("<tr><td>"+'<span>'+proposalNo+'</span>'+"</td>" +
                        "<td>"+collectionDateStr+"</td>" +
                        "<td>"+collAmStr+"</td>"+
                        "<td>" + proposerName + "</td>" +
                        "<td>" + premAmountStr + "</td>" +
                        "<td>" + totalCollAmountStr + "</td>" +
                        "<td>" + totalDueAmountStr + "</td>" +
                        "<td>" + office + "</td>" +
                        "<td>" +
                        '<button id="adjust" type="button" value='+pgid+","+propoNo+","+premAmountStr+","+totalCollAmountStr+","+totalDueAmountStr+' onclick="return setValuesForAdvCollForm(this.value);" class="btn btn-danger">'+
                        "ADJUST"
                        +'</button>' + "</td></tr>");
                });
                $("#dataTable").DataTable();
            },
            error: function (e) {
            }
        });
    }
}
function setValuesForAdvCollForm(values){

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Adjust This Record',
        buttons: {
            ok: function () {
                document.location.href="/collection/getAdvanceCollForAdjustment/"+values;
            },
            cancel: function () {
            },
        }
    });
}

function getDateShow(dateObject) {

    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};