$(document).ready(function() {
    bankAutoComplete();
    clientBankAutoComplete();
    //clientBranchAutoComplete();
});

function clientBankAutoComplete() {
    var ClientBankName = "#" + "ClientBankName";
    var url = "/collection/bankAutoComplete";
    var ClientBankNameId = "#" + "bankId";
    autocompleteBank(ClientBankName, url, "", ClientBankNameId);
}
/*function clientBranchAutoComplete() {
    var clientBranchName = "#" + "clientBranchName";
    var url = "/collection/branchAutoComplete";
    var clientBranchNameId = "#" + "clientBranchNameId";
    autocomplete(clientBranchName, url, "", clientBranchNameId);
}*/
function bankAutoComplete() {
    var bankName = "#" + "bankName";
    var url = "/collection/bankAutoComplete";
    var bankId = "#" + "bankId";
    autocompleteBank(bankName, url, "", bankId);
}

function autocompleteBank(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                    getBranchList(ui.item.value,focusId);
                }
            });

        }
    });
}

function getBranchList(bankId,focusId){

    var accountType = $('#accountType').val();

    var json = {
        "bankId": bankId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
        + bankId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            if(focusId==='#bankName'){
                $('#branchName').empty();
                $.each(data, function(key, value) {
                    $('#branchName').append('<option value="' + key + '">' + value + '</option>');
                });
            }else{
                $('#clientBranchName').empty();
                $.each(data, function(key, value) {
                    $('#clientBranchName').append('<option value="' + key + '">' + value + '</option>');
                });
            }
            var branchId = $('#branchId').val();
            setCompanyAccountNo(accountType,branchId);
        },
        error: function (e) {

        }
    });
}

function setCompanyAccountNo(accountType,branchId){

    var json = {
        "accountType": accountType,
        "branchId": branchId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getCompanyAccountNo/"
        + accountType+"/"+branchId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('#accountNo').val('');
            $('#accountNo').val(data);
        },
        error: function (e) {

        }
    });

}
function setPolicyOffice(policyNo){

    var json = {
        "policyNo": policyNo
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getPolicyOffice/"
        + policyNo,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var val_0 = "";
            $.each(data, function (i, l) {
                val_0 = l[0];
            });
            $('#servicingOffice').val('');
            $('#servicingOffice').val(val_0);
        },
        error: function (e) {
            showAlert("Something Wrong!!!");
        }
    });

}

$(function(){

    $("#policyNo").on('change', function(){

        var policyNo = $('#policyNo').val();
        setPolicyOffice(policyNo);


        var json = {
            "policyNo": policyNo
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/collection/getSurDisChqIssueInfo/"
            + policyNo,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {

                    var val_0 = "";
                    var val_1 = "";
                $.each(data, function (i, l) {
                    val_0 = l[0];
                    val_1 = l[1];
                });
                $('#pgId').val('');
                $('#applSlNo').val('');
                $('#pgId').val(val_0);
                $('#applSlNo').val(val_1);
            },
            error: function (e) {
                showAlert("Something Wrong!!!");
            }
        });
})
});

function dataValidation() {

    var status = true;

    if ($("#policyNo").val() == "" || $("#policyNo").val() == "0") {
        status = false;
        $("#policyNoSpan").text("Empty field found!!");
        $("#policyNo").focus();
    }
    else $("#policyNoSpan").text("");

    return status;
}

$(document).ready(function () {

$("#surrCheqIssueBtn").click(function () {

    var flag = dataValidation();

    if (flag == true) {

        var applSlNo = $('pgId').val();
        applSlNo = '9002/210/11';
        if(applSlNo){
            applSlNo = applSlNo.split('/').join(' ');
            //alert(applSlNo);
        }
        var pgId = $('#applSlNo').val();
        pgId = '9905310647000397';
        var surVal = $('#surrenderValue').val();
        var netSurAmount = $('#netSurrenderAmount').val();
        var appAmount = $('#approvedAmount').val();
        var netDisAmt = $('#disbursementAmount').val();

        var surChqIssue = {};

        surChqIssue.applSlNo = applSlNo;
        surChqIssue.pgId = pgId;
        surChqIssue.surVal = surVal;
        surChqIssue.netSurAmount = netSurAmount;
        surChqIssue.appAmount = appAmount;
        surChqIssue.netDisAmt = netDisAmt;


        //alert(JSON.stringify(surChqIssue));

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/collection/saveSurChqIssue",
            data: JSON.stringify(surChqIssue),
            dataType: 'json',
            success: function (data) {
                //alert(JSON.stringify(surChqIssue));
                showAlert("Successfully added.");
            },
            error: function (e) {
                alert("Something Wrong!!!");
            }
        });
    }

})
});


/*


<div layout:fragment="script">

    <script>
    $(document).ready(function () {
        load();
        currentDate();
        $("#calcualtionoption").on("change",function () {
            var calcualtionoption = $(this).val();
            if(calcualtionoption == 1){
                $("#officetype").removeAttr("disabled");
                $("#officetype").val("-1");
                $("#employee").attr("disabled","disabled");
            } else{
                $("#employee").removeAttr("disabled");
                $("#employee_id").val("");
                $("#employee").val("");
                $("#officetype").attr("disabled","disabled");
            }
        });
    });
function currentDate(){
    n =  new Date();
    y = n.getFullYear();
    m = n.getMonth() + 1;
    d = n.getDate();
    currentDate =  d + "/" + m + "/" + y;
    $("#salarydate").val(currentDate);
}

function salaryDateRange(){
    var salarymonthfrom = Number($("#salarymonthfrom").val());
    var salarymonthto = Number($("#salarymonthto").val());
    if(salarymonthto < salarymonthfrom){
        salarymonthto = salarymonthfrom;
        $("#salarymonthto").val(salarymonthto);
    }
    var salaryyear = $("#salaryyear").val();
    var salarydatefrom = "01/"+salarymonthfrom+"/"+salaryyear;
    $("#salarydatefrom").val(salarydatefrom);
    var salarydateto = new Date(salaryyear, Number(salarymonthto), 0).getDate()+"/"+salarymonthto+"/"+salaryyear;
    $("#salarydateto").val(salarydateto);
}

$(".month_data,#salaryyear").on("change",function(){
    salaryDateRange();
});

$("#employee").on("change",function () {
    var employee = $(this).val().trim();
    $.ajax(
        {
            method:"POST",
            url:"/payroll/employee-list",
            dataType:"json",
            success:function (data) {
            }
        }
    );
});

function load(){
    $.ajax(
        {
            method:"POST",
            url:"/payroll/load-data",
            dataType:"json",
            success:function (data) {
                var employee_type = "";
                $.each(data.emptypes,function (k,v) {
                    employee_type+="<option value='"+v[0]+"'>"+v[1]+"</option>";
                });
                $("#employeetype").html(employee_type);
                var months = "";
                $.each(data.months,function (k,v) {
                    months+="<option value='"+v[1]+"'>"+v[0]+"</option>";
                });
                $(".month_data").html(months);
                var years = "";
                $.each(data.years,function (k,v) {
                    years+="<option value='"+v[0]+"'>"+v[1]+"</option>";
                });
                $("#salaryyear").html(years);
                var offices = "<option value='-1'>--Select All--</option>";
                $.each(data.offices,function (k,v) {
                    offices+="<option value='"+v[0]+"'>"+v[1]+"</option>";
                });
                $("#officetype").html(offices);
            }
        }
    ).then(function(){
        salaryDateRange();
    });
}
</script>

</div>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
    $(document).ready(function () {
        $('.select').select2();
    });
</script>


*/
