var policNoLabelAndTextField = '<tr>'
    + '<td><label name="collDate" id="collDate"><h5>Policy No</h5></label>'
    + '</td> '
    + '<td><input type="text" name="policyNo" id="policyNo" style="margin-left: 64%;width: 196%;height: 35px;" value="'+'">'
    + '<span style="color:red;" class="err_msg" id="policyNoSpan">'
    + '</td> '
    + '</tr>';

var requisitonNoLabelAndTextField = '<tr>'
    + '<td><label name="requisitionNoLabel" id="requisitionNoLabel"><h5>Requisition No</h5></label>'
    + '</td> '
    + '<td><input type="text" name="requisitionNo" id="requisitionNo" autocomplete="true" style="margin-left: 44%;width: 195%;height: 35px; value="'+'">'
    + '<span style="color:red;" class="err_msg" id="requisitionNoSpan">'
    + '</td> '
    + '</tr>';

var transactionNoLabelAndTextField = '<tr>'
    + '<td><label name="transactionNoLabel" id="transactionNoLabel"><h5>Transaction No</h5></label>'
    + '</td> '
    + '<td><input type="text" name="transactionNo" id="transactionNo" autocomplete="true" style="margin-left: 42%;width: 195%;height: 35px; value="'+'">'
    + '<span style="color:red;" class="err_msg" id="transactionNoSpan">'
    + '</td> '
    + '</tr>';

var employeeIdLabelAndTextField = '<tr>'
    + '<td><label name="emplIdLabel" id="emplIdLabel"><h5>Employee ID</h5></label>'
    + '</td> '
    + '<td><input type="text" name="emplId" id="emplId" style="margin-left: 50%;width: 196%;height: 35px;" autocomplete="true" value="'+'">'
    + '<span style="color:red;" class="err_msg" id="emplIdSpan">'
    + '</td> '
    + '</tr>';

var billNoLabelAndTextField = '<tr>'
    + '<td><label name="billNolabel" id="billNolabel"><h5>Bill No</h5></label>'
    + '</td> '
    + '<td><select name="billNo" id="billNo" style="margin-left: 500%;height: 35px;">'
    + '</select>'
    + '<span style="color:red;" class="err_msg" id="billNoSpan">'
    + '</td> '
    + '</tr>';

$(document).ready(function() {

    $('#agentName').attr('disabled',true);
    $('#descendentOfficeVal').val('0');

    //agentAutoComplete();

    function clearform(){
        $('#paymentFor').val('');
        $('#officeId').val('');
        $('#emplId').val('');
        $('#appFromDate').val('');
        $('#appToDate').val('');
        $('#chequeDate').val('');
        $('#policyNo').val('');
        $('#requisitionNo').val('');
        $('#transactionNo').val('');
        $('#descendentOfficeVal').val('');
    }

    $( "#chequeDate" ).val($.datepicker.formatDate('dd/mm/yy', new Date()));
    officeAutoComplete();
    //setEmplId();

    var regexVal =/(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/;

    $('#chequeDateSpan').remove();
    $('#chequeDate').on('keypress keydown keyup blur focus',function(){

        if (!$(this).val().match(regexVal)) {
            //$('#chequeDateSpan').show().text("only dd/mm/yyyy is allowed!!");
            $('#chequeDate').val('');
        }
    });

    /*$("#agentName").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#agentNameSpan").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });*/

    $("#tableBody").empty();
    $("#tableBody")
        .append(policNoLabelAndTextField);

    $('#paymentFor').on('change', function() {

        var item = $('#paymentFor').val();
        var agentName = $('#agentName').val();

        if(item === "" || item === "01" || item === "03" || item === "02" || item === "06" || item === "07"){

            $('#policyBody').empty();

            $("#tableBody").empty();
            $("#tableBody")
                .append(policNoLabelAndTextField);

            $('#requisitionNoBody').empty();
            $('#transactionNoBody').empty();
            $('#emplIdBody').empty();
            $('#billNoBody').empty();

            $('#requisitionNoBody').append('<input type="hidden" name="requisitionNo" id="requisitionNo" value="">');
            $('#transactionNoBody').append('<input type="hidden" name="transactionNo" id="transactionNo" value="">');
            $('#emplIdBody').append('<input type="hidden" name="emplId" id="emplId" value="">');
            $('#billNoBody').append('<input type="hidden" name="billNo" id="billNo" value="">');

            if(agentName==null){
                $('#agentName').empty();
                $('#agentName').append(
                    '<option value="' + "0" + '">' + "" + '</option>'
                );

            }

        }
        if(item === "08"){

            $("#tableBody").empty();
            //$("#tableBody").append('<option value="' + '' + '">' + 'Select Bill No' + '</option>');
            /*$("#tableBody")
                .append(billNoLabelAndTextField);*/

            $('#billNoBody').empty();

            $("#policyBody").empty();
            $('#requisitionNoBody').empty();
            $('#transactionNoBody').empty();
            $('#emplIdBody').empty();

            $('#policyBody').append('<input type="hidden" name="policyNo" id="policyNo" value="">');
            $('#requisitionNoBody').append('<input type="hidden" name="requisitionNo" id="requisitionNo" value="">');
            $('#transactionNoBody').append('<input type="hidden" name="transactionNo" id="transactionNo" value="">');
            $('#emplIdBody').append('<input type="hidden" name="emplId" id="emplId" value="">');
        }
        if(item === "03"){

            $("#tableBody").empty();
            $("#tableBody")
                .append(requisitonNoLabelAndTextField);

            $('#requisitionNoBody').empty();

            $("#policyBody").empty();
            $('#transactionNoBody').empty();
            $('#emplIdBody').empty();
            $('#billNoBody').empty();

            $('#billNoBody').append('<input type="hidden" name="billNo" id="billNo" value="">');
            $('#policyBody').append('<input type="hidden" name="policyNo" id="policyNo" value="">');
            $('#transactionNoBody').append('<input type="hidden" name="transactionNo" id="transactionNo" value="">');
            $('#emplIdBody').append('<input type="hidden" name="emplId" id="emplId" value="">');

        }
        if(item === "13"){

            $("#tableBody").empty();
            $("#tableBody")
                .append(transactionNoLabelAndTextField);

            $('#transactionNoBody').empty();

            $("#policyBody").empty();
            $('#requisitionNoBody').empty();
            $('#emplIdBody').empty();
            $('#billNoBody').empty();

            $('#billNoBody').append('<input type="hidden" name="billNo" id="billNo" value="">');
            $('#policyBody').append('<input type="hidden" name="policyNo" id="policyNo" value="">');
            $('#requisitionNoBody').append('<input type="hidden" name="requisitionNo" id="requisitionNo" value="">');
            $('#emplIdBody').append('<input type="hidden" name="emplId" id="emplId" value="">');
        }
        if(item === "12"){

            $("#tableBody").empty();
            $("#tableBody")
                .append(employeeIdLabelAndTextField);

            $('#emplIdBody').empty();

            $("#policyBody").empty();
            $('#requisitionNoBody').empty();
            $('#billNoBody').empty();
            $('#transactionNoBody').empty();

            $('#billNoBody').append('<input type="hidden" name="billNo" id="billNo" value="">');
            $('#policyBody').append('<input type="hidden" name="policyNo" id="policyNo" value="">');
            $('#requisitionNoBody').append('<input type="hidden" name="requisitionNo" id="requisitionNo" value="">');
            $('#transactionNoBody').append('<input type="hidden" name="transactionNo" id="transactionNo" value="">');

        }

        if(item === "05"){

            $("#tableBody").empty();
            $("#tableBody")
                .append(policNoLabelAndTextField);

            $('#policyBody').empty();

            $('#requisitionNoBody').empty();
            $('#billNoBody').empty();
            $('#transactionNoBody').empty();
            $('#emplIdBody').empty();

            $('#billNoBody').append('<input type="hidden" name="billNo" id="billNo" value="">');
            $('#emplIdBody').append('<input type="hidden" name="emplId" id="emplId" value="">');
            $('#requisitionNoBody').append('<input type="hidden" name="requisitionNo" id="requisitionNo" value="">');
            $('#transactionNoBody').append('<input type="hidden" name="transactionNo" id="transactionNo" value="">');

        }

    });

    $('#descendentOffice').on('change', function() {
        $('#descOfficeVal').val('');
        $('#descOfficeVal').val(this.checked ? this.value : '');
    });

        /*$("#policyScheduleReport").click(function () {
            var collOfficeId = $('#officeId').val();
            var descOfficeVal = $('#descendentOfficeVal').val();
            if(collOfficeId || descOfficeVal){
                window.open('/collection/generatePolicyScheduleReport.pdf?StrArray=' +collOfficeId+descOfficeVal, '_blank');
            }else{
                alert("please select office or descendent office!!");
            }
        });*/

    $( "#appToDate" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });
    $( "#appFromDate" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });
    $( "#chequeDate" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });
    function getDateFormat(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = year + "-" + month + "-" + day;

        return date;
    };

    $("#writeBtn").click(function () {

            var paymentFor = $('#paymentFor').val();
            var currDate = new Date();
            var officeId = $('#officeId').val();
            var emplId = $('#emplId').val();
            var appFromDate = $('#appFromDate').val();
            var appToDate = $('#appToDate').val();
            var chequeDate = $('#chequeDate').val();
            var policyNo = $('#policyNo').val();
            var requisitionNo = $('#requisitionNo').val();
            var transactionNo = $('#transactionNo').val();
            var descendentOfficeVal = $('#descendentOfficeVal').val();
            var agentName = $('#agentName').val();
            var billNo = $('#billNo').val();



            if(paymentFor == ''){
                $.confirm({
                    title: 'Message',
                    content: 'Please Select Payment Type!!',
                    buttons: {
                        ok: function () {
                            $('#chqWritngProcess').attr('onsubmit','return false;');
                        },
                    }
                });
            }else{

                if(officeId == ''){
                    $.confirm({
                        title: 'Message',
                        content: 'Please Select Office!!',
                        buttons: {
                            ok: function () {
                                $('#chqWritngProcess').attr('onsubmit','return false;');
                            },
                        }
                    });
                }else{
                    var chequeInfoWriting = {};

                    chequeInfoWriting.paymentFor = paymentFor;
                    chequeInfoWriting.officeId = officeId;
                    if(emplId){
                        chequeInfoWriting.emplId = emplId;
                    }else{
                        chequeInfoWriting.emplId = "";
                    }
                    if(appFromDate){
                        appFromDate = appFromDate.split("/").reverse().join("/");
                        appFromDate = getDate(appFromDate);
                    }else{
                        appFromDate = currDate
                    }
                    if(chequeDate){
                        chequeDate = chequeDate.split("/").reverse().join("/");
                        chequeDate = getDate(chequeDate);
                    }else{
                        chequeDate = currDate
                    }
                    if(appToDate){
                        appToDate = appToDate.split("/").reverse().join("/");
                        appToDate = getDate(appToDate);
                    }else{
                        appToDate = currDate
                    }
                    chequeInfoWriting.appFromDate = appFromDate;
                    chequeInfoWriting.appToDate = appToDate;
                    chequeInfoWriting.chequeDate = chequeDate;
                    if(policyNo){
                        chequeInfoWriting.policyNo = policyNo;
                    }else{
                        chequeInfoWriting.policyNo = "";
                    }
                    if(requisitionNo){
                        chequeInfoWriting.transId = requisitionNo;
                    }else{
                        chequeInfoWriting.transId = transactionNo;
                    }
                    chequeInfoWriting.descendentOfficeVal = descendentOfficeVal;
                    chequeInfoWriting.agentId = agentName;
                    chequeInfoWriting.billNo = billNo;

                    //alert(JSON.stringify(chequeInfoWriting));

                    $.confirm({
                        title: 'Confirm',
                        content: 'Are you sure to Write This Cheque?',
                        buttons: {
                            ok: function () {
                                $.ajax({
                                    type: 'POST',
                                    contentType: 'application/json',
                                    url: "/collection/saveChequeInfo",
                                    data: JSON.stringify(chequeInfoWriting),
                                    dataType: 'json',
                                    success: function (data) {
                                        $.each(data, function(key, value) {

                                        });
                                        if(data[2] == "Success"){
                                            $.confirm({
                                                title: 'Confirmation',
                                                content: data[1],
                                                buttons: {
                                                    ok: function () {
                                                    }
                                                }
                                            });
                                        }else{
                                            $.confirm({
                                                title: 'Confirmation',
                                                content: data[2],
                                                buttons: {
                                                    ok: function () {
                                                    }
                                                }
                                            });
                                        }

                                        clearform();
                                    },
                                    error: function (e) {
                                        $.confirm({
                                            title: 'Confirmation',
                                            content: data[2],
                                            buttons: {
                                                ok: function () {
                                                }
                                            }
                                        });
                                        //showAlert("Duplicate Date Found!!");
                                    }
                                });
                            },
                            cancel: function () {

                            }
                        }
                    });
                }
            }
    });
});

function officeAutoComplete() {

    var office = "#" + "office";
    var url = "/collection/officeAutoComplete";
    var officeId = "#" + "officeId";
    autocompleteAgent(office, url, "", officeId);
}

function autocompleteAgent(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                    getAgentName(ui.item.value);
                }
            });

        }
    });
}
function getAgentName(officeId){

    var paymentFor = $('#paymentFor').val();

    var json = {
        "officeId": officeId

    };


    if(paymentFor == "08"){
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/collection/getChqAgentId/"
                + officeId,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                $('#agentName').empty();
                $('#agentName').append('<option value="' + '' + '">' + 'Select Agent Name' + '</option>');
                $.each(data, function(key, value) {
                    $('#agentName').append('<option value="' + key + '">' + value + '</option>');
                });
            },
            error: function (e) {

            }
        });

    }else{
        $('#agentName').empty();
        $('#agentName').attr('disabled',true);
    }

}


function getDate(date){
    var d = new Date(date);
    return d;
}

function getAgentId(agentId){

    var officeId = $('#officeId').val();
    if(agentId == "08"){
        $('#agentName').attr('disabled',false);
        if(officeId){
            getAgentName(officeId);
        }
    }else{
        $("#billNo").empty();
        $('#agentName').empty();
        $('#agentName').attr('disabled',true);
        $('#billNo').attr('disabled',true);
    }
}

function getBillNo() {

    var officeId =  $('#officeId').val();
    var agentName =  $('#agentName').val();
    var appFromDateStr =  $('#appFromDate').val();
    var appToDateStr =  $('#appToDate').val();
    var paymentFor =  $('#paymentFor').val();
    //alert(officeId+agentName+appFromDateStr+appToDateStr+paymentFor);

    //officeId = "0774";
    //agentName = "00902912";
    //appFromDateStr = "18/10/2009";
    //appToDateStr = "18/10/2009";

        if(paymentFor == "08"){
            $.ajax({
                url:  "getBillNo",
                type: 'GET',
                data: {
                    officeId: officeId,
                    agentName: agentName,
                    appFromDate: appFromDateStr,
                    appToDate: appToDateStr
                },
                success: function(response) {
                    var billNo = "";
                    var billDate = "";

                    $("#billNo").empty();
                    $("#billNo").append('<option value="' + '' + '">' + 'Select Bill No' + '</option>');
                    $.each(response, function (i, l) {
                        billNo = l[0];
                        billDate = l[1];
                        $("#billNo").append('<option value="' + billNo + '">' + billDate + '</option>');
                    });
                },
                error: function(xhr, status, error) {
                    //showAlertByType("Something went wrong !!", "F");
                },
                complete: function () {
                }
            });
        }else{

            $("#billNo").empty();
        }
}

