$(document).ready(function() {

    $("#groupPremCollBtn").click(function () {

        var flag = dataValidation();

        var dueDateTo = $('#dueDateTo').val();
        var date = new Date();

        if (flag == true) {

            var grpPremColl = {};

            var pgId = '1806200694000008';

            grpPremColl.applSlNo = '';
            grpPremColl.pgId = pgId;
            if(dueDateTo!=null){
                dueDateTo = getDate(dueDateTo);
                grpPremColl.dueDateTo = dueDateTo;
            }else{
                grpPremColl.dueDateTo = date;
            }

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/collection/saveGrpPremColl",
                data: JSON.stringify(grpPremColl),
                dataType: 'json',
                success: function (data) {
                    alert(JSON.stringify(grpPremColl));
                    showAlert("Successfully added.");
                },
                error: function (e) {
                    alert("Something Wrong!!!");
                }
            });
        }

    })

    setCollOfficeByEmplId();

    $('#collMedia').empty();
    $('#collMedia').append(
        '<option value="' + "3" + '">' + "OL" + '</option>'
    );
    $('#collMedia').prop('disabled', true);
    $('#mediaNo').prop('disabled', true);
    $('#mediaDate').prop('disabled', true);

    bankAutoComplete();

    $('#collCategory').change(function (e) {
        var selectedValue = $(this).val();


        if(selectedValue == 0) {
            return;
        }

        if(selectedValue == '1') {
            $('#mediaNo').prop('disabled', true);
            $('#mediaDate').prop('disabled', true);
            $('#collMedia').empty();
            $('#collMedia').append(
                '<option value="' + "3" + '">' + "OL" + '</option>'
            );
            $('#collMedia').prop('disabled', true);
        }
        if(selectedValue == '2') {
            $('#collMedia').prop('disabled', false);
            $('#collMedia').empty();
            $('#collMedia').append(
                '<option value="' + "1" + '">' + "PR" + '</option>'
                +'<option value="' + "2" + '">' + "BM" + '</option>'
                +'<option value="' + "3" + '">' + "OL" + '</option>'
                +'<option value="' + "4" + '">' + "OR" + '</option>'
            );
            $('#mediaNo').prop('disabled', false);
            $('#mediaDate').prop('disabled', false);
        }
    });
});
function getDate(dateObject) {
    var d = new Date(dateObject);

    return d;
};
function setCollOfficeByEmplId(){

    var json = {
        "officeId": ''

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getCollOffByEmplId",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            //alert(data);

            var value_1 = "";
            var value_2 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
                value_2 = l[1];
            });

            $("#collOffice").val(value_1);
            $("#collOfficeId").val(value_2);
        },
        error: function (e) {
        }
    });

}

function bankAutoComplete() {
    var bankName = "#" + "bankName";
    var url = "/collection/bankAutoComplete";
    var bankId = "#" + "bankId";
    autocompleteBank(bankName, url, "", bankId);
}
function autocompleteBank(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                    getBranchList(ui.item.value);
                }
            });

        }
    });
}
function getBranchList(bankId){

    var json = {
        "bankId": bankId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
        + bankId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

                $('#branchName').empty();
                $.each(data, function(key, value) {
                    $('#branchName').append('<option value="' + key + '">' + value + '</option>');
                });
        },
        error: function (e) {

        }
    });
}
function getGrpPremCollInfo(policyNo){

    var json = {
        "policyNo": policyNo

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getGrpPremCollInfo/"
        + policyNo,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var lastPaidDate = "";
            var riskDate = "";
            var dateComm = "";
            var assuredName = "";
            var address = "";
            var prdName = "";
            var prdIdTerm = "";
            var modeOfPayment = "";
            var summAssured = "";
            var policyStatus = "";

            $.each(data, function (i, l) {
                lastPaidDate = l[23];
                assuredName = l[9];
                dateComm = l[13];
                address = l[14];
                riskDate = l[10];
                prdName = l[24];
                prdIdTerm = l[17];
                modeOfPayment = l[18];
                summAssured = l[15];
                policyStatus = l[21];
            });

            if(riskDate === '' || riskDate === null){
                $("#riskDate").val('');
            }else{
                riskDate = getDateShow(riskDate);
                $("#riskDate").val(riskDate);
            }
            if(lastPaidDate === '' || lastPaidDate === null){
                $("#lastPaidDate").val('');
            }else{
                lastPaidDate = getDateShow(lastPaidDate);
                $("#lastPaidDate").val(lastPaidDate);
            }
            $("#modeOfPayment").val(modeOfPayment);
            $("#sumAssured").val(summAssured);
            $("#policyStatus").val(policyStatus);
            $("#assuredName").val(assuredName);

            if(dateComm === '' || dateComm === null){
                $("#dateOfComm").val('');
            }else{
                dateComm = getDateShow(dateComm);
                $("#dateOfComm").val(dateComm);
            }
            $("#address").val(address);
            $("#productName").val(prdName);
            $("#productIdTerm").val(prdIdTerm);
        },
        error: function (e) {

        }
    });

}

$(function(){

    $("#policyNo").on('change', function(){
        //$("#riskDate").empty();
        /*$("#lastPaidDate").val('');
        $("#modeOfPayment").val('');
        $("#sumAssured").val('');
        $("#policyStatus").val('');
        $("#assuredName").val('');
        $("#dateOfComm").val('');
        $("#address").val('');
        $("#productName").val('');
        $("#productIdTerm").val('');*/

        var policyNo = $('#policyNo').val();
        if(policyNo!=null){
            getGrpPremCollDueInfo(policyNo);
            getGrpPremCollInfo(policyNo);
        }
})
});

function dataValidation() {

    var status = true;

    if ($("#policyNo").val() == "" || $("#policyNo").val() == "0") {
        status = false;
        $("#policyNoSpan").text("Empty field found!!");
        $("#policyNo").focus();
    }
    else $("#policyNoSpan").text("");

    return status;
}
function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

function getGrpPremCollDueInfo(policyNo){

    var json = {
        "policyNo": policyNo

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getGrpPremCollDueInfo/"
        + policyNo,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            //alert(data);

            var value_0 = "";
            var value_1 = "";
            var value_2 = "";
            var value_3 = "";

            $("#tableBody").empty();
            $.each(data, function (i, l) {
                value_0 = l[0];
                value_1 = l[1];
                if(value_1){
                    value_1 = getDateShow(value_1);
                }
                value_2 = l[2];
                value_3 = l[3];

                $("#tableBody")
                    .append(
                        '<tr>'
                        + '<td><input type="text" name="collDate" id="collDate" style="width: 132px" readonly value="'+value_0+'">'
                        + '</td> '
                        + '<td><input type="text" name="collType" id="collType" style="width: 132px" readonly value="'+value_1+'">'
                        + '</td> '
                        + '<td><input type="text" name="collCat" id="collCat" style="width: 132px" readonly value="'+value_2+'">'
                        + '</td> '
                        + '<td><input type="text" name="collAmount" id="collAmount" style="width: 132px" readonly value="'+value_3+'">'
                        + '</td> '
                        + '</tr>');

            });

        },
        error: function (e) {

        }
    });
}
