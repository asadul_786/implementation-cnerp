
function bankAutoComplete() {
    var ClientBankName = "#" + "ClientBankName";
    var url = "/collection/bankAutoComplete";
    var ClientBankNameId = "#" + "ClientBankNameId";
    autocompleteBank(ClientBankName, url, "", ClientBankNameId);
}
function officeAutoComplete() {

    var collOffice = "#" + "collOffice";
    var url = "/collection/officeAutoComplete";
    var collOfficeId = "#" + "collOfficeId";
    autocomplete(collOffice, url, "", collOfficeId);
}
function autocompleteBank(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                    getBranchName(ui.item.value);
                }
            });

        }
    });
}

function getBranchName(bankCd){

    var json = {
        "bankCd": bankCd

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
        + bankCd,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('#clientBranchName').empty();
            $.each(data, function(key, value) {
                $('#clientBranchName').append('<option value="' + key + '">' + value + '</option>');
            });
        },
        error: function (e) {

        }
    });


}


function getAccInfo(accNo){

    var json = {
        "accNo": accNo
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/collection/getAccInfo/"+accNo,
        data: JSON.stringify(json),
        dataType: 'json',
        success: function (data) {

            var bank = '';
            var bankId = '';
            var branch = '';
            var branchId = '';
            var glAccNo = '';

            $('#bankName').val('');
            $('#bankId').val('');
            $('#branchName').val('');
            $('#branchNameId').val('');
            $('#glAccountNo').val('');

            $.each(data, function (i, l) {

                bank = l[2];
                bankId = l[0];
                branch = l[3];
                branchId = l[1];
                glAccNo = l[5];
            });

            $('#bankName').val(bank);
            $('#bankId').val(bankId);
            $('#branchName').val(branch);
            $('#branchNameId').val(branchId);
            $('#glAccountNo').val(glAccNo);

        },
        error: function (e) {
        }
    });
}
function getServicingOffice(policyNo){


    var json = {
        "policyNo": policyNo
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/collection/getServicingOffice/"+policyNo,
        data: JSON.stringify(json),
        dataType: 'json',
        success: function (data) {

            var serOffCd = '';
            var serOff = '';
            var regOff = '';

            $('#servOfficeId').val('');
            $('#servicingOffice').val('');
            $('#regionalOffice').val('');

            $.each(data, function (i, l) {
                serOffCd = l[0];
                serOff = l[1];
                regOff = l[3];
            });

            $('#servOfficeId').val(serOffCd);
            $('#servicingOffice').val(serOff);
            $('#regionalOffice').val(regOff);
            //getCompanyAccInfo(serOffCd);

        },
        error: function (e) {
        }
    });
}
$(document).ready(function() {
    officeAutoComplete();
    //getSurChqIssueView();
    bankAutoComplete();
    //getServicingOffice();

    $("#surrCheqIssueBtn").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var applSlNo = $('#applSlNo').val();
            if(applSlNo){
                applSlNo = applSlNo.split('/').join(' ');
            }
            var pgId = $('#pgId').val();
            var surVal = $('#surrenderValue').val();
            var netSurAmount = $('#netSurrenderAmount').val();
            var appAmount = $('#approvedAmount').val();
            var chequeNo = $('#chequeNo').val();
            var servicingOffice = $('#servOfficeId').val();
            var chequeDate = $('#chequeDate').val();
            var paymentType = $('#paymentType').val();
            var payableBonusAmount = $('#payableBonusAmount').val();
            var accountType = $('#accountType').val();
            var bankName = $('#bankName').val();
            var branchName = $('#branchName').val();
            var policyLoanAmount = $('#policyLoanAmount').val();
            var loanInterestAmount = $('#loanInterestAmount').val();
            var anfLoanAmount = $('#anfLoanAmount').val();
            var anfInterestAmount = $('#anfInterestAmount').val();
            var ClientBankNameId = $('#ClientBankNameId').val();
            var clientBranchName = $('#clientBranchName').val();
            var clientAccountNo = $('#clientAccountNo').val();
            var disbursementAmount = $('#disbursementAmount').val();
            var disAccNo = $('#accountNo').val();
            var policyNo = $('#policyNo').val();
            var glAccountNo = $('#glAccountNo').val();
            var disVoucher = $('#disVoucher').val();

            var surChqIssue = {};

            surChqIssue.applSlNo = applSlNo;
            surChqIssue.pgId = pgId;
            surChqIssue.surVal = surVal;
            surChqIssue.netSurAmount = netSurAmount;
            surChqIssue.appAmount = appAmount;
            surChqIssue.chequeNo = chequeNo;
            surChqIssue.servicingOffice = servicingOffice;
            if(chequeDate){
                chequeDate = chequeDate.split("/").reverse().join("/");
                chequeDate = getDate(chequeDate);
                surChqIssue.chequeDate = chequeDate;
            }
            surChqIssue.paymentType = paymentType;
            surChqIssue.payableBonusAmount = payableBonusAmount;
            surChqIssue.accountType = accountType;
            surChqIssue.bankName = bankName;
            surChqIssue.branchName = branchName;
            surChqIssue.policyLoanAmount = policyLoanAmount;
            surChqIssue.loanInterestAmount = loanInterestAmount;
            surChqIssue.anfLoanAmount = anfLoanAmount;
            surChqIssue.anfInterestAmount = anfInterestAmount;
            surChqIssue.ClientBankNameId = ClientBankNameId;
            surChqIssue.clientBranchName = clientBranchName;
            surChqIssue.clientAccountNo = clientAccountNo;
            surChqIssue.disbursementAmount = disbursementAmount;
            surChqIssue.disAccNo = disAccNo;
            surChqIssue.policyNo = policyNo;
            surChqIssue.glAccountNo = glAccountNo;
            surChqIssue.disVoucher = disVoucher;


            //alert(JSON.stringify(surChqIssue));

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/collection/saveSurChqIssue",
                data: JSON.stringify(surChqIssue),
                dataType: 'json',
                success: function (data) {
                    showAlert("Successfully added.");
                    var processTxt= '';
                    $.each(data, function(key, value) {
                        if(key == 1){
                            $('#disVoucher').val('')
                            $('#disVoucher').val(value)

                        }
                    });
                    if(data[3] == null){
                        $.confirm({
                            title: 'Confirmation',
                            content: data[2],
                            buttons: {
                                ok: function () {
                                }
                            }
                        });
                    }else{
                        $.confirm({
                            title: 'Confirmation',
                            content: data[3],
                            buttons: {
                                ok: function () {
                                }
                            }
                        });
                    }
                    /*if(data[12] != "success"){
                        $.confirm({
                            title: 'Confirmation',
                            content: data[12],
                            buttons: {
                                ok: function () {
                                }
                            }
                        });

                    }*/
                },
                error: function (e) {
                    showAlert("Can not save due to transaction timeout");
                }
            });
        }

    })

    function dataValidation() {
        var status = true;

        if ($("#netSurrenderAmount").val() == "" || $("#netSurrenderAmount").val() == "0" ) {
            status = false;
            $("#netSurrenderAmountSpan").text("Empty field found!!").css('color','red');
            $("#netSurrenderAmount").focus();
        } else $("#netSurrenderAmountSpan").text("");

        if ($("#approvedAmount").val() == "") {
            status = false;
            $("#approvedAmountSpan").text("Empty field found!!").css('color','red');
            $("#approvedAmount").focus();
        } else $("#approvedAmountSpan").text("");


        if ($("#disbursementAmount").val() == "" || $("#disbursementAmount").val() == "0") {
            status = false;
            $("#disbursementAmountSpan").text("Empty field found!!").css('color','red');
            $("#disbursementAmount").focus();
        }
        else $("#disbursementAmountSpan").text("");

        if ($("#policyNo").val() == "" || $("#policyNo").val() == "0") {
            status = false;
            $("#policyNoSpan").text("Empty field found!!").css('color','red');
            $("#policyNo").focus();
        }
        else $("#policyNoSpan").text("");

        return status;
    }
});

/*function getAccountInfo(accType){
    //alert(accType);

    var json = {
        "accType": accType
    };

    $.ajax({
     type: 'POST',
     contentType: 'application/json',
     url: "/collection/getListSurChqIssue/"+accType,
     data: JSON.stringify(json),
     dataType: 'json',
     success: function (data) {

     },
     error: function (e) {
     }
     });
}*/

/*
function officeAutoComplete() {

    var collOffice = "#" + "collOffice";
    var url = "/collection/officeAutoComplete";
    var collOfficeId = "#" + "collOfficeId";
    autocomplete(collOffice, url, "", collOfficeId);
}
*/

/*
function getChqForApproval(){

    //var officeId = $('#collOfficeId').val();

    var json = {
        "officeId": ''
    };

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Issue This Cheque',
        buttons: {
            ok: function () {
                document.location.href="/collection/getSurrenderChequeIssueForm";
            },
            cancel: function () {

            },
        }
    });
}
*/

function getDate(date){
    var d = new Date(date);
    return d;
}

$(function(){

    $("#policyNo").on('change', function(){

        var policyNo = $('#policyNo').val();

        if(policyNo){
            getServicingOffice(policyNo);
        }

        var json = {
            "policyNo": policyNo
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/collection/getSurChqIssueInfo/"
            + policyNo,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {

                var pgid = "";
                var applSlNo = "";
                var appAmount = "";
                var netSurAmount = "";
                var surVal = "";
                var payBonusAmount = "";
                var policyLnAmount = "";
                var policyLnInAmount = "";
                var anfLnAmount = "";
                var anfLnInAmount = "";

                $.each(data, function (i, l) {
                    pgid = l[0];
                    applSlNo = l[1];
                    appAmount = l[2];
                    netSurAmount = l[3];
                    surVal = l[4];
                    payBonusAmount = l[5];
                    policyLnAmount = l[6];
                    policyLnInAmount = l[7];
                    anfLnAmount = l[8];
                    anfLnInAmount = l[9];
                });
                if(applSlNo == ''){
                    $('#policyNo').val('');
                }
                $('#pgId').val('');
                $('#applSlNo').val('');
                $('#approvedAmount').val('');
                $('#netSurrenderAmount').val('');
                $('#surrenderValue').val('');
                $('#payableBonusAmount').val('');
                $('#policyLoanAmount').val('');
                $('#loanInterestAmount').val('');
                $('#anfLoanAmount').val('');
                $('#anfInterestAmount').val('');

                $('#pgId').val(pgid);
                $('#applSlNo').val(applSlNo);
                $('#approvedAmount').val(appAmount);
                $('#netSurrenderAmount').val(netSurAmount);
                $('#surrenderValue').val(surVal);
                $('#payableBonusAmount').val(payBonusAmount);
                $('#policyLoanAmount').val(policyLnAmount);
                $('#loanInterestAmount').val(policyLnInAmount);
                $('#anfLoanAmount').val(anfLnAmount);
                $('#anfInterestAmount').val(anfLnInAmount);
            },
            error: function (e) {
            }
        });
    })
});

/*function getSurChqIssueView(){

    var json = {
        "param": ''
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getSurChqIssueView",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var policyNo = "";
            var applSlNo = "";
            var chqDate = "";
            var accountNo = "";
            var disAmount = "";

            $("#tableBody").empty();

            $.each(data, function (i, l) {

                policyNo = l[0];
                applSlNo = l[1];
                chqDate = l[2];
                if(chqDate != null){
                    chqDate = getDateShow(chqDate);
                }
                accountNo = l[3];
                disAmount = l[4];

                $("#tableBody")
                    .append(
                        '<tr>'
                        + '<td><input type="text" name="policyNo" id="policyNo" readonly value="'+policyNo+'">'
                        + '</td> '
                        + '<td><input type="text" name="applSlNo" id="applSlNo" readonly value="'+applSlNo+'">'
                        + '</td> '
                        + '<td><input type="text" name="chqDate" id="chqDate" readonly value="'+chqDate+'">'
                        + '</td> '
                        + '<td><input type="text" name="accountNo" id="accountNo" readonly value="'+accountNo+'">'
                        + '</td> '
                        + '<td><input type="text" name="disAmount" id="disAmount" readonly value="'+disAmount+'">'
                        + '</td> '
                        + '</tr>');

            });


        },
        error: function (e) {

        }
    });
}*/

function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};