/*author:moahsin
* 10-March-2020*/

$(document).ready(function () {

    function clearForm() {
         $('#pgId').val('');
         $('#payOrderNo').val('');
         $('#glAccountNo').val('');
         $('#policyNo').val('');
         $('#servOfficeId').val('');
         $('#servOffice').val('');
         $('#clientName').val('');
         $('#totalPayAmt').val('');
         $('#chequeNo').val('');
         $('#chequeDate').val('');
         $('#accNo').val('');
         $('#bankName').val('');
        $('#bankId').val('');
        $('#branchName').val('');
        $('#branchNameId').val('');
        //$('#claimType').empty();


    }

    bankAutoComplete();
    companyBankAutoComplete();


    $("#saveBtn").click(function () {

        var currDate = $.datepicker.formatDate('dd/mm/yy', new Date());
        var flag = dataValidation();
        var claimType = $('#claimType').val();

        var chequeNo = $('#chequeNo').val();
        var bankId = $('#bankId').val();
        var branchNameId = $('#branchNameId').val();
        var accNo = $('#accNo').val();
        var glAccountNo = $('#glAccountNo').val();
        var pgId = $('#pgId').val();
        var chequeDate = $('#chequeDate').val();
        var payOrderNo = $('#payOrderNo').val();


        if(flag === true){

            var claimCommutaInfo = {};

            claimCommutaInfo.chequeNo = chequeNo;
            claimCommutaInfo.bankName = bankId;
            if (chequeDate) {
                var chequeDate = chequeDate.split("/").reverse().join("-");
                claimCommutaInfo.chequeDate = chequeDate;
            } else {
                chequeDate = currDate.split("/").reverse().join("-");
                claimCommutaInfo.chequeDate = chequeDate;

            }
            claimCommutaInfo.branchName = branchNameId;
            claimCommutaInfo.accNo = accNo;
            claimCommutaInfo.disGlNo = glAccountNo;
            claimCommutaInfo.pgId = pgId;
            claimCommutaInfo.payOrderNo = payOrderNo;

            //alert(JSON.stringify(claimCommutaInfo));

            if(claimType == '2'){

                $.confirm({
                    title: 'Confirm',
                    content: 'Are You Sure To Issue This Record',
                    buttons: {
                        ok: function () {

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/collection/updatePenCommClaimChq",
                    data: JSON.stringify(claimCommutaInfo),
                    dataType: 'json',
                    success: function (data) {
                        if(data === 1){
                            showAlert("Successfully Issued");
                            clearForm();
                        }else{
                            showAlert("Transaction Already Updated!!");
                        }
                    },
                    error: function (e) {
                        showAlert("Transaction Already Updated!!");
                    }
                });
                        },
                        cancel: function () {

                        }
                    }
                        });
            }else{

                $.confirm({
                    title: 'Confirm',
                    content: 'Are You Sure To Issue This Record',
                    buttons: {
                        ok: function () {

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/collection/updateMaturityDeathClaimChq",
                    data: JSON.stringify(claimCommutaInfo),
                    dataType: 'json',
                    success: function (data) {
                        if(data === 1){
                            showAlert("Successfully Issued");
                            clearForm();
                        }else{
                            showAlert("Transaction Already Updated!!");
                        }
                    },
                    error: function (e) {
                        showAlert("Transaction 2nd Already Updated!!");
                    }
                });
            },
            cancel: function () {

            }
        }
    });
            }
        }
    });
});

function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

$(function(){
    $("#policyNo").on('change', function(){

        var claimType = $('#claimType').val();
        var policyNo = $('#policyNo').val();

        var json = {
            "policyNo": policyNo
        };


        if(claimType == '1'){

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/collection/getMatDeathClaimInfo/"
                    + policyNo,
                data: JSON
                    .stringify(json),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (data) {

                    var payOrderNo = "";
                    var clientName = "";
                    //var clientType = "";
                    var totalPayAmt = "";
                    var pgId = "";
                    var servOffice = "";
                    var servOfficeId = "";

                    $('#payOrderNo').val('');
                    $('#clientName').val('');
                    //$('#clientType').val('');
                    $('#totalPayAmt').val('');
                    $('#pgId').val('');
                    $('#servOffice').val('');
                    $('#servOfficeId').val('');

                    $.each(data, function (i, l) {

                        payOrderNo = l[3];
                        clientName = l[5];
                        //clientType = l[0];
                        totalPayAmt = l[4];
                        pgId = l[0];
                        servOffice = l[1];
                        servOfficeId = l[2];

                    });

                    $('#payOrderNo').val(payOrderNo);
                    $('#clientName').val(clientName);
                    //$('#clientType').val(clientType);
                    if(totalPayAmt){
                        $('#totalPayAmt').val(totalPayAmt);
                    }else{
                        $('#totalPayAmt').val(0);
                    }
                    $('#pgId').val(pgId);
                    $('#servOffice').val(servOffice);
                    $('#servOfficeId').val(servOfficeId);


                },
                error: function (e) {
                }
            });

        }else{

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/collection/getPenCommutationClaimInfo/"
                    + policyNo,
                data: JSON
                    .stringify(json),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (data) {

                    var pgId = "";
                    var clientName = "";
                    var totalPayAmt = "";
                    var servOffice = "";
                    var servOfficeId = "";

                    $('#pgId').val('');
                    $('#clientName').val('');
                    $('#totalPayAmt').val('');
                    $('#servOffice').val('');
                    $('#servOfficeId').val('');

                    $.each(data, function (i, l) {

                        pgId = l[0];
                        clientName = l[2];
                        totalPayAmt = l[6];
                        servOffice = l[4];
                        servOfficeId = l[5];

                    });

                    $('#pgId').val(pgId);
                    $('#clientName').val(clientName);
                    if(totalPayAmt){
                        $('#totalPayAmt').val(totalPayAmt);
                    }else{
                        $('#totalPayAmt').val(0);
                    }
                    $('#servOffice').val(servOffice);
                    $('#servOfficeId').val(servOfficeId);


                },
                error: function (e) {
                }
            });

        }
    })

});

function dataValidation() {
    var status = true;

    if ($("#claimType").val() == "") {
        status = false;
        $("#claimTypeSpan").text("Empty field found!!");
        $("#claimType").focus();
    } else $("#claimTypeSpan").text("");


    if ($("#policyNo").val() == "" || $("#policyNo").val() == "0") {
        status = false;
        $("#policyNoSpan").text("Empty field found!!");
        $("#policyNo").focus();
    }
    else $("#policyNoSpan").text("");

    return status;
}
function getServicingOffice(policyNo){


    var json = {
        "policyNo": policyNo
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/collection/getServicingOffice/"+policyNo,
        data: JSON.stringify(json),
        dataType: 'json',
        success: function (data) {

            var serOffCd = '';
            var serOff = '';
            var regOff = '';

            $('#servOfficeId').val('');
            $('#servOffice').val('');

            $.each(data, function (i, l) {
                serOffCd = l[0];
                serOff = l[1];
            });

            $('#servOfficeId').val(serOffCd);
            $('#servOffice').val(serOff);

        },
        error: function (e) {
        }
    });
}


function bankAutoComplete() {
    var ClientBankName = "#" + "clientBankName";
    var url = "/collection/bankAutoComplete";
    var ClientBankNameId = "#" + "ClientBankNameId";
    autocompleteBank(ClientBankName, url, "", ClientBankNameId);
}

function companyBankAutoComplete() {
    var ClientBankName = "#" + "bankName";
    var url = "/collection/bankAutoComplete";
    var ClientBankNameId = "#" + "bankId";
    autocompleteComBank(ClientBankName, url, "", ClientBankNameId);
}
function autocompleteBank(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                    getBranchName(ui.item.value);
                }
            });

        }
    });
}

function autocompleteComBank(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                    getComBranchName(ui.item.value);
                }
            });

        }
    });
}

function getBranchName(bankCd){

    var json = {
        "bankCd": bankCd

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
            + bankCd,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('#clientBranchName').empty();
            $.each(data, function(key, value) {
                $('#clientBranchName').append('<option value="' + key + '">' + value + '</option>');
            });
        },
        error: function (e) {

        }
    });


}

function getComBranchName(bankCd){

    var json = {
        "bankCd": bankCd

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
            + bankCd,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('#branchName').empty();
            $.each(data, function(key, value) {
                $('#branchName').append('<option value="' + key + '">' + value + '</option>');
            });
        },
        error: function (e) {

        }
    });


}

function getPaymentDisAccInfo(accNo){

    var json = {
        "accNo": accNo
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/collection/getPaymentDisAccInfo/"+accNo,
        data: JSON.stringify(json),
        dataType: 'json',
        success: function (data) {

            var bank = '';
            var bankId = '';
            var branch = '';
            var branchId = '';
            var glAccNo = '';

            $('#bankName').val('');
            $('#bankId').val('');
            $('#branchName').val('');
            $('#branchNameId').val('');
            $('#glAccountNo').val('');

            $.each(data, function (i, l) {

                bank = l[2];
                bankId = l[0];
                branch = l[3];
                branchId = l[1];
                glAccNo = l[5];
            });

            $('#bankName').val(bank);
            $('#bankId').val(bankId);
            $('#branchName').val(branch);
            $('#branchNameId').val(branchId);
            $('#glAccountNo').val(glAccNo);

        },
        error: function (e) {
        }
    });
}