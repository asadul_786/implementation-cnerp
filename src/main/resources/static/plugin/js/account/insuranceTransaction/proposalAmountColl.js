
function bankAutoComplete() {
    var bank = "#" + "bankName";
    var url = "/collection/bankAutoComplete";
    var bankId = "#" + "bankIdTest";
    autocomplete(bank, url, "", bankId);
}
function autocomplete(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                }
            });

        }
    });
}

function getBranchName(bankCd){

    var json = {
        "bankCd": bankCd

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
        + bankCd,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('#branchName').empty();
            $.each(data, function(key, value) {
                $('#branchName').append('<option value="' + key + '">' + value + '</option>');
            });
        },
        error: function (e) {

        }
    });


}

function setCollectionOfficeByEmplId(){

    var json = {
        "officeId": ''

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getCollectionOfficeByEmplId",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var value_1 = "";
            var value_2 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
                value_2 = l[1];
            });

            $("#collectionOffice").val(value_1);
            $("#collectionOfficeId").val(value_2);


        },
        error: function (e) {

        }
    });

}

function setPolicyOfficeByPgId(){

    var pgId = $('#pgid').val();

    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getPolicyOfficeByPgId/"
        + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var value_1 = "";
            var value_2 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
                value_2 = l[1];
            });
            $("#policyOffice").val(value_1);
            $("#policyOfficeId").val(value_2);


        },
        error: function (e) {

        }
    });

}

function setProposarNameByPgId(){

    var pgId = $('#pgid').val();

    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getAssuredNmByPgId/"
        + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $("#assuredName").val(data);
        },
        error: function (e) {

        }
    });

}

function getLastRowkey(){

    var json = {
        "officeId": ""
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getLastRowkeyInsPProposal",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $("#rowKeyLast").val(data[0]);
        },
        error: function (e) {
        }
    });
}

function getLastRecvid(){

    var json = {
        "officeId": ""
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getLastReceiveIdAccAdvRecv",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var value_1 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
            });

            $("#recvIdLastIndex").val(value_1);
        },
        error: function (e) {

        }
    });
}

function getDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}
function getNextLastPgIdInsPProposal(pgIdStr){

    var json = {
        "pgIdStr": pgIdStr

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getLastPgIdNextInsPProposal/"
        + pgIdStr,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

        },
        error: function (e) {
        }
    });

}
function getLastPgIdInsPProposal(){

    var json = {
        "officeId": ""
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getLastPgIdInsPProposal",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var pgidStr = data[0];
            getNextLastPgIdInsPProposal(pgidStr);

        },
        error: function (e) {

        }
    });
}

function setProposalNoByPgId(){

    var pgId = $('#pgId').val();

    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getProposalNoByPgId/"
        + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

        },
        error: function (e) {
        }
    });

}
function getRecvIdProposalNo(recvId){

    var json = {
        "recvId": recvId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getRecvIdProposalNo/"
        + recvId,
        data: JSON.stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            if(data){
                window.open('/insTranReport/proposalAmntCollReports?receiveId='+data[0][0]+"&proposalNumber="+data[0][1], '_blank');
            }
        },
        error: function (e) {
        }
    });
}
$(document).ready(function () {

    var premAmount = $('#premAmount').val();
    var dueAmount = $('#dueAmount').val();
    var totalCollAmount = $('#totalCollAmount').val();

    $('#premAmnt').val('');
    $('#dueAmnt').val('');
    $('#totalCollAmnt').val('');

    $('#premAmnt').val(premAmount);
    $('#dueAmnt').val(dueAmount);
    $('#totalCollAmnt').val(totalCollAmount);

    var regexname=  /^[0-9]+\.?[0-9]*$/;


    $('#totalAmnt').on('keypress keydown keyup blur focus',function(){

        if (!$(this).val().match(regexname)) {
            $('#totalAmntSpan').show().text("Only Positive Number!!");
            $('#totalAmnt').val('');
            $('#advanceCollForm').attr('onsubmit','return false;');
        }
        else{
            $('#totalAmntSpan').show().text("");
            $('#advanceCollForm').attr('onsubmit','return true;');
        }
    });

    setProposarNameByPgId();
    setPolicyOfficeByPgId();
    getLastPgIdInsPProposal();
    setCollectionOfficeByEmplId();
    getLastRowkey();
    getLastRecvid();
    $('#collectionMedia').prop('disabled', true);
    $('#collectionMedia').append(
        '<option value="' + "OL" + '">' + "Not Applicable" + '</option>'
    );
    $('#proposalNumber').prop('disabled', true);
    $('#mediaNo').prop('disabled', true);
    $('#mediaDate').prop('disabled', true);
    $('#bankName').prop('disabled', true);
    $('#branchName').prop('disabled', true);
    $('#checkNo').prop('disabled', true);
    $('#checkDate').prop('disabled', true);
    $('#receiveDate').prop('disabled', true);
    $('#receiveDate').val($.datepicker.formatDate('dd/mm/yy', new Date()));

    $('#collCategory').change(function (e) {
        var selectedValue = $(this).val();


        if(selectedValue == 0) {
            return;
        }

        if(selectedValue == '1') {
            $('#collectionMedia').empty();
            $('#collectionMedia').prop('disabled', true);
            $('#collectionMedia').append(
                '<option value="' + "OL" + '">' + "Not Applicable" + '</option>'
            );

            $('#mediaNo').prop('disabled', true);
            $('#mediaDate').prop('disabled', true);

        }
        if(selectedValue == '2') {
            $("#collectionMedia option[value='0']").remove();
            $('#collectionMedia').prop('disabled', false);
            $('#collectionMedia').append(
                '<option value="' + "PR" + '">' + "PR" + '</option>'
                +'<option value="' + "BM" + '">' + "BM" + '</option>'
                +'<option value="' + "JV" + '">' + "JV" + '</option>'
                +'<option value="' + "MR" + '">' + "MR" + '</option>'
                +'<option value="' + "OL" + '">' + "Not Applicable" + '</option>'
            );

            $('#mediaNo').prop('disabled', false);
            $('#mediaDate').prop('disabled', false);
        }



    });

    $('#collectionType').change(function (e) {

        var selectedValue = $(this).val();


        if(selectedValue == 0) {
            return;
        }

        if(selectedValue == '1') {
            $('#checkNo').prop('disabled', true);
            $('#checkDate').prop('disabled', true);
            $('#bankName').prop('disabled', true);
            $('#branchName').prop('disabled', true);
        }else{
            $('#bankName').prop('disabled', false);
            $('#branchName').prop('disabled', false);
            $('#checkNo').prop('disabled', false);
            $('#checkDate').prop('disabled', false);
        }


    });
    $("#refresh_button").click(function () {
        clearform();
    });

    $("#add_button").click(function () {

        var currDate = $.datepicker.formatDate('dd/mm/yy', new Date());
        var flag = dataValidation();

        if(flag === true){

        var v_late_fee = '0';
        var v_off_cd = $('#policyOfficeId').val();
        var activeOfficeCd = $('#policyOfficeId').val();
        var proposalNumber = $('#proposalNumberF').val();
        var receiveDateStr = $('#receiveDate').val();
        var collectionCat = $('#collCategory').val();
        var colAmnt = $('#totalAmnt').val();
        var chq_no = $('#checkNo').val();
        var checkDateStr = $('#checkDate').val();
        var officeCode = $('#collectionOfficeId').val();
        var receiveMode = $('#collectionType').val();
        var recvIdLastIndex = $('#recvIdLastIndex').val();
        var receiveId = $('#receiveId').val();
        var pgid = $('#pgid').val();
        var partyName = $('#assuredName').val();
        var collectionMedia = $('#collectionMedia').val();
        var collSlipNo = $('#mediaNo').val();
        var collSlipDt = $('#mediaDate').val();
        var propoNo = $('#propoNo').val();
        var activeOffNm = $('#collectionOffice').val();
        var bankName = $('#bankName').val();
        var branchName = $('#branchName').val();






        var advanceReceivables = {};

        advanceReceivables.v_off_cd = v_off_cd;
        advanceReceivables.pgid = pgid;
        if(propoNo){
            advanceReceivables.proposalNumber = propoNo;
        }else{
            advanceReceivables.proposalNumber = proposalNumber;
        }
        if (receiveDateStr) {
            var receiveDate = receiveDateStr;
            advanceReceivables.receiveDate = receiveDate;
        } else {
            var receiveDateCurr = currDate;
            advanceReceivables.receiveDate = receiveDateCurr;

        }
        advanceReceivables.collectionCat = collectionCat;
        advanceReceivables.collAmount = colAmnt;
        advanceReceivables.chqNo = chq_no;
        if (checkDateStr) {
            var checkDateStr = checkDateStr.split("/").reverse().join("-");
            advanceReceivables.chqDt = checkDateStr;
        }else {
            advanceReceivables.chqDt = null;

        }
        if (collSlipDt) {
            var collSlipDtStr = collSlipDt.split("/").reverse().join("-");
            advanceReceivables.collSlipDt = collSlipDtStr;
        } else {
            var collSlipDtCurrStr = currDate.split("/").reverse().join("-");
            advanceReceivables.collSlipDt = collSlipDtCurrStr;

        }
        advanceReceivables.officeCode = officeCode;
        advanceReceivables.activeOfficeCd = activeOfficeCd;
        advanceReceivables.collMedia = collectionMedia;
        if(receiveId){
            advanceReceivables.recvId = receiveId;
        }else{
            advanceReceivables.recvId = recvIdLastIndex;
        }
        advanceReceivables.partyName = partyName;
        advanceReceivables.receiveMode = receiveMode;
        advanceReceivables.collSlipNo = collSlipNo;
        advanceReceivables.activeOffNm = activeOffNm;
        advanceReceivables.bankName = bankName;
        advanceReceivables.branchName = branchName;

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/collection/saveAdvanceRecvAndPrc",
            data: JSON.stringify(advanceReceivables),
            dataType: 'json',
            success: function (data) {

                $.each(data, function(key, value) {

                });
                /*if(data[1]){

                    $.confirm({
                        title: 'Message',
                        content: data[1],
                        buttons: {
                            ok: function () {
                            }
                        }
                    });

                }*/
                /*if(data[2]){
                    $.confirm({
                        title: 'Message',
                        content: data[2],
                        buttons: {
                            ok: function () {
                            }
                        }
                    });
                }*/

                if(data[3] == ''){
                    showAlert("Bank Account for Premium deposit of Active Office- Not Found has not set yet.");
                }else{
                    showAlert("Successfully added.");
                    getRecvIdProposalNo(data[4]);
                    clearform();
                }
            },
            error: function (e) {
                showAlert("Can't save due to Transaction Time Out!!");
            }
        });
        }
    });

    function clearform() {
        $('#pgid').val("");
        $('#activeOfficeCd').val("");
        $('#collectionOffice').val("");
        $('#proposalNumber').val("");
        $('#collCategory').val("");
        $('#collectionType').val("");
        $('#collectionMedia').val("");
        $('#mediaNo').val("");
        $('#mediaDate').val("");
        $('#policyOffice').val("");
        $('#assuredName').val("");
        $('#bankName').val("");
        $('#branchName').val("");
        $('#checkNo').val("");
        $('#checkDate').val("");
        $('#receiveDate').val("");
        $('#totalAmnt').val("");
        $('#recvIdLastIndex').val("");

    }

    var showAlert = function (alertContent) {
        new PNotify({
            title: 'Info',
            text: alertContent,
            type: 'info',
            styling: 'bootstrap3'
        });
        setTimeout(alertContent, 2000);
    }

    $("#refresh_button").click(function (event) {
        clearform();

    });

    function dataValidation() {
        var status = true;

        if ($("#policyOffice").val() == "" || $("#policyOffice").val() == "0" ) {
            status = false;
            $("#policyOfficeSpan").text("Empty field found!!");
            $("#policyOffice").focus();
        } else $("#policyOfficeSpan").text("");

        if ($("#collectionOffice").val() == "") {
            status = false;
            $("#collectionOfficeSpan").text("Empty field found!!");
            $("#collectionOffice").focus();
        } else $("#collectionOfficeSpan").text("");


        if ($("#collectionType").val() == "" || $("#collectionType").val() == "0") {
            status = false;
            $("#collectionTypeSpan").text("Empty field found!!");
            $("#collectionType").focus();
        }
        else $("#collectionTypeSpan").text("");

        /*if ($("#proposalNumber").val() == "" || $("#proposalNumber").val() == "0") {
            status = false;
            $("#proposalNumberSpan").text("Empty field found!!");
            $("#proposalNumber").focus();
        }
        else $("#proposalNumberSpan").text("");*/

        if ($("#assuredName").val() == "" || $("#assuredName").val() == "0") {
            status = false;
            $("#assuredNameSpan").text("Empty field found!!");
            $("#assuredName").focus();
        }
        else $("#assuredNameSpan").text("");

        if ($("#receiveDate").val() == "" || $("#receiveDate").val() == "0") {
            status = false;
            $("#receiveDateSpan").text("Empty field found!!");
            $("#receiveDate").focus();
        }
        else $("#receiveDateSpan").text("");

        if ($("#totalAmnt").val() == "" || $("#totalAmnt").val() == "0") {
            status = false;
            $("#totalAmntSpan").text("Empty field found!!");
            $("#totalAmnt").focus();
        }
        else $("#totalAmntSpan").text("");

        var str_sms = $('#proMobile').val();

        return status;
    }


});
if (document.readyState == 'complete'){
    alert($("#collectionOfficeId").val());
}
/*
$(window).load(function(){

});*/
