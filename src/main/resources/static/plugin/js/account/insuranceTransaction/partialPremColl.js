function getBranchName(bankCd){

    var json = {
        "bankCd": bankCd

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
        + bankCd,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#branchName').empty();
            $.each(data, function(key, value) {

                $('#branchName').append('<option value="' + key + '">' + value + '</option>');

            });

        },
        error: function (e) {

        }
    });


}
function setCollectionOfficeByEmplId(){

    var json = {
        "officeId": ''

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getCollectionOfficeByEmplId",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var value_1 = "";
            var value_2 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
                value_2 = l[1];
            });

            $("#collectionOffice").val(value_1);
            $("#collectionOfficeId").val(value_2);


        },
        error: function (e) {

        }
    });

}
function getLastRecvidPartialColl(){

    var json = {
        "officeId": ""
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getLastRecvIdPartialColl",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $("#receiveId").val('');
            $("#receiveId").val(data[0]);
        },
        error: function (e) {

        }
    });
}
function getDate(dateObject) {
    var d = new Date(dateObject);

    return d;
};

$(function(){

    $("#policyNo").on('change', function(){

        var policyNo = $('#policyNo').val();

        var json = {
            "policyNo": policyNo
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/collection/getPgidByPolicyNo/"
            + policyNo,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {

                var pgid = "";
                var policyOffice = "";
                var policyOfficeId = "";
                var assuredName = "";
                var proposalNumber = "";

                $("#pgid").val('');

                $("#policyOffice").val('');
                $("#policyOfficeId").val('');

                $("#assuredName").val('');

                $("#proposalNumber").val('');

                $.each(data, function (i, l) {

                    pgid = l[0];
                    policyOffice = l[1];
                    policyOfficeId = l[2];
                    assuredName = l[3];
                    proposalNumber = l[4];
                });
                $("#pgid").val(pgid);
                $("#policyOffice").val(policyOffice);
                $("#policyOfficeId").val(policyOfficeId);
                $("#assuredName").val(assuredName);
                $("#proposalNumber").val(proposalNumber);
            },
            error: function (e) {
            }
        });
    })
});

$(document).ready(function () {
    setCollectionOfficeByEmplId();
    $("#refresh_button").click(function () {
        clearform();
    });

    getLastRecvidPartialColl();

    $('#collectionMedia').prop('disabled', true);
    $('#collectionMedia').append(
        '<option value="' + "0" + '">' + "Not Applicable" + '</option>'
    );

    $('#collCategory').change(function (e) {
        var selectedValue = $(this).val();


        if(selectedValue == 0) {
            return;
        }

        if(selectedValue == '1') {
            $('#collectionMedia').empty();
            $('#collectionMedia').prop('disabled', true);
            $('#collectionMedia').append(
                '<option value="' + "0" + '">' + "Not Applicable" + '</option>'
            );

            $('#mediaNo').prop('disabled', true);
            $('#mediaDate').prop('disabled', true);

        }
        if(selectedValue == '2') {
            $("#collectionMedia option[value='0']").remove();
            $('#collectionMedia').prop('disabled', false);
            $('#collectionMedia').append(
                '<option value="' + "1" + '">' + "PR" + '</option>'
                +'<option value="' + "2" + '">' + "BM" + '</option>'
                +'<option value="' + "3" + '">' + "JV" + '</option>'
                +'<option value="' + "4" + '">' + "MR" + '</option>'
                +'<option value="' + "0" + '">' + "Not Applicable" + '</option>'
            );

            $('#mediaNo').prop('disabled', false);
            $('#mediaDate').prop('disabled', false);
        }



    });

    $('#collectionType').change(function (e) {

        var selectedValue = $(this).val();


        if(selectedValue == 0) {
            return;
        }

        if(selectedValue == '1') {
            $('#checkNo').prop('disabled', true);
            $('#checkDate').prop('disabled', true);
            $('#bankName').prop('disabled', true);
            $('#branchName').prop('disabled', true);
        }else{
            $('#bankName').prop('disabled', false);
            $('#branchName').prop('disabled', false);
            $('#checkNo').prop('disabled', false);
            $('#checkDate').prop('disabled', false);
        }


    });

    function dataValidation() {
        var status = true;

        if ($("#collAmnt").val() == "" || $("#collAmnt").val() == "0") {
            status = false;
            $("#collAmntSpan").text("Empty field found!!").css("color","red");
            $("#collAmnt").focus();
        }
        else $("#collAmntSpan").text("");

        if ($("#collectionDate").val() == "" || $("#collectionDate").val() == "0") {
            status = false;
            $("#collectionDateSpan").text("Empty field found!!").css("color","red");
            $("#collectionDate").focus();
        }
        else $("#collectionDateSpan").text("");

        if ($("#policyNo").val() == "" || $("#policyNo").val() == "0") {
            status = false;
            $("#policyNoSpan").text("Empty field found!!").css("color","red");
            $("#policyNo").focus();
        }
        else $("#policyNoSpan").text("");

        return status;
    }

    $("#partialPremiumAdd_button").click(function () {

        var currDate = new Date();
        var flag = dataValidation();

        if (flag == true) {

            var receiveId = $('#receiveId').val();
            var collAmnt = $('#collAmnt').val();
            var pgid = $('#pgid').val();
            var collectionOfficeId = $('#collectionOfficeId').val();
            var policyOfficeId = $('#policyOfficeId').val();
            var collectionType = $('#collectionType').val();
            var collCategory = $('#collCategory').val();
            var assuredName = $('#assuredName').val();
            var collectionMedia = $('#collectionMedia').val();
            var mediaNo = $('#mediaNo').val();
            var mediaDate = $('#mediaDate').val();
            var bankName = $('#bankName').val();
            var branchName = $('#branchName').val();
            var checkNo = $('#checkNo').val();
            var checkDate = $('#checkDate').val();
            var collectionDate = $('#collectionDate').val();

            var accPartialReceivables = {};

            accPartialReceivables.receiveId = receiveId;
            accPartialReceivables.collAmnt = collAmnt;
            if(collectionDate){
                collectionDate = collectionDate.split("/").reverse().join("/");
                collectionDate = getDate(collectionDate);
                accPartialReceivables.receiveDate = collectionDate;
            }else{
                accPartialReceivables.receiveDate = currDate;
            }
            if(mediaDate){
                mediaDate = mediaDate.split("/").reverse().join("/");
                mediaDate = getDate(mediaDate);
                accPartialReceivables.mediaDate = mediaDate;
            }else{
                accPartialReceivables.mediaDate = currDate;
            }
            if(checkDate){
                checkDate = checkDate.split("/").reverse().join("/");
                checkDate = getDate(checkDate);
                accPartialReceivables.chqDate = checkDate;
            }else{
                accPartialReceivables.chqDate = currDate;
            }
            accPartialReceivables.collectionOfficeId = collectionOfficeId;
            accPartialReceivables.policyOfficeId = policyOfficeId;
            accPartialReceivables.collectionType = collectionType;
            accPartialReceivables.collCat = collCategory;
            accPartialReceivables.assuredName = assuredName;
            accPartialReceivables.collectionMedia = collectionMedia;
            accPartialReceivables.pgid = pgid;
            accPartialReceivables.mediaNo = mediaNo;
            accPartialReceivables.bankCd = bankName;
            accPartialReceivables.brnCd = branchName;
            accPartialReceivables.chqNo = checkNo;

           // https://reqres.in/api/users
            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/collection/saveAccPartialCollection",
                data: JSON.stringify(accPartialReceivables),
                dataType: 'json',
                beforeSend: function(){
                    //$("#loading").show();
                },
                complete: function () {
                    //$("#loading").hide();
                },
                success: function (data) {
                    //$("#loader").hide();
                    showAlert("Successfully added.");
                    window.open('/collection/generatePartialPremiumCollRec.pdf?receiveId=' +receiveId, '_blank');
                    clearform();
                },
                error: function (e) {
                    //$("#loader").hide();
                    showAlert("Can't save collection Amount due to Data Mismatch!!");
                }
            });

        }

    });
    var showAlert = function (alertContent) {
        new PNotify({
            title: 'Info',
            text: alertContent,
            type: 'success',
            styling: 'bootstrap3',
        });
        setTimeout(alertContent, 100);
    }

});
function clearform() {
    $('#collectionDate').val("");
    $('#collectionOffice').val("");
    $('#collectionOfficeId').val("");
    $('#policyOffice').val("");
    $('#policyOfficeId').val("");
    $('#collectionOfficeId').val("");
    $('#collCategory').val("");
    $('#collectionType').val("");
    $('#assuredName').val("");
    $('#proposalNumber').val("");
    $('#collectionMedia').val("");
    $('#mediaNo').val("");
    $('#mediaDate').val("");
    $('#bankName').val("");
    $('#branchName').val("");
    $('#checkNo').val("");
    $('#checkDate').val("");
    $('#collAmnt').val("");
    $('#pgid').val("");
    $('#receiveId').val("");

}