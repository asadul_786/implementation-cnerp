$(document).ready(function() {
    officeAutoComplete();

    $('#descendentOffice').on('change', function() {
        $('#descOfficeVal').val(this.checked ? this.value : '');
    });

        $("#policyScheduleReport").click(function () {
            var collOfficeId = $('#collOfficeId').val();
            var descOfficeVal = $('#descOfficeVal').val();
            if(collOfficeId || descOfficeVal){
                window.open('/collection/generatePolicyScheduleReport.pdf?StrArray=' +collOfficeId+descOfficeVal, '_blank');
            }else{
                alert("please select office or descendent office!!");
            }
        });
});

function officeAutoComplete() {

    var collOffice = "#" + "collOffice";
    var url = "/collection/officeAutoComplete";
    var collOfficeId = "#" + "collOfficeId";
    autocomplete(collOffice, url, "", collOfficeId);
}