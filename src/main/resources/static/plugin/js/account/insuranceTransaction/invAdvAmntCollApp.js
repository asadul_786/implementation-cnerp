/*author : Mohasin
* time: 08/March/2020*/
$(document).ready(function() {
    //officeAutoComplete();
});

function dataValid() {
    var status = true;

    if ($("#proposalNo").val() == "" || $("#proposalNo").val() == "0") {
        status = false;
    }
    if ($("#assuredName").val() == "" || $("#assuredName").val() == "0") {
        status = false;
    }

    return status;
}

function proposalValid() {
    var status = true;

    if ($("#proposalNo").val() == "" || $("#proposalNo").val() == "0") {
        status = false;
    }

    return status;
}

function assNameValid() {
    var status = true;

    if ($("#assuredName").val() == "" || $("#assuredName").val() == "0") {
        status = false;
    }
    return status;
}
function getInvAdvAmntCollAppList(){

    $('#invAdvAmntCollApp').attr('onsubmit','return false;');

    var allValidation = dataValid();
    var proposalValidation = proposalValid();
    var assNameValidation = assNameValid();

        if(allValidation === true){
            //alert("all");
            $( "#allField" ).val("");
            $( "#allField" ).val("all");
            $('#invAdvAmntCollApp').attr('onsubmit','return true;');
        }else{
            if(proposalValidation === true){
                //alert("proposalField");
                $( "#proposalField" ).val("");
                $( "#proposalField" ).val("proposal");
                $('#invAdvAmntCollApp').attr('onsubmit','return true;');
            }else if(assNameValidation == true){
                //alert("assNameValidation");
                $( "#nameField" ).val("");
                $( "#nameField" ).val("assName");
                $('#invAdvAmntCollApp').attr('onsubmit','return true;');
            }else{
                $('#invAdvAmntCollApp').attr('onsubmit','return true;');
            }
        }
}

/*function createInvalidAdvAmntColl(recvId){

    var json = {
        "recvId": recvId
    };
    //alert(JSON.stringify(json));

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Prepare This Record For Invalid',
        buttons: {
            ok: function () {
                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/collection/saveInvalidAdvAmntColl/"+recvId,
                    data: JSON.stringify(json),
                    dataType: 'json',
                    success: function (data) {
                        alert(data);
                        if(data){
                            showAlert("Successfully Saved.");
                        }else{
                            showAlert("Can't Save Due To Data MisMatch!!");
                        }
                    },
                    error: function (e) {
                        showAlert("Can't Save Due To Data MisMatch!!");
                    }
                });
            },
            cancel: function () {

            },
        }
    });
}*/

/*
function checkInvalidAdvColl(recvId){

    var json = {
        "recvId": recvId
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/collection/checkInvalidAdvColl/"+recvId,
        data: JSON.stringify(json),
        dataType: 'json',
        success: function (data) {
            alert(data);
            if(data){
                showAlert("This record already Applied For Invalid");
            }else{
                showAlert("This record already Applied For Invalid");
            }
        },
        error: function (e) {
            showAlert("This record already Applied For Invalid");
        }
    });
}*/
