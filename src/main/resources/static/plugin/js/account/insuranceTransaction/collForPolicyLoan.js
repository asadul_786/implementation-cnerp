/*author:moahsin
* 10-March-2020*/

var collDate = '<tr>'
    + '<td><label name="collDate" id="collDate"><h5>Collection Date</h5></label>'
    + '</td> '
    + '<td><input type="text" name="collDate" id="collDate" style="margin-left: 37%;width: 173%;height: 35px;" value="'+'">'
    + '<span class="err_msg" id="collDateSpan">'
    + '</td> '
    + '</tr>';
var adjustmentDate = '<tr>'
    + '<td><label name="adjustmentDate" id="adjustmentDate"><h5>Adjustment Date</h5></label>'
    + '</td> '
    + '<td><input type="text" name="adjustmentDate" id="adjustmentDate" style="margin-left: 33%;width: 173%;height: 35px;" value="'+'">'
    + '<span class="err_msg" id="adjustmentDateSpan">'
    + '</td> '
    + '</tr>';
$(document).ready(function () {
    setCollOfficeByEmplId();

    $("#collInfoBody").empty();
    $("#collInfoBody")
        .append(collDate);

    $('#collCatgory').change(function (e) {

        var selectedValue = $(this).val();

        if(selectedValue == 0) {
            return;
        }
        if(selectedValue == '1') {
            $('#collMedia').empty();
            $('#collMedia').prop('disabled', true);
            $('#collMedia').append(
                '<option value="' + "0" + '">' + "Not Applicable" + '</option>'
            );

            $('#mediaNo').prop('disabled', true);
            $('#mediaDate').prop('disabled', true);

            $("#collInfoBody").empty();
            $("#collInfoBody")
                .append(collDate);

        }
        if(selectedValue == '2') {
            $("#collMedia option[value='0']").remove();
            $('#collMedia').prop('disabled', false);
            $('#collMedia').append(
                '<option value="' + "1" + '">' + "PR" + '</option>'
                +'<option value="' + "2" + '">' + "BM" + '</option>'
                +'<option value="' + "3" + '">' + "JV" + '</option>'
                +'<option value="' + "4" + '">' + "MR" + '</option>'
                +'<option value="' + "0" + '">' + "Not Applicable" + '</option>'
            );

            $('#mediaNo').prop('disabled', false);
            $('#mediaDate').prop('disabled', false);

            $("#collInfoBody").empty();
            $("#collInfoBody")
                .append(adjustmentDate);
        }
    });
});


function setCollOfficeByEmplId(){

    var json = {
        "officeId": ''

    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getCollectionOfficeByEmplId",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var value_1 = "";
            var value_2 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
                value_2 = l[1];
            });
            $("#collOffice").val(value_1);
            $("#collOfficeId").val(value_2);
        },
        error: function (e) {
        }
    });

}
function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

$(function(){
    $("#policyNo").on('change', function(){

        var policyNo = $('#policyNo').val();

        var json = {
            "policyNo": policyNo
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/collection/getLoanSceduleInfo/"
            + policyNo,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {

                var installmentNo = "";
                var installmentDate = "";
                var installmentAmount = "";
                var principleRecvAmount = "";
                var collAmount = "";
                var duePrinAmount = "";
                var dueInterestAmount = "";
                var totalDueAmount = "";
                var paymentStatus = "";

                $("#tableBody").empty();

                $.each(data, function (i, l) {
                    installmentNo = l[0];
                    installmentDate = l[2];
                    if(installmentDate != null){
                        installmentDate = getDateShow(installmentDate);
                    }
                    installmentAmount = l[6];
                    principleRecvAmount = l[10];
                    collAmount = l[8];
                    duePrinAmount = l[11];
                    dueInterestAmount = l[12];
                    totalDueAmount = l[9];
                    paymentStatus = l[7];

                    $("#tableBody")
                        .append(
                            '<tr>'
                            + '<td><input type="text" name="installmentNo" id="installmentNo" style="width: 70px" readonly value="'+installmentNo+'">'
                            + '</td> '
                            + '<td><input type="text" name="installmentDate" id="installmentDate" style="width: 74px" readonly value="'+installmentDate+'">'
                            + '</td> '
                            + '<td><input type="text" name="installmentAmount" id="installmentAmount" style="width: 92px" readonly value="'+installmentAmount+'">'
                            + '</td> '
                            + '<td><input type="text" name="principleRecvAmount" id="principleRecvAmount" style="width: 103px" readonly value="'+principleRecvAmount+'">'
                            + '</td> '
                            + '<td><input type="text" name="collAmount" id="collAmount" style="width: 84px" readonly value="'+collAmount+'">'
                            + '</td> '
                            + '<td><input type="text" name="duePrinAmount" id="duePrinAmount" style="width: 104px" readonly value="'+duePrinAmount+'">'
                            + '</td> '
                            + '<td><input type="text" name="dueInterestAmount" id="dueInterestAmount" style="width: 95px" readonly value="'+dueInterestAmount+'">'
                            + '</td> '
                            + '<td><input type="text" name="dueInterestAmount" id="dueInterestAmount" style="width: 90px" readonly value="'+totalDueAmount+'">'
                            + '</td> '
                            + '<td><input type="text" name="dueInterestAmount" id="dueInterestAmount" style="width: 90px" readonly value="'+paymentStatus+'">'
                            + '</td> '
                            + '</tr>');

                });


            },
            error: function (e) {

            }
        });
    })

});
