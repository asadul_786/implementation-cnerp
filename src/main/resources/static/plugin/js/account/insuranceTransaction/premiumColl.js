function getDueListPolicyHistory(policyNo){

    var json = {
        "policyNo": policyNo

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getDueListPolicyHistory/"
        + policyNo,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var installmentNo = "";
            var dueDate = "";
            var premAmount = "";
            var dueStatus = "";

            $("#tableBody").empty();

            $.each(data, function (i, l) {
                installmentNo = l[0];
                dueDate = l[1];
                dueDate = getDateFormat(dueDate);
                premAmount = l[2];
                dueStatus = l[3];

                $("#tableBody")
                    .append(
                        '<tr>'
                        + '<td><input type="text" name="installmentNo" id="installmentNo" style="width: 133px" readonly value="'+installmentNo+'">'
                        + '</td> '
                        + '<td><input type="text" name="dueDate" id="dueDate" style="width: 77px" readonly value="'+dueDate+'">'
                        + '</td> '
                        + '<td><input type="text" name="premAmount" id="premAmount" style="width: 109px" readonly value="'+premAmount+'">'
                        + '</td> '
                        + '<td><input type="text" name="dueStatus" id="dueStatus" style="width: 103px" readonly value="'+dueStatus+'">'
                        + '</td> '
                        + '</tr>');
            });

        },
        error: function (e) {

        }
    });


}
function getDateFormat(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = year + "-" + month + "-" + day;

    return date;
};

function getBranchName(bankCd){

    var json = {
        "bankCd": bankCd

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
        + bankCd,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#branchName').empty();
            $.each(data, function(key, value) {
                $('#branchName').append('<option value="' + key + '">' + value + '</option>');
            });

        },
        error: function (e) {

        }
    });
}

function getInsPSummeryInfoByPgId(policyNo){

    var json = {
        "policyNo": policyNo

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getInsPSummery/"
        + policyNo,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var value_0 = "";
            var dateOfCom = "";
            var riskDate = "";
            var value_3 = "";
            var value_4 = "";
            var value_5 = "";
            var value_6 = "";
            var option = "";
            var orgSetup = "";
            var lastPaidDate = "";

            $.each(data, function (i, l) {
                value_0 = l[0];
                dateOfCom = l[1];
                riskDate = l[2];
                value_3 = l[3];
                value_4 = l[4];
                value_5 = l[5];
                value_6 = l[6];
                option = l[7];
                orgSetup = l[9];
                lastPaidDate = l[8];
            });
            if(riskDate){
                riskDate = getDateShow(riskDate);
            }
            if(dateOfCom){
                dateOfCom = getDateShow(dateOfCom);
            }
            if(lastPaidDate){
                lastPaidDate = getDateShow(lastPaidDate);
            }
            $("#riskDate").val(riskDate);
            $("#productIdTerm").val(value_3);
            $("#modeOfPayment").val(value_4);
            $("#sumAssured").val(value_5);
            $("#policyStatus").val(value_6);
            $("#dateOfCommencement").val(dateOfCom);
            $("#lastPaidDate").val(lastPaidDate);
            $("#organizationalSetup").val(orgSetup);
            $("#option").val(option);
        },
        error: function (e) {

        }
    });

}
function getDate(dateObject) {
    var d = new Date(dateObject);

    return d;
};
function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

function getCollectionDate(){
    var mediaDateStr = $('#mediaDate').val();
    var mediaDate = getYear(mediaDateStr);
    var duedtToSTr = $('#dueDateTo').val();
    var duedtTo = getYear(duedtToSTr);
    if(mediaDate && duedtTo){
        if(mediaDate === duedtTo){
            $('#collectionDate').val('');
            $('#collectionDate').val($.datepicker.formatDate('dd/mm/yy', new Date()));
        }else{
            $( "#collectionDate" ).datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true
            })
        }
    }else{
        alert("please select Media Date and Due Date To");
    }

}
function getYear(dateObject) {
    var d = new Date(dateObject);
    var year = d.getFullYear();

    return year;
};

function getLastPGIDInsPPersonal(policyNo){

    var json = {
        "policyNo": policyNo
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getLastPGIDInsPPersonal/"+policyNo,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $("#pgId").val(data[0]);
            getAssuredNmInPPersonal(data[0]);
            getAddressInsPAdd(data[0]);
            getProductNmInsPrd(data[0]);
            setMobileNumberByPgId(data[0]);
        },
        error: function (e) {

        }
    });
}
function getAssuredNmInPPersonal(pgId){

    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getAssuredNmInsPPersonal/"
        + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $("#assuredName").val(data[0]);

        },
        error: function (e) {

        }
    });

}
function getAddressInsPAdd(pgId){

    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getAddressInsPAdd/"
        + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $("#address").val(data[0]);

        },
        error: function (e) {

        }
    });

}
function getProductNmInsPrd(pgId){

    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getProductNmInsPrd/"
        + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var value_1 = "";
            var value_2 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
                value_2 = l[1];
            });
            $("#productName").val(value_1);
            $("#productId").val(value_2);

        },
        error: function (e) {

        }
    });

}
function getLastReceiveIdAccRecv(){

    var json = {
        "officeId": ""
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getLastReceiveIdAccRecv",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $("#receiveId").val(data[0]);

        },
        error: function (e) {

        }
    });
}

function dataValidation() {

    var status = true;
    var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
    var duedtToStr = $('#dueDateTo').val();
    var dueDtFromStr = $('#dueDateFrom').val();
    var mobileNo = $('#mobileNumber').val();

    if ($("#installmentNoFrom").val() == "" || $("#installmentNoTo").val() == "") {
        status = false;
        $.confirm({
            title: 'Message',
            content: "Installment No From or To Can't empty please provide valid policy no",
            buttons: {
                ok: function () {
                }
            }
        });
    }

    if ($("#dueDateFrom").val() == "" || $("#dueDateFrom").val() == "0") {
        status = false;
        $("#dueDateFromSpan").text("Empty field found!!").css('color', 'red');
        $("#dueDateFrom").focus();
    }
    else $("#dueDateFromSpan").text("");

    if ($("#dueDateTo").val() == "" || $("#dueDateTo").val() == "0") {
        status = false;
        $("#dueDateToSpan").text("Empty field found!!").css('color', 'red');
        $("#dueDateTo").focus();
    }
    else $("#dueDateToSpan").text("");

    if ($("#collectionDate").val() == "" || $("#collectionDate").val() == "0") {
        status = false;
        $("#collectionDateSpan").text("Empty field found!!").css('color', 'red');
        $("#collectionDate").focus();
    }
    else $("#collectionDateSpan").text("");

    if(new Date(duedtToStr) < new Date(dueDtFromStr))
    {
        status = false;
        alert("Due Date To can not be greater than Due Date From!");
        var duedtToStr = $('#dueDateTo').val('');
        $("#dueDateTo").focus(duedtToStr.css({'background-color' : 'yellow'}));
    }
    if ($("#policyNo").val() == "" || $("#policyNo").val() == "0") {
        status = false;
        $("#policyNoSpan").text("Empty field found!!").css('color', 'red');
        $("#policyNo").focus();
    }
    else $("#policyNoSpan").text("");

   if ($("#mobileNumber").val() == "" || $("#mobileNumber").val() == "0") {
        status = false;
        $("#mobileNumberSpan").text("Empty field found!!").css('color', 'red');
        $("#mobileNumber").focus();
    }
    else $("#mobileNumberSpan").text("");

    var str_sms = $('#mobileNumber').val();

    if (numberRegex.test(str_sms) == false
        || $("#mobileNumber").val().length != 11) {
        status = false;
        $("#mobileNumberSpan").text("Invalid contact no");
        $("#mobileNumber").focus();

    } else
        $("#mobileNumberSpan").text("");


    return status;
}
function setCollDateByMediaDate(){
    var currDateStr = new Date();
    var mediaDateStr = $('#mediaDate').val();
    if(mediaDateStr){
        var mediaDateYear = getYear(mediaDateStr);
        var curDateYear = getYear(currDateStr);
        if(mediaDateYear === curDateYear){
            $('#collectionDate').val('');
            $('#collectionDate').val($.datepicker.formatDate('dd/mm/yy', new Date()));
        }else{
            $('#collectionDate').val('');
            var customCollDate = '31/12'+'/'+mediaDateYear;
            $('#collectionDate').val(customCollDate);
        }
    }

}

$(function(){
    $("#policyNo").on('change', function(){

        var policyNo = $('#policyNo').val();
        getLastPGIDInsPPersonal(policyNo);
        getDueListPolicyHistory(policyNo);
        getInsPSummeryInfoByPgId(policyNo);

        var json = {
            "policyNo": policyNo
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/collection/getDueInfoPremiumColl/"
            + policyNo,
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                var value_0 = "";
                var value_1 = "";
                var value_2 = "";
                var value_3 = "";
                var value_4 = "";
                $.each(data, function (i, l) {
                    value_0 = l[0];
                    value_1 = l[1];
                    value_2 = l[2];
                    value_3 = l[3];
                    value_4 = l[4];

                    $("#dueDateFrom").val('');
                    $("#dueDateTo").val('');
                    $("#totalDueAmount").val('');
                    $("#totalPremiumAmount").val('');
                    $("#totalPremiumAmount").val(value_0);
                    $("#installmentNoFrom").val(value_1);
                    $("#installmentNoTo").val(value_2);

                    if(value_3){
                        var momentDate = moment(value_3).utc().format('MM/DD/YYYY');
                        var increDueDateFrom = getDateByAddingOne(momentDate);
                        var increDueDateFromFormat = getDateShow(increDueDateFrom);
                        $("#dueDateFrom").val(increDueDateFromFormat);
                    }
                    if(value_4){
                        var momentDate = moment(value_4).utc().format('MM/DD/YYYY');
                        var increDueDateFrom = getDateByAddingOne(momentDate);
                        var increDueDateToFormat = getDateShow(increDueDateFrom);
                        $("#dueDateTo").val(increDueDateToFormat);
                    }

                    if(value_0){
                        var totPrmAmnt = Number(value_0);
                        var lateFee = 0;
                        var lateFeeWaiver = 0;
                        var totalDueAmnt = ((totPrmAmnt+Number(lateFee))-Number(lateFeeWaiver));
                        $("#totalDueAmount").val('');
                        $("#totalDueAmount").val(totalDueAmnt);
                    }

                });


            },
            error: function (e) {

            }
        });
    })

});


function setCollectionOfficeByEmplId(){

    var json = {
        "officeId": ''

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getCollectionOfficeByEmplId",
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var value_1 = "";
            var value_2 = "";
            $.each(data, function (i, l) {
                value_1 = l[0];
                value_2 = l[1];
            });

            $("#collectionOffice").val(value_1);
            $("#collectionOfficeId").val(value_2);


        },
        error: function (e) {
        }
    });

}
function getDateByAddingOne(recvDate) {
    var date = new Date(recvDate);
    var newdate = new Date(date);

    newdate.setDate(newdate.getDate() + 1);

    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    var someFormattedDate = mm + '/' + dd + '/' + y;
    return someFormattedDate;
}

function setMobileNumberByPgId(pgId){

    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getMobileNumberByPgId/"
        + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $("#mobileNumber").val('');
            $("#mobileNumber").val(data);
        },
        error: function (e) {

        }
    });
}

$(document).ready(function () {
    $("#collectionDate").datepicker({ dateFormat: "dd/mm/yy"}).datepicker("setDate", new Date());
    $('#collectionMedia').empty();
    $('#collectionMedia').prop('disabled', true);
    $('#collectionMedia').append(
        '<option value="' + "OL" + '">' + "Not Applicable" + '</option>'
    );

    var regexname=  /^[0-9]+\.?[0-9]*$/;


    $('#availableSuspenseAmnt').on('keypress keydown keyup blur focus',function(){
        if (!$(this).val().match(regexname)) {
            $('#availableSuspenseAmntSpan').show().text("only Positive Number!!").css('color', 'red');
            $('#availableSuspenseAmnt').val('');
            //$('#premiumCollForm').attr('onsubmit','return false;');
        }
        else{
            $('#availableSuspenseAmntSpan').show().text("");
        }
    });
    $('#totalPremiumAmount').on('keypress keydown keyup blur focus',function(){
        if (!$(this).val().match(regexname)) {
            $('#totalPremiumAmountSpan').show().text("only Positive Number!!").css('color', 'red');
            $('#totalPremiumAmount').val('');
        }
        else{
            $('#totalPremiumAmountSpan').show().text("");
        }
    });
    $('#lateFee').on('keypress keydown keyup blur focus',function(){
        if (!$(this).val().match(regexname)) {
            $('#lateFeeSpan').show().text("only Positive Number!!").css('color', 'red');
            $('#lateFee').val('');
        }
        else{
            $('#lateFeeSpan').show().text("");
        }
    });
    $('#lateFeeWaiver').on('keypress keydown keyup blur focus',function(){
        if (!$(this).val().match(regexname)) {
            $('#lateFeeWaiverSpan').show().text("only Positive Number!!").css('color', 'red');
            $('#lateFeeWaiver').val('');
        }
        else{
            $('#lateFeeWaiverSpan').show().text("");
        }
    });
    $('#totalDueAmount').on('keypress keydown keyup blur focus',function(){
        if (!$(this).val().match(regexname)) {
            $('#totalDueAmountSpan').show().text("only Positive Number!!").css('color', 'red');
            $('#totalDueAmount').val('');
        }
        else{
            $('#totalDueAmountSpan').show().text("");
        }
    });
    $('#totalCollAmnt').on('keypress keydown keyup blur focus',function(){
        if (!$(this).val().match(regexname)) {
            $('#totalCollAmntSpan').show().text("only Positive Number!!").css('color', 'red');
            $('#totalCollAmnt').val('');
        }
        else{
            $('#totalCollAmntSpan').show().text("");
        }
    });

    $("#refresh_button").click(function () {
        clearform();
    });

    setCollectionOfficeByEmplId();

    $("#refreshBtn").click(function () {
        clearform();
    });

    var lateFee = $('#lateFee').val(0);
    var lateFeeWaiver = $('#lateFeeWaiver').val(0);
    getLastReceiveIdAccRecv();
    //getLastPGIDInsPPersonal();

    $("#add_button").click(function () {
        var flag = dataValidation();
        if (flag == true) {
            var currDate =  $.datepicker.formatDate('dd/mm/yy', new Date());

            var collAmount = $('#totalCollAmnt').val();
            var receiveId = $('#receiveId').val();
            var pgId = $('#pgId').val();
            var modeTp = $('#collCategory').val();
            var officeCode = $('#collectionOfficeId').val();
            var policyNo = $('#policyNo').val();
            var collectionType = $('#collectionMedia').val();
            var receiveMode = $('#collectionType').val();
            var dueDtFromStr = $('#dueDateFrom').val();
            var duedtToStr = $('#dueDateTo').val();
            var receiveDateStr = $('#collectionDate').val();
            var checkNo = $('#checkNo').val();
            var checkDate = $('#checkDate').val();
            var policyNo = $('#policyNo').val();
            var avlSusAmnt = $('#availableSuspenseAmnt').val();
            if (checkDate) {
                checkDate = getDate(checkDate);
            } else {
                checkDate = null;
            }
            if (receiveDateStr) {
                var receiveDate = getDate(receiveDateStr);
            } else {
                var receiveDate = null;
            }
            var totPremiumAmt = $('#totalPremiumAmount').val();
            var lateFee = $('#lateFee').val();
            var lateFeeWaiver = $('#lateFeeWaiver').val();
            var mediaDateStr = $('#mediaDate').val();
            if (mediaDateStr) {
                var mediaDate = getDate(mediaDateStr);
            } else {
                var mediaDate = null;
            }
            var installmentNoFrom = $('#installmentNoFrom').val();
            var installmentNoTo = $('#installmentNoTo').val();
            var mobileNumber = $('#mobileNumber').val();
            var bankName = $('#bankName').val();
            var branchName = $('#branchName').val();


            var accReceivables = {};
            accReceivables.collAmount = collAmount;
            accReceivables.receiveId = receiveId;
            accReceivables.modeTp = modeTp;
            accReceivables.officeCode = officeCode;
            accReceivables.policyNo = policyNo;
            accReceivables.collectionType = collectionType;
            accReceivables.receiveMode = receiveMode;
            if (dueDtFromStr) {
                var dueDtFrom = getDate(dueDtFromStr);
                accReceivables.dueDtFrom = dueDtFrom;
            } else {
                var dueDtFromCurr = getDate(currDate);
                accReceivables.dueDtFrom = dueDtFromCurr;

            }
            if (duedtToStr) {
                var duedtTo = getDate(duedtToStr);
                accReceivables.duedtTo = duedtTo;
            } else {
                var duedtToCurrStr = getDate(currDate);
                accReceivables.duedtTo = duedtToCurrStr;
            }
            accReceivables.receiveDate = receiveDate;
            accReceivables.pgId = pgId;
            accReceivables.totPremiumAmt = totPremiumAmt;
            accReceivables.lateFee = lateFee;
            accReceivables.lateFeeWaiver = lateFeeWaiver;
            accReceivables.mediaDate = mediaDate;
            accReceivables.installmentNoFrom = installmentNoFrom;
            accReceivables.installmentNoTo = installmentNoTo;
            accReceivables.mobileNumber = mobileNumber;
            accReceivables.checkNo = checkNo;
            accReceivables.checkDate = checkDate;
            accReceivables.policyNo = policyNo;
            accReceivables.avlSusAmnt = avlSusAmnt;
            accReceivables.bankCd = bankName;
            accReceivables.brCd = branchName;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/collection/saveAccReceivables",
                data: JSON.stringify(accReceivables),
                dataType: 'json',
                success: function (data) {
                    clearform();
                    if(data[1] == '' || data[1] == null){
                        $.confirm({
                            title: 'Message',
                            content: data[2],
                            buttons: {
                                ok: function () {
                                }
                            }
                        });
                    }else{
                        showAlert("Successfully added.");
                        window.open('/collection/generatePremiumCollRec.pdf?receiveId='+data[3], '_blank');
                        clearform();
                    }
                },
                error: function (e) {
                    $.confirm({
                        title: 'Message',
                        content: data[2],
                        buttons: {
                            ok: function () {
                            }
                        }
                    });
                }
            });
        }
    });

    var showAlert = function (alertContent) {
        new PNotify({
            title: 'Info',
            text: alertContent,
            type: 'info',
            styling: 'bootstrap3'
        });
    }


    $('#mediaNo').prop('disabled', true);
    $('#mediaDate').prop('disabled', true);
    //$('#collectionMedia').prop('disabled', true);
    $('#bankName').prop('disabled', true);
    $('#branchName').prop('disabled', true);
    $('#checkNo').prop('disabled', true);
    $('#checkDate').prop('disabled', true);
    $('#productName').prop('disabled', true);
    $('#address').prop('disabled', true);


    $('#collCategory').change(function (e) {
        var selectedValue = $(this).val();

        if(selectedValue == 0) {
            return;
        }

        if(selectedValue == '1') {
            $('#collectionMedia').empty();
            $('#collectionMedia').prop('disabled', true);
            $('#collectionMedia').append(
                '<option value="' + "OL" + '">' + "Not Applicable" + '</option>'
            );

            $('#mediaNo').prop('disabled', true);
            $('#mediaDate').prop('disabled', true);
        }

        if(selectedValue == '2') {
            $('#collectionMedia').empty();
            $('#collectionMedia').prop('disabled', false);
            $('#collectionMedia').append(
                '<option value="' + "PR" + '">' + "PR" + '</option>'
                +'<option value="' + "BM" + '">' + "BM" + '</option>'
                +'<option value="' + "JV" + '">' + "JV" + '</option>'
                +'<option value="' + "MR" + '">' + "MR" + '</option>'
                +'<option value="' + "OL" + '">' + "Not Applicable" + '</option>'
            );

            $('#mediaNo').prop('disabled', false);
            $('#mediaDate').prop('disabled', false);
        }

    });

    $('#collectionType').change(function (e) {

        var selectedValue = $(this).val();

        if(selectedValue == 0) {
            return;
        }

        if(selectedValue == '1') {
            $('#checkNo').prop('disabled', true);
            $('#checkDate').prop('disabled', true);
            $('#bankName').prop('disabled', true);
            $('#branchName').prop('disabled', true);
        }else{
            $('#bankName').prop('disabled', false);
            $('#branchName').prop('disabled', false);
            $('#checkNo').prop('disabled', false);
            $('#checkDate').prop('disabled', false);
        }


    });

    //scroll table
    if(  $('#results tr').length >= 5 ) {
        $('#myTable').addClass('add-scroll');
    }

    function clearform() {
        $('#riskDate').val("");
        $('#collCategory').val("");
        $('#receiveId').val("");
        $('#pgId').val("");
        $('#collectionOffice').val("");
        $('#collectionOfficeId').val("");
        $('#policyNo').val("");
        $('#lastPaidDate').val("");
        $('#assuredName').val("");
        $('#address').val("");
        $('#organizationalSetup').val("");
        $('#productName').val("");
        $('#productId').val("");
        $('#riskDate').val("");
        $('#productIdTerm').val("");
        $('#option').val("");
        $('#modeOfPayment').val("");
        $('#sumAssured').val("");
        $('#policyStatus').val("");
        $('#collectionMedia').val("");
        $('#mediaDate').val("");
        $('#mediaNo').val("");
        $('#collectionType').val("");
        $('#bankName').val("");
        $('#bankId').val("");
        $('#checkNo').val("");
        $('#checkDate').val("");
        $('#dueDateFrom').val("");
        $('#dueDateTo').val("");
        $('#collectionDate').val("");
        $('#branchName').val("");
        $('#branchId').val("");
        $('#availableSuspenseAmnt').val("");
        $('#reference').val("");
        $('#lateFee').val("");
        $('#lateFeeWaiver').val("");
        $('#totalDueAmount').val("");
        $('#totalPremiumAmount').val("");
        $('#totalCollAmnt').val("");
        $('#dateOfCommencement').val("");
        $("#mobileNumber").val("")

    }
});