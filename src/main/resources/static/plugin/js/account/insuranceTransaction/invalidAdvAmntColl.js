/*author : Mohasin
* time: 03/03/2020*/
$(document).ready(function() {
    //officeAutoComplete();
});

function officeAutoComplete() {

    var proposalOffice = "#" + "proposalOffice";
    var url = "/collection/officeAutoComplete";
    var proposalOfficeId = "#" + "proposalOfficeId";
    autocomplete(proposalOffice, url, "", proposalOfficeId);
}

function dataValid() {
    var status = true;

    if ($("#proposalNo").val() == "" || $("#proposalNo").val() == "0") {
        status = false;
    }
    if ($("#assuredName").val() == "" || $("#assuredName").val() == "0") {
        status = false;
    }

    return status;
}

function proposalValid() {
    var status = true;

    if ($("#proposalNo").val() == "" || $("#proposalNo").val() == "0") {
        status = false;
    }

    return status;
}

function assNameValid() {
    var status = true;

    if ($("#assuredName").val() == "" || $("#assuredName").val() == "0") {
        status = false;
    }
    return status;
}
function getInvalidAdvAmntColl(){

    $('#invAdvAmntColl').attr('onsubmit','return false;');

    var allValidation = dataValid();
    var proposalValidation = proposalValid();
    var assNameValidation = assNameValid();

        if(allValidation === true){
            //alert("all");
            $( "#allField" ).val("");
            $( "#allField" ).val("all");
            $('#invAdvAmntColl').attr('onsubmit','return true;');
        }else{
            if(proposalValidation === true){
                //alert("proposalField");
                $( "#proposalField" ).val("");
                $( "#proposalField" ).val("proposal");
                $('#invAdvAmntColl').attr('onsubmit','return true;');
            }else if(assNameValidation == true){
                //alert("assNameValidation");
                $( "#nameField" ).val("");
                $( "#nameField" ).val("assName");
                $('#invAdvAmntColl').attr('onsubmit','return true;');
            }else{
                $('#invAdvAmntColl').attr('onsubmit','return true;');
            }
        }
}

function createInvalidAdvAmntColl(recvId){

        var json = {
            "recvId": recvId
        };

        $.confirm({
            title: 'Message',
            content: 'Are You Sure To Prepare This Record For Invalid',
            buttons: {
                ok: function () {
                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json',
                        url: "/collection/saveInvalidAdvAmntColl/"+recvId,
                        data: JSON.stringify(json),
                        dataType: 'json',
                        success: function (data) {

                            if(data){
                                showAlert("Successfully Saved.");
                            }else{
                                showAlert("Can't Save Due To Data MisMatch!!");
                            }
                        },
                        error: function (e) {
                            showAlert("Can't Save Due To Data MisMatch!!");
                        }
                    });
                },
                cancel: function () {

                },
            }
        });
}

function checkInvalidAdvColl(recvId){

    var json = {
        "recvId": recvId
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/collection/checkInvalidAdvColl/"+recvId,
        data: JSON.stringify(json),
        dataType: 'json',
        success: function (data) {

            if(data!=''){
                showAlert("This record already Applied For Invalid");
            }else{
                createInvalidAdvAmntColl(recvId);
            }
        },
        error: function (e) {
            showAlert("This record already Applied For Invalid");
        }
    });
}