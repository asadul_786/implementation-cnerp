/*author : Mohasin
* time: 20/02/2020*/
$(document).ready(function() {
    //$( "#collFromDate" ).val($.datepicker.formatDate('dd/mm/yy', new Date()));
    //$( "#collToDate" ).val($.datepicker.formatDate('dd/mm/yy', new Date()));
    officeAutoComplete();
    bankAutoComplete();

});
function dataValid() {
    var status = true;

    if ($("#collFromDate").val() == "" || $("#collFromDate").val() == "0") {
        status = false;
    }
    if ($("#collToDate").val() == "" || $("#collToDate").val() == "0") {
        status = false;
    }
    if ($("#bank").val() == "" || $("#bank").val() == "0") {
        status = false;
    }
    if ($("#branchName").val() == "" || $("#branchName").val() == "0") {
        status = false;
    }
    if ($("#policyNo").val() == "" || $("#policyNo").val() == "0") {
        status = false;
    }
    if ($("#descendentOfficeVal").val() == "" || $("#descendentOfficeVal").val() == "0") {
        status = false;
    }

    return status;
}

function dateValid() {
    var status = true;

    if ($("#collToDate").val() == "" || $("#collToDate").val() == "0") {
        status = false;
    }
    if ($("#collFromDate").val() == "" || $("#collFromDate").val() == "0") {
        status = false;
    }
    return status;
}
function bankValid() {
    var status = true;

    if ($("#bank").val() == "" || $("#bank").val() == "0") {
        status = false;
    }
    if ($("#branchName").val() == "" || $("#branchName").val() == "0") {
        status = false;
    }
    return status;
}
function policyNoValid() {
    var status = true;

    if ($("#policyNo").val() == "" || $("#policyNo").val() == "0") {
        status = false;
    }
    return status;
}
function descendentOfficeValid() {
    var status = true;

    if ($("#descendentOfficeVal").val() == "" || $("#descendentOfficeVal").val() == "0") {
        status = false;
    }
    return status;
}
function getInvalidAdvAmntColl(){

    $('#premCollChqSt').attr('onsubmit','return false;');

    var allValidation = dataValid();
    var dateValidation = dateValid();
    var bankValidation = bankValid();
    var policyNoValidation = policyNoValid();
    var descendentOfficeValidation = descendentOfficeValid();

   var collOffice = jQuery.trim($('#collOffice').val());

    if (collOffice.length > 0) {

        if(allValidation === true){
            //alert("all");
            $( "#allField" ).val("");
            $( "#allField" ).val("all");
            $('#premCollChqSt').attr('onsubmit','return true;');
        }else{
            if(dateValidation === true){
                //alert("date");
                $( "#dateField" ).val("");
                $( "#dateField" ).val("date");
                $('#premCollChqSt').attr('onsubmit','return true;');
            }else if(bankValidation === true){
                //alert("bank");
                $( "#bankField" ).val("");
                $( "#bankField" ).val("bank");
                $('#premCollChqSt').attr('onsubmit','return true;');
            }else if(policyNoValidation === true){
                //alert("policy");
                $( "#policyField" ).val("");
                $( "#policyField" ).val("policy");
                $('#premCollChqSt').attr('onsubmit','return true;');
            }else if(descendentOfficeValidation === true){
                //alert("descendentOfficeValidation");
                $( "#desField" ).val("");
                $( "#desField" ).val("desField");
                $('#premCollChqSt').attr('onsubmit','return true;');
            }
            //alert("not match");
            $('#premCollChqSt').attr('onsubmit','return true;');
        }
    }else{
        $("#collOfficeSpan").text("Empty field found!!");
        $("#collOffice").focus();
        $('#premCollChqSt').attr('onsubmit','return false;');
    }
}

function getDescendentOffice() {
    var checkBox = document.getElementById("descendentOffice");
    var text = document.getElementById("text");
    if (checkBox.checked == true){
        $( "#descendentOfficeVal" ).val("");
        $( "#descendentOfficeVal" ).val("1");
        //text.style.display = "block";
    } else {
        $( "#descendentOfficeVal" ).val("");
        $( "#descendentOfficeVal" ).val("0");
        //text.style.display = "none";
    }
}

function officeAutoComplete() {

    var collOffice = "#" + "collOffice";
    var url = "/collection/officeAutoComplete";
    var collOfficeId = "#" + "collOfficeId";
    autocomplete(collOffice, url, "", collOfficeId);
}
function bankAutoComplete() {

    var bank = "#" + "bank";
    var url = "/collection/bankAutoComplete";
    var bankId = "#" + "bankId";
    autocompleteBank(bank, url, "", bankId);
}
function autocompleteBank(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                    getBranchList(ui.item.value);
                }
            });

        }
    });
}
function getBranchId(key){
    $( "#branchId" ).val('');
    $( "#branchId" ).val(key);
}
function getBranchList(bankId){

    var json = {
        "bankId": bankId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/collection/getBankCd/"
            + bankId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#branchName').empty();
            $.each(data, function(key, value) {
                $( "#branchId" ).val("");
                $('#branchName').append('<option value="' + key + '">' + value + '</option>');
                $( "#branchId" ).val('');
                $( "#branchId" ).val(key);
            });
        },
        error: function (e) {

        }
    });
}

function changeChqStatus() {
    var chequeStatus = $('#chequeStatus').val();

    if(chequeStatus === 'null'){
        $('#chqOption').prop('disabled', true);
    }else if(chequeStatus === '1'){
        $('#chqOption').prop('disabled', true);
    }else if(chequeStatus === '2'){
        $('#chqOption').prop('disabled', true);
    }else{
        $('#chqOption').prop('disabled', false);
    }
}
