/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#pr_book_info_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "getPRBookInfo",
            "type": "GET",
            /*"data": function ( d ) {
                d.inputYear = $.trim($("#input_year").val())
            },*/
            "dataType": "json"
        },
        "initComplete": function(settings, json) {
            $("#pr_book_info_table_processing").css({"top": "5px", "color": "green"});
        },
        "autoWidth": true,
        "columns": [
            // { "data": "pgId", "name": "PGID", className: "dt-hd" },
            { "data": "bookName.bookName" },
            { "data": "bookNo" },
            { "data": "noOfPages" },
            { "data": "firstPgNo" },
            { "data": "lastPgNo" },
            { "data": "officeNm.office_NM" },
            { "data": "deptNm.divdept_nm" },
            {
                "data": "status",
                "render": function (data) {
                    if(data == "0")
                        return "Not Allocated";
                    if(data == "1")
                        return "Allocated to Control Office";
                    if(data == "2")
                        return "Allocated to User Office";
                    if(data == "3")
                        return "Used";
                    if(data == "4")
                        return "Cancelled";
                }
            },
            /*{
                "className": "dt-btn",
                "render": function () {
                    return '<button id="editBtn" class="btn fa fa-pencil" style="text-align:center;vertical-align: middle;font-size:20px;"></button>';
                }
            },*/
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="dltBtn" class="btn fa fa-trash-o" style="text-align:center;vertical-align: middle;font-size:20px;color:red;"></button>';
                }
            },
            { "data": "bookId", className: "dt-hd" }
        ]
    });

    $(document).on('input', '#no_of_pages', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
        $('#first_page_no').val() != "" ? $('#last_page_no').val(Number($('#first_page_no').val()) + Number($('#no_of_pages').val()))
            : "";
    });

    $(document).on("change", "#book_id", function () {

        var bookId = $("#book_id").val();

        if(bookId != -1){

            $.get("getPRBookNo?bookId=" + bookId,

                function (data, status) {
                    data != "" ? $('#book_no').val(data) : $('#book_no').val('001');
                });

            $.get("getFirstPgNo?bookId=" + bookId,

                function (data, status) {
                    data != "" ? $('#first_page_no').val(data) : $('#first_page_no').val('11' + bookId + '00000001');
                    $('#last_page_no').val((Number($('#first_page_no').val()) + 50));
                });

            $('#no_of_pages').val('50');

        }
        else{
            $('#book_no').val('');
            $('#first_page_no').val('');
            $('#last_page_no').val('');
        }
    });

    $(document).on("click", "#save_pr_book_info", function () {
        if(validate()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var prBookInfo = {};
                        prBookInfo.bookId = $("#book_id").val();
                        prBookInfo.bookNo = $("#book_no").val();
                        prBookInfo.noOfPages = $("#no_of_pages").val();
                        prBookInfo.firstPgNo = $("#first_page_no").val();
                        prBookInfo.lastPgNo = $("#last_page_no").val();
                        //prBookInfo.ownerOff = $("#owner_office_id").val();
                        //prBookInfo.ownerDpt = $("#owner_dept").val();
                        //prBookInfo.status = $("#status").val();

                        $.ajax({
                            contentType: 'application/json',
                            url:  "savePrBookInfo",
                            type: 'POST',
                            data: JSON.stringify(prBookInfo),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : "";
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }
    });

    //var bookNo = "";

    /*$("#pr_info_table tbody").on("click", "#editBtn", function () {

        var curRow = $(this).closest('tr');
        $("#book_id").val($.trim(curRow.find('td:eq(9)').text()));
        $("#owner_office_id").val($.trim(curRow.find('td:eq(7)').text()));
        $("#alloc_dt").val($.trim(curRow.find('td:eq(4)').text()));
        $("#cntrl_office_id").val($.trim(curRow.find('td:eq(8)').text()));

        $('#book_no').empty();
        $('#book_no').append($('<option/>', {
            value: $.trim(curRow.find('td:eq(3)').text()),
            text: $.trim(curRow.find('td:eq(3)').text()) + " - " + $.trim(curRow.find('td:eq(2)').text())
        }));

        $("#prAllocationForm select").prop("disabled", true);
        //$("pr_info_table_length").prop("disabled", false);

        $('html, body').animate({
            scrollTop: $('#scrlDiv').offset().top
        }, 500);
    });*/



    $("#pr_book_info_table tbody").on("click", "#dltBtn", function () {

        var curRow = $(this).closest('tr');

        $.confirm({
            title: 'Confirm',
            content: 'Are your sure?',
            buttons: {
                ok: function () {

                    $.ajax({
                        //contentType: 'application/json',
                        url:  "dltPRBookInfo",
                        type: 'DELETE',
                        data: {
                            bookId: $.trim(curRow.find('td:eq(9)').text()),
                            bookNo: $.trim(curRow.find('td:eq(1)').text())
                        },
                        dataType: 'json',
                        success: function(response) {
                            if(response == "1"){
                                showAlert("Deleted Successfully");
                                table.row( curRow ).remove().draw( false );
                            }
                            else
                                showAlert("Unknown error occured !!");
                        },
                        error: function(xhr, status, error) {
                            showAlert("Something went wrong !!");
                        },
                        complete: function () {
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

    $(document).on("click", "#clear_btn", function () {
        $('#prBookInfoForm').trigger("reset");
        $(".rmv").text("");
    });

    function validate() {

        $(".rmv").text("");

        if($("#book_id").val() == "-1"){
            $("#err_book_id").text("Required !!");
            return;
        }

        if($("#no_of_pages").val() == ""){
            $("#err_no_of_pages").text("Required !!");
            return;
        }

        return true;
    }

});