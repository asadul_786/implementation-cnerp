/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#pr_info_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "dummyCall",
            "type": "GET",
            "data": function ( d ) {
                d.inputYear = $.trim($("#input_year").val())
            },
            "dataType": "json"
        },
        "initComplete": function(settings, json) {
            $("#pr_info_table_processing").css({"top": "5px", "color": "green"});
        },
        "autoWidth": true,
        "columns": [
            // { "data": "pgId", "name": "PGID", className: "dt-hd" },
            { "data": "ownerOffNM", "name": "OWNER_OFFICE_NAME" },
            { "data": "controlOffNM", "name": "CONTROL_OFFICE_NAME" },
            { "data": "bookNM", "name": "PR_BOOK_NAME" },
            { "data": "bookNo", "name": "PR_BOOK_NO" },
            { "data": "allocDT", "name": "ALLOCATION_DT" },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="editBtn" class="btn fa fa-pencil" style="text-align:center;vertical-align: middle;font-size:20px;"></button>';
                }
            },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="dltBtn" class="btn fa fa-trash-o" style="text-align:center;vertical-align: middle;font-size:20px;color:red;"></button>';
                }
            },
            { "data": "ownerOff", "name": "OWNER_OFFICE", className: "dt-hd" },
            { "data": "controlOff", "name": "CONTROL_OFFICE", className: "dt-hd" },
            { "data": "bookId", "name": "PR_BOOK_ID", className: "dt-hd" }
        ]
    });

    $(document).on("change", "#book_id", function () {

        var bookId = $("#book_id").val();

        if(bookId != -1){

            $.get("getBookNo?bookId=" + bookId,

                function (data, status) {

                    var bookNoDrpDwn = $('#book_no');
                    bookNoDrpDwn.empty();
                    bookNoDrpDwn.append($('<option/>', {
                        value: "-1",
                        text: "-- Select Book No. --"
                    }));

                    $.each(data, function (index, bookNo) {
                        bookNoDrpDwn.append($('<option/>', {
                            value: bookNo[0],
                            text: bookNo[0] + ' - ' + bookNo[2]
                        }));
                    });

                    $('#book_no').prop("disabled", false);

                });
        }
        else{
            $('#book_no').empty();
            $('#book_no').append($('<option/>', {
                value: "-1",
                text: "-- Select Book No. --"
            }));
            $('#book_no').prop("disabled", true);
        }
    });

    $(document).on("click", "#save_pr_info", function () {
        if(validate()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var prInfo = {};
                        prInfo.ownerOff = $("#owner_office_id").val();
                        prInfo.bookId = $("#book_id").val();
                        prInfo.bookNo = $("#book_no").val();
                        prInfo.controlOff = $("#cntrl_office_id").val();
                        prInfo.tmpAllocDT = $.trim($("#alloc_dt").val());
                        //prInfo.editMd = editMd;

                        $.ajax({
                            contentType: 'application/json',
                            url:  "savePrInfo",
                            type: 'POST',
                            data: JSON.stringify(prInfo),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                $("#save_pr_info").text('Save');
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }
    });

    //var bookNo = "";

    $("#pr_info_table tbody").on("click", "#editBtn", function () {

        var curRow = $(this).closest('tr');
        $("#book_id").val($.trim(curRow.find('td:eq(9)').text()));
        $("#owner_office_id").val($.trim(curRow.find('td:eq(7)').text()));
        $("#alloc_dt").val($.trim(curRow.find('td:eq(4)').text()));
        $("#cntrl_office_id").val($.trim(curRow.find('td:eq(8)').text()));

        $('#book_no').empty();
        $('#book_no').append($('<option/>', {
            value: $.trim(curRow.find('td:eq(3)').text()),
            text: $.trim(curRow.find('td:eq(3)').text()) + " - " + $.trim(curRow.find('td:eq(2)').text())
        }));

        $("#prAllocationForm select").prop("disabled", true);
        //$("pr_info_table_length").prop("disabled", false);

        $('html, body').animate({
            scrollTop: $('#scrlDiv').offset().top
        }, 500);
    });

    $("#pr_info_table tbody").on("click", "#dltBtn", function () {

        var curRow = $(this).closest('tr');

        $.confirm({
            title: 'Confirm',
            content: 'Are your sure?',
            buttons: {
                ok: function () {

                    var prInfo = {};
                    prInfo.ownerOff = $.trim(curRow.find('td:eq(7)').text());
                    prInfo.bookId = $.trim(curRow.find('td:eq(9)').text());
                    prInfo.bookNo = $.trim(curRow.find('td:eq(3)').text());
                    prInfo.controlOff = $.trim(curRow.find('td:eq(8)').text());
                    prInfo.tmpAllocDT = $.trim(curRow.find('td:eq(4)').text());

                    $.ajax({
                        contentType: 'application/json',
                        url:  "dltPrInfo",
                        type: 'DELETE',
                        data: JSON.stringify(prInfo),
                        dataType: 'json',
                        success: function(response) {
                            if(response == "1"){
                                showAlert("Deleted Successfully");
                                table.row( curRow ).remove().draw( false );
                            }
                            else
                                showAlert("Unknown error occured !!");
                        },
                        error: function(xhr, status, error) {
                            showAlert("Something went wrong !!");
                        },
                        complete: function () {
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

    $(document).on("click", "#search_by_year", function () {
        $("#err_input_year").text("");
        if($.trim($("#input_year").val()).length == 4){
            table.ajax.url("getPRInfo").load();
        }
        else
            $("#err_input_year").text("Please input valid year !!");
    });

    //var editMd = 0;

    $(document).on("click", "#clear_btn", function () {
        $("select").prop("disabled", false);
        $('#prAllocationForm').trigger("reset");
        $('#book_id').val(-1).trigger('change');
        $(".rmv").text("");
        //editMd = 0;
    });

    function validate() {

        $(".rmv").text("");

        if($("#owner_office_id").val() == "-1"){
            $("#err_owner_office_id").text("Required !!");
            return;
        }

        if($("#cntrl_office_id").val() == "-1"){
             $("#err_cntrl_office_id").text("Required !!");
             return;
         }

        if($("#book_id").val() == "-1"){
            $("#err_book_id").text("Required !!");
            return;
        }

        if($("#book_no").val() == "-1"){
            $("#err_book_no").text("Required !!");
            return;
        }

        if($.trim($("#alloc_dt").val()) == ""){
            $("#err_alloc_dt").text("Required !!");
            return;
        }

        return true;
    }

});