/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#pol_loan_rec_undo_approval_list_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "dummyCall",
            "type": "GET",
            "data": function ( d ) {
                d.policyNo = $.trim($("#pol_no").val())
            },
            "dataType": "json"
        },
        "initComplete": function(settings, json) {
            $("#pol_loan_rec_undo_approval_list_table_processing").css({"top": "5px", "color": "green"});
        },
        "autoWidth": true,
        "columns": [
            // { "data": "pgId", "name": "PGID", className: "dt-hd" },
            { "data": "receiveId" },
            { "data": "receiveDt" },
            { "data": "actOffice" },
            {
                "data": "modeType",
                "render": function (data) {
                    if(data == "1")
                        return "Online";
                    else if(data == "2")
                        return "Offline";
                    else
                        return "Unknown";
                }
            },
            {
                "data": "receiveMode",
                "render": function (data) {
                    if(data == "1")
                        return "Cash";
                    else if(data == "2")
                        return "Cheque";
                    else if(data == "3")
                        return "DD";
                    else if(data == "4")
                        return "Others";
                    else
                        return "Unknown";
                }
            },
            { "data": "slNo" },
            { "data": "colAmt" },
            { "data": "receivedBy" },
            { "data": "status",
                "render": function (data) {
                    if(data == "0")
                        return "Not Adjusted";
                    else if(data == "1")
                        return "Adjusted";
                    else
                        return "Unknown";
                }
            },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="process_approve" class="btn-sm btn-success" style="text-align:center;vertical-align: middle;font-size:12px;">Approve</button>';
                }
            },
            { "data": "pgid", "className": "dt-hd" }
        ]
    });

    $("#pol_loan_rec_undo_approval_list_table tbody").on("click", "#process_approve", function () {

        var curRow = $(this).closest('tr');

        $.confirm({
            title: 'Confirm',
            content: 'Are your sure?',
            buttons: {
                ok: function () {
                    $.ajax({
                        url:  "apprvPolLoanRecUndo",
                        data: {
                            recId: $.trim(curRow.find('td:eq(0)').text()),
                            recDt: $.trim(curRow.find('td:eq(1)').text()),
                            pgid: $.trim(curRow.find('td:eq(10)').text())
                        },
                        type: 'POST',
                        success: function(response) {
                            if(response == "1"){
                                table.row( curRow ).remove().draw( false );
                                showAlert("Policy Loan Recovery successfully undo");
                            }
                            else{
                                showAlertByType(response, "F");
                            }
                        },
                        error: function(xhr, status, error) {
                            showAlertByType("Something went wrong !!", "F");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

    $(document).on("click", "#search_ln_history", function () {
        $("#err_pol_no").text("");
        if($.trim($("#pol_no").val()).length == 10){
            table.ajax.url("getPolLoanRecUndoList").load();
        }
        else
            $("#err_pol_no").text("Please input valid Policy No !!");
    });

    $(document).on("click", "#clear_btn", function () {
        $('#polLoanRecUndoApprovalForm').trigger("reset");
        $(".rmv").text("");
    });

});