/**
 * Created by mithu on 11/5/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearform();
    changeView('I');
    loadDatableWithFilteredUnitData();


    /*-------------- changeView function start ------------------------*/
    $("#addNewUnitBtnAdd").click(function () {
        changeView('N')
    });

    function changeView(viewStyle) {

        if (viewStyle == 'N') {
            $("#pnl-vendor-info").css("display", "block");
            $("#pnl-vendor-list").css("display", "none");
        }
        else if (viewStyle == 'I') { //new next
            $("#pnl-vendor-info").css("display", "none");
            $("#pnl-vendor-list").css("display", "block");
        }
        else if (viewStyle == 'U') { //new next
            $("#pnl-vendor-info").css("display", "block");
            $("#pnl-vendor-list").css("display", "none");
        }
        else if (viewStyle == 'S') {
            $("#pnl-vendor-info").css("display", "none");
            $("#pnl-vendor-list").css("display", "block");
        }
        else {
            $("#pnl-vendor-info").css("display", "block");
            $("#pnl-vendor-list").css("display", "none");
        }
        clearform();

    }

    /*-------------- changeView function End ------------------------*/


    /*-------------- asset save function start ------------------------*/
    $("#vendorBtnSave").click(function () {
        saveData(0);
    });
    $("#vendorBtnReset").click(function () {
        changeView('I');
    });

    function getFormData(isDeletedFlg) {
        var vendorDto = {};

        vendorDto.encId = $("#encId").val();

        vendorDto.vendorCode = $("#vendorCode").val();
        vendorDto.vendorName = $("#vendorName").val();
        vendorDto.vendorType = $("#vendorType").val();
        vendorDto.address = $("#address").val();
        vendorDto.contactNo = $("#contactNo").val();
        vendorDto.authPerson = $("#authPerson").val();
        vendorDto.authContactNo = $("#authContactNo").val();
        vendorDto.authPersonDetails = $("#authPersonDetails").val();
        vendorDto.email = $("#email").val();
        vendorDto.webAddress = $("#webAddress").val();
        vendorDto.remarks = $("#remarks").val();
        vendorDto.blackList = $("#blackList").val();
        vendorDto.vendorQuality = $("#vendorQuality").val();

        //vendorDto.isDeletedFlg =isDeletedFlg;
        vendorDto.isActiveFlg = $("#isActiveFlg").val();

        return vendorDto;
    }

    function saveData(isDeletedFlg) {

        if (isValidated() || isDeletedFlg == 1) {
            const saveUnitUrl = "/inventory-ajax/save-vendor-post";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData(isDeletedFlg)),
                contentType: 'application/json',
                async: false,
                success: function (commonMsg) {

                    if (commonMsg.isSucceed) {

                        clearform();
                        loadDatableWithFilteredUnitData();
                        changeView('I');
                        showAlertByType(commonMsg.msg, 'S');

                    }
                    else {
                        showAlertByType(commonMsg.msg, 'F');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearform() {
        $("#encId").val('');
        $("#vendorCode").val('');
        $("#vendorName").val('');
        $("#vendorType").val('');
        $("#address").val('');
        $("#contactNo").val('');
        $("#authPerson").val('');
        $("#authContactNo").val('');
        $("#authPersonDetails").val('');
        $("#email").val('');
        $("#webAddress").val('');
        $("#remarks").val('');
        $("#blackList").val('');
        $("#vendorQuality").val('');
        $("#isActiveFlg").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/


    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#vendorCode").val())) {

            valdiationMsgContent = "Vendor Code is required!";
            $("#vendorCode").focus();
            return false;
        }
        else if (isEmptyString($("#vendorName").val())) {

         valdiationMsgContent = "Vendor Name is required!";
         $("#vendorName").focus();
         return false;
         }
         else if (isEmptyString($("#vendorType").val())) {

         valdiationMsgContent = "Vendor Type is required!";
         $("#vendorType").focus();
         return false;
         }
         else if (isEmptyString($("#contactNo").val())) {

         valdiationMsgContent = "Vendor ContactNo is required!";
         $("#contactNo").focus();
         return false;
         }
         else if (isEmptyString($("#authPerson").val())) {

         valdiationMsgContent = "authorize Person Name is required!";
         $("#authPerson").focus();
         return false;
         }
         else if (isEmptyString($("#authContactNo").val())) {

         valdiationMsgContent = "authorize Person ContactNo is required!";
         $("#authContactNo").focus();
         return false;
         }
         else if (isEmptyString($("#blackList").val())) {

         valdiationMsgContent = "BlackList is required!";
         $("#blackList").focus();
         return false;
         }
         else if (isEmptyString($("#isActiveFlg").val())) {

         valdiationMsgContent = "Status is required!";
         $("#isActiveFlg").focus();
         return false;
         }

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var vendorTable = $('#tbl-vendor-lst').DataTable({

        data: [],
        "columns": [
            {"data": null, defaultContent: ''},
            {"data": "vendorName"},
            {"data": "contactNo"},
            {"data": "vendorQuality"},
            {"data": "authPerson"},
            {"data": "authContactNo"},
            {"data": "isList"},
            {"data": "isActive"},
            {"data": null, defaultContent: ''}

        ],
        'columnDefs': [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round red vendor-delete"   encId="' + data.encId + '" ><i class="fa fa-remove" aria-hidden="true"   style="font-size: large"></i></a>';
                }
            }, {
                'targets': 8,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round vendor-edit"   encId="' + data.encId + '"> <i class="fa fa-edit" aria-hidden="true" style="font-size: large"></i></a>';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    function loadDatableWithFilteredUnitData() {

        var urlBuilder = "/inventory-ajax/vendor-get-filtered-data";

        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (result) {
            vendorTable.clear().draw();
            vendorTable.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }

    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    $(document).on("click", ".vendor-delete", function () {

        var deleteId = $("#encId").val($(this).closest("tr").find(".vendor-delete").attr("encId"));

        $.confirm({
            icon: 'ace-icon fa fa-exclamation-circle',
            theme: 'material',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',

            title: "Are you sure to delete this record ?",
            content: 'Once deleted , you will not be able to retrieve or undo that record,',
            typeAnimated: true,
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    action: function () {

                        $.ajax({
                            contentType: 'application/json',
                            url: "/inventory-ajax/delete-vendor/" + deleteId,
                            type: 'POST',
                            //async: false,
                            //data: JSON.stringify(answerDto),
                            dataType: 'json',
                            success: function (response) {

                                if (response === true) {
                                    showAlertByType("Delete Success", 'S');
                                    loadDatableWithFilteredUnitData();
                                    changeView('I');
                                }
                                else {
                                    showAlertByType("Unknown Error", 'F');
                                }

                            },
                            error: function (xhr, status, error) {
                                showAlertByType("Unknown Error", 'F');
                            }
                        });

                    }

                },
                CANCEL: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                        return false;
                    }
                }
            }

        });


    });
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $(document).on("click", ".vendor-edit", function () {
        // $(".asst-cat-edit").click(function () {
        $("#encId").val($(this).closest("tr").find(".vendor-edit").attr("encId"));


        changeView('U');
        loadFormData($(this).closest("tr").find(".vendor-edit").attr("encId"));


    });

    function loadFormData(encId) {
        if (isEmptyString(encId)) {
            showAlertByType("Unable to edit this data.", 'W');
        }
        else {

            const getUnitUrl = "/inventory-ajax/vendor-get/" + encId;
            $.ajax({
                url: getUnitUrl,
                type: 'GET',
                // data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (vendorDto) {
                    //console.log(assetDto);
                    if (vendorDto == null) {

                    }
                    else {
                        loadData(vendorDto);
                    }

                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
    }

    function loadData(vendorDto) {

        $("#encId").val(vendorDto.encId);

        $("#vendorCode").val(vendorDto.vendorCode);
        $("#vendorName").val(vendorDto.vendorName);
        $("#vendorType").val(vendorDto.vendorType);
        $("#address").val(vendorDto.address);
        $("#contactNo").val(vendorDto.contactNo);
        $("#authPerson").val(vendorDto.authPerson);
        $("#authContactNo").val(vendorDto.authContactNo);
        $("#authPersonDetails").val(vendorDto.authPersonDetails);
        $("#email").val(vendorDto.email);
        $("#webAddress").val(vendorDto.webAddress);
        $("#remarks").val(vendorDto.remarks);
        $("#blackList").val(vendorDto.blackList);
        $("#vendorQuality").val(vendorDto.vendorQuality);

        $("#isActiveFlg").val(vendorDto.isActiveFlg);

        $("#vendorCode").focus();
        $('.selectpicker').selectpicker('refresh');
    }
    /*-------------- update End ------------------------*/




});
