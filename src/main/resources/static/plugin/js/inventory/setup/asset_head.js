/**
 * Created by Mithun on 4/28/2020.
 */

$(document).ready(function () {

    clearform();
    changeView('I');
    //loadDatableWithFilteredAssetCatData();
    loadDatableWithFilteredAssetHeadData();

    var dateFrom = "";
    var dateTo = "";


    /*-------------- changeView function start ------------------------*/
    $("#addNewAsstBtnAdd").click(function () {
        changeView('N')
    });

    function changeView(viewStyle) {

        if (viewStyle == 'N') {
            $("#pnl-asset-head-info").css("display", "block");
            $("#pnl-asset-head-list").css("display", "none");
        }
        else if (viewStyle == 'I') { //new next
            $("#pnl-asset-head-info").css("display", "none");
            $("#pnl-asset-head-list").css("display", "block");
        }
        else if (viewStyle == 'U') { //new next
            $("#pnl-asset-head-info").css("display", "block");
            $("#pnl-asset-head-list").css("display", "none");
        }
        else if (viewStyle == 'S') {
            $("#pnl-asset-head-info").css("display", "none");
            $("#pnl-asset-head-list").css("display", "block");
        }
        else {
            $("#pnl-asset-head-info").css("display", "block");
            $("#pnl-asset-head-list").css("display", "none");
        }
        clearform();

    }

    /*-------------- changeView function End ------------------------*/


    /*-------------- asset save function start ------------------------*/
    $("#asstBtnSave").click(function () {
        saveData(0);

    });
    $("#asstBtnReset").click(function () {
        changeView('I')
    });

    function getFormData(isDeletedFlg) {
        var assetDto = {};

        assetDto.encId = $("#encId").val();
        assetDto.assetCategoryId = $("#assetCategoryId").val();
        assetDto.unitId = $("#unitId").val();
        assetDto.assetName = $("#assetName").val();
        assetDto.assetShortName = $("#assetShortName").val();
        assetDto.assetCode = $("#assetCode").val();
        assetDto.thresholdLevel = $("#thresholdLevel").val();
        assetDto.reorderLevel = $("#reorderLevel").val();
        assetDto.isActiveFlg = $("#isActiveFlg").val();
        assetDto.isDeletedFlg = isDeletedFlg;//0/1

        return assetDto;
    }

    function saveData(isDeletedFlg) {

        if (isValidated() || isDeletedFlg == 1) {
            const saveAssetUrl = "/inventory-ajax/save-asset-head-post";
            $.ajax({
                url: saveAssetUrl,
                type: 'POST',
                data: JSON.stringify(getFormData(isDeletedFlg)),
                contentType: 'application/json',
                async: false,
                success: function (commonMsg) {

                    if (commonMsg.isSucceed) {

                        clearform();
                        loadDatableWithFilteredAssetHeadData();
                        changeView('I');
                        showAlertByType(commonMsg.msg, 'S');

                    }
                    else {
                        showAlertByType(commonMsg.msg, 'F');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearform() {
        $("#encId").val('');
        $("#assetCategoryId").val('');
        $("#unitId").val('');
        $("#assetName").val('');
        $("#assetShortName").val('');
        $("#assetCode").val('');
        $("#thresholdLevel").val('');
        $("#reorderLevel").val('');
        $("#isDeletedFlg").val('');
        $("#isActiveFlg").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";


        clearSearchCreiteria();
        $('.selectpicker').selectpicker('refresh');

    }

    function clearSearchCreiteria() {
        dateFrom = "";
        dateTo = "";

        $("#src-coa-head").val('');
        $("#src-coa-head").val('');
        $("#src-traceable").val('');
        $("#src-status").val('');
        $("#date-range").val('');

    }

    /*-------------- asset save function End ------------------------*/


    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#assetCategoryId").val())) {

            valdiationMsgContent = "Asset Category Id is required!";
            $("#assetCategoryId").focus();
            return false;
        }
        else if (isEmptyString($("#assetName").val())) {

         valdiationMsgContent = "Asset name is required!";
         $("#assetName").focus();
         return false;
         }
         else if (isEmptyString($("#assetShortName").val())) {

         valdiationMsgContent = "Asset Short name is required!";
         $("#assetShortName").focus();
         return false;
         }
         else if (isEmptyString($("#assetCode").val())) {

         valdiationMsgContent = "Asset Code is required!";
         $("#assetCode").focus();
         return false;
         }
         else if (isEmptyString($("#thresholdLevel").val())) {

         valdiationMsgContent = "Threshold Level is required!";
         $("#thresholdLevel").focus();
         return false;
         }

         else if (isEmptyString($("#reorderLevel").val())) {

         valdiationMsgContent = "Reorder Level is required!";
         $("#reorderLevel").focus();
         return false;
         }

         else if (isEmptyString($("#isActiveFlg").val())) {

         valdiationMsgContent = "IsActiveFlg is required!";
         $("#isActiveFlg").focus();
         return false;
         }

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var assetHeadTable = $('#tbl-asset-head-lst').DataTable({

        data: [],
        "columns": [
            {"data": null, defaultContent: ''},
            {"data": "assetCode"},
            {"data": "assetName"},
            {"data": "assetShortName"},
            {"data": "assetCategoryId"},
            {"data": "thresholdLevel"},
            {"data": "reorderLevel"},
            {"data": "isActive"},
            {"data": null, defaultContent: ''}


        ],
        'columnDefs': [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    //console.log(data);
                    return '  <a class="btn btn-link  btn-round red asst-head-delete"   encId="' + data.encId + '" ><i class="fa fa-remove" aria-hidden="true"   style="font-size: large"></i></a>';
                }
            }, {
                'targets': 8,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round asst-head-edit"   encId="' + data.encId + '"> <i class="fa fa-edit" aria-hidden="true" style="font-size: large"></i></a>';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    function loadDatableWithFilteredAssetHeadData() {

        var flgQuestionMark = false;
        var urlBuilder = "/inventory-ajax/asset-head-get-filtered-data";

        if (!isEmptyString($("#srcAssetCategory").val())) {
            if (flgQuestionMark) {
                urlBuilder += '&srcAssetCategory=' + $("#srcAssetCategory").val();
            }
            else {
                urlBuilder += '?srcAssetCategory=' + $("#srcAssetCategory").val();
                flgQuestionMark = true;
            }
        }

        if (!isEmptyString($("#srcAssetHeadStatus").val())) {
            if (flgQuestionMark) {
                urlBuilder += '&srcAssetHeadStatus=' + $("#srcAssetHeadStatus").val();
            }
            else {
                urlBuilder += '?srcAssetHeadStatus=' + $("#srcAssetHeadStatus").val();
                flgQuestionMark = true;
            }
        }


        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (result) {
            assetHeadTable.clear().draw();
            assetHeadTable.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }

    /*-------------- Data Table End ------------------------*/


    $("#assetCatBtnSearch").click(function () {

        //  $('#tbl-asset-cat-lst-tbody').empty();
        loadDatableWithFilteredAssetHeadData();

    });
    $("#assetCatBtnClearSearchPanel").click(function () {

        //  $('#tbl-asset-cat-lst-tbody').empty();
        clearform();
        loadDatableWithFilteredAssetHeadData();

    });




/*-------------- Delete start ------------------------*/
    $(document).on("click", ".asst-head-delete", function () {
        // $(".asst-head-delete").click(function () {
        $("#encId").val($(this).closest("tr").find(".asst-head-delete").attr("encId"));
        $.confirm({
            icon: 'ace-icon fa fa-exclamation-circle',
            theme: 'material',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',

            title: "Are you sure to delete this record ?",
            content: 'Once deleted , you will not be able to retrieve or undo that record,',
            typeAnimated: true,
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    action: function () {
                        saveData(1);
                        changeView('I');

                    }

                },
                CANCEL: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                        return false;
                    }
                }
            }

        });


    });
/*-------------- Delete End ------------------------*/



/*-------------- update Start ------------------------*/
    $(document).on("click", ".asst-head-edit", function () {
        // $(".asst-cat-edit").click(function () {
        $("#encId").val($(this).closest("tr").find(".asst-head-edit").attr("encId"));


        changeView('U');
        loadFormData($(this).closest("tr").find(".asst-head-edit").attr("encId"));


    });

    function loadFormData(encId) {
        if (isEmptyString(encId)) {
            showAlertByType("Unable to edit this data.", 'W');
        }
        else {

            const getAssetUrl = "/inventory-ajax/asset-head-get/" + encId;
            $.ajax({
                url: getAssetUrl,
                type: 'GET',
                // data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (assetDto) {
                    //console.log(assetDto);
                    if (assetDto == null) {

                    }
                    else {
                        loadData(assetDto);
                    }

                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
    }

    function loadData(assetDto) {

        $("#encId").val(assetDto.encId);
        $("#assetCategoryId").val(assetDto.assetCategoryId);
        $("#unitId").val(assetDto.unitId);

        $("#assetCode").val(assetDto.assetCode);
        $("#assetName").val(assetDto.assetName);
        $("#assetShortName").val(assetDto.assetShortName);

        $("#thresholdLevel").val(assetDto.thresholdLevel);
        $("#reorderLevel").val(assetDto.reorderLevel);

        $("#isDeletedFlg").val(assetDto.isDeletedFlg);
        $("#isActiveFlg").val(assetDto.isActiveFlg);

        $("#category-name").focus();
        $('.selectpicker').selectpicker('refresh');
    }
/*-------------- update End ------------------------*/



    $(".code_generator").change(function () {

        var shortHeadName = "";
        var assetHeadName = $("#assetName").val();
        var assetHeadNameLength = 0;

        if (!isEmptyString(assetHeadName)) {
            assetHeadName = assetHeadName.toUpperCase();
            assetHeadNameLength = assetHeadName.trim().length;

            if (assetHeadNameLength > 2) {
                shortHeadName = assetHeadName.substring(0, 3);
            }
            else {
                assetHeadNameLength = assetHeadNameLength == 0 ? 0 : assetHeadNameLength - 1;
                shortHeadName = assetHeadName.substring(0, assetHeadNameLength - 1);
            }


            if ($("#assetCode").val().length < 1) {
                var d = new Date();

                var daySum = parseInt(d.getMonth()) + parseInt(d.getDate()) + parseInt(d.getFullYear());
                //console.log(parseInt(d.getMonth()));
                //console.log(parseInt(d.getDate()));
                //console.log(parseInt(d.getFullYear()));
                var catCode = shortHeadName + '-' + daySum + '' + Math.floor(Math.random() * 90 + 10);


                //  if($("#category-short-name").val().length<3) {
                $("#assetShortName").val(shortHeadName);
                $("#assetCode").val(catCode);
                //  }
            }


        }

    });





//////////////////////////////Date picker


    $("#date-range").daterangepicker({
        "autoApply": true,
        "showDropdowns": true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "linkedCalendars": true


    }, function (start, end, label) {
        dateFrom = start.format('DD/MM/YYYY'); //dd/MM/yyyy")
        dateTo = end.format('DD/MM/YYYY');
    });
    $("#date-range").val('');


});
