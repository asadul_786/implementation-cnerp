/**
 * Created by mithu on 11/5/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearform();
    changeView('I');
    loadDatableWithFilteredUnitData();


    /*-------------- date concerting function start ------------------------*/
    $(".tenderDatepicker").datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd/mm/yy'
        // ,
        // onClose: function(dateText, inst) {
        //     $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
        // }
    });
    /*-------------- date concerting function End ------------------------*/


    /*-------------- changeView function start ------------------------*/
    $("#addNewUnitBtnAdd").click(function () {
        changeView('N')
    });

    function changeView(viewStyle) {

        if (viewStyle == 'N') {
            $("#pnl-tender-info").css("display", "block");
            $("#pnl-tender-list").css("display", "none");
        }
        else if (viewStyle == 'I') { //new next
            $("#pnl-tender-info").css("display", "none");
            $("#pnl-tender-list").css("display", "block");
        }
        else if (viewStyle == 'U') { //new next
            $("#pnl-tender-info").css("display", "block");
            $("#pnl-tender-list").css("display", "none");
        }
        else if (viewStyle == 'S') {
            $("#pnl-tender-info").css("display", "none");
            $("#pnl-tender-list").css("display", "block");
        }
        else {
            $("#pnl-tender-info").css("display", "block");
            $("#pnl-tender-list").css("display", "none");
        }
        clearform();

    }

    /*-------------- changeView function End ------------------------*/


    /*-------------- asset save function start ------------------------*/
    $("#tenderBtnSave").click(function () {
        saveData(0);
    });
    $("#tenderBtnReset").click(function () {
        changeView('I');
    });

    function getFormData(isDeletedFlg) {

        var dateOfTenderApproval=$("#dateOfTenderApproval").val().split('/');
        var dateOfTenderPublish=$("#dateOfTenderPublish").val().split('/');
        var contractStartDate=$("#contractStartDate").val().split('/');
        var contractEndDate=$("#contractEndDate").val().split('/');
        var extensionDate=$("#extensionDate").val().split('/');

        var tenderDto = {};

        tenderDto.encId = $("#encId").val();

        tenderDto.refNo = $("#refNo").val();
        tenderDto.officeCd = $("#officeCd").val();
        tenderDto.dateOfTenderApproval = new Date(dateOfTenderApproval[2], dateOfTenderApproval[1] - 1, dateOfTenderApproval[0]);
        tenderDto.dateOfTenderPublish = new Date(dateOfTenderPublish[2], dateOfTenderPublish[1] - 1, dateOfTenderPublish[0]);
        tenderDto.approvalAuthority = $("#approvalAuthority").val();
        tenderDto.tenderDetails = $("#tenderDetails").val();
        tenderDto.tenderType = $("#tenderType").val();
        tenderDto.tenderAmount = $("#tenderAmount").val();
        tenderDto.vendor = $("#vendor").val();
        tenderDto.paymentMode = $("#paymentMode").val();
        tenderDto.purchaseItemHead = $("#purchaseItemHead").val();
        tenderDto.publishSource = $("#publishSource").val();
        tenderDto.terminate = $("#terminate").val();
        tenderDto.contractStartDate = new Date(contractStartDate[2], contractStartDate[1] - 1, contractStartDate[0]);
        tenderDto.contractEndDate = new Date(contractEndDate[2], contractEndDate[1] - 1, contractEndDate[0]);
        tenderDto.extensionDate = new Date(extensionDate[2], extensionDate[1] - 1, extensionDate[0]);

        tenderDto.isDeletedFlg =isDeletedFlg;
        tenderDto.isActiveFlg = $("#isActiveFlg").val();

        return tenderDto;
    }

    function saveData(isDeletedFlg) {

        if (isValidated() || isDeletedFlg == 1) {
            const saveUnitUrl = "/inventory-ajax/save-tender-post";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData(isDeletedFlg)),
                contentType: 'application/json',
                async: false,
                success: function (commonMsg) {

                    if (commonMsg.isSucceed) {

                        clearform();
                        loadDatableWithFilteredUnitData();
                        changeView('I');
                        showAlertByType(commonMsg.msg, 'S');

                    }
                    else {
                        showAlertByType(commonMsg.msg, 'F');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearform() {
        $("#encId").val('');

        $("#refNo").val('');
        $("#officeCd").val('');
        $("#dateOfTenderApproval").val('');
        $("#dateOfTenderPublish").val('');
        $("#approvalAuthority").val('');
        $("#tenderDetails").val('');
        $("#tenderType").val('');
        $("#tenderAmount").val('');
        $("#vendor").val('');
        $("#paymentMode").val('');
        $("#purchaseItemHead").val('');
        $("#publishSource").val('');
        $("#terminate").val('');
        $("#contractStartDate").val('');
        $("#contractEndDate").val('');
        $("#extensionDate").val('');

        $("#isActiveFlg").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/


    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#refNo").val())) {

            valdiationMsgContent = "Reference Number is required!";
            $("#refNo").focus();
            return false;
        }
        else if (isEmptyString($("#dateOfTenderApproval").val())) {

         valdiationMsgContent = "Tender approval date is required!";
         $("#dateOfTenderApproval").focus();
         return false;
         }
        else if (isEmptyString($("#dateOfTenderPublish").val())) {

            valdiationMsgContent = "Tender publish date is required!";
            $("#dateOfTenderPublish").focus();
            return false;
        }
        else if (isEmptyString($("#tenderAmount").val())) {

            valdiationMsgContent = "Tender amount is required!";
            $("#tenderAmount").focus();
            return false;
        }
        else if (isEmptyString($("#approvalAuthority").val())) {

            valdiationMsgContent = "Approval authority is required!";
            $("#approvalAuthority").focus();
            return false;
        }
        else if (isEmptyString($("#tenderType").val())) {

            valdiationMsgContent = "Tender type is required!";
            $("#tenderType").focus();
            return false;
        }
        else if (isEmptyString($("#vendor").val())) {

            valdiationMsgContent = "Vendor name is required!";
            $("#vendor").focus();
            return false;
        }
        else if (isEmptyString($("#contractStartDate").val())) {

            valdiationMsgContent = "Contract start date is required!";
            $("#contractStartDate").focus();
            return false;
        }
        else if (isEmptyString($("#purchaseItemHead").val())) {

            valdiationMsgContent = "Purchase item head is required!";
            $("#purchaseItemHead").focus();
            return false;
        }
        else if (isEmptyString($("#contractEndDate").val())) {

            valdiationMsgContent = "Contract end date is required!";
            $("#contractEndDate").focus();
            return false;
        }
        else if (isEmptyString($("#publishSource").val())) {

            valdiationMsgContent = "Publish source is required!";
            $("#publishSource").focus();
            return false;
        }
        else if (isEmptyString($("#terminate").val())) {

            valdiationMsgContent = "Terminate is required!";
            $("#terminate").focus();
            return false;
        }
        else if (isEmptyString($("#isDeletedFlg").val())) {

            valdiationMsgContent = "IsDeletedFlg is required!";
            $("#isDeletedFlg").focus();
            return false;
        }
        else if (isEmptyString($("#isActiveFlg").val())) {

            valdiationMsgContent = "IsActiveFlg is required!";
            $("#isActiveFlg").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var tenderTable = $('#tbl-tender-lst').DataTable({

        data: [],

        "columns": [
            {"data": null, defaultContent: ''},
            {"data": "purchaseItemHead"},
            {"data": "vendor"},
            {"data": "dateOfTenderPublish"},
            {"data": "refNo"},
            {"data": "isActiveFlg"},
            {"data": null, defaultContent: ''}

        ],
        'columnDefs': [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round red tender-delete"   encId="' + data.encId + '" ><i class="fa fa-remove" aria-hidden="true"   style="font-size: large"></i></a>';
                }
            }, {
                'targets': 6,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round tender-edit"   encId="' + data.encId + '"> <i class="fa fa-edit" aria-hidden="true" style="font-size: large"></i></a>';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    function loadDatableWithFilteredUnitData() {

        var urlBuilder = "/inventory-ajax/tender-get-filtered-data";

        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (result) {
            tenderTable.clear().draw();
            tenderTable.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }

    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    $(document).on("click", ".tender-delete", function () {

        $("#encId").val($(this).closest("tr").find(".tender-delete").attr("encId"));
        $.confirm({
            icon: 'ace-icon fa fa-exclamation-circle',
            theme: 'material',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',

            title: "Are you sure to delete this record ?",
            content: 'Once deleted , you will not be able to retrieve or undo that record,',
            typeAnimated: true,
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    action: function () {
                        saveData(1);
                        changeView('I');

                    }

                },
                CANCEL: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                        return false;
                    }
                }
            }

        });


    });
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $(document).on("click", ".tender-edit", function () {
        $("#encId").val($(this).closest("tr").find(".tender-edit").attr("encId"));


        changeView('U');
        loadFormData($(this).closest("tr").find(".tender-edit").attr("encId"));


    });

    function loadFormData(encId) {
        if (isEmptyString(encId)) {
            showAlertByType("Unable to edit this data.", 'W');
        }
        else {

            const getTenderUrl = "/inventory-ajax/tender-get/" + encId;
            $.ajax({
                url: getTenderUrl,
                type: 'GET',
                // data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (tenderDto) {
                    //console.log(assetDto);
                    if (tenderDto == null) {

                    }
                    else {
                        loadData(tenderDto);
                    }

                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
    }

    function loadData(tenderDto) {

       /* var dateOfTenderApproval=val(tenderDto.dateOfTenderApproval).split('/');
        alert(dateOfTenderApproval);
        var dateOfTenderApproval22= new Date(dateOfTenderApproval[2], dateOfTenderApproval[1] - 1, dateOfTenderApproval[0]);
        alert(dateOfTenderApproval22);*/

        $("#encId").val(tenderDto.encId);

        $("#refNo").val(tenderDto.refNo);
       $("#dateOfTenderApproval").val(tenderDto.dateOfTenderApproval);
      // $("#dateOfTenderApproval").val(dateOfTenderApproval22);
        $("#dateOfTenderPublish").val(tenderDto.dateOfTenderPublish);
        $("#approvalAuthority").val(tenderDto.approvalAuthority);
        $("#tenderDetails").val(tenderDto.tenderDetails);
        $("#tenderType").val(tenderDto.tenderType);
        $("#tenderAmount").val(tenderDto.tenderAmount);
        $("#vendor").val(tenderDto.vendor);
        $("#paymentMode").val(tenderDto.paymentMode);
        $("#purchaseItemHead").val(tenderDto.purchaseItemHead);
        $("#publishSource").val(tenderDto.publishSource);
        $("#terminate").val(tenderDto.terminate);
        $("#contractStartDate").val(tenderDto.contractStartDate);
        $("#contractEndDate").val(tenderDto.contractEndDate);
        $("#extensionDate").val(tenderDto.extensionDate);

        $("#isDeletedFlg").val(tenderDto.isDeletedFlg);
        $("#isActiveFlg").val(tenderDto.isActiveFlg);

        $("#refNo").focus();
        $('.selectpicker').selectpicker('refresh');
    }
    /*-------------- update End ------------------------*/




});
