/**
 * Created by Mithun on 4/28/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}

    $(document).ready(function () {
        clearform();
        changeView('I');
        loadDatableWithFilteredAssetCatData();

        var dateFrom = "";
        var dateTo = "";
///////Buttons
        $("#addNewAsstCatBtnAdd").click(function () {
            changeView('N')
        });
        $("#asstCatBtnSave").click(function () {
            saveData(0);

        });
        $("#asstCatBtnReset").click(function () {
            changeView('I')
        });
        $("#assetCatBtnSearch").click(function () {

            //  $('#tbl-asset-cat-lst-tbody').empty();
            loadDatableWithFilteredAssetCatData();

        });
        $("#assetCatBtnClearSearchPanel").click(function () {

            //  $('#tbl-asset-cat-lst-tbody').empty();
            clearform();
            loadDatableWithFilteredAssetCatData();

        });
/////////////////////////////////
        $(document).on("click", ".asst-cat-edit", function () {
            // $(".asst-cat-edit").click(function () {
            $("#category-encId").val($(this).closest("tr").find(".asst-cat-edit").attr("encId"));


            changeView('U');
            loadFormData($(this).closest("tr").find(".asst-cat-edit").attr("encId"));


        });
        $(document).on("click", ".asst-cat-delete", function () {
            // $(".asst-cat-delete").click(function () {
            $("#category-encId").val($(this).closest("tr").find(".asst-cat-delete").attr("encId"));
            $.confirm({
                icon: 'ace-icon fa fa-exclamation-circle',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'orange',

                title: "Are you sure to delete this record ?",
                content: 'Once deleted , you will not be able to retrieve or undo that record,',
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function () {
                            saveData(1);
                            changeView('I');

                            //alert($(this).attr("encId"));
                        }

                    },
                    CANCEL: {
                        text: 'Cancel',
                        btnClass: 'btn-warning',
                        action: function () {
                            return true;
                        }
                    }
                }

            });


        });

        $(".code_generator").change(function () {

            var shortCatName = "";
            var categoryName = $("#category-name").val();
            var categoryNameLength = 0;

            if (!isEmptyString(categoryName)) {
                categoryName = categoryName.toUpperCase();
                categoryNameLength = categoryName.trim().length;

                if (categoryNameLength > 2) {
                    shortCatName = categoryName.substring(0, 3);
                }
                else {
                    categoryNameLength = categoryNameLength == 0 ? 0 : categoryNameLength - 1;
                    shortCatName = categoryName.substring(0, categoryNameLength - 1);
                }


                if ($("#category-code").val().length < 1) {
                    var d = new Date();

                    var daySum = parseInt(d.getMonth()) + parseInt(d.getDate()) + parseInt(d.getFullYear());
                    //console.log(parseInt(d.getMonth()));
                    //console.log(parseInt(d.getDate()));
                    //console.log(parseInt(d.getFullYear()));
                    var catCode = shortCatName + '-' + daySum + '' + Math.floor(Math.random() * 90 + 10);


                    //  if($("#category-short-name").val().length<3) {
                    $("#category-short-name").val(shortCatName);
                    $("#category-code").val(catCode);
                    //  }
                }


            }

        });

/////functions//////************************************************************************
        function changeView(viewStyle) {

            if (viewStyle == 'N') {
                $("#pnl-asset-cat-info").css("display", "block");
                $("#pnl-asset-cat-list").css("display", "none");
            }
            else if (viewStyle == 'I') { //new next
                $("#pnl-asset-cat-info").css("display", "none");
                $("#pnl-asset-cat-list").css("display", "block");
            }
            else if (viewStyle == 'U') { //new next
                $("#pnl-asset-cat-info").css("display", "block");
                $("#pnl-asset-cat-list").css("display", "none");
            }
            else if (viewStyle == 'S') {
                $("#pnl-asset-cat-info").css("display", "none");
                $("#pnl-asset-cat-list").css("display", "block");
            }
            else {
                $("#pnl-asset-cat-info").css("display", "block");
                $("#pnl-asset-cat-list").css("display", "none");
            }
            clearform();

        }

        function loadFormData(catEncId) {
            if (isEmptyString(catEncId)) {
                showAlertByType("Unable to edit this data.", 'W');
            }
            else {

                const getAssetCatUrl = "/inventory-ajax/asset-category-get/" + catEncId;
                $.ajax({
                    url: getAssetCatUrl,
                    type: 'GET',
                    // data: JSON.stringify(getFormData()),
                    contentType: 'application/json',
                    async: false,
                    success: function (assetCategoryDto) {
                        //console.log(assetCategoryDto);
                        if (assetCategoryDto == null) {

                        }
                        else {
                            loadData(assetCategoryDto);
                        }

                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                    }

                });
            }
        }

        function saveData(isDeletedFlg) {

            if (isValidated() || isDeletedFlg == 1) {
                const saveAssetCatUrl = "/inventory-ajax/save-asset-category-post";
                $.ajax({
                    url: saveAssetCatUrl,
                    type: 'POST',
                    data: JSON.stringify(getFormData(isDeletedFlg)),
                    contentType: 'application/json',
                    async: false,
                    success: function (commonMsg) {
                        //console.log(commonMsg);

                        if (commonMsg.isSucceed) {


                            clearform();
                            loadDatableWithFilteredAssetCatData();
                            changeView('I');
                            showAlertByType(commonMsg.msg, 'S');

                        }
                        else {
                            showAlertByType(commonMsg.msg, 'F');
                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                    }

                });
            }
            else {

                customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
            }


        }

        function clearform() {
            $("#category-encId").val('');
            $("#category-name").val('');
            $("#category-short-name").val('');
            $("#category-code").val('');
            $("#coa-head").val('');
            $("#status").val('');
            $("#traceAble").val('');
            valdiationMsgTitle = "";
            valdiationMsgContent = "";


            clearSearchCreiteria();
            $('.selectpicker').selectpicker('refresh');

        }

        function clearSearchCreiteria() {
            dateFrom = "";
            dateTo = "";

            $("#src-coa-head").val('');
            $("#src-coa-head").val('');
            $("#src-traceable").val('');
            $("#src-status").val('');
            $("#date-range").val('');
            //clearform();
        }

        var valdiationMsgTitle;
        var valdiationMsgContent;

        function isValidated() {
            valdiationMsgTitle = "Required field validation!!!";
            valdiationMsgContent = "";
            if (isEmptyString($("#category-name").val())) {

                valdiationMsgContent = "Category name is required!";
                $("#category-name").focus();
                return false;
            }
            else if (isEmptyString($("#category-short-name").val())) {

                valdiationMsgContent = "Category short name is required!";
                $("#category-short-name").focus();
                return false;
            }
            else if ($("#category-short-name").val().trim().length != 3) {
                valdiationMsgTitle = "";
                valdiationMsgContent = "Category short name's must contain exactly 3 latter !";
                $("#category-short-name").focus();
                return false;
            }
            else if (isEmptyString($("#category-code").val())) {

                valdiationMsgContent = "Category code is required!";
                $("#category-code").focus();
                return false;
            }
            else if (isEmptyString($("#consumable").val())) {

                valdiationMsgContent = "Tending to consume  is required!";
                return false;
            }
            else if (isEmptyString($("#traceAble").val())) {

                valdiationMsgContent = "Traceability is required!";
                return false;
            }
            else if (isEmptyString($("#status").val())) {

                valdiationMsgContent = "Status is required!";
                return false;
            } else {
                return true;
            }

        }

        function getFormData(isDeletedFlg) {
            var assetCategoryDto = {};

            assetCategoryDto.encId = $("#category-encId").val();
            assetCategoryDto.categoryName = $("#category-name").val();
            assetCategoryDto.categoryShortName = $("#category-short-name").val();
            assetCategoryDto.categoryCode = $("#category-code").val();


            assetCategoryDto.chartOfAcctID = $("#coa-head").val(); //--ddl
            if (!isEmptyString($("#coa-head").val())) {

                assetCategoryDto.chartOfAccountHead = $("#coa-head option:selected").text();
            }


            assetCategoryDto.isConsumeableFlg = $("#consumable").val();
            assetCategoryDto.isTrackableFlg = $("#traceAble").val();
            assetCategoryDto.isActiveFlg = $("#status").val();
            assetCategoryDto.isDeletedFlg = isDeletedFlg;//0/1


            return assetCategoryDto
        }

        function loadData(assetCategoryDto) {
            //console.log(assetCategoryDto);
            $("#category-encId").val(assetCategoryDto.encId);
            $("#category-name").val(assetCategoryDto.categoryName);
            $("#category-short-name").val(assetCategoryDto.categoryShortName);
            $("#category-code").val(assetCategoryDto.categoryCode);
            $("#coa-head").val(assetCategoryDto.chartOfAcctID); //--ddl


            $("#consumable").val(assetCategoryDto.isConsumeableFlg);
            $("#traceAble").val(assetCategoryDto.isTrackableFlg);
            $("#status").val(assetCategoryDto.isActiveFlg);

            $("#category-name").focus();
            $('.selectpicker').selectpicker('refresh');
        }

///////////////////////////Data table
        var assetCatTable = $('#tbl-asset-cat-lst').DataTable({

            data: [],
            "columns": [
                {"data": null, defaultContent: ''},
                {"data": "categoryCode"},
                {"data": "categoryName"},
                {"data": "categoryShortName"},
                {"data": "chartOfAccountHead"},
                {"data": "consumeableStatus"},
                {"data": "trackableStatus"},
                {"data": "activeStatus"},
                {"data": null, defaultContent: ''}
            ],
            'columnDefs': [
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta) {
                        //console.log(data);
                        return '  <a class="btn btn-link  btn-round red asst-cat-delete"   encId="' + data.encId + '" ><i class="fa fa-remove" aria-hidden="true"   style="font-size: large"></i></a>';
                    }
                }, {
                    'targets': 8,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta) {
                        return '  <a class="btn btn-link  btn-round asst-cat-edit"   encId="' + data.encId + '"> <i class="fa fa-edit" aria-hidden="true" style="font-size: large"></i></a>';
                    }
                }
            ],
            'order': [[1, 'asc']]
        });

        function loadDatableWithFilteredAssetCatData() {

            var flgQuestionMark = false;
            var urlBuilder = "/inventory-ajax/asset-category-get-filtered-data";

            if (!isEmptyString($("#src-coa-head").val())) {
                if (flgQuestionMark) {
                    urlBuilder += '&coaId=' + $("#src-coa-head").val();
                }
                else {
                    urlBuilder += '?coaId=' + $("#src-coa-head").val();
                    flgQuestionMark = true;
                }
            }
            if (!isEmptyString($("#src-consumable").val())) {
                if (flgQuestionMark) {
                    urlBuilder += '&conmableVal=' + $("#src-consumable").val();
                }
                else {
                    urlBuilder += '?conmableVal=' + $("#src-consumable").val();
                    flgQuestionMark = true;
                }
            }
            if (!isEmptyString($("#src-traceable").val())) {
                if (flgQuestionMark) {
                    urlBuilder += '&traceableVal=' + $("#src-traceable").val();
                }
                else {
                    urlBuilder += '?traceableVal=' + $("#src-traceable").val();
                    flgQuestionMark = true;
                }
            }
            if (!isEmptyString($("#src-status").val())) {
                if (flgQuestionMark) {
                    urlBuilder += '&activeVal=' + $("#src-status").val();
                }
                else {
                    urlBuilder += '?activeVal=' + $("#src-status").val();
                    flgQuestionMark = true;
                }
            }
            if (!isEmptyString(dateFrom) && !isEmptyString(dateTo)) {
                if (flgQuestionMark) {
                    urlBuilder += '&fromDate=' + dateFrom + '&toDate=' + dateTo;
                }
                else {
                    urlBuilder += '?fromDate=' + dateFrom + '&toDate=' + dateTo;
                    flgQuestionMark = true;
                }

            }


            $.ajax({
                url: urlBuilder,
                type: "get"

            }).done(function (result) {
                assetCatTable.clear().draw();
                assetCatTable.rows.add(result).draw();
            }).fail(function (jqXHR, textStatus, errorThrown) {

            });
        }

//////////////////////////////Date picker

        //
        // $("#date-range").daterangepicker({
        //     "autoApply": true,
        //     "showDropdowns": true,
        //     locale: {
        //         format: 'DD-MM-YYYY'
        //     },
        //     ranges: {
        //         'Today': [moment(), moment()],
        //         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //         'This Month': [moment().startOf('month'), moment().endOf('month')],
        //         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        //     },
        //     "linkedCalendars": true
        //
        //
        // }, function (start, end, label) {
        //     dateFrom = start.format('DD/MM/YYYY'); //dd/MM/yyyy")
        //     dateTo = end.format('DD/MM/YYYY');
        // });
        // $("#date-range").val('');


    });
