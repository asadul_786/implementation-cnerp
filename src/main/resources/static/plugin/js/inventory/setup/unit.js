/**
 * Created by mithu on 11/5/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearform();
    changeView('I');
    loadDatableWithFilteredUnitData();


    /*-------------- changeView function start ------------------------*/
    $("#addNewUnitBtnAdd").click(function () {
        changeView('N')
    });

    function changeView(viewStyle) {

        if (viewStyle == 'N') {
            $("#pnl-unit-info").css("display", "block");
            $("#pnl-unit-list").css("display", "none");
        }
        else if (viewStyle == 'I') { //new next
            $("#pnl-unit-info").css("display", "none");
            $("#pnl-unit-list").css("display", "block");
        }
        else if (viewStyle == 'U') { //new next
            $("#pnl-unit-info").css("display", "block");
            $("#pnl-unit-list").css("display", "none");
        }
        else if (viewStyle == 'S') {
            $("#pnl-unit-info").css("display", "none");
            $("#pnl-unit-list").css("display", "block");
        }
        else {
            $("#pnl-unit-info").css("display", "block");
            $("#pnl-unit-list").css("display", "none");
        }
        clearform();

    }

    /*-------------- changeView function End ------------------------*/


    /*-------------- asset save function start ------------------------*/
    $("#unitBtnSave").click(function () {
        saveData(0);
    });
    $("#unitBtnReset").click(function () {
        changeView('I');
    });

    function getFormData(isDeletedFlg) {
        var unitDto = {};

        unitDto.encId = $("#encId").val();

        unitDto.caption = $("#caption").val();
        unitDto.description = $("#description").val();
        unitDto.unitOf = $("#unitOf").val();

        unitDto.isDeletedFlg =isDeletedFlg;
        unitDto.isActiveFlg = $("#isActiveFlg").val();

        return unitDto;
    }

    function saveData(isDeletedFlg) {

        if (isValidated() || isDeletedFlg == 1) {
            const saveUnitUrl = "/inventory-ajax/save-unit-post";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData(isDeletedFlg)),
                contentType: 'application/json',
                async: false,
                success: function (commonMsg) {

                    if (commonMsg.isSucceed) {

                        clearform();
                        loadDatableWithFilteredUnitData();
                        changeView('I');
                        showAlertByType(commonMsg.msg, 'S');

                    }
                    else {
                        showAlertByType(commonMsg.msg, 'F');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearform() {
        $("#encId").val('');
        $("#caption").val('');
        $("#description").val('');
        $("#unitOf").val('');

        $("#isActiveFlg").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/


    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#caption").val())) {

            valdiationMsgContent = "Caption is required!";
            $("#caption").focus();
            return false;
        }
        else if (isEmptyString($("#unitOf").val())) {

         valdiationMsgContent = "UnitOf is required!";
         $("#unitOf").focus();
         return false;
         }

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var unitTable = $('#tbl-unit-lst').DataTable({

        data: [],
        "columns": [
            {"data": null, defaultContent: ''},
            {"data": "caption"},
            {"data": "description"},
            {"data": "unitOf"},
            {"data": "isActive"},
            {"data": null, defaultContent: ''}

        ],
        'columnDefs': [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round red unit-delete"   encId="' + data.encId + '" ><i class="fa fa-remove" aria-hidden="true"   style="font-size: large"></i></a>';
                }
            }, {
                'targets': 5,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round unit-edit"   encId="' + data.encId + '"> <i class="fa fa-edit" aria-hidden="true" style="font-size: large"></i></a>';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    function loadDatableWithFilteredUnitData() {

        var urlBuilder = "/inventory-ajax/unit-get-filtered-data";

        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (result) {
            unitTable.clear().draw();
            unitTable.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }

    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    $(document).on("click", ".unit-delete", function () {

        $("#encId").val($(this).closest("tr").find(".unit-delete").attr("encId"));
        $.confirm({
            icon: 'ace-icon fa fa-exclamation-circle',
            theme: 'material',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',

            title: "Are you sure to delete this record ?",
            content: 'Once deleted , you will not be able to retrieve or undo that record,',
            typeAnimated: true,
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    action: function () {
                        saveData(1);
                        changeView('I');

                    }

                },
                CANCEL: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                        return false;
                    }
                }
            }

        });


    });
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $(document).on("click", ".unit-edit", function () {
        // $(".asst-cat-edit").click(function () {
        $("#encId").val($(this).closest("tr").find(".unit-edit").attr("encId"));


        changeView('U');
        loadFormData($(this).closest("tr").find(".unit-edit").attr("encId"));


    });

    function loadFormData(encId) {
        if (isEmptyString(encId)) {
            showAlertByType("Unable to edit this data.", 'W');
        }
        else {

            const getUnitUrl = "/inventory-ajax/unit-get/" + encId;
            $.ajax({
                url: getUnitUrl,
                type: 'GET',
                // data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (unitDto) {
                    //console.log(assetDto);
                    if (unitDto == null) {

                    }
                    else {
                        loadData(unitDto);
                    }

                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
    }

    function loadData(unitDto) {

        $("#encId").val(unitDto.encId);

        $("#caption").val(unitDto.caption);
        $("#description").val(unitDto.description);
        $("#unitOf").val(unitDto.unitOf);



        $("#isActiveFlg").val(unitDto.isActiveFlg);

        $("#caption").focus();
        $('.selectpicker').selectpicker('refresh');
    }
    /*-------------- update End ------------------------*/




});
