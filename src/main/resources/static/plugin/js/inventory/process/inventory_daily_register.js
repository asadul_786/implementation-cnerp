/**
 * Created by Mithun on 5/14/2020.
 */
$(document).ready(function () {
    loadCurrentRegisterTblData();
    //event handeler
    $("#product").change(function () {
        searchProduct();
    });
    $("#transaction-type").change(function () {
        loadRefDddl();
    });

    $("#assetCategory").on('change', function () {
        loadAssetHeadData(true);

    });
    $(document).on("change", "#asset", function () {
    // $("#asset").on('change', function () {
        searchProduct();

    });
    $("#lnk-btn-search-panel-show").on('click', function () {
        $("#hideaway-search-panel").css("display","block")

    });
    $("#lnk-btn-search-panel-hide").on('click', function () {
        $("#hideaway-search-panel").css("display","none")

    });
    $("#btnClosePopup").click(function () {
        bootstrapModalClose();
    })


    ///////////////////////////////////Functions
    function loadAssetHeadData(isSearchPanelReq) {
        var assetDdl= '<option value="">Select Asset</option>';
        var urlBuilder ="/inventory-ajax/asset-head-get-filtered-data?srcAssetCategory=";
        urlBuilder+= isSearchPanelReq?$("#assetCategory").val():$("#ddlNewProductassetCat").val();

        urlBuilder+= "&srcAssetHeadStatus=1";
        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (listItems) {

            //
            if(isSearchPanelReq){
                $("#asset").empty();
            }
            else{
                $("#ddlNewProductasset").empty();
            }
            // assetLst=listItems;
            for(var i=0 ; i<listItems.length;i++){
                // alert(listItems[i].id);
                assetDdl+= '<option value="'+listItems[i].id+'"' +
                    // ' asstCode="'+listItems[i].assetCode+'" unit="'+listItems[i].unitId+'"' +
                    // 'thresholdLevel="'+listItems[i].thresholdLevel+'"' +
                    // 'reorderLevel="'+listItems[i].reorderLevel+'"' +
                    '>'
                    +listItems[i].assetName+'</option>';
            }

            if(isSearchPanelReq){
                $("#asset").html(assetDdl);
            }
            else {
                $("#ddlNewProductasset").html(assetDdl);
            }

            $('.selectpicker').selectpicker('refresh');


        }).fail(function (jqXHR, textStatus, errorThrown) {

            if(isSearchPanelReq){
                $("#asset").empty();
                $("#asset").html(assetDdl);
            }
            else {
                $("#ddlNewProductasset").empty();
                $("#ddlNewProductasset").html(assetDdl);
            }


            $('.selectpicker').selectpicker('refresh');

        });
    }
    function loadCurrentRegisterTblData(){
        $('body').append('<div id="requestOverlay" class="request-overlay"></div>');
        $("#requestOverlay").show();
        var urlBuilder = "/inventory-ajax/get-current-register";
        $.ajax({
            url: urlBuilder,
            type: "get"

        }).done(function (result) {
            tblCurrentRegister.clear().draw();
            tblCurrentRegister.rows.add(result).draw();
            $("#requestOverlay").remove();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            $("#requestOverlay").remove();
        });
    }
    function  loadRefDddl() {

        var refDdl= '<option value="">Select one</option>';
        var urlBuilder ="/inventory-ajax/get-ref-tracking-list?transactionType="+ $("#transaction-type").val();
        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (listItems) {

            $("#ddl-ref-tracking").empty();
            // refDdl=listItems;
            for(var i=0 ; i<listItems.length;i++){


                refDdl += '<option value="' + listItems[i].dropDownValue + '">'
                    + listItems[i].dropDownText + '</option>';

            }


            $("#ddl-ref-tracking").html(refDdl);
            $('.selectpicker').selectpicker('refresh');


        }).fail(function (jqXHR, textStatus, errorThrown) {

            $("#ddl-ref-tracking").empty();
            $("#ddl-ref-tracking").html(refDdl);
            $('.selectpicker').selectpicker('refresh');

        });

    }
    function  searchProduct() {
        var productCodeOrName = $("#product-code").val();
        var assetID = $("#asset").val();

        var urlBuilder = "/inventory-ajax/get-filtered-product-list?";
        var productCodeName = $("#product").val();
        var assetId = $("#asset").val();
        var hasProductNamePart = false;
        if (!isEmptyString(productCodeName)) {
            urlBuilder += "srcProductCode=" + productCodeName + "&srcProductName=" + productCodeName;
            hasProductNamePart = true;
        }

        if (isPositiveNumericNum(assetId)) {
            if (hasProductNamePart) {
                urlBuilder += "&";
            }
            urlBuilder += "assetId=" + assetId;
        }

        var tblHrml = "";


        $.ajax({
            url: urlBuilder,
            type: "GET",
            async: true

        }).done(function (listItems) {
            console.log(listItems);
            for (var i = 0; i < listItems.length; i++) {
                tblHrml += "<tr class='clickable-product-row' product-id='" + listItems[i].id + "' unit='" + listItems[i].unitCaption + "'  product-code='"+listItems[i].productCode +"'"  +
                    "product-det='" +
                    "Product-" + listItems[i].productName + "," +
                    "Code-" + listItems[i].productCode + "," +
                    "Asset-" + listItems[i].assetName + "," +
                    "Asset-code-" + listItems[i].assetCode + "," +
                    "Brand-" + listItems[i].brandName + "," +
                    "Model-" + listItems[i].productModel + "," + "'>" +

                    "<td>" + listItems[i].productCode + "</td>" +
                    "<td>" + listItems[i].productName + "</td>" +
                    "<td>" + listItems[i].assetCode + "</td>" +
                    "<td>" + listItems[i].assetName + "</td>" +
                    "<td>" + listItems[i].brandName + "</td>" +
                    "<td>" + listItems[i].productModel + "</td>" +


                    "</tr>";


            }
            if(!isEmptyString(tblHrml)){
                popupTheTable(tblHrml);
            }
            else{
                forFilteredNoDataFound();
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            tblHrml = "";
        });
    };
    function forFilteredNoDataFound(){
        $.confirm({
            icon: 'ace-icon fa fa-exclamation-circle',
            theme: 'material',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',

            title:"No data were found",
            content:"Do you want to add new Product",
            typeAnimated: true,
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-blue',
                    action: function () {
                       // newProduct
                        $("#exampleModal").modal("hide");
                        $("#newProduct").modal("show");
                    }
                },

                Cancel: {
                    text:'cancel',
                    btnClass: 'btn-danger',
                    action: function () {
                        return true;
                    }


                }
            }

        });
    }
    function   popupTheTable(tblHrml) {
        bootstrapModalClose();
    var dynContent = ' <div>  ' +
        '<div class="alert alert-info"> <i class="ace-icon fa fa-hand-o-right"></i> ' +
        '<strong>Note :</strong> This list is auto generated depend on your search criteria :( Please select one of them.)</div>' +
        '<table class="table table-bordered table-striped"><thead class="thin-border-bottom"> ' +
        '' +
        '<tr> ' +

        '<th><i class="ace-icon fa fa-caret-right blue"></i>Product Code</th> ' +
        '<th><i class="ace-icon fa fa-caret-right blue"></i>Product Name</th> ' +
        '<th><i class="ace-icon fa fa-caret-right blue"></i>Asset Code</th> ' +
        '<th><i class="ace-icon fa fa-caret-right blue"></i>Asset Name</th> ' +
        '<th><i class="ace-icon fa fa-caret-right blue"></i>Brand Name</th> ' +
        '<th><i class="ace-icon fa fa-caret-right blue"></i>Model</th> ' +
        '' +
        '</tr></thead> <tbody>' + tblHrml + '</tbody> </table>';


    var jqConfirmDialog = $.confirm({
        icon: 'ace-icon fa fa-smile-o',
        theme: 'material',
        closeIcon: true,
        animation: 'scale',
        type: 'blue',
        boxWidth: '50%',
        useBootstrap: false,
        title:  'Product List',
        content: dynContent,
        typeAnimated: true,
        confirmButtonClass: 'hide',
        buttons: {
            Yes: {

                isHidden: true, // hide the button
                action: function () {
                }
            }
        }

    });
    $(document).on("click", ".clickable-product-row", function () {
       $("#unit").html( $(this).closest("tr").attr("unit"));
       $("#product-name").val( $(this).closest("tr").attr("product-det"));
       $("#product-code").val($(this).closest("tr").attr("product-code"));
       $("#product-id").val($(this).closest("tr").attr("product-id"));
        jqConfirmDialog.close();
        bootstrapModalClose();
    });

}
    function  bootstrapModalClose() {
    $("#exampleModal").modal("hide");
    $("#product").val("");
     $("#assetCategory").val("");
     $("#asset").val("");
    $('.selectpicker').selectpicker('refresh');

}
    function  bootstrapNewProductModalClose() {
    $("#newProduct").modal("hide");
    // $("#product").val("");
    //  $("#assetCategory").val("");
    //  $("#asset").val("");
    // $('.selectpicker').selectpicker('refresh');

}


////////////////////////////////Common Processing unit/////////////////////////////

    var valdiationMsgTitle="";
    var valdiationMsgContent="";
    $(document).on("click", "#tbl-asset-current-register>tbody>tr", function () {
            $(this).addClass("selected").siblings().removeClass("selected");
            $("#unit").html( $(this).closest("tr").find(".process-data").attr("unit"));
            $("#product-name").val( $(this).closest("tr").find(".process-data").attr("product-det"));
            $("#product-code").val($(this).closest("tr").find(".process-data").attr("product-code"));
            $("#product-id").val($(this).closest("tr").find(".process-data").attr("product-id"));

            $("#dailyRegisterUpdate").css("display","block");
        $(".new-product-search").css("display","none")


    });
    $("#btn-register-product").click(function (){

        saveRegister();
    });
    $("#new-product-register").click(function (){
        $("#dailyRegisterUpdate").css("display","block");
        $("#tbl-asset-current-register").removeClass("selected");
        $(".new-product-search").css("display","inline");
        clearData();

    });

   function  ddlDisabled(isnewProduct){
       if(isnewProduct){

       }

    }

    $("#btn-reset").click(function (){
        $("#tbl-asset-current-register").removeClass("selected");
        $("#dailyRegisterUpdate").css("display","none");
        clearData();
    });
    function clearData(){
        $("#unit").html( "unit");
        $("#product-name").val( "");
        $("#product-code").val("");
        $("#product-id").val("");

        $("#transaction-type").val("");
        $("#ddl-ref-tracking").val("");
        $("#remarks").val("");
        $("#product-qnty").val("");
        bootstrapModalClose();
    }
    function saveRegister( ){
        $('body').append('<div id="requestOverlay" class="request-overlay"></div>');
        $("#requestOverlay").show();
        if (validatedFormData()) {
            const saveAssetCatUrl = "/inventory-ajax/office-wise-inv-daily-register-update-post";
            $.ajax({
                url: saveAssetCatUrl,
                type: 'POST',
                data: JSON.stringify(getMapData()),
                contentType: 'application/json',
                async: false,
                success: function (commonMsg) {
                    //console.log(commonMsg);

                    if (commonMsg.isSucceed) {


                        showAlertByType(commonMsg.msg, 'S');
                        loadCurrentRegisterTblData();

                        $("#requestOverlay").remove();
                        $("#btn-reset").trigger( "click" );
                    }
                    else {
                        $("#requestOverlay").remove();
                        showAlertByType(commonMsg.msg==null?"Please try agin later":commonMsg.msg, 'F');

                    }

                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                    $("#requestOverlay").remove();

                }

            });
        }
        else {
            $("#requestOverlay").remove();
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }
    }
    function validatedFormData(){                  //TODO:
        return true;
    }
    function    validateProduct() {                   //TODO:
        return true;
    }
    function getMapData() {
        // var datePickerData=$("#purchaseDate").val().split('/');
        // var inventoryProduct={};
        //
        //
        // inventoryProduct.encId=$("").val();
        // inventoryProduct.productName=$("#productName").val();
        // inventoryProduct.productModel=$("#model").val();
        // inventoryProduct.productCode=$("#productCode").val();
        // inventoryProduct.assetId=$("#asset").val();
        // inventoryProduct.brandName=$("#brandName").val();
        // inventoryProduct.unitPrice=$("#unitPrice").val();
        // inventoryProduct.purchaseDate=new Date(datePickerData[2], datePickerData[1] - 1, datePickerData[0]);//$("#purchaseDate").val();
        // inventoryProduct.warrantyDetails=$("#warrantyDetails").val();
        // inventoryProduct.tenderId=$("#tender").val();
        // inventoryProduct.tenderRefNo=$("#tender option:selected").text();
        // inventoryProduct.remarks=$("#remarks").val();

        var inventoryDto = {};
        inventoryDto.quantity =  $("#product-qnty").val();
        inventoryDto.productId= $("#product-id").val();
        inventoryDto.transactionType=$("#transaction-type").val();
        inventoryDto.refRegister=$("#ddl-ref-tracking").val();
        inventoryDto.remarks=$("#remarks").val();

        // inventoryDto.inventoryProductDto=inventoryProduct;
        return inventoryDto
    }
    function  setNewProductMapData() {
        if(validateProduct()){
            $("#newProduct").modal("hide");
        }
        else{

        }

    }

/////////////////////////////
    $("#btnClosenewProductPopup").click(function () {
        bootstrapNewProductModalClose();
    })
    $("#btn-add-new-product").click(function () {
        setNewProductMapData();
    })
    $(document).on("change", "#ddlNewProductassetCat" ,function () {
        loadAssetHeadData(false);

        // $("#assetCatCode").val($("#assetCategory option:selected").text());
    });







    //Datat Tables
    var tblCurrentRegister = $('#tbl-asset-current-register').DataTable(  {
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'csvHtml5'
            ],
            data: [],
            "columns": [

                //Product
                {"data": null, defaultContent: ''},
                {"data": "catName"},
                {"data": "asstCode"},
                {"data": "assetHead"},
                {"data": "productCode"},
                {"data": "productName"},
                {"data": "productModel"},
                {"data": "brandName"},
                {"data": "qnty"},
                {"data": "opningQnty"},
                {"data": "unit"}

            ],
        'columnDefs': [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return "<span class='process-data'  unit='"+data.unit+"' product-id='"+data.productId+"' product-code='"+data.productCode+"'  product-det='" +
                    "Product-" + data.productName + "," +
                    // "Code-" + data.productCode + "," +
                    "Asset-" + data.assetHead + "," +
                    "Asset-code-" + data.asstCode + "," +
                    "Brand-" + data.brandName + "," +
                    "Model-" + data.productModel + "'" +" >"+data.catCode+"  </span>";
                }
            }
        ],
            'order': [[1, 'asc']]
        });
    $("#purchaseDate").datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd/mm/yy'
        // ,
        // onClose: function(dateText, inst) {
        //     $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
        // }
    });
});