/**
 * Created by Mithun on 5/5/2020.
 */
$(document).ready(function () {
    var valdiationMsgTitle="";
    var valdiationMsgContent="";
    $("#purchaseDate").datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd/mm/yy'
        // ,
        // onClose: function(dateText, inst) {
        //     $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
        // }
    });

    //Datatable
    // var tblCurrentRegister = $('#tbl-asset-current-register').DataTable(
    //
    //     {
    //         dom: 'Bfrtip',
    //         buttons: [
    //             'copyHtml5',
    //
    //             'csvHtml5'
    //
    //         ],
    //     data: [],
    //     "columns": [
    //         {"data": "assetCategory"},
    //         {"data": "assetCatCode"},
    //         {"data": "assetHead"},
    //         {"data": "assetCode"},
    //         {"data": "qnty"},
    //         {"data": "thresholdLevel"},
    //         {"data": "reorderLevel"},
    //         {"data": "assetUnit"}
    //
    //     ],
    //     'order': [[1, 'asc']]
    // });

    var tblCurrentRegister = $('#tbl-asset-current-register').DataTable(  {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'csvHtml5'
        ],
        data: [],
        "columns": [

            //Product
            {"data": null, defaultContent: ''},
            {"data": "catName"},
            {"data": "asstCode"},
            {"data": "assetHead"},
            {"data": "productCode"},
            {"data": "productName"},
            {"data": "productModel"},
            {"data": "brandName"},
            {"data": "qnty"},
            {"data": "opningQnty"},
            {"data": "unit"}

        ],
        'columnDefs': [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return "<span class='process-data'  unit='"+data.unit+"' product-id='"+data.productId+"' product-code='"+data.productCode+"'  product-det='" +
                        "Product-" + data.productName + "," +
                        // "Code-" + data.productCode + "," +
                        "Asset-" + data.assetHead + "," +
                        "Asset-code-" + data.asstCode + "," +
                        "Brand-" + data.brandName + "," +
                        "Model-" + data.productModel + "'" +" >"+data.catCode+"  </span>";
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    //END here: Datatable
     clear();
    loadCurrentRegisterTblData();
    var assetLst = [];
    $('#add-new-asset').click(function (e) {


    });
    $("#assetCategory").on('change', function () {
        loadAssetHeadData();
        // $("#assetCatCode").val($("#assetCategory option:selected").text());
    });
    $("#asset").on('change', function () {
        setAssetRelatedData();
    });

    $("#btn-save-register").on('click', function () {
        if (validatedFormData()) {
            const saveAssetCatUrl = "/inventory-ajax/office-wise-inv-opening-post";
            $.ajax({
                url: saveAssetCatUrl,
                type: 'POST',
                data: JSON.stringify(getMapData()),
                contentType: 'application/json',
                async: false,
                success: function (commonMsg) {
                    //console.log(commonMsg);

                    if (commonMsg.isSucceed) {

                        $.confirm({
                            icon: 'ace-icon fa fa-exclamation-circle',
                            theme: 'material',
                            closeIcon: true,
                            animation: 'scale',
                            type: 'orange',

                            title: "Info[Success]",
                            content: commonMsg.msg+' Are you want to add more?',
                            typeAnimated: true,
                            buttons: {
                                Yes: {
                                    text: 'Yes',
                                    btnClass: 'btn-danger',
                                    action: function () {
                                        $(".hideablePanel").css("display", "block");
                                        clear();
                                        loadCurrentRegisterTblData();
                                    }

                                },
                                CANCEL: {
                                    text: 'Cancel',
                                    btnClass: 'btn-warning',
                                    action: function () {
                                        $(".hideablePanel").css("display", "none");
                                        clear();
                                        loadCurrentRegisterTblData();
                                    }
                                }
                            }

                        });


                    }
                    else {
                        showAlertByType(commonMsg.msg==null?"Please try agin later":commonMsg.msg, 'F');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }
    });
    $("#resetPanel").on('click', function () {
        clear();

    });
    $("#addNewRegister").on('click', function (){

        $(".hideablePanel").css("display", "block");
    });
    $("#cancelPanel").on('click', function (){

        $(".hideablePanel").css("display", "none");
        clear();
    });
    $("#productName").change(function () {

        var shortproductName = "";
        var productName = $("#productName").val();
        var productNameLength = 0;

        if (!isEmptyString(productName)) {
            productName = productName.toUpperCase();
            productNameLength = productName.trim().length;

            if (productNameLength > 2) {
                shortproductName = productName.substring(0, 3);
            }
            else {
                productNameLength = productNameLength == 0 ? 0 : productNameLength - 1;
                shortproductName = productName.substring(0, productNameLength - 1);
            }



                var d = new Date();

                var daySum = parseInt(d.getMonth()) + parseInt(d.getDate()) + parseInt(d.getFullYear());

                var productCode = shortproductName + '-' + daySum + '' + Math.floor(Math.random() * 90 + 10);



                $("#productCode").val(productCode);





        }

    });
    function validatedFormData() {
        if (!isPositiveNumericNum($("#qnty").val())) {

            valdiationMsgTitle="Invalid Data";
            valdiationMsgContent="Quantity is required with valid positive number.";
            return false;
        }else   if (!isPositiveNumericNum($("#unitPrice").val())){
            valdiationMsgTitle="Required";
            valdiationMsgContent="Invalid Price!";
            return false;
        }else   if (isEmptyString($("#asset").val())){
            valdiationMsgTitle="Required";
            valdiationMsgContent="Please select a asset head first!";
            return false;
        }else   if (isEmptyString($("#productName").val())){
            valdiationMsgTitle="Required";
            valdiationMsgContent="Product name can't be blank!";
            return false;
        }else   if (isEmptyString($("#tender").val())){
            valdiationMsgTitle="Required";
            valdiationMsgContent="Please select a Tender  first!";
            return false;
        }else   if (isEmptyString($("#brandName").val())){
            valdiationMsgTitle="Required";
            valdiationMsgContent="Brand Name is required!";
            return false;
        }else   if (isEmptyString($("#purchaseDate").val())){
            valdiationMsgTitle="Required";
            valdiationMsgContent="Purchase Date is required!";
            return false;
        }
        else{
            return true;
        }
    }
    function getMapData() {

        var datePickerData=$("#purchaseDate").val().split('/');
        var inventoryProduct={};


        inventoryProduct.encId=$("").val();
        inventoryProduct.productName=$("#productName").val();
        inventoryProduct.productModel=$("#model").val();
        inventoryProduct.productCode=$("#productCode").val();
        inventoryProduct.assetId=$("#asset").val();
        inventoryProduct.brandName=$("#brandName").val();
        inventoryProduct.unitPrice=$("#unitPrice").val();
        inventoryProduct.purchaseDate=new Date(datePickerData[2], datePickerData[1] - 1, datePickerData[0]);//$("#purchaseDate").val();
        inventoryProduct.warrantyDetails=$("#warrantyDetails").val();
        inventoryProduct.tenderId=$("#tender").val();
        inventoryProduct.tenderRefNo=$("#tender option:selected").text();
        inventoryProduct.remarks=$("#remarks").val();

        var inventoryDto = {};
        inventoryDto.quantity =        $("#qnty").val();
        inventoryDto.inventoryProductDto=inventoryProduct;
        return inventoryDto
    }
    function clear() {
        $("#qnty").val("");
        $("#assetCategory").val("");


        $(".unit").html( 'unit');


        $("#productName").val("");
        $("#model").val("");
        $("#productCode").val("");
        $("#asset").val("");
        $("#brandName").val("");
        $("#unitPrice").val("");
       $("#purchaseDate").val("");
        $("#warrantyDetails").val("");
        $("#tender").val("");

        $("#remarks").val("");
        $('.selectpicker').selectpicker('refresh');

    }
    function loadCurrentRegisterTblData(){
        var urlBuilder = "/inventory-ajax/get-current-register";
        $.ajax({
            url: urlBuilder,
            type: "get"

        }).done(function (result) {
            tblCurrentRegister.clear().draw();
            tblCurrentRegister.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }
    function loadAssetHeadData() {
        var assetDdl= '<option value="">Select Asset</option>';
        var urlBuilder ="/inventory-ajax/asset-head-get-filtered-data?srcAssetCategory="+ $("#assetCategory").val()+
            "&srcAssetHeadStatus=1";
        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (listItems) {

            $("#asset").empty();
            assetLst=listItems;
            for(var i=0 ; i<listItems.length;i++){
                // alert(listItems[i].id);
                assetDdl+= '<option value="'+listItems[i].id+'"' +
                    // ' asstCode="'+listItems[i].assetCode+'" unit="'+listItems[i].unitId+'"' +
                    // 'thresholdLevel="'+listItems[i].thresholdLevel+'"' +
                    // 'reorderLevel="'+listItems[i].reorderLevel+'"' +
                    '>'
                    +listItems[i].assetName+'</option>';
            }


            $("#asset").html(assetDdl);
            $('.selectpicker').selectpicker('refresh');


        }).fail(function (jqXHR, textStatus, errorThrown) {

            $("#asset").empty();
            $("#asset").html(assetDdl);
            $('.selectpicker').selectpicker('refresh');

        });
    }
    function setAssetRelatedData() {
        for(var i=0 ; i<assetLst.length;i++) {
            if(assetLst[i].id== $("#asset").val()){
                $(".unit").html( assetLst[i].unit==null?'unit':assetLst[i].unit);

                // $("#thresholdLevel").val(assetLst[i].thresholdLevel);
                // $("#reorderLevel").val(assetLst[i].reorderLevel);
                // $("#assetCode").val(assetLst[i].assetCode);
            }

        }

    }



});
