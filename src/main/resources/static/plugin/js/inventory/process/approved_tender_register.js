function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}

$(document).ready(function () {
    clearform();
    changeView('I');
    loadDatableWithFilteredTenderRegData();

    $( ".datepicker" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1970:' + new Date().getFullYear().toString()
    });

///////Buttons
    $("#regNewTenderBtnAdd").click(function () {
        changeView('N')
    });
    $("#tenderRegBtnSave").click(function () {
        saveData(0);

    });
    $("#tenderRegBtnReset").click(function () {
        changeView('I')
    });
    $("#tenderRegBtnSearch").click(function () {
        
        loadDatableWithFilteredTenderRegData();

    });
    $("#tenderRegBtnClearSearchPanel").click(function () {

        
        clearform();
        loadDatableWithFilteredTenderRegData();

    });
/////////////////////////////////
    $(document).on("click", ".tender-reg-edit", function () {
        // $(".tender-reg-edit").click(function () {
        $("#tender-encId").val($(this).closest("tr").find(".tender-reg-edit").attr("encId"));


        changeView('U');
        loadFormData($(this).closest("tr").find(".tender-reg-edit").attr("encId"));


    });
    $(document).on("click", ".tender-reg-delete", function () {
        // $(".tender-reg-delete").click(function () {
        $("#tender-encId").val($(this).closest("tr").find(".tender-reg-delete").attr("encId"));
        $.confirm({
            icon: 'ace-icon fa fa-exclamation-circle',
            theme: 'material',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',

            title: "Are you sure to delete this record ?",
            content: 'Once deleted , you will not be able to retrieve or undo that record,',
            typeAnimated: true,
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    action: function () {
                        saveData(1);
                        changeView('I');

                        //alert($(this).attr("encId"));
                    }

                },
                CANCEL: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                        return true;
                    }
                }
            }

        });


    });

    $(".code_generator").change(function () {

        var refNo = "";
        var office = $("#office").val().toUpperCase();
        var purchaseItemHead = $("#purchaseItemHead").val().toUpperCase();

        if(office.trim().length > 2){
            office = office.substring(0, 2);
        }

        if(purchaseItemHead.trim().length > 2){
            purchaseItemHead = purchaseItemHead.substring(0, 2);
        }

        var d = new Date();
        var daySum = parseInt(d.getMonth()) + parseInt(d.getDate()) + parseInt(d.getFullYear());
        refNo = 'TN-' + office + '-' + purchaseItemHead+ '-' + daySum + '' + Math.floor(Math.random() * 90 + 10);
        $("#refNo").val(refNo);

    });

/////functions//////************************************************************************
    function changeView(viewStyle) {

        if (viewStyle == 'N') {
            $("#pnl-tender-reg-info").css("display", "block");
            $("#pnl-tender-reg-list").css("display", "none");
        }
        else if (viewStyle == 'I') { //new next
            $("#pnl-tender-reg-info").css("display", "none");
            $("#pnl-tender-reg-list").css("display", "block");
        }
        else if (viewStyle == 'U') { //new next
            $("#pnl-tender-reg-info").css("display", "block");
            $("#pnl-tender-reg-list").css("display", "none");
        }
        else if (viewStyle == 'S') {
            $("#pnl-tender-reg-info").css("display", "none");
            $("#pnl-tender-reg-list").css("display", "block");
        }
        else {
            $("#pnl-tender-reg-info").css("display", "block");
            $("#pnl-tender-reg-list").css("display", "none");
        }
        clearform();

    }

    function loadFormData(tenderEncId) {
        if (isEmptyString(tenderEncId)) {
            showAlertByType("Unable to edit this data.", 'W');
        }
        else {

            const getTenderRegUrl = "/inventory-ajax/tender-reg-get/" + tenderEncId;
            $.ajax({
                url: getTenderRegUrl,
                type: 'GET',
                // data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (tenderDto) {
                    //console.log(tenderDto);
                    if (tenderDto == null) {

                    }
                    else {
                        loadData(tenderDto);
                    }

                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
    }

    function saveData(isDeletedFlg) {

        if (isValidated() || isDeletedFlg == 1) {
            const saveTenderRegUrl = "/inventory-ajax/save-tender-reg-post";
            $.ajax({
                url: saveTenderRegUrl,
                type: 'POST',
                data: JSON.stringify(getFormData(isDeletedFlg)),
                contentType: 'application/json',
                success: function (commonMsg) {
                    //console.log(commonMsg);

                    if (commonMsg.isSucceed) {


                        clearform();
                        loadDatableWithFilteredTenderRegData();
                        changeView('I');
                        showAlertByType(commonMsg.msg, 'S');

                    }
                    else {
                        showAlertByType(commonMsg.msg, 'F');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearform() {
        $("#tender-encId").val('');
        $("#refNo").val('');
        $("#office").val('');
        $("#tenderApprovalDate").val('');
        $("#tenderPublishDate").val('');
        $("#approvalAuth").val('');
        $("#tenderDetails").val('');
        $("#tenderType").val('');
        $("#tenderAmount").val('');
        $("#vendor").val('');
        $("#paymentMode").val('');
        $("#purchaseItemHead").val('');
        $("#publishSource").val('');
        $("#terminate").val('');
        $("#contractStartDate").val('');
        $("#contractEndDate").val('');
        $("#extensionDate").val('');
        $("#status").val('');
        valdiationMsgTitle = "";
        valdiationMsgContent = "";


        clearSearchCreiteria();
        $('.selectpicker').selectpicker('refresh');

    }

    function clearSearchCreiteria() {

        $("#src-refNo").val('');
        $("#src-f-tenderApprovalDate").val('');
        $("#src-t-tenderApprovalDate").val('');
        $("#src-tenderType").val('');
        $("#src-purchaseItemHead").val('');
        //clearform();
    }

    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        // if (isEmptyString($("#refNo").val())) {
        //
        //     valdiationMsgContent = "Reference number is required!";
        //     $("#refNo").focus();
        //     return false;
        // }
        if (isEmptyString($("#office").val())) {

            valdiationMsgContent = "Office is required!";
            $("#office").focus();
            return false;
        }
        else if (isEmptyString($("#tenderPublishDate").val())) {

            valdiationMsgContent = "Publish Date is required!";
            $("#tenderPublishDate").focus();
            return false;
        }
        else if (isEmptyString($("#tenderApprovalDate").val())) {

            valdiationMsgContent = "Approval Date is required!";
            $("#tenderApprovalDate").focus();
            return false;
        }
        else if (isEmptyString($("#approvalAuth").val())) {

            valdiationMsgContent = "Approval authority  is required!";
            $("#approvalAuth").focus();
            return false;
        }
        else if (isEmptyString($("#tenderDetails").val())) {

            valdiationMsgContent = "Tender Details is required!";
            $("#tenderDetails").focus();
            return false;
        }
        else if (isEmptyString($("#tenderType").val())) {

            valdiationMsgContent = "Tender Type is required!";
            $("#tenderType").focus();
            return false;
        }
        else if (isEmptyString($("#tenderAmount").val())) {

            valdiationMsgContent = "Tender Amount is required!";
            $("#tenderAmount").focus();
            return false;
        }
        else if (isEmptyString($("#vendor").val())) {

            valdiationMsgContent = "Vendor is required!";
            $("#vendor").focus();
            return false;
        }
        else if (isEmptyString($("#paymentMode").val())) {

            valdiationMsgContent = "Payment Mode is required!";
            $("#paymentMode").focus();
            return false;
        }
        else if (isEmptyString($("#purchaseItemHead").val())) {

            valdiationMsgContent = "Product Head is required!";
            $("#purchaseItemHead").focus();
            return false;
        }
        else if (isEmptyString($("#publishSource").val())) {

            valdiationMsgContent = "Publish Source is required!";
            $("#publishSource").focus();
            return false;
        }
        else if (isEmptyString($("#terminate").val())) {

            valdiationMsgContent = "Terminate is required!";
            $("#terminate").focus();
            return false;
        }
        else if (isEmptyString($("#contractStartDate").val())) {

            valdiationMsgContent = "Contract Start Date is required!";
            $("#contractStartDate").focus();
            return false;
        }
        else if (isEmptyString($("#contractEndDate").val())) {

            valdiationMsgContent = "Contract End Date is required!";
            $("#contractEndDate").focus();
            return false;
        }
        else if (isEmptyString($("#extensionDate").val())) {

            valdiationMsgContent = "Extension Date is required!";
            $("#extensionDate").focus();
            return false;
        }
        else if (isEmptyString($("#status").val())) {

            valdiationMsgContent = "Status is required!";
            $("#status").focus();
            return false;
        }
        else if (!isEmptyString($("#tenderAmount").val())) {

            if (isNaN($("#tenderAmount").val())){
                valdiationMsgContent = "Enter a valid amount!";
                $("#tenderAmount").focus();
                return false;
            }
            else {
                if ($("#tenderAmount").val() < 0){
                    valdiationMsgContent = "Enter a valid amount!";
                    $("#tenderAmount").focus();
                    return false;
                }
                else return true;
            }
        }
        else {
            return true;
        }
    }

    function getFormData(isDeletedFlg) {
        var tenderDto = {};

        tenderDto.encId = $("#tender-encId").val();
        tenderDto.refNo = $("#refNo").val();
        tenderDto.officeCd = $("#office").val();
        tenderDto.tenderPublishDate = $("#tenderPublishDate").val();
        tenderDto.tenderApprovalDate = $("#tenderApprovalDate").val();
        tenderDto.approvalAuth = $("#approvalAuth").val();
        tenderDto.tenderDetails = $("#tenderDetails").val();
        tenderDto.tenderType = $("#tenderType").val();
        tenderDto.tenderAmount = $("#tenderAmount").val();
        tenderDto.vendor = $("#vendor").val();
        tenderDto.paymentMode = $("#paymentMode").val();
        tenderDto.purchaseItemHead = $("#purchaseItemHead").val();
        tenderDto.publishSource = $("#publishSource").val();
        tenderDto.terminate = $("#terminate").val();
        tenderDto.contractStartDate = $("#contractStartDate").val();
        tenderDto.contractEndDate = $("#contractEndDate").val();
        tenderDto.extensionDate = $("#extensionDate").val();
        tenderDto.isActiveFlg = $("#status").val();
        tenderDto.isDeletedFlg = isDeletedFlg;//0/1

        return tenderDto
    }

    function loadData(tenderDto) {
        //console.log(tenderDto);
        $("#tender-encId").val(tenderDto.encId);
        $("#refNo").val(tenderDto.refNo);
        $("#office").val(tenderDto.officeCd);
        $("#tenderPublishDate").val(tenderDto.tenderPublishDate);
        $("#tenderApprovalDate").val(tenderDto.tenderApprovalDate);
        $("#approvalAuth").val(tenderDto.approvalAuth);
        $("#tenderDetails").val(tenderDto.tenderDetails);
        $("#tenderType").val(tenderDto.tenderType);
        $("#tenderAmount").val(tenderDto.tenderAmount);
        $("#vendor").val(tenderDto.vendor);
        $("#paymentMode").val(tenderDto.paymentMode);
        $("#purchaseItemHead").val(tenderDto.purchaseItemHead);
        $("#publishSource").val(tenderDto.publishSource);
        $("#terminate").val(tenderDto.terminate);
        $("#contractStartDate").val(tenderDto.contractStartDate);
        $("#contractEndDate").val(tenderDto.contractEndDate);
        $("#extensionDate").val(tenderDto.extensionDate)
        $("#status").val(tenderDto.isActiveFlg);

        $("#refNo").focus();
        $('.selectpicker').selectpicker('refresh');
    }

///////////////////////////Data table
    var tenderRegTable = $('#tbl-tender-reg-lst').DataTable({

        data: [],
        "columns": [
            {"data": null, defaultContent: ''},
            {"data": "refNo"},
            {"data": "tenderApprovalDate"},
            {"data": "vendor"},
            {"data": "purchaseItemHead"},
            {"data": "contractStartDate"},
            {"data": "contractEndDate"},
            {"data": "tenderType"},
            {"data": "activeStatus"},
            {"data": null, defaultContent: ''}
        ],
        'columnDefs': [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    //console.log(data);
                    return '  <a class="btn btn-link  btn-round red tender-reg-delete"   encId="' + data.encId + '" ><i class="fa fa-remove" aria-hidden="true"   style="font-size: large"></i></a>';
                }
            }, {
                'targets': 9,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round tender-reg-edit"   encId="' + data.encId + '"> <i class="fa fa-edit" aria-hidden="true" style="font-size: large"></i></a>';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    function loadDatableWithFilteredTenderRegData() {

        var flgQuestionMark = false;
        var urlBuilder = "/inventory-ajax/approved-tender-reg-get-filtered-data";

        if (!isEmptyString($("#src-refNo").val())) {
            if (flgQuestionMark) {
                urlBuilder += '&refNo=' + $("#src-refNo").val();
            }
            else {
                urlBuilder += '?refNo=' + $("#src-refNo").val();
                flgQuestionMark = true;
            }
        }
        if (!isEmptyString($("#src-f-tenderApprovalDate").val())) {
            if (flgQuestionMark) {
                urlBuilder += '&f_tenderApprovalDate=' + $("#src-f-tenderApprovalDate").val();
            }
            else {
                urlBuilder += '?f_tenderApprovalDate=' + $("#src-f-tenderApprovalDate").val();
                flgQuestionMark = true;
            }
        }
        if (!isEmptyString($("#src-t-tenderApprovalDate").val())) {
            if (flgQuestionMark) {
                urlBuilder += '&t_tenderApprovalDate=' + $("#src-t-tenderApprovalDate").val();
            }
            else {
                urlBuilder += '?t_tenderApprovalDate=' + $("#src-t-tenderApprovalDate").val();
                flgQuestionMark = true;
            }
        }
        if (!isEmptyString($("#src-tenderType").val())) {
            if (flgQuestionMark) {
                urlBuilder += '&tenderType=' + $("#src-tenderType").val();
            }
            else {
                urlBuilder += '?tenderType=' + $("#src-tenderType").val();
                flgQuestionMark = true;
            }
        }
        if (!isEmptyString($("#src-purchaseItemHead").val())) {
            if (flgQuestionMark) {
                urlBuilder += '&productHead=' + $("#src-purchaseItemHead").val();
            }
            else {
                urlBuilder += '?productHead=' + $("#src-purchaseItemHead").val();
                flgQuestionMark = true;
            }
        }


        $.ajax({
            url: urlBuilder,
            type: "get"

        }).done(function (result) {
            tenderRegTable.clear().draw();
            tenderRegTable.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }

//////////////////////////////Date picker

    //
    // $("#date-range").daterangepicker({
    //     "autoApply": true,
    //     "showDropdowns": true,
    //     locale: {
    //         format: 'DD-MM-YYYY'
    //     },
    //     ranges: {
    //         'Today': [moment(), moment()],
    //         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    //         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //         'This Month': [moment().startOf('month'), moment().endOf('month')],
    //         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //     },
    //     "linkedCalendars": true
    //
    //
    // }, function (start, end, label) {
    //     dateFrom = start.format('DD/MM/YYYY'); //dd/MM/yyyy")
    //     dateTo = end.format('DD/MM/YYYY');
    // });
    // $("#date-range").val('');


});
