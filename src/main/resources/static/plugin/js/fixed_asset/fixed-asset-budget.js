/**
 * Created by Shantanu Dutta on 7/22/2019.
 */
$(document).ready (function(){

    $('.yearpicker').yearpicker();// select a year in input field

    var table = $("#fa_budget").DataTable({
        "columnDefs": [
            {"targets": [0], "searchable": false}
        ]
    });

    $(document).on('input', '#fa_budget_amount', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });

    $(document).on('input', '#fa_budget_consumed_amount', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });

    function hideValidation(){
        $('#err_fa_budget_year').hide();
        $('#err_fa_budget_head').hide();
        $('#err_fa_budget_office_catg').hide();
        $('#err_fa_budget_amount').hide();
        $('#err_fa_budget_consumed_amount').hide();
        $('#err_fa_budget_approv_date').hide();
        $('#err_fa_active_status').hide();
    }

    function validate(){
        hideValidation();

        if ($('#fa_budget_year').val() == "") {

            $('#err_fa_budget_year').text("Please select budget year");
            $('#err_fa_budget_year').show();
            return false;
        }
        else {
            $('#err_fa_budget_year').hide();
        }

        if ($('#fa_budget_head').val() == "-1") {

            $('#err_fa_budget_head').text("Please select budget head");
            $('#err_fa_budget_head').show();
            return false;
        }
        else {
            $('#err_fa_budget_head').hide();
        }

        if ($('#fa_budget_office_catg').val() == "-1") {

            $('#err_fa_budget_office_catg').text("Please select office category");
            $('#err_fa_budget_office_catg').show();
            return false;
        }
        else {
            $('#err_fa_budget_office_catg').hide();
        }

        if ($('#fa_budget_office_id').val() == "-1") {

            $('#err_fa_budget_office_id').text("Please select an office");
            $('#err_fa_budget_office_id').show();
            return false;
        }
        else {
            $('#err_fa_budget_office_id').hide();
        }

        if ($('#fa_budget_amount').val() == "") {

            $('#err_fa_budget_amount').text("Please enter budget amount");
            $('#err_fa_budget_amount').show();
            return false;
        }
        else {
            $('#err_fa_budget_amount').hide();
        }

        if ($('#fa_budget_consumed_amount').val() == "") {

            $('#err_fa_budget_consumed_amount').text("Please enter consumed amount");
            $('#err_fa_budget_consumed_amount').show();
            return false;
        }
        else {
            $('#err_fa_budget_consumed_amount').hide();
        }

        if ($('#fa_budget_approv_date').val() == "") {

            $('#err_fa_budget_approv_date').text("Please select a date");
            $('#err_fa_budget_approv_date').show();
            return false;
        }
        else {
            $('#err_fa_budget_approv_date').hide();
        }

        if ($('#fa_active_status_yes').is(":checked")==false && $('#fa_active_status_no').is(":checked")==false){
          $('#err_fa_active_status').text("This field is required");
          $('#err_fa_active_status').show();
          return false;
      }
        else {
          $('#err_fa_active_status').hide();
      }

        return true;
    }

    var curRowEditMode;
    $('#fa_budget').on('click', '#edit', function () {
        hideValidation();

        $("#fixed_asset_budget_save").prop("value", "Update");

        curRowEditMode = $(this).closest('tr');
        var col1 = curRowEditMode.find('td:eq(0)').text();
        var col2 = curRowEditMode.find('td:eq(1)').text();
        var col3 = $.trim(curRowEditMode.find('td:eq(2)').text());
        var col4 = $.trim(curRowEditMode.find('td:eq(3)').text());
        var col5 = $.trim(curRowEditMode.find('td:eq(4)').text());
        var col6 = (curRowEditMode.find('td:eq(5)').text());
        var col7 = curRowEditMode.find('td:eq(6)').text();
        var col8 = $.trim(curRowEditMode.find('td:eq(7)').text());
        var col9 = $.trim(curRowEditMode.find('td:eq(8)').text());

        $('#id').val(col1);
        $('#fa_budget_year').val(col2);

        $("#fa_budget_head option").each(function() {
            if($(this).text() == col3) {
                $("#fa_budget_head").val($(this).val());
                return false;
            }
        });


        $("#fa_budget_office_catg option").each(function() {
            if($(this).text() == col4) {
                $("#fa_budget_office_catg").val($(this).val());
                $('#fa_budget_office_catg').trigger('change');
                return false;
            }
        });

        $('#fa_budget_amount').val(col6);
        $('#fa_budget_consumed_amount').val(col7);
        $('#fa_budget_approv_date').val(col8);

        if (col9 == "Yes"){
            $('#fa_active_status_yes').prop("checked",true);
        }
       else {
            $('#fa_active_status_no').prop("checked",true);
        }

        setTimeout(function(){
            $("#fa_budget_office_id option").each(function() {
             if ($(this).text() == col5) {
                 $("#fa_budget_office_id").val($(this).val());
                 return false;
             }
             });
        }, 500);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top,
        }, 500);

    });


    $('#fa_budget').on('click', '#delete', function () {
        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/fixed-asset-budget/delete/" + col1,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            if (response == true) {
                                table.row(curRow).remove().draw(false);
                                showAlert("Deleted Successfully");
                            }
                            else {
                                showAlert("Unknown error");
                            }

                        },
                        error: function (xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });


    $(document).on ("click", "#fixed_asset_budget_save", function(e){
        var fixed_asset_budget= {};
  if (validate()) {
    fixed_asset_budget.id = $("#id").val();
    fixed_asset_budget.budgetYear = $("#fa_budget_year").val();
    fixed_asset_budget.budgetHead = $("#fa_budget_head").val();
    fixed_asset_budget.officeCatg = $("#fa_budget_office_catg").val();
    fixed_asset_budget.officeId = $("#fa_budget_office_id").val();
    fixed_asset_budget.budgetAmnt = $("#fa_budget_amount").val();
    fixed_asset_budget.consumedAmnt = $("#fa_budget_consumed_amount").val();
    fixed_asset_budget.approvDate = $("#fa_budget_approv_date").val();
    //fixed_asset_budget.doc = $("#fa_doc_upload").val();

    var activeStatus;
    if ($("#fa_budget_active_status_yes").is(":checked")) {
        fixed_asset_budget.activeStatus = 1;
        activeStatus = "Yes";
    }

    else {
        fixed_asset_budget.activeStatus = 0;
        activeStatus = "No";
    }

    var budgetHead = $("#fa_budget_head :selected").text();
    var budgetOfficeCatg = $("#fa_budget_office_catg :selected").text();
    var budgetOfficeId =  $("#fa_budget_office_id :selected").text();

    $.ajax({
        contentType: 'application/json',
        url: "add-fixed-asset-budget",
        type: 'POST',
        //async: false,
        data: JSON.stringify(fixed_asset_budget),
        dataType: 'json',
        success: function (response) {

            var editBtn = "<button id='edit' type='button' class='btn fa fa-edit' style='text-align:center;vertical-align: middle;font-size:20px;'></button>";
            var dltBtn = "<button id='delete' type='button' class='btn fa fa-trash-o' style='text-align:center;vertical-align: middle;font-size:20px;color:red;'></button>";

             if (fixed_asset_budget.id != "") {
                //Update
                 var rowIndex = table.row( curRowEditMode ).index();
                var updatedRow = table.row( rowIndex ).data( [response, fixed_asset_budget.budgetYear, budgetHead, budgetOfficeCatg, budgetOfficeId, fixed_asset_budget.budgetAmnt, fixed_asset_budget.consumedAmnt, fixed_asset_budget.approvDate, activeStatus, editBtn, dltBtn] ).invalidate();

                updatedRow.nodes().to$().attr("style", "background-color: rgb(255, 245, 238);");
                updatedRow.nodes().to$().attr("id", "updateScroll");
                document.getElementById("updateScroll").scrollIntoView();
                updatedRow.nodes().to$().removeAttr("id");

                setTimeout(function(){
                    updatedRow.nodes().to$().removeAttr("style");
                }, 3000);

                showAlert("Updated successfully");
             }
            else {
                // //ADD
                var newRow = table.row.add([response, fixed_asset_budget.budgetYear, budgetHead, budgetOfficeCatg, budgetOfficeId, fixed_asset_budget.budgetAmnt, fixed_asset_budget.consumedAmnt, fixed_asset_budget.approvDate, activeStatus, editBtn, dltBtn]).draw();
                var colCount = table.columns()[0].length;

                newRow.nodes().to$().find("td:eq(0)").attr("style", "text-align:center;display: none");
                newRow.nodes().to$().attr("style", "background-color: rgb(255, 245, 238);");
                newRow.nodes().to$().attr("id", "newScroll");
                var i;
                for(i = 1; i < colCount; i++){
                    newRow.nodes().to$().find("td:eq(" + i + ")").attr("style", "text-align:center;vertical-align: middle;");
                }
                newRow.show().draw(false);
                document.getElementById("newScroll").scrollIntoView();
                newRow.nodes().to$().find("tr").removeAttr("id");

                setTimeout(function(){
                    newRow.nodes().to$().find("tr").removeAttr("style");
                }, 3000);

                showAlert("Saved successfully");
            }

            $("#fixed_asset_budget_reset").click();

        },
        error: function (xhr, status, error) {

            showAlert("Something Went Wrong!");
        }
    });

  }
});

    $(document).on("click", "#fixed_asset_budget_reset", function (e){
        hideValidation();

        $("#id").val('');
        $("#fixed_asset_budget_save").prop("value", "Save");

    });

    // select office name by office category (only js file is needed in this case)
    $(document).on("change", "#fa_budget_office_catg", function (e) {
        var catId = $("#fa_budget_office_catg option:selected").val();

        $.get("/fixed-asset-budget/get-office-by-catg?officeCatg=" + catId,

            function (data, status) {
                var office = $('#fa_budget_office_id');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Office---",

                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices.id,
                        text: offices.officeName
                    }));
                });

            });

    });

});

