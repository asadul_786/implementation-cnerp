$(document).ready(function () {


    function hideValidation() {
        $('#err_check_inv_item_head').hide();
        $('#err_check_inv_record_type').hide();
        //$('#err_check_inv_date').hide();

    }
    //var table =$("#check_inv_reg").DataTable();

    function validate (){
        hideValidation();

        if ($('#check_inv_item_head').val() == "-1") {

            $('#err_check_inv_item_head').text("Please select an item head");
            $('#err_check_inv_item_head').show();
            return false;
        }
        else {
            $('#err_check_inv_item_head').hide();
        }

        if ($('#check_inv_record_type').val() == "-1") {

            $('#err_check_inv_record_type').text("Please select a record type");
            $('#err_check_inv_record_type').show();
            return false;
        }
        else {
            $('#err_check_inv_record_type').hide();
        }

        return true;
    }

    $(document).on('click',"#check_inv_reg_search",function (event) {
        event.preventDefault();

        if (validate()) {
         var bothDate = $("#reservation").val();
         var office = $("#officeId").val();
         var itemHead = $("#check_inv_item_head option:selected").val();
         var recordType = $("#check_inv_record_type option:selected").val();
         var split = bothDate.split(" - ");
         var fromDate = split[0];
         var toDate = split[1];

         $.ajax({
             contentType: 'application/json',
             url: "add-inventory-register",
             type: 'GET',
             async: false,
             data: {
                 fromDate: fromDate,
                 toDate: toDate,
                 office: office,
                 itemHead: itemHead,
                 recordType: recordType
             },
             dataType: 'json',
             success: function (res) {
                 var table = $('#check_inv_reg').dataTable({
                     data: res,
                     columns: [
                         {
                             "data": "faItemLogTableId",
                             "visible": false
                         },
                         {"data": "itemHeadName"},
                         {"data": "recordTypeName"},
                         {"data": "itemAdd"},
                         {"data": "itemSub"},
                         {"data": "officeName"},
                         {"data": "date"},

                         {
                             "data": "faItemLogTableId",
                             render: function (data, type, row) {
                                 var v = row.faItemLogTableId;
                                 // v =
                                 //     '<button style="text-align:center;vertical-align: middle;" ' +
                                 //     'type="button" id ="delete" class="btn btn-info empl_release_select_button" value="Select" id="' +
                                 //     v + '"> <i class="fa fa-close"></i></button>';
                                 //'type="button" class="btn btn-info empl_release_select_button" value="Select" id="action"> <i class="fa fa-close"></i></button>';

                                 v =
                                     '<button style="text-align:center;vertical-align: middle;" ' +
                                     'type="button" class="btn btn-info delete" value="Select" id="' +
                                     v + '"> <i class="fa fa-close"></i></button>';

                                 return v;
                             }
                         }
                     ],
                     bDestroy: true,
                     iDisplayLength: 15,
                     columnDefs: [
                         {
                             className: "dt-center"
                         }
                     ]
                 });

                 // table.column( 0 ).visible( false );

                 $("#check_inv_reg_reset").click();
             },

             error: function (xhr, status, error) {
                 showAlert("Something went wrong");

             }
         });

     }
    });

    $(document).on ('click', "#currentQuantity", function(e){
        e.preventDefault();
        var val = $("#importedOfficeId").val();

        $.ajax({
            contentType: 'application/json',
            url: "current-quantity-inventory",
            type: 'GET',
            //async: false,
            data: {id: val},
            dataType: 'json',
            success: function (res) {

                $(".result").html(res);
            },

        });

    });

    $('#check_inv_reg').on('click', '.delete', function (e) {
        e.preventDefault();
         //var curRow = $(this).closest('tr');
        // var col1 = curRow.find('td:eq(0)').text();

        var col1 = $(this).attr("id");
       //var col1= table.row( this ).id();

        var test1 = $(this).closest("tr");
        var test2 = $(this).parent().parent();


        $.confirm({
            title: 'Confirm',
            content: 'Are you sure?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/inventory-register/delete/" +col1,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {
                            if (response == true) {

                                test1.remove().draw(false);
                                //test2.remove().draw();

                                showAlert("Deleted Successfully");
                            }
                            else {
                                showAlert("Unknown error");
                            }

                        },
                        error: function (xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });



    $(document).on("click", "#check_inv_reg_reset", function (e){
        hideValidation();

        //$("#officeId").val('');
        //$("#fixed_asset_budget_save").prop("value", "Save");

    });

});