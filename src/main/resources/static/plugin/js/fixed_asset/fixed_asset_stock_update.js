/**
 * Created by Raj on 07-Aug-19.
 */

$(document).ready(function () {

    //select2
    $('.js-example-basic-single').select2();
    //Datatable
    var table = $("#item_pending_table").DataTable({
        "columnDefs": [
            {"className": "dt-center", "targets": "_all"}
        ]
    });

    var requestTable = $("#request_details_table").DataTable();


    //Datepicker
    $(document).on("change", "#item_pending_DF_in", function (e) {
        if($('#item_pending_DF_in').val() == ""){
            $("#item_pending_DT_in").val("");
            $("#item_pending_DT_in").attr("disabled", true);
        }
    });

    $(document).on("change", "#item_pending_DT_in", function (e) {
        $("#item_pending_DT_in").attr("readonly", true);
    });

    $( "#item_pending_DF_in" ).datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateStr)
        {
            $("#item_pending_DT_in").attr("disabled", false);
            $("#item_pending_DF_in").attr("readonly", true);
            $("#item_pending_DT_in").datepicker("destroy");
            // $("#fa_in_DD").val(dateStr);
            $("#item_pending_DT_in").datepicker({ minDate: dateStr, dateFormat: 'dd/mm/yy'});
        }
    });

    var status = true;
    //ReceiveBtnValidation
    function validateRecBtn(val, amount) {

        var one =Number($('#rec'+val).val());
        var two = Number($('#dis'+val).val());
        var add = one + two;
        if($('#rec'+val).val() == "" && status){
            status = false;
            alert("Enter Received Amount");
        }
        else if($('#rec'+val).val() > amount && status){
            status = false;
            alert("Must be less than Allocated amount");
        }
        else if($('#dis'+val).val() == "" && status){
            status = false;
            alert("Enter Disposed Amount");
        }
        else if($('#dis'+val).val() > amount && status){
            status = false;
            alert("Must be less than Allocated amount");
        }
        else if(add !== amount){
            status = false;
            alert("Sum of Received amount and Disposed amount must be equal to Allocated amount");
        }

        return status;
    }



    function amountText(amount) {
        var text = "<p>"+amount+"</p>"
        return text;
    }

    function receivedAmount(id) {
        var input = "<input id='rec"+id+"' type='number' min='1'/>";
        return input;
    }

    function disposedAmount(id) {
        var input = "<input id='dis"+id+"' type='number' min='1'/>";
        return input;
    }

    function receiveBtn(id) {
        var btn = "<button type='button' name='"+id+"' data-toggle='tooltip' data-placement='bottom' title='Save' style='text-align:center;vertical-align: middle;font-size:15px; cursor: hand' class='btn btn-success receive_btn' id='Received"+id+"'>"+
            "<span class='glyphicon glyphicon-upload'></span></button>";

        return btn;
    }


    //Pressing received button
    $(document).on('click', '.receive_btn', function () {
        var val = $(this).attr('name');
        var row = $(this).closest('tr');
        var amount;
        $.ajax({
            contentType: 'application/json',
            url: 'get-allocated-amount',
            type: 'GET',
            async: false,
            data: {id: val},
            dataType:'json',
            success:function (response) {
                amount = response;
            },
            error: function (xhr, status, error) {
                alert("Something went wrong!");
            }

        });
        if(validateRecBtn(val, amount)){
            var approvalDto = {};
            approvalDto.id = val;
            approvalDto.receivedAmount = $('#rec'+val).val();
            approvalDto.disposedAmount = $('#dis'+val).val();
            $.ajax({
                contentType: 'application/json',
                url: 'add-updated-inventory',
                type: 'POST',
                async: false,
                data: JSON.stringify(approvalDto),
                success:function (response) {
                    showAlert("Successfilly Saved");
                    $(row).hide();
                },
                error: function (xhr, status, error) {
                    alert("Something went wrong!");
                }

            })
        }
        else{
            status = true;
        }
    });


    //view details
    $(document).on('click', '.view_details', function (e) {
        $("#popup_item_request_details").show();
        var n = $(document).height();
        $('html, body').animate({ scrollTop: n }, 'slow');
       var val = $(this).attr('id');
       $.ajax({
           contentType: 'application/json',
           url: "get-requested-items",
           type: 'GET',
           async: false,
           data: {id:val},
           dataType: 'json',
           success: function (response) {
               var count = 0;
               requestTable.clear().draw();
               $.each(response.fixedAssetItemReqDetails,function (index, data) {

                   if(data.isReceived != '1')
                   {
                       $("#fa_RO_label").text(data.faItemRequestMstId.requestingOfficeId.officeName);
                       $("#fa_RBE_label").text(response.empName);
                       var reqDate = new Date(data.faItemRequestMstId.requestDate);
                       var formattedDate = reqDate.getDate() + "/" + (reqDate.getMonth()+1) + "/" + reqDate.getFullYear();
                       $("#fa_RD_label").text(formattedDate);
                       if(data.faItemRequestMstId.urgencyLevel == '1'){
                           $("#fa_UR_label").text("Very Urgent");
                       }
                       else if(data.faItemRequestMstId.urgencyLevel == '2'){
                           $("#fa_UR_label").text("Urgent");
                       }
                       else if(data.faItemRequestMstId.urgencyLevel == '3'){
                           $("#fa_UR_label").text("Not Urgent");
                       }
                       if(data.faItemRequestMstId.authorityOfficeId == '-1'){
                           $("#fa_AO_label").text('None');
                       }
                       else{
                           $.ajax({
                               contentType: 'application/json',
                               url: "get-authoritative-office",
                               type: 'GET',
                               async: false,
                               data: {id: data.faItemRequestMstId.authorityOfficeId},
                               dataType: 'json',
                               success:function (result) {
                                   $("#fa_AO_label").text(result.officeName);
                               },
                               error: function (xhr, status, error) {
                                   alert("Something went wrong!");
                               }
                           });
                       }
                       $("#fa_RE_label").text();
                       var dueDate = new Date(data.faItemRequestMstId.dueDate);
                       var formattedDueDate = dueDate.getDate() + "/" + (dueDate.getMonth()+1) + "/" + dueDate.getFullYear();
                       $("#fa_DD_label").text(formattedDueDate);
                       // var tr = "<tr id ='" + data.id + "'>" +
                       //     "<td style='text-align: center'>" + (++count) + "</td>" +
                       //     "<td style='text-align: center'>" + data.faItemHeadId + "</td>" +
                       //     "<td style='text-align: center'>" + data.faItemHeadQuantity + "</td>" +
                       //     "<td style='text-align: center'>" +  + "</td>" +
                       //     "<td style='text-align: center'>" + data.faItemSpecificName + "</td>" +
                       //     "<td style='text-align: center'>" + approvalBtns(data.id) + "</td>"
                       // "</tr>";
                       // $("#fa_item_pending_table_body").append(tr);

                       if(data.pendingReq == '0'){
                           requestTable.row.add([
                               (++count),
                               data.faItemHeadId.itemHeadName,
                               data.faItemSpecificName,
                               data.faItemHeadQuantity,
                               allocatedItemsField(data.id),
                               ""
                           ]).draw( false );
                       }
                       else if(data.pendingReq == '1'){
                           requestTable.row.add([
                               (++count),
                               data.faItemHeadId.itemHeadName,
                               data.faItemSpecificName,
                               data.faItemHeadQuantity,
                               amountText(data.approvedQuantity),
                               receivedAmount(data.id),
                               disposedAmount(data.id),
                               receiveBtn(data.id)
                           ]).draw( false );
                       }
                   }
               })
               table.draw();
           },
           error: function (xhr, status, error) {
               alert("Something went wrong!");
           }
       })
    });





    $(document).on("click", "#item_pending_reload_btn", function (e) {
        location.reload();
    });
});