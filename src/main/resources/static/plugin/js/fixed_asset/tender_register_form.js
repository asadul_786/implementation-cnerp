/**
 * Created by Raj on 7/18/2019.
 */


$(document).ready(function () {


    //select2
    function matchCustom(params, data) {
        // If there are no search terms, return all of the data
        if ($.trim(params.term) === '') {
            return data;
        }

        // Do not display the item if there is no 'text' property
        if (typeof data.text === 'undefined') {
            return null;
        }

        // `params.term` should be the term that is used for searching
        // `data.text` is the text that is displayed for the data object
        if (data.text.indexOf(params.term) > -1) {
            var modifiedData = $.extend({}, data, true);
            // You can return modified objects from here
            // This includes matching the `children` how you want in nested data sets
            return modifiedData;
        }

        // Return `null` if the term should not be displayed
        return null;
    }

    $(".js-example-matcher").select2({
        matcher: matchCustom
    });

    $(document).on("change", "#tender_DOCS_in", function (e) {
        if($('#tender_DOCS_in').val() == ""){
            $("#tender_DOCE_in").val("");
            $("#tender_DOCE_in").attr("disabled", true);
        }
    });

    $(document).on("change", "#tender_DOCE_in", function (e) {

        $("#tender_DOCE_in").attr("readonly", true);

    });

    $( "#tender_DOCS_in" ).datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateStr)
        {
            $("#tender_DOCE_in").attr("disabled", false);
            $("#tender_DOCS_in").attr("readonly", true);
            $("#tender_DOCE_in").datepicker("destroy");
            // $("#fa_in_DD").val(dateStr);
            $("#tender_DOCE_in").datepicker({ minDate: dateStr, dateFormat: 'dd/mm/yy'});
        }
    });

    var status = true;
    function validate() {

        if($("#tender_RN_in").val() === "" && status) {
            status = false;
            $("#err_tender_RN_in").text("Please Enter Tender Reference Number");
            $("#tender_RN_in").focus();
        }
        else{
            $("#err_tender_RN_in").text("");
        }
        if($("#tender_DOTP_in").val() === "" && status) {
            status = false;
            $("#err_tender_DOTP_in").text("Please Enter Date of Tender Purchase");
        }
        else{
            $("#err_tender_DOTP_in").text("");
        }
        if($("#tender_TT_in").val() === "-1" && status) {
            status = false;
            $("#err_tender_TT_in").text("Please Enter Tender Type");
            $("#tender_TT_in").focus();
        }
        else{
            $("#err_tender_TT_in").text("");
        }
        if($("#tender_V_in").val() === "-1" && status) {
            status = false;
            $("#err_tender_V_in").text("Please Enter Vendor Type");
            $("#tender_V_in").focus();
        }
        else{
            $("#err_tender_V_in").text("");
        }
        if($("#tender_PIH_in").val() === "-1" && status) {
            status = false;
            $("#err_tender_PIH_in").text("Please Enter Purchase Item Head");
            $("#tender_PIH_in").focus();
        }
        else{
            $("#err_tender_PIH_in").text("");
        }
        if($("#tender_DOA_in").val() === "" && status) {
            status = false;
            $("#err_tender_DOA_in").text("Please Enter Date of Approval");
        }
        else{
            $("#err_tender_DOA_in").text("");
        }
        if($("#tender_AA_in").val() === "" && status) {
            status = false;
            $("#err_tender_AA_in").text("Please Enter Approval Authority");
            $("#tender_AA_in").focus();
        }
        else{
            $("#err_tender_AA_in").text("");
        }
        if($("#tender_TA_in").val() === "" && status) {
            status = false;
            $("#err_tender_TA_in").text("Please Enter Tender Amount");
            $("#tender_TA_in").focus();
        }
        else{
            $("#err_tender_TA_in").text("");
        }
        if($("#tender_DOCS_in").val() === "" && status) {
            status = false;
            $("#err_tender_DOCS_in").text("Please Enter Date of Contract Start");
        }
        else{
            $("#err_tender_DOCS_in").text("");
        }
        if($("#tender_DOCE_in").val() === "" && status) {
            status = false;
            $("#err_tender_DOCE_in").text("Please Enter Date of Contract Start");
        }
        else{
            $("#err_tender_DOCE_in").text("");
        }

        return status;
    }


    function generateActionBtns(id){

        var btn = "<button type='button' style='text-align:center;vertical-align: middle;font-size:20px; cursor: hand' class='btn fixed_asset_edit' id='"+id+"'>"+
                "<i class='fa fa-pencil'></i>"+
            "</button>"+"<button type='button' style='text-align:center;vertical-align: middle;font-size:20px; color: red;  cursor: hand' class='btn fixed_asset_remove' id='"+id+"'>"+
            "<i class='fa fa-trash-o'></i></button>";

        return btn;
    }
    
    
    
    $.get("/tender-register/get-all-tender",function (data,status) {
        $.each(data,function (index,elm) {
            var tenderReferenceId = elm.tenderReferenceId;
            var dateOfTenderPublish = new Date(elm.dateOfTenderPublish);
            var formatted_date = dateOfTenderPublish.getDate() + "/" + (dateOfTenderPublish.getMonth() + 1) + "/" + dateOfTenderPublish.getFullYear();
            var itemHead = elm.purchaseItemHead.itemHeadName;
            var html = "<tr id='" + elm.id + "'>" +
                "<td>" + itemHead + "</td>" +
                "<td>" + elm.vendor.vendorName + "</td>" +
                "<td>" + formatted_date + "</td>" +
                "<td>" +  tenderReferenceId + "</td>" +
                "<td>" + generateActionBtns(elm.id) + "</td>" +
                "</tr>";

            $("#fixed_assset_item_master_table_body").append(html);


        });

        $("#tender_register_table").DataTable({
            "columnDefs": [
                {"targets": [0], "searchable": false}
            ]
        });
    });



    $(document).on("click", "#tender_register_btn", function(){

        if(validate()) {

            var tenderFormObject = {};
            tenderFormObject.id = $("#tender_pk").val();
            tenderFormObject.tenderReferenceId = $("#tender_RN_in").val();
            tenderFormObject.publishDate = $("#tender_DOTP_in").val();
            tenderFormObject.tenderDetails = $("#tender_TD_in").val();
            tenderFormObject.tenderType = $("#tender_TT_in").val();
            tenderFormObject.vendor = $("#tender_V_in").val();
            tenderFormObject.purchaseItemHead = $("#tender_PIH_in").val();
            tenderFormObject.publishSource = $("#tender_PS_in").val();
            // tenderFormObject.terminate = $("#tender_T_Yes_in").val();
            if($("#tender_T_Yes_in").is(':checked')){
                tenderFormObject.terminate = $("#tender_T_Yes_in").val();
            }
            else{
                tenderFormObject.terminate = $("#tender_T_No_in").val();
            }

            tenderFormObject.tenderApprovalDate = $("#tender_DOA_in").val();
            tenderFormObject.tenderApprovalAuthority = $("#tender_AA_in").val();
            tenderFormObject.tenderAmount = $("#tender_TA_in").val();
            tenderFormObject.modeOfPayment = $("#tender_MOP_in").val();
            tenderFormObject.contractStrat = $("#tender_DOCS_in").val();
            tenderFormObject.contractEnd = $("#tender_DOCE_in").val();
            tenderFormObject.dateOfExtension = $("#tender_DOE_in").val();
            $.ajax({
                contentType: 'application/json',
                url: "add-tender-register-req",
                async: false,
                type: 'POST',
                data: JSON.stringify(tenderFormObject),
                dataType: 'json',
                success: function (response) {

                    if($("#tender_pk").val() != "0")
                    {
                        var tr = '#'+response.empMasterId;
                        $(tr).remove();
                    }
                    var tableRow = "<tr id='"+response.empMasterId+"'>"+
                        "<td >"+$("#tender_PIH_in option:selected").text()+"</td>"+
                        "<td >"+$("#tender_V_in option:selected").text()+"</td>"+
                        "<td >"+$("#tender_DOTP_in").val()+"</td>"+
                        "<td >"+$("#tender_RN_in").val()+"</td>"+
                        "<td >" + generateActionBtns(response.empMasterId) + "</td>"+
                        "</tr>";

                    $("#fixed_assset_item_master_table_body").append(tableRow);


                    showAlert("Successfully saved");
                },
                error: function (xhr, status, error) {
                    alert("Something went wrong!");
                }

            });
        }
        else
        {
            status = true;
        }

    });


    $(document).on("click",".fixed_asset_edit",function (e) {

        var curRow = $(this).closest('tr').attr('id');
        $("#tender_terminate").show();
        $("#tender_extension").show();
        $.ajax({
            url:'get-tender-by-id',
            data:{id:$(this).closest('tr').attr('id')},
            async: false,
            datType:'json',
            type:'GET',
            success:function (response) {

                var dateOfTenderPublish = new Date(response.dateOfTenderPublish);
                var daetOfApproval = new Date(response.daetOfApproval);
                var dateOfContractStart = new Date(response.dateOfContractStart);
                var dateOfContractEnd = new Date(response.dateOfContractEnd);
                if(response.dateOfExtension == null){
                    var format_dateOfContractExt = "";
                }else{
                    var dateOfContractExt = new Date(response.dateOfExtension);
                    var format_dateOfContractExt = dateOfContractExt.getDate() + "/" + (dateOfContractExt.getMonth() + 1) + "/" + dateOfContractExt.getFullYear();
                }

                var format_dateOfTenderPublish = dateOfTenderPublish.getDate() + "/" + (dateOfTenderPublish.getMonth() + 1) + "/" + dateOfTenderPublish.getFullYear();
                var format_daetOfApproval = daetOfApproval.getDate() + "/" + (daetOfApproval.getMonth() + 1) + "/" + daetOfApproval.getFullYear();
                var format_dateOfContractStart = dateOfContractStart.getDate() + "/" + (dateOfContractStart.getMonth() + 1) + "/" + dateOfContractStart.getFullYear();
                var format_dateOfContractEnd = dateOfContractEnd.getDate() + "/" + (dateOfContractEnd.getMonth() + 1) + "/" + dateOfContractEnd.getFullYear();

                $("#tender_pk").val(response.id);
                $("#tender_RN_in").val(response.tenderReferenceId);
                $("#tender_DOTP_in").val(format_dateOfTenderPublish);
                $("#tender_TD_in").val(response.tenderDetails);
                $("#tender_TT_in").val(response.tenderType);
                $('#tender_TT_in').trigger('change');
                $("#tender_V_in").val(response.vendor.id);
                $('#tender_V_in').trigger('change');
                $("#tender_PIH_in").val(response.purchaseItemHead.id);
                $('#tender_PIH_in').trigger('change');
                $("#tender_PS_in").val(response.publishSource);
                $('#tender_PS_in').trigger('change');
                $("#tender_DOA_in").val(format_daetOfApproval);
                $("#tender_AA_in").val(response.approvalAuthority);
                $("#tender_TA_in").val(response.tenderAmount);
                $("#tender_MOP_in").val(response.modeOfPayment);
                $('#tender_MOP_in').trigger('change');
                $("#tender_DOCS_in").val(format_dateOfContractStart);
                $("#tender_DOCE_in").val(format_dateOfContractEnd);
                $("#tender_DOE_in").val(format_dateOfContractExt);
                status = true;
                validate();
            },
            error: function (xhr, status, error) {
                alert("Something went wrong!");
            }
        });



        $("html, body").animate({ scrollTop: 0 }, "slow");
    });


    $(document).on("click",".fixed_asset_remove", function (e) {

        var curRow = $(this).closest('tr').attr('id');

        $.confirm({
            title:'Confirm',
            content:'Delete The Item ?',
            buttons:{
                ok:function(){
                    $.ajax({
                        contentType:'application/json',
                        url:'remove-tender-register',
                        data:{id : curRow},
                        dataType:'json',
                        type:'GET',
                        async:false,
                        success:function(response){
                            var tr = '#'+response.id;
                            $("#tender_pk").val('0');
                            $("#tender_RN_in").val('');
                            $("#tender_DOTP_in").val('');
                            $("#tender_TD_in").val('');
                            $("#tender_TT_in").val('-1');
                            $('#tender_TT_in').trigger('change');
                            $("#tender_V_in").val('-1');
                            $('#tender_V_in').trigger('change');
                            $("#tender_PIH_in").val('-1');
                            $('#tender_PIH_in').trigger('change');
                            $("#tender_PS_in").val('-1');
                            $('#tender_PS_in').trigger('change');
                            $("#tender_DOA_in").val('');
                            $("#tender_AA_in").val('');
                            $("#tender_TA_in").val('');
                            $("#tender_MOP_in").val('-1');
                            $('#tender_MOP_in').trigger('change');
                            $("#tender_DOCS_in").val('');
                            $("#tender_DOCE_in").val('');
                            $("#tender_DOE_in").val('');
                            $(tr).slideUp(function () {
                                $(this).remove();
                            });
                            showAlert("Item has been deleted");
                        },
                        error:function (xhr,status,error) {
                            alert("Something went wrong!");
                        }
                    });
                },
                cancel:function(){

                }
            }
        })
    });




    $(document).on("click", "#tender_register_reload", function (e) {
        location.reload();
    });
});