/**
 * Created by Raj on 7/9/2019.
 */


$(document).ready(function () {

    //select2
    $('.js-example-basic-single').select2();

     $('#fixed_assset_item_master_table').DataTable();
    //Datepicker
    $(document).on("change", "#fa_in_RD", function (e) {
        if($('#fa_in_RD').val() == ""){
            $("#fa_in_DD").val("");
            $("#fa_in_DD").attr("disabled", true);
        }
    });

    $(document).on("change", "#fa_in_DD", function (e) {

        $("#fa_in_DD").attr("readonly", true);

    });

    $( "#fa_in_RD" ).datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateStr)
        {
            $("#fa_in_DD").attr("disabled", false);
            $("#fa_in_RD").attr("readonly", true);
            $("#fa_in_DD").datepicker("destroy");
            // $("#fa_in_DD").val(dateStr);
            $("#fa_in_DD").datepicker({ minDate: dateStr, dateFormat: 'dd/mm/yy'});
        }
    });


    var status = true;
    function validate(){
        if($("#fa_in_RD").val()==="" && status) {
            status = false;
            $("#err_fa_in_RD").text("Please enter office name");
            $("#fa_in_RD").focus();
        }
        else $("#err_fa_in_RD").text("");
        if($("#fa_in_DD").val()==="" && status){
            status = false;
            $("#err_fa_in_DD").text("Please enter office name");
            $("#fa_in_DD").focus();
        }
        else $("#err_fa_in_DD").text("");
        if($("#fa_in_AD").val()==="" && status){
            status = false;
            $("#err_fa_in_AD").text("Please enter office name");
            $("#fa_in_AD").focus();
        }
        else $("#err_fa_in_AD").text("");
        if($("#fa_option_UR").val()==="-1" && status){
            status = false;
            $("#err_fa_option_UR").text("Please enter office name");
            $("#fa_option_UR").focus();
        }
        else $("#err_fa_option_UR").text("");
        return status;
    }
    var itemStatus = true;
    function itemValidate(){

        if($("#fa_select_item").val() === "-1" && itemStatus){
            $("#err_item_category").text("Please select an Item Head");
            $("#fa_select_item").focus();
            itemStatus = false;
        }
        else $("#err_item_category").text("");

        if($("#fa_in_item_count").val() === "" && itemStatus){

            $("#err_item_name").text("Please select an Amount");
            $("#fa_in_item_count").focus();
            itemStatus = false;
        }
        else if(parseInt($("#fa_in_item_count").val()) <= 0){
            $("#err_item_name").text("Please provide a valid amount");
            $("#fa_in_item_count").focus();
            itemStatus = false;
        }

        else $("#err_item_name").text("");

        if($("#fa_in_item_name").val() === "" && itemStatus){
            $("#err_item_count").text("Please give an item name");
            $("#fa_in_item_name").focus();
            itemStatus = false;
        }
        else $("#err_item_count").text("");

        return itemStatus;
    }


    var count;
    $(document).on("click","#fixed_asset_btn",function(e) {

        if(validate()) {

            var fixed_asset_item_request = {};
            fixed_asset_item_request.fixedAssetRequestingOffice = $("#fa_in_RO").val();
            fixed_asset_item_request.fixedAssetRequestedByEmp = $("#fa_in_RBE").val();
            fixed_asset_item_request.fixedAssetRequestDate = $("#fa_in_RD").val();
            fixed_asset_item_request.fixedAssetDueDate = $("#fa_in_DD").val();
            fixed_asset_item_request.fixedAssetAuthoritativeOffice = $("#fa_option_AO").val();
            fixed_asset_item_request.fixedAssetDetails = $("#fa_in_AD").val();
            fixed_asset_item_request.fixedAssetUrgencyRoting = $("#fa_option_UR").val();
            fixed_asset_item_request.fixedAssetRemark = $("#fa_in_R").val();

            $.ajax({
                contentType: 'application/json',
                url: "add-fixed-asset-item-req",
                type: 'POST',
                async: false,
                data: JSON.stringify(fixed_asset_item_request),
                dataType: 'json',
                success: function (response) {
                    showAlert("Successfully saved Master");
                    $("#item_row").show();
                    $('.js-example-basic-single').select2();
                    count = 0;
                },
                error: function (xhr, status, error) {
                    alert("Something went wrong!");
                }
            });

        }else{
            status = true;
        }

    });

    function generateActionBtns(id){

        var btn = "<button type='button' style='text-align:center;vertical-align: middle;font-size:20px;color:red; cursor: hand' class='btn fa fa-trash-o fixed_asset_remove' id='"+id+"'>"+
                "</button>";

        return btn;
    }



    $(document).on("click","#fixed_asset_item_btn",function(e){

        if(itemValidate()) {

            var fixed_asset_item_detail = {};
            fixed_asset_item_detail.itemHeadSelect = $("#fa_select_item").val();
            fixed_asset_item_detail.itemExactName = $("#fa_in_item_name").val();
            fixed_asset_item_detail.itemCount = $("#fa_in_item_count").val();
            fixed_asset_item_detail.fixed_asset_id = $("#fixed_asset_pk").val();
            $.ajax({
                contentType: 'application/json',
                url: "add-fixed-asset-item-details",
                type: 'POST',
                async: false,
                data: JSON.stringify(fixed_asset_item_detail),
                dataType: 'json',
                success: function (response) {
                    if ($("#fixed_asset_pk").val() != "0") {
                        var tr = '#' + response.id;
                        tr.remove();
                    }
                    if (response.id == "-1") {
                        alert("Same Item already exists");
                        return;
                    }


                    var tr = "<tr id ='" + response.id + "'>" +
                        "<td style='text-align: center'>" + (++count) + "</td>" +
                        "<td style='text-align: center'>" + $('#fa_select_item option:selected').text() + "</td>" +
                        "<td style='text-align: center'>" + $('#fa_in_item_count').val() + "</td>" +
                        "<td style='text-align: center'>" + $('#fa_in_item_name').val() + "</td>" +
                        "<td style='text-align: center'>" + generateActionBtns(response.id) + "</td>"
                    "</tr>";
                    $("#fixed_asset_table_body").append(tr);
                    $("#fa_select_item").val(-1);
                    $("#fa_in_item_count").val("");
                    $("#fa_in_item_name").val("");
                    showAlert("Successfully saved Item");


                },
                error: function (xhr, status, error) {
                    alert("Something went wrong!");
                }
            })
        }else{
            itemStatus = true;
        }


    });

    $(document).on("click",".fixed_asset_remove", function(e){

        var curRow = $(this).closest('tr').attr('id');

        $.confirm({
            title:'Confirm',
            content:'Delete The Item ?',
            buttons:{
                ok:function(){
                    $.ajax({
                        contentType:'application/json',
                        url:'remove-fixed-asset-item',
                        data:{id : curRow},
                        dataType:'json',
                        type:'GET',
                        async:false,
                        success:function(response){
                            showAlert("Item has been deleted");
                            var tr = '#'+response.id;
                            $(tr).slideUp(function () {
                               $(this).remove();
                            });
                        },
                        error:function (xhr,status,error) {
                            alert("Something went wrong!");
                        }
                    })
                },
                cancel:function(){

                }
            }
        })



    });


    $(document).on("click", "#fixed_asset_reload", function (e) {
        location.reload();
    });




});
