/**
 * Created by Raj on 7/23/2019.
 */

$(document).ready(function () {

    //select2
    $('.js-example-basic-single').select2();
    //Datatable
    var table = $("#item_pending_table").DataTable();

    var requestTable = $("#request_details_table").DataTable();


    //Datepicker
    $(document).on("change", "#item_pending_DF_in", function (e) {
        if($('#item_pending_DF_in').val() == ""){
            $("#item_pending_DT_in").val("");
            $("#item_pending_DT_in").attr("disabled", true);
        }
    });

    $(document).on("change", "#item_pending_DT_in", function (e) {
        $("#item_pending_DT_in").attr("readonly", true);
    });


    //Validation
    var status = true;
    function validate(){
        if($("#item_pending_ROC_in").val()==="-1" && status){
            status = false;
            $("#err_item_pending_ROC_in").text("Please enter office Category");
            $("#item_pending_ROC_in").focus();
        }
        else $("#err_item_pending_ROC_in").text("");

        if($("#item_pending_RO_in").val()==="-1" && status){
            status = false;
            $("#err_item_pending_RO_in").text("Please enter office name");
            $("#item_pending_RO_in").focus();
        }
        else $("#err_item_pending_RO_in").text("");

        var from = $('#item_pending_DF_in').datepicker("getDate");
        var to = $('#item_pending_DT_in').datepicker("getDate");
        if(from > to && status){
            status = false;
            $("#err_item_pending_DT_in").text("Please enter correct date");

        }
        else $("#err_item_pending_DT_in").text("");
        return status;
    }

    function approvalValidation(id, amount) {

       if($("#input"+id).val() == ""){
           alert("Please insert amount before approving request");
           return false;
       }
       else if($("#input"+id).val() == ""){


           return false;
       }
       else{
           return true;
       }
    }


    $( "#item_pending_DF_in" ).datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateStr)
        {
            $("#item_pending_DT_in").attr("disabled", false);
            $("#item_pending_DF_in").attr("readonly", true);
            $("#item_pending_DT_in").datepicker("destroy");
            // $("#fa_in_DD").val(dateStr);
            $("#item_pending_DT_in").datepicker({ minDate: dateStr, dateFormat: 'dd/mm/yy'});
        }
    });




    function generateActionBtns(id){

        var btn = "<button type='button' style='text-align:center;vertical-align: middle;font-size:15px; cursor: hand' class='btn view_details_btn' id='"+id+"'>"+
            "View Details</button>";
        return btn;
    }

    function amountText(amount) {
        var text = "<p>"+amount+"</p>"
        return text;
    }


    function approvalBtns(id) {
        var btn = "<button type='button' name='"+id+"' data-toggle='tooltip' data-placement='bottom' title='Approve' style='text-align:center;vertical-align: middle;font-size:15px; cursor: hand' class='btn btn-default request_approve_btn' id='Accept"+id+"'>"+
            "<span class='glyphicon glyphicon-ok' style='color: #00A000'></button>" + "<button type='button' data-toggle='tooltip' data-placement='bottom' title='Decline' style='text-align:center;vertical-align: middle;font-size:15px; cursor: hand' name='"+id+"' class='btn btn-default request_decline_btn' id='Decline"+id+"'>"+
            "<span class='glyphicon glyphicon-remove' style='color:red'></button>"+
            "<span name='approved"+id+"' style='text-align: center; display: none; color: #00A000'>Approved</span>";

        return btn;
    }

    function allocatedItemsField(id) {
        var input = "<input id='input"+id+"' type='number' min='1'/>";
        return input;
    }


    $(document).on('click', '.request_approve_btn', function () {
        var val = $(this).attr('name');
        var reqAmount = $('#input'+val).val();
        if(approvalValidation(val, reqAmount)){
            var tr = $(this).closest('tr');
            var itemDto = {};
            itemDto.id = val;
            itemDto.amount = $('#input'+val).val();
            $.confirm({
                title:'Confirm',
                content:'Approve The Item ?',
                buttons:{
                    ok:function(){
                        $.ajax({
                            contentType:'application/json',
                            url:'add-item-approved-confirmation',
                            type:'POST',
                            data:JSON.stringify(itemDto),
                            async:false,
                            dataType:'json',
                            success:function (response) {
                                if(response.flag == '0'){
                                    alert("Request Declined, Allocating more items than requested.");
                                }
                                else{
                                    $('[name="'+val+'"]').hide();
                                    $('[name="approved'+val+'"]').show();
                                    $(tr).hide();
                                    showAlert("Request Approved");
                                }
                            },
                            error:function (xhr,status,error) {
                                alert("Something went wrong!");
                            }

                        });
                    },
                    cancel:function(){

                    }
                }
            })
        }
        else{

        }
    });

    $(document).on('click', '.request_decline_btn', function () {
        var val = $(this).attr('name');
        var tr = $(this).closest('tr');
        $.confirm({
            title:'Confirm',
            content:'Approve The Item ?',
            buttons:{
                ok:function(){
                    $.ajax({
                        contentType:'application/json',
                        url:'add-item-decline-confirmation',
                        type:'GET',
                        data:{id:val},
                        async:false,
                        success:function (response) {
                            $(tr).hide();
                            showAlert("Request Declined");
                        },
                        error:function (xhr,status,error) {
                            alert("Something went wrong!");
                        }

                    });
                },
                cancel:function(){

                }
            }
        })
    });





    $(document).on('click', '.view_details_btn', function () {
       $("#popup_item_request_details").show();
        var n = $(document).height();
        $('html, body').animate({ scrollTop: n }, 'slow');
        var value = $(this).attr('id');
        $.ajax({
            contentType:'application/json',
            url:'get-fa-item-pending-details',
            data:{id: value},
            type:'GET',
            dataType:'json',
            async:false,
            success:function (response) {
                console.log(response.empName);
                var count = 0;
                requestTable.clear().draw();
                $.each(response.list, function (index, data) {
                    $("#fa_RO_label").text(data.faItemRequestMstId.requestingOfficeId.officeName);
                    $("#fa_RBE_label").text(response.empName);
                    var reqDate = new Date(data.faItemRequestMstId.requestDate);
                    var formattedDate = reqDate.getDate() + "/" + (reqDate.getMonth()+1) + "/" + reqDate.getFullYear();
                    $("#fa_RD_label").text(formattedDate);
                    if(data.faItemRequestMstId.urgencyLevel == '1'){
                        $("#fa_UR_label").text("Very Urgent");
                    }
                    else if(data.faItemRequestMstId.urgencyLevel == '2'){
                        $("#fa_UR_label").text("Urgent");
                    }
                    else if(data.faItemRequestMstId.urgencyLevel == '3'){
                        $("#fa_UR_label").text("Not Urgent");
                    }
                    // $("#fa_UR_label").text(data.faItemRequestMstId.urgencyLevel);
                    $("#fa_RE_label").text();
                    var dueDate = new Date(data.faItemRequestMstId.dueDate);
                    var formattedDueDate = dueDate.getDate() + "/" + (dueDate.getMonth()+1) + "/" + dueDate.getFullYear();
                    $("#fa_DD_label").text(formattedDueDate);
                    // var tr = "<tr id ='" + data.id + "'>" +
                    //     "<td style='text-align: center'>" + (++count) + "</td>" +
                    //     "<td style='text-align: center'>" + data.faItemHeadId + "</td>" +
                    //     "<td style='text-align: center'>" + data.faItemHeadQuantity + "</td>" +
                    //     "<td style='text-align: center'>" +  + "</td>" +
                    //     "<td style='text-align: center'>" + data.faItemSpecificName + "</td>" +
                    //     "<td style='text-align: center'>" + approvalBtns(data.id) + "</td>"
                    // "</tr>";
                    // $("#fa_item_pending_table_body").append(tr);

                    if(data.pendingReq == '0'){
                        requestTable.row.add([
                            (++count),
                            data.faItemHeadId.itemHeadName,
                            data.faItemHeadQuantity,
                            allocatedItemsField(data.id),
                            data.faItemSpecificName,
                            approvalBtns(data.id)
                        ]).draw( false );
                    }
                    else if(data.pendingReq == '1'){
                        requestTable.row.add([
                            (++count),
                            data.faItemHeadId.itemHeadName,
                            data.faItemHeadQuantity,
                            amountText(data.approvedQuantity),
                            data.faItemSpecificName,
                            "approved"
                        ]).draw( false );
                    }
                    else{
                        requestTable.row.add([
                            (++count),
                            data.faItemHeadId.itemHeadName,
                            data.faItemHeadQuantity,
                            "none",
                            data.faItemSpecificName,
                            "Declined"
                        ]).draw( false );
                    }
                });
            },
            error:function (xhr,status,error) {
                alert("Something went wrong!");
            }
        });
    });

    $(document).on("change","#item_pending_ROC_in", function (e) {
        var catId = $("#item_pending_ROC_in option:selected").val();
        $.get("/fixed-asset-item-pending/get-item-pending-office?offCatgId=" + catId,
            function (data, status) {
                //REMOVING VALIDATION TEXT(IF EXISTS)
                $("#err_item_pending_ROC_in").text("");
                var office = $('#item_pending_RO_in');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Office---"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices.id,
                        text: offices.officeName
                    }));
                });

            });
    });

    $(document).on('change', '#item_pending_RO_in', function (e) {
        //REMOVING VALIDATION TEXT(IF EXISTS)
        $("#err_item_pending_RO_in").text("");
    })


    $(document).on("click","#item_pending_save_btn", function (e) {

        if(validate()){
            var from = $('#item_pending_DF_in').datepicker("getDate");
            var to = $('#item_pending_DT_in').datepicker("getDate");
            var catId = $("#item_pending_RO_in option:selected").val();
            $.get("/fixed-asset-item-pending/get-item-pending-req-office-list?offId=" + catId,function (data,status) {
                table.clear().draw();
                var count = 0;
                if($('#item_pending_DF_in').val() == '' && $('#item_pending_DT_in').val()== ''){
                    $.each(data,function (index,elm) {
                        var requestingOfficeId = elm.requestingOfficeId;
                        var requestDate = new Date(elm.requestDate);
                        var date,month,year;
                        if(requestDate.getDate()<"10"){
                            date = '0' + requestDate.getDate();
                        }
                        else{
                            date = requestDate.getDate();
                        }
                        if((requestDate.getMonth() + 1)<"10"){
                            month = '0' + (requestDate.getMonth() + 1);
                        }
                        else{
                            month = requestDate.getMonth();
                        }
                        if(requestDate.getFullYear()<"10"){
                            year = '0' + requestDate.getFullYear();
                        }
                        else{
                            year = requestDate.getFullYear();
                        }
                        var formatted_date = date + "/" + month + "/" + year;
                        var pendingStatus;

                        table.row.add([
                            ++count,
                            $("#item_pending_RO_in option:selected").text(),
                            formatted_date,
                            generateActionBtns(elm.id)
                        ]).draw( false );

                    });
                }
                else{
                    $.each(data,function (index,elm) {
                        var show = true;
                        var requestDate = new Date(elm.requestDate);
                        if (from && requestDate < from)
                            show = false;

                        //if to date is valid and row date is greater than to date, hide the row
                        if (to && requestDate > to)
                            show = false;
                        if(show){
                            var date,month,year;
                            if(requestDate.getDate()<"10"){
                                date = '0' + requestDate.getDate();
                            }
                            else{
                                date = requestDate.getDate();
                            }
                            if((requestDate.getMonth() + 1)<"10"){
                                month = '0' + (requestDate.getMonth() + 1);
                            }
                            else{
                                month = requestDate.getMonth();
                            }
                            if(requestDate.getFullYear()<"10"){
                                year = '0' + requestDate.getFullYear();
                            }
                            else{
                                year = requestDate.getFullYear();
                            }
                            var formatted_date = date + "/" + month + "/" + year;
                            var pendingStatus;

                            table.row.add([
                                ++count,
                                $("#item_pending_RO_in option:selected").text(),
                                formatted_date,
                                generateActionBtns(elm.id)
                            ]).draw( true );
                        }


                    });
                }
            });
        }
        else{
            status = true;
        }
    });

    $(document).on("click", "#item_pending_reload_btn", function (e) {
        location.reload();
    });
});