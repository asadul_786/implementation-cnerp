$(document).ready(function (){

    $(document).on('input', '.phone', function(e){
        e.target.value = e.target.value.replace(/[^0-9+]/g,'');
    });

    /*if (mobileNo.charAt(0) != "0" || mobileNo.charAt(1) != "1" || mobileNo.length != 11){

     return false ;
     }
     else {
     return true;
     }
     */
    function validateMobileNo(mobileNo){

        var phoneno = /^\+[0-9]{13,17}$/;
        if(mobileNo.match(phoneno)) {
            return true;
        }
        else {
            return false;
        }
    }

    function ValidateEmail(mail)
    {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(mail);
    }

    function hideValidation (){
        $('#err_fa_vendor_name_id').hide();
        $('#err_fa_vendor_add_id').hide();
        $('#err_fa_vendor_contact_no_id').hide();
        $('#err_fa_vendor_email_add_id').hide();
        $('#err_fa_vendor_quality').hide();
        $('#err_fa_vendor_type').hide();
        $('#err_fa_auth_person_name_id').hide();
        $('#err_fa_auth_person_details').hide();
        $('#err_fa_emergency_contact_no_id').hide();
        $('#err_fa_contract_reg_date').hide();
        $('#err_fa_blacklist').hide();

    }

    function validate() {
        hideValidation();

        if ($('#fa_vendor_name_id').val() == "") {

            $('#err_fa_vendor_name_id').text("Please enter vendor name");
            $('#err_fa_vendor_name_id').show();

            return false;

        }
        else {
            $('#err_fa_vendor_name_id').hide();
        }

        if ($('#fa_vendor_add_id').val() == "") {

            $('#err_fa_vendor_add_id').text("Please enter vendor address");
            $('#err_fa_vendor_add_id').show();
            return false;
        }

        else {
            $('#err_fa_vendor_add_id').hide();
        }

        if ($('#fa_vendor_contact_no').val() == "") {

            $('#err_fa_vendor_contact_no_id').text("Please enter vendor contact no");
            $('#err_fa_vendor_contact_no_id').show();
            return false;
        }

        else {
            $('#err_fa_vendor_contact_no_id').hide();
            var mobileNo = $("#fa_vendor_contact_no").val();

            var flag = true;
            flag = validateMobileNo(mobileNo);
             if (!flag) {
                 $('#err_fa_vendor_contact_no_id').text("Please enter valid mobile number");
                 $('#err_fa_vendor_contact_no_id').show();
                 return false;
             }

            }

        if ($("#fa_vendor_email_add_id").val() == "") {
            $('#err_fa_vendor_email_add_id').text("Please enter vendor email address");
            $('#err_fa_vendor_email_add_id').show();
            return false;
        }

        else {
            $('#err_fa_vendor_email_add_id').hide();
            var mail= $("#fa_vendor_email_add_id").val();

            if(!ValidateEmail(mail)){
                $('#err_fa_vendor_email_add_id').text("Please enter valid email address");
                $('#err_fa_vendor_email_add_id').show();
                return false;
            }
        }

       if ($("#fa_vendor_quality").val() == "-1") {
            $('#err_fa_vendor_quality').text("Please enter vendor quality");
            $('#err_fa_vendor_quality').show();
            return false;
        }

        else {
            $('#err_fa_vendor_quality').hide();
        }

        if ($("#fa_vendor_type").val() == "-1") {
            $('#err_fa_vendor_type').text("Please enter vendor type");
            $('#err_fa_vendor_type').show();
            return false;
        }

        else {
            $('#err_fa_vendor_type').hide();
        }

        if ($('#fa_auth_person_name_id').val() == "") {
            $('#err_fa_auth_person_name_id').text("Please enter person name");
            $('#err_fa_auth_person_name_id').show();
            return false;

        }
        else {
            $('#err_fa_auth_person_name_id').hide();
        }

        if ($('#fa_auth_person_details').val() == "") {
            $('#err_fa_auth_person_details').text("Please enter person details");
            $('#err_fa_auth_person_details').show();
            return false;

        }
        else {
            $('#err_fa_auth_person_details').hide();
        }

        if ($('#fa_emergency_contact_no_id').val() == "") {
            $('#err_fa_emergency_contact_no_id').text("Please enter contact no");
            $('#err_fa_emergency_contact_no_id').show();
            return false;

        }
        else {
            $('#err_fa_emergency_contact_no_id').hide();
             var urgentMobileNo = $("#fa_emergency_contact_no_id").val();

            if (!validateMobileNo(urgentMobileNo)){
                $('#err_fa_emergency_contact_no_id').text("Please enter valid mobile number");
                $('#err_fa_emergency_contact_no_id').show();
                return false;
            }

        }

        if ($('#fa_contract_reg_date').val() == "") {
            $('#err_fa_contract_reg_date').text("Please select a date");
            $('#err_fa_contract_reg_date').show();
            return false;

        }
        else {
            $('#err_fa_contract_reg_date').hide();
        }

        if ($('#fa_blacklist').val() == "") {

            $('#err_fa_blacklist').show();
            return false;
        }
        else {
            $('#err_fa_blacklist').hide();
        }

        return true;
    }

    var table = $("#vendorsList").DataTable({
        "columnDefs": [
            {"targets": [0], "searchable": false}
        ]
    });

    var curRowEditMode;

    $('#vendorsList').on('click', '#edit', function () {
        hideValidation();

        $("#fixed_asset_vendor_save").prop("value", "Update");

        curRowEditMode = $(this).closest('tr');
        var col1 = curRowEditMode.find('td:eq(0)').text();
        var col2 = curRowEditMode.find('td:eq(1)').text();
        var col3 = curRowEditMode.find('td:eq(2)').text();
        var col4 = curRowEditMode.find('td:eq(3)').text();
        var col5 = curRowEditMode.find('td:eq(4)').text();
        var col6 = $.trim(curRowEditMode.find('td:eq(5)').text());
        var col7 = $.trim(curRowEditMode.find('td:eq(6)').text());
        var col8 = curRowEditMode.find('td:eq(7)').text();
        var col9 = curRowEditMode.find('td:eq(8)').text();
        var col10 = curRowEditMode.find('td:eq(9)').text();
        var col11 = $.trim(curRowEditMode.find('td:eq(10)').text());
        var col12 = $.trim(curRowEditMode.find('td:eq(11)').text());
        var col13 = $.trim(curRowEditMode.find('td:eq(12)').text());


        $('#id').val(col1);
        $('#fa_vendor_name_id').val(col2);
        $('#fa_vendor_add_id').val(col3);
        $('#fa_vendor_contact_no').val(col4);
        $('#fa_vendor_email_add_id').val(col5);

        if (col6 == "Good")
            $('#fa_vendor_quality').val(1);
        else if (col6 == "Moderate")
            $('#fa_vendor_quality').val(2);
        else if (col6 == "Less Moderate")
            $('#fa_vendor_quality').val(3);
        else
            $('#fa_vendor_quality').val(-1);

        if (col7 == "Tender")
            $('#fa_vendor_type').val(1);
        else if (col7 == "Quotation")
            $('#fa_vendor_type').val(2);
        else
            $('#fa_vendor_type').val(-1);

        $('#fa_auth_person_name_id').val(col8);
        $('#fa_auth_person_details').val(col9);
        $('#fa_emergency_contact_no_id').val(col10);
        $('#fa_contract_reg_date').val(col11);

        if (col12 == "Yes")
            $('#fa_blacklist').prop("checked", true);
        else
            $('#fa_blacklist').prop("checked", false);



        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);
    });

    $('#vendorsList').on('click', '#delete', function () {
        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/fixed-asset/delete/" + col1,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            if (response == true) {
                                table.row(curRow).remove().draw(false);
                                showAlert("Deleted Successfully");
                            }
                            else {
                                showAlert("Unknown error");
                            }

                        },
                        error: function (xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

    // function Remark(){
    //     fixed_asset_vendor.remark = $("#fa_remark").val();
    //     $("#fa_remark").show();
    //  }

    var fixed_asset_vendor = {};

    $(document).on("click", "#fixed_asset_vendor_save", function (e) {

        if (validate()) {

            fixed_asset_vendor.id = $("#id").val();
            fixed_asset_vendor.vendorName = $("#fa_vendor_name_id").val();
            fixed_asset_vendor.vendorAddress = $("#fa_vendor_add_id").val();
            fixed_asset_vendor.vendorCell = $("#fa_vendor_contact_no").val();
            fixed_asset_vendor.vendorEmail = $("#fa_vendor_email_add_id").val();
            fixed_asset_vendor.vendorQuality = $("#fa_vendor_quality").val();
            fixed_asset_vendor.vendorType = $("#fa_vendor_type").val();
            fixed_asset_vendor.authName = $("#fa_auth_person_name_id").val();
            fixed_asset_vendor.authPersonName = $("#fa_auth_person_details").val();
            fixed_asset_vendor.urgentContact =  $("#fa_emergency_contact_no_id").val();
            fixed_asset_vendor.contractRegDate = $("#fa_contract_reg_date").val();

            var venBlacklist;

            if ($("#fa_blacklist").is(":checked")) {
                fixed_asset_vendor.isBlacklisted = 1;
                venBlacklist = "Yes";
            }
            else
            {
                fixed_asset_vendor.isBlacklisted = 0;
                venBlacklist = "No";
            }
            fixed_asset_vendor.remark = $("#fa_remark").val();


            var venQuality = $("#fa_vendor_quality :selected").text();
            var venType = $("#fa_vendor_type :selected").text();


            $.ajax({
                contentType: 'application/json',
                url: "add-fixed-asset-vendor",
                type: 'POST',
                //async: false,
                data: JSON.stringify(fixed_asset_vendor),
                dataType: 'json',
                success: function (response) {

                    var editBtn = "<button id='edit' type='button' class='btn fa fa-edit' style='text-align:center;vertical-align: middle;font-size:20px;'></button>";
                    var dltBtn = "<button id='delete' type='button' class='btn fa fa-trash-o' style='text-align:center;vertical-align: middle;font-size:20px;color:red;'></button>";

                    if (fixed_asset_vendor.id != "") {
                        //Update
                        var rowIndex = table.row( curRowEditMode ).index();
                        var updatedRow = table.row( rowIndex ).data( [response, fixed_asset_vendor.vendorName, fixed_asset_vendor.vendorAddress, fixed_asset_vendor.vendorCell, fixed_asset_vendor.vendorEmail, venQuality, venType, fixed_asset_vendor.authName, fixed_asset_vendor.authPersonName, fixed_asset_vendor.urgentContact, fixed_asset_vendor.contractRegDate, venBlacklist, editBtn, dltBtn] ).invalidate();

                        updatedRow.nodes().to$().attr("style", "background-color: rgb(255, 245, 238);");
                        updatedRow.nodes().to$().attr("id", "updateScroll");
                        document.getElementById("updateScroll").scrollIntoView();
                        updatedRow.nodes().to$().removeAttr("id");

                        setTimeout(function(){
                            updatedRow.nodes().to$().removeAttr("style");
                        }, 3000);

                        showAlert("Updated successfully");
                    }
                    else {
                        //ADD
                        var newRow = table.row.add([response, fixed_asset_vendor.vendorName, fixed_asset_vendor.vendorAddress, fixed_asset_vendor.vendorCell, fixed_asset_vendor.vendorEmail, venQuality, venType, fixed_asset_vendor.authName, fixed_asset_vendor.authPersonName, fixed_asset_vendor.urgentContact, fixed_asset_vendor.contractRegDate, venBlacklist, fixed_asset_vendor.remark, editBtn, dltBtn]).draw();
                        var colCount = table.columns()[0].length;

                        newRow.nodes().to$().find("td:eq(0)").attr("style", "text-align:center;display: none");
                        newRow.nodes().to$().attr("style", "background-color: rgb(255, 245, 238);");
                        newRow.nodes().to$().attr("id", "newScroll");
                        var i;
                        for(i = 1; i < colCount; i++){
                            newRow.nodes().to$().find("td:eq(" + i + ")").attr("style", "text-align:center;vertical-align: middle;");
                        }
                        newRow.show().draw(false);
                        document.getElementById("newScroll").scrollIntoView();
                        newRow.nodes().to$().find("tr").removeAttr("id");

                        setTimeout(function(){
                            newRow.nodes().to$().find("tr").removeAttr("style");
                        }, 3000);

                        showAlert("Saved successfully");
                    }

                    $("#fixed_asset_vendor_reset").click();

                },
                error: function (xhr, status, error) {
                    showAlert("Something Went Wrong!");
                }
            });
        }
    });

    $(document).on("click", "#fixed_asset_vendor_reset", function (e) {

        hideValidation();
            $("#id").val('');
            $("#fixed_asset_vendor_save").prop("value", "Save");

        });

   });
