/**
 * Created by Raj on 01-Aug-19.
 */

$(document).ready(function () {

    //select2
    $('.js-example-basic-single').select2();

    //DataTable
    var table = $("#update_inventory_table").DataTable();

    $(document).on("click", "#update_inventory_reload_btn", function (e) {
        location.reload();
    });


    //specific office after changing office category

    $(document).on('change', '#update_inventory_OC_in', function () {
        var catId = $("#update_inventory_OC_in option:selected").val();
        $.get("/fixed-asset-inventory-update/get-item-pending-office?offCatgId=" + catId,
            function (data, status) {
                //REMOVING VALIDATION TEXT(IF EXISTS)
                $("#err_update_inventory_OC_text").text("");
                var office = $('#update_inventory_O_in');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "--Select Office--"
                }));
                $.each(data, function (index, offices) {
                    office.append($('<option/>', {
                        value: offices.id,
                        text: offices.officeName
                    }));
                });

            });
    });

    //changing purchase type
    $(document).on('change', '#update_inventory_PT_text', function () {
        if($("#update_inventory_PT_text option:selected").val() == '1'){
            $("#update_inventory_TRN_text").attr('disabled',false);
            $.get("/fixed-asset-inventory-update/get-tender",
                function (data, status) {
                    //REMOVING VALIDATION TEXT(IF EXISTS)
                    $("#err_update_inventory_OC_text").text("");
                    var tender = $('#update_inventory_TRN_text');
                    tender.empty();
                    tender.append($('<option/>', {
                        value: "-1",
                        text: "--Select Tender--"
                    }));
                    $.each(data, function (index, tenders) {
                        tender.append($('<option/>', {
                            value: tenders.id,
                            text: tenders.tenderReferenceId
                        }));
                    });

                });
        }
        else{
            $("#update_inventory_TRN_text").val('-1');
            $("#update_inventory_TRN_text").attr('disabled',true);
        }
    });

    //save tender
    $(document).on('click', '#update_inventory_save_btn', function () {

        var item_inventory = {};
        item_inventory.stockFor = $("#update_inventory_O_in").val();
        item_inventory.receivedBy = $("#update_inventory_RB_text").val();
        item_inventory.purchaseType = $("#update_inventory_PT_text").val();
        item_inventory.tenderRefNo = $("#update_inventory_TRN_text").val();
        item_inventory.vendor = $("#update_inventory_V_text").val();
        item_inventory.itemRecDate = $("#update_inventory_DOIR_text").val();
        item_inventory.stockUpdateDate = $("#update_inventory_DOSU_text").val();
        item_inventory.receivedRemark = $("#update_inventory_RR_text").val();
        $.confirm({
            title:'Confirm',
            content:'Are you sure ?',
            buttons:{
                ok:function(){
                    $.ajax({
                        contentType:'application/json',
                        url:'add-tender-item-inventory',
                        data:JSON.stringify(item_inventory),
                        dataType:'json',
                        type:'POST',
                        async:false,
                        success:function(response){
                            showAlert("Tender Saved");
                        },
                        error:function (xhr,status,error) {
                            alert("Something went wrong!");
                        }
                    })
                },
                cancel:function(){

                }
            }
        })

    });
});