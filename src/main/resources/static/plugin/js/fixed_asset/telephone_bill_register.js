$(document).ready(function () {

    //$('.js-example-basic-single').select2();

    var table = $('#telephone_bill_register_table').DataTable({
        "columnDefs": [
            { "targets": [0, 1, 2, 3], "searchable": false }
        ]
    });

    $(document).on('input', '#telephone_number_id', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });

    $(document).on('input', '#bill_line_rent_id', function(e){
        e.target.value = e.target.value.replace(/[^1-9]/g,'');
    });

    $(document).on('input', '#bill_vat_amount_id', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });

    $(document).on('input', '#bill_total_amount_id', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
    });

    $('#office_category').on('click', function () {
        $('#err_office_category').hide();
    });

    $('#office_id_id').on('click', function () {
        $('#err_office_id_id').hide();
    });

    $('#biller_name_id').keyup(function () {
        $('#err_biller_name_id').hide();
    });

    $('#telephone_type_id').on('click', function () {
        $('#err_telephone_type_id').hide();
    });

    $('#telephone_number_id').keyup(function () {
        $('#err_telephone_number_id').hide();
    });

    $('#bill_no_id').keyup(function () {
        $('#err_bill_no_id').hide();
    });

    $('#bill_month_id').on('click', function () {
        $('#err_bill_month_id').hide();
    });

    $('#bill_line_rent_id').keyup(function () {
        $('#err_bill_line_rent_id').hide();
    });

    $('#bill_vat_amount_id').keyup(function () {
        $('#err_bill_vat_amount_id').hide();
    });

    $('#bill_total_amount_id').keyup(function () {
        $('#err_bill_total_amount_id').hide();
    });

    $('#bill_last_pay_date_id').on('click', function () {
        $('#err_bill_last_pay_date_id').hide();
    });

    $('#bill_payment_date_id').on('click', function () {
        $('#err_bill_payment_date_id').hide();
    });

    function FieldValidation()
    {

        $('#err_office_category').hide();
        $('#err_office_id_id').hide();
        $('#err_biller_name_id').hide();
        $('#err_telephone_type_id').hide();
        $('#err_telephone_number_id').hide();
        $('#err_bill_no_id').hide();
        $('#err_bill_month_id').hide();
        $('#err_bill_line_rent_id').hide();
        $('#err_bill_vat_amount_id').hide();
        $('#err_bill_total_amount_id').hide();
        $('#err_bill_last_pay_date_id').hide();
        $('#err_bill_payment_date_id').hide();


        if ($('#office_category').val() == "-1")
        {
            $('#err_office_category').text("Please enter office category");
            $('#err_office_category').show();

            return false;
        }
        else
        {
            $('#err_office_category').hide();
        }


        if ($('#office_id_id').val() == "-1")
        {
            $('#err_office_id_id').text("Please enter office id");
            $('#err_office_id_id').show();

            return false;
        }
        else
        {
            $('#err_office_id_id').hide();
        }


        if ($('#biller_name_id').val() == "")
        {
            $('#err_biller_name_id').text("Please enter consumer name");
            $('#err_biller_name_id').show();

            return false;
        }
        else
        {
            $('#err_biller_name_id').hide();
        }



        if ($('#telephone_type_id').val() == "-1")
        {
            $('#err_telephone_type_id').text("Please enter telephone type");
            $('#err_telephone_type_id').show();

            return false;
        }
        else
        {
            $('#err_telephone_type_id').hide();
        }

        if ($('#telephone_number_id').val() == "")
        {
            $('#err_telephone_number_id').text("Please enter telephone number");
            $('#err_telephone_number_id').show();

            return false;
        }
        else
        {
            $('#err_telephone_number_id').hide();
        }


        if ($('#bill_no_id').val() == "")
        {
            $('#err_bill_no_id').text("Please enter bill no.");
            $('#err_bill_no_id').show();

            return false;
        }
        else
        {
            $('#err_bill_no_id').hide();
        }


        if ($('#bill_month_id').val() == "")
        {
            $('#err_bill_month_id').text("Please enter bill month");
            $('#err_bill_month_id').show();

            return false;
        }
        else
        {
            $('#err_bill_month_id').hide();
        }


        if ($('#bill_line_rent_id').val() == "")
        {
            $('#err_bill_line_rent_id').text("Please enter bill line rent");
            $('#err_bill_line_rent_id').show();

            return false;
        }
        else
        {
            $('#err_bill_line_rent_id').hide();
        }


        if ($('#bill_vat_amount_id').val() == "")
        {
            $('#err_bill_vat_amount_id').text("Please enter bill vat amount");
            $('#err_bill_vat_amount_id').show();

            return false;
        }
        else
        {
            $('#err_bill_vat_amount_id').hide();
        }


        if ($('#bill_total_amount_id').val() == "")
        {
            $('#err_bill_total_amount_id').text("Please enter bill total amount");
            $('#err_bill_total_amount_id').show();

            return false;
        }
        else
        {
            $('#err_bill_total_amount_id').hide();
        }


        if ($('#bill_last_pay_date_id').val() == "")
        {
            $('#err_bill_last_pay_date_id').text("Please enter bill last pay date");
            $('#err_bill_last_pay_date_id').show();

            return false;
        }
        else
        {
            $('#err_bill_last_pay_date_id').hide();
        }


        if ($('#bill_payment_date_id').val() == "")
        {
            $('#err_bill_payment_date_id').text("Please enter bill total amount");
            $('#err_bill_payment_date_id').show();

            return false;
        }
        else
        {
            $('#err_bill_payment_date_id').hide();
        }
        return true;
    }



    $('#office_category').change(function () {

        var dropdown_val = $('#office_category').val();

        if(dropdown_val != "-1"){ //Dropdown value is not -- Select --

            $.ajax({
                contentType: 'application/json',
                url: "/fixed-asset/get-offices-by-category/" + dropdown_val,
                type: 'GET',
                dataType: 'json',
                success: function(response) {

                    console.log(response);
                    $("#office_id_id").empty();

                    if(response.length == 0)
                    {
                        $("#office_id_id").append($('<option></option>').val(-1).html("No offices available ..."));
                    }
                    else
                    {
                        $("#office_id_id").append($('<option></option>').val('-1').html('-- Select --'));
                        $.each( response, function( key, value )
                        {
                            $("#office_id_id").append($('<option></option>').val(value.id).html(value.officeName));
                        });
                    }

                },
                error: function(xhr, status, error) {
                    alert(xhr.status);
                }
            }).done(function(data){

                if(editModeEnabled == 1) {
                    var offId = $.trim(curRowEditMode.find('td:eq(1)').text());
                    $('#office_id_id').val(offId);
                    editModeEnabled = 0;
                }

            });
        }
        else
        {
            $("#office_id_id").empty();
            $("#office_id_id").append($('<option></option>').val('-1').html('-- Select --'));
        }

    });


    var telephoneBillRegister = {};

    $(document).on("click", "#telephone_bill_register_save", function () {

        if (FieldValidation())
        {
            telephoneBillRegister.id = $("#id").val();
            telephoneBillRegister.officeCatId = $("#office_category").val();
            telephoneBillRegister.officeId = $("#office_id_id").val();
            telephoneBillRegister.billerName = $("#biller_name_id").val();
            telephoneBillRegister.telephoneType = $("#telephone_type_id").val();
            telephoneBillRegister.telephoneNumber = $("#telephone_number_id").val();
            telephoneBillRegister.billNo = $("#bill_no_id").val();
            telephoneBillRegister.billMonth = $("#bill_month_id").val();
            telephoneBillRegister.billLineRent = $("#bill_line_rent_id").val();
            telephoneBillRegister.billVatAmount = $("#bill_vat_amount_id").val();
            telephoneBillRegister.billTotalAmount = $("#bill_total_amount_id").val();
            telephoneBillRegister.billLastPayDate = $("#bill_last_pay_date_id").val();
            telephoneBillRegister.billPaymentDate = $("#bill_payment_date_id").val();
            telephoneBillRegister.payorInfo = $("#payor_info_id").val();


            var telephoneType = $("#telephone_type_id option:selected").text();
            var officeName = $("#office_id_id option:selected").text();

            var editBtn = "<button id='edit' type='button' class='btn fa fa-edit' style='text-align:center;vertical-align: middle;font-size:20px;'></button>";
            var dltBtn = "<button id='delete' type='button' class='btn fa fa-trash-o' style='text-align:center;vertical-align: middle;font-size:20px;color:red;'></button>";

            $.ajax({
                contentType: 'application/json',
                url: "/fixed-asset/save-telephone-bill-register",
                type: 'POST',
                //async: false,
                data: JSON.stringify(telephoneBillRegister),
                dataType: 'json',
                success: function (response) {
                    if(response)
                    {
                        if (telephoneBillRegister.id != "")
                        {
                            var rowIndex = table.row( curRowEditMode ).index();
                            var updatedRow = table.row( rowIndex ).data( [response,
                                                                        telephoneBillRegister.officeId,
                                                                        telephoneBillRegister.officeCatId,
                                                                        telephoneBillRegister.telephoneType,
                                                                        officeName,
                                                                        telephoneBillRegister.billerName,
                                                                        telephoneType,
                                                                        telephoneBillRegister.telephoneNumber,
                                                                        telephoneBillRegister.billNo,
                                                                        telephoneBillRegister.billMonth,
                                                                        telephoneBillRegister.billLineRent,
                                                                        telephoneBillRegister.billVatAmount,
                                                                        telephoneBillRegister.billTotalAmount,
                                                                        telephoneBillRegister.billLastPayDate,
                                                                        telephoneBillRegister.billPaymentDate,
                                                                        telephoneBillRegister.payorInfo,
                                                                        editBtn,
                                                                        dltBtn] ).invalidate();

                            updatedRow.nodes().to$().attr("style", "background-color: rgb(255, 245, 238);");
                            updatedRow.nodes().to$().attr("id", "updateScroll");
                            document.getElementById("updateScroll").scrollIntoView();
                            updatedRow.nodes().to$().removeAttr("id");

                            setTimeout(function(){
                                updatedRow.nodes().to$().removeAttr("style");
                            }, 3000);

                            showAlert("Updated successfully !");
                        }
                        else
                        {
                            var newRow = table.row.add([response,
                                                        telephoneBillRegister.officeId,
                                                        telephoneBillRegister.officeCatId,
                                                        telephoneBillRegister.telephoneType,
                                                        officeName,
                                                        telephoneBillRegister.billerName,
                                                        telephoneType,
                                                        telephoneBillRegister.telephoneNumber,
                                                        telephoneBillRegister.billNo,
                                                        telephoneBillRegister.billMonth,
                                                        telephoneBillRegister.billLineRent,
                                                        telephoneBillRegister.billVatAmount,
                                                        telephoneBillRegister.billTotalAmount,
                                                        telephoneBillRegister.billLastPayDate,
                                                        telephoneBillRegister.billPaymentDate,
                                                        telephoneBillRegister.payorInfo,
                                                        editBtn,
                                                        dltBtn]).draw();
                            var colCount = table.columns()[0].length;

                            /*newRow.nodes().to$().find("td:eq(0)").attr("style", "text-align:center;display: none");
                            newRow.nodes().to$().find("td:eq(1)").attr("style", "text-align:center;display: none");
                            newRow.nodes().to$().find("td:eq(2)").attr("style", "text-align:center;display: none");
                            newRow.nodes().to$().find("td:eq(3)").attr("style", "text-align:center;display: none");*/

                            newRow.nodes().to$().attr("style", "background-color: rgb(255, 245, 238);");
                            newRow.nodes().to$().attr("id", "newScroll");
                            var i;
                            for(i = 0; i < 4; i++){
                                newRow.nodes().to$().find("td:eq(" + i + ")").attr("style", "text-align:center;vertical-align: middle; display: none");
                            }
                            for(i = 4; i < colCount; i++){
                                newRow.nodes().to$().find("td:eq(" + i + ")").attr("style", "text-align:center;vertical-align: middle;");
                            }
                            newRow.show().draw(false);
                            document.getElementById("newScroll").scrollIntoView();
                            newRow.nodes().to$().find("tr").removeAttr("id");

                            setTimeout(function(){
                                newRow.nodes().to$().find("tr").removeAttr("style");
                            }, 3000);

                            showAlert("Saved successfully !");

                        }
                    }
                },
                error: function (xhr, status, error) {
                    showAlert("Something went wrong!");
                }
            });
        }

    });


    var curRowEditMode;
    var editModeEnabled = 0;

    $('#telephone_bill_register_table').on('click', '#edit', function () {

        $("#telephone_bill_register_save").text("Update");

        editModeEnabled = 1;

        curRowEditMode = $(this).closest('tr');
        var col1 = curRowEditMode.find('td:eq(0)').text(); //id
        var col2 = $.trim(curRowEditMode.find('td:eq(1)').text());  //office ID
        var col3 = $.trim(curRowEditMode.find('td:eq(2)').text());  //office category
        var col4 = $.trim(curRowEditMode.find('td:eq(3)').text());  //telephone type id
        var col5 = $.trim(curRowEditMode.find('td:eq(4)').text()); //office name
        var col6 = $.trim(curRowEditMode.find('td:eq(5)').text());  //biller name
        var col7 = $.trim(curRowEditMode.find('td:eq(6)').text());  //telephone type
        var col8 = $.trim(curRowEditMode.find('td:eq(7)').text());  //telephone number
        var col9 = $.trim(curRowEditMode.find('td:eq(8)').text());  //bill no
        var col10 = $.trim(curRowEditMode.find('td:eq(9)').text());  //bill month
        var col11 = $.trim(curRowEditMode.find('td:eq(10)').text());  //bill line rent
        var col12 = $.trim(curRowEditMode.find('td:eq(11)').text());  //bill vat amount
        var col13 = $.trim(curRowEditMode.find('td:eq(12)').text());  //bill total amount
        var col14 = $.trim(curRowEditMode.find('td:eq(13)').text());  //bill last pay date
        var col15 = $.trim(curRowEditMode.find('td:eq(14)').text());  //bill payment date
        var col16 = $.trim(curRowEditMode.find('td:eq(15)').text());  //payor info


        $('#id').val(col1);


        $('#office_category').val(col3);
        $('#office_category').trigger('change');

        $('#biller_name_id').val(col6);


        $('#telephone_type_id').val(col4);

        $('#telephone_number_id').val(col8);
        $('#bill_no_id').val(col9);
        $('#bill_month_id').val(col10);

        $('#bill_line_rent_id').val(col11);
        $('#bill_vat_amount_id').val(col12);
        $('#bill_total_amount_id').val(col13);
        $('#bill_last_pay_date_id').val(col14);
        $('#bill_payment_date_id').val(col15);
        $('#payor_info_id').val(col16);

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });





    $('#telephone_bill_register_table tbody').on('click', '#delete', function () {
        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url:  "/fixed-asset/delete-bill/" + col1,
                        type: 'POST',
                        dataType: 'json',
                        success: function(response) {

                            if(response === true){
                                table.row( curRow ).remove().draw( false );
                                showAlert("Deleted Successfully");
                            }
                            else {
                                showAlert("Unknown error");
                            }

                        },
                        error: function(xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

});