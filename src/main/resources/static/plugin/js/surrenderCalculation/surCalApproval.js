/*author:moahsin
* 11-Feb-2020*/


function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};
function getDateByAddingOne(recvDate) {
    var date = new Date(recvDate);
    var newdate = new Date(date);

    newdate.setDate(newdate.getDate() + 1);

    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    var someFormattedDate = mm + '/' + dd + '/' + y;
    return someFormattedDate;
}

function getSurrCalApproval(){

    var applSlNo = $('#applSlNo').val();
    var netSurrAmount = $('#netSurrAmount').val();
    var res = getSurrAppRejectCount(netSurrAmount)

    if(applSlNo){
        applSlNo = applSlNo.split('/').join(' ');
    }

    if(res > 0){

        $.confirm({
            title: 'Confirm',
            content: 'Are You Sure To Approve This Record',
            buttons: {
                ok: function () {

                    var json = {
                        "applSlNo": applSlNo,
                    };

                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json',
                        url: "/surrender/approveSurCal/"+applSlNo,
                        data: JSON
                            .stringify(json),
                        dataType: 'json',
                        success: function (data) {
                            showAlert("Successfully Approved");
                            clearform();
                            //window.location.reload();
                        },
                        error: function (e) {
                            showAlert("This Record Already Approved");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }else{
        $.confirm({
            title: 'Confirm',
            content: 'You are not authorized to Approve This Record',
            buttons: {
                ok: function () {
                },
            }
        });
    }

}
function genSurrRejectReport(pgid,applSlNo){
    //pgid='0506280502000190';
    //applSlNo = '9002/13364/20';
    var values = pgid.concat(","+applSlNo);
    //alert(values);
    if(pgid != '' && applSlNo != ''){
        window.open('/surrender/genSurrRejectReport?strArr='+values, '_blank');
    }
}
function getSurrAppReject(){

    var applSlNo = $('#applSlNo').val();
    var remarkStr = $('#remark').val();
    var pgid = $('#pgId').val();

    //genSurrRejectReport(pgid,applSlNo);

    var remarks = "";
    if(remarkStr){
        remarks = remarkStr;
    }

    var netSurrAmount = $('#netSurrAmount').val();
    var res = getSurrAppRejectCount(netSurrAmount);

    if(applSlNo){
        applSlNo = applSlNo.split('/').join(' ');
    }
    if(res > 0){
        $.confirm({
            title: 'Confirm',
            content: 'Are You Sure To Reject This Record',
            buttons: {
                ok: function () {

                    var json = {
                        "applSlNo": applSlNo,
                        "remarks": remarks
                    };


                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json',
                        url: "/surrender/surrAppReject/"+applSlNo+"/"+remarks,
                        data: JSON
                            .stringify(json),
                        dataType: 'json',
                        success: function (data) {
                            showAlert("Successfully Rejected");
                            genSurrRejectReport(pgid,applSlNo);
                            clearform();
                            //window.location.reload();
                        },
                        error: function (e) {
                            showAlert("Already Rejected");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }else{
        $.confirm({
            title: 'Confirm',
            content: 'You are not authorized to Reject This Record',
            buttons: {
                ok: function () {
                },
            }
        });
    }
}

$(document).ready(function () {


    $("#approved_button").click(function () {

        //var flag = dataValidation();

        //if (flag == true) {


            var appSlNo = $('#appSlNo').val();
            var pgId = $('#pgId').val();

            var surrenderCal = {};

            surrenderCal.appSlNo = appSlNo;
            surrenderCal.pgId = pgId;
            //alert(surrenderCal);

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/surrender/approveSurCal",
                data: JSON.stringify(surrenderCal),
                dataType: 'json',
                success: function (data) {
                    alert(JSON.stringify(surrenderCal));
                    showAlert("Successfully added.");
                },
                error: function (e) {
                    alert("Something Wrong!!!");
                }
            });
       // }


    });


    var showAlert = function (alertContent) {
        new PNotify({
            title: 'Info',
            text: alertContent,
            type: 'info',
            styling: 'bootstrap3'
        });
    }

    function clearform() {
        $('#pgid').val("");
        $('#activeOfficeCd').val("");
        $('#collectionOffice').val("");
        $('#proposalNumber').val("");
        $('#collCategory').val("");
        $('#collectionType').val("");
        $('#collectionMedia').val("");
        $('#mediaNo').val("");
        $('#mediaDate').val("");
        $('#policyOffice').val("");
        $('#assuredName').val("");
        $('#bankName').val("");
        $('#branchName').val("");
        $('#checkNo').val("");
        $('#checkDate').val("");
        $('#receiveDate').val("");
        $('#totalAmnt').val("");
        $('#recvIdLastIndex').val("");

    }

});

function getSurrAppRejectForm(pgId){

    $.confirm({
        title: 'Confirm',
        content: 'Are you sure to proceed?',
        buttons: {
            ok: function () {
                window.location = '/surrender/surrAppRejectForm/'+pgId;
            },
            cancel: function () {

            }
        }
    });
}
function getSurrAppRejectInfo(pgId){


    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/surrender/getSurrAppRejectInfo/"
            + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var netSurrAmount = "";
            var applSlNo = "";
            $.each(data, function (i, l) {
                netSurrAmount = l[7];
                applSlNo = l[1];
            });

            $("#netSurrAmount").val(netSurrAmount);
            $("#applSlNo").val(applSlNo);

        },
        error: function (e) {

        }
    });

}

function getSurAppRejectAppSlNoDate(pgId){


    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/surrender/getAppSlNoDate/"
            + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            //alert("insp"+data);

            var value_0 = "";
            var value_1 = "";
            $.each(data, function (i, l) {
                value_0 = l[0];
                value_1 = l[1];
            });

            value_1 = getDateShow(value_1);
            $("#appSlNo").val(value_0);
            $("#applicationDate").val(value_1);

        },
        error: function (e) {

        }
    });

}
function getSurrAppRejectInfoInsPSumm(pgId){

    var json = {
        "pgId": pgId

    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/surrender/getSurrInfoInsPSumm/"
            + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var policyStatus = "";
            var prdNm = "";
            var comDate = "";
            var payMode = "";
            var summAssured = "";
            var riskDate = "";
            var nextDueInsNo = "";
            var nextDueDate = "";
            var age = "";
            var partyNm = "";
            var option = "";
            var term = "";
            var mDate = "";
            var address = "";
            var district = "";
            var thana = "";
            var policyNo = "";
            var lastPaidDate = "";
            var policyStatusId = "";
            var optionCd = "";
            var productCd = "";

            $.each(data, function (i, l) {

                policyStatus = l[0];
                prdNm = l[1];
                comDate = l[2];
                payMode = l[3];
                summAssured = l[4];
                riskDate = l[5];
                nextDueInsNo = l[6];
                nextDueDate = l[7];
                age = l[8];
                partyNm = l[9];
                option = l[10];
                term = l[11];
                mDate = l[12];
                address = l[13];
                district = l[14];
                thana = l[15];
                policyNo = l[16];
                lastPaidDate = l[17];
                policyStatusId = l[18];
                optionCd = l[19];
                productCd = l[20];

            });

            if(comDate!=null){
                comDate = getDateShow(comDate);
            }
            if(riskDate!=null){
                riskDate = getDateShow(riskDate);
            }
            if(nextDueDate!=null){
                nextDueDate = getDateShow(nextDueDate);
            }
            if(lastPaidDate!=null){
                lastPaidDate = getDateShow(lastPaidDate);
            }
            if(thana == null){
                thana = "";
            }
            if(mDate!=null){
                mDate = getDateShow(mDate);
            }
            var mAge = "";
            if(age!=null && term!=null){
                mAge = Number(age)+Number(term);
            }
            if(district == null){
                district = "";
            }
            $("#dateOfComm").val(comDate);
            $("#riskDate").val(riskDate);
            $("#sumAssured").val(summAssured);
            $("#applicantName").val(partyNm);
            $("#option").val(option);
            $("#productName").val(prdNm);
            $("#policyStatus").val(policyStatus);
            $("#address").val(address);
            $("#district").val(district);
            $("#thana").val(thana);
            $("#paymode").val(payMode);
            $("#policyTerm").val(term);
            $("#lastPaidDate").val(lastPaidDate);
            $("#maturityDate").val(mDate);
            $("#nextDueDate").val(nextDueDate);
            $("#nextDueInstNo").val(nextDueInsNo);
            $("#maturityAge").val(mAge);
            $("#policyHolderAge").val(age);
            $("#policyNoSurrCal").val(policyNo);
            $("#policyStatusId").val(policyStatusId);
            $("#optionCd").val(optionCd);
            $("#productCd").val(productCd);

        },
        error: function (e) {

        }
    });

}


window.onload = function() {

    var pgId = $("#pgId").val();
    if(pgId!=null){
        getSurAppRejectAppSlNoDate(pgId);
        getSurrAppRejectInfoInsPSumm(pgId);
        getSurrAppRejectInfo(pgId);
    }

};

function getSurrAppRejectCount(netSurrAmount){

    var keyValue = "";
    var json = {
        "netSurrAmount": netSurrAmount
    };

    $.ajax({
        async: false,
        type: 'POST',
        contentType: 'application/json',
        url: "/surrender/getSurrAppRejectCount/"+netSurrAmount,
        data: JSON.stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            keyValue = data;
        },
        error: function (e) {
        }
    });
    return keyValue;
}

function clearform() {
    $('#pgId').val("");
    $('#applSlNo').val("");
    $('#policyNoSurrCal').val("");
    $('#dateOfComm').val("");
    $('#applicantName').val("");
    $('#option').val("");
    $('#productName').val("");
    $('#policyStatus').val("");
    $('#district').val("");
    $('#thana').val("");
    $('#paymode').val("");
    $('#sumAssured').val("");
    $('#lastPaidDate').val("");
    $('#policyTerm').val("");
    $('#riskDate').val("");
    $('#maturityDate').val("");
    $('#nextDueDate').val("");
    $('#nextDueInstNo').val("");
    $('#address').val("");
    $('#remark').val("");
    $('#netSurrAmount').val("");

}

