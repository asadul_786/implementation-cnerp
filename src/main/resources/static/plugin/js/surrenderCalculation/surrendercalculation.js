/*author:moahsin
* 10-Feb-2020*/

function getSurrenderCal(pgId){

    $.confirm({
        title: 'Confirm',
        content: 'Are you sure to proceed?',
        buttons: {
            ok: function () {
                window.location = '/surrender/getSurrCalForm/'+pgId;
            },
            cancel: function () {

            }
        }
    });
}

function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};
function getDateByAddingOne(recvDate) {
    var date = new Date(recvDate);
    var newdate = new Date(date);

    newdate.setDate(newdate.getDate() + 1);

    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    var someFormattedDate = mm + '/' + dd + '/' + y;
    return someFormattedDate;
}
function officeAutoComplete() {

    var collOffice = "#" + "collOffice";
    var url = "/surrender/officeAutoComplete";
    var collOfficeId = "#" + "collOfficeId";
    autocomplete(collOffice, url, "", collOfficeId);
}
function deleteSurrenderCal(pgId){

    var applSlNo = $('#appSlNoVal').val();
    //alert(applSlNo);
    if(applSlNo){
        applSlNo = applSlNo.split('/').join(' ');
        //alert(applSlNo);
    }

    $.confirm({
        title: 'Confirm',
        content: 'Selected record will be deleted.',
        buttons: {
            ok: function () {

                var json = {
                    "pgId": pgId,
                    "applSlNo": applSlNo
                };
                /*alert(JSON
                    .stringify(json));*/

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/surrender/deleteSurrenderCal/" + pgId+"/"+applSlNo,
                    data: JSON
                        .stringify(json),
                    dataType: 'json',
                    success: function (data) {
                        //alert(data);
                        /* if (data === true) {
                         showAlert("Deleted Successfully");
                         } else {
                         showAlert("Unknown error");
                         }*/
                        showAlert("Deleted Successfully");
                        window.location.reload();
                    },
                    error: function (e) {
                        showAlert("child record found");
                    }
                });
            },
            cancel: function () {

            }
        }
    });
}
function getDate(date){
    var d = new Date(date);
    return d;
}
$(document).ready(function () {

    officeAutoComplete();

    $("#create_button").click(function () {

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to proceed?',
            buttons: {
                ok: function () {
                     var appSlNo = $('#appSlNo').val();
                     var pgId = $('#pgId').val();
                     var bonusAmount = $('#bonusAmount').val();
                     var surrenderValue = $('#surrenderValue').val();
                     var netSurrenderAmount = $('#netSurrenderAmount').val();
                     var stagePaymentAmount = $('#stagePaymentAmount').val();
                     var paidUpValue = $('#paidUpValue').val();
                     var nextDueInstNo = $('#nextDueInstNo').val();
                     var policyStatus = $('#policyStatusId').val();
                     var optionCd = $('#optionCd').val();
                     var premiumPaidYear = $('#premiumPaidYear').val();
                     //var status = $('#status').val();
                     var proposedAmount = $('#proposedAmount').val();
                     var payableBonusAmount = $('#payableBonusAmount').val();
                     var productCd = $('#productCd').val();

                     var sumAssured = $('#sumAssured').val();
                     var maturityAge = $('#maturityAge').val();
                     var riskDate = $('#riskDate').val();
                     if(riskDate){
                         riskDate = riskDate.split("/").reverse().join("/");
                         riskDate = getDate(riskDate);
                     }
                     var policyTerm = $('#policyTerm').val();
                     var lastPaidDate = $('#lastPaidDate').val();
                    lastPaidDate = lastPaidDate.toString();
                     if(lastPaidDate){
                         lastPaidDate = lastPaidDate.split("/").reverse().join("/");
                         lastPaidDate = getDate(lastPaidDate);
                     }
                     var policyHolderAge = $('#policyHolderAge').val();
                     var nextDueDate = $('#nextDueDate').val();
                     if(nextDueDate){
                         nextDueDate = nextDueDate.split("/").reverse().join("/");
                         nextDueDate = getDate(nextDueDate);
                     }


                    var surrenderCal = {};

                    surrenderCal.appSlNo = appSlNo;
                    surrenderCal.pgId = pgId;
                    surrenderCal.bonusAmount = bonusAmount;
                    surrenderCal.surrValue = surrenderValue;
                    surrenderCal.netSurrAmnt = netSurrenderAmount;
                    surrenderCal.stagePaymentAmount = stagePaymentAmount;
                    surrenderCal.paidUpValue = paidUpValue;
                    surrenderCal.nextDueInstNo = nextDueInstNo;
                    surrenderCal.policyStatus = policyStatus;
                    surrenderCal.optionCd = optionCd;
                    surrenderCal.premPaidYear = premiumPaidYear;
                    //surrenderCal.appSt = status;
                    surrenderCal.approvedAmount = proposedAmount;
                    surrenderCal.payBonusAmount = payableBonusAmount;
                    surrenderCal.prdCd = productCd;
                    surrenderCal.summAss = sumAssured;
                    surrenderCal.maturityAge = maturityAge;
                    surrenderCal.riskDate = riskDate;
                    surrenderCal.policyTerm = policyTerm;
                    surrenderCal.lastPaidDate = lastPaidDate;
                    surrenderCal.policyHAge = policyHolderAge;
                    surrenderCal.nextDueDate = nextDueDate;

                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json',
                        url: "/surrender/calculateSurrVal",
                        data: JSON.stringify(surrenderCal),
                        dataType: 'json',
                        success: function (data) {
                            //alert(JSON.stringify(surrenderCal));
                            showAlert("Successfully saved.");
                            clearform();
                        },
                        error: function (e) {
                            alert("Something Wrong!!!");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

    $("#calSurrenderAmount").click(function () {

        var flag = dataValidation();

        if (flag == true) {

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to proceed?',
            buttons: {
                ok: function () {
                    var policyNo = $('#policyNoSurrCal').val();
                    var appSlNo = $('#appSlNo').val();
                    var appDate = $('#applicationDate').val();


                    if(appDate){
                        appDate = appDate.split('/').join(' ');
                        //alert(appDate);
                    }
                    if(appSlNo){
                        appSlNo = appSlNo.split('/').join(' ');
                        //alert(appSlNo);
                    }

                    var json = {
                        "policyNo": policyNo,
                        "appSlNo": appSlNo,
                        "appDate": appDate

                    };


                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json',
                        url: "/surrender/exeSurrenderCalPro/"
                        +policyNo+"/"+appSlNo+"/"+appDate,
                        data: JSON
                            .stringify(json),
                        dataType: 'json',
                        success: function (data) {

                            $("#paidUpValue").val('');
                            $("#payableBonusAmount").val('');
                            $("#surrenderValue").val('');
                            $("#netSurrenderAmount").val('');
                            $("#stagePaymentAmount").val('');
                            $("#netPayable").val('');
                            $("#bonusAmount").val('');
                            $("#proposedAmount").val('');

                            $.each(data, function(key, value) {

                                var loanTypeName = "";
                                var loanAmount = "";
                                var interestAmount = "";
                                if(key == 4){
                                    $("#paidUpValue").val(value);
                                }
                                if(key == 5){
                                    $("#payableBonusAmount").val(value);
                                }
                                if(key == 6){
                                    $("#surrenderValue").val(value);
                                }
                                if(key == 7){
                                    $("#netSurrenderAmount").val(value);
                                }
                                if(key == 8){
                                    $("#stagePaymentAmount").val(value);
                                }
                                if(key == 9){
                                    $("#netPayable").val(value);
                                    $("#proposedAmount").val(value);
                                }
                                if(key == 2){
                                    loanTypeName = value;
                                }
                                if(key == 1){
                                    interestAmount = value;
                                }
                                if(key == 3){
                                    loanAmount = value;
                                }
                                if(key == 10){
                                    $("#bonusAmount").val(value);
                                }

                                $("#tableBody")
                                    .append(
                                        '<tr>'
                                        + '<td><input type="text" name="loanTypeName" id="loanTypeName" style="width: 132px" readonly value="'+loanTypeName+'">'
                                        + '</td> '
                                        + '<td><input type="text" name="loanAmount" id="loanAmount" style="width: 132px" readonly value="'+loanAmount+'">'
                                        + '</td> '
                                        + '<td><input type="text" name="interestAmount" id="interestAmount" style="width: 132px" readonly value="'+interestAmount+'">'
                                        + '</td> '
                                        + '</tr>');

                            });
                            if(data[12] == "success"){
                                $.confirm({
                                    title: 'Confirmation',
                                    content: data[11],
                                    buttons: {
                                        ok: function () {
                                        }
                                    }
                                });
                            }
                            if(data[12] != "success"){
                                $.confirm({
                                    title: 'Confirmation',
                                    content: data[12],
                                    buttons: {
                                        ok: function () {
                                        }
                                    }
                                });

                            }
                        },
                        error: function (e) {
                            alert("Something Wrong!!!");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }
});


    function dataValidation() {

        var status = true;

        if ($("#policyNo").val() == "" || $("#policyNo").val() == "0" ) {
            status = false;
            $("#policyNoSpan").text("Empty field found!!");
            $("#policyNo").focus();
        } else $("#policyNoSpan").text("");

        if ($("#appSlNo").val() == "") {
            status = false;
            $("#appSlNoSpan").text("Empty field found!!");
            $("#appSlNo").focus();
        } else $("#appSlNoSpan").text("");


        if ($("#applicationDate").val() == "") {
            status = false;
            $("#applicationDateSpan").text("Empty field found!!");
            $("#applicationDate").focus();
        }
        else $("#applicationDateSpan").text("");

        return status;
    }

    function clearform(){

        $("#riskDate").val("");
        $("#lastPaidDate").val("");
        $("#nextDueDate").val("");
        $("#maturityDate").val("");
        $("#applicationDate").val("");
        $("#paymode").val('');
        $("#sumAssured").val('');
        $("#policyStatus").val("");
        $("#dateOfComm").val("");
        $("#appSlNo").val('');
        $("#applicantName").val('');
        $("#productName").val('');
        $("#address").val('');
        $("#district").val('');
        $("#thana").val('');
        $("#surrenderValue").val('');
        $("#netSurrenderAmount").val('');
        $("#option").val('');
        $("#policyNoSurrCal").val('');
        $("#netPayable").val('');
        $("#maturityAge").val('');
        $("#nextDueInstNo").val('');
        $("#policyHolderAge").val('');
        $("#policyTerm").val('');
        $("#paidUpValue").val('');
        $("#stagePaymentAmount").val('');
        $("#payableBonusAmount").val('');
        $("#payableBonusAmount").val('');

    }
    var showAlert = function (alertContent) {
        new PNotify({
            title: 'Info',
            text: alertContent,
            type: 'info',
            styling: 'bootstrap3',
            setTimeout: 300
        });

    }

});


function autocomplete(focusId, url, param, setId) {
    $.ajax({
        url : url,
        type : "POST",
        data : {"param" : ""},
        success : function(data) {
            var datajson = data;
            $(focusId).autocomplete({
                source : datajson,
                focus : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                change : function(event, ui) {
                    var levelName = $(focusId).val();
                    if (ui.item !== undefined && ui.item !== null) {
                        if (levelName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val(levelName);
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val(levelName);
                        $(setId).val('');
                    }
                },
                keypress : function(event, ui) {
                    if (ui.item !== undefined && ui.item !== null) {
                        var customerName = $(focusId).val();
                        if (customerName == ui.item.label) {
                            return true;
                        } else {
                            $(focusId).val('');
                            $(setId).val('');
                        }
                    } else {
                        $(focusId).val('');
                        $(setId).val('');
                    }
                },
                select : function(event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(setId).val(ui.item.value);
                }
            });

        }
    });
}
function getAppSlNoDate(pgId){


     var json = {
          "pgId": pgId

      };

      $.ajax({
          type: "POST",
          contentType: "application/json",
          url: "/surrender/getAppSlNoDate/"
              + pgId,
          data: JSON
              .stringify(json),
          dataType: 'json',
          cache: false,
          timeout: 600000,
          success: function (data) {
              //alert("insp"+data);

              var value_0 = "";
              var value_1 = "";
              $.each(data, function (i, l) {
                  value_0 = l[0];
                  value_1 = l[1];
              });

              value_1 = getDateShow(value_1);
              $("#appSlNo").val(value_0);
              $("#applicationDate").val(value_1);

          },
          error: function (e) {

          }
      });

}
function getSurrInfoInsPSumm(pgId){

    var json = {
        "pgId": pgId

    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/surrender/getSurrInfoInsPSumm/"
        + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var policyStatus = "";
            var prdNm = "";
            var comDate = "";
            var payMode = "";
            var summAssured = "";
            var riskDate = "";
            var nextDueInsNo = "";
            var nextDueDate = "";
            var age = "";
            var partyNm = "";
            var option = "";
            var term = "";
            var mDate = "";
            var address = "";
            var district = "";
            var thana = "";
            var policyNo = "";
            var lastPaidDate = "";
            var policyStatusId = "";
            var optionCd = "";
            var productCd = "";

            $.each(data, function (i, l) {

                policyStatus = l[0];
                prdNm = l[1];
                comDate = l[2];
                payMode = l[3];
                summAssured = l[4];
                riskDate = l[5];
                nextDueInsNo = l[6];
                nextDueDate = l[7];
                age = l[8];
                partyNm = l[9];
                option = l[10];
                term = l[11];
                mDate = l[12];
                address = l[13];
                district = l[14];
                thana = l[15];
                policyNo = l[16];
                lastPaidDate = l[17];
                policyStatusId = l[18];
                optionCd = l[19];
                productCd = l[20];

            });

            if(comDate!=null){
                comDate = getDateShow(comDate);
            }
            if(riskDate!=null){
                riskDate = getDateShow(riskDate);
            }
            if(nextDueDate!=null){
                nextDueDate = getDateShow(nextDueDate);
            }
            if(lastPaidDate!=null){
                lastPaidDate = getDateShow(lastPaidDate);
            }
            if(thana == null){
                thana = "";
            }
            if(mDate!=null){
                mDate = getDateShow(mDate);
            }
            var mAge = "";
            if(age!=null && term!=null){
                mAge = Number(age)+Number(term);
            }
            if(district == null){
                district = "";
            }
            $("#dateOfComm").val(comDate);
            $("#riskDate").val(riskDate);
            $("#sumAssured").val(summAssured);
            $("#applicantName").val(partyNm);
            $("#option").val(option);
            $("#productName").val(prdNm);
            $("#policyStatus").val(policyStatus);
            $("#address").val(address);
            $("#district").val(district);
            $("#thana").val(thana);
            $("#paymode").val(payMode);
            $("#policyTerm").val(term);
            $("#lastPaidDate").val(lastPaidDate);
            $("#maturityDate").val(mDate);
            $("#nextDueDate").val(nextDueDate);
            $("#nextDueInstNo").val(nextDueInsNo);
            $("#maturityAge").val(mAge);
            $("#policyHolderAge").val(age);
            $("#policyNoSurrCal").val(policyNo);
            $("#policyStatusId").val(policyStatusId);
            $("#optionCd").val(optionCd);
            $("#productCd").val(productCd);

        },
        error: function (e) {

        }
    });

}

window.onload = function() {

    var pgId = $("#pgId").val();
    if(pgId!=null){
        getAppSlNoDate(pgId);
        getSurrInfoInsPSumm(pgId);
    }

};
