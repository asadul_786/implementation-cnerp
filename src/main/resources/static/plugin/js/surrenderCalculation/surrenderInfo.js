/*author:moahsin
* 15-June-2020*/

function getAppSlNoDate(pgId){

    var json = {
        "pgId": pgId

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/surrender/getAppSlNoDate/"
            + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var value_0 = "";
            var value_1 = "";
            $.each(data, function (i, l) {
                value_0 = l[0];
                value_1 = l[1];
            });
            if(value_1){
                value_1 = getDateShow(value_1);
            }
            $("#appSlNo").val(value_0);
            $("#applicationDate").val(value_1);

        },
        error: function (e) {

        }
    });

}

function getSurrInfoInsPSumm(pgId){


    var json = {
        "pgId": pgId

    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/surrender/getSurrInfoInsPSumm/"
            + pgId,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var policyStatus = "";
            var prdNm = "";
            var comDate = "";
            var payMode = "";
            var summAssured = "";
            var riskDate = "";
            var nextDueInsNo = "";
            var nextDueDate = "";
            var age = "";
            var partyNm = "";
            var option = "";
            var term = "";
            var mDate = "";
            var address = "";
            var district = "";
            var thana = "";
            var policyNo = "";
            var lastPaidDate = "";
            var policyStatusId = "";
            var optionCd = "";
            var productCd = "";

            $.each(data, function (i, l) {

                policyStatus = l[0];
                prdNm = l[1];
                comDate = l[2];
                payMode = l[3];
                summAssured = l[4];
                riskDate = l[5];
                nextDueInsNo = l[6];
                nextDueDate = l[7];
                age = l[8];
                partyNm = l[9];
                option = l[10];
                term = l[11];
                mDate = l[12];
                address = l[13];
                district = l[14];
                thana = l[15];
                policyNo = l[16];
                lastPaidDate = l[17];
                policyStatusId = l[18];
                optionCd = l[19];
                productCd = l[20];

            });

            if(comDate!=null){
                comDate = getDateShow(comDate);
            }
            if(riskDate!=null){
                riskDate = getDateShow(riskDate);
            }
            if(nextDueDate!=null){
                nextDueDate = getDateShow(nextDueDate);
            }
            if(lastPaidDate!=null){
                lastPaidDate = getDateShow(lastPaidDate);
            }
            if(thana==null){
                thana = "Not Found";
            }
            if(mDate!=null){
                mDate = getDateShow(mDate);
            }
            var mAge = "";
            if(age!=null && term!=null){
                mAge = Number(age)+Number(term);
            }
            $("#dateOfComm").val(comDate);
            $("#riskDate").val(riskDate);
            $("#sumAssured").val(summAssured);
            $("#applicantName").val(partyNm);
            $("#option").val(option);
            $("#productName").val(prdNm);
            $("#policyStatus").val(policyStatus);
            $("#address").val(address);
            $("#district").val(district);
            $("#thana").val(thana);
            $("#paymode").val(payMode);
            $("#policyTerm").val(term);
            $("#lastPaidDate").val(lastPaidDate);
            $("#maturityDate").val(mDate);
            $("#nextDueDate").val(nextDueDate);
            $("#nextDueInstNo").val(nextDueInsNo);
            $("#maturityAge").val(mAge);
            $("#policyHolderAge").val(age);
            $("#policyNoSurrCal").val(policyNo);
            $("#policyStatusId").val(policyStatusId);
            $("#optionCd").val(optionCd);
            $("#productCd").val(productCd);

        },
        error: function (e) {

        }
    });

}

$(function(){
    $("#policyNoInfo").on('change', function(){

        var policyNo = $('#policyNoInfo').val();


        var json = {
            "policyNo": policyNo

        };

        if(policyNo){
            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/surrender/getPgIdSurrInfo/"
                    + policyNo,
                data: JSON
                    .stringify(json),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (data) {
                    getAppSlNoDate(data);
                    getSurrInfoInsPSumm(data);
                },
                error: function (e) {
                }
            });
        }
    })
});

$(document).ready(function () {

    //getSurrOffices();

$("#calSurrenderAmountInfo").click(function () {

    var flag = dataValidation();

    if (flag == true) {

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to proceed?',
            buttons: {
                ok: function () {
                    var policyNo = $('#policyNoSurrCal').val();
                    var appSlNo = $('#appSlNo').val();
                    var appDate = $('#applicationDate').val();
                    //var office = $('#office').val();
                    //alert(office);


                    if(appDate){
                        appDate = appDate.split('/').join(' ');
                        //alert(appDate);
                    }
                    if(appSlNo){
                        appSlNo = appSlNo.split('/').join(' ');
                        //alert(appSlNo);
                    }

                    var json = {
                        "policyNo": policyNo,
                        "appSlNo": appSlNo,
                        "appDate": appDate
                        //"office":  office

                    };


                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json',
                        url: "/surrender/exeSurrenderCalProInfo/"
                            +policyNo+"/"+appSlNo+"/"+appDate,
                        data: JSON
                            .stringify(json),
                        dataType: 'json',
                        success: function (data) {

                            $("#paidUpValue").val('');
                            $("#payableBonusAmount").val('');
                            $("#surrenderValue").val('');
                            $("#netSurrenderAmount").val('');
                            $("#stagePaymentAmount").val('');
                            $("#netPayable").val('');
                            $("#bonusAmount").val('');
                            $("#proposedAmount").val('');

                            $.each(data, function(key, value) {

                                var loanTypeName = "";
                                var loanAmount = "";
                                var interestAmount = "";
                                if(key == 4){
                                    $("#paidUpValue").val(value);
                                }
                                if(key == 5){
                                    $("#payableBonusAmount").val(value);
                                }
                                if(key == 6){
                                    $("#surrenderValue").val(value);
                                }
                                if(key == 7){
                                    $("#netSurrenderAmount").val(value);
                                }
                                if(key == 8){
                                    $("#stagePaymentAmount").val(value);
                                }
                                if(key == 9){
                                    $("#netPayable").val(value);
                                    $("#proposedAmount").val(value);
                                }
                                if(key == 2){
                                    loanTypeName = value;
                                }
                                if(key == 1){
                                    interestAmount = value;
                                }
                                if(key == 3){
                                    loanAmount = value;
                                }
                                if(key == 10){
                                    $("#bonusAmount").val(value);
                                }

                            });
                            if(data[12] == "success"){
                                $.confirm({
                                    title: 'Confirmation',
                                    content: data[11],
                                    buttons: {
                                        ok: function () {
                                        }
                                    }
                                });
                            }else{
                                $.confirm({
                                    title: 'Confirmation',
                                    content: data[12],
                                    buttons: {
                                        ok: function () {
                                        }
                                    }
                                });
                            }
                        },
                        error: function (e) {
                            $.confirm({
                                title: 'Confirmation',
                                content: data[12],
                                buttons: {
                                    ok: function () {
                                    }
                                }
                            });
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }
});

    function dataValidation() {

        var status = true;

        if ($("#policyNo").val() == "" || $("#policyNo").val() == "0" ) {
            status = false;
            $("#policyNoSpan").text("Empty field found!!");
            $("#policyNo").focus();
        } else $("#policyNoSpan").text("");

        if ($("#appSlNo").val() == "") {
            status = false;
            $("#appSlNoSpan").text("Empty field found!!");
            $("#appSlNo").focus();
        } else $("#appSlNoSpan").text("");


        if ($("#applicationDate").val() == "") {
            status = false;
            $("#applicationDateSpan").text("Empty field found!!");
            $("#applicationDate").focus();
        }
        else $("#applicationDateSpan").text("");

        return status;
    }

});

/*function getSurrOffices(){

    var $select = $('#office');

    var json = {
        "param": ""
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/surrender/getSurrOffices",
        data: JSON.stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var office = "";
            var officeId = "";

            $('.selectpicker').selectpicker({
                style: ''
            });
            $select.html('');
            $select.append('<option value="' + '' + '">' + 'Select Office' + '</option>');
            $.each(data, function (i, l) {
                office = l[0];
                officeId = l[1];
                $select.append('<option value="' + officeId + '">' + office + '</option>');
            });
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (e) {
        }
    });
}*/

function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};