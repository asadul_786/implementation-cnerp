/**
 * Created by Geetanjali Oishe on 4/8/2019.
 */
$(document).ready(function () {

    function getInfoForClientMedicalQuestion() {
        var masterId = $("#proposalMasterNo").val();
        $.ajax({
            contentType: 'application/json',
            url: "get-info-for-medical-ques",
            type: 'POST',
            async: false,
            data: JSON.stringify(masterId),
            dataType: 'json',
            success: function (response) {
                $('#clientMedFullName').val(response.fullName);
                $('#clientMedGender').val(response.gender);
                $('#clientMedFatherName').val(response.fatherOrHusbandName);
                $('#clientMedAddress').val(response.address);
                $('#clientMedDOB').val(response.age);
                $('#clientMedOccupation').val(response.occupation);
                $('#clientMedMaritalStatus').val(response.maritalStatus);

                $('#clientMedAge').val(getAge($('#clientMedDOB ').val()));

                if ($('#clientMedGender').val() == "Female") {
                    $('#medQuesForFemale').show();
                }
                else {
                    $('#medQuesForFemale').hide();
                }

            },
            error: function (xhr, status, error) {
            }
        });

    }

    function populateClientMedQuestions(strSet) {
        // var htmlButton = "<button type='button' class='btn btn-success' id='client-medical-ans'>Save Ans</button>";
        $("#clientMedQuesSection2 tr").remove();
        $("#clientMedQuesSection3 tr").remove();
        $("#clientMedQuesSection4 tr").remove();
        $("#clientMedQuesSection5 tr").remove();
        $("#clientMedQuesSection6 tr").remove();
        $("#clientMedQuesSection7 tr").remove();
        var masterId = $('#proposalMasterNo').val();

        $.ajax({
            contentType: 'application/json',
            url:  "/generic/question/all/"+ "client_medical?proposalMasterId=" + masterId,
            type: 'GET',
            async: false,
            dataType: 'json',
            success: function(res) {

                var c2 = 0, c3 = 0, c4 = 0, c5 = 0, c6 = 0, c7 = 0;
                $.each(res, function( key, value ) {

                    if (value.sectionNo == 2) {
                        // var html2 = '<tr>';
                        // html2 += '<td>';
                        // html2 += c2 + 1;
                        // html2 += '</td><td>';
                        // html2 += value.questionText;
                        // html2 += '</td><td>';
                        //
                        // html2 += getElemStr(value);
                        //
                        // html2 += '</td>';
                        // html2 += '</tr>';

                        $("#clientMedQuesSection2").append(prepareAnswer(value, c2));
                        c2++;
                    }

                    if (value.sectionNo == 3) {
                        $("#clientMedQuesSection3").append(prepareAnswer(value, c3));
                        c3++;
                    }

                    if (value.sectionNo == 4) {
                        $("#clientMedQuesSection4").append(prepareAnswer(value, c4));
                        c4++;
                    }

                    if (value.sectionNo == 5) {
                        $("#clientMedQuesSection5").append(prepareAnswer(value, c5));
                        c5++;
                    }

                    if (value.sectionNo == 6) {
                        $("#clientMedQuesSection6").append(prepareAnswer(value, c6));
                        c6++;
                    }

                    if (value.sectionNo == 7) {
                        $("#clientMedQuesSection7").append(prepareAnswer(value, c7));
                        c7++;
                    }

                    if (value.selectedAnsOption != null){
                        radioSelectEval(value);
                    }

                });
            },
            error: function(xhr, status, error) {
                alert("Something went wrong!");
            }
        });

        // $("#client_medical_question_save_btn").append(htmlButton);
    }

    function getElemStr(value) {
        var textVal = (value.answerText == null) ? '' : "'" + value.answerText + "'";

        if(value.questionType == 1) {
            return prepareGenAnswer(value, textVal);
        }

        else if(value.questionType == 4) {
            return prepareRadioAnswer(value);
        }

        else if (value.questionType == 6) {
            return prepareMixedAnswer(value, textVal);
        }
    }

    function prepareAnswer(value, c) {
        var html2 = '<tr>';
        html2 += '<td>';
        html2 += c + 1;
        html2 += '</td><td>';
        html2 += value.questionText;
        html2 += '</td><td>';

        html2 += getElemStr(value);

        html2 += '</td>';
        html2 += '</tr>';
        return html2;
    }

    function prepareGenAnswer(value, textVal){
        var str = "<input class='form-control col-md-7 col-xs-12 ans-input-client-medical' questionId =" + value.id + " questionSetId =" + value.questionSetId + " " +
            " answerId=" + value.answerId +
            " type='text' " +
            " value=" + textVal + "></input>";
        return str;
    }

    function prepareRadioAnswer(value) {
         var str =  "<div class='col-md-7 col-xs-12'> " +
                "<label> <input class='ans-client-medical-radio client-med-radio-yes' type='radio' name="+ value.serialNo + " value='1' questionId =" + value.id + " questionSetId =" + value.questionSetId + " answerId=" + value.answerId + "> Yes </label> " +
                "<label> <input class='ans-client-medical-radio client-med-radio-no' type='radio' name="+ value.serialNo + " value='0' questionId =" + value.id + " questionSetId =" + value.questionSetId + " answerId=" + value.answerId + "> No </label> " +
                "</div>";
            return str;
    }

    function prepareMixedAnswer(value, textVal) {
        var str =  "<div class='col-md-7 col-xs-12'> " +
            "<label> <input class='ans-client-medical-radio-mixed client-med-radio-yes' type='radio' name="+ value.serialNo + " value='1' questionId =" + value.id + " questionSetId =" + value.questionSetId + " answerId=" + value.answerId + "> Yes </label> " +
            "<label> <input class='ans-client-medical-radio-mixed client-med-radio-no' type='radio' name="+ value.serialNo + " value='0' questionId =" + value.id + " questionSetId =" + value.questionSetId + " answerId=" + value.answerId + "> No </label> " +
            "</div>";

        str += "<input class='form-control col-md-7 col-xs-12 ans-input-client-medical-mixed' questionId =" + value.id + " questionSetId =" + value.questionSetId + " " +
            " answerId=" + value.answerId +
            " type='text' " +
            " value=" + textVal + "></input>";
        return str;
    }

    function radioSelectEval(value) {
          if (value.selectedAnsOption == 0) {
              $('.client-med-radio-no[answerId =' + value.answerId + ']').prop("checked", true);
          }
          else if (value.selectedAnsOption == 1) {
              $('.client-med-radio-yes[answerId =' + value.answerId + ']').prop("checked", true);
          }
    }

    $(document).on("click", "#profile-tab46", function (event) {
        getInfoForClientMedicalQuestion();
        populateClientMedQuestions();
    });

    var confirmClientMedAnsDialog = function (text, answerMegaDtos) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        $.ajax({
                            contentType: 'application/json',
                            url:  "/generic/answer/create",
                            type: 'POST',
                            async: false,
                            data : JSON.stringify(answerMegaDtos),
                            success: function(res) {
                                /* for (var i=0; i<res.length; i++) {
                                 console.log(res[i].answerId);
                                 }*/
                                var i=0;
                                $('input[type="text"].ans-input-client-medical').each(function () {
                                    $(this).attr("answerId", res[i++].answerId);

                                });
                                showAlert("Successfully Saved");
                            },
                            error: function(xhr, status, error) {
                                alert("Something went wrong!");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };

    $(document).on("click", "#client-medical-ans", function (e) {
        var answerMegaDtos = [];
        var masterId = $('#proposalMasterNo').val();

        $('input[type="text"].ans-input-client-medical').each(function () {

            var answerMiniDto = {};
            answerMiniDto.answerText = $(this).val();
            answerMiniDto.questionId = $(this).attr("questionId");
            answerMiniDto.questionSetId = $(this).attr("questionSetId");
            answerMiniDto.answerId = $(this).attr("answerId");
            answerMiniDto.proposalMasterId = masterId;

            answerMegaDtos.push(answerMiniDto);
            // console.log($(this).val());
        });

        $('input[type="radio"].ans-client-medical-radio:checked').each(function () {
            var answerMiniDto = {};
            answerMiniDto.selectedAnsOption = $(this).val();
            answerMiniDto.questionId = $(this).attr("questionId");
            answerMiniDto.questionSetId = $(this).attr("questionSetId");
            answerMiniDto.answerId = $(this).attr("answerId");
            answerMiniDto.proposalMasterId = masterId;

            answerMegaDtos.push(answerMiniDto);
        });

        $('input[type="text"].ans-input-client-medical-mixed').each(function () {
            var answerMiniDto = {};
            answerMiniDto.answerText = $(this).val();
            answerMiniDto.questionId = $(this).attr("questionId");
            answerMiniDto.questionSetId = $(this).attr("questionSetId");
            answerMiniDto.answerId = $(this).attr("answerId");
            answerMiniDto.proposalMasterId = masterId;


            var selectedAnsOption = $('.ans-client-medical-radio-mixed[answerId =' + $(this).attr("answerId") + ']:checked').val();
            if (selectedAnsOption != 'undefined') {
                answerMiniDto.selectedAnsOption = selectedAnsOption;
            }

            answerMegaDtos.push(answerMiniDto);
            // console.log($(this).val());
        });


        confirmClientMedAnsDialog("Are you sure to save this questionnaire?", answerMegaDtos);
    });




});