$(document).ready(function () {

    // $('#edit_show').click(function () {
    //
    //     $('.uwSection').css("display","");
    //
    // });

   $('#tbl-underwriting-lst').DataTable({
       //dom: 'Bfrtip', "scrollX": true,
       // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
       // scrollY:        "300px",
       scrollX:        true,
       scrollCollapse: true,
       paging:         true,
       columnDefs: [{ width: '30%', targets: 0 }]
        //,
       // fixedColumns: true
    });

    $("#refreshBtn").click(function () {
        //resetForm();
    });

    function resetForm() {

        $('#proposalNo').val("");
        $('#officeCode').val("-1");
    }

});


$(document).on("click", "#edit_show", function() {

    var curRow = $(this).closest('tr');
    var col0 = curRow.find('td:eq(0)').text();
    var col1 = curRow.find('td:eq(6)').text();
    var col2 = curRow.find('td:eq(10)').text();
    var col3 = curRow.find('td:eq(11)').text();
    var col4 = curRow.find('td:eq(13)').text();


    $.confirm({
        title: 'Confirm',
        content: 'Are you sure to Going for Underwriting?',
        buttons: {
            ok: function () {

                if(col4==3 || col4==20 ||col4==21){

                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/checking-underwriting/" + col1 +"/"+ col3 +"/"+ col2,
                        type: 'POST',
                        // async: false,
                        // data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            if(response==true){

                                $.ajax({
                                    contentType: 'application/json',
                                    // url: "/new-business/check-underwriting-go-link/" + col0 +"/"+ col4,
                                    url: "/new-business/check-underwriting-go-link/" + col0,
                                    type: 'POST',
                                    //async: false,
                                    //data: JSON.stringify(answerDto),
                                    dataType: 'json',
                                    success: function (response) {
                                        // alert(response);
                                        if(response==true){
                                            //showAlert("Successfully done to Underwriting");
                                            window.location.replace("/new-business/underwritingAcceptProcess?pgid="+col0,target="_blank");

                                        }else{
                                            //window.location.replace("/new-business/getNotAuthenticatePage",target="_blank");
                                            showAlertByType("Wrong !! You are not able to do for Underwriting !!","F");
                                        }
                                    },
                                    error: function (xhr, status, error) {

                                    }
                                });


                            }else{

                                showAlertByType("Wrong !! You are not able to do for Underwriting !!","F");
                                //window.location.replace("/new-business/getNotAuthenticatePage","_blank");


                            }
                        },
                        error: function (xhr, status, error) {

                            showAlertByType("Wrong !! You are not able to do for Underwriting !!","F");
                        }
                    });

                }else {

                    $.ajax({
                       contentType: 'application/json',
                       url: "/new-business/check-dublicate-uw-value/" + col0,
                       type: 'POST',
                        //async: false,
                       // data: JSON.stringify(answerDto),
                       dataType: 'json',
                       success: function (response) {

                        if(response==false){
                           //showAlert("Successfully Locked for Underwriting");
                           showAlert("Already Underwriting In Progress ,Try Another One","F");

                         }else{
                                $.ajax({
                                    contentType: 'application/json',
                                    url: "/new-business/checking-underwriting/" + col1 +"/"+ col3 +"/"+ col2,
                                    type: 'POST',
                                    // async: false,
                                    // data: JSON.stringify(answerDto),
                                    dataType: 'json',
                                    success: function (response) {

                                        if(response==true){
                                            //  alert(response);
                                            $.ajax({
                                                contentType: 'application/json',
                                                url: "/new-business/update-underwriting-process/" + col0,
                                                type: 'POST',
                                                //async: false,
                                                //data: JSON.stringify(answerDto),
                                                dataType: 'json',
                                                success: function (response) {

                                                    showAlert("You are Succeed for underwriting.. !!");
                                                    window.location.reload();

                                                },
                                                error: function (xhr, status, error) {
                                                    showAlertByType("Wrong !! You are not able to do for Underwriting ","F");
                                                }
                                            });

                                        }else{
                                           // window.location.replace("/new-business/getNotAuthenticatePage","_blank");
                                            showAlertByType("Wrong !! You are not able to do for Underwriting ","F");

                                        }
                                    },
                                    error: function (xhr, status, error) {

                                        showAlertByType("Wrong !! You are not able to do for Underwriting !!","F");
                                    }
                                });


                            }

                        },
                       error: function (xhr, status, error) {

                        }
                    });

                }

            },
            cancel: function () {

            }
        }
    });

});



function validateForm() {
   // alert(document.forms["proposalSearchPanel"]["officeCode"].value);
    var x = document.forms["proposalSearchPanel"]["officeCode"].value;
    // console.log(x);
    if (isEmptyString(x)) {
       customAlert(alertTypes.WARNING,"Required!","Servicing office is required ")
        //showAlertByType("Servicing office is required!","W");
        // $('.requiredF').attr("display","none");
        return false;
    }
}

function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}

var locale='en';
const alertTypes = {
    INFO: 'Info',
    SUCCESS: 'Success',
    WARNING: 'Waring',
    FAILED: 'Failed',
    DENIED: 'Illegal action',
    ERROR: 'Error'
}
function  customAlert(alertType,titleMsg,detMsg) {

    var msgTitle = '';


    switch (alertType){
        case alertTypes.INFO:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'তথ্য':'Info'): titleMsg;


            $.confirm({
                icon: 'ace-icon fa fa-eye',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'blue',
                buttons: locale == 'bn'?'ঠিক আছে':'OK',
                title: msgTitle,
                content: isEmptyString(detMsg)?'Tray Again latter.':detMsg,
                typeAnimated: true,
                btnClass: 'btn-blue'


            });
            break;
        case alertTypes.SUCCESS:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'? 'সফল হয়েছে':'Success'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-smile-o',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'purple',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Successfully Done.':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        case alertTypes.WARNING:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'সতর্কীকরণ!':'Warning!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-exclamation-circle',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'orange',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        case alertTypes.FAILED:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'অসফল হয়েছে!':'Failed!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-ban',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'red',

                title: msgTitle,
                content:  isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        case alertTypes.DENIED:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'অবৈধ!':'Denied!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-lock',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'red',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        case alertTypes.ERROR:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'ভুল !':'Denied!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-ban',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'red',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        default:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'কিছু ভুল হয়েছে !':'Something Wen\'t wrong!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-bullhorn',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'dark',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;

    }
}

