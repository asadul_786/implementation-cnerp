function scrollToSpecificQuestions(questionNo) {

    var fullQuestionNo = "#scroll" + questionNo;

    //scroll to questions

    $('html, body').animate({
        scrollTop: $(fullQuestionNo).offset().top
    }, 500);

}
$(function () {

    function readURLPhoto(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                if (input.files[0].type.match('image.*')) {
                    //console.log("is an image");
                    $('#doctor_sig_view').attr('src', e.target.result);
                    $('#err_doctor_sig_id').hide();
                    $('#doctor_sig_view').css("display", "inline");
                    $('#chk_file_upload').val("1");
                }
                else{
                    $("#doctor_sig_id").val('');
                    $('#doctor_sig_view').attr('src', '');
                    $('#err_doctor_sig_id').text("Please select image file !!");
                    $('#err_doctor_sig_id').show();
                    $('#chk_file_upload').val('');
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on("change", "#doctor_sig_id", function (e) {
        if (this.files[0].size > 1048576) {
            showAlert("Photo size can't be greater than 1MB!");
            $("#doctor_sig_id").val('');
        }
        else readURLPhoto(this);

    });

    $('#btnSubmit').click(function (e) {

        //hide err msg first
        $('#validate_ques_1_a').hide();
        $('#validate_ques_1_b').hide();
        $('#validate_ques_2').hide();
        $('#validate_ques_3_a').hide();
        $('#validate_ques_3_b').hide();
        $('#validate_ques_3_c').hide();
        $('#validate_ques_3_d_e_f').hide();
        $('#validate_ques_4_a').hide();
        $('#validate_ques_4_b').hide();
        $('#validate_ques_4_c').hide();
        $('#validate_ques_5_a_1').hide();
        $('#validate_ques_5_a_2').hide();
        $('#validate_ques_5_b').hide();
        $('#validate_ques_5_c_1').hide();
        $('#validate_ques_5_c_2').hide();
        $('#validate_ques_5_d').hide();
        $('#validate_ques_5_e_1').hide();
        $('#validate_ques_5_e_2').hide();
        $('#validate_ques_6_a').hide();
        $('#validate_ques_6_b').hide();
        $('#validate_ques_6_c').hide();
        $('#validate_ques_7_a').hide();
        $('#validate_ques_7_b').hide();
        $('#validate_ques_7_c').hide();
        $('#validate_ques_8_a').hide();
        $('#validate_ques_8_b').hide();
        $('#validate_ques_8_c').hide();
        $('#validate_ques_9').hide();
        $('#validate_ques_10').hide();

        //Question 1
        var q1_a = $('#input_1_a').val();
        if(q1_a == "")
        {
            $('#validate_ques_1_a').text("Fields cannot be empty !!");
            $('#validate_ques_1_a').show();

            //scroll to question 1
            scrollToSpecificQuestions(1);

            return;
        }
        else
        {
            $('#validate_ques_1_a').hide();
        }

        var q1_b = $('input[name="q1"]:checked').val();
        if(q1_b == undefined)
        {
            $('#validate_ques_1_b').text("Please select !!");
            $('#validate_ques_1_b').show();

            //scroll to question 1
            scrollToSpecificQuestions(1);

            return;
        }
        else
        {
            $('#validate_ques_1_b').hide();
        }

        //Question 2
        var q2 = $('#input_2').val();
        if(q2 == "")
        {
            $('#validate_ques_2').text("Fields cannot be empty !!");
            $('#validate_ques_2').show();

            //scroll to question 2
            scrollToSpecificQuestions(2);

            return;
        }
        else
        {
            $('#validate_ques_2').hide();
        }

        //Question 3
        var q3_a = $('input[name="q3_a"]:checked').val();
        if(q3_a == undefined)
        {
            $('#validate_ques_3_a').text("Please select !!");
            $('#validate_ques_3_a').show();

            //scroll to question 3
            scrollToSpecificQuestions(3);

            return;
        }
        else
        {
            $('#validate_ques_3_a').hide();
        }

        var q3_b = $('input[name="q3_b"]:checked').val();
        if(q3_b == undefined)
        {
            $('#validate_ques_3_b').text("Please select !!");
            $('#validate_ques_3_b').show();

            //scroll to question 3
            scrollToSpecificQuestions(3);

            return;
        }
        else
        {
            $('#validate_ques_3_b').hide();
        }

        var q3_c = $('input[name="q3_c"]:checked').val();
        if(q3_c == undefined)
        {
            $('#validate_ques_3_c').text("Please select !!");
            $('#validate_ques_3_c').show();

            //scroll to question 3
            scrollToSpecificQuestions(3);

            return;
        }
        else
        {
            $('#validate_ques_3_c').hide();
        }

        var q3_d_1 = $('#input_3_d_1').val();
        var q3_d_2 = $('#input_3_d_2').val();
        var q3_d_3 = $('#input_3_d_3').val();

        var q3_e_1 = $('#input_3_e_1').val();
        var q3_e_2 = $('#input_3_e_2').val();

        var q3_f = $('#input_3_f').val();

        if(q3_d_1 == "" || q3_d_2 == "" || q3_d_3 == "" || q3_e_1 == "" || q3_e_2 == "" || q3_f == "")
        {
            $('#validate_ques_3_d_e_f').text("Fields cannot be empty !!");
            $('#validate_ques_3_d_e_f').show();

            //scroll to question 3
            scrollToSpecificQuestions(3);

            return;
        }
        else
        {
            $('#validate_ques_3_d_e_f').hide();
        }

        //Question 4
        var q4_a = $('input[name="q4_a"]:checked').val();
        if(q4_a == undefined)
        {
            $('#validate_ques_4_a').text("Please select !!");
            $('#validate_ques_4_a').show();

            //scroll to question 4
            scrollToSpecificQuestions(4);

            return;
        }
        else
        {
            $('#validate_ques_4_a').hide();
        }

        var q4_b = $('#input_4_b').val();
        if(q4_b == "")
        {
            $('#validate_ques_4_b').text("Fields cannot be empty !!");
            $('#validate_ques_4_b').show();

            //scroll to question 4
            scrollToSpecificQuestions(4);

            return;
        }
        else
        {
            $('#validate_ques_4_b').hide();
        }

        var q4_c = $('input[name="q4_c"]:checked').val();
        if(q4_c == undefined)
        {
            $('#validate_ques_4_c').text("Please select !!");
            $('#validate_ques_4_c').show();

            //scroll to question 4
            scrollToSpecificQuestions(4);

            return;
        }
        else
        {
            $('#validate_ques_4_c').hide();
        }

        //Question 5
        var q5_a_1 = $('input[name="q5_a"]:checked').val();
        if(q5_a_1 == undefined)
        {
            $('#validate_ques_5_a_1').text("Please select !!");
            $('#validate_ques_5_a_1').show();

            //scroll to question 5
            scrollToSpecificQuestions(5);

            return;
        }
        else
        {
            $('#validate_ques_5_a_1').hide();
        }

        var q5_a_2 = $('#input_5_a_2').val();
        if(q5_a_2 == "")
        {
            $('#validate_ques_5_a_2').text("Fields cannot be empty !!");
            $('#validate_ques_5_a_2').show();

            //scroll to question 5
            scrollToSpecificQuestions(5);

            return;
        }
        else
        {
            $('#validate_ques_5_a_2').hide();
        }

        var q5_b = $('input[name="q5_b"]:checked').val();
        if(q5_b == undefined)
        {
            $('#validate_ques_5_b').text("Fields cannot be empty !!");
            $('#validate_ques_5_b').show();

            //scroll to question 5
            scrollToSpecificQuestions(5);

            return;
        }
        else
        {
            $('#validate_ques_5_b').hide();
        }

        var q5_c_1 = $('#input_5_c_1').val();
        if(q5_c_1 == "")
        {
            $('#validate_ques_5_c_1').text("Fields cannot be empty !!");
            $('#validate_ques_5_c_1').show();

            //scroll to question 5
            scrollToSpecificQuestions(5);

            return;
        }
        else
        {
            $('#validate_ques_5_c_1').hide();
        }

        var q5_c_2 = $('#input_5_c_2').val();
        if(q5_c_2 == "")
        {
            $('#validate_ques_5_c_2').text("Fields cannot be empty !!");
            $('#validate_ques_5_c_2').show();

            //scroll to question 5
            scrollToSpecificQuestions(5);

            return;
        }
        else
        {
            $('#validate_ques_5_c_2').hide();
        }

        var q5_d = $('input[name="q5_d"]:checked').val();
        if(q5_d == undefined)
        {
            $('#validate_ques_5_d').text("Please select !!");
            $('#validate_ques_5_d').show();

            //scroll to question 5
            scrollToSpecificQuestions(5);

            return;
        }
        else
        {
            $('#validate_ques_5_d').hide();
        }

        var q5_e_1 = $('#input_5_e_1').val();
        if(q5_e_1 == "")
        {
            $('#validate_ques_5_e_1').text("Fields cannot be empty !!");
            $('#validate_ques_5_e_1').show();

            //scroll to question 5
            scrollToSpecificQuestions(5);

            return;
        }
        else
        {
            $('#validate_ques_5_e_1').hide();
        }

        var q5_e_2 = $('#input_5_e_2').val();
        if(q5_e_2 == "")
        {
            $('#validate_ques_5_e_2').text("Fields cannot be empty !!");
            $('#validate_ques_5_e_2').show();

            //scroll to question 5
            scrollToSpecificQuestions(5);

            return;
        }
        else
        {
            $('#validate_ques_5_e_2').hide();
        }

        //Question 6
        var q6_a = $('input[name="q6_a"]:checked').val();
        if(q6_a == undefined)
        {
            $('#validate_ques_6_a').text("Please select !!");
            $('#validate_ques_6_a').show();

            //scroll to question 6
            scrollToSpecificQuestions(6);

            return;
        }
        else
        {
            $('#validate_ques_6_a').hide();
        }

        var q6_b = $('input[name="q6_b"]:checked').val();
        if(q6_b == undefined)
        {
            $('#validate_ques_6_b').text("Please select !!");
            $('#validate_ques_6_b').show();

            //scroll to question 6
            scrollToSpecificQuestions(6);

            return;
        }
        else
        {
            $('#validate_ques_6_b').hide();
        }

        var q6_c = $('input[name="q6_c"]:checked').val();
        if(q6_c == undefined)
        {
            $('#validate_ques_6_c').text("Please select !!");
            $('#validate_ques_6_c').show();

            //scroll to question 6
            scrollToSpecificQuestions(6);

            return;
        }
        else
        {
            $('#validate_ques_6_c').hide();
        }

        //Question 7
        var q7_a = $('input[name="q7_a"]:checked').val();
        if(q7_a == undefined)
        {
            $('#validate_ques_7_a').text("Please select !!");
            $('#validate_ques_7_a').show();

            //scroll to question 7
            scrollToSpecificQuestions(7);

            return;
        }
        else
        {
            $('#validate_ques_7_a').hide();
        }

        var q7_b = $('input[name="q7_b"]:checked').val();
        if(q7_b == undefined)
        {
            $('#validate_ques_7_b').text("Please select !!");
            $('#validate_ques_7_b').show();

            //scroll to question 7
            scrollToSpecificQuestions(7);

            return;
        }
        else
        {
            $('#validate_ques_7_b').hide();
        }

        var q7_c = $('input[name="q7_c"]:checked').val();
        if(q7_c == undefined)
        {
            $('#validate_ques_7_c').text("Please select !!");
            $('#validate_ques_7_c').show();

            //scroll to question 7
            scrollToSpecificQuestions(7);

            return;
        }
        else
        {
            $('#validate_ques_7_c').hide();
        }

        //Question 8
        var q8_a_1 = $('#input_8_a_1').val();
        var q8_a_2 = $('#input_8_a_2').val();
        if(q8_a_1 == "" || q8_a_2 == "")
        {
            $('#validate_ques_8_a').text("Field cannot be empty !!");
            $('#validate_ques_8_a').show();

            //scroll to question 8
            scrollToSpecificQuestions(8);

            return;
        }
        else
        {
            $('#validate_ques_8_a').hide();
        }

        var q8_b = $('input[name="q8_b"]:checked').val();
        if(q8_b == undefined)
        {
            $('#validate_ques_8_b').text("Please select !!");
            $('#validate_ques_8_b').show();

            //scroll to question 8
            scrollToSpecificQuestions(8);

            return;
        }
        else
        {
            $('#validate_ques_8_b').hide();
        }

        var q8_c = $('input[name="q8_c"]:checked').val();
        if(q8_c == undefined)
        {
            $('#validate_ques_8_c').text("Please select !!");
            $('#validate_ques_8_c').show();

            //scroll to question 8
            scrollToSpecificQuestions(8);

            return;
        }
        else
        {
            $('#validate_ques_8_c').hide();
        }

        //Question 9
        var q9 = $('#input_9').val();
        if(q9 == "")
        {
            $('#validate_ques_9').text("Field cannot be empty !!");
            $('#validate_ques_9').show();

            //scroll to question 9
            scrollToSpecificQuestions(9);

            return;
        }
        else
        {
            $('#validate_ques_9').hide();
        }

        //Question 10
        var q10 = $('input[name="q10"]:checked').val();
        if(q10 == undefined)
        {
            $('#validate_ques_10').text("Please select !!");
            $('#validate_ques_10').show();

            //scroll to question 10
            scrollToSpecificQuestions(10);

            return;
        }
        else
        {
            $('#validate_ques_10').hide();
        }

        var proposalMasterId = $('#proposalMasterId').val();
        e.preventDefault();

        //Doctor's verification

        //hide err msg first
        $('#err_examination_place').hide();
        $('#err_examination_date').hide();
        $('#err_full_name').hide();
        $('#err_degree').hide();
        $('#err_code_no').hide();
        $('#err_present_address').hide();
        $('#err_doctor_sig_id').hide();

        var examPlace = $('#examination_place').val();
        $('#err_examination_date').hide();

        if(examPlace == "")
        {
            $('#err_examination_place').text("Required !!");
            $('#err_examination_place').show();
            return;
        }
        else
        {
            $('#err_examination_place').hide();
        }

        var examDate = $('#examination_date').val();

        if(examDate == "")
        {
            $('#err_examination_date').text("Required !!");
            $('#err_examination_date').show();
            return;
        }
        else
        {
            $('#err_examination_date').hide();
        }

        var fullName = $('#full_name').val();

        if(fullName == "")
        {
            $('#err_full_name').text("Required !!");
            $('#err_full_name').show();
            return;
        }
        else
        {
            $('#err_full_name').hide();
        }

        var degree = $('#degree').val();

        if(degree == "")
        {
            $('#err_degree').text("Required !!");
            $('#err_degree').show();
            return;
        }
        else
        {
            $('#err_degree').hide();
        }

        var regNo = $('#reg_no').val();

        if(regNo == "")
        {
            $('#err_reg_no').text("Required !!");
            $('#err_reg_no').show();
            return;
        }
        else
        {
            $('#err_reg_no').hide();
        }

        var codeNo = $('#code_no').val();

        if(codeNo == "")
        {
            $('#err_code_no').text("Required !!");
            $('#err_code_no').show();
            return;
        }
        else
        {
            $('#err_code_no').hide();
        }

        var presentAddress = $('#present_address').val();

        if(presentAddress == "")
        {
            $('#err_present_address').text("Required !!");
            $('#err_present_address').show();
            return;
        }
        else
        {
            $('#err_present_address').hide();
        }

        var fileUploaded = $("#doctor_sig_id").val();
        var fileAlreadyExist = $("#chk_file_upload").val();

        if(fileUploaded == "" && fileAlreadyExist == "")
        {
            $('#err_doctor_sig_id').text("Required !!");
            $('#err_doctor_sig_id').show();
            return;
        }


        //Answer Dto
        var answerDto = {};

        answerDto.ans_1_a_text = q1_a;
        answerDto.ans_1_b_text = q1_b;

        answerDto.ans_2_text = q2;

        answerDto.ans_3_a_text = q3_a;
        answerDto.ans_3_b_text = q3_b;
        answerDto.ans_3_c_text = q3_c;
        answerDto.ans_3_d_1_text = q3_d_1;
        answerDto.ans_3_d_2_text = q3_d_2;
        answerDto.ans_3_d_3_text = q3_d_3;
        answerDto.ans_3_e_1_text = q3_e_1;
        answerDto.ans_3_e_2_text = q3_e_2;
        answerDto.ans_3_f_text = q3_f;

        answerDto.ans_4_a_text = q4_a;
        answerDto.ans_4_b_text = q4_b;
        answerDto.ans_4_c_text = q4_c;

        answerDto.ans_5_a_1_text = q5_a_1;
        answerDto.ans_5_a_2_text = q5_a_2;
        answerDto.ans_5_b_text = q5_b;
        answerDto.ans_5_c_1_text = q5_c_1;
        answerDto.ans_5_c_2_text = q5_c_2;
        answerDto.ans_5_d_text = q5_d;
        answerDto.ans_5_e_1_text = q5_e_1;
        answerDto.ans_5_e_2_text = q5_e_2;

        answerDto.ans_6_a_text = q6_a;
        answerDto.ans_6_b_text = q6_b;
        answerDto.ans_6_c_text = q6_c;

        answerDto.ans_7_a_text = q7_a;
        answerDto.ans_7_b_text = q7_b;
        answerDto.ans_7_c_text = q7_c;

        answerDto.ans_8_a_1_text = q8_a_1;
        answerDto.ans_8_a_2_text = q8_a_2;
        answerDto.ans_8_b_text = q8_b;
        answerDto.ans_8_c_text = q8_c;

        answerDto.ans_9_text = q9;

        answerDto.ans_10_text = q10;

        answerDto.examinationPlace = examPlace;
        answerDto.examinationDate = examDate;
        answerDto.fullName = fullName;
        answerDto.degree = degree;
        answerDto.regNo = regNo;
        answerDto.codeNo = codeNo;
        answerDto.presentAddress = presentAddress;

        $.confirm({
            title: 'Confirm',
            content: 'Are your sure?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url:  "/new-business/save-med-report",
                        type: 'POST',
                        async: false,
                        data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function(response) {

                            console.log(response);
                            if(response.id != null){

                                $.confirm({
                                    title: 'Success',
                                    content: 'Saved Successfully',
                                    buttons: {
                                        ok: function () {
                                            window.location.href = '/generic/medical/list';
                                        }
                                    }
                                });

                               if(fileAlreadyExist != ""){
                                   $.ajax({
                                       url: "med-ques-upload-file",
                                       type: "POST",
                                       data: new FormData($("#agent_question_form")[0]),
                                       enctype: 'multipart/form-data',
                                       async: false,
                                       processData: false,
                                       contentType: false,
                                       dataType: 'json',
                                       success: function (res) {
                                           //console.log(res);
                                           //showAlert("File uploaded");

                                           if(res == true)
                                           {
                                               showAlert("Saved successfully");
                                               //setTimeout(window.location.href = '/generic/medical/list', 10000);
                                           }

                                       },
                                       error: function (jqXHR, textStatus, errorThrown) {
                                           showAlert("File upload failed");
                                       }
                                   });
                               }

                            }
                            else {
                                showAlert("Cannot add.");
                            }

                        },
                        error: function(xhr, status, error) {
                        }
                    });
                },
                cancel: function () {

                }
            }
        });




    });
});