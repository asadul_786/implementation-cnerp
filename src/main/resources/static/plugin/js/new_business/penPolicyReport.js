$(document).ready(function() {
    officeAutoComplete();
    $('#policySchDate').val($.datepicker.formatDate('dd/mm/yy', new Date()));

    $("#descendentOffice").click(function() {
        if($('#descendentOffice').is(':checked')) {
            $('#descOfficeVal').val('0');
        } else {
            $('#descOfficeVal').val('1');
        }
    });
    $("#genPolSchReport").click(function () {

        var reportType = $('#reportType').val();
        var policyNoReport = $('#policyNoReport').val();
        var polOffId = $('#polOffId').val();
        var descOfficeVal = $('#descOfficeVal').val();
        var policySchDate = $('#policySchDate').val();

        var polVal = policyNoReport.concat(","+policySchDate);
        var offVal = polOffId.concat(","+descOfficeVal).concat(","+policySchDate);
        if(reportType == "0"){
            $.confirm({
                title: 'Message',
                content: 'Please Select Report Type',
                buttons: {
                    ok: function () {
                    },
                }
            });
            $('#polOffId').val('');
            $('#policyOffice').val('');
            $('#policyNoReport').val('');
        }else{
            if(policyNoReport){
                $.confirm({
                    title: 'Message',
                    content: 'Are You Sure To Generate Pension Policy Schedule Report',
                    buttons: {
                        ok: function () {
                            window.open('/policySummReport/genPenPolRepByPolNo?strArr='+polVal, '_blank');
                        },
                        cancel: function () {
                        },
                    }
                });
            }else if(polOffId){
                $.confirm({
                    title: 'Message',
                    content: 'Are You Sure To Generate Pension Policy Schedule Report',
                    buttons: {
                        ok: function () {
                            window.open('/policySummReport/genPenPolRepByPolOff?strArr='+offVal, '_blank');
                        },
                        cancel: function () {
                        },
                    }
                });
            }else{
                $.confirm({
                    title: 'Message',
                    content: 'Please Select Policy No or Policy Office',
                    buttons: {
                        ok: function () {
                        },
                    }
                });
            }
        }
    });
});

function officeAutoComplete() {

    var policyOffice = "#" + "policyOffice";
    var url = "/collection/officeAutoComplete";
    var polOffId = "#" + "polOffId";
    autocomplete(policyOffice, url, "", polOffId);
}

function getPolSchReportParam(){

    var reportType = $('#reportType').val();
    if(reportType == '1'){
        $('#polOffId').val('');
        $('#policyOffice').val('');
        $("#policyOffice").attr("disabled", true);
        $("#descendentOffice").attr("disabled", true);
    }else{
        $("#policyOffice").attr("disabled", false);
        $("#descendentOffice").attr("disabled", false);
    }
    if(reportType == '2'){
        $('#policyNoReport').val('');
        $("#policyNoReport").attr("disabled", true);
    }else{
        $("#policyNoReport").attr("disabled", false);
    }
}

function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};