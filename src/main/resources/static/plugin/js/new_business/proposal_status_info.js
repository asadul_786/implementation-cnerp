
$(document).ready(function () {

   $('#tbl-proposal-lst').DataTable({
      // dom: 'Bfrtip', "scrollX": true,
      // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
       //scrollY:        "300px",
       scrollX:        true,
       scrollCollapse: true,
       paging:         true,
       columnDefs: [{ width: '30%', targets: 0 }]
    });

});

function validateForm() {
   // alert(document.forms["proposalSearchPanel"]["officeCode"].value);
    var x = document.forms["proposalSearchPanel"]["officeCode"].value;
    // console.log(x);
    if (isEmptyString(x)) {
       customAlert(alertTypes.WARNING,"Required!","Servicing office is required ")
        //showAlertByType("Servicing office is required!","W");
        // $('.requiredF').attr("display","none");
        return false;
    }
}

$("#refreshbtn").click(function () {
    resetForm();
});



$(document).on("click", "#lockForUw", function() {

    var curRow = $(this).closest('tr');
    var col1 = curRow.find('td:eq(0)').text();
    var logValue = curRow.find('td:eq(1)').text();

  // if(logValue==7){
   //     return;
  // }
 //  else {

        $.confirm({
            icon: 'ace-icon fa fa-exclamation-circle',
            theme: 'material',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',

            title: "Are you sure to Lock for Underwriting?",
            content: 'Once forwarded  , you will not be able to modify proposal data!',
            typeAnimated: true,
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                            contentType: 'application/json',
                            url: "/new-business/check-dublicate-lock-value/" + col1,
                            type: 'POST',
                            //async: false,
                            //data: JSON.stringify(answerDto),
                            dataType: 'json',
                            success: function (response) {

                                if (response == false) {
                                    //showAlert("Successfully Locked for Underwriting");
                                    showAlert("Already Locked for Underwriting ,Try Another One", "F");

                                } else {
                                    $.ajax({
                                        contentType: 'application/json',
                                        url: "/new-business/update-log-cycle-value/" + col1,
                                        type: 'POST',
                                        //async: false,
                                        //data: JSON.stringify(answerDto),
                                        dataType: 'json',
                                        success: function (response) {
                                           // alert("hi..");
                                            if (response == true) {

                                                showAlert("Successfully Locked for Underwriting");
                                                // $("#updateProposalInfo").attr("disabled", true);
                                                //$('#updateProposalInfo').prop('disabled', true);
                                                //  $('#updateProposalInfo').button ("disable");
                                               // $('#updateProposalInfo').disabled = true;

                                            } else {
                                                showAlert("Wrong !! Not Locked for Underwriting");
                                            }

                                        },
                                        error: function (xhr, status, error) {

                                        }
                                    });
                                }

                            },
                            error: function (xhr, status, error) {

                            }
                        });
                    }

                },
                CANCEL: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                        return true;
                    }
                }
            }
        });
   // }
});

$(document).on("click", "#updateProposalInfo", function() {
    var curRow = $(this).closest('tr');
    var col0 = curRow.find('td:eq(0)').text();
    var logValue = curRow.find('td:eq(1)').text();

   // if(logValue==7){
   //   return;
    //}
     //else {

        window.location.replace("/new-business/updateProposalInfo?pgid=" + col0, target = "_blank");

    // }

});





function resetForm() {

    $('#proposalNo').val("");
    $('#officeCode').val("-1");

}

function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}


