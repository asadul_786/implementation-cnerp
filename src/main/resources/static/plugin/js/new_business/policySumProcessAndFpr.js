$(document).ready(function() {


    //$('#processPolicySumm').attr('disabled',false);
    //$('#reportGen').attr('disabled',true);

    /*  $("#proposalNo").on('change', function(){

          $('#reportGen').attr('disabled',true);

          $("#installmentAmnt").val('');
          $("#recvAmnt").val('');

          var proposalNo = $('#proposalNo').val();

          var json = {
              "proposalNo": proposalNo
          };

          $.ajax({
              type: "POST",
              contentType: "application/json",
              url: "/policySummReport/getInstAndRecvAmount/"
              + proposalNo,
              data: JSON
                  .stringify(json),
              dataType: 'json',
              cache: false,
              timeout: 600000,
              success: function (data) {
                  if(data == null || data == ""){
                      $.alert({
                          title: '',
                          content: 'Please Enter Valid Proposal No!',
                      });
                      $("#proposalNo").val('');
                  }else{
                      $('#reportGen').attr('disabled',false);
                      var value_0 = "";
                      var value_1 = "";

                      $.each(data, function (i, l) {

                          value_0 = l[0];
                          value_1 = l[1];

                          $("#installmentAmnt").val(value_1);
                          $("#recvAmnt").val(value_0);

                      });
                  }
              },
              error: function (e) {
              }
          });
      })*/
    function getUnderWritingStatus(){

        //$('#processPolicySumm').attr('disabled',true);

        var json = {
            "bankCd": ""

        };
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/policySummReport/getUnderWritingStatus",
            data: JSON
                .stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                //$('#processPolicySumm').attr('disabled',true);
            },
            error: function (e) {
            }
        });


    }
    //$('#processPolicySumm').attr('disabled',true);
    //getUnderWritingStatus();
    $("#refresh_button").click(function () {
        clearform();
    });

    $("#reportGen").click(function () {

        var policyNo = $('#policyNoProcedure').val();
        var receiveAmount = $('#recvAmnt').val();

        var values = policyNo.concat(",").concat(receiveAmount);

        if(policyNo){

            var json = {
                "policyNo": policyNo
            };

            $.confirm({
                title: 'Message',
                content: 'Are You Sure To Generate FPR',
                buttons: {
                    ok: function () {
                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/policySummReport/genFprProcess/"+policyNo,
                            data: JSON.stringify(json),
                            dataType: 'json',
                            success: function (data) {
                                $.each(data, function(key, value) {

                                });
                                if(data[1] == "Success"){
                                    $.confirm({
                                        title: 'Confirmation',
                                        content: data[1],
                                        buttons: {
                                            ok: function () {
                                                window.open('/policySummReport/generatePolicySummFPR.pdf?policyNo='+policyNo, '_blank');
                                                sendSmsToClient(values);
                                            }
                                        }
                                    });
                                }
                                if(data[1] != "Success"){
                                    $.confirm({
                                        title: 'Confirmation',
                                        content: data[1],
                                        buttons: {
                                            ok: function () {

                                            }
                                        }
                                    });
                                }
                            },
                            error: function (e) {
                            }
                        });
                    },
                    cancel: function () {
                    },
                }
            });
        }else{
            $.confirm({
                title: 'Message',
                content: 'First Do Exactly Process Policy Summery',
                buttons: {
                    ok: function () {
                    },
                }
            });
        }
    });

    $("#processPolicySumm").click(function () {

        var proposalNo = $('#proposalNo').val();
        var policyNo = $('#policyNo').val();

        //var pgId = $('#pgId').val();

        var json = {
            "proposalNo": proposalNo,
            "policyNo": policyNo
        };

        if(policyNo == 0){
            $.confirm({
                title: 'Message',
                content: 'Are You Sure To Do This Process',
                buttons: {
                    ok: function () {
                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/policySummReport/excPolicySummFprPorocess/"+proposalNo+"/"+policyNo,
                            data: JSON.stringify(json),
                            dataType: 'json',
                            success: function (data) {

                                $("#policyNoProcedure").val('');
                                $("#policyNoPro").val('');

                                $.each(data, function(key, value) {

                                    if(key == 1){
                                        $("#policyNoProcedure").val(value);
                                        $("#policyNoPro").val(value);
                                        //$('#processPolicySumm').attr('disabled',true);
                                    }

                                });
                                getBeforeFprInfo(proposalNo);
                                if(data[2] == "success"){
                                    //getPolicySummFprInfoList(pgId);
                                    //$('#reportGen').attr('disabled',false);
                                    $.confirm({
                                        title: 'Confirmation',
                                        content: data[2],
                                        buttons: {
                                            ok: function () {
                                            }
                                        }
                                    });
                                }
                                if(data[2] != "success"){
                                    //getBeforeFprInfo(proposalNo);
                                    //$('#reportGen').attr('disabled',true);
                                    //$('#processPolicySumm').attr('disabled',false);
                                    $.confirm({
                                        title: 'Confirmation',
                                        content: data[3],
                                        buttons: {
                                            ok: function () {
                                            }
                                        }
                                    });

                                }
                            },
                            error: function (e) {
                            }
                        });
                    },
                    cancel: function () {
                    },
                }
            });
        }else{
            $.confirm({
                title: 'Confirmation',
                content: 'Already Summary Data is being processed',
                buttons: {
                    ok: function () {
                    }
                }
            });
        }

    });
    $("#genReport").click(function () {

        var policyNo = $('#policyNoReport').val();

        if(policyNo){

            var json = {
                "policyNo": policyNo
            };

            $.confirm({
                title: 'Message',
                content: 'Are You Sure To Generate FPR Report',
                buttons: {
                    ok: function () {
                        window.open('/policySummReport/generatePolicySummFPR.pdf?policyNo='+policyNo, '_blank');
                    },
                    cancel: function () {
                    },
                }
            });
        }else{
            $.confirm({
                title: 'Message',
                content: 'Please Provide Valid Policy No',
                buttons: {
                    ok: function () {
                    },
                }
            });
        }
    });

    var showAlert = function (alertContent) {
        new PNotify({
            title: 'Info',
            text: alertContent,
            type: 'success',
            styling: 'bootstrap3',
        });
        setTimeout(alertContent, 100);
    }
});

function dataValidation() {

    var status = true;

    if ($("#proposalNo").val() == "" || $("#proposalNo").val() == "0") {
        status = false;
        $("#proposalNoSpan").text("Empty field found!!");
        $("#proposalNo").focus();
    }
    else $("#proposalNoSpan").text("");

    return status;
}


function getPolicySummFpr(proposalNo){

    var json = {
        "proposalNo": proposalNo
    };

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Process!!',
        buttons: {
            ok: function () {
                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/policySummReport/getPolicySumFprInfo/"+proposalNo,
                    data: JSON.stringify(json),
                    dataType: 'json',
                    success: function (data) {
                        var policySumFprinfo = data;
                        document.location.href="/policySummReport/getPolicySummFpr/"+policySumFprinfo;
                    },
                    error: function (e) {
                    }
                });
            },
            cancel: function () {

            },
        }
    });
}

function getBeforeFprInfo(proposalNo){

    var table = $("#dataTable tbody");

    var json = {
        "proposalNo": proposalNo
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/policySummReport/getBeforeFprInfo/"+proposalNo,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            table.empty();

            var fprNo = "";
            var policyNo = "";
            var assNm = "";
            var add = "";
            var orgSetup = "";
            var proNo = "";
            var proDate = "";
            var commDate = "";
            var nextDueDate = "";
            var summAss = "";
            var prodIdTerm = "";
            var paymentMode = "";
            var susAmnt = "";
            var premAmnt = "";

            $.each(data, function (i, l) {

                fprNo = l[0];
                if(fprNo){
                    fprNo = fprNo;
                }else{
                    fprNo = "";
                }
                policyNo = l[3];
                if(policyNo){
                    policyNo = policyNo;
                }else{
                    policyNo = "";
                }
                assNm = l[9];
                if(assNm){
                    assNm = assNm;
                }else{
                    assNm = "";
                }
                add = l[14]
                if(add){
                    add = add;
                }else{
                    add = "";
                }
                orgSetup = l[4];
                if(orgSetup){
                    orgSetup = orgSetup;
                }else{
                    orgSetup = "";
                }
                proNo = l[6];
                if(proNo){
                    proNo = proNo;
                }else{
                    proNo = "";
                }
                proDate = l[7];
                if(proDate){
                    proDate = getDateShow(proDate);
                }else{
                    proDate = "";
                }
                commDate = l[13];
                if(commDate){
                    commDate = getDateShow(commDate);
                }else{
                    commDate = "";
                }
                nextDueDate = l[12];
                if(nextDueDate){
                    nextDueDate = getDateShow(nextDueDate);
                }else{
                    nextDueDate = "";
                }
                summAss = l[15]
                if(summAss){
                    summAss = summAss;
                }else{
                    summAss = "";
                }
                prodIdTerm = l[17];
                if(prodIdTerm){
                    prodIdTerm = prodIdTerm;
                }else{
                    prodIdTerm = "";
                }
                paymentMode = l[18];
                if(paymentMode){
                    paymentMode = paymentMode;
                }else{
                    paymentMode = "";
                }
                susAmnt = l[11];
                if(susAmnt){
                    susAmnt = susAmnt;
                }else{
                    susAmnt = "";
                }
                premAmnt = l[5];
                if(premAmnt){
                    premAmnt = premAmnt;
                }else{
                    premAmnt = "";
                }

                table.append("<tr>" +
                    "<td>"+'<span>'+fprNo+'</span>'+"</td>" +
                    "<td>"+'<span>'+policyNo+'</span>'+"</td>" +
                    "<td>"+assNm+"</td>" +
                    "<td>"+add+"</td>"+
                    "<td>" + orgSetup + "</td>" +
                    "<td>" + proNo + "</td>" +
                    "<td>" + proDate + "</td>" +
                    "<td>" + commDate + "</td>" +
                    "<td>" + nextDueDate + "</td>" +
                    "<td>" + summAss + "</td>" +
                    "<td>" + prodIdTerm + "</td>" +
                    "<td>" + paymentMode + "</td>" +
                    "<td>" + susAmnt + "</td>" +
                    "<td>" + premAmnt + "</td>" +
                    "</tr>");
            });
            $("#dataTable").DataTable();
        },
        error: function (e) {
        }
    });



}

function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};


function sendSmsToClient(values){

    var json = {
        "values": values

    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/policySummReport/sendSmsToClient/"+values,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

        },
        error: function (e) {
        }
    });

}
