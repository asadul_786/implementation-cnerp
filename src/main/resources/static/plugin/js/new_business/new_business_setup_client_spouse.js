/**
 * Created by User on 3/24/2019.
 */

$(document).ready(function () {

    $("#add-client-spouse").click(function () {

        var flag=ValidatingData();

        if(flag== true){

            var proSpouse = {};

            //proSpouse.spPGID=$("#pgidss").val();
            proSpouse.spPARTY_CD=$("#cl_party_id").val();
            proSpouse.spSP_NAME = $("#spSP_NAME").val();
            proSpouse.spANNUAL_INCONME = $("#spANNUAL_INCONME").val();
            proSpouse.occupationRelation = $("#occupationRelation").val();
            proSpouse.spIncomeSource = $("#spIncomeSource").val();

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/new-business/add-spouse",
                data: JSON.stringify(proSpouse),
                dataType: 'json',
                success: function (data) {
                    // alert(data);
                    updateTableList();
                    showAlert("Successfully Saved!");
                    refresh();
                },
                error: function (e) {
                    showAlert("Something Wrong!!!");
                }
            });
        }
    });

    function updateTableList() {

       // alert("hi");
        var spouseTable = $('#spouseTableId');
       // alert("hlw");
        $.ajax({
            type: "GET",
            url: "/new-business/addSpouseList",
            success: function (data) {
              //  alert(data);
                var no = 1;
                var tableBody = "";

                $('#spouseTableId tbody').empty();
                $.each(data, function (idx, elem) {

                    // alert (elem[idx]);

                    var edit = '<a class="btn btn-success spouseInformationEdit" value="Edit" id="editSpouse"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger spouseInformationDelete" value="Delete" id="deleteSpouse"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td>" + elem[2] + "</td>";
                    tableBody += "<td  hidden class='sp-party-id'>" + elem[3] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td>" + elem[5] + "</td>";
                    tableBody += "<td hidden class='ann-party-id'>" + elem[6] + "</td>";
                    tableBody += "<td>" + elem[7] + "</td>";
                    tableBody += "<td>" + edit + "</td>"
                    tableBody += "<td>" + del + "</td>"
                    tableBody += "<tr>";
                });
                spouseTable.append(tableBody);
            }
        });
    }

    function refresh (){
        $('#spSP_NAME').val("");
        $('#spANNUAL_INCONME').val("");
        $('#occupationRelation').val("-1");
        $('#spIncomeSource').val("-1");
        $('#s_partycd').val("");

       $('#cl_party_id').val("-1");
    }

    $(document).on("click", "#spouseTableId tbody tr", function() {

        var curRow = $(this).closest('tr');

        var pgid = curRow.find('td:eq(0)').text();
        var pparty = curRow.find('td:eq(1)').text();
        var spname = curRow.find('td:eq(2)').text();
        var occuRelation = curRow.find('td:eq(3)').text();
        var annualIncome = curRow.find('td:eq(5)').text();
        var incomeSource = curRow.find('td:eq(7)').text();

        $('#pgid').val(pgid);
        $('#s_partycd').val(pparty);
        $('#spSP_NAME').val(spname);
        $('#occupationRelation').val(( $(this).find(".sp-party-id").html()));
        $('#spANNUAL_INCONME').val(annualIncome);
        $('#spIncomeSource').val(( $(this).find(".ann-party-id").html()));

    });

    $(document).on("click", "#deleteSpouse", function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
       // alert(col1);
       // alert(col2);
        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to Delete?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/spouse-proposal-info-delete/" + col1 +"/"+col2,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            refresh();
                            showAlert("Successfully deleted");
                            $('#spouseTableId tbody tr').empty();
                            updateTableList();

                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });


    function ValidatingData() {

        var status = true;
        var cDigit = /^[0-9]+$/;

        if ($("#spSP_NAME").val() == "") {
            status = false;
            $("#err_spouse_bn_name_id").text("Empty field found!!");
            $("#spSP_NAME").focus();
        } else $("#err_spouse_bn_name_id").text("");

        // if ($("#spANNUAL_INCONME").val() == "") {
        //     status = false;
        //     $("#err_aincome_bn_name_id").text("Empty field found!!");
        //     $("#spANNUAL_INCONME").focus();
        // } else $("#err_aincome_bn_name_id").text("");

        if ($("#spANNUAL_INCONME").val() == "") {
            status = false;
            $("#err_aincome_bn_name_id").text("Empty field found!!");
            $("#spANNUAL_INCONME").focus();
        } else if (!cDigit.test($("#spANNUAL_INCONME").val())){
            status = false;
            $('#err_aincome_bn_name_id').text("Only Digit allow here !!");
            $('#spANNUAL_INCONME').focus();
        } else $('#err_aincome_bn_name_id').text("");

        if ($("#occupationRelation").val() == "-1") {
            status = false;
            $("#err_occupation_bn_relation_id").text("Empty field found!!");
            $("#occupationRelation").focus();
        } else $("#err_occupation_bn_relation_id").text("");

        if ($("#spIncomeSource").val() == "-1") {
            status = false;
            $("#err_spIncome_bn_relation_id").text("Empty field found!!");
            $("#spIncomeSource").focus();
        } else $("#err_spIncome_bn_relation_id").text("");

        if ($("#cl_party_id").val()== "-1") {
            status = false;
            $("#err_cl_party_id").text("Empty field found!!");
            $("#cl_party_id").focus();
        } else $("#err_cl_party_id").text("");

        return status;
    }


});

//
// $(document).ready(function () {
//     var validationClientFamily = function () {
//         var isValid = true;
//         $(".cl_family_form .err_msg").text("");
//         $(".cl_family_form input.mand").each(function(){
//             if(this.value.length == 0) {
//                 $("#err_" + this.id ).text("Please enter the value");
//                 $(this.id ).focus();
//                 isValid = false;
//             }
//         });
//         $(".cl_family_form select.mand").each(function(){
//             if(this.value == -1) {
//                 $("#err_" + this.id ).text("Please select the value");
//                 $(this.id ).focus();
//                 isValid = false;
//             }
//         });
//         return isValid;
//     };
//
//     var resetClientFamilyForm = function () {
//         $('#cl_fam_id').val("0");
//         $('#cl_fam_relation_id').val("-1");
//         $('#cl_fam_age_id').val("");
//         $('#cl_fam_phy_id').val("");
//         $('#cl_fam_age_death_id').val("");
//         $('#cl_fam_cause_death_id').val("");
//         $('#cl_fam_yr_death_id').val("");
//         $('#cl_fam_remarks_id').val("");
//
//     };
//
//     var updateClientFamilyTable = function () {
//         var tableData = $("#cl_family_table_data_id");
//         masterId = $('#proposalMasterNo').val();
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-client-family-info",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(masterId),
//             dataType: 'json',
//             success: function (response) {
//                 console.log("here client family info table2");
//                 tableData.html("");
//                 html = "";
//                 $.each(response, function(idx, elem){
//                     var ageDeath = (elem.causeDeath == null) ? "N/A" : elem.causeDeath;
//                     var causeDeath = (elem.causeDeath == null) ? "N/A" : elem.causeDeath;
//                     var yearDeath = (elem.yearDeath == null) ? "N/A" : elem.yearDeath;
//                     var remarks = (elem.remarks == null) ? "N/A" : elem.remarks;
//
//                     html = "";
//                     html += "<tr>";
//                     html += "<td>" + elem.relationName + "</td>";
//                     html += "<td>" + elem.age + "</td>";
//                     html += "<td>" + elem.phyCondition + "</td>";
//                     html += "<td>" + ageDeath + "</td>";
//                     html += "<td>" + causeDeath + "</td>";
//                     html += "<td>" + yearDeath + "</td>";
//                     html += "<td>" + remarks + "</td>";
//                     html += "<td>" + '<button type="button" class="btn  btn-sm btn-info cl_fam_edit_button" value="Edit" button_id="'+ elem.id +'"> ' +
//                         '<i class="fa fa-pencil"></i></button>' + '<button type="button" class="btn  btn-sm btn-danger cl_fam_delete_button" value="Delete" button_id="'+ elem.id +'"> '   +
//                         '<i class="fa fa-trash"></i></button>'+ "</td>";
//                     html += "<tr>";
//                     tableData.append(html);
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//
//
//
//     };
//
//     updateClientFamilyTable();
//
//     var confirmClientFamilyDialog = function (text) {
//         $.confirm({
//             title: 'Confirm!',
//             content: text,
//             buttons: {
//                 confirm: {
//                     btnClass: 'btn-info',
//                     keys: ['enter'],
//                     action: function () {
//                         var proposal_client_family = {};
//
//                         proposal_client_family.masterId = $('#proposalMasterNo').val();
//                         proposal_client_family.id = $('#cl_fam_id').val();
//                         proposal_client_family.relationId = $('#cl_fam_relation_id').val();
//                         proposal_client_family.age = $('#cl_fam_age_id').val();
//                         proposal_client_family.phyCondition = $('#cl_fam_phy_id').val();
//                         proposal_client_family.ageDeath = $('#cl_fam_age_death_id').val();
//                         proposal_client_family.causeDeath = $('#cl_fam_cause_death_id').val();
//                         proposal_client_family.yearDeath = $('#cl_fam_yr_death_id').val();
//                         proposal_client_family.remarks = $('#cl_fam_remarks_id').val();
//
//                         $.ajax({
//                             contentType: 'application/json',
//                             url: "add-client-family-info",
//                             type: 'POST',
//                             async: false,
//                             data: JSON.stringify(proposal_client_family),
//                             dataType: 'json',
//                             success: function (response) {
//                                 console.log(proposal_client_family);
//                                 console.log("here client family");
//                                 updateClientFamilyTable();
//                                 resetClientFamilyForm();
//                                 showAlert("Successfully updated");
//                             },
//                             error: function (xhr, status, error) {
//                             }
//                         });
//                     }
//
//                 },
//                 cancel: function () {
//
//                 }
//             }
//         });
//     };
//
//
//     $("#cl_family_save_button").click(function (event) {
//         event.preventDefault();
//         var isVaild = validationClientFamily();
//         if(isVaild) {
//             confirmClientFamilyDialog("Are you sure to save Family history?");
//         }
//     });
//     //edit button
//     $(document).on("click", ".cl_fam_edit_button", function (event) {
//         var row_id = $(this).attr("button_id");
//         console.log("row_id: " + row_id);
//
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-client-family-info-dtl",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(row_id),
//             dataType: 'json',
//             success: function (response) {
//                 console.log("here client family info dtl");
//                 $('#cl_fam_id').val(response.id);
//                 $('#cl_fam_relation_id').val(response.relationId);
//                 $('#cl_fam_age_id').val(response.age);
//                 $('#cl_fam_phy_id').val(response.phyCondition);
//                 $('#cl_fam_age_death_id').val(response.ageDeath);
//                 $('#cl_fam_cause_death_id').val(response.causeDeath);
//                 $('#cl_fam_yr_death_id').val(response.yearDeath);
//                 $('#cl_fam_remarks_id').val(response.remarks);
//
//
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//
//     });
//
//
// });