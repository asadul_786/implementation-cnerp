function parseNumber(str) {
    if(str==null|| str.trim()==""||str=="undefined"){
        return null;
    }
    else{
        var  intNum=parseInt(str);

        return intNum=="NaN"?null:intNum;
    }

}
$(document).ready(function () {

    $("#add-new-guardian").click(function () {

        var flag=ValidatingData();

        if(flag== true){

            //var guardpgidss =  $("#pgidss").val();
           // var guardianType = $("#guardianType").val();
            var guardassuredid = $("#assuredNameDropdownGuardian").val();
            var guardpartyname = $("#guardpartyname").val();
            var guarddateofbirth = $("#guarddateofbirth").val();
            var guardpartyRelation = $("#guardpartyRelation").val();
            var sl_no = parseNumber($("#sl_gaud").val());

            // alert(pgidss);

            var proposalGuard = {};

            proposalGuard.guardassuredid = guardassuredid;
            proposalGuard.guardpartyname = guardpartyname;
            proposalGuard.guarddateofbirth = guarddateofbirth;
            proposalGuard.guardpartyRelation = guardpartyRelation;
            proposalGuard.guardserialNo = sl_no;

            // alert(proposalGuard.guardassuredid);
            // alert(proposalGuard.guardpartyname);
            // alert(proposalGuard.guarddateofbirth);
            // alert(proposalGuard.guardpartyRelation);

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/new-business/add-guardian",
                data: JSON.stringify(proposalGuard),
                dataType: 'json',
                success: function (data) {
                    // alert(data);
                    updateTableList();
                    showAlert("Successfully Saved!");
                    refresh();
                },
                error: function (e) {
                    showAlert("Something Wrong!!!");
                }
            });
        }
    });

    function getFormatedDate(strDate) {

        var dateS = new Date(strDate);

        var d = dateS.getDate();
        var m = dateS.getMonth() + 1;
        var y = dateS.getFullYear();


        // return y+'-'+(m <= 9 ? '0' + m : m)+'-'+(d <= 9 ? '0' + d : d);
        return (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + y;

    }



    function updateTableList() {
        //alert("hi");
        var guardianTable = $('#guardianTableId');

        $.ajax({
            type: "GET",
            url: "/new-business/addGuardianList",
            success: function (data) {
               // alert(data);
                var no = 1;
                var tableBody = "";

                $('#guardianTableId tbody').empty();
                $.each(data, function (idx, elem) {
                   // alert (elem[idx]);
                    var edit = '<a class="btn btn-success GuardianInformationEdit" value="Edit" id="editGuardian"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger GuardianInformationDelete" value="Delete" id="deleteGuardian"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";

                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td hidden>" + elem[2] + "</td>";
                     tableBody += "<td hidden class='ngg-party-type'>" + elem[3] + "</td>";
                    // tableBody += "<td hidden>" + elem[2] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td>" + getFormatedDate(elem[5]) + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + edit + "</td>"
                    tableBody += "<td>" + del + "</td>"
                    tableBody += "<tr>";
                });
                guardianTable.append(tableBody);
            }
        });
    }



    $(document).on("click", "#profile-tab5", function (event) {
        loadAssuredDropdown();
        // updatePayorTable();
    });

    $( "#guarddateofbirth" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });

    $(document).on("click", "#guardianTableId tbody tr", function() {

        var curRow = $(this).closest('tr');

        var pgid = curRow.find('td:eq(0)').text();
        var sl_no = curRow.find('td:eq(1)').text();
        var party_cd = curRow.find('td:eq(2)').text();
        var party = curRow.find('td:eq(3)').text();

        var guardpartyname = curRow.find('td:eq(4)').text();
        var guarddateofbirth = curRow.find('td:eq(5)').text();
        var  guardpartyRelation= curRow.find('td:eq(6)').text();

        $('#pgid').val(pgid);
        $('#sl_gaud').val(sl_no);
        $('#gd_party').val(party_cd);

        $('#assuredNameDropdownGuardian').val(( $(this).find(".ngg-party-type").html()));
        $('#guardpartyname').val(guardpartyname);
        $('#guarddateofbirth').val(guarddateofbirth);
        $('#guardpartyRelation').val(guardpartyRelation);



    });


    var loadAssuredDropdown = function () {
        // var pgId = $("#pgid").val();
        //alert(pgId);

        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: "/new-business/get-payor-assured",
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                // alert(data);
                var html = '';
                html += '<option value="-1">--Select Assured--</option>';
                $.each(data, function (i, l) {
                    html += '<option value="' + l[0] + '">'
                        + l[1]
                        + '</option>';
                });
                html += '</option>';

                $('#assuredNameDropdownGuardian').html(html);
                //alert('accss the data');
            },
            error: function (e) {
                showAlert("Something wrong!!!");
            }
        });
    }

    function refresh (){
        //$('#guardianType').val("");
        $('#assuredNameDropdownGuardian').val("-1");
        $('#guardpartyname').val("");
        $('#guarddateofbirth').val("");
        $('#guardpartyRelation').val("-1");
        $('#pgid').val("");
        $('#sl_gaud').val("");
        $('#gd_party').val("");
    }




    $('#guardianTableId tbody').on('click', '#deleteGuardian', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to Delete?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/gaurdian-proposal-info-delete/" + col1+"/"+ col2+"/"+col3,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            refresh();
                            showAlert("Successfully deleted");
                            $('#guardianTableId tbody tr').empty();
                            updateTableList();

                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

    function ValidatingData() {

        var status = true;
        var cDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        var cChar = /^[a-zA-Z]+$/;

        // if ($("#guardpartyname").val() == "") {
        //     status = false;
        //     $("#err_guardianName_bn_name_id").text("Empty field found!!");
        //     $("#guardpartyname").focus();
        // } else $("#err_guardianName_bn_name_id").text("");

        if ($("#guardpartyname").val()=="") {
            status = false;
            $("#err_guardianName_bn_name_id").text("Empty field found!!");
            $("#guardpartyname").focus();
        } else $("#err_guardianName_bn_name_id").text("");
        // } else if (!cChar.test($("#guardpartyname").val())) {
        //     status = false;
        //     $("#err_guardianName_bn_name_id").text("Invalid field value found");
        //     $("#guardpartyname").focus();
        //


        if ($("#guarddateofbirth").val()=="") {
            status = false;
            $("#err_dobguardian_bn_name_id").text("Empty field found!!");
            $("#guarddateofbirth").focus();
        } else if (!cDate.test($("#guarddateofbirth").val())) {
            status = false;
            $("#err_dobguardian_bn_name_id").text("Invalid date field found!!");
            $("#guarddateofbirth").focus();
        } else $("#err_dobguardian_bn_name_id").text("");

        if ($("#guardpercentage").val() == "") {
            status = false;
            $("#err_guardian_bn_name_id").text("Empty field found!!");
            $("#guardpercentage").focus();
        } else $("#err_guardian_bn_name_id").text("");

        if ($("#guardianType").val() == "-1") {
             status = false;
             $("#err_guardian_bn_name_id").text("Empty field found!!");
             $("#guardianType").focus();
         } else $("#err_guardian_bn_name_id").text("");

        if ($("#guardpartyRelation").val() == "-1") {
            status = false;
            $("#err_guard_bn_name_id").text("Empty field found!!");
            $("#guardpartyRelation").focus();
        } else $("#err_guard_bn_name_id").text("");
        if ($("#assuredNameDropdownGuardian").val() == "-1") {
            status = false;
            $("#err_guardian_asssured_id").text("Empty field found!!");
            $("#assuredNameDropdownGuardian").focus();
        } else $("#err_guardian_asssured_id").text("");

        return status;
    }

});
//
// $(document).ready(function () {
//     $(document).on("click", "#profile-tab6", function () {
//         $.ajax({
//             type: "POST",
//             contentType: "application/json",
//             url: "/new-business/getProductObj",
//             dataType: 'json',
//             cache: false,
//             timeout: 600000,
//             success: function (data) {
//
//                 var html = '';
//                 html += '<option value="0"></option>';
//                 $.each(data, function (i, l) {
//                     html += '<option value="' + l[0] + '">'
//                         + l[1]
//                         + '</option>';
//                 });
//                 html += '</option>';
//
//                 $('#objectivecd').html(html);
//                 //alert('accss the data');
//             },
//             error: function (e) {
//                 showAlert("Something wrong!!!");
//             }
//         });
//
//     });
// });


// function getLastNextPGIDINSPPRoposal(pgid) {
//
//     var json = {
//         "officeId": ""
//     };
//
//     $.ajax({
//         type: "POST",
//         contentType: "application/json",
//         url: "/new-business/getLastNextPGID/" + pgid,
//         data: JSON
//             .stringify(json),
//         dataType: 'json',
//         cache: false,
//         timeout: 600000,
//         success: function (data) {
//             //$('#pgidss').val(data);
//         },
//         error: function (e) {
//         }
//     });
// }



// var updatePayorTable = function () {
//     var tableData = $("#payor_table_data_id");
//     masterId = $('#proposalMasterNo').val();
//     $.ajax({
//         contentType: 'application/json',
//         url: "get-proposal-payor-info",
//         type: 'POST',
//         async: false,
//         data: JSON.stringify(masterId),
//         dataType: 'json',
//         success: function (response) {
//             tableData.html("");
//             html = "";
//             $.each(response, function(idx, elem){
//                 html = "";
//                 html += "<tr>";
//                 html += "<td>" + elem.assuredName + "</td>";
//                 html += "<td>" + elem.payorName + "</td>";
//                 html += "<td>" + elem.payorDOB + "</td>";
//                 html += "<td>" + elem.relationWithAssured + "</td>";
//                 html += "<td>" + elem.payorPremium + "</td>";
//                 html += "<td>" + '<button type="button" class="btn btn-info payor_edit_button" value="Edit"  button_id="'+ elem.payorId +'"> ' +
//                     '<i class="fa fa-pencil"></i></button>' + '<button type="button" class="btn btn-danger payor_delete_button" value="Delete" button_id="'+ elem.payorId +'"> '   +
//                     '<i class="fa fa-trash"></i></button>'+ "</td>";
//                 html += "<tr>";
//                 tableData.append(html);
//             });
//         },
//         error: function (xhr, status, error) {
//         }
//     });
// };
//
// $(document).on("click", "#profile-tab3", function (event) {
//     loadAssuredDropdown();
//     updatePayorTable();
// });
//
// var loadAssuredDropdown = function () {
//     var masterId = $("#proposalMasterNo").val();
//     $.ajax({
//         contentType: 'application/json',
//         url: "get-payor-assured-list",
//         type: 'POST',
//         async: false,
//         data: JSON.stringify(masterId),
//         dataType: 'json',
//         success: function (response) {
//             var asrdList = $('#assuredNameDropdown');
//             asrdList.empty();
//             asrdList.append($('<option/>', {
//                 value: "-1",
//                 text: "--Select--"
//             }));
//             $.each(response, function (index, asrd) {
//                 asrdList.append($('<option/>', {
//                     value: asrd.id,
//                     text: asrd.displayName
//                 }));
//             });
//         },
//         error: function (xhr, status, error) {
//         }
//     });
// }
//
// var confirmPayorDelete = function (text, row_id) {
//     $.confirm({
//         title: 'Confirm!',
//         content: text,
//         buttons: {
//             confirm: {
//                 btnClass: 'btn-info',
//                 keys: ['enter'],
//                 action: function () {
//                     $.ajax({
//                         contentType: 'application/json',
//                         url: "del-proposal-payor",
//                         type: 'POST',
//                         async: false,
//                         data: JSON.stringify(row_id),
//                         dataType: 'json',
//                         success: function (response) {
//                             if (response.isDeleted) {
//                                 showAlertByType("Successfully deleted.", "S");
//                                 updatePayorTable();
//                             }
//                             else {
//                                 showAlertByType(response.errorCause, "F");
//                             }
//                         },
//                         error: function (xhr, status, error) {
//                             showAlertByType("Failed to delete payor", "F");
//                         }
//                     });
//                 }
//             },
//             cancel: function () {
//
//             }
//         }
//     });
// }
//
// function  assuredNameValid() {
//     $('#assuredNameDropdown').next("span").remove();
//
//     if ($('#assuredNameDropdown').val() == -1) {
//         $('#assuredNameDropdown').after('<span class=error>Choose an Assured</span>');
//         return false;
//     }
//     else {
//         return true;
//     }
// }
//
// function payorNameValid() {
//     $('#payorName').next("span").remove();
//
//     var nameCheck = new RegExp("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$");
//
//     if (nameCheck.test($('#payorName').val())) {
//         return true;
//     }
//     else {
//         $('#payorName').after('<span class= error>Invalid Name</span>');
//         return false;
//     }
// }
//
// function  dobValid() {
//     $('#dobPayor').next("span").remove();
//
//     var dobCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");
//     if (dobCheck.test($('#dobPayor').val())) {
//         return true;
//     }
//     else {
//         $('#dobPayor').after('<span class=error>Invalid Date</span>');
//         return false;
//     }
// }
//
// function premiumValid() {
//     $('#payorPremiumPercent').next("span").remove();
//
//     var a = $('#payorPremiumPercent').val();
//
//     if (a > 0 && a <= 100) {
//         return true;
//     }
//     else {
//         $('#payorPremiumPercent').after('<span class= error>Invalid Premium Percent</span>');
//         return false;
//     }
// }
//
// function payorRelationValid() {
//     $('#payorRelation').next("span").remove();
//
//     if ($('#payorRelation').val() == -1) {
//         $('#payorRelation').after('<span class=error>Choose a relation</span>');
//         return false;
//     }
//     else {
//         return true;
//     }
// }
//
// $('#payorName').change(function () {
//     payorNameValid();
// });
//
// $('#dobPayor').change(function () {
//     dobValid();
// });
//
// $('#payorPremiumPercent').change(function () {
//     premiumValid();
// });
//
// $('#payorRelation').change(function () {
//     var isRelValid = payorRelationValid();
//
//     if (isRelValid) {
//         if ($("#payorRelation option:selected").text() == "Self"){
//             getPayorSelf();
//         }
//         else {
//
//         }
//     }
// });
//
// $('#assuredNameDropdown').change(function () {
//     assuredNameValid();
// })
//
// function validatePayorForm() {
//     // $('span.error').remove();
//     assuredNameValid();
//     payorNameValid();
//     dobValid();
//     premiumValid();
//     payorRelationValid();
//
//     if ($("span").hasClass("error")) {
//         return false;
//     }
//     else {
//         return true;
//     }
// }
//
// function getPayorSelf() {
//     var masterId = $('#proposalMasterNo').val();
//     $.ajax({
//         contentType: 'application/json',
//         url: "get-proposal-self",
//         type: 'POST',
//         async: false,
//         data: JSON.stringify(masterId),
//         dataType: 'json',
//         success: function (response) {
//             $("#payorName").val(response.proposerName);
//             $("#dobPayor").val(response.proposerDOB);
//         },
//         error: function (xhr, status, error) {
//         }
//     });
// };
//
// // add new payor
// $("#add-new-payor").click(function (event) {
//     event.preventDefault();
//     var isVaild = validatePayorForm();
//     if(isVaild) {
//         confirmPayorSaveDialog("Are you sure to save payor?");
//     }
// });
//
// var confirmPayorSaveDialog = function (text) {
//     $.confirm({
//         title: 'Confirm!',
//         content: text,
//         buttons: {
//             confirm: {
//                 btnClass: 'btn-info',
//                 keys: ['enter'],
//                 action: function () {
//                     var proposal_payor = {};
//
//                     proposal_payor.masterId = $('#proposalMasterNo').val();
//                     proposal_payor.assuredId = $("#assuredNameDropdown").find('option:selected').val();
//                     proposal_payor.payorName = $("#payorName").val();
//                     proposal_payor.payorDOB = $("#dobPayor").val();
//                     proposal_payor.payorPremium = $("#payorPremiumPercent").val();
//                     proposal_payor.relationId = $("#payorRelation").find('option:selected').val();
//                     // proposal_payor.payorId =  $('#add-new-payor').data('id');
//                     proposal_payor.payorId =  $('#nb_payor_id').val();
//                     $.ajax({
//                         contentType: 'application/json',
//                         url:  "add-proposal-payor",
//                         type: 'POST',
//                         async: false,
//                         data: JSON.stringify(proposal_payor),
//                         dataType: 'json',
//                         success: function(response) {
//                             if(response == true){
//                                 updatePayorTable();
//                                 resetPayorForm();
//                                 showAlert("Successfully updated");
//                             }
//                             if(response == false) {
//                                 showAlert("Cannot updated due to input perchantage error");
//                             }
//                         },
//                         error: function(xhr, status, error) {
//                         }
//                     });
//                 }
//
//             },
//             cancel: function () {
//
//             }
//         }
//     });
// }
//
// $(document).on("click", ".payor_edit_button", function (event) {
//     editPayor(this);
// });
//
// $(document).on("click", ".payor_delete_button", function (event) {
//     deletePayor(this);
// });
//
// function  editPayor(identifier) {
//     var row_id = $(identifier).attr("button_id");
//     console.log("row_id: " + row_id);
//
//     $.ajax({
//         contentType: 'application/json',
//         url: "get-proposal-payor-info-dtl",
//         type: 'POST',
//         async: false,
//         data: JSON.stringify(row_id),
//         dataType: 'json',
//         success: function (response) {
//
//
//             $("#assuredNameDropdown").val(response.assuredId);
//             $("#payorName").val(response.payorName);
//             $("#dobPayor").val(response.payorDOB);
//             $("#payorPremiumPercent").val(response.payorPremium);
//             $("#payorRelation").val(response.relationId);
//             // $('#add-new-payor').data('id', row_id);
//             $('#nb_payor_id').val(row_id);
//         },
//         error: function (xhr, status, error) {
//         }
//     });
//
// }
//
// function deletePayor() {
//     // alert("Delete under progress");
// }
//
// var resetPayorForm = function () {
//     $('#assuredNameDropdown').val("-1");
//     $('#dobPayor').val("");
//     $('#payorName').val("");
//     $('#payorPremiumPercent').val('');
//     $('#payorRelation').val("-1");
//     // $('#add-new-payor').data('id',"0");
//     $('#nb_payor_id').val('0');
// };
//
// $(document).on("click", ".payor_delete_button", function (event) {
//     var row_id = $(this).attr("button_id");
//     confirmPayorDelete("Are you sure to delete this Payor?", row_id);
// });









// $(document).ready(function () {
//
//     $(document).on("click", "#profile-tab5", function (event) {
//         loadGuardianTypeDropdown();
//         loadAssuredDropdown();
//         loadNomineeDropdown();
//         updateGuardianTable();
//         guardianDropdownEvent();
//     });
//
//     $("#guardianType").change(function() {
//         guardianDropdownEvent();
//     });
//
//     function guardianDropdownEvent() {
//         var guardType = $("#guardianType").find('option:selected').val();
//         var guardTypeName = $("#guardianType").find('option:selected').text();
//
//         if (guardType == 846 || guardTypeName == 'Assured Guardian') {  // compare with text
//             $('#assuredNameDropdownGuardianId').show();
//             $('#nomineeNameDropdownGuardianId').hide();
//         }
//
//         else if (guardType == 847 || guardTypeName == 'Nominee Guardian') { // compare with text
//             $('#nomineeNameDropdownGuardianId').show();
//             $('#assuredNameDropdownGuardianId').hide();
//         }
//
//         else {
//             $('#assuredNameDropdownGuardianId').hide();
//             $('#nomineeNameDropdownGuardianId').hide();
//         }
//     }
//
//     var updateGuardianTable = function () {
//         var tableData = $("#guardian_table_data_id");
//         masterId = $('#proposalMasterNo').val();
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-proposal-guardian-info",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(masterId),
//             dataType: 'json',
//             success: function (response) {
//                 tableData.html("");
//                 html = "";
//                 $.each(response, function(idx, elem){
//
//                     var name;
//                     if (elem.assuredId != -1) { //assured available
//                         name = elem.assuredName;
//                     }
//                     else {
//                         name = elem.nomineeName;
//                     }
//
//                     html = "";
//                     html += "<tr>";
//                     html += "<td>" + name + "</td>";
//                     html += "<td>" + elem.guardianName + "</td>";
//                     html += "<td>" + elem.guardianType + "</td>";
//                     html += "<td>" + elem.guardianDob + "</td>";
//                     html += "<td>" + elem.relationWithAssuredNominee + "</td>";
//                     html += "<td>" +
//                         '<button type="button" class="btn btn-info guardian_edit_button" value="Edit" button_id="'+ elem.guardianId +'"> ' +
//                         '<i class="fa fa-pencil"></i></button>' +
//                         '<button type="button" class="btn btn-danger guardian_delete_button" value="Delete" button_id="'+ elem.guardianId +'"> '   +
//                         '<i class="fa fa-trash"></i></button>'+ "</td>";
//                     html += "<tr>";
//                     tableData.append(html);
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//     };
//
//     var loadAssuredDropdown = function () {
//         var masterId = $("#proposalMasterNo").val();
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-guardian-assured-list",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(masterId),
//             dataType: 'json',
//             success: function (response) {
//                 var asrdList = $('#assuredNameDropdownGuardian');
//                 asrdList.empty();
//                 asrdList.append($('<option/>', {
//                     value: "-1",
//                     text: "--Select Assured--"
//                 }));
//                 $.each(response, function (index, asrd) {
//                     asrdList.append($('<option/>', {
//                         value: asrd.id,
//                         text: asrd.displayName
//                     }));
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//     }
//
//     var loadGuardianTypeDropdown = function () {
//         var masterId = $("#proposalMasterNo").val();
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-guardian-type",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(masterId),
//             dataType: 'json',
//             success: function (response) {
//                 var asrdList = $('#guardianType');
//                 asrdList.empty();
//                 asrdList.append($('<option/>', {
//                     value: "-1",
//                     text: "--Select Guardian Type--"
//                 }));
//                 $.each(response, function (index, guardian) {
//                     asrdList.append($('<option/>', {
//                         value: guardian.id,
//                         text: guardian.displayName
//                     }));
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//     }
//
//     var loadNomineeDropdown = function () {
//         var masterId = $("#proposalMasterNo").val();
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-guardian-nominee-list",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(masterId),
//             dataType: 'json',
//             success: function (response) {
//                 var asrdList = $('#nomineeNameDropdownGuardian');
//                 asrdList.empty();
//                 asrdList.append($('<option/>', {
//                     value: "-1",
//                     text: "--Select Nominee--"
//                 }));
//                 $.each(response, function (index, nominee) {
//                     asrdList.append($('<option/>', {
//                         value: nominee.id,
//                         text: nominee.displayName
//                     }));
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//     }
//
//     var confirmGuardianDelete = function(text, row_id) {
//          $.confirm({
//                 title: 'Confirm!',
//                 content: text,
//                 buttons: {
//                     confirm: {
//                         btnClass: 'btn-info',
//                         keys: ['enter'],
//                         action: function () {
//                             $.ajax({
//                                 contentType: 'application/json',
//                                 url: "del-proposal-guardian",
//                                 type: 'POST',
//                                 async: false,
//                                 data: JSON.stringify(row_id),
//                                 dataType: 'json',
//                                 success: function (response) {
//                                     if (response.isDeleted) {
//                                         showAlertByType("Successfully deleted", "S");
//                                         updateGuardianTable();
//                                     }
//                                     else {
//                                         showAlertByType(response.errorCause, "F");
//                                     }
//                                 },
//                                 error: function (xhr, status, error) {
//                                     showAlertByType("Failed to delete guardian", "F");
//                                 }
//                             });
//                         }
//                     },
//                     cancel: function () {
//
//                     }
//                 }
//             });
//
//     }
//
//     $("#add-new-guardian").click(function (event) {
//         event.preventDefault();
//         var isVaild = validateGuardianForm();
//         if(isVaild) {
//             confirmGuardianSaveDialog("Are you sure to save this Guardian?");
//         }
//     });
//
//     function validateGuardianForm() {
//         $('span.error').remove();
//
//         guardTypeValid();
//         assuredValid();
//         nomineeValid();
//         guardnameValid();
//         dobValid();
//         guardRelationValid();
//
//         if ($("span").hasClass("error")) {
//             return false;
//         }
//         else {
//             return true;
//         }
//     }
//
//     function  guardTypeValid() {
//         $('#guardianType').next("span").remove();
//
//         if ($('#guardianType').val() == -1) {
//             $('#guardianType').after('<span class=error>Choose a type</span>');
//             return false;
//         }
//         else {
//             return true;
//         }
//     }
//
//     function  assuredValid() {
//         if ($("#assuredNameDropdownGuardianId").is(":visible")) {
//             $('#assuredNameDropdownGuardian').next("span").remove();
//
//             if ($('#assuredNameDropdownGuardian').val() == -1) {
//                 $('#assuredNameDropdownGuardian').after('<span class=error>Choose an assured</span>');
//                 return false;
//             }
//             else {
//                 return true;
//             }
//         }
//         else {
//             return true;
//         }
//     }
//
//     function nomineeValid() {
//         if ($("#nomineeNameDropdownGuardianId").is(":visible")) {
//             $('#nomineeNameDropdownGuardian').next("span").remove();
//
//             if ($('#nomineeNameDropdownGuardian').val() == -1) {
//                 $('#nomineeNameDropdownGuardian').after('<span class=error>Choose a nominee</span>');
//                 return false;
//             }
//             else {
//                 return true;
//             }
//         }
//         else {
//             return true;
//         }
//     }
//
//     function guardnameValid() {
//         $('#guardianName').next("span").remove();
//
//         var nameCheck = new RegExp("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$");
//
//         if (nameCheck.test($('#guardianName').val())) {
//             return true;
//         }
//         else {
//             $('#guardianName').after('<span class= error>Invalid Name</span>');
//             return false;
//         }
//     }
//
//     function dobValid() {
//         $('#dobGuardian').next("span").remove();
//
//         var dobCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");
//         if (dobCheck.test($('#dobGuardian').val())) {
//             return true;
//         }
//         else {
//             $('#dobGuardian').after('<span class=error>Invalid Date</span>');
//             return false;
//         }
//     }
//
//     function guardRelationValid() {
//         $('#guardianRelation').next("span").remove();
//
//         if ($('#guardianRelation').val() == -1) {
//             $('#guardianRelation').after('<span class=error>Choose a relation</span>');
//             return false;
//         }
//         else {
//             return true;
//         }
//     }
//
//     $('#guardianType').change(function () {
//         guardTypeValid();
//     });
//
//     $('#assuredNameDropdownGuardian').change(function () {
//         assuredValid();
//     });
//
//     $('#nomineeNameDropdownGuardian').change(function () {
//         nomineeValid();
//     });
//
//     $('#guardianName').change(function () {
//         guardnameValid();
//     });
//
//     $('#dobGuardian').change(function () {
//         dobValid();
//     });
//
//     $('#guardianRelation').change(function () {
//         guardRelationValid();
//     });
//
//     var resetGuardianForm = function () {
//         $('#guardianType').val("-1");
//         $('#assuredNameDropdownGuardian').val("-1");
//         $('#nomineeNameDropdownGuardian').val("-1");
//         $('#dobGuardian').val("");
//         $('#guardianName').val("");
//         $('#guardianRelation').val("-1");
//         // $('#add-new-guardian').data('id',"0");
//         $('#nb_guardian_id').val('0')
//
//         guardianDropdownEvent();
//     };
//
//     var confirmGuardianSaveDialog = function (text) {
//         $.confirm({
//             title: 'Confirm!',
//             content: text,
//             buttons: {
//                 confirm: {
//                     btnClass: 'btn-info',
//                     keys: ['enter'],
//                     action: function () {
//                         var proposal_guardian = {};
//
//                         proposal_guardian.masterId = $('#proposalMasterNo').val();
//                         proposal_guardian.assuredId = $("#assuredNameDropdownGuardian").find('option:selected').val();
//                         proposal_guardian.nomineeId = $("#nomineeNameDropdownGuardian").find('option:selected').val();
//                         proposal_guardian.guardianName = $("#guardianName").val();
//                         proposal_guardian.guardianTypeId = $("#guardianType").find('option:selected').val();
//                         proposal_guardian.guardianDob = $("#dobGuardian").val();
//                         proposal_guardian.relationId = $("#guardianRelation").val();
//
//                         // proposal_guardian.guardianId = $('#add-new-guardian').data('id');
//                         proposal_guardian.guardianId = $('#nb_guardian_id').val();
//
//                         $.ajax({
//                             contentType: 'application/json',
//                             url:  "add-proposal-guardian",
//                             type: 'POST',
//                             async: false,
//                             data: JSON.stringify(proposal_guardian),
//                             dataType: 'json',
//                             success: function(response) {
//                                 updateGuardianTable();
//                                 resetGuardianForm();
//                                 showAlert("Successfully updated");
//                             },
//                             error: function(xhr, status, error) {
//                             }
//                         });
//                     }
//
//                 },
//                 cancel: function () {
//
//                 }
//             }
//         });
//     };
//
//     $(document).on("click", ".guardian_edit_button", function (event) {
//         editGuardian(this);
//     });
//
//     $(document).on("click", ".guardian_delete_button", function (event) {
//         var row_id = $(this).attr("button_id");
//         confirmGuardianDelete("Are you sure to delete this Guardian?", row_id);
//         // deleteGuardian(this);
//     });
//
//     function  deleteGuardian(identifier) {
//      alert("Delete under progress");
//     }
//
//     function  editGuardian(identifier) {
//         var row_id = $(identifier).attr("button_id");
//         console.log("row_id: " + row_id);
//
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-proposal-guardian-info-dtl",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(row_id),
//             dataType: 'json',
//             success: function (response) {
//
//                 $("#guardianType").val(response.guardianTypeId);
//                 $("#guardianName").val(response.guardianName);
//                 $("#dobGuardian").val(response.guardianDob);
//                 $("#guardianRelation").val(response.relationId);
//                 // $('#add-new-guardian').data('id', row_id);
//
//                 $('#nb_guardian_id').val(row_id);
//
//                 guardianDropdownEvent();
//                 if (response.nomineeId != null) {
//                     $("#nomineeNameDropdownGuardian").val(response.nomineeId);
//                 }
//                 else if (response.assuredId != null) {
//                     $("#assuredNameDropdownGuardian").val(response.assuredId);
//                 }
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//
//
//     }
//
//
//
// });