/**
 * Created by User on 3/24/2019.
 */

$(document).ready(function () {

    $("#add-client-p_plicy").click(function () {

        var flag=ValidatingData();

        if(flag== true){

            var proPolicy = {};

           // proPolicy.policyPGID=$("#pgidss").val();
            proPolicy.policyPARTY_CD=$("#cl_party_id").val();

            proPolicy.cl_policy_id = $("#cl_policy_id").val();
            proPolicy.policyRISK_DATE = $("#riskDate").val();
            proPolicy.policyPOLICY_NO = $("#policyNo").val();
            proPolicy.policyPOLICY_DESC = $("#pDescription").val();
            proPolicy.policyTERM = $("#proPTerm").val();
            proPolicy.policySUM_ASSURED = $("#sumPAssured").val();
            proPolicy.policyREMARKS = $("#pRemark").val();

              // alert(proPolicy.policyPGID);
              // alert(proPolicy.policyPARTY_CD);
              // alert(proPolicy.cl_policy_id);
              // alert(proPolicy.policyRISK_DATE);
              // alert(proPolicy.policyPOLICY_NO);
              // alert(proPolicy.policyPOLICY_DESC);
              // alert(proPolicy.policyTERM);
              // alert(proPolicy.policySUM_ASSURED);
              // alert(proPolicy.policyREMARKS);

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/new-business/add-policy",
                data: JSON.stringify(proPolicy),
                dataType: 'json',
                success: function (data) {
                    // alert(data);
                    updateTableList();
                    showAlert("Successfully Saved!");
                    refresh();
                },
                error: function (e) {
                    showAlert("Something Wrong!!!");
                }
            });
        }
    });

    function refresh (){
        // $('#cl_party_id').val("-1");
        $('#cl_policy_id').val("-1");
        $('#riskDate').val("");
        $('#pDescription').val("");
        $('#proPTerm').val("");
        $('#policyNo').val("");
        $('#sumPAssured').val("");
        $('#pRemark').val("");
    }

    function updateTableList() {

        // alert("hi");
        var policyTable = $('#policyTableId');
        // alert("hlw");
        $.ajax({
            type: "GET",
            url: "/new-business/addPolicyList",
            success: function (data) {
                //  alert(data);
                var no = 1;
                var tableBody = "";

                $('#policyTableId tbody').empty();
                $.each(data, function (idx, elem) {
                    // alert (elem[idx]);

                    var edit = '<a class="btn btn-success policyInformationEdit" value="Edit" id="editPolicy"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger policyInformationDelete" value="Delete" id="deletePolicy"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td hidden class='pp-party-type'>" + elem[2] + "</td>";
                    tableBody += "<td >" + elem[3] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td>" + elem[5] + "</td>";
                    tableBody += "<td>" + getFormatedDate(elem[6]) + "</td>";
                    tableBody += "<td>" + elem[7] + "</td>";
                    tableBody += "<td>" + elem[8] + "</td>";
                    tableBody += "<td>" + elem[9] + "</td>";
                    tableBody += "<td>" + edit + "</td>";
                    tableBody += "<td>" + del + "</td>"
                    tableBody += "<tr>";
                });
                policyTable.append(tableBody);
            }
        });
    }

    function getFormatedDate(strDate) {

        var dateS = new Date(strDate);

        var d = dateS.getDate();
        var m = dateS.getMonth() + 1;
        var y = dateS.getFullYear();


        // return y+'-'+(m <= 9 ? '0' + m : m)+'-'+(d <= 9 ? '0' + d : d);
        return (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + y;

    }
    $( "#riskDate" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });


    function ValidatingData() {

        var status = true;
        var cDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        //var cDigit = /^[0-9]+$/;
        var cDigit = /^[0-9]+$/;
        if ($("#riskDate").val() == "") {

        if ($("#cl_policy_id").val() == "-1") {
            status = false;
            $("#err_company_p_bn_code_id").text("Empty field found!!");
            $("#cl_policy_id").focus();
        } else $("#err_company_p_bn_code_id").text("");

        // if ($("#riskDate").val() == "") {
        //     status = false;
        //     $("#err_riskDate_bn_dob_id").text("Empty field found!!");
        //     $("#riskDate").focus();
        // } else if (!cDate.test($("#riskDate").val())) {
        //     status = false;
        //     $("#err_riskDate_bn_dob_id").text("Invalid date format found!!");
        //     $("#riskDate").focus();
        // } else $("#err_riskDate_bn_dob_id").text("");

        if ($("#policyNo").val() == "") {
            status = false;
            $("#err_policyNo_bn_name_id").text("Empty field found!!");
            $("#policyNo").focus();
        } else if (!cDigit.test($("#policyNo").val())) {
            status = false;
            $("#err_policyNo_bn_name_id").text("Invalid field value found!!");
            $("#policyNo").focus();
        } else $("#err_policyNo_bn_name_id").text("");

        if ($("#proPTerm").val()==""){
            status = false;
            $("#err_proPTerm").text("Empty field found!!");
            $("#policyNo").focus();
        }else if ($("#proPTerm").val().length>2) {
            status = false;
            $("#err_proPTerm").text("Not suitable length found!!");
            $("#policyNo").focus();
        } else if (!cDigit.test($("#proPTerm").val())) {
            status = false;
            $("#err_proPTerm").text("Invalid field value found!!");
            $("#proPTerm").focus();
        } else $("#err_proPTerm").text("");
          if ($("#sumPAssured").val()==""){
              status = false;
              $("#err_sumPAssured_bn_sum_id").text("Empty field found!!");
              $("#sumPAssured").focus();
        }else if (!cDigit.test($("#sumPAssured").val())) {
            status = false;
            $("#err_sumPAssured_bn_sum_id").text("Invalid field value found!!");
            $("#sumPAssured").focus();
        } else $("#err_sumPAssured_bn_sum_id").text("");
          
        if ($("#cl_party_id").val()== "-1") {
            status = false;
            $("#err_cl_party_id").text("Empty field found!!");
            $("#cl_party_id").focus();
        } else $("#err_cl_party_id").text("");

        return status;
    }else {

            if ($("#cl_policy_id").val() == "-1") {
                status = false;
                $("#err_company_p_bn_code_id").text("Empty field found!!");
                $("#cl_policy_id").focus();
            } else $("#err_company_p_bn_code_id").text("");

           if (!cDate.test($("#riskDate").val())) {
                status = false;
                $("#err_riskDate_bn_dob_id").text("Invalid date format found!!");
                $("#riskDate").focus();
            } else $("#err_riskDate_bn_dob_id").text("");

            if ($("#policyNo").val() == "") {
                status = false;
                $("#err_policyNo_bn_name_id").text("Empty field found!!");
                $("#policyNo").focus();
            } else if (!cDigit.test($("#policyNo").val())) {
                status = false;
                $("#err_policyNo_bn_name_id").text("Invalid field value found!!");
                $("#policyNo").focus();
            } else $("#err_policyNo_bn_name_id").text("");

            if ($("#proPTerm").val()==""){
                status = false;
                $("#err_proPTerm").text("Empty field found!!");
                $("#policyNo").focus();
            }else if ($("#proPTerm").val().length>2) {
                status = false;
                $("#err_proPTerm").text("Not suitable length found!!");
                $("#policyNo").focus();
            } else if (!cDigit.test($("#proPTerm").val())) {
                status = false;
                $("#err_proPTerm").text("Invalid field value found!!");
                $("#proPTerm").focus();
            } else $("#err_proPTerm").text("");
            if ($("#sumPAssured").val()==""){
                status = false;
                $("#err_sumPAssured_bn_sum_id").text("Empty field found!!");
                $("#sumPAssured").focus();
            }else if (!cDigit.test($("#sumPAssured").val())) {
                status = false;
                $("#err_sumPAssured_bn_sum_id").text("Invalid field value found!!");
                $("#sumPAssured").focus();
            } else $("#err_sumPAssured_bn_sum_id").text("");

            if ($("#cl_party_id").val()== "-1") {
                status = false;
                $("#err_cl_party_id").text("Empty field found!!");
                $("#cl_party_id").focus();
            } else $("#err_cl_party_id").text("");

            return status;

        }
    }


    //$('#policyTableId tbody').on('click', '#deletePolicy', function () {
        $(document).on("click", "#deletePolicy", function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(4)').text();

        $.confirm({
            title: 'Confirm',
             content: 'Are you sure?',
             buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/previous-policy-info-delete/" + col1 +"/"+col2+"/"+col3,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                                refresh();
                                showAlert("Successfully deleted");
                              $('#policyTableId tbody tr').empty();
                               updateTableList();

                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }
            }
         });

    });

    $(document).on("click", "#policyTableId tbody tr", function() {

        var curRow = $(this).closest('tr');

        var pgid = curRow.find('td:eq(0)').text();
        var party = curRow.find('td:eq(1)').text();
        var cl_policy_id = curRow.find('td:eq(2)').text();
        var policyPOLICY_NO = curRow.find('td:eq(4)').text();
        var policyPOLICY_DESC = curRow.find('td:eq(5)').text();
        var policyRISK_DATE = curRow.find('td:eq(6)').text();
        var policyTERM = curRow.find('td:eq(7)').text();
        var policySUM_ASSURED = curRow.find('td:eq(8)').text();
        var policyREMARKS = curRow.find('td:eq(9)').text();



        $('#pgid').val(pgid);
        $('#cl_policy_id').val(( $(this).find(".pp-party-type").html()));
        $('#policyNo').val(policyPOLICY_NO);
        $('#pDescription').val(policyPOLICY_DESC);
        $('#riskDate').val(policyRISK_DATE);
        $('#proPTerm').val(policyTERM);
        $('#sumPAssured').val(policySUM_ASSURED);
        $('#pRemark').val(policyREMARKS);


    });

});



//
// $(document).ready(function () {
//     var validationClientFamily = function () {
//         var isValid = true;
//         $(".cl_family_form .err_msg").text("");
//         $(".cl_family_form input.mand").each(function(){
//             if(this.value.length == 0) {
//                 $("#err_" + this.id ).text("Please enter the value");
//                 $(this.id ).focus();
//                 isValid = false;
//             }
//         });
//         $(".cl_family_form select.mand").each(function(){
//             if(this.value == -1) {
//                 $("#err_" + this.id ).text("Please select the value");
//                 $(this.id ).focus();
//                 isValid = false;
//             }
//         });
//         return isValid;
//     };
//
//     var resetClientFamilyForm = function () {
//         $('#cl_fam_id').val("0");
//         $('#cl_fam_relation_id').val("-1");
//         $('#cl_fam_age_id').val("");
//         $('#cl_fam_phy_id').val("");
//         $('#cl_fam_age_death_id').val("");
//         $('#cl_fam_cause_death_id').val("");
//         $('#cl_fam_yr_death_id').val("");
//         $('#cl_fam_remarks_id').val("");
//
//     };
//
//     var updateClientFamilyTable = function () {
//         var tableData = $("#cl_family_table_data_id");
//         masterId = $('#proposalMasterNo').val();
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-client-family-info",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(masterId),
//             dataType: 'json',
//             success: function (response) {
//                 console.log("here client family info table2");
//                 tableData.html("");
//                 html = "";
//                 $.each(response, function(idx, elem){
//                     var ageDeath = (elem.causeDeath == null) ? "N/A" : elem.causeDeath;
//                     var causeDeath = (elem.causeDeath == null) ? "N/A" : elem.causeDeath;
//                     var yearDeath = (elem.yearDeath == null) ? "N/A" : elem.yearDeath;
//                     var remarks = (elem.remarks == null) ? "N/A" : elem.remarks;
//
//                     html = "";
//                     html += "<tr>";
//                     html += "<td>" + elem.relationName + "</td>";
//                     html += "<td>" + elem.age + "</td>";
//                     html += "<td>" + elem.phyCondition + "</td>";
//                     html += "<td>" + ageDeath + "</td>";
//                     html += "<td>" + causeDeath + "</td>";
//                     html += "<td>" + yearDeath + "</td>";
//                     html += "<td>" + remarks + "</td>";
//                     html += "<td>" + '<button type="button" class="btn  btn-sm btn-info cl_fam_edit_button" value="Edit" button_id="'+ elem.id +'"> ' +
//                         '<i class="fa fa-pencil"></i></button>' + '<button type="button" class="btn  btn-sm btn-danger cl_fam_delete_button" value="Delete" button_id="'+ elem.id +'"> '   +
//                         '<i class="fa fa-trash"></i></button>'+ "</td>";
//                     html += "<tr>";
//                     tableData.append(html);
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//
//
//
//     };
//
//     updateClientFamilyTable();
//
//     var confirmClientFamilyDialog = function (text) {
//         $.confirm({
//             title: 'Confirm!',
//             content: text,
//             buttons: {
//                 confirm: {
//                     btnClass: 'btn-info',
//                     keys: ['enter'],
//                     action: function () {
//                         var proposal_client_family = {};
//
//                         proposal_client_family.masterId = $('#proposalMasterNo').val();
//                         proposal_client_family.id = $('#cl_fam_id').val();
//                         proposal_client_family.relationId = $('#cl_fam_relation_id').val();
//                         proposal_client_family.age = $('#cl_fam_age_id').val();
//                         proposal_client_family.phyCondition = $('#cl_fam_phy_id').val();
//                         proposal_client_family.ageDeath = $('#cl_fam_age_death_id').val();
//                         proposal_client_family.causeDeath = $('#cl_fam_cause_death_id').val();
//                         proposal_client_family.yearDeath = $('#cl_fam_yr_death_id').val();
//                         proposal_client_family.remarks = $('#cl_fam_remarks_id').val();
//
//                         $.ajax({
//                             contentType: 'application/json',
//                             url: "add-client-family-info",
//                             type: 'POST',
//                             async: false,
//                             data: JSON.stringify(proposal_client_family),
//                             dataType: 'json',
//                             success: function (response) {
//                                 console.log(proposal_client_family);
//                                 console.log("here client family");
//                                 updateClientFamilyTable();
//                                 resetClientFamilyForm();
//                                 showAlert("Successfully updated");
//                             },
//                             error: function (xhr, status, error) {
//                             }
//                         });
//                     }
//
//                 },
//                 cancel: function () {
//
//                 }
//             }
//         });
//     };
//
//
//     $("#cl_family_save_button").click(function (event) {
//         event.preventDefault();
//         var isVaild = validationClientFamily();
//         if(isVaild) {
//             confirmClientFamilyDialog("Are you sure to save Family history?");
//         }
//     });
//     //edit button
//     $(document).on("click", ".cl_fam_edit_button", function (event) {
//         var row_id = $(this).attr("button_id");
//         console.log("row_id: " + row_id);
//
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-client-family-info-dtl",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(row_id),
//             dataType: 'json',
//             success: function (response) {
//                 console.log("here client family info dtl");
//                 $('#cl_fam_id').val(response.id);
//                 $('#cl_fam_relation_id').val(response.relationId);
//                 $('#cl_fam_age_id').val(response.age);
//                 $('#cl_fam_phy_id').val(response.phyCondition);
//                 $('#cl_fam_age_death_id').val(response.ageDeath);
//                 $('#cl_fam_cause_death_id').val(response.causeDeath);
//                 $('#cl_fam_yr_death_id').val(response.yearDeath);
//                 $('#cl_fam_remarks_id').val(response.remarks);
//
//
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//
//     });
//
//
// });