function parseNumber(str) {
    if(str==null|| str.trim()==""||str=="undefined"){
        return null;
    }
    else{
        var  intNum=parseInt(str);

        return intNum=="NaN"?null:intNum;
    }

}

$(document).ready(function () {

    $("#add-new-payor").click(function () {

        var flag=ValidatingData();

        if(flag== true){

            //var pgidss =  $("#pgidss").val();
            var assuredNameDropdown = $("#assuredNameDropdown").val();
            var payorpartyname = $("#payorpartyname").val();
            var payordateofbirth = $("#payordateofbirth").val();
            var payorpercentage = $("#payorpercentage").val();
            var payorpartyRelation = $("#payorpartyRelation").val();
            var payorserialNo =parseNumber($("#sl_no").val());

            var proposalPayor = {};

            //proposalPayor.payorpgidss=pgidss;
            proposalPayor.payorassuredid = assuredNameDropdown;
            proposalPayor.payorpartyname = payorpartyname;
            proposalPayor.payordateofbirth = payordateofbirth;
            proposalPayor.payorpercentage = payorpercentage;
            proposalPayor.payorpartyRelation = payorpartyRelation;
            proposalPayor.payorserialNo = payorserialNo;

              //alert(proposalPayor.payorpgidss);
             // alert(proposalPayor.payorpartyname);
             // alert(proposalPayor.payordateofbirth);
             // alert(proposalPayor.payorpercentage);
              //alert(proposalPayor.payorpartyRelation);

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/new-business/add-payor",
                data: JSON.stringify(proposalPayor),
                dataType: 'json',
                success: function (data) {
                    // alert(data);
                    updateTableList();
                    showAlert("Successfully Saved!");
                    refresh();
                },
                error: function (e) {
                    showAlert("Something Wrong!!!");
                }
            });
        }
    });

    function refresh (){

        $('#assuredNameDropdown').val("-1");
        $('#payorpartyname').val("");
        $('#payordateofbirth').val("");
        $('#payorpercentage').val("");
        $('#payorpartyRelation').val("-1");
        $('#pgid').val("");
        $('#sl_no').val("");
        $('#p_partycd').val("");

    }

    $( "#payordateofbirth" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });

    $("#chkboxPy").click(function () {

        if ($(this).is(":checked")) {
            var assured=$('#proposalName').val();
            var assured_date=$('#proposerDOB').val();
            var assured_percg=100;
            var assured_rel=11;

            $('#payorpartyname').val(assured);
            $('#payordateofbirth').val(assured_date);
            $('#payorpercentage').val(assured_percg);
            $('#payorpartyRelation').val(assured_rel);

        } else {


            //payor
            $('#payorpartyname').val("");
            $('#payordateofbirth').val("");
            $('#payorpercentage').val("");
            $('#payorpartyRelation').val("-1");
            // $("#chkbox").hide();
        }
    });



    function ValidatingData() {

        var status = true;
        var cDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        var cDigit = /^[0-9]+$/;

        if ($("#payorpartyname").val() == "") {
            status = false;
            $("#err_payor_bn_name_id").text("Empty field found!!");
            $("#payorpartyname").focus();
        } else $("#err_payor_bn_name_id").text("");

        if ($("#payordateofbirth").val()=="") {
            status = false;
            $("#err_dobpayor_bn_name_id").text("Empty field found!!");
            $("#payordateofbirth").focus();
        }else if (!cDate.test($("#payordateofbirth").val())) {
            status = false;
            $("#err_dobpayor_bn_name_id").text("Invalid Date format found!!");
            $("#payordateofbirth").focus();
        } else $("#err_dobpayor_bn_name_id").text("");

        // if ($("#payorpercentage").val() == "") {
        //     status = false;
        //     $("#err_payorpercent_bn_name_id").text("Empty field found!!");
        //     $("#payorpercentage").focus();
        // } else $("#err_payorpercent_bn_name_id").text("");

        if ($("#payorpercentage").val() == "") {
            status = false;
            $("#err_payorpercent_bn_name_id").text("Empty field found!!");
            $("#payorpercentage").focus();
        } else if (!cDigit.test($("#payorpercentage").val())){
            status = false;
            $('#err_payorpercent_bn_name_id').text("Only Digit allow !!");
            $('#payorpercentage').focus();
        } else $('#err_payorpercent_bn_name_id').text("");

        if ($("#payorpartyRelation").val() == "-1") {
            status = false;
            $("#err_payorrel_bn_name_id").text("Empty field found!!");
            $("#payorpartyRelation").focus();
        } else $("#err_payorrel_bn_name_id").text("");
        if ($("#assuredNameDropdown").val() == "-1") {
            status = false;
            $("#err_assuredPay_bn_name_id").text("Empty field found!!");
            $("#assuredNameDropdown").focus();
        } else $("#err_assuredPay_bn_name_id").text("");

        return status;
    }


    function getFormatedDate(strDate) {

        var dateS = new Date(strDate);

        var d = dateS.getDate();
        var m = dateS.getMonth() + 1;
        var y = dateS.getFullYear();


        // return y+'-'+(m <= 9 ? '0' + m : m)+'-'+(d <= 9 ? '0' + d : d);
        return (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + y;

    }

    function updateTableList() {
        //alert("hi");
        var payorTable = $('#payorTableId');

        $.ajax({
            type: "GET",
            url: "/new-business/addPayorList",
            success: function (data) {
               // alert(data);
                var no = 1;
                var tableBody = "";

                $('#payorTableId tbody').empty();
                $.each(data, function (idx, elem) {
                    //alert (elem[idx]);

                    var edit = '<a class="btn btn-success payorInformationEdit" value="Edit" id="editPayor"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger payorInformationEdit" value="Delete" id="deletePayor"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";
                    // tableBody += "<td>" + elem[2] + "</td>";
                    // tableBody += "<td>" + elem[4] + "</td>";
                    // tableBody += "<td>" + elem[6] + "</td>";
                    // tableBody += "<td>" + elem[8] + "</td>";
                    // tableBody += "<td>" + elem[9] + "</td>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td hidden>" + elem[2] + "</td>";
                    tableBody += "<td hidden class='nb-party-type'>" + elem[3] + "</td>";
                    // tableBody += "<td>" + elem[3] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td>" + getFormatedDate(elem[5]) + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + elem[7] + "</td>";
                    tableBody += "<td>" + edit + "</td>";
                    tableBody += "<td>" + del + "</td>"
                    tableBody += "<tr>";
                });
                payorTable.append(tableBody);
            }
        });
    }

    $(document).on("click", "#deletePayor", function() {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
       // alert(col1);
       // alert(col2);
       // alert(col3);
        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to Delete?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/payor-proposal-info-delete/" + col1+"/"+ col2+"/"+col3,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {
                            refresh2();
                           // alert("hi..");
                            showAlert("Successfully deleted");
                            $('#payorTableId tbody tr').empty();
                            updateTableList();


                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });


    function refresh2(){

        $('#assuredNameDropdown').val("-1");
        $('#payorpartyname').val("");
        $('#payordateofbirth').val("");
        $('#payorpercentage').val("");
        $('#payorpartyRelation').val("-1");
        $('#pgid').val("");
        $('#sl_no').val("");
        $('#p_partycd').val("");

    }


});



$(document).on("click", "#payorTableId tbody tr", function() {

    var curRow = $(this).closest('tr');

    var pgid = curRow.find('td:eq(0)').text();
    var sl_no = curRow.find('td:eq(1)').text();
    var party_cd = curRow.find('td:eq(2)').text();
    var party = curRow.find('td:eq(3)').text();
    var payorpartyname = curRow.find('td:eq(4)').text();
    var payordateofbirth = curRow.find('td:eq(5)').text();
    var  payorpercentage = curRow.find('td:eq(6)').text();
    var  payorpartyRelation = curRow.find('td:eq(7)').text();

    $('#pgid').val(pgid);
    $('#sl_no').val(sl_no);
    $('#p_partycd').val(party_cd);
   // $('#assuredNameDropdown').val(assuredName);
    $('#assuredNameDropdown').val(( $(this).find(".nb-party-type").html()));
    $('#payorpartyname').val(payorpartyname);
    $('#payordateofbirth').val(payordateofbirth);
    $('#payorpercentage').val(payorpercentage);
    $('#payorpartyRelation').val(payorpartyRelation);

    //$('#nb_report_type_id').val( $(this).find(".nb-report-type").html());

});

$( "#payordateofbirth" ).datepicker({
    changeMonth: true,
    changeYear: true
});

$(document).on("click", "#profile-tab3", function (event) {

    loadAssuredDropdown();

  // updatePayorTable();
});

var loadAssuredDropdown = function () {

   // var pgId = $("#pgid").val();
    //alert(pgId);

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/new-business/get-payor-assured",
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
             // alert(data);
            var html = '';
            html += '<option value="-1">--Select Assured--</option>';
            $.each(data, function (i, l) {
                html += '<option value="' + l[0] + '">'
                    + l[1]
                    + '</option>';
            });
            html += '</option>';

            $('#assuredNameDropdown').html(html);
            //alert('accss the data');
        },
        error: function (e) {
            showAlert("Something wrong!!!");
        }
    });
}


//
// function getAssuredNameInfo() {
//
//     var pgid = $('#pgid').val();
//
//     var json = {
//         "pgid": pgid
//     };
//
//     $.ajax({
//         type: "POST",
//         contentType: "application/json",
//         url: "/new-business/get-payor-assured"+pgid,
//         data: JSON
//             .stringify(json),
//         dataType: 'json',
//         cache: false,
//         timeout: 600000,
//         success: function (data) {
//             alert(data);
//             var html = '';
//             html += '<option value="0"></option>';
//             $.each(data, function (i, l) {
//                 html += '<option value="' + l[0] + '">'
//                     + l[1]
//                     + '</option>';
//             });
//             html += '</option>';
//
//             $('#assuredNameDropdown').html(html);
//             //alert('accss the data');
//
//         },
//         error: function (e) {
//
//         }
//
//     });
//
// }

// function getLastNextPGIDINSPPRoposal(pgid) {
//
//     var json = {
//         "officeId": ""
//     };
//
//     $.ajax({
//         type: "POST",
//         contentType: "application/json",
//         url: "/new-business/getLastNextPGID/" + pgid,
//         data: JSON
//             .stringify(json),
//         dataType: 'json',
//         cache: false,
//         timeout: 600000,
//         success: function (data) {
//             //$('#pgidss').val(data);
//         },
//         error: function (e) {
//         }
//     });
// }



// var updatePayorTable = function () {
    //     var tableData = $("#payor_table_data_id");
    //     masterId = $('#proposalMasterNo').val();
    //     $.ajax({
    //         contentType: 'application/json',
    //         url: "get-proposal-payor-info",
    //         type: 'POST',
    //         async: false,
    //         data: JSON.stringify(masterId),
    //         dataType: 'json',
    //         success: function (response) {
    //             tableData.html("");
    //             html = "";
    //             $.each(response, function(idx, elem){
    //                 html = "";
    //                 html += "<tr>";
    //                 html += "<td>" + elem.assuredName + "</td>";
    //                 html += "<td>" + elem.payorName + "</td>";
    //                 html += "<td>" + elem.payorDOB + "</td>";
    //                 html += "<td>" + elem.relationWithAssured + "</td>";
    //                 html += "<td>" + elem.payorPremium + "</td>";
    //                 html += "<td>" + '<button type="button" class="btn btn-info payor_edit_button" value="Edit"  button_id="'+ elem.payorId +'"> ' +
    //                     '<i class="fa fa-pencil"></i></button>' + '<button type="button" class="btn btn-danger payor_delete_button" value="Delete" button_id="'+ elem.payorId +'"> '   +
    //                     '<i class="fa fa-trash"></i></button>'+ "</td>";
    //                 html += "<tr>";
    //                 tableData.append(html);
    //             });
    //         },
    //         error: function (xhr, status, error) {
    //         }
    //     });
    // };
    //
    // $(document).on("click", "#profile-tab3", function (event) {
    //     loadAssuredDropdown();
    //     updatePayorTable();
    // });
    //
    // var loadAssuredDropdown = function () {
    //     var masterId = $("#proposalMasterNo").val();
    //     $.ajax({
    //         contentType: 'application/json',
    //         url: "get-payor-assured-list",
    //         type: 'POST',
    //         async: false,
    //         data: JSON.stringify(masterId),
    //         dataType: 'json',
    //         success: function (response) {
    //             var asrdList = $('#assuredNameDropdown');
    //             asrdList.empty();
    //             asrdList.append($('<option/>', {
    //                 value: "-1",
    //                 text: "--Select--"
    //             }));
    //             $.each(response, function (index, asrd) {
    //                 asrdList.append($('<option/>', {
    //                     value: asrd.id,
    //                     text: asrd.displayName
    //                 }));
    //             });
    //         },
    //         error: function (xhr, status, error) {
    //         }
    //     });
    // }
    //
    // var confirmPayorDelete = function (text, row_id) {
    //     $.confirm({
    //         title: 'Confirm!',
    //         content: text,
    //         buttons: {
    //             confirm: {
    //                 btnClass: 'btn-info',
    //                 keys: ['enter'],
    //                 action: function () {
    //                     $.ajax({
    //                         contentType: 'application/json',
    //                         url: "del-proposal-payor",
    //                         type: 'POST',
    //                         async: false,
    //                         data: JSON.stringify(row_id),
    //                         dataType: 'json',
    //                         success: function (response) {
    //                             if (response.isDeleted) {
    //                                 showAlertByType("Successfully deleted.", "S");
    //                                 updatePayorTable();
    //                             }
    //                             else {
    //                                 showAlertByType(response.errorCause, "F");
    //                             }
    //                         },
    //                         error: function (xhr, status, error) {
    //                             showAlertByType("Failed to delete payor", "F");
    //                         }
    //                     });
    //                 }
    //             },
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    // }
    //
    // function  assuredNameValid() {
    //     $('#assuredNameDropdown').next("span").remove();
    //
    //     if ($('#assuredNameDropdown').val() == -1) {
    //         $('#assuredNameDropdown').after('<span class=error>Choose an Assured</span>');
    //         return false;
    //     }
    //     else {
    //         return true;
    //     }
    // }
    //
    // function payorNameValid() {
    //     $('#payorName').next("span").remove();
    //
    //     var nameCheck = new RegExp("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$");
    //
    //     if (nameCheck.test($('#payorName').val())) {
    //         return true;
    //     }
    //     else {
    //         $('#payorName').after('<span class= error>Invalid Name</span>');
    //         return false;
    //     }
    // }
    //
    // function  dobValid() {
    //     $('#dobPayor').next("span").remove();
    //
    //     var dobCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");
    //     if (dobCheck.test($('#dobPayor').val())) {
    //         return true;
    //     }
    //     else {
    //         $('#dobPayor').after('<span class=error>Invalid Date</span>');
    //         return false;
    //     }
    // }
    //
    // function premiumValid() {
    //     $('#payorPremiumPercent').next("span").remove();
    //
    //     var a = $('#payorPremiumPercent').val();
    //
    //     if (a > 0 && a <= 100) {
    //         return true;
    //     }
    //     else {
    //         $('#payorPremiumPercent').after('<span class= error>Invalid Premium Percent</span>');
    //         return false;
    //     }
    // }
    //
    // function payorRelationValid() {
    //     $('#payorRelation').next("span").remove();
    //
    //     if ($('#payorRelation').val() == -1) {
    //         $('#payorRelation').after('<span class=error>Choose a relation</span>');
    //         return false;
    //     }
    //     else {
    //         return true;
    //     }
    // }
    //
    // $('#payorName').change(function () {
    //     payorNameValid();
    // });
    //
    // $('#dobPayor').change(function () {
    //     dobValid();
    // });
    //
    // $('#payorPremiumPercent').change(function () {
    //     premiumValid();
    // });
    //
    // $('#payorRelation').change(function () {
    //     var isRelValid = payorRelationValid();
    //
    //     if (isRelValid) {
    //         if ($("#payorRelation option:selected").text() == "Self"){
    //             getPayorSelf();
    //         }
    //         else {
    //
    //         }
    //     }
    // });
    //
    // $('#assuredNameDropdown').change(function () {
    //     assuredNameValid();
    // })
    //
    // function validatePayorForm() {
    //     // $('span.error').remove();
    //     assuredNameValid();
    //     payorNameValid();
    //     dobValid();
    //     premiumValid();
    //     payorRelationValid();
    //
    //     if ($("span").hasClass("error")) {
    //         return false;
    //     }
    //     else {
    //         return true;
    //     }
    // }
    //
    // function getPayorSelf() {
    //     var masterId = $('#proposalMasterNo').val();
    //     $.ajax({
    //         contentType: 'application/json',
    //         url: "get-proposal-self",
    //         type: 'POST',
    //         async: false,
    //         data: JSON.stringify(masterId),
    //         dataType: 'json',
    //         success: function (response) {
    //             $("#payorName").val(response.proposerName);
    //             $("#dobPayor").val(response.proposerDOB);
    //         },
    //         error: function (xhr, status, error) {
    //         }
    //     });
    // };
    //
    // // add new payor
    // $("#add-new-payor").click(function (event) {
    //     event.preventDefault();
    //     var isVaild = validatePayorForm();
    //     if(isVaild) {
    //         confirmPayorSaveDialog("Are you sure to save payor?");
    //     }
    // });
    //
    // var confirmPayorSaveDialog = function (text) {
    //     $.confirm({
    //         title: 'Confirm!',
    //         content: text,
    //         buttons: {
    //             confirm: {
    //                 btnClass: 'btn-info',
    //                 keys: ['enter'],
    //                 action: function () {
    //                     var proposal_payor = {};
    //
    //                     proposal_payor.masterId = $('#proposalMasterNo').val();
    //                     proposal_payor.assuredId = $("#assuredNameDropdown").find('option:selected').val();
    //                     proposal_payor.payorName = $("#payorName").val();
    //                     proposal_payor.payorDOB = $("#dobPayor").val();
    //                     proposal_payor.payorPremium = $("#payorPremiumPercent").val();
    //                     proposal_payor.relationId = $("#payorRelation").find('option:selected').val();
    //                     // proposal_payor.payorId =  $('#add-new-payor').data('id');
    //                     proposal_payor.payorId =  $('#nb_payor_id').val();
    //                     $.ajax({
    //                         contentType: 'application/json',
    //                         url:  "add-proposal-payor",
    //                         type: 'POST',
    //                         async: false,
    //                         data: JSON.stringify(proposal_payor),
    //                         dataType: 'json',
    //                         success: function(response) {
    //                             if(response == true){
    //                                 updatePayorTable();
    //                                 resetPayorForm();
    //                                 showAlert("Successfully updated");
    //                             }
    //                             if(response == false) {
    //                                 showAlert("Cannot updated due to input perchantage error");
    //                             }
    //                         },
    //                         error: function(xhr, status, error) {
    //                         }
    //                     });
    //                 }
    //
    //             },
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    // }
    //
    // $(document).on("click", ".payor_edit_button", function (event) {
    //     editPayor(this);
    // });
    //
    // $(document).on("click", ".payor_delete_button", function (event) {
    //     deletePayor(this);
    // });
    //
    // function  editPayor(identifier) {
    //     var row_id = $(identifier).attr("button_id");
    //     console.log("row_id: " + row_id);
    //
    //     $.ajax({
    //         contentType: 'application/json',
    //         url: "get-proposal-payor-info-dtl",
    //         type: 'POST',
    //         async: false,
    //         data: JSON.stringify(row_id),
    //         dataType: 'json',
    //         success: function (response) {
    //
    //
    //             $("#assuredNameDropdown").val(response.assuredId);
    //             $("#payorName").val(response.payorName);
    //             $("#dobPayor").val(response.payorDOB);
    //             $("#payorPremiumPercent").val(response.payorPremium);
    //             $("#payorRelation").val(response.relationId);
    //             // $('#add-new-payor').data('id', row_id);
    //             $('#nb_payor_id').val(row_id);
    //         },
    //         error: function (xhr, status, error) {
    //         }
    //     });
    //
    // }
    //
    // function deletePayor() {
    //     // alert("Delete under progress");
    // }
    //
    // var resetPayorForm = function () {
    //     $('#assuredNameDropdown').val("-1");
    //     $('#dobPayor').val("");
    //     $('#payorName').val("");
    //     $('#payorPremiumPercent').val('');
    //     $('#payorRelation').val("-1");
    //     // $('#add-new-payor').data('id',"0");
    //     $('#nb_payor_id').val('0');
    // };
    //
    // $(document).on("click", ".payor_delete_button", function (event) {
    //     var row_id = $(this).attr("button_id");
    //     confirmPayorDelete("Are you sure to delete this Payor?", row_id);
    // });

