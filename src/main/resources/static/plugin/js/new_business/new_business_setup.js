// function getAge(dateString) {
//     if(dateString == '') {
//         return '';
//     }
//     var now = new Date(); //Todays Date
//     var birthday = dateString;
//     birthday=birthday.split("/");
//
//     var dobMonth= birthday[1];
//     var dobDay= birthday[0];
//     var dobYear= birthday[2];
//
//     var nowDay= now.getDate();
//     var nowMonth = now.getMonth() + 1;  //jan = 0 so month + 1
//     var nowYear= now.getFullYear();
//
//     var ageyear = nowYear - dobYear;
//     var agemonth = nowMonth - dobMonth;
//     var ageday = nowDay- dobDay;
//     if (agemonth <= 0) {
//         ageyear--;
//         agemonth = (12 + agemonth);
//     }
//     if (nowDay < dobDay) {
//         agemonth--;
//         ageday = 30 + ageday;
//     }
//     var val = ageyear;
//     return val;
// }
//

function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}

$(document).ready(function () {

    //  $(".uwSection").click(function (e) {
    //    window.location.href = $(this).find('a').attr('href');
    //  });



//            $('#master_tab').hide();

//            $('#tab_content10').css("display","");

    //  $('.medReport').css("display", "");

    $('input[type="radio"]').click(function(){

        if($("input[name='policyCategory']:checked").attr('value') != "1" ) {
            $('.medReport').css("display", "");
        }else if($("input[name='policyCategory']:checked").attr('value') != "0" ){
            $('.medReport').css("display", "none");
        }else{
            $('.medReport').css("display", "");
        }

    });

  //    $('.medReport').css("display", "")
  //            if($("input[name='policyCategory']:checked").attr('value') != "1" ){
  //                //$('#profile-tab10').css("display","none");
  //                $('#profile-tab10').show();
  //            }else{
  //                $('#profile-tab10').hide();
  //            }

    function getAgentName(office_code){
        //console.log(office_code);
        //alert(office_code);
        var json = {
            "office_code": office_code
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/new-business/getAgent/"+ office_code,
            data: JSON.stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
//              console.log(data);
                //alert(JSON.stringify(json));
                $('#agentId').empty();
                var html = '';
                html += '<option value="-1">--Choose Agent--</option>';
                $.each(data, function (i, l) {

                    html += '<option value="' + l[0] + '">' + l[1] + '</option>';
                });

                $('#agentId').append(html)
                $("#agentId").selectpicker("refresh");
//                    console.log(html);
            },
            error: function (e) {
                //console.log('rrr');
            }
        });
    }

    $("#btn_newbusinesssave").click(function (event) {
        event.preventDefault();
        var isVaild = validationProposalInfo();

        if (isVaild) {
            confirmProposalInfoDialog("Are you sure to save Proposal Info?");
        }

    });

    var confirmProposalInfoDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        var proposal = {};

                        proposal.agentId = $('#agentId').val();
                        proposal.assuranceType = $('#assuranceType').val();
                        proposal.servicingOffice = $('#servicingOffice').val();
                        proposal.policyCategory = $('#policyCategory').val();
                       // proposal.commDate = $('#commDate').val();

                        proposal.proposalNo = $('#proposalNo').val();
                        proposal.proposalName = $('#proposalName').val();
                        proposal.proMobile = $('#proMobile').val();
                        proposal.proposalDate = $('#proposalDate').val();
                        proposal.organizationalSetup = $('#organizationalSetup').val();

                        proposal.rowkey = $('#rowkey').val();
                        proposal.pgid = $('#pgid').val();
                        proposal.generateValue = $('#generateValue').val();

                        //alert(proposal.commDate);

                        $.ajax({
                            contentType: 'application/json',
                            url: "/new-business/addPolicy",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(proposal),
                            dataType: 'json',
                            success: function (response) {
                                // alert(JSON.stringify(proposal));
                                //console.log(proposal);
                                //console.log("here client family");
                                // updateClientFamilyTable();
                                //resetClientFamilyForm();

                                $("#master_tab").show();
                                $("#btn_newbusinesssave").attr("disabled", true);
                                $("#btnRefresh").attr("disabled", true);
                                showAlert("Successfully saved");

                            },
                            error: function (xhr, status, error) {
                                showAlert("Something Wrong!!!");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };

    $( "#commDate" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1970:2010'
    });
    $( "#proposalDate" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1970:2010'
    });

//            $("#commDate").datepicker({
//                dateFormat: "dd/mm/yyyy"
//            });

    $("#btnRefresh").click(function () {
        clearform();
    });

    function validationProposalInfo() {
        var status = true;
        var cDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        var cMb = /^((?=(01))[0-9]{11}|)$/;
        //var cChar = /^[a-z][a-z\s]*$/

        if ($("#agentId").val() == "-1") {
            status = false;
            $("#agentIdSpan").text("Empty field found!!");
            $("#agentId").focus();
        } else $("#agentIdSpan").text("");

        if ($("#proposalName").val() == "") {
            status = false;
            $("#proposalNameSpan").text("Empty field found!!");
            $("#proposalName").focus();
        } else $('#proposalNameSpan').text("");

//                } else if (!cChar.test($("#proposalName").val())){
//                    status = false;
//                    $('#proposalNameSpan').text("Invalid field value found !!");
//                    $('#proposalName').focus();

        if ($('#proMobile').val() == "") {
            status = false;
            $('#proposalMobileSpan').text("Empty field found!!");
            $('#proMobile').focus();
        } else if (!cMb.test($('#proMobile').val())) {
            status = false;
            $('#proposalMobileSpan').text("Invalid contact no!!");
            $('#proMobile').focus();
        } else $('#proposalMobileSpan').text("");

        // if ($("#commDate").val() == "") {
        //     status = false;
        //     $("#commDateSpan").text("Empty field found!!");
        //     $("#commDate").focus();
        // } else if (!cDate.test($("#commDate").val())) {
        //     status = false;
        //     $("#commDateSpan").text("Invalid date format found!!");
        //     $("#commDate").focus();
        // } else $("#commDateSpan").text("");

        if ($("#proposalDate").val()=="") {
            status = false;
            $("#proposalDateSpan").text("Empty field found!!");
            $("#proposalDate").focus();
        } else if (!cDate.test($("#proposalDate").val())) {
            status = false;
            $("#proposalDateSpan").text("Invalid date format found!!");
            $("#proposalDate").focus();
        } else $("#proposalDateSpan").text("");

        if($("input[name='policyCategory']:checked").attr('value') != "0" && $("input[name='policyCategory']:checked").attr('value') != "1" ){
            status=false;
            $("#err_policyCategory").text("Empty field found!!");
            $("#policyCategory").focus();
        }else $("#err_policyCategory").text("");

        if($("input[name='assuranceType']:checked").attr('value') != "1" && $("input[name='assuranceType']:checked").attr('value') != "2" && $("input[name='assuranceType']:checked").attr('value') != "3" ){
            status=false;
            $("#err_assuranceType").text("Empty field found!!");
            $("#assuranceType").focus();
        }else $("#err_assuranceType").text("");

        if ($("#policyCategory").val() == "") {
            status = false;
            $("#policyCategorySpan").text("Empty field found!!");
            $("#policyCategory").focus();
        } else $("#policyCategorySpan").text("");

        if ($("#assuredName").val() == "") {
            status = false;
            $("#assuredNameSpan").text("Empty field found!!");
            $("#assuredName").focus();
        } else $("#assuredNameSpan").text("");

        if ($("#servicingOffice").val() == "-1") {
            status = false;
            $("#officeserviceSpan").text("Empty field found!!");
            $("#servicingOffice").focus();
        } else $("#officeserviceSpan").text("");

        if ($("#organizationalSetup").val() == "") {
            status = false;
            $("#organizationalSpan").text("Empty field found!!");
            $("#organizationalSetup").focus();
        } else $("#organizationalSpan").text("");

        if ($("#proposalNo").val() == "") {
            status = false;
            $("#proposalNoSpan").text("Empty field found!!");
            $("#proposalNo").focus();
        } else $("#proposalNoSpan").text("");

        //var str_sms = $('#smsMobile').val();

        return status;
    }

    var pathname = window.location.pathname;
    var short_path_name = pathname.split('/')[2];
    //   alert(short_path_name);
    if (short_path_name === "underwritingAcceptProcess") {

        if($("input[name='policyCategory']:checked").attr('value') != "1" ){
            $("#medicalTab").show();

        }else{
            $("#medicalTab").hide();
        }
        //$("#updateProposalInfo").addClass('active');
        $("#underwritingAcceptProcess").show();

    }

    if (short_path_name === "updateProposalInfo") {

        if($("input[name='policyCategory']:checked").attr('value') != "1" ){
            $("#medicalTab").show();

        }else{
            $("#medicalTab").hide();
        }

    }



//            $('input[type="radio"]').click(function(){
//
//                if($("input[name='policyCategory']:checked").attr('value') != "1" ) {
//                    $('.medReport').css("display", "");
//                }else if($("input[name='policyCategory']:checked").attr('value') != "0" ){
//                    $('.medReport').css("display", "none");
//                }else{
//                    $('.medReport').css("display", "");
//                }
//
//            });
//
//


    // if($("input[name='policyCategory']:checked").attr('value') != "1" ) {
    //    $("#uwAcceptance").addClass('active');
    //  $("#medReport").addClass('');
    // $("#uwSection").addClass('active');
    // }else {
    //     $("#master_tab_class").addClass('active');
    //     $("#medReport").addClass('active');
    //     $("#uwSection").addClass('active');
    //  }


//            if ($("#pgid").val() != "" ) {
//                $("#master_tab").show();
//            }

    function clearform() {

        $('#agentId').val("-1");
        $('#agentId').selectpicker('refresh');
        $('#proposalName').val("");
        $('#proMobile').val("");
        $('#commDate').val("");
        $('#proposalDate').val("");
        //$('#policyCategory')
        //$("input:radio").attr("checked", false);
        $('#assuranceType').val("");
        $('#proposalNo').val("");
        $('#organizationalSetup').val("");
        $('#organizationalSetup').selectpicker('refresh');
        $('#generateValue').val("");
        $('#pgid').val("");
        $('#rowkey').val("");
        $('#servicingOffice').val('-1');
        $('#servicingOffice').selectpicker('refresh');

    }

    $('#servicingOffice').on('change', function() {

        getAgentName( $(this).children("option:selected").val());
        //generateProposalNo();
        $("#organizationalSetup").val('');
        $('#organizationalSetup').selectpicker('refresh');
        $("#rowkey").val('');
        $("#pgid").val('');
        $("#proposalNo").val('');
        $("#generateValue").val('');
        $("input[name='assuranceType']").removeAttr("checked");

         //alert( $(this).children("option:selected").val());
    });

    $('input[type=radio][name=assuranceType]').change(function() {

        generateProposalNo();
    });

    function  generateProposalNo() {
        //var getAgentId =  $('#agentId').children("option:selected").val();

        var getOfficeId =  $('#servicingOffice').children("option:selected").val();

//                var getAssuranceType = $('#assuranceType').val();
        var getAssuranceType = $("input[name='assuranceType']:checked").attr('value');

        if(isEmptyString(getOfficeId) || isEmptyString(getAssuranceType)|| getOfficeId=='-1' ){

            $("#rowkey").val('');
            $("#pgid").val('');
            $("#proposalNo").val('');
            $("#generateValue").val('');

        }
        else{

            var json = {
                "getOfficeId": getOfficeId,
                "getAssuranceType":getAssuranceType
            };

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/new-business/getProposalId/" + getOfficeId + getAssuranceType,
                data: JSON.stringify(json),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (data) {

                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";
                    $.each(data, function (i, l) {
                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];
                    });
                    $("#rowkey").val(value_1);
                    $("#pgid").val(value_3);
                    $("#proposalNo").val(value_2);
                    $("#generateValue").val(value_4);
                },
                error: function (e) {

                    showAlert("Something wrong!!!");
                }
            });
        }
    }

    $('#agentId').on('change', function() {

        var getAgentId = $('#agentId').val();

        if (getAgentId != "") {
           // alert("hi...");
            generateDoId();
            generateDmId();

            var json = {
                "getAgentId": getAgentId
            };

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/new-business/getOrgSetupId/" + getAgentId,
                data: JSON.stringify(json),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (data) {

                    var value_4 = data[0];
                    $("#organizationalSetup").val(value_4);
                },
                error: function (e) {
                    showAlert("Something wrong.. !!!");
                }

            });

        }else {

        }
        //generateProposalNo();
    });

    function generateDoId() {

        var getAgentId = $('#agentId').val();

        var json = {
            "getAgentId": getAgentId
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/new-business/add-DoIdName/" + getAgentId,
            data: JSON.stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {

                var value_4 = data[0];
                $("#doidName").val(value_4);
            },
            error: function (e) {
                showAlert("Something wrong.. !!!");
            }

        });
    }

    function generateDmId() {

        var getAgentId = $('#agentId').val();

        var json = {
            "getAgentId": getAgentId
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/new-business/add-DmIdName/" + getAgentId,
            data: JSON.stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {

                var value_4 = data[0];
                $("#dmidName").val(value_4);
            },
            error: function (e) {
                showAlert("Something wrong.. !!!");
            }

        });
    }
    // End Generate the value using agentId
    var showAlert = function (alertContent) {
        new PNotify({
            title: 'Info',
            text: alertContent,
            type: 'info',
            styling: 'bootstrap3'
        });
    }

});


