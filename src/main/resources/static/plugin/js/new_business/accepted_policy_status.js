$(document).ready(function () {

    $('#edit_show').click(function () {

        $('.uwSection').css("display","");

    });

   $('#tbl-underwriting-lst').DataTable({
        dom: 'Bfrtip',
      "scrollX": true,
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
       scrollY:        "300px",
       scrollX:        true,
       scrollCollapse: true,
       paging:         true,
       columnDefs: [
           { width: '30%', targets: 0 }
       ]//,
       // fixedColumns: true
    }) ;

    $("#refreshBtn").click(function () {
        resetForm();
    });

    function resetForm() {

        $('#proposalNo').val("");
        $('#officeCode').val("-1");
    }

});

function validateForm() {
   // alert(document.forms["proposalSearchPanel"]["officeCode"].value);
    var x = document.forms["proposalSearchPanel"]["officeCode"].value;
    // console.log(x);
    if (isEmptyString(x)) {
       customAlert(alertTypes.WARNING,"Required!","Servicing office is required ")
        //showAlertByType("Servicing office is required!","W");
        // $('.requiredF').attr("display","none");
        return false;
    }
}

function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}

var locale='en';
const alertTypes = {
    INFO: 'Info',
    SUCCESS: 'Success',
    WARNING: 'Waring',
    FAILED: 'Failed',
    DENIED: 'Illegal action',
    ERROR: 'Error'
}
function  customAlert(alertType,titleMsg,detMsg) {

    var msgTitle = '';


    switch (alertType){
        case alertTypes.INFO:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'তথ্য':'Info'): titleMsg;


            $.confirm({
                icon: 'ace-icon fa fa-eye',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'blue',
                buttons: locale == 'bn'?'ঠিক আছে':'OK',
                title: msgTitle,
                content: isEmptyString(detMsg)?'Tray Again latter.':detMsg,
                typeAnimated: true,
                btnClass: 'btn-blue'


            });
            break;
        case alertTypes.SUCCESS:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'? 'সফল হয়েছে':'Success'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-smile-o',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'purple',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Successfully Done.':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        case alertTypes.WARNING:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'সতর্কীকরণ!':'Warning!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-exclamation-circle',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'orange',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        case alertTypes.FAILED:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'অসফল হয়েছে!':'Failed!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-ban',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'red',

                title: msgTitle,
                content:  isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        case alertTypes.DENIED:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'অবৈধ!':'Denied!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-lock',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'red',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        case alertTypes.ERROR:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'ভুল !':'Denied!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-ban',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'red',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;
        default:
            msgTitle= isEmptyString(titleMsg)? (locale == 'bn'?  'কিছু ভুল হয়েছে !':'Something Wen\'t wrong!'): titleMsg;
            $.confirm({
                icon: 'ace-icon fa fa-bullhorn',
                theme: 'material',
                closeIcon: true,
                animation: 'scale',
                type: 'dark',

                title: msgTitle,
                content: isEmptyString(detMsg)?'Something wen\'t wrong .':detMsg,
                typeAnimated: true,
                buttons: {
                    Yes: {
                        text: locale == 'bn'?'ঠিক আছে':'OK'
                    }
                }

            });
            break;

    }
}

