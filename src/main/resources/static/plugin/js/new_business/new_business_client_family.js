/**
 * Created by Asad on 3/24/2019.
 */
function parseNumber(str) {
    if(str==null|| str.trim()==""||str=="undefined"){
        return null;
    }
    else{
        var  intNum=parseInt(str);

        return intNum=="NaN"?null:intNum;
    }

}

$(document).ready(function () {

        $("#cl_family_save_button").click(function (event) {
            event.preventDefault();
            var isVaild = validationClientFamily();
            if(isVaild) {
                confirmClientFamilyDialog("Are you sure to save Family history?");
            }
        });

    var confirmClientFamilyDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                       // var proposal_client_family = {};
                        var proFamily = {};

                        proFamily.famPGID=$("#pgidss").val();
                        proFamily.famPARTY_CD=$("#cl_party_id").val();

                        proFamily.cl_fam_member_name=$("#cl_fam_member_name").val();
                        proFamily.famRELATION_CD = $("#famRELATION_CD").val();
                        proFamily.famAGE = $("#cl_fam_age_id").val();
                        proFamily.famPHYSICAL_CONDITION = $("#cl_fam_phy_id").val();
                        proFamily.famAGE_AT_DEATH = $("#cl_fam_age_death_id").val();
                        proFamily.famCAUSE_OF_DEATH = $("#cl_fam_cause_death_id").val();
                        proFamily.famYEAR_OF_DEATH = $("#cl_fam_yr_death_id").val();
                        proFamily.famREMARK = $("#cl_fam_remarks_id").val();
                        proFamily.famREMARK = $("#cl_fam_remarks_id").val();
                        proFamily.famRELATION_SL_NO = parseNumber($("#fm_sl").val());

                        $.ajax({
                            contentType: 'application/json',
                            url: "/new-business/add-family",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(proFamily),
                            dataType: 'json',
                            success: function (response) {
                                //alert(JSON.stringify(proFamily));
                                //console.log("here client family");
                               // updateClientFamilyTable();
                                updateTableList();
                                showAlert("Successfully saved");
                                resetClientFamilyForm();
                            },
                            error: function (xhr, status, error) {
                                showAlert("Please check your parent info...");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };

    var resetClientFamilyForm = function () {

        $('#cl_fam_member_name').val("");
        $('#famRELATION_CD').val("-1");
        $('#cl_fam_age_id').val("");
        $('#cl_fam_phy_id').val("");
        $('#cl_fam_age_death_id').val("");
        $('#cl_fam_cause_death_id').val("");
        $('#cl_fam_yr_death_id').val("");
        $('#cl_fam_remarks_id').val("");
        $('#fm_pgid').val("");
        $('#partyId').val("");
        $('#fm_sl').val("");
        $("#cl_party_id").val();
    };


    function updateTableList() {

        // alert("hi");
        var familyTable = $('#cl_family_table_id');
        // alert("hlw");
        $.ajax({
            type: "GET",
            url: "/new-business/addfamilyList",
            success: function (data) {
                //  alert(data);
                var no = 1;
                var tableBody = "";

                $('#cl_family_table_id tbody').empty();
                $.each(data, function (idx, elem) {
                    // alert (elem[idx]);

                    var edit = '<a class="btn btn-success fmailyInformationEdit" value="Edit" id="editFamily"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger fmailyInformationDelete" value="Delete" id="deleteFamily"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    //tableBody += "<td>" + elem[1] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td hidden>" + elem[2] + "</td>";
                    tableBody += "<td>" + elem[3] + "</td>";
                    tableBody += "<td hidden class='fm-party-type'>" + elem[4] + "</td>";
                    tableBody += "<td>" + elem[5] + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + elem[7] + "</td>";
                    tableBody += "<td>" + elem[8] + "</td>";
                    tableBody += "<td>" + elem[9] + "</td>";
                    tableBody += "<td>" + elem[10] + "</td>";
                    tableBody += "<td>" + elem[11] + "</td>";
                    tableBody += "<td>" + edit + "</td>"
                    tableBody += "<td>" + del + "</td>"
                    tableBody += "<tr>";
                });
                familyTable.append(tableBody);
            }
        });
    }

    $(document).on("click", "#cl_family_table_id tbody tr", function() {

        var curRow = $(this).closest('tr');

        var pgid = curRow.find('td:eq(0)').text();
        var fm_party = curRow.find('td:eq(1)').text();
        var fm_sl = curRow.find('td:eq(2)').text();
        var member = curRow.find('td:eq(3)').text();
        // var famRELATION_CD = curRow.find('td:eq(2)').text();
        var famPHYSICAL_CONDITION = curRow.find('td:eq(6)').text();
        var famAGE = curRow.find('td:eq(7)').text();
        var  famAGE_AT_DEATH= curRow.find('td:eq(8)').text();
        var  famCAUSE_OF_DEATH= curRow.find('td:eq(9)').text();
        var  famYEAR_OF_DEATH= curRow.find('td:eq(10)').text();
        var  famREMARK= curRow.find('td:eq(11)').text();


        $('#pgid').val(pgid);
        $('#fm_sl').val(fm_sl);
        $('#fm_party').val(fm_party);
        $("#cl_fam_member_name").val(member);
        $('#famRELATION_CD').val(( $(this).find(".fm-party-type").html()));
        $('#cl_fam_phy_id').val(famPHYSICAL_CONDITION);
        $('#cl_fam_age_id').val(famAGE);
        $('#cl_fam_age_death_id').val(famAGE_AT_DEATH);
        $('#cl_fam_cause_death_id').val(famCAUSE_OF_DEATH);
        $('#cl_fam_yr_death_id').val(famYEAR_OF_DEATH);
        $('#cl_fam_remarks_id').val(famREMARK);

    });

    $(document).on("click", "#deleteFamily", function () {
        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();

       // alert(col1);
       // alert(col2);
        //alert(col3);
        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to Delete?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/family-proposal-info-delete/" + col1+"/"+col2+"/"+col3,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            resetClientFamilyForm();
                            showAlert("Successfully deleted");
                            $('#cl_family_table_id tbody tr').empty();
                            updateTableList();

                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });



    //     var updateClientFamilyTable = function () {
    //
    //     var tableData = $("#cl_family_table_data_id");
    //
    //         famPGID = $('#pgidss').val();
    //
    //     $.ajax({
    //         contentType: 'application/json',
    //         url: "/new-business/get-client-family"+famPGID,
    //         type: 'POST',
    //         async: false,
    //         data: JSON.stringify(famPGID),
    //         dataType: 'json',
    //         success: function (response) {
    //
    //             console.log("here client family info table2");
    //
    //             alert(JSON.stringify(famPGID));
    //
    //             tableData.html("");
    //             html = "";
    //             $.each(response, function(idx, elem){
    //
    //                 var famPARTY_CD = (elem.famPARTY_CD == null) ? "N/A" : elem.famPARTY_CD;
    //                 var famPGID = (elem.famPGID == null) ? "N/A" : elem.famPGID;
    //                 //var causeDeath = (elem.famPARTY_ID == null) ? "N/A" : elem.famPARTY_ID;
    //                // var yearDeath = (elem.yearDeath == null) ? "N/A" : elem.yearDeath;
    //                 //var remarks = (elem.remarks == null) ? "N/A" : elem.remarks;
    //                  // alert(famPGID);
    //                 html = "";
    //                 html += "<tr>";
    //               //  html += "<td>" + elem.relationName + "</td>";
    //                // html += "<td>" + elem.age + "</td>";
    //                // html += "<td>" + elem.phyCondition + "</td>";
    //                 html += "<td>" + famPARTY_CD + "</td>";
    //                 html += "<td>" + famPGID + "</td>";
    //                // html += "<td>" + yearDeath + "</td>";
    //                // html += "<td>" + remarks + "</td>";
    //                 html += "<td>" + '<button type="button" class="btn  btn-sm btn-info cl_fam_edit_button" value="Edit" button_id="'+ elem.id +'"> ' +
    //                     '<i class="fa fa-pencil"></i></button>' + '<button type="button" class="btn  btn-sm btn-danger cl_fam_delete_button" value="Delete" button_id="'+ elem.id +'"> '   +
    //                     '<i class="fa fa-trash"></i></button>'+ "</td>";
    //                 html += "<tr>";
    //                 tableData.append(html);
    //             });
    //         },
    //         error: function (xhr, status, error) {
    //         }
    //     });
    //
    // };

   // updateClientFamilyTable();

    function validationClientFamily() {

        var status = true;
        var cDigit = /^[0-9]+$/;
        var cChar = /^[a-zA-Z]+$/;

        if($('#cl_fam_age_death_id').val()==""){

            if ($("#cl_fam_age_id").val() == "") {
                status = false;
                $("#err_cl_fam_age_id").text("Empty field found!!");
                $("#cl_fam_age_id").focus();
            } else if (!cDigit.test($("#cl_fam_age_id").val())){
                status = false;
                $('#err_cl_fam_age_id').text("Invalid field value found !!");
                $('#cl_fam_age_id').focus();
            } else $('#err_cl_fam_age_id').text("");

            if ($("#famRELATION_CD").val() == "-1") {
                status = false;
                $("#err_cl_fam_relation_id").text("Empty field found!!");
                $("#famRELATION_CD").focus();
            } else $("#err_cl_fam_relation_id").text("");

            if ($("#cl_fam_member_name").val()==""){
                status = false;
                $("#err_cl_fam_member_name").text("Empty field found!!");
                $("#cl_fam_member_name").focus();
            } else $("#err_cl_fam_member_name").text("");
            // }else if(!cChar.test($("#cl_fam_member_name").val())) {
            //     status = false;
            //     $("#err_cl_fam_member_name").text("Invalid filed value found!!");
            //     $("#cl_fam_member_name").focus();
            //


            return status;

        }else {
            if ($("#cl_fam_age_id").val() == "") {
                status = false;
                $("#err_cl_fam_age_id").text("Empty field found!!");
                $("#cl_fam_age_id").focus();
            } else if (!cDigit.test($("#cl_fam_age_id").val())){
                status = false;
                $('#err_cl_fam_age_id').text("Only Digit allow here !!");
                $('#cl_fam_age_id').focus();
            } else $('#err_cl_fam_age_id').text("");

            if ($("#famRELATION_CD").val() == "-1") {
                status = false;
                $("#err_cl_fam_relation_id").text("Empty field found!!");
                $("#famRELATION_CD").focus();
            } else $("#err_cl_fam_relation_id").text("");

            if (!cDigit.test($("#cl_fam_age_death_id").val())) {
                status = false;
                $("#err_cl_fam_age_death_id").text("Invalid field value found!!");
                $("#cl_fam_age_death_id").focus();
            } else $("#err_cl_fam_age_death_id").text("");

            if (!cDigit.test($("#cl_fam_yr_death_id").val())) {
                status = false;
                $("#err_cl_fam_yr_death_id").text("Invalid field value found !!");
                $("#cl_fam_yr_death_id").focus();
            } else $("#err_cl_fam_yr_death_id").text("");

            if ($("#cl_fam_member_name").val()==""){
                status = false;
                $("#err_cl_fam_member_name").text("Empty field found!!");
                $("#cl_fam_member_name").focus();
            } else $("#err_cl_fam_member_name").text("");
            // } else if(!cChar.test($("#cl_fam_member_name").val())) {
            //     status = false;
            //     $("#err_cl_fam_member_name").text("Invalid filed value found!!");
            //     $("#cl_fam_member_name").focus();


            if ($("#cl_party_id").val()== "-1") {
                status = false;
                $("#err_cl_party_id").text("Empty field found!!");
                $("#cl_party_id").focus();
            } else $("#err_cl_party_id").text("");

            return status;
        }

    }

});

//==================================================
//
// $(document).ready(function () {
//
//     var validationClientFamily = function () {
//         var isValid = true;
//         $(".cl_family_form .err_msg").text("");
//         $(".cl_family_form input.mand").each(function(){
//             if(this.value.length == 0) {
//                 $("#err_" + this.id ).text("Please enter the value");
//                 $(this.id ).focus();
//                 isValid = false;
//             }
//         });
//         $(".cl_family_form select.mand").each(function(){
//             if(this.value == -1) {
//                 $("#err_" + this.id ).text("Please select the value");
//                 $(this.id ).focus();
//                 isValid = false;
//             }
//         });
//         return isValid;
//     };
//
//     var resetClientFamilyForm = function () {
//         $('#cl_fam_id').val("0");
//         $('#cl_fam_relation_id').val("-1");
//         $('#cl_fam_age_id').val("");
//         $('#cl_fam_phy_id').val("");
//         $('#cl_fam_age_death_id').val("");
//         $('#cl_fam_cause_death_id').val("");
//         $('#cl_fam_yr_death_id').val("");
//         $('#cl_fam_remarks_id').val("");
//
//     };
//
//     var updateClientFamilyTable = function () {
//         var tableData = $("#cl_family_table_data_id");
//         masterId = $('#proposalMasterNo').val();
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-client-family-info",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(masterId),
//             dataType: 'json',
//             success: function (response) {
//                 console.log("here client family info table2");
//                 tableData.html("");
//                 html = "";
//                 $.each(response, function(idx, elem){
//                     var ageDeath = (elem.causeDeath == null) ? "N/A" : elem.causeDeath;
//                     var causeDeath = (elem.causeDeath == null) ? "N/A" : elem.causeDeath;
//                     var yearDeath = (elem.yearDeath == null) ? "N/A" : elem.yearDeath;
//                     var remarks = (elem.remarks == null) ? "N/A" : elem.remarks;
//
//                     html = "";
//                     html += "<tr>";
//                     html += "<td>" + elem.relationName + "</td>";
//                     html += "<td>" + elem.age + "</td>";
//                     html += "<td>" + elem.phyCondition + "</td>";
//                     html += "<td>" + ageDeath + "</td>";
//                     html += "<td>" + causeDeath + "</td>";
//                     html += "<td>" + yearDeath + "</td>";
//                     html += "<td>" + remarks + "</td>";
//                     html += "<td>" + '<button type="button" class="btn  btn-sm btn-info cl_fam_edit_button" value="Edit" button_id="'+ elem.id +'"> ' +
//                         '<i class="fa fa-pencil"></i></button>' + '<button type="button" class="btn  btn-sm btn-danger cl_fam_delete_button" value="Delete" button_id="'+ elem.id +'"> '   +
//                         '<i class="fa fa-trash"></i></button>'+ "</td>";
//                     html += "<tr>";
//                     tableData.append(html);
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//
//
//
//     };
//
//     updateClientFamilyTable();
//
//     var confirmClientFamilyDialog = function (text) {
//         $.confirm({
//             title: 'Confirm!',
//             content: text,
//             buttons: {
//                 confirm: {
//                     btnClass: 'btn-info',
//                     keys: ['enter'],
//                     action: function () {
//                         var proposal_client_family = {};
//
//                         proposal_client_family.masterId = $('#proposalMasterNo').val();
//                         proposal_client_family.id = $('#cl_fam_id').val();
//                         proposal_client_family.relationId = $('#cl_fam_relation_id').val();
//                         proposal_client_family.age = $('#cl_fam_age_id').val();
//                         proposal_client_family.phyCondition = $('#cl_fam_phy_id').val();
//                         proposal_client_family.ageDeath = $('#cl_fam_age_death_id').val();
//                         proposal_client_family.causeDeath = $('#cl_fam_cause_death_id').val();
//                         proposal_client_family.yearDeath = $('#cl_fam_yr_death_id').val();
//                         proposal_client_family.remarks = $('#cl_fam_remarks_id').val();
//
//                         $.ajax({
//                             contentType: 'application/json',
//                             url: "add-client-family-info",
//                             type: 'POST',
//                             async: false,
//                             data: JSON.stringify(proposal_client_family),
//                             dataType: 'json',
//                             success: function (response) {
//                                 console.log(proposal_client_family);
//                                 console.log("here client family");
//                                 updateClientFamilyTable();
//                                 resetClientFamilyForm();
//                                 showAlert("Successfully updated");
//                             },
//                             error: function (xhr, status, error) {
//                             }
//                         });
//                     }
//
//                 },
//                 cancel: function () {
//
//                 }
//             }
//         });
//     };
//
//
//     $("#cl_family_save_button").click(function (event) {
//         event.preventDefault();
//         var isVaild = validationClientFamily();
//         if(isVaild) {
//             confirmClientFamilyDialog("Are you sure to save Family history?");
//         }
//     });
//
//  //   edit button
//  //    $(document).on("click", ".cl_fam_edit_button", function (event) {
//  //        var row_id = $(this).attr("button_id");
//  //        console.log("row_id: " + row_id);
//  //
//  //        $.ajax({
//  //            contentType: 'application/json',
//  //            url: "get-client-family-info-dtl",
//  //            type: 'POST',
//  //            async: false,
//  //            data: JSON.stringify(row_id),
//  //            dataType: 'json',
//  //            success: function (response) {
//  //                console.log("here client family info dtl");
//  //                $('#cl_fam_id').val(response.id);
//  //                $('#cl_fam_relation_id').val(response.relationId);
//  //                $('#cl_fam_age_id').val(response.age);
//  //                $('#cl_fam_phy_id').val(response.phyCondition);
//  //                $('#cl_fam_age_death_id').val(response.ageDeath);
//  //                $('#cl_fam_cause_death_id').val(response.causeDeath);
//  //                $('#cl_fam_yr_death_id').val(response.yearDeath);
//  //                $('#cl_fam_remarks_id').val(response.remarks);
//  //
//  //
//  //            },
//  //            error: function (xhr, status, error) {
//  //            }
//  //        });
//  //
//  //    });
//
//
// });