/**
 * Created by user on 27-Mar-19.
 */

$(document).ready(function () {

    $("#add-client-habit").click(function () {

        var flag=ValidatingData();

        if(flag== true){

          //  var pgidss =  $("#pgidss").val();
            var cl_party_id =  $("#cl_party_id").val();
            var habitNameId = $("#habitNameId").val();
            var habitValue = $("#habitValue").val();
            var habitUnitCd = $("#habitUnitCd").val();

            // alert(pgidss);

            var proposalHabit = {};

            //proposalHabit.habitpgid=pgidss;
            proposalHabit.clpartyid = cl_party_id;
            proposalHabit.habitNameId = habitNameId;
            proposalHabit.habitValue = habitValue;
            proposalHabit.habitUnitCd = habitUnitCd;
              //
              // alert(proposalHabit.habitpgid);
              // alert(proposalHabit.clpartyid);
              // alert(proposalHabit.habitNameId);
              // alert(proposalHabit.habitValue);
              // alert(proposalHabit.habitUnitCd);

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/new-business/add-habit",
                data: JSON.stringify(proposalHabit),
                dataType: 'json',
                success: function (data) {
                    // alert(data);
                    updateTableList();
                    showAlert("Successfully Saved!");
                    refresh();
                },
                error: function (e) {
                    showAlert("Something Wrong!!!");
                }
            });
        }
    });


    function refresh (){
        // $('#cl_party_id').val("-1");
        $('#habitNameId').val("-1");
        $('#habitValue').val("");
        $('#habitUnitCd').val("-1");
    }

    function updateTableList() {

        // alert("hi");
        var habitTable = $('#habitTableId');
      //  alert("hlw");
        $.ajax({
            type: "GET",
            url: "/new-business/addHabitList",
            success: function (data) {
               // alert(data);
                var no = 1;
                var tableBody = "";

                $('#habitTableId tbody').empty();
                $.each(data, function (idx, elem) {
                    // alert (elem[idx]);

                    var edit = '<a class="btn btn-success habitInformationEdit" value="Edit" id="editHabit"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger habitInformationDelete" value="Delete" id="deleteHabit"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";

                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td hidden class='hbcd-party-type'>" + elem[2] + "</td>";
                    tableBody += "<td>" + elem[3] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td hidden class='unit-party-type'>" + elem[5] + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + edit + "</td>"
                    tableBody += "<td>" + del + "</td>"
                    tableBody += "<tr>";
                });
                habitTable.append(tableBody);
            }
        });
    }

    function ValidatingData() {

        var status = true;
        var cDigit = /^[0-9]+$/;

        if ($("#habitNameId").val() == "-1") {
            status = false;
            $("#habitIdErrorSpan").text("Empty field found!!");
            $("#habitNameId").focus();
        } else $("#habitIdErrorSpan").text("");

        if ($("#habitValue").val() == "") {
            status = false;
            $("#habitValueErrorSpan").text("Empty field found!!");
            $("#habitValue").focus();
        } else if (!cDigit.test($("#habitValue").val())){
            status = false;
           $('#habitValueErrorSpan').text("Only Digit allow here !!");
           $('#habitValue').focus();
         } else $('#habitValueErrorSpan').text("");

        if ($("#habitUnitCd").val() == "-1") {
            status = false;
            $("#habitidunitErrorSpan").text("Empty field found!!");
            $("#habitUnitCd").focus();
        } else $("#habitidunitErrorSpan").text("");

        if ($("#cl_party_id").val()== "-1") {
            status = false;
            $("#err_cl_party_id").text("Empty field found!!");
            $("#cl_party_id").focus();
        } else $("#err_cl_party_id").text("");

        return status;
    }

    $(document).on("click", "#habitTableId tbody tr", function() {

        var curRow = $(this).closest('tr');

        var pgid = curRow.find('td:eq(0)').text();
        var partyid = curRow.find('td:eq(1)').text();
        var habitNameId = curRow.find('td:eq(2)').text();
        var habitValue = curRow.find('td:eq(4)').text();
        var habitUnitCd = curRow.find('td:eq(5)').text();

        $('#pgid').val(pgid);
       // $('#pgid').val(partyid);
        $('#habitNameId').val(( $(this).find(".hbcd-party-type").html()));
        $('#habitValue').val(habitValue);
        $('#habitUnitCd').val(( $(this).find(".unit-party-type").html()));

    });


   // $('#habitTableId tbody').on('click', '#deleteHabit', function () {
        $(document).on("click", "#deleteHabit", function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to Delete?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/habit-proposal-info-delete/" + col1 +"/"+col2+"/"+col3,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            refresh();
                            showAlert("Successfully deleted");
                            $('#habitTableId tbody tr').empty();
                            updateTableList();

                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });


});


// function getLastNextPGIDINSPPRoposal(pgid) {
//
//     var json = {
//         "officeId": ""
//     };
//
//     $.ajax({
//         type: "POST",
//         contentType: "application/json",
//         url: "/new-business/getLastNextPGID/" + pgid,
//         data: JSON
//             .stringify(json),
//         dataType: 'json',
//         cache: false,
//         timeout: 600000,
//         success: function (data) {
//             //$('#pgidss').val(data);
//         },
//         error: function (e) {
//         }
//     });
// }




// $(document).ready(function () {
//     var habitNameDD = [];
//         function clearErrorMsg(){
//             $("#habitIdErrorSpan").html("");
//             $("#habitValueErrorSpan").html("");
//         }
//         function validate(){
//             var status = 1;
//             if($("#habitNameId").val() === "-1"){
//                 //$("#habitNameId").focus();
//                 $("#habitIdErrorSpan").html("Please Select");
//                 status =0;
//             }
//             if ($("#habitValue").val() === ""){
//                 $("#habitValue").focus();
//                 $("#habitValueErrorSpan").html("Please enter a value");
//                 status = 0;
//             }
//             return status;
//         }
//
//         var tableData = $("#habit_table_data_id");
//         masterId = $("#proposalMasterNo").val();
//         $.ajax({
//             contentType: 'application/json',
//             url: "get-client-habit-by-id",
//             type: 'POST',
//             async: false,
//             data: JSON.stringify(masterId),
//             dataType: 'json',
//             success: function (response) {
//                 tableData.html("");
//                 html = "";
//                 $.each(response, function(idx, elem){
//
//                     habitNameDD.push(elem.habitId);
//                     html = "";
//                     html += "<tr " + "id=row" + elem.id + ">";
//                     html += "<td>" + elem.habitName + "</td>";
//                     html += "<td>" + elem.value + "</td>";
//                     html += "<td>" + '<button type="button" class="btn btn-info habit_edit_button" value="Edit" id="'+ elem.id +'">' +
//                         '<i class="fa fa-pencil"></i></button>' +
//                         '<button type="button" class="btn btn-danger habit_delete_button" ' +
//                         'value="Delete" ' +
//                          'habit_dd_id ="' + elem.habitId + '"'+
//                         ' id="hab-del-'+ elem.id +'" >' +
//                         '<i class="fa fa-trash"></i></button>'+
//                         "</td>";
//
//                     html += "<tr>";
//                     tableData.append(html);
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//
//     $(document).on("click", "#add-client-habit", function (e) {
//         var res = validate();
//
//         if (res == 1){
//             if(!habitNameDD.includes($("#habitNameId option:selected").val())){
//                 clearErrorMsg();
//                 $.confirm({
//                     title: 'Confirm!',
//                     content: "Are you sure to save the data?",
//                     buttons: {
//                         confirm: {
//                             btnClass: 'btn-info',
//                             keys: ['enter'],
//                             action: function () {
//                                 var client_habit = {};
//                                 var habitName = $("#habitNameId option:selected").text();
//                                 var value = $("#habitValue").val();
//
//                                 client_habit.habitName = habitName;
//                                 client_habit.habitNameId = $("#habitNameId").val();
//                                 client_habit.habitValue = $("#habitValue").val();
//                                 client_habit.propMasterId = $("#proposalMasterNo").val();
//
//                                 $.ajax({
//                                     contentType: 'application/json',
//                                     url:  "add-client-habit",
//                                     type: 'POST',
//                                     async: false,
//                                     data: JSON.stringify(client_habit),
//                                     dataType: 'json',
//                                     success: function(response) {
//
//                                     action =
//                                             '<button type="button" class="btn btn-info habit_edit_button" value="Edit" id="'+ response.id +'"> ' +
//                                             '<i class="fa fa-pencil"></i></button>' ;
//
//                                     action += '<button type="button" class="btn btn-danger habit_delete_button" ' +
//                                         'value="Delete" ' +
//                                         'habit_dd_id ="' + $("#habitNameId").val() + '"'+
//                                         'id="hab-del-'+ response.id +'"> ' +
//                                         '<i class="fa fa-trash"></i></button>';
//
//                                         $("#habit_table_data_id").append("<tr " + "id=row"+ response.id + ">" + "<td>"+ habitName + "</td><td>" + value + "</td><td>"+ action +"</td></tr>");
//                                         showAlert("Successfully Saved");
//                                     },
//                                     error: function(xhr, status, error) {
//                                         showAlert("Something went wrong!");
//                                     }
//                                 });
//                             }
//
//                         },
//                         cancel: function () {
//
//                         }
//                     }
//                 });
//                 habitNameDD.push($("#habitNameId").val());
//             }
//             else {
//                 showAlert("Habit name already added!");
//             }
//
//         }
//
//
//
//     });
//
//     $(document).on("click", ".habit_edit_button", function (e) {
//
//
//             clearErrorMsg();
//             var id = $(this).attr("id");
//             var str = '#' + id;
//
//             $.confirm({
//                 title: 'Confirm!',
//                 content: "Are you sure to edit the data?",
//                 buttons: {
//                     confirm: {
//                         btnClass: 'btn-info',
//                         keys: ['enter'],
//                         action: function () {
//
//                             $.ajax({
//                                 contentType: 'application/json',
//                                 url: "edit-client-habit",
//                                 type: 'GET',
//                                 dataType: "json",
//                                 data: {
//                                     id: id
//                                 },
//                                 success: function(res) {
//                                     // var prop = "<button  type='button' class='btn btn-success' id='edit-client-habit' value='edit' >Edit</button>"
//                                     $("#habitValue").val(res.value); //habitNameId
//                                     $("#habitNameId").val(res.habitId);
//
//                                     $("#habitButtons").html(
//                                         "<button  type='button' class='btn btn-success' id='add-client-habit' value='add'>Add</button>" +
//                                         "<button  type='button' class='btn btn-edit' id='edit-client-habit' value='edit'" + " pk=" + res.id + ">Edit</button>"
//                                     );
//
//                                     //showAlert("Successfully edited!");
//
//                                 },
//                                 error: function(xhr, status, error) {
//                                     showAlert("Something went wrong");
//
//                                 }
//                             });
//                         }
//
//                     },
//                     cancel: function () {
//
//                     }
//                 }
//             });
//
//
//
//
//
//        // $(str).remove();
//     });
//
//     $(document).on("click", "#edit-client-habit", function (e) {
//
//         var res = validate();
//
//         if (res == 1){
//             clearErrorMsg();
//             var id = $(this).attr("pk");
//             var row = "#row"+id;
//
//             var client_habit = {};
//             var habitName = $("#habitNameId option:selected").text();
//             var value = $("#habitValue").val();
//             var habitId = $("#habitNameId").val();
//             var masterId = $("#proposalMasterNo").val();
//
//
//                 $.confirm({
//                     title: 'Confirm!',
//                     content: "Are you sure to save the data",
//                     buttons: {
//                         confirm: {
//                             btnClass: 'btn-info',
//                             keys: ['enter'],
//                             action: function () {
//
//
//                                 action =
//                                     '<button type="button" class="btn btn-info habit_edit_button" value="Edit" id="'+ id +'"> ' +
//                                     '<i class="fa fa-pencil"></i></button>' ;
//
//                                 action += '<button type="button" class="btn btn-danger habit_delete_button" ' +
//                                     'value="Delete"' +
//                                     'habit_dd_id ="' + habitId + '"'+
//                                     ' id="hab-del-'+ id +'"> ' +
//                                     '<i class="fa fa-trash"></i></button>';
//
//                                 // $(row).remove();
//
//                                 $.ajax({
//                                     contentType: 'application/json',
//                                     url: "edit-client-habit-final",
//                                     type: 'GET',
//                                     async: false,
//                                     data: {
//                                         id: id,
//                                         value : value,
//                                         habitId : habitId,
//                                         masterId : masterId,
//                                         habitName : habitName
//                                     },
//                                     dataType: 'json',
//                                     success: function (response) {
//                                         $(row).remove();
//                                         $("#habit_table_data_id").append("<tr" + " id=row"+ id + ">" + "<td>"+ habitName + "</td><td>" + value + "</td><td>"+ action +"</td></tr>");
//                                         showAlert("Successfully edited");
//                                     },
//                                     error: function (xhr, status, error) {
//                                         showAlert("Something went wrong!");
//                                     }
//                                 });
//                             }
//
//                         },
//                         cancel: function () {
//
//                         }
//                     }
//                 });
//
//
//
//         }
//
//
//     });
//
//     $(document).on("click", ".habit_delete_button", function (e) {
//         var id = $(this).attr("id");
//         var habit_dd_id = $(this).attr("habit_dd_id");
//         var deleteId = id.substring(8, id.length);
//         var str = '#row' + deleteId;
//         //$(str).remove();
//
//         $.confirm({
//             title: 'Confirm!',
//             content: "Are you sure to delete the data",
//             buttons: {
//                 confirm: {
//                     btnClass: 'btn-info',
//                     keys: ['enter'],
//                     action: function () {
//
//                         $.ajax({
//                             contentType: 'application/json',
//                             url: "delete-client-habit",
//                             type: 'POST',
//                             async: false,
//                             data: JSON.stringify(deleteId),
//                             dataType: 'json',
//                             success: function (response) {
//                                 if(response.message!='fail'){
//                                     showAlert("Successfully Deleted");
//                                     $(str).remove();
//
//                                     var index = habitNameDD.indexOf(habit_dd_id);
//                                     if (index > -1) {
//                                         habitNameDD.splice(index, 1);
//                                     }
//
//                                 }
//                                 else showAlert("Something went wrong!");
//
//                             },
//                             error: function (xhr, status, error) {
//                                 showAlert("Something went wrong!");
//                             }
//                         });
//
//                     }
//
//                 },
//                 cancel: function () {
//
//                 }
//             }
//         });
//
//
//
//     });
//
// });