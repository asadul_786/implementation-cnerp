function parseNumber(str) {
    if(str==null|| str.trim()==""||str=="undefined"){
        return null;
    }
    else{
        var  intNum=parseInt(str);

        return intNum=="NaN"?null:intNum;
    }

}

$(document).ready(function () {

    $("#add-new-assured").click(function () {

        var flag=ValidatingData();

        if(flag== true) {

            var proposalAssure = {};

            //proposalAssure.pgidsss = $("#pgid").val();
            proposalAssure.partyname = $("#partyname").val();
            proposalAssure.dateofbirth = $("#dateofbirth").val();
            proposalAssure.percentage = $("#percentage").val();
            proposalAssure.partyRelation = $("#partyRelation").val();
            proposalAssure.serialNo =parseNumber($("#slno").val());

            $.ajax({
                contentType: 'application/json',
                url: "/new-business/add-assured",
                type: 'POST',
                async: false,
                data: JSON.stringify(proposalAssure),
                dataType: 'json',
                success: function (response) {
                    //console.log(proposalAssure);
                    //console.log("here client family");
                    // resetClientFamilyForm();
                    //updateTableList();
                    updateTableList();
                    showAlert("Successfully saved");
                    refresh();
                },
                error: function (xhr, status, error) {
                    //alert("Something Wrong,Please check your value!!!");
                }
            });

        }
    });

    function getFormatedDate(strDate) {

        var dateS = new Date(strDate);

        var d = dateS.getDate();
        var m = dateS.getMonth() + 1;
        var y = dateS.getFullYear();


        // return y+'-'+(m <= 9 ? '0' + m : m)+'-'+(d <= 9 ? '0' + d : d);
        return (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + y;

    }
    function updateTableList() {

        var assuredTable = $('#assuredTableId');

        $.ajax({
            type: "GET",
            url: "/new-business/addAssuredList",
            success: function (data) {
                var no = 1;
                var tableBody = "";

                var date= new Date().toString('dd/MM/yyyy');

                $('#assuredTableId tbody').empty();
                $.each(data, function (idx, elem) {
                    //alert (elem[idx]);
                    var edit ='<a class="btn btn-success assuredInformationEdit" value="Edit" id="editAssured"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger assuredInformationDelete" value="Delete" id="deleteAssured"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td hidden>" + elem[2] + "</td>";
                    tableBody += "<td>" + elem[3] + "</td>";
                    // tableBody += "<td>" + elem[1] + "</td>";
                    tableBody += "<td>" +getFormatedDate( elem[4])  + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + elem[5] + "</td>";
                    tableBody += "<td >" + edit + "</td>";
                    tableBody += "<td >" + del + "</td>"
                    tableBody += "<tr>";
                });
                assuredTable.append(tableBody);
            }
        });
    }


    function refresh (){
        $('#partyname').val("");
        $('#dateofbirth').val("");
        $('#percentage').val("");
        $('#partyRelation').val("");
        $('#slno').val("");
        $('#partyCd').val("");
        $('#pgid').val("");
    }
    $( "#dateofbirth" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });

    $("#chkbox").click(function () {

        if ($(this).is(":checked")) {
            var assured=$('#proposalName').val();
            var assured_date=$('#proposerDOB').val();
            var assured_percg=100;
            var assured_rel=11;
            $('#partyname').val(assured);
            $('#dateofbirth').val(assured_date);
            $('#percentage').val(assured_percg);
            $('#partyRelation').val(assured_rel);

        } else {
            $('#partyname').val("");
            $('#dateofbirth').val("");
            $('#percentage').val("");
            $('#partyRelation').val("-1");

        }
    });

    // $('#chkbox').change(function() {
    //     var assured=$('#proposalName').val();
    //     var assured_date=$('#proposerDOB').val();
    //     $('#partyname').val(assured);
    //     $('#dateofbirth').val(assured_date);
    // });

    //$('#assuredTableId tbody').on('click', '#deleteAssured', function () {
        $(document).on("click", "#deleteAssured", function() {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col12 = curRow.find('td:eq(1)').text();
        var col13 = curRow.find('td:eq(2)').text();

        //alert(col1+" "+col12+" "+col13);

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to Delete?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/assured-proposal-info-delete/" + col1 +"/"+ col12 +"/"+ col13,
                        type: 'POST',
                        //async: false,
                       // data: JSON.stringify(data),
                        dataType: 'json',
                        success: function (response) {
                          //  alert(JSON.stringify(data));
                            refresh();
                            showAlert("Successfully deleted");
                            $('#assuredTableId tbody tr').empty();
                            updateTableList();

                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }

            }

        });

    });


    $(document).on("click", "#assuredTableId tbody tr", function() {
        //some think
        // });
        // $('.assuredTableId tbody tr').on('click',  function () {

        // console.log($(this).html());

        var curRow = $(this).closest('tr');

        var pgid = curRow.find('td:eq(0)').text();
        //var sl_no2=$('#slno').val();
        var sl_no = curRow.find('td:eq(1)').text();
        var party_cd = curRow.find('td:eq(2)').text();

        var partyname = curRow.find('td:eq(3)').text();
        var dateofbirth = curRow.find('td:eq(4)').text();
        var partyRelation = curRow.find('td:eq(6)').text();
        var  percentage = curRow.find('td:eq(5)').text();

        $('#pgid').val(pgid);
        $('#slno').val(sl_no);
        $('#partyCd').val(party_cd);

        $('#partyname').val(partyname);
        $('#dateofbirth').val(dateofbirth);
        $('#percentage').val(partyRelation);
        $('#partyRelation').val(percentage);

    });


    function ValidatingData() {

        var status = true;
        var cDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        var cDigit = /^[0-9]+$/;

        if ($("#partyname").val() == "") {
            status = false;
            $("#err_assured_bn_name_id").text("Empty Assured Name field found!!");
            $("#partyname").focus();
        } else $("#err_assured_bn_name_id").text("");

        if ($("#dateofbirth").val()=="") {
            status = false;
            $("#err_assured_bn_dob_id").text("Empty field found!!");
            $("#dateofbirth").focus();
        }else if (!cDate.test($("#dateofbirth").val())) {
            status = false;
            $("#err_assured_bn_dob_id").text("Invalid Dob field found!!");
            $("#dateofbirth").focus();
        } else $("#err_assured_bn_dob_id").text("");
        //
        // if ($("#percentage").val() == "") {
        //     status = false;
        //     $("#err_assured_bn_percent_id").text("Empty Percentage field found!!");
        //     $("#percentage").focus();
        // } else $("#err_assured_bn_percent_id").text("");

        if ($("#percentage").val() == "") {
            status = false;
            $("#err_assured_bn_percent_id").text("Empty field found!!");
            $("#percentage").focus();
        } else if (!cDigit.test($("#percentage").val())){
            status = false;
            $('#err_assured_bn_percent_id').text("Only Digit allow here !!");
            $('#percentage').focus();
        } else $('#err_assured_bn_percent_id').text("");

        if ($("#partyRelation").val() == "-1") {
            status = false;
            $("#err_assured_bn_relation_id").text("Empty Relation field found!!");
            $("#partyRelation").focus();
        } else $("#err_assured_bn_relation_id").text("");

        status= percentageChwck();


        return status;
    }
    function percentageChwck() {
      //  alert("hi.. ");
        var totalPerchantage=0;
        $('#assuredTableId tr').each(function(){
            var percentage=$(this).find('td:eq(3)').text();
            totalPerchantage+=parseFloat( percentage  );
        });
        //alert(totalPerchantage);
        if(totalPerchantage>100)
        {
            return false;
        }
        else {
            return true;
        }
    }
});



