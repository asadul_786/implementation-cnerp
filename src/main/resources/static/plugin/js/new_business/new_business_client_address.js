$(document).ready(function () {

    // $(document).on("change", "#cl_pre_div_id", function (e) {
    //
    //     var divisionCD = $("#cl_pre_div_id option:selected").val();
    //
    //     $.get("/new-business/getDistrict?divisionCD=" + divisionCD,
    //
    //         function (data, status) {
    //
    //             var office = $('#cl_pre_dis_id');
    //             office.empty();
    //             office.append($('<option/>', {
    //                 value: "-1",
    //                 text: "---Select District---"
    //             }));
    //             $.each(data, function (index, branchbank) {
    //                 office.append($('<option/>', {
    //                     value: branchbank[0],
    //                     text: branchbank[1]
    //                 }));
    //
    //             });
    //
    //         });
    // });

    $('#cl_pre_div_id').on('change', function() {
        getDistrictName( $(this).children("option:selected").val());
    });

    $('#cl_pre_dis_id').on('change', function() {
        getThanaName( $(this).children("option:selected").val());
    });

    function getDistrictName(divisionCD){
        //console.log(office_code);
        //alert(occuArea_cd)
        var json = {
            "divisionCD": divisionCD
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/new-business/getDistrict/"+ divisionCD,
            data: JSON.stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
//              console.log(data);
                //alert(JSON.stringify(json));
                $('#cl_pre_dis_id').empty();
                var html = '';
                html += '<option value="-1">--Select District--</option>';
                $.each(data, function (i, l) {

                    html += '<option value="' + l[0] + '">' + l[1] + '</option>';
                });

                $('#cl_pre_dis_id').append(html)
                $("#cl_pre_dis_id").selectpicker("refresh");
//                    console.log(html);
            },
            error: function (e) {
                //console.log('rrr');
            }
        });
    }

    function getThanaName(districtCD){
        //console.log(office_code);
        //alert(occuArea_cd)
        var json = {
            "districtCD": districtCD
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/new-business/getThana/"+ districtCD,
            data: JSON.stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
//                        console.log(data);
                //alert(JSON.stringify(json));
                $('#cl_pre_thana_id').empty();
                var html = '';
                html += '<option value="-1">--Select Thana--</option>';
                $.each(data, function (i, l) {

                    html += '<option value="' + l[0] + '">' + l[1] + '</option>';
                });

                $('#cl_pre_thana_id').append(html)
                $("#cl_pre_thana_id").selectpicker("refresh");
//                    console.log(html);
            },
            error: function (e) {
                //console.log('rrr');
            }
        });
    }

    $('#cl_perss_div_id1').on('change', function() {
        getDistrictName2( $(this).children("option:selected").val());
    });

    $('#cl_per_dis_id1').on('change', function() {
        getThanaName2( $(this).children("option:selected").val());
    });

    function getDistrictName2(divisionCD){
        //console.log(office_code);
        //alert(occuArea_cd)
        var json = {
            "divisionCD": divisionCD
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/new-business/getDistrict/"+ divisionCD,
            data: JSON.stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            async: false,
            success: function (data) {
//                        console.log(data);
                //alert(JSON.stringify(json));
                $('#cl_per_dis_id1').empty();
                var html = '';
                html += '<option value="-1">--Select District--</option>';
                $.each(data, function (i, l) {

                    html += '<option value="' + l[0] + '">' + l[1] + '</option>';
                });

                $('#cl_per_dis_id1').append(html)
                $("#cl_per_dis_id1").selectpicker("refresh");

                if(isSameAddressTiggered){
                    $('#cl_per_dis_id1').val($('#cl_pre_dis_id').val());

                    $("#cl_per_dis_id1").selectpicker("refresh");
                    $('#cl_per_dis_id1').trigger( "change" );

                }

//                    console.log(html);
            },
            error: function (e) {
                //console.log('rrr');
            }
        });
    }

    function getThanaName2(districtCD){
        //console.log(office_code);
        //alert(occuArea_cd)
        var json = {
            "districtCD": districtCD
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/new-business/getThana/"+ districtCD,
            data: JSON.stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            async: false,
            success: function (data) {
//                        console.log(data);
                //alert(JSON.stringify(json));
                $('#cl_per_thana_id1').empty();
                var html = '';
                html += '<option value="-1">--Select Thana--</option>';
                $.each(data, function (i, l) {

                    html += '<option value="' + l[0] + '">' + l[1] + '</option>';
                });

                $('#cl_per_thana_id1').append(html)
                $("#cl_per_thana_id1").selectpicker("refresh");
                if(isSameAddressTiggered){
                    $('#cl_per_thana_id1').val($('#cl_pre_thana_id').val());

                    $("#cl_per_thana_id1").selectpicker("refresh");

                }
//                    console.log(html);
            },
            error: function (e) {
                //console.log('rrr');
            }
        });
    }
//added by mithun
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        if(target=="#tab_content7"){
            $("#profile-tab41").addClass("active");
            $("#tab_content41").addClass("active in ");
            // $("#tab_content41").addClass("in");
        }

    });


    $("#cl_address_save_button").click(function () {

            event.preventDefault();

            var isVaild = validationAddressInfo();
            //  alert("hi");
            if(isVaild) {
                confirmProposalInfoDialog("Are you sure to save Address Info?");
            }
        });



    $(document).on("click", ".js-same-address", function (event) {
        setVisibleField($(this).is(":checked"));

    });
    $(document).on("change", ".text-change-addrs", function (event) {
        if(isSameAddressTiggered){
          //  removeTemporaryAddress();
            $('.switchery').trigger('click');
        }

    });


//added by mithun
    var isSameAddressTiggered=false;
    // $('#cl_same_as_pre_no').prop('checked',true);
    $( ".same-as-mailing-address" ).change(function() {
        $('#cl_same_as_pre_no').prop('checked',true);
    });
//end here
      function setVisibleField(isSelected) {
        var val_checked = $('input[name=sameAs]:checked').val();

        if (isSelected) {
            getTemporaryAddress();
        }
        else{
            removeTemporaryAddress();
        }
    };

    function getTemporaryAddress() {

        isSameAddressTiggered=true;
        console.log(isSameAddressTiggered);
        $('#cl_per_add_id1').val($('#cl_press_add_id').val()); //vill moholla
        $('#cl_perss_div_id1').val($('#cl_pre_div_id').val());
        $('#cl_perss_div_id1').trigger( "change" );

        $('#addpzipno1').val($('#addtzipno').val());
        $('#postOff').val($('#prePostOff').val());
        $('#addpphoneno').val($('#addtphoneno').val());
        $('#cl_per_country_id1').val($('#cl_press_country_id').val());
        $("#cl_per_thana_id1").selectpicker("refresh");


    }
    function removeTemporaryAddress() {
        isSameAddressTiggered=false;
        $('#cl_per_add_id1').val("");
        $('#cl_perss_div_id1').val("-1");
        $('#cl_per_dis_id1').val("-1");
        $('#cl_per_thana_id1').val("-1");
        $('#addpzipno1').val("");
        $('#addpphoneno').val("");
        $('#postOff').val("");
        $('#cl_per_dis_id1').selectpicker('refresh');
        $('#cl_per_thana_id1').selectpicker('refresh');

    }

    var confirmProposalInfoDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        var cl_party_id = $("#cl_party_id").val();

                        var cl_pre_add_id = $("#cl_press_add_id").val();
                        var cl_pre_div_id = $("#cl_pre_div_id").val();
                        var cl_pre_dis_id = $("#cl_pre_dis_id").val();
                        var cl_pre_thana_id = $("#cl_pre_thana_id").val();
                        var pre_addtzipno = $("#addtzipno").val();
                        var pre_addtphoneno =  $("#addtphoneno").val();
                        var cl_press_country_id = $("#cl_press_country_id").val();

                        var pre_postOfice = $("#prePostOff").val();


                        //var cl_party_id = $("#cl_party_id").val();

                        var cl_per_add_id = $("#cl_per_add_id1").val();
                        var cl_per_div_id = $("#cl_perss_div_id1").val();
                        var cl_per_dis_id = $("#cl_per_dis_id1").val();
                        var cl_per_thana_id = $("#cl_per_thana_id1").val();
                        var per_addtzipno = $("#addpzipno1").val();
                        var cl_per_phone = $("#addpphoneno").val();
                        var cl_per_country = $("#cl_per_country_id1").val();

                        var per_postOfice = $("#postOff").val();



                        var proposalAddress = {};

                        proposalAddress.cl_party_id=cl_party_id;

                        proposalAddress.cl_press_add_id =cl_pre_add_id;
                        proposalAddress.cl_pre_div_id =cl_pre_div_id;
                        proposalAddress.cl_pre_dis_id =cl_pre_dis_id;
                        proposalAddress.cl_pre_thana_id =cl_pre_thana_id;
                        proposalAddress.addtzipno =pre_addtzipno;
                        proposalAddress.addtphoneno =pre_addtphoneno;
                        proposalAddress.cl_press_country_id =cl_press_country_id;

                        proposalAddress.prePostOff =pre_postOfice;

                        proposalAddress.cl_per_add_id1 = cl_per_add_id;
                        proposalAddress.cl_perss_div_id1 = cl_per_div_id;
                        proposalAddress.cl_per_dis_id1 = cl_per_dis_id;
                        proposalAddress.cl_per_thana_id1 = cl_per_thana_id;
                        proposalAddress.addpzipno1 = per_addtzipno;
                        proposalAddress.addpphoneno = cl_per_phone;
                        proposalAddress.cl_per_country_id1 =cl_per_country;
                        proposalAddress.postOff =per_postOfice;

                        //  alert("Address : "+ proposalAddress.cl_press_add_id);
                        //  alert("Division  : "+ proposalAddress.cl_pre_div_id);
                        //  alert("District  : "+ proposalAddress.cl_pre_dis_id);
                        //  alert("Thana  : "+ proposalAddress.cl_pre_thana_id);
                        //  alert("zip  : "+ proposalAddress.addtzipno);
                        //  alert("phone  : "+ proposalAddress.addtphoneno);
                        //  alert("Country  : "+ proposalAddress.cl_press_country_id);
                        //
                        // alert("Address2  : "+ proposalAddress.cl_per_add_id1);
                        // alert("Division2 : "+ proposalAddress.cl_perss_div_id1);
                        // alert("District2 : "+ proposalAddress.cl_per_dis_id1);
                        // alert("Thana2 : "+ proposalAddress.cl_per_thana_id1);
                        // alert("zip2 : "+ proposalAddress.addpzipno1);
                        // alert("phone2 : "+ proposalAddress.addpphoneno);
                        // alert("Country2 : "+ proposalAddress.cl_per_country_id1);


                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/new-business/add-address",
                            data: JSON.stringify(proposalAddress),
                            dataType: 'json',
                            success: function (data) {
                                // alert(data);
                                showAlert("Successfully saved.");
                                // clearform();
                            },
                            error: function (e) {
                                showAlert("Something Wrong!!");
                            }
                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };


    function validationAddressInfo() {

        var status = true;
        var cMb = /^((?=(01))[0-9]{11}|)$/;
        var cDigit = /^[0-9]+$/;

        if ($("#cl_press_add_id").val() == "") {
            status = false;
            $("#err_cl_press_add_id").text("Empty field found!!");
            $("#cl_press_add_id").focus();
        } else $("#err_cl_press_add_id").text("");

        if ($("#cl_pre_div_id").val() == "-1") {
            status = false;
            $("#err_cl_pre_div_id").text("Empty field found!!");
            $("#cl_pre_div_id").focus();
        } else $("#err_cl_pre_div_id").text("");

        if ($("#cl_pre_dis_id").val() == "-1") {
            status = false;
            $("#err_cl_pre_dis_id").text("Empty field found!!");
            $("#cl_pre_dis_id").focus();
        } else $("#err_cl_pre_dis_id").text("");

        if ($("#cl_pre_thana_id").val() == "-1") {
            status = false;
            $("#err_cl_pre_thana_id").text("Empty field found!!");
            $("#cl_pre_thana_id").focus();
        } else $("#err_cl_pre_thana_id").text("");

      if (!cDigit.test($("#addtphoneno").val())){
            status = false;
            $('#err_cl_pre_phone_id').text("Invalid field value found !!");
            $('#addtphoneno').focus();
        } else $('#err_cl_pre_phone_id').text("");

      if(!cDigit.test($("#addtzipno").val())) {
            status = false;
            $("#err_cl_pre_zip_id").text("Invalid field value found !!");
            $("#addtzipno").focus();
        } else $("#err_cl_pre_zip_id").text("");

         if ($("#cl_press_country_id").val()=="-1") {
                    status = false;
                    $("#err_cl_press_country_id").text("Empty field found!!");
                    $("#cl_press_country_id").focus();
                } else $("#err_cl_press_country_id").text("");


        if ($("#cl_party_id").val()== "-1") {
            status = false;
            $("#err_cl_party_id").text("Empty field found!!");
            $("#cl_party_id").focus();
        } else $("#err_cl_party_id").text("");

        return status;
    }

});


