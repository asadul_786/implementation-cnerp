function parseNumber(str) {
    if(str==null|| str.trim()==""||str=="undefined"){
        return null;
    }
    else{
        var  intNum=parseInt(str);

        return intNum=="NaN"?null:intNum;
    }

}

$(document).ready(function () {

    $("#nb_nom_save_button").click(function () {

        var flag=ValidatingData();
           //  alert("hi");
        if(flag== true){

            //var nompgidss =  $("#pgidss").val();

            var nb_nom_asrd_id = $("#nb_nom_asrd_id").val();
            var nompartyname = $("#nompartyname").val();
            var nomdateofbirth = $("#nomdateofbirth").val();
            var nompercentage = $("#nompercentage").val();
            var nompartyRelation = $("#nompartyRelation").val();
            var sl_no = parseNumber($("#sl_nomin").val());

            // alert(pgidss);

            var proposalNominee = {};

            //proposalNominee.nompgidss=nompgidss;
            proposalNominee.nomassuredid = nb_nom_asrd_id;
            proposalNominee.nompartyname = nompartyname;
            proposalNominee.nomdateofbirth = nomdateofbirth;
            proposalNominee.nompercentage = nompercentage;
            proposalNominee.nompartyRelation = nompartyRelation;
            proposalNominee.nomserialNo = sl_no;
            //
            // alert(proposalNominee.nomassuredid);
            // alert(proposalNominee.nompartyname);
            // alert(proposalNominee.nomdateofbirth);
            // alert(proposalNominee.nompercentage);
            // alert(proposalNominee.nompartyRelation);

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/new-business/add-nominee",
                data: JSON.stringify(proposalNominee),
                dataType: 'json',
                success: function (data) {

                    if (data == false) {
                        showAlertByType('Nominee Percentage cant be more 100%!!', "W");
                    } else {
                        // alert(data);
                        updateTableList();
                        showAlert("Successfully Saved!");
                        refresh();
                    }
                },
                error: function (e) {
                    showAlert("Something Wrong!!!");
                }
            });
        }
    });


    function ValidatingData() {

        var status = true;
        var cDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        var cDigit = /^[0-9]+$/;
        var cChar = /^[a-zA-Z]+$/;

        // if ($("#nompartyname").val() == "") {
        //     status = false;
        //     $("#err_nb_nom_name_id").text("Empty field found!!");
        //     $("#nompartyname").focus();
        // } else $("#err_nb_nom_name_id").text("");

        if ($("#nompartyname").val()=="") {
            status = false;
            $("#err_nb_nom_name_id").text("Empty field found!!");
            $("#nompartyname").focus();
        } else $("#err_nb_nom_name_id").text("");
        // } else if (!cChar.test($("#nompartyname").val())) {
        //     status = false;
        //     $("#err_nb_nom_name_id").text("Invalid field value found");
        //     $("#nompartyname").focus();
        //

        if ($("#nomdateofbirth").val()=="") {
            status = false;
            $("#err_nb_nom_dob_id").text("Empty field found!!");
            $("#nomdateofbirth").focus();
        } else if (!cDate.test($("#nomdateofbirth").val())) {
            status = false;
            $("#err_nb_nom_dob_id").text("Invalid date format found!!");
            $("#nomdateofbirth").focus();
        } else $("#err_nb_nom_dob_id").text("");

        // if ($("#nompercentage").val() == "") {
        //     status = false;
        //     $("#err_nb_nom_perch_id").text("Empty field found!!");
        //     $("#nompercentage").focus();
        // } else $("#err_nb_nom_perch_id").text("");

        if ($("#nompercentage").val() == "") {
            status = false;
            $("#err_nb_nom_perch_id").text("Empty field found!!");
            $("#nompercentage").focus();
        } else if (!cDigit.test($("#nompercentage").val())){
            status = false;
            $('#err_nb_nom_perch_id').text("Invalid field value found !!");
            $('#nompercentage').focus();
        } else $('#err_nb_nom_perch_id').text("");

        if ($("#nompartyRelation").val() == "-1") {
            status = false;
            $("#err_nb_nom_rel_id").text("Empty field found!!");
            $("#nompartyRelation").focus();
        } else $("#err_nb_nom_rel_id").text("");
        if ($("#nb_nom_asrd_id").val() == "-1") {
            status = false;
            $("#err_nb_nom_asrd_id").text("Empty field found!!");
            $("#nb_nom_asrd_id").focus();
        } else $("#err_nb_nom_asrd_id").text("");

        return status;
    }


    function getFormatedDate(strDate) {

        var dateS = new Date(strDate);

        var d = dateS.getDate();
        var m = dateS.getMonth() + 1;
        var y = dateS.getFullYear();


        // return y+'-'+(m <= 9 ? '0' + m : m)+'-'+(d <= 9 ? '0' + d : d);
        return (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + y;

    }

    // var saveNotification = /*[[${saveMess}]]*/ '';
    // if (saveNotification != undefined) {
    //     alert("hi..");
    //     showAlertByType(saveNotification, "S");
    // }

    function updateTableList() {
        //alert("hi");
        var nomineeTable = $('#nomineeTableId');

        $.ajax({
            type: "GET",
            url: "/new-business/addNomineeList",
            success: function (data) {
                // alert(data);
                var no = 1;
                var tableBody = "";

                $('#nomineeTableId tbody').empty();
                $.each(data, function (idx, elem) {
                    //  alert (elem[idx]);

                    var edit = '<a class="btn btn-success NomineeInformationEdit" value="Edit" id="editNominee"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger NomineeInformationDelete" value="Delete" id="deleteNominee"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";

                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td hidden>" + elem[2] + "</td>";
                    tableBody += "<td hidden class='nm-party-type'>" + elem[3] + "</td>";
                    tableBody += "<td>" + elem[4] + "</td>";
                    tableBody += "<td>" + getFormatedDate(elem[5]) + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + elem[7] + "</td>";
                    tableBody += "<td>" + edit + "</td>"
                    tableBody += "<td>" + del + "</td>"
                    tableBody += "<tr>";
                });
                nomineeTable.append(tableBody);
            }
        });
    }

    $(document).on("click", "#deleteNominee", function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();

     //   alert(col1);
       // alert(col2);
       // alert(col3);

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to Delete?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/nominee-proposal-info-delete/" + col1 +"/"+ col2 +"/"+ col3,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {
                           // alert("ho..");
                            refresh2();
                            showAlert("Successfully deleted");
                            $('#nomineeTableId tbody tr').empty();
                            updateTableList();

                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });


    function refresh (){
        $('#nb_nom_asrd_id').val("-1");
        $('#nompartyname').val("");
        $('#nomdateofbirth').val("");
        $('#nompercentage').val("");
        $('#pgid').val("");
        $('#nompartyRelation').val("");
        $('#sl_nomin').val("");
        $('#nm_party').val("");


    }

    $( "#nomdateofbirth" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });

    $(document).on("click", "#profile-tab4", function (event) {
        loadAssuredDropdown();
        // updatePayorTable();
    });


    var loadAssuredDropdown = function () {
        // var pgId = $("#pgid").val();
        //alert(pgId);

        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: "/new-business/get-payor-assured",
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                // alert(data);
                var html = '';
                html += '<option value="-1">--Select Assured--</option>';
                $.each(data, function (i, l) {
                    html += '<option value="' + l[0] + '">'
                        + l[1]
                        + '</option>';
                });
                html += '</option>';

                $('#nb_nom_asrd_id').html(html);
                //alert('accss the data');
            },
            error: function (e) {
                showAlert("Something wrong!!!");
            }
        });
    }

    function refresh2(){
        $('#nb_nom_asrd_id').val("-1");
        $('#nompartyname').val("");
        $('#nomdateofbirth').val("");
        $('#nompercentage').val("");
        $('#pgid').val("");
        $('#sl_nomin').val("");
        $('#nm_party').val("");
    }

    $(document).on("click", "#nomineeTableId tbody tr", function() {
        //alert("hi....");
        var curRow = $(this).closest('tr');

        var pgid = curRow.find('td:eq(0)').text();
        var sl_no = curRow.find('td:eq(1)').text();
        var party_cd = curRow.find('td:eq(2)').text();
        var party = curRow.find('td:eq(3)').text();
        var nompartyname = curRow.find('td:eq(4)').text();
        var nomdateofbirth = curRow.find('td:eq(5)').text();
        var  nompercentage= curRow.find('td:eq(6)').text();
        var  nompartyRelation= curRow.find('td:eq(7)').text();

        $('#pgid').val(pgid);
        $('#sl_nomin').val(sl_no);
        $('#nm_party').val(party_cd);

        $('#nb_nom_asrd_id').val(( $(this).find(".nm-party-type").html()));
        $('#nompartyname').val(nompartyname);
        $('#nomdateofbirth').val(nomdateofbirth);
        $('#nompercentage').val(nompercentage);
        $('#nompartyRelation').val(nompartyRelation);

    });



});


//
//
// function getLastPGIDINSPPRoposal() {
//     var json = {
//         "pgid": ""
//     };
//
//     $.ajax({
//         type: "POST",
//         contentType: "application/json",
//         url: "/new-business/getLastPGID",
//         data: JSON
//             .stringify(json),
//         dataType: 'json',
//         cache: false,
//         timeout: 600000,
//         success: function (data) {
//
//             $("#pgidss").val(data);
//
//         },
//         error: function (e) {
//
//         }
//     });
// }


// function getLastNextPGIDINSPPRoposal(pgid) {
//
//     var json = {
//         "officeId": ""
//     };
//
//     $.ajax({
//         type: "POST",
//         contentType: "application/json",
//         url: "/new-business/getLastNextPGID/" + pgid,
//         data: JSON
//             .stringify(json),
//         dataType: 'json',
//         cache: false,
//         timeout: 600000,
//         success: function (data) {
//             //$('#pgidss').val(data);
//         },
//         error: function (e) {
//         }
//     });
// }



// var updatePayorTable = function () {
//     var tableData = $("#payor_table_data_id");
//     masterId = $('#proposalMasterNo').val();
//     $.ajax({
//         contentType: 'application/json',
//         url: "get-proposal-payor-info",
//         type: 'POST',
//         async: false,
//         data: JSON.stringify(masterId),
//         dataType: 'json',
//         success: function (response) {
//             tableData.html("");
//             html = "";
//             $.each(response, function(idx, elem){
//                 html = "";
//                 html += "<tr>";
//                 html += "<td>" + elem.assuredName + "</td>";
//                 html += "<td>" + elem.payorName + "</td>";
//                 html += "<td>" + elem.payorDOB + "</td>";
//                 html += "<td>" + elem.relationWithAssured + "</td>";
//                 html += "<td>" + elem.payorPremium + "</td>";
//                 html += "<td>" + '<button type="button" class="btn btn-info payor_edit_button" value="Edit"  button_id="'+ elem.payorId +'"> ' +
//                     '<i class="fa fa-pencil"></i></button>' + '<button type="button" class="btn btn-danger payor_delete_button" value="Delete" button_id="'+ elem.payorId +'"> '   +
//                     '<i class="fa fa-trash"></i></button>'+ "</td>";
//                 html += "<tr>";
//                 tableData.append(html);
//             });
//         },
//         error: function (xhr, status, error) {
//         }
//     });
// };
//
// $(document).on("click", "#profile-tab3", function (event) {
//     loadAssuredDropdown();
//     updatePayorTable();
// });
//
// var loadAssuredDropdown = function () {
//     var masterId = $("#proposalMasterNo").val();
//     $.ajax({
//         contentType: 'application/json',
//         url: "get-payor-assured-list",
//         type: 'POST',
//         async: false,
//         data: JSON.stringify(masterId),
//         dataType: 'json',
//         success: function (response) {
//             var asrdList = $('#assuredNameDropdown');
//             asrdList.empty();
//             asrdList.append($('<option/>', {
//                 value: "-1",
//                 text: "--Select--"
//             }));
//             $.each(response, function (index, asrd) {
//                 asrdList.append($('<option/>', {
//                     value: asrd.id,
//                     text: asrd.displayName
//                 }));
//             });
//         },
//         error: function (xhr, status, error) {
//         }
//     });
// }
//
// var confirmPayorDelete = function (text, row_id) {
//     $.confirm({
//         title: 'Confirm!',
//         content: text,
//         buttons: {
//             confirm: {
//                 btnClass: 'btn-info',
//                 keys: ['enter'],
//                 action: function () {
//                     $.ajax({
//                         contentType: 'application/json',
//                         url: "del-proposal-payor",
//                         type: 'POST',
//                         async: false,
//                         data: JSON.stringify(row_id),
//                         dataType: 'json',
//                         success: function (response) {
//                             if (response.isDeleted) {
//                                 showAlertByType("Successfully deleted.", "S");
//                                 updatePayorTable();
//                             }
//                             else {
//                                 showAlertByType(response.errorCause, "F");
//                             }
//                         },
//                         error: function (xhr, status, error) {
//                             showAlertByType("Failed to delete payor", "F");
//                         }
//                     });
//                 }
//             },
//             cancel: function () {
//
//             }
//         }
//     });
// }
//
// function  assuredNameValid() {
//     $('#assuredNameDropdown').next("span").remove();
//
//     if ($('#assuredNameDropdown').val() == -1) {
//         $('#assuredNameDropdown').after('<span class=error>Choose an Assured</span>');
//         return false;
//     }
//     else {
//         return true;
//     }
// }
//
// function payorNameValid() {
//     $('#payorName').next("span").remove();
//
//     var nameCheck = new RegExp("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$");
//
//     if (nameCheck.test($('#payorName').val())) {
//         return true;
//     }
//     else {
//         $('#payorName').after('<span class= error>Invalid Name</span>');
//         return false;
//     }
// }
//
// function  dobValid() {
//     $('#dobPayor').next("span").remove();
//
//     var dobCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");
//     if (dobCheck.test($('#dobPayor').val())) {
//         return true;
//     }
//     else {
//         $('#dobPayor').after('<span class=error>Invalid Date</span>');
//         return false;
//     }
// }
//
// function premiumValid() {
//     $('#payorPremiumPercent').next("span").remove();
//
//     var a = $('#payorPremiumPercent').val();
//
//     if (a > 0 && a <= 100) {
//         return true;
//     }
//     else {
//         $('#payorPremiumPercent').after('<span class= error>Invalid Premium Percent</span>');
//         return false;
//     }
// }
//
// function payorRelationValid() {
//     $('#payorRelation').next("span").remove();
//
//     if ($('#payorRelation').val() == -1) {
//         $('#payorRelation').after('<span class=error>Choose a relation</span>');
//         return false;
//     }
//     else {
//         return true;
//     }
// }
//
// $('#payorName').change(function () {
//     payorNameValid();
// });
//
// $('#dobPayor').change(function () {
//     dobValid();
// });
//
// $('#payorPremiumPercent').change(function () {
//     premiumValid();
// });
//
// $('#payorRelation').change(function () {
//     var isRelValid = payorRelationValid();
//
//     if (isRelValid) {
//         if ($("#payorRelation option:selected").text() == "Self"){
//             getPayorSelf();
//         }
//         else {
//
//         }
//     }
// });
//
// $('#assuredNameDropdown').change(function () {
//     assuredNameValid();
// })
//
// function validatePayorForm() {
//     // $('span.error').remove();
//     assuredNameValid();
//     payorNameValid();
//     dobValid();
//     premiumValid();
//     payorRelationValid();
//
//     if ($("span").hasClass("error")) {
//         return false;
//     }
//     else {
//         return true;
//     }
// }
//
// function getPayorSelf() {
//     var masterId = $('#proposalMasterNo').val();
//     $.ajax({
//         contentType: 'application/json',
//         url: "get-proposal-self",
//         type: 'POST',
//         async: false,
//         data: JSON.stringify(masterId),
//         dataType: 'json',
//         success: function (response) {
//             $("#payorName").val(response.proposerName);
//             $("#dobPayor").val(response.proposerDOB);
//         },
//         error: function (xhr, status, error) {
//         }
//     });
// };
//
// // add new payor
// $("#add-new-payor").click(function (event) {
//     event.preventDefault();
//     var isVaild = validatePayorForm();
//     if(isVaild) {
//         confirmPayorSaveDialog("Are you sure to save payor?");
//     }
// });
//
// var confirmPayorSaveDialog = function (text) {
//     $.confirm({
//         title: 'Confirm!',
//         content: text,
//         buttons: {
//             confirm: {
//                 btnClass: 'btn-info',
//                 keys: ['enter'],
//                 action: function () {
//                     var proposal_payor = {};
//
//                     proposal_payor.masterId = $('#proposalMasterNo').val();
//                     proposal_payor.assuredId = $("#assuredNameDropdown").find('option:selected').val();
//                     proposal_payor.payorName = $("#payorName").val();
//                     proposal_payor.payorDOB = $("#dobPayor").val();
//                     proposal_payor.payorPremium = $("#payorPremiumPercent").val();
//                     proposal_payor.relationId = $("#payorRelation").find('option:selected').val();
//                     // proposal_payor.payorId =  $('#add-new-payor').data('id');
//                     proposal_payor.payorId =  $('#nb_payor_id').val();
//                     $.ajax({
//                         contentType: 'application/json',
//                         url:  "add-proposal-payor",
//                         type: 'POST',
//                         async: false,
//                         data: JSON.stringify(proposal_payor),
//                         dataType: 'json',
//                         success: function(response) {
//                             if(response == true){
//                                 updatePayorTable();
//                                 resetPayorForm();
//                                 showAlert("Successfully updated");
//                             }
//                             if(response == false) {
//                                 showAlert("Cannot updated due to input perchantage error");
//                             }
//                         },
//                         error: function(xhr, status, error) {
//                         }
//                     });
//                 }
//
//             },
//             cancel: function () {
//
//             }
//         }
//     });
// }
//
// $(document).on("click", ".payor_edit_button", function (event) {
//     editPayor(this);
// });
//
// $(document).on("click", ".payor_delete_button", function (event) {
//     deletePayor(this);
// });
//
// function  editPayor(identifier) {
//     var row_id = $(identifier).attr("button_id");
//     console.log("row_id: " + row_id);
//
//     $.ajax({
//         contentType: 'application/json',
//         url: "get-proposal-payor-info-dtl",
//         type: 'POST',
//         async: false,
//         data: JSON.stringify(row_id),
//         dataType: 'json',
//         success: function (response) {
//
//
//             $("#assuredNameDropdown").val(response.assuredId);
//             $("#payorName").val(response.payorName);
//             $("#dobPayor").val(response.payorDOB);
//             $("#payorPremiumPercent").val(response.payorPremium);
//             $("#payorRelation").val(response.relationId);
//             // $('#add-new-payor').data('id', row_id);
//             $('#nb_payor_id').val(row_id);
//         },
//         error: function (xhr, status, error) {
//         }
//     });
//
// }
//
// function deletePayor() {
//     // alert("Delete under progress");
// }
//
// var resetPayorForm = function () {
//     $('#assuredNameDropdown').val("-1");
//     $('#dobPayor').val("");
//     $('#payorName').val("");
//     $('#payorPremiumPercent').val('');
//     $('#payorRelation').val("-1");
//     // $('#add-new-payor').data('id',"0");
//     $('#nb_payor_id').val('0');
// };
//
// $(document).on("click", ".payor_delete_button", function (event) {
//     var row_id = $(this).attr("button_id");
//     confirmPayorDelete("Are you sure to delete this Payor?", row_id);
// });

