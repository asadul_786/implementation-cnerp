/*author:moahsin
* 30-March-2020*/


function getDateShow(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

$(document).ready(function () {

});
function getPolicyInfo(pgid){

    $.confirm({
        title: 'Message',
        content: 'Are You Sure To Preview This Policy Info',
        buttons: {
            ok: function () {
                window.open('/policySummReport/genPolicyInfoReport?pgid='+pgid, '_blank');
            },
            cancel: function () {
            },
        }
    });
}