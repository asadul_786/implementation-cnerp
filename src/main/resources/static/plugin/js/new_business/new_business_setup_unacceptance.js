/**
 * Created by Asad on 28-Mar-19.
 */

$(document).ready(function () {

    $('.cust-disabled').attr('disabled','disabled');

    //$('.uwSection').css("display","none");
   // $('.row').css("display","none");

    //
    //$('.collapse-link').click(function () {
      //  $('.x_content').css("display","n");
    //     if($("input[name='prod_staff_rebate']:checked").attr('value') == "1"){
    //         $('#staffRebateEmpl').css("display","");
    //     }else if ($("input[name='prod_staff_rebate']:checked").attr('value') == "0") {
    //         $('#staffRebateEmpl').css("display", "none");
    //     }else {
    //         $('#staffRebateEmpl').css("display","none");
    //     }
    //
    // });

    // if(!$("input:radio[class='prod_staff_rebate']").is(":checked")) {
    //     $('#st



    $("#unLoad").click(function () {

            var asssType=$('#assuranceType').val();

            var prodCd=$('#productcd').val();
            var payMode=$('#paymodecd').val();
            var termt=$('#productterm').val();
            var age2=$('#productage').val();
            var sumAss=$('#sumofassured').val();
            var optCd=$('#optioncd').val();
            var occupat=$('#occupation').val();
            var supben=$('#nbprodBenid').val();


            $('#v_product_cd').val(prodCd);
            $('#v_pay_mode_cd').val(payMode);
            $('#v_term').val(termt);
            $('#v_age').val(age2);
            $('#v_main_sum_assured').val(sumAss);
            $('#optCd').val(optCd);
            $('#unOccupation').val(occupat);
            $('#supBName').val(supben);


           // $('#yearlyPremium').val(yInstallMnt);
             $('#unASSURANCE_TYPE').val(asssType);

            //$('#obtCd').val(obtCd);
            //$('#instPrem').val(instPrem);


    });

        $("#acceptPolicy").click(function (event) {

            event.preventDefault();
            var isVaild = validationLoadInfo();

            if (isVaild) {
                confirmLoadingInfoDialog("Are you sure to Loading Data...?");
           }

        });

     var confirmLoadingInfoDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        var propUnAct = {};

                        var staff_rebate = $("input[name='prod_staff_rebate']:checked").attr('value');
                        var assurance_type = $("input[name='assuranceType']:checked").attr('value');
                        propUnAct.v_product_cd = $('#v_product_cd').val();
                        propUnAct.v_pay_mode_cd = $('#v_pay_mode_cd').val();
                        propUnAct.v_term = $('#v_term').val();
                        propUnAct.v_age = $('#v_age').val();
                        propUnAct.v_main_sum_assured = $('#v_main_sum_assured').val();
                        propUnAct.optCd = $('#optCd').val();
                        propUnAct.assLifeClass = $('#assLifeClass').val();
                        propUnAct.accLifeClass = $('#accLifeClass').val();

                        propUnAct.p_life_premium_yrly = $('#p_life_premium_yrly').val();
                        propUnAct.p_life_premium_inst = $('#p_life_premium_inst').val();
                        propUnAct.p_total_rebate = $('#p_total_rebate').val();
                        //propUnAct.v_staff_rebate_flag=$('#v_staff_rebate_flag').val();
                        //propUnAct.v_staff_rebate_flag=$('#prod_staff_rebate').val();
                        propUnAct.v_staff_rebate_flag=staff_rebate;
                        //propUnAct.v_staff_rebate_flag=staff_rebate;

                        propUnAct.p_pay_mode_rebate_inst = $('#p_pay_mode_rebate_inst').val();
                        propUnAct.p_staff_rebate_inst = $('#p_staff_rebate_inst').val();
                        propUnAct.p_sum_as_rebate_inst = $('#p_sum_as_rebate_inst').val();

                        propUnAct.p_supp_total_extra = $('#p_supp_total_extra').val();
                        propUnAct.p_net_life_premium_inst = $('#p_net_life_premium_inst').val();
                        propUnAct.p_net_life_premium_yrly = $('#p_net_life_premium_yrly').val();
                        propUnAct.v_extra_cd = $('#v_extra_cd').val();
                        propUnAct.v_extra_inst = $('#v_extra_inst').val();

                       // propUnAct.unASSURANCE_TYPE = $('#unASSURANCE_TYPE').val();
                        propUnAct.unASSURANCE_TYPE = assurance_type;
                        propUnAct.accType = $('#accType').val();
                        propUnAct.accLien = $('#accLien').val();

                        propUnAct.unRemarks = $('#unRemarks').val();

                        //
                        // alert(propUnAct.v_product_cd)
                        // alert(propUnAct.v_pay_mode_cd)
                        // alert(propUnAct.v_term)
                        // alert(propUnAct.v_age)
                        // alert(propUnAct.v_main_sum_assured)
                        // alert(propUnAct.optCd)
                        // alert(propUnAct.assLifeClass)
                        // alert(propUnAct.accLifeClass)
                        //
                        // alert(propUnAct.p_life_premium_yrly)
                        // alert("prem not null "+propUnAct.p_life_premium_inst)
                        // alert(propUnAct.p_total_rebate)
                        //
                        // alert(propUnAct.p_pay_mode_rebate_inst)
                        // alert(propUnAct.p_staff_rebate_inst)
                        // alert(propUnAct.p_sum_as_rebate_inst)
                        //
                        // alert(propUnAct.p_supp_total_extra)
                        // alert(propUnAct.p_net_life_premium_inst)
                        // alert(propUnAct.p_net_life_premium_yrly)
                        // alert(propUnAct.v_extra_inst)
                        //
                        //   alert(propUnAct.unASSURANCE_TYPE)
                        //alert(propUnAct.accType)
                        // alert("lien cd"+propUnAct.accLien)
                        // alert("remarks"+propUnAct.remark)
                        //alert(propUnAct.v_staff_rebate_flag);

                        //alert(propUnAct.v_staff_rebate_flag)

                        $.ajax({
                            contentType: 'application/json',
                            url: "/new-business/addUnderWritingPolicy",
                            type: 'POST',
                            //async: false,
                            data: JSON.stringify(propUnAct),
                            dataType: 'json',
                            success: function (response) {
                                //alert(JSON.stringify(propUnAct));
                                showAlert("Successfully saved");
                                // $("#master_tab").show();
                                // $("#btn_newbusinesssave").attr("disabled", true);
                                // $("#btnRefresh").attr("disabled", true);
                            },
                            error: function (xhr, status, error) {
                                showAlertByType("Something Wrong!!!",'F');
                            }

                        });
                    }

                },
                cancel: function () {

                }
            }
        });
    };

    // function validationLoadInfo() {
    //     // alert(document.forms["proposalSearchPanel"]["officeCode"].value);
    //     var x = document.forms["uw_form"]["accType"].value;
    //     // console.log(x);
    //     if (isEmptyString(x)) {
    //         customAlert(alertTypes.WARNING,"Required!","Acceptance Type is required ")
    //         //showAlertByType("Servicing office is required!","W");
    //         // $('.requiredF').attr("display","none");
    //         return false;
    //     }
    // }

    function validationLoadInfo() {
        var status = true;

        if ($("#accType").val() == "-1") {
            status = false;
            $("#err_accType").text("Empty field found!!");
            $("#accType").focus();
        } else $("#err_accType").text("");

        // if ($("#accLien").val() == "-1") {
        //     status = false;
        //     $("#err_lienTyp").text("Empty field found!!");
        //     $("#accLien").focus();
        // } else $("#err_lienTyp").text("");

        return status;

    }

   // $("#unCalculate").click(function () {

        $("#unCalculate").click(function (event) {

            event.preventDefault();
            var isVaild = validationCalInfo();

            if (isVaild) {
                confirmCalculatInfoDialog("Are you sure to Do Calculation...?");
            }

        });

    $('#previewReport').click(function () {
        alert("Policy Summery Sheet Report is under development !!");
    });



    var confirmCalculatInfoDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        // var inPgid = $('#p_pgid').val();
                        var inPodCode=$('#v_product_cd').val();
                        var inPayMode=$('#v_pay_mode_cd').val();
                        var inTerm =$('#v_term').val();
                        var inAge=$('#v_age').val();
                        var inSumAssure=$('#v_main_sum_assured').val();

                       // var inStaffRebat=$('#v_staff_rebate_flag').val();
                        var staff_rebate = $("input[name='prod_staff_rebate']:checked").attr('value');
                        var v_supp_code = $('#v_supp_cd').val();
                        var v_extra_cd = $('#v_extra_cd').val();
                        var v_extra_inst = $('#v_extra_inst').val();

                        var inParameterVal = {};

                        //inParameterVal.p_pgid = inPgid;
                        inParameterVal.v_product_cd = inPodCode;
                        inParameterVal.v_pay_mode_cd = inPayMode;
                        inParameterVal.v_term = inTerm;
                        inParameterVal.v_age = inAge;
                        inParameterVal.v_main_sum_assured = inSumAssure;

                        inParameterVal.v_staff_rebate_flag = staff_rebate;
                        inParameterVal.v_supp_cd = v_supp_code;

                        inParameterVal.v_extra_cd = v_extra_cd;
                        inParameterVal.v_extra_inst = v_extra_inst;

                         // alert(inParameterVal.v_staff_rebate_flag );
                        //  alert(inParameterVal.v_product_cd);
                        //  alert(inParameterVal.v_pay_mode_cd);
                        //  alert(inParameterVal.v_term);
                        //  alert(inParameterVal.v_age);
                        //  alert(inParameterVal.v_main_sum_assured);
                        // alert("staff_rebate_flag : "+inParameterVal.v_staff_rebate_flag);
                        // alert("Supp_cd : "+inParameterVal.v_supp_cd);
                        // alert("extra_cd : "+inParameterVal.v_extra_cd );
                        // alert("extra_inst :"+inParameterVal.v_extra_inst);

                        //inParameterVal.optCd = $('#optCd').val();
                        //var applicationDate = $('#applicationDate').val();
                        // if (applicationDate) {
                        //     applicationDate = applicationDate.split("/").reverse().join("/");
                        //     applicationDate = getDate(applicationDate);
                        // }
                        // var lastpaidDate = $('#lastpaidDate').val();
                        // if (lastpaidDate) {
                        //     lastpaidDate = lastpaidDate.split("/").reverse().join("/");
                        //     lastpaidDate = getDate(lastpaidDate);
                        // }
                        //var paymodeCode = $('#paymodeCode').val();
                        //var optionCode = $('#optionCode').val();
                        //var policyStatusCode = $('#policyStatusCode').val();
                        // var nextDueDate = $('#nextDueDate').val();
                        // if (nextDueDate) {
                        //     nextDueDate = nextDueDate.split("/").reverse().join("/");
                        //     nextDueDate = getDate(nextDueDate);
                        // }
                        //var askingType = $('#askingType').val();
                        //var askingRate = $('#askingRate').val();
                        // var commDate = $('#commDate').val();
                        // if (commDate) {
                        //     commDate = commDate.split("/").reverse().join("/");
                        //     commDate = getDate(commDate);
                        // }
                        //var lastPaidInstNo = $('#lastPaidInstNo').val();
                        //var surrenderPer = $('#surrenderPer').val();
                        //var interestRate = $('#interestRate').val();
                        //var totalYearlyPremium = $('#totalYearlyPremium').val();

                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/new-business/addCalculateAccptance",
                            data: JSON.stringify(inParameterVal),
                            dataType: 'json',
                            success: function (data) {
                                // console.log(data);
                                // if(data.p_process_st == null){
                                //     $.alert({
                                //         title: 'Message',
                                //         content: data,
                                //         buttons: {
                                //             ok: function () {

                                //alert(data.p_life_premium_yrly);

                                $("#p_life_premium_yrly").val(data.p_life_premium_yrly);
                                $("#p_life_premium_inst").val(data.p_life_premium_inst);

                                $("#p_net_life_premium_inst").val(data.p_net_life_premium_inst);
                                $("#p_net_life_premium_yrly").val(data.p_net_life_premium_yrly);

                                $("#p_total_rebate").val(data.p_pay_mode_rebate_inst);

                                $("#p_pay_mode_rebate_inst").val(data.p_staff_rebate_inst);
                                $("#p_staff_rebate_inst").val(data.p_sum_as_rebate_inst);
                                $("#p_sum_as_rebate_inst").val(data.p_supp_total_extra );

                                $("#p_supp_total_extra").val(data.p_total_rebate);


                                //     }
                                //   }
                                //  });

                                // }else {
                                //     showAlert(data.p_process_st)
                                // }
                            },
                            error: function (e) {
                                showAlert("Something Wrong!!!");
                                //console.log(e);
                            }
                        });



                    }

                },
                cancel: function () {

                }
            }
        });
    };


    function validationCalInfo() {
        var status = true;

        if ($("#v_extra_cd").val() == "-1") {
            status = false;
            $("#err_v_extra_cd").text("Empty field found!!");
            $("#v_extra_cd").focus();
        } else $("#err_v_extra_cd").text("");

        if ($("#v_extra_inst").val() == "") {
            status = false;
            $("#err_v_extra_inst").text("Empty field found!!");
            $("#v_extra_inst").focus();
        } else $("#err_v_extra_inst").text("");

        // if ($("#assLifeClass").val() == "-1") {
        //     status = false;
        //     $("#err_assLifeClass").text("Empty field found!!");
        //     $("#assLifeClass").focus();
        // } else $("#err_assLifeClass").text("");

        // if ($("#accLifeClass").val() == "-1") {
        //     status = false;
        //     $("#err_accLifeClass").text("Empty field found!!");
        //     $("#accLifeClass").focus();
        // } else $("#err_accLifeClass").text("");

        return status;

    }


});

//
//
// $(document).ready(function () {
//
//
//     function populateQuestions(strSet) {
//         var htmlButton = "<button type='button' class='btn btn-success' id='agentInsss-ans'>Save</button>";
//         var masterId = $('#proposalMasterNo').val();
//         $.ajax({
//             contentType: 'application/json',
//             url:  "/generic/question/all/"+ "agentInsss?proposalMasterId=" + masterId,
//             type: 'GET',
//             async: false,
//             dataType: 'json',
//             success: function(res) {
//
//                 $.each(res, function( key, value ) {
//                     // console.log('caste: ' + value.id + ' | id: ' + value.questionText);
//                     // var textVal = "";
//                     // textVal = value.answerText;
//                     // var html = "<p>" + value.questionText + "</p>" +
//                     //     "<input class='form-control col-md-7 col-xs-12 ans-input-agentInsss' questionId =" + value.id + " questionSetId =" + value.questionSetId +" " +
//                     //     " answerId="+ value.answerId +
//                     //     " value="+ textVal +
//                     //     " type='text'/>";
//
//                     var textVal = (value.answerText == null) ? '' : "'" +value.answerText + "'";
//                     var html = '<tr>';
//                     html += '<td>';
//                     html += key + 1;
//                     html += '</td><td>';
//                     html += value.questionText;
//                     html += '</td><td>';
//                     html += "<input class='form-control col-md-7 col-xs-12 ans-input-agentInsss' questionId =" + value.id + " questionSetId =" + value.questionSetId +" " +
//                             " answerId="+ value.answerId +
//                             " type='text' " +
//                             " value="+ textVal + "></input>";
//                     html += '</td>';
//                     html += '</tr>';
//
//
//                     $("#questionAgentDiv").append(html);
//                 });
//             },
//             error: function(xhr, status, error) {
//                 alert("Something went wrong!");
//             }
//         });
//
//         $("#agent_question_save_btn").append(htmlButton);
//     }
//
//
//     populateQuestions();
//
//     var confirmAgentAnsDialog = function (text, answerMegaDtos) {
//         $.confirm({
//             title: 'Confirm!',
//             content: text,
//             buttons: {
//                 confirm: {
//                     btnClass: 'btn-info',
//                     keys: ['enter'],
//                     action: function () {
//                         $.ajax({
//                             contentType: 'application/json',
//                             url:  "/generic/answer/create",
//                             type: 'POST',
//                             async: false,
//                             data : JSON.stringify(answerMegaDtos),
//                             success: function(res) {
//                                 /* for (var i=0; i<res.length; i++) {
//                                  console.log(res[i].answerId);
//                                  }*/
//                                 var i=0;
//                                 $('input[type="text"].ans-input-agentInsss').each(function () {
//                                     $(this).attr("answerId", res[i++].answerId);
//
//                                 });
//                                 showAlert("Successfully Saved");
//                             },
//                             error: function(xhr, status, error) {
//                                 alert("Something went wrong!");
//                             }
//                         });
//                     }
//
//                 },
//                 cancel: function () {
//
//                 }
//             }
//         });
//     };
//
//
//     $(document).on("click", "#agentInsss-ans", function (e) {
//         var answerMegaDtos = [];
//         var masterId = $('#proposalMasterNo').val();
//
//         $('input[type="text"].ans-input-agentInsss').each(function () {
//
//             var answerMiniDto = {};
//             answerMiniDto.answerText = $(this).val();
//             answerMiniDto.questionId = $(this).attr("questionId");
//             answerMiniDto.questionSetId = $(this).attr("questionSetId");
//             answerMiniDto.answerId = $(this).attr("answerId");
//             answerMiniDto.proposalMasterId = masterId;
//             answerMegaDtos.push(answerMiniDto);
//             // console.log($(this).val());
//         });
//
//         confirmAgentAnsDialog("Are you sure to save this questionnaire?", answerMegaDtos);
//
//
//
//     });
//
//
//
// });