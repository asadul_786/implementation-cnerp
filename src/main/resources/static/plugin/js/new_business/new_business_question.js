
    $(function (){
        $(document).on("click", "#profile-tab8, #profile-tab9", function (event) {
            console.log('hi');
            $.ajax({
                contentType: 'application/json',
                url:  "/generic/questionnaire/answer",
                type: 'GET',
                async: false,
               // data: JSON.stringify(questionDtos),
                dataType: 'json',
                success: function(response) {

                    console.log(response);
                    response.forEach(function(item) {
                        // do something with `item`
                        console.log(item);
                        var questionnaireId = item.questionnaireId;

                        if(item.questionnaireDto.questionType === 0) {
                            $("input[name='"+ questionnaireId +"']").val(item.answer);
                        }


                        if(item.questionnaireDto.questionType === 1){
                            $("input[name='" + questionnaireId +"']").each(function () {
                                console.log($(this).val());
                                if($(this).val() === item.answer){
                                    $(this).prop('checked',true);
                                }
                            });
                        }

                        if(item.questionnaireDto.questionType === 2){
                            $("input[name='" + questionnaireId +"']").each(function () {
                                console.log($(this).val());
                                var resultList = item.answer.split(";");
                                var res = $(this).val();
                                var checkbox = $(this);
                                resultList.forEach(function (ans) {
                                    //alert(res + " " + ans);
                                    if(res == ans){
                                        //$(this).prop('checked',true);
                                        checkbox.prop('checked',true);
                                        //alert("Y");
                                    }
                                });
                            });
                        }

                        if(item.questionnaireDto.questionType === 3){
                            $("input[name='" + questionnaireId +"']").each(function () {
                                console.log($(this).val());
                                if($(this).val() === item.answer){
                                    $(this).prop('checked',true);
                                }
                            });

                            $("input[name='" + questionnaireId + "-optional-" + item.questionnaireDto.optionalMandatory  +"'").val(item.optionalAnswer);
                        }
                    });


                },
                error: function(xhr, status, error) {
                }
            });

        });

        $(document).on("click", "#btnQuestionniareSubmit", function (e) {
            e.preventDefault();
            console.log('log');

            var values = $(this).closest("form").serializeArray();
            console.log(values);

            var submittedForm = true;

            var inputElements = [];

            $(this).closest("form").find('input').each(function() {
               // console.log($(this));// do your staff with each checkbox

                var inputType = $(this).attr('type');
                var name = $(this).attr('name');

                console.log(inputType);
                console.log(name);

                if(inputType == 'text') {
                    if($(this).val() == ''){
                        showAlert('Select all the field of text box');
                        submittedForm = false;
                        return false;
                    }
                }


                if(inputType == 'radio' || inputType == 'checkbox') {

                    var exists = inputElements.find(x=> x == name);
                    if(exists === undefined)
                    {
                        inputElements.push(name);
                    }

                }
            });

            for(var i = 0 ; i < inputElements.length ;i++){
                var q4_a = $('input[name="'+ inputElements[i]+'"]:checked').val();
                if(q4_a == undefined){
                    showAlert('Select all the field ');
                    submittedForm = false;
                    break;
                }
            }

            var questionDtos = [];

            values.forEach(function(item,index) {
                // do something with `item`
                console.log(item);
                console.log(index);
                var optionalMandatory = '';
                var questionDto = {};



                if (item.name !==  null && item.name.indexOf('optional') > -1)
                {
                    var questionId =  item.name.split("-")[0];
                    questionDto.questionnaireId = questionId;
                    questionDto.optionalAnswer = item.value;

                    var optionalMandatory = item.name.split("-")[2];
                    console.log(optionalMandatory);


                }else {
                    questionDto.questionnaireId = item.name;
                    questionDto.answer = item.value;
                }

                if(item.value == "" && optionalMandatory == 'true') {
                    submittedForm = false;
                    $("span[id="+ item.name +"]").html('No Ans Given')
                }
                questionDtos.push(questionDto);
            });

            if(submittedForm) {
                $.ajax({
                    contentType: 'application/json',
                    url:  "/generic/questionnaire/create",
                    type: 'POST',
                    async: false,
                    data: JSON.stringify(questionDtos),
                    dataType: 'json',
                    success: function(response) {

                        console.log(response);
                        if(response === true){
                            // resetAssuredForm();
                            showAlert("Successfully added");
                            //$('#frmQuestion').trigger("reset");
                        }
                        else {
                            showAlert("Cannot add.");
                        }

                    },
                    error: function(xhr, status, error) {
                    }
                });
            }


        });



    });