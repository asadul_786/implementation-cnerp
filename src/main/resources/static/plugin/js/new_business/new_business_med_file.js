/**
 * Created by SIL_PC_73 on 4/9/2019.
 */
$(document).ready(function () {


    var tbodyId ;
    var fileName;
    var fileUploadForm;
    var clickToFilePath;
    var selectedDD = [];

    function validateMedPage(){
        if($("#report_type_id option:selected").val()=== '-1' || $("#report_id option:selected").val() === '-1')
         return false;

        return true;
    }

    function generateTableTile(reportTypeName, reportName) {
        var tableTitle = "<div class='master_med_add_div'><div class='x_title'> " +
                        "<br/> " +
                        "<h2>" +  reportTypeName+ " - " + reportName +
                        "</h2> " +
                        "<div class='clearfix'>" +
                        "</div> " +
                        "</div>";

        return tableTitle;
    }

    function generateTableMeta(fileUploadForm, tbodyId, fileName, _tbodyId, _fileUploadForm) {
        var tableMeta = "<form id=" + "'" + fileUploadForm + "'" +  ">" +
            "<table class='table table-striped table-bordered bulk_action'>" +
            " <thead> " +
            "<tr> " +
            "<th width='30%'>Title</th> " +
            "<th width='35%'>Attach Files</th>" +
            " <th width='25%'>Action</th> " +
            "</tr> " +
            "</thead>" +
            " <tbody id=" + "'" + tbodyId + "'" +  ">" +
            "<tr>" +
            "<td>" +
            "<input class='form-control file-title' name='titles' type='text'/>"+
            "<input type='hidden'  name='fileName' value=" + "'" + fileName + "'" +  ">" +
            "</td>"+
            "<td>" +
            "<input class='form-control med-files' name='files'  type='file' accept='image/*'/>" +
            "</td>"+
            "<td>" +
            " <button type='button' class='btn btn-info master_med_add' " + "addTBodyId=" + "'" + _tbodyId + "'" +
            "addFormId=" + "'" + _fileUploadForm + "'" +  ">" +
            " <i class='fa fa-plus'></i>" +
            "</button>" +
            "</td>"+
            "</tr>" +
            " </tbody> " +
            "</table> </form> </div>";

        return tableMeta;
    }

    function generateTableBody(resId, resTitle, fileName, clickToFilePath) {
        var tableBody = "<tr id=" + "'" + resId + "'" +  ">" +
            "<td>" +
            "<h4>" + "  " +resTitle + "</h4>"+
            "<input type='hidden'  name='fileName' value=" + "'" + fileName + "'" +  ">" +
            "</td>"+
            "<td>" +
            "<h4><a href='" + clickToFilePath + "' target='_blank'>View</a></h4>" +
            "</td>"+
            "<td>" +
            " <button  class='btn btn-danger file-delete' type='button' " + " id=" + "'delete-" + resId + "'" +  ">" +
            "<i class='fa fa-trash'></i>" + "</button>" +
            "</td>"
        "</tr>";

        return tableBody;
    }

    if($("#proposalMasterNo").val()!='0'){
        $.ajax({
            contentType: 'application/json',
            url: "get-proposal-med-attach",
            type: 'GET',
            async: false,
            data: {
                id: $("#proposalMasterNo").val()
            },
            dataType: 'json',
            success: function (response) {
                var i;
                var j;
                for (i = 0; i < response.length; ++i) {

                    tbodyId = response[i].reportTypeId + "-" + response[i].reportId ;
                    fileName = tbodyId + "-" + $("#proposalMasterNo").val();
                    fileUploadForm = "form-" + tbodyId;

                    selectedDD.push(tbodyId); //new change

                    var titleMeta = generateTableTile(response[i].reportTypeName, response[i].reportName);
                    $("#upload_space").append(titleMeta);

                    var tableMeta = generateTableMeta(fileUploadForm, tbodyId, fileName, tbodyId, fileUploadForm);
                    $("#upload_space").append(tableMeta);

                    for(j=0; j<response[i].attachments.length; ++j){

                        var _clickToFilePath = "/new-business/med-file-view?fileName=" + response[i].attachments[j].fileName
                            +"&fileTitle=" + response[i].attachments[j].fileTitle;

                        var tableBody = generateTableBody(response[i].attachments[j].attachmentId,
                            response[i].attachments[j].fileTitle, fileName, _clickToFilePath);

                        var str = '#' + tbodyId;
                        $(str).append(tableBody);

                    }
                }

            },

            error: function (xhr, status, error) {
                showAlert("Something went wrong!");
            }
        });
    }

    $(document).on("change", "#report_type_id", function (e) {

        var id = $("#report_type_id").val();
        $.get("/new-business/get-report-by-id?id=" + id,
            function (data, status) {
                var reports = $('#report_id');
                reports.empty();
                reports.append($('<option/>', {
                    value: "-1",
                    text: "--Select Report--"
                }));
                $.each(data, function (index, dis) {
                    reports.append($('<option/>', {
                        value: dis.id,
                        text: dis.name
                    }));
                });

            });


    });

    $(document).on("click", "#med_upload_button", function (e) {


        if(validateMedPage()){
            tbodyId = $("#report_type_id option:selected").val() + "-" + $("#report_id option:selected").val() ;
            fileName = tbodyId + "-" + $("#proposalMasterNo").val();
            fileUploadForm = "form-" + tbodyId;
            if(!selectedDD.includes(tbodyId)){

                var titleMeta = generateTableTile($("#report_type_id option:selected").text(),
                    $("#report_id option:selected").text());
                var tableMeta = generateTableMeta(fileUploadForm, tbodyId, fileName, tbodyId, fileUploadForm);

                $("#upload_space").append(titleMeta);
                $("#upload_space").append(tableMeta);
                selectedDD.push(tbodyId);
                window.location.hash = '#' + fileUploadForm;
            }
            else {
                showAlert("Report type and Report already selected!");
            }
        }
        else {
            showAlert("Please select Report type and Report!");
        }

    });

    $(document).on("click", '.master_med_add', function (e) {
        var addFormId = $(this).attr("addFormId");
        var addTBodyId = $(this).attr("addTBodyId");
        var form = $('#' + addFormId)[0];

        $.confirm({
            title: 'Confirm!',
            content: "Are you sure to save the data",
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {



                        formData = new FormData(form);
                        $.ajax({
                            url: "uploadMedFile",
                            type: "POST",
                            data: formData,
                            enctype: 'multipart/form-data',
                            async: false,
                            processData: false,
                            contentType: false,
                            dataType: 'json',
                            cache: false,
                            success: function (res) {
                                if(res.id===''){
                                    showAlert("File upload failed!");
                                }
                                else {
                                    showAlert("Successfully uploaded");
                                    clickToFilePath = "/new-business/med-file-view?fileName=" + res.fileName + "&fileTitle=" + res.title;

                                    var tableBody = generateTableBody(res.id, res.title, fileName, clickToFilePath);

                                    var str = '#' + addTBodyId;
                                    $(str).append(tableBody);
                                    $(".file-title").val('');
                                    $(".med-files").val('');
                                }

                            },
                            error: function () {
                                showAlert("File upload failed");
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });



    });

    $(document).on("click", ".file-delete", function (e) {
        var id = $(this).attr("id");
        var deleteId = id.substring(7, id.length);
        var str = '#' + deleteId;
        //$(str).remove();

        $.confirm({
            title: 'Confirm!',
            content: "Are you sure to delete the data",
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {


                        $.ajax({
                            contentType: 'application/json',
                            url: "del-med-file",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(deleteId),
                            dataType: 'json',
                            success: function (response) {
                                showAlert("Successfully Deleted");
                                $(str).remove();
                            },
                            error: function (xhr, status, error) {
                                showAlert("Something went wrong!");
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });



    });


});