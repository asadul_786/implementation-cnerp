
$(document).ready(function () {

    $("#profile-tab1").trigger("click");

    $("#proposerProposalAdd").click(function () {

        var flag=ValidatingAddressData();

        if(flag== true){

            var proposalGeneral = {};

            //proposalGeneral.genpgid=$("#genpgid").val();
            proposalGeneral.genPName =$("#proposerName").val();
            proposalGeneral.genPFather = $("#proposerFather").val();
            proposalGeneral.genPMother = $("#proposerMother").val();
            proposalGeneral.genProposerDOB=$("#proposerDOB").val();
            proposalGeneral.genplaceOfBirthId = $("#placeOfBirthId").val();
            proposalGeneral.genOccupation = $("#occupation").val();
            proposalGeneral.genOccupArea = $("#occupArea").val();
            proposalGeneral.genResponsibiltyId = $("#genResponsibiltyId").val();
            proposalGeneral.genAnnualIncome = $("#genAnnualIncome").val();
            proposalGeneral.incomeSource = $("#incomeSource").val();
            proposalGeneral.genMobileNo =  $("#mobileNo").val();
            proposalGeneral.genEmail =  $("#email").val();
            proposalGeneral.ageProofCd =  $("#ageProofCd").val();


            proposalGeneral.genTin = $("#tin").val();
            proposalGeneral.genGender = $("#gender").val();
            proposalGeneral.genMaritalStatus =$("#maritalStatus").val();
            proposalGeneral.eenEducation =$("#education").val();
            proposalGeneral.genNationality =$("#nationality").val();
            proposalGeneral.genIdentifiMark =  $("#identifiMark").val();
            proposalGeneral.genEmployerName = $("#employerName").val();
            proposalGeneral.genDesignation = $("#designation").val();
            proposalGeneral.remarks= $("#remarks").val();

              //alert("General PGID : "+proposalGeneral.genpgid);
             // alert("Proposal Name : "+proposalGeneral.genPName );
             // alert("Father Name : "+proposalGeneral.genPFather);
             // alert("Mother Name : "+proposalGeneral.genPMother);
             // alert("Date of Birth : "+proposalGeneral.genProposerDOB);
             // alert("Birth Place : "+proposalGeneral.genplaceOfBirthId);
             // alert("Occupation : "+proposalGeneral.genOccupation);
             // alert("Occupation Area : "+proposalGeneral.genOccupArea);
             // alert("Responsibility : "+proposalGeneral.genResponsibiltyId);
             // alert("Annual Income : "+proposalGeneral.genAnnualIncome);
             // alert("Income Source : "+proposalGeneral.incomeSource);
             // alert("Mobile No : "+proposalGeneral.genMobileNo);
             // alert("Email : "+proposalGeneral.genEmail);
             // alert("Tin : "+proposalGeneral.genTin);
             // alert("Gender : "+proposalGeneral.genGender);
             // alert("Marital Status : "+proposalGeneral.genMaritalStatus);
             // alert("Education : "+proposalGeneral.eenEducation);
             // alert("Nationality : "+proposalGeneral.genNationality);
             // alert("Identification : "+proposalGeneral.genIdentifiMark);
             // alert("Employee Name : "+proposalGeneral.genEmployerName);
            // alert("age doc : "+proposalGeneral.ageProofCd);


            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/new-business/add-general",
                data: JSON.stringify(proposalGeneral),
                dataType: 'json',
                success: function (data) {
                    // alert(data);
                    showAlert("Successfully Saved!");

                },
                error: function (e) {
                    showAlert("Something Wrong!!!");
                }
            });
        }
    });

       // var cal_join = $('#joinDate').val();

        // var parts = join_date.split('/');
        // var mydate = new Date(parts[2], parts[1], parts[0]); //year month day3
        // var year = mydate.getFullYear();
        // var month = mydate.getMonth();
        // var day = mydate.getDate();
        //
        // //===========
        // var cal_join_parts = cal_join.split('/');
        // var cal_join_mydate = new Date(cal_join_parts[2], cal_join_parts[1], cal_join_parts[0]); //year month day3
        // var cal_join_year = cal_join_mydate.getFullYear();
        // var cal_join_month = cal_join_mydate.getMonth();
        // var cal_join_day = cal_join_mydate.getDate();
        // //===========
        //
        // var lpr = new Date(year + 59, month, day);
        // var lpr_year = lpr.getFullYear();
        // var lpr_month = lpr.getMonth();
        // var lpr_day = lpr.getDate();
        //
        // var retire = new Date(year + 60, month, day);
        // var retire_year = retire.getFullYear();
        // var retire_month = retire.getMonth();
        // var retire_day = retire.getDate();
        //
        // var confirm_date = new Date(cal_join_year, cal_join_month + 6, cal_join_day);
        // var conf_year = confirm_date.getFullYear();
        // var conf_month = confirm_date.getMonth();
        // var conf_day = confirm_date.getDate();
        //
        // $('#lprDate').val(lpr_day + "/" + lpr_month + "/" + lpr_year);
        // $('#retireDt').val(retire_day + "/" + retire_month + "/" + retire_year);
        // $('#confirmDate').val(conf_day + "/" + conf_month + "/" + conf_year);
    //});



    $('#occupArea').on('change', function() {
        getResponsibityName( $(this).children("option:selected").val());
    });

    function getResponsibityName(occuArea_cd){
        //console.log(office_code);
        //alert(occuArea_cd)
        var json = {
            "occuArea_cd": occuArea_cd
        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/new-business/getResponbty/"+ occuArea_cd,
            data: JSON.stringify(json),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
//                        console.log(data);
                //alert(JSON.stringify(json));
                $('#genResponsibiltyId').empty();
                var html = '';
                html += '<option value="-1">Select Responsibility--</option>';
                $.each(data, function (i, l) {

                    html += '<option value="' + l[0] + '">' + l[1] + '</option>';
                });

                $('#genResponsibiltyId').append(html)
                $("#genResponsibiltyId").selectpicker("refresh");
//                    console.log(html);
            },
            error: function (e) {
                //console.log('rrr');
            }
        });
    }

    //
    // function getLastPGIDINSPPRoposal() {
    //     var json = {
    //         "pgid": ""
    //     };
    //
    //     $.ajax({
    //         type: "POST",
    //         contentType: "application/json",
    //         url: "/new-business/getLastPGID",
    //         data: JSON
    //             .stringify(json),
    //         dataType: 'json',
    //         cache: false,
    //         timeout: 600000,
    //         success: function (data) {
    //
    //             $("#pgidss").val(data);
    //
    //         },
    //         error: function (e) {
    //
    //         }
    //     });
    // }

    // $('#proposerDOB').on('change', function() {
    //
    //     $('#proposerDOB').datepicker({changeMonth: true, maxDate: '0',yearRange: '1970:2010',changeYear: true, dateFormat: 'dd/mm/yy',
    //         onClose: function(){
    //             var today = new Date(),
    //                 birthday = $('#proposerDOB').datepicker("getDate"),
    //                 age = (
    //                     (today.getMonth() > birthday.getMonth())
    //                     ||
    //                     (today.getMonth() == birthday.getMonth() && today.getDate() >= birthday.getDate())
    //                 ) ? today.getFullYear() - birthday.getFullYear() : today.getFullYear() - birthday.getFullYear()-1;
    //
    //             alert("Age: " + age);}
    //     });
    // });
    //$(document).ready(function() {


   // });


    $('#proposerDOB').datepicker({changeMonth: true,changeYear: true, yearRange: '1970:2010', dateFormat: 'dd/mm/yy',
        onClose: function(){
            var today = new Date(),
                birthday = $('#proposerDOB').datepicker("getDate"),
                age = (
                    (today.getMonth() > birthday.getMonth())
                    ||
                    (today.getMonth() == birthday.getMonth() && today.getDate() >= birthday.getDate())
                ) ? today.getFullYear() - birthday.getFullYear() : today.getFullYear() - birthday.getFullYear()-1;

            //alert("Age: " + age);
           $('#productage').val(age);}
    });

    $( "#proposerDOB" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1970:2010'
    });

    function ValidatingAddressData() {

        var status = true;
       // var cString=/^[a-z]+$/;
        var cDigit = /^[0-9]+$/;

       // var mobileRegex = /^(?:\+88|01)?(?:\d{11}|\d{13})$/;
        //var mobileRegex =/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        // var mobileRegex =/[^0-9]/;

        var cDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        //var cMb = /^((?=(01))[0-9]{11}|)$/;
        var emailRegex=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        // if ($("#proposerName").val() == "") {
        //     status = false;
        //     $("#err_proposerName").text("Empty field found!!");
        //     $("#proposerName").focus();
        // } else $("#err_proposerName").text("");

        if ($("#proposerFather").val() == "") {
            status = false;
            $("#err_proposerFather").text("Empty field found!!");
            $("#proposerFather").focus();
        } else $("#err_proposerFather").text("");
        // }else if (!cString.test($("#proposerFather").val())){
        //     status=false;
        //      $("#err_proposerFather").text("Invalid field found!!");
        //      $("#proposerFather").focus();
        //

        if ($("#proposerMother").val() == "") {
            status = false;
            $("#err_proposerMother").text("Empty field found!!");
            $("#proposerMother").focus();
        } else $("#err_proposerMother").text("");

        // }else if (!cString.test($("#proposerMother").val())){
        //     status=false;
        //     $("#err_proposerMother").text("Invalid field found!!");
        //     $("#proposerMother").focus();
        //

        if ($("#proposerDOB").val()=="") {
            status = false;
            $("#err_proposerDOB").text("Empty field found!!");
            $("#proposerDOB").focus();
        }else if (!cDate.test($("#proposerDOB").val())) {
            status = false;
            $("#err_proposerDOB").text("Invalid Date format found!!");
            $("#proposerDOB").focus();
        } else $("#err_proposerDOB").text("");

        if ($("#placeOfBirthId").val() == "-1") {
            status = false;
            $("#err_placeOfBirth").text("Empty field found!!");
            $("#placeOfBirthId").focus();
        } else $("#err_placeOfBirth").text("");

        if ($("#occupation").val() == "-1") {
            status = false;
            $("#err_occupation").text("Empty field found!!");
            $("#occupation").focus();
        } else $("#err_occupation").text("");

        if ($("#occupArea").val() == "-1") {
            status = false;
            $("#err_occupArea_bn_name_id").text("Empty  field found!!");
            $("#occupArea").focus();
        } else $("#err_occupArea_bn_name_id").text("");

        if ($("#responsibiltyId").val() == "-1") {
            status = false;
            $("#err_respons_bn_name_id").text("Empty  field found!!");
            $("#responsibiltyId").focus();
        } else $("#err_respons_bn_name_id").text("");

        if ($("#genAnnualIncome").val() == "") {
            status = false;
            $("#err_incomeS_bn_name_id").text("Empty field found!!");
            $("#genAnnualIncome").focus();
        } else $("#err_incomeS_bn_name_id").text("");

        if ($("#incomeSource").val() == "-1") {
            status = false;
            $("#err_incomeSource_bn_name_id").text("Empty field found!!");
            $("#incomeSource").focus();
        } else $("#err_incomeSource_bn_name_id").text("");

        if (!cDigit.test($("#genAnnualIncome").val())){
            status = false;
            $('#err_incomeS_bn_name_id').text("Only Digit allow for Income !!");
            $('#genAnnualIncome').focus();
        } else $('#err_incomeS_bn_name_id').text("");

        // if ($("#other_income_id").val() == "") {
        //     status = false;
        //     $("#err_other_income_bn_name_id").text("Empty field found!!");
        //     $("#other_income_id").focus();
        // } else $("#err_other_income_bn_name_id").text("");

        // if ($('#mobileNo').val() == "") {
        //     status = false;
        //     $('#err_mobileNo').text("Empty field found!!");
        //     $('#mobileNo').focus();
        // } else if (!cMb.test($('#mobileNo').val())) {
        //     status = false;
        //     $('#err_mobileNo').text("Invalid contact no!!");
        //     $('#mobileNo').focus();
        // } else $('#err_mobileNo').text("");

        //
        // if ($("#mobileNo").val() == "") {
        //     status = false;
        //     $("#err_mobileNo").text("Empty field found!!");
        //     $("#mobileNo").focus();
        // } else $("#err_mobileNo").text("");
        //
        // if (!cMb.test($("#mobileNo").val())) {
        //     status = false;
        //     $("#err_mobileNo").text("Invalid contact No found!!");
        //     $("#mobileNo").focus();
        // } else $("#err_mobileNo").text("");

        // if ($("#email").val()=="") {
        //     status = false;
        //     $("#err_email_bn_name_id").text("Empty field found!!");
        //     $("#email").focus();
        // }else if (!emailRegex.test($("#email").val())) {
        //     status = false;
        //     $("#err_email_bn_name_id").text("Invalid email found!!");
        //     $("#email").focus();
        // } else $("#err_email_bn_name_id").text("");

        // if ($("#tin").val() == "") {
        //     status = false;
        //     $("#err_tin_bn_name_id").text("Empty  field found!!");
        //     $("#tin").focus();
        // } else $("#err_tin_bn_name_id").text("");

        if ($("#gender").val() == "-1") {
            status = false;
            $("#err_religion").text("Empty  field found!!");
            $("#gender").focus();
        } else $("#err_religion").text("");

        if ($("#maritalStatus").val() == "-1") {
            status = false;
            $("#err_maritalStatus").text("Empty field found!!");
            $("#maritalStatus").focus();
        } else $("#err_maritalStatus").text("");

        if ($("#education").val() == "-1") {
            status = false;
            $("#err_education").text("Empty field found!!");
            $("#education").focus();
        } else $("#err_education").text("");

        if ($("#nationality").val() == "-1") {
            status = false;
            $("#err_nationality").text("Empty field found!!");
            $("#nationality").focus();
        } else $("#err_nationality").text("");

        // if ($("#identifiMark").val() == "") {
        //     status = false;
        //     $("#err_identifiMark").text("Empty  field found!!");
        //     $("#identifiMark").focus();
        // } else $("#err_identifiMark").text("");

        // if ($("#employerName").val() == "") {
        //     status = false;
        //     $("#err_employerName").text("Empty field found!!");
        //     $("#employerName").focus();
        // } else $("#err_employerName").text("");

        // if ($("#designation").val() == "") {
        //     status = false;
        //     $("#err_designation").text("Empty field found!!");
        //     $("#designation").focus();
        // } else $("#err_designation").text("");

        // if ($("#remarks").val() == "") {
        //     status = false;
        //     $("#err_remarks").text("Empty field found!!");
        //     $("#remarks").focus();
        // } else $("#err_remarks").text("");


        return status;
    }

});


// function getLastNextPGIDINSPPRoposal(pgid) {
//
//     var json = {
//         "officeId": ""
//     };
//
//     $.ajax({
//         type: "POST",
//         contentType: "application/json",
//         url: "/new-business/getLastNextPGID/" + pgid,
//         data: JSON
//             .stringify(json),
//         dataType: 'json',
//         cache: false,
//         timeout: 600000,
//         success: function (data) {
//             //$('#pgidss').val(data);
//         },
//         error: function (e) {
//         }
//     });
// }





// $("img").hide();
    //
    // $('#uploadfile').on('change', function() {
    //     $("img").show();
    // });
    //
    //
    // if($("#proposalMasterNo").val()!="0"){
    //     $("img").show();
    //     $("#propMasterIdGen").val($("#proposalMasterNo").val());
    //
    //     $.ajax({
    //         contentType: 'application/json',
    //         url:  "load-files-general",
    //         type: 'GET',
    //         //async: false,
    //         //data: JSON.stringify(answerDto),
    //         dataType: 'json',
    //         success: function(response) {
    //
    //             if(response[0] != undefined){
    //                 $("#ageProofFileLink").attr("href", response[0]);
    //                 $("#ageProofFileLink").text("Download Link");
    //             }
    //
    //             if(response[1] != undefined){
    //                 $("#eduFileLink").attr("href", response[1]);
    //                 $("#eduFileLink").text("Download Link");
    //             }
    //
    //             $('#proposerPhoto').attr("src", response[2]);
    //             $('#sigFile').attr("src", response[3]);
    //
    //         },
    //         error: function(xhr, status, error) {
    //             //showAlert("Unknown error");
    //         }
    //     });
    // }
    //
    // var proposal_detail = {};
    // function readURLPhoto(input) {
    //
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //
    //         reader.onload = function(e) {
    //             $('#proposerPhoto').attr('src', e.target.result);
    //         }
    //
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // }
    //
    // function readURLSig(input) {
    //
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //
    //         reader.onload = function(e) {
    //             $('#sigFile').attr('src', e.target.result);
    //         }
    //
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // }
    //
    // $("#uploadfile").change(function() {
    //     if(this.files[0].size > 1048576){
    //         $('#uploadfile').val('');
    //         showAlert("Photo size can't be greater than 1MB!");
    //     }
    //     else readURLPhoto(this);
    // });
    //
    // $("#uploadSigfile").change(function() {
    //     if(this.files[0].size > 1048576){ //204800 307200
    //         $('#uploadSigfile').val('');
    //         showAlert("Signature size can't be greater than 1MB!");
    //     }
    //     else readURLSig(this);
    // });
    //
    // $("#annualIncome").change(function() {
    //     if (!$.isNumeric($("#annualIncome").val())){
    //         $("#err_" + "annualIncome").text("Please enter numeric value");
    //         $("#annualIncome").focus();
    //     }
    //     else{
    //         $("#err_annualIncome").text("");
    //     }
    // });
    //
    // $("#email").change(function() {
    //     var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //     if(!regexEmail.test($("#email").val().toLowerCase())) {
    //         $("#err_email").text("please provide valid email address");
    //         //alert("please provide valid email address");
    //         $("#email").focus();
    //     }
    //     else{
    //         $("#err_email").text("");
    //     }
    // });
    //
    // $("#mobileNo").change(function() {
    //     var regexMobile = /^(?:\+?88)?01[1,2,3,5,6,7,8,9]{1}[0-9]{8}$/;
    //     if(!regexMobile.test($("#mobileNo").val())) {
    //         $("#err_mobileNo").text("please provide valid mobile number");
    //       //  showAlert("please provide valid mobile number");
    //         $("#mobileNo").focus();
    //     }else{
    //         $("#err_mobileNo").text("");
    //     }
    // });
    //
    // if ($("#occupation option:selected").text() === "Other Occupations"){
    //     $("#other_occupation_div").show();
    // }
    //
    // $("#occupation").change(function() {
    //     if ($("#occupation option:selected").text() === "Other Occupations"){
    //         $("#other_occupation_div").show();
    //     }
    //     else $("#other_occupation_div").hide();
    // });
    //
    // function validateProposalDetail(proposal_detail){
    //     //nayeem code starts
    //
    //     var isValid = true;
    //     $(".proposer_info_form .err_msg").text("");
    //
    //     if($("#proposerName").val() == "" && isValid) {
    //         $("#err_" + "proposerName" ).text("Please enter proposer name");
    //         $("#proposerName").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#proposerFather").val() == "" && isValid) {
    //         $("#err_" + "proposerFather" ).text("Please enter proposer father name");
    //         $("#proposerFather").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#proposerMother").val() == "" && isValid) {
    //         $("#err_" + "proposerMother" ).text("Please enter proposer mother name");
    //         $("#proposerMother").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#proposerDOB").val() == "" && isValid) {
    //         $("#err_" + "proposerDOB" ).text("Please enter date of birth");
    //         $("#proposerDOB").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#placeOfBirth").val() == "" && isValid) {
    //         $("#err_" + "placeOfBirth" ).text("Please enter place of birth");
    //         $("#placeOfBirth").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#occupation").val() == "-1" && isValid) {
    //         $("#err_" + "occupation" ).text("Please enter occupation");
    //         $("#occupation").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#occupArea").val() == "-1" && isValid) {
    //         $("#err_" + "occupArea" ).text("Please select occupation area");
    //         $("#occupArea").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#annualIncome").val() == "" && isValid) {
    //         $("#err_" + "annualIncome" ).text("Please enter annual income");
    //         $("#annualIncome").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#incomeSource").val() == "-1" && isValid) {
    //         $("#err_" + "incomeSource" ).text("Please select income source");
    //         $("#incomeSource").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#mobileNo").val() == "" && isValid) {
    //         $("#err_" + "mobileNo" ).text("Please enter mobileNo");
    //         $("#mobileNo").focus();
    //         isValid = false;
    //     }
    //
    //
    //     if($("#gender").val() == "-1" && isValid) {
    //         $("#err_" + "gender" ).text("Please select gender");
    //         $("#gender").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#religion").val() == "-1" && isValid) {
    //         $("#err_" + "religion" ).text("Please select religion");
    //         $("#religion").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#maritalStatus").val() == "-1" && isValid) {
    //         $("#err_" + "maritalStatus" ).text("Please select marital status");
    //         $("#maritalStatus").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#education").val() == "-1" && isValid) {
    //         $("#err_" + "education" ).text("Please select education");
    //         $("#education").focus();
    //         isValid = false;
    //     }
    //
    //     if($("#nationality").val() == "-1" && isValid) {
    //         $("#err_" + "nationality" ).text("Please select nationality");
    //         $("#nationality").focus();
    //         isValid = false;
    //     }
    //
    //     if ($("#propDetailId").val() != "0"){
    //
    //         if($("#uploadfile").val() == "" && $("#proposerPhoto").attr("src") == "" && isValid) {
    //             $("#err_" + "uploadfile" ).text("Please select an image file");
    //             $("#uploadfile").focus();
    //             isValid = false;
    //         }
    //
    //         if($("#uploadSigfile").val() == "" && $("#sigFile").attr("src") == "" && isValid) {
    //             $("#err_" + "upload_sig_file" ).text("Please select an image file");
    //             $("#uploadSigfile").focus();
    //             isValid = false;
    //         }
    //     }
    //
    //
    //     return isValid;
    //
    //
    //     //nayeem code ends
    //
    // }
    //
    // var confirmDetailDialog = function(text){
    //
    //     $.confirm({
    //         title: 'Confirm!',
    //         content: text,
    //         buttons: {
    //             confirm: {
    //                 btnClass: 'btn-info',
    //                 keys: ['enter'],
    //                 action: function () {
    //                     $("#propMasterIdGen").val($("#proposalMasterNo").val());
    //
    //                     proposal_detail.proposerName = $("#proposerName").val();
    //                     proposal_detail.proposerFather = $("#proposerFather").val();
    //                     proposal_detail.proposerMother = $("#proposerMother").val();
    //                     proposal_detail.proposerDOB = $("#proposerDOB").val();
    //                     proposal_detail.placeOfBirth = $("#placeOfBirthId").val();
    //                     proposal_detail.ageProofDoc = $("#ageProofDoc").val();
    //                     proposal_detail.occupation = $("#occupation").val();
    //                     proposal_detail.occupArea = $("#occupArea").val();
    //                     proposal_detail.responsibility = $("#responsibiltyId").val();
    //                     proposal_detail.annualIncome = $("#annualIncome").val();
    //                     proposal_detail.incomeSource = $("#incomeSource").val();
    //                     proposal_detail.gender = $("#gender").val();
    //                     proposal_detail.maritalStatus = $("#maritalStatus").val();
    //                     proposal_detail.education = $("#education").val();
    //                     proposal_detail.educationDoc = $("#educationDoc").val();
    //                     proposal_detail.nationality = $("#nationality").val();
    //                     proposal_detail.employerName = $("#employerName").val();
    //                     proposal_detail.designation = $("#designation").val();
    //                     proposal_detail.remarks = $("#remarks").val();
    //                     proposal_detail.tin = $("#tin").val();
    //                     proposal_detail.email = $("#email").val();
    //                     proposal_detail.mobileNo = $("#mobileNo").val();
    //                     proposal_detail.propMasterId = $("#proposalMasterNo").val();
    //                     proposal_detail.identifiMark = $("#identifiMark").val();
    //                     proposal_detail.propDetailId = $("#propDetailId").val();
    //
    //                     proposal_detail.otherOccuName = $("#other_occupation").val();
    //                     proposal_detail.religion = $("#religion").val();
    //                     proposal_detail.otherIncomeSource = $("#other_income_id").val();
    //
    //                     var valid = validateProposalDetail(proposal_detail);
    //                     if(valid) {
    //
    //                             $.ajax({
    //                                 contentType: 'application/json',
    //                                 url: "add-proposal-detail",
    //                                 type: 'POST',
    //                                 async: false,
    //                                 data: JSON.stringify(proposal_detail),
    //                                 dataType: 'json',
    //                                 success: function (response) {
    //                                     $("#propDetailId").val(response.propDetailId);
    //                                 },
    //                                 error: function (xhr, status, error) {
    //                                     showAlert("Something went wrong!");
    //                                 }
    //                             });
    //
    //                             //var test = new FormData($("#general_form")[0]);
    //                             $.ajax({
    //                                 url: "uploadFile",
    //                                 type: "POST",
    //                                 data: new FormData($("#general_form")[0]),
    //                                 enctype: 'multipart/form-data',
    //                                 async: false,
    //                                 processData: false,
    //                                 contentType: false,
    //                                 dataType: 'json',
    //                                 cache: false,
    //                                 success: function (res) {
    //
    //                                     showAlert("Successfully saved!");
    //
    //                                     $('#ageProofFile').val('');
    //
    //                                     if(res[0] != undefined){
    //                                         $("#ageProofFileLink").attr("href", res[0]);
    //                                         $("#ageProofFileLink").text("Download Link");
    //                                     }
    //
    //                                     $('#uploadEduFile').val('');
    //
    //                                     if(res[1] != undefined){
    //                                         $("#eduFileLink").attr("href", res[1]);
    //                                         $("#eduFileLink").text("Download Link");
    //                                     }
    //
    //                                     $('#uploadfile').val('');
    //                                     $('#proposerPhoto').prop("src", res[2]);
    //
    //                                     $('#uploadSigfile').val('');
    //                                     $('#sigFile').prop("src", res[3]);
    //
    //
    //                                     console.log(res[0]);
    //                                 },
    //                                 error: function () {
    //                                     showAlert("File upload failed");
    //                                 }
    //                             });
    //
    //                     }
    //
    //                 }
    //             },
    //
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    //
    //
    // }
    //
    // $("#proposerProposalAdd").click(function (event) {
    //     event.preventDefault();
    //     var valid = validateProposalDetail(proposal_detail);
    //     if(valid) {
    //         confirmDetailDialog("Are you sure to save?");
    //     }
    // });


