/**
 * Created by asad on 27-jan-20.
 */
function parseNumber(str) {
    if(str==null|| str.trim()==""||str=="undefined"){
        return null;
    }
    else{
        var  intNum=parseInt(str);

        return intNum=="NaN"?null:intNum;
    }

}

$(document).ready(function () {


    $(document).on("click", "#profile-tab10", function () {



        function readURLPhoto(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                 reader.onload = function (e) {
                    $('#med_photo_view').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $("#err_uploadEduFile").text("");
            }
        }

        $(document).on("change", "#uploadEduFile", function (e) { //200KB
                if (this.files[0].size > 1048576) {
                    $("#err_uploadEduFile").text("Photo size can't be greater than 1MB!");
                } else
                    readURLPhoto(this);
        });

        // getDiagTestName(DIAG_REQ_CD);

        $("#medical_save_button").click(function () {

            var flag = ValidatingData();
            //alert("hi");
            if(flag== true){
                // var diagPGID =  $("#pgidss").val();
                 var nb_report_type_id = $("#nb_report_type_id").val();
                 var sb_report_type_id = $("#sb_report_type_id").val();
                 var uploadEduFile = $("#uploadEduFile").val();
                 // var id_comment_text = $("#id_comment_text").val();
                 // var party_by_id = $("#party_by_id").val();
                 // var id_comment_by = $("#id_comment_by").val();
                 var id_roky=parseNumber($("#rowky").val());
                 var uploadEduFile = $("#uploadEduFile").val();


                 // alert(pgidss);

                 var medicalReport = {};

               // medicalReport.diagPGID=diagPGID;
                medicalReport.diagDIAG_REQ_CD = nb_report_type_id;
                medicalReport.diagDIAG_TEST_CD = sb_report_type_id;
                // // medicalReport.diagCOMMENTS = id_comment_text;
                // medicalReport.diagCOMMENT_PARTY = party_by_id;
                // medicalReport.diagCOMMENTED_BY = id_comment_by;
                medicalReport.medROWKEY = id_roky;
                medicalReport.uploadEduFile = uploadEduFile;

               //  alert(medicalReport.diagPGID);
              //  alert(medicalReport.diagDIAG_REQ_CD);
               // alert(medicalReport.diagDIAG_TEST_CD);
               // alert(medicalReport.diagCOMMENTS);
               // alert(medicalReport.diagCOMMENT_PARTY);
                //alert(medicalReport.diagCOMMENTED_BY );

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: "/new-business/add-medical",
                    data: JSON.stringify(medicalReport),
                    dataType: 'json',
                    success: function (data) {
                        // alert(data);
                        updateTableList();
                        showAlert("Successfully Saved!");
                        refresh();
                    },
                    error: function (e) {
                        showAlert("Something Wrong!!!");
                    }
                });
            }
        });

        function ValidatingData() {

            var status = true;

           // var isValid = true;

            if ($("#nb_report_type_id").val() == "-1") {
                status = false;
                $("#err_nb_report_type_id").text("Empty field found!!");
                $("#nb_report_type_id").focus();
            } else $("#err_nb_report_type_id").text("");

            if ($("#sb_report_type_id").val() == "-1") {
                status = false;
                $("#err_sb_report_type_id").text("Empty field found!!");
                $("#sb_report_type_id").focus();
            } else $("#err_sb_report_type_id").text("");

            // if($("#uploadEduFile").val() == "" && $("#uploadEduFile").attr("src") == "" && status) {
            //     $("err_" + "uploadEduFile" ).text("Please select an image file");
            //     $("#uploadEduFile").focus();
            //     status = false;
            // }

            // if ($("#uploadEduFile").val() == "") {
            //     status = false;
            //     $("#err_uploadEduFile").text("Empty field found!!");
            //     $("#uploadEduFile").focus();
            // } else $("#err_uploadEduFile").text("");

            // if ($("#id_comment_text").val() == "") {
            //     status = false;
            //     $("#err_report_comment_id").text("Empty field found!!");
            //     $("#id_comment_text").focus();
            // } else $("#err_report_comment_id").text("");
            //
            // if ($("#party_by_id").val() == "-1") {
            //     status = false;
            //     $("#err_Issued_by_id").text("Empty field found!!");
            //     $("#party_by_id").focus();
            // } else $("#err_Issued_by_id").text("");
            // if ($("#id_comment_by").val() == "") {
            //     status = false;
            //     $("#err_id_comment_by").text("Empty field found!!");
            //     $("#id_comment_by").focus();
            // } else $("#err_id_comment_by").text("");

            return status;
        }

       });


    $(document).on("click", "#medReportTableId tbody tr", function() {


        // $('.assuredTableId tbody tr').on('click',  function () {
        //console.log($(this));
        var curRow = $(this).closest('tr');

        var pgid = curRow.find('td:eq(0)').text();
        var rowky = curRow.find('td:eq(1)').text();
        var nb_report_type_id = curRow.find('td:eq(2)').text();
        var nb_report_type_nm = curRow.find('td:eq(3)').text();
        var sb_report_type_id = curRow.find('td:eq(4)').text();
        var sb_report_type_nm = curRow.find('td:eq(5)').text();
        // var id_comment_text = curRow.find('td:eq(6)').text();
        // var  party= curRow.find('td:eq(7)').text();
        // var  comment_by= curRow.find('td:eq(8)').text();
        var  uploadEduFile= curRow.find('td:eq(6)').text();

        $('#pgid').val(pgid);
        $('#rowky').val(rowky);

        $('#nb_report_type_id').val( $(this).find(".nb-report-type").html());
        $('#sb_report_type_id').val( $(this).find(".sb-report-type").html());
        // $('#id_comment_text').val(id_comment_text);
        //  $('#party_by_id').val(party);
        //  $('#id_comment_by').val(comment_by);
         $('#uploadEduFile').val(uploadEduFile);

    });


    //$('#medReportTableId tbody').on('click', '#deletemedReport', function () {
        $(document).on("click", "#deletemedReport", function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Are you sure to Delete?',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/new-business/medical-report-info-delete/" + col1+"/"+col2,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            refresh();
                            showAlert("Successfully deleted");
                            $('#medReportTableId tbody tr').empty();
                            updateTableList();

                        },
                        error: function (xhr, status, error) {

                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

    function refresh(){

        $('#nb_report_type_id').val("-1");
        $('#sb_report_type_id').val("-1");
        // // $('#id_comment_text').val("");
        // $('#party_by_id').val("-1");
        // $('#id_comment_by').val("");
        $('#med_pgid').val("");
        $('#rowky').val("");
        $('#uploadEduFile').val("");

    }


    function updateTableList() {
        //alert("hi");
        var medReportTable = $('#medReportTableId');

        $.ajax({
            type: "GET",
            url: "/new-business/addMedReportList",
            success: function (data) {
                // alert(data);
                var no = 1;
                var tableBody = "";

                $('#medReportTableId tbody').empty();
                $.each(data, function (idx, elem) {
                    // alert (elem[idx]);
                    var edit = '<a class="btn btn-success medReportInformationEdit" value="Edit" id="editmedReport"> ' + '<i class="fa fa-pencil"></i></a> ';
                    var del = '<a class="btn btn-danger medReportInformationDelete" value="Delete" id="deletemedReport"> ' + '<i class="fa fa-trash"></i></a> ';
                    tableBody += "<tr'>";
                    tableBody += "<td hidden>" + elem[0] + "</td>";
                    tableBody += "<td hidden>" + elem[1] + "</td>";
                    tableBody += "<td hidden  class='nb-report-type'>" + elem[2] + "</td>";
                    tableBody += "<td>" + elem[3] + "</td>";
                    tableBody += "<td hidden  class='sb-report-type'>" + elem[4] + "</td>";
                    tableBody += "<td>" + elem[5] + "</td>";
                    // tableBody += "<td>" + elem[6] + "</td>";
                    // tableBody += "<td hidden>" + elem[7] + "</td>";
                    // tableBody += "<td hidden>" + elem[8] + "</td>";
                    tableBody += "<td>" + elem[6] + "</td>";
                    tableBody += "<td>" + edit + "</td>";
                    tableBody += "<td>" + del + "</td>"
                    tableBody += "<tr>";
                });
                medReportTable.append(tableBody);
            }
        });
    }


});



function getDiagTestName(DIAG_REQ_CD){

    var json = {
        "DIAG_REQ_CD": DIAG_REQ_CD

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/new-business/getDiagTest/"
            + DIAG_REQ_CD,
        data: JSON
            .stringify(json),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            //  alert(data);
            var html = '';
            html += '<option value="0"></option>';
            $.each(data, function (i, l) {

                html += '<option value="' + l[0] + '">'
                    + l[1]
                    + '</option>';
            });
            html += '</option>';

            $('#sb_report_type_id').html(html)

        },
        error: function (e) {

        }
    });
}


//
// $(document).ready(function () {
//
//
//     function populateQuestions(strSet) {
//         var htmlButton = "<button type='button' class='btn btn-success' id='agentInsss-ans'>Save</button>";
//         var masterId = $('#proposalMasterNo').val();
//         $.ajax({
//             contentType: 'application/json',
//             url:  "/generic/question/all/"+ "agentInsss?proposalMasterId=" + masterId,
//             type: 'GET',
//             async: false,
//             dataType: 'json',
//             success: function(res) {
//
//                 $.each(res, function( key, value ) {
//                     // console.log('caste: ' + value.id + ' | id: ' + value.questionText);
//                     // var textVal = "";
//                     // textVal = value.answerText;
//                     // var html = "<p>" + value.questionText + "</p>" +
//                     //     "<input class='form-control col-md-7 col-xs-12 ans-input-agentInsss' questionId =" + value.id + " questionSetId =" + value.questionSetId +" " +
//                     //     " answerId="+ value.answerId +
//                     //     " value="+ textVal +
//                     //     " type='text'/>";
//
//                     var textVal = (value.answerText == null) ? '' : "'" +value.answerText + "'";
//                     var html = '<tr>';
//                     html += '<td>';
//                     html += key + 1;
//                     html += '</td><td>';
//                     html += value.questionText;
//                     html += '</td><td>';
//                     html += "<input class='form-control col-md-7 col-xs-12 ans-input-agentInsss' questionId =" + value.id + " questionSetId =" + value.questionSetId +" " +
//                             " answerId="+ value.answerId +
//                             " type='text' " +
//                             " value="+ textVal + "></input>";
//                     html += '</td>';
//                     html += '</tr>';
//
//
//                     $("#questionAgentDiv").append(html);
//                 });
//             },
//             error: function(xhr, status, error) {
//                 alert("Something went wrong!");
//             }
//         });
//
//         $("#agent_question_save_btn").append(htmlButton);
//     }
//
//
//     populateQuestions();
//
//     var confirmAgentAnsDialog = function (text, answerMegaDtos) {
//         $.confirm({
//             title: 'Confirm!',
//             content: text,
//             buttons: {
//                 confirm: {
//                     btnClass: 'btn-info',
//                     keys: ['enter'],
//                     action: function () {
//                         $.ajax({
//                             contentType: 'application/json',
//                             url:  "/generic/answer/create",
//                             type: 'POST',
//                             async: false,
//                             data : JSON.stringify(answerMegaDtos),
//                             success: function(res) {
//                                 /* for (var i=0; i<res.length; i++) {
//                                  console.log(res[i].answerId);
//                                  }*/
//                                 var i=0;
//                                 $('input[type="text"].ans-input-agentInsss').each(function () {
//                                     $(this).attr("answerId", res[i++].answerId);
//
//                                 });
//                                 showAlert("Successfully Saved");
//                             },
//                             error: function(xhr, status, error) {
//                                 alert("Something went wrong!");
//                             }
//                         });
//                     }
//
//                 },
//                 cancel: function () {
//
//                 }
//             }
//         });
//     };
//
//
//     $(document).on("click", "#agentInsss-ans", function (e) {
//         var answerMegaDtos = [];
//         var masterId = $('#proposalMasterNo').val();
//
//         $('input[type="text"].ans-input-agentInsss').each(function () {
//
//             var answerMiniDto = {};
//             answerMiniDto.answerText = $(this).val();
//             answerMiniDto.questionId = $(this).attr("questionId");
//             answerMiniDto.questionSetId = $(this).attr("questionSetId");
//             answerMiniDto.answerId = $(this).attr("answerId");
//             answerMiniDto.proposalMasterId = masterId;
//             answerMegaDtos.push(answerMiniDto);
//             // console.log($(this).val());
//         });
//
//         confirmAgentAnsDialog("Are you sure to save this questionnaire?", answerMegaDtos);
//
//
//
//     });
//
//
//
// });