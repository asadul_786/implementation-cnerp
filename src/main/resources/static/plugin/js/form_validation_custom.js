
$(document).ready(function() {
    $('#departmentForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            department: {
                message: 'The department is not valid',
                validators: {
                    notEmpty: {
                        message: 'The department is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 16,
                        message: 'The department must be more than 2 and less than 16 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'The department can only consist of alphabetical, number and underscore'
                    }
                }
            },
            departmentBangla: {
                message: 'The department is not valid',
                validators: {
                    notEmpty: {
                        message: 'The department is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 16,
                        message: 'The department must be more than 2 and less than 16 characters long'
                    }
                }
            },
//                departmentBangla: {
//                    validators: {
//                        notEmpty: {
//                            message: 'The email is required and cannot be empty'
//                        },
//                        emailAddress: {
//                            message: 'The input is not a valid email address'
//                        }
//                    }
//                }
        }
    });
});

