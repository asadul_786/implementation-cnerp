$(document).ready(function () {

    $(document).on("click", ".forward-btn", function (event) {
        var proposal_id = $(this).closest("td").find('button').attr('data-id');
        $("#proposal_id").val(proposal_id);
        $.ajax({
            contentType: 'application/json',
            url: "get-forward-office-list",
            type: 'POST',
            async: false,
            // data: JSON.stringify(masterId),
            dataType: 'json',
            success: function (response) {
                var offices = $('#offices_id');
                offices.empty();
                offices.append($('<option/>', {
                    value: "-1",
                    text: "---Select Office---"
                }));
                $.each(response, function (index, o) {
                    offices.append($('<option/>', {
                        value: o.id,
                        text: o.displayName
                    }));
                });
            },
            error: function (xhr, status, error) {
            }
        });

        $("#forward-modal").modal();
    });
    $(document).on("click", ".backward-btn", function (event) {
        alert("backward");
    });

    $(document).on("click", ".forward-submit", function (event) {
 /*       var officeId = $("#offices_id").val();
        var proposal_id = $("#proposal_id").val();
        console.log(officeId);
        console.log(proposal_id);*/
    });

    $(document).on("click", "#uw_for", function (e) {

        // var officeCat = $('#officeCatId option:selected').val();

        // model.officeId = $('#offices_id option:selected').val();
        //
        // var prop_master = $("#proposal_id").val();
        // var split = bothDate.split(" - ");
        // var fromDate = split[0];
        // var toDate = split[1];
        // model.fromDate = fromDate;
        // model.toDate = toDate;
        //
        // model.maxLimit = $( "#maxLimit").val();
        // model.departmentId = $('#departmentId option:selected').val();
        // model.designationId = $('#designationId option:selected').val();
        // model.policyTp = $("input[name='policyTp']:checked").val();
        // model.lifeStd = $("input[name='lifeStd']:checked").val();
       var policyId =  $("#proposal_id").val() + "_id";
       var  model ={};
       model.officeId = $('#offices_id option:selected').val();
       model.proposalId = $("#proposal_id").val();
       model.policyTypeId = $("#"+policyId).text();





        console.log( $('#offices_id option:selected').val());

        $.confirm({
            title: 'Confirm!',
            content: "Are you sure to save this tasksss?",
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        $.ajax({
                            contentType: 'application/json',
                            url:  "forwardProposal",
                            type: 'POST',
                            async: false,
                            data: JSON.stringify(model),
                            success: function(response){
                            console.log(response);
                                showAlert(response);
                            },
                            error: function(xhr, status, error) {
                                alert("Something went wrong!");
                            }
                        });
                    }
                },
                cancel: function () {
                }
            }
        });



    });

});





