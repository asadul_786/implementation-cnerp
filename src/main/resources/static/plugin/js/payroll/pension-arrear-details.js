/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    //$('#driverNo').select2();

    var arrearNoDate = $("#arrearNoDate").val();
    $("#arrearNo").val(arrearNoDate);
    $.ajax({
        url: "/pension/getPensionArrearSetupDetails/" + arrearNoDate,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var circularCause = "";
            var arrearType = "";
            var noOfMonth = "";
            var payableMonth = "";
            var year = "";
            var applicableFor = "";
            var employeeID = "";



            $.each(response, function (i, l) {
                circularCause = l[0];
                arrearType = l[1];
                noOfMonth = l[2];
                payableMonth = l[3];
                year = l[4];
                applicableFor = l[5];
                employeeID = l[6];


            });

            $("#circularCause").val(circularCause).trigger('change');
            $("#arrearType").val(arrearType).trigger('change');
            $("#noOfMonth").val(noOfMonth);
            $("#payableMonth").val(payableMonth).trigger('change');
            $("#year").val(year).trigger('change');
            $("#applicableFor").val(applicableFor).trigger('change');
            $("#employeeID").val(employeeID);

            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var employeeGid = "";
                    var employeeName = "";



                    $.each(response, function (i, l) {
                        employeeGid = l[0];
                        employeeName = l[1];


                    });

                    $("#employeeGid").val(employeeGid);
                    $("#employeeName").val(employeeName);


                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $("#employeeGid").val("");
                    $("#employeeName").val("");



                    $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                }
            });

            $("#error_UDForApproval").text("");

        },
        error: function (xhr, status, error) {
            $("#circularCause").val("");
            $("#arrearType").val("");
            $("#noOfMonth").val("");
            $("#payableMonth").val("");
            $("#year").val("");
            $("#applicableFor").val("");
            $("#employeeID").val("");



            $("#error_UDForApproval").text("Sorry! Arrear No not match!!");
        }
    });

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#pensionArrearSetupDetailsSave").click(function () {
        saveData();
    });
    $("#pensionArrearSetupDetailsReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var pensionArrearSetupDetails = {};
        var arrearNo = $("#arrearNoDate").val();
        pensionArrearSetupDetails.arrearNo = arrearNo;

        pensionArrearSetupDetails.headCode = $("#headCode").val();
        pensionArrearSetupDetails.amountType = $("#amountType").val();
        pensionArrearSetupDetails.basedOn = $("#basedOn").val();
        pensionArrearSetupDetails.amountParcentage = $("#amountParcentage").val();
        pensionArrearSetupDetails.fixedAmount = $("#fixedAmount").val();

        pensionArrearSetupDetails.insertUser = $('#insertUser').val();


        return pensionArrearSetupDetails;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/pension/save-pension-arrear-details-post";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 1000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#circularCause").val('');
        $("#payableMonth").val('');
        $("#year").val('');
        $("#employeeID").val('');
        $("#employeeGid").val('');
        $("#arrearType").val('');
        $("#noOfMonth").val('');
        $("#applicableFor").val('');
        $("#employeeName").val('');

        $("#arrearNo").val('');
        $("#headCode").val('');
        $("#amountType").val('');
        $("#basedOn").val('');
        $("#amountParcentage").val('');
        $("#fixedAmount").val('');

        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#headCode").val())) {

            valdiationMsgContent = "Head Name is required!";
            $("#headCode").focus();
            return false;
        }
        /*else if (isEmptyString($("#vehicleNo").val())) {

            valdiationMsgContent = "Vehicle Name is required!";
            $("#vehicleNo").focus();
            return false;
        }
        else if (isEmptyString($("#driverNo").val())) {

            valdiationMsgContent = "Driver Name is required!";
            $("#driverNo").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceAmount").val())) {

            valdiationMsgContent = "Maintenance Amount is required!";
            $("#maintenanceAmount").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceDate").val())) {

            valdiationMsgContent = "Maintenance Date is required!";
            $("#maintenanceDate").focus();
            return false;
        }

        else if (isEmptyString($("#fileNumber").val())) {
            valdiationMsgContent = "File Number is required!";
            $("#fileNumber").focus();
            return false;
        }
        else if ($("#fileNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "File Number maximum 50 characters!!";
            $("#fileNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptNumber").val())) {
            valdiationMsgContent = "Receipt Number is required!";
            $("#receiptNumber").focus();
            return false;
        }
        else if ($("#receiptNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Receipt Number maximum 50 characters!!";
            $("#receiptNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptDate").val())) {
            valdiationMsgContent = "Receipt Date is required!";
            $("#receiptDate").focus();
            return false;
        }
        else if (isEmptyString($("#serviceCenterAddress").val())) {

            valdiationMsgContent = "Service Center Address is required!";
            $("#serviceCenterAddress").focus();
            return false;
        }
        else if ($("#serviceCenterAddress").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Service Center Address maximum 50 characters!!";
            $("#serviceCenterAddress").focus();
            return false;
        }
        else if (isEmptyString($("#serviceCenterName").val())) {

            valdiationMsgContent = "Service Center Name is required!";
            $("#serviceCenterName").focus();
            return false;
        }
        else if ($("#serviceCenterName").val().length >30) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Service Center Name maximum 30 characters!!";
            $("#serviceCenterName").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceRemarks").val())) {

            valdiationMsgContent = "Maintenance Remarks is required!";
            $("#maintenanceRemarks").focus();
            return false;
        }
        else if ($("#maintenanceRemarks").val().length >100) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Maintenance Remarks maximum 100 characters!!";
            $("#maintenanceRemarks").focus();
            return false;
        }*/

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var arrearNo = curRow.find('td:eq(0)').text();
        var headCode = curRow.find('td:eq(1)').text();
        var amountType = curRow.find('td:eq(2)').text();
        var basedOn = curRow.find('td:eq(3)').text();
        var amountParcentage = curRow.find('td:eq(4)').text();
        var fixedAmount = curRow.find('td:eq(5)').text();


        var insertUser = curRow.find('td:eq(7)').text();

        $('#arrearNo').val(arrearNo);
        $('#headCode').val(headCode).trigger('change');
        $('#amountType').val(amountType).trigger('change');
        $('#basedOn').val(basedOn).trigger('change');
        $('#amountParcentage').val(amountParcentage);
        $('#fixedAmount').val(fixedAmount);

        $('#insertUser').val(insertUser);

        $.ajax({
            url: "/pension/getPensionArrearSetupDetails/" + arrearNo,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var circularCause = "";
                var arrearType = "";
                var noOfMonth = "";
                var payableMonth = "";
                var year = "";
                var applicableFor = "";
                var employeeID = "";



                $.each(response, function (i, l) {
                    circularCause = l[0];
                    arrearType = l[1];
                    noOfMonth = l[2];
                    payableMonth = l[3];
                    year = l[4];
                    applicableFor = l[5];
                    employeeID = l[6];


                });

                $("#circularCause").val(circularCause).trigger('change');
                $("#arrearType").val(arrearType).trigger('change');
                $("#noOfMonth").val(noOfMonth);
                $("#payableMonth").val(payableMonth).trigger('change');
                $("#year").val(year).trigger('change');
                $("#applicableFor").val(applicableFor).trigger('change');
                $("#employeeID").val(employeeID);

                $.ajax({
                    url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var employeeGid = "";
                        var employeeName = "";



                        $.each(response, function (i, l) {
                            employeeGid = l[0];
                            employeeName = l[1];


                        });

                        $("#employeeGid").val(employeeGid);
                        $("#employeeName").val(employeeName);


                        $("#error_UDForApproval").text("");

                    },
                    error: function (xhr, status, error) {
                        $("#employeeGid").val("");
                        $("#employeeName").val("");



                        $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                    }
                });

                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $("#circularCause").val("");
                $("#arrearType").val("");
                $("#noOfMonth").val("");
                $("#payableMonth").val("");
                $("#year").val("");
                $("#applicableFor").val("");
                $("#employeeID").val("");



                $("#error_UDForApproval").text("Sorry! Arrear No not match!!");
            }
        });
        $.ajax({
            url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var employeeGid = "";
                var employeeName = "";



                $.each(response, function (i, l) {
                    employeeGid = l[0];
                    employeeName = l[1];


                });

                $("#employeeGid").val(employeeGid);
                $("#employeeName").val(employeeName);


                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $("#employeeGid").val("");
                $("#employeeName").val("");



                $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
            }
        });
        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/

    /*$('#dataTable tbody').on('click', '#details', function () {
        var curRow = $(this).closest('tr');
        var arrearNo = curRow.find('td:eq(0)').text();
        var headCode = curRow.find('td:eq(1)').text();
        var amountType = curRow.find('td:eq(2)').text();
        var basedOn = curRow.find('td:eq(3)').text();
        var amountParcentage = curRow.find('td:eq(4)').text();
        var fixedAmount = curRow.find('td:eq(5)').text();


        var insertUser = curRow.find('td:eq(7)').text();
        alert(insertUser);

        $('#arrearNo').val(arrearNo);
        $('#headCode').val(headCode).trigger('change');
        $('#amountType').val(amountType).trigger('change');
        $('#basedOn').val(basedOn).trigger('change');
        $('#amountParcentage').val(amountParcentage);
        $('#fixedAmount').val(fixedAmount);


        $('#arrearNo').val(arrearNo);
        $('#circularCause').val(circularCause).trigger('change');
        $('#arrearType').val(arrearType).trigger('change');
        $('#payableMonth').val(payableMonth).trigger('change');
        $('#year').val(year).trigger('change');
        $('#applicableFor').val(applicableFor).trigger('change');
        $('#noOfMonth').val(noOfMonth);
        $('#employeeID').val(employeeID);

        $('#insertUser').val(insertUser);

        $.ajax({
            url: "/pension/getPensionArrearSetupDetails/" + arrearNo,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var circularCause = "";
                var arrearType = "";
                var noOfMonth = "";
                var payableMonth = "";
                var year = "";
                var applicableFor = "";
                var employeeID = "";



                $.each(response, function (i, l) {
                    circularCause = l[0];
                    arrearType = l[1];
                    noOfMonth = l[2];
                    payableMonth = l[3];
                    year = l[4];
                    applicableFor = l[5];
                    employeeID = l[6];


                });

                $("#circularCause").val(circularCause).trigger('change');
                $("#arrearType").val(arrearType).trigger('change');
                $("#noOfMonth").val(noOfMonth);
                $("#payableMonth").val(payableMonth).trigger('change');
                $("#year").val(year).trigger('change');
                $("#applicableFor").val(applicableFor).trigger('change');
                $("#employeeID").val(employeeID);

                $.ajax({
                    url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var employeeGid = "";
                        var employeeName = "";



                        $.each(response, function (i, l) {
                            employeeGid = l[0];
                            employeeName = l[1];


                        });

                        $("#employeeGid").val(employeeGid);
                        $("#employeeName").val(employeeName);


                        $("#error_UDForApproval").text("");

                    },
                    error: function (xhr, status, error) {
                        $("#employeeGid").val("");
                        $("#employeeName").val("");



                        $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                    }
                });

                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $("#circularCause").val("");
                $("#arrearType").val("");
                $("#noOfMonth").val("");
                $("#payableMonth").val("");
                $("#year").val("");
                $("#applicableFor").val("");
                $("#employeeID").val("");



                $("#error_UDForApproval").text("Sorry! Arrear No not match!!");
            }
        });
        $.ajax({
            url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var employeeGid = "";
                var employeeName = "";



                $.each(response, function (i, l) {
                    employeeGid = l[0];
                    employeeName = l[1];


                });

                $("#employeeGid").val(employeeGid);
                $("#employeeName").val(employeeName);


                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $("#employeeGid").val("");
                $("#employeeName").val("");



                $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
            }
        });

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });*/


    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#employeeID", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeID = $('#employeeID').val();


        if (employeeID != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var employeeGid = "";
                    var employeeName = "";



                    $.each(response, function (i, l) {
                        employeeGid = l[0];
                        employeeName = l[1];


                    });

                    $("#employeeGid").val(employeeGid);
                    $("#employeeName").val(employeeName);


                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $("#employeeGid").val("");
                    $("#employeeName").val("");



                    $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeID').val() == "") {
                $("#employeeGid").val("");
                $("#employeeName").val("");
                $("#employeeID").val("");



            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });

    /*-------------- datalaod End two------------------------*/

    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#arrearNo", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var arrearNo = $('#arrearNo').val();


        if (arrearNo != "") {
            $.ajax({
                url: "/pension/getPensionArrearSetupDetails/" + arrearNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var circularCause = "";
                    var arrearType = "";
                    var noOfMonth = "";
                    var payableMonth = "";
                    var year = "";
                    var applicableFor = "";
                    var employeeID = "";



                    $.each(response, function (i, l) {
                        circularCause = l[0];
                        arrearType = l[1];
                        noOfMonth = l[2];
                        payableMonth = l[3];
                        year = l[4];
                        applicableFor = l[5];
                        employeeID = l[6];


                    });

                    $("#circularCause").val(circularCause).trigger('change');
                    $("#arrearType").val(arrearType).trigger('change');
                    $("#noOfMonth").val(noOfMonth);
                    $("#payableMonth").val(payableMonth).trigger('change');
                    $("#year").val(year).trigger('change');
                    $("#applicableFor").val(applicableFor).trigger('change');
                    $("#employeeID").val(employeeID);

                    $.ajax({
                        url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            var employeeGid = "";
                            var employeeName = "";



                            $.each(response, function (i, l) {
                                employeeGid = l[0];
                                employeeName = l[1];


                            });

                            $("#employeeGid").val(employeeGid);
                            $("#employeeName").val(employeeName);


                            $("#error_UDForApproval").text("");

                        },
                        error: function (xhr, status, error) {
                            $("#employeeGid").val("");
                            $("#employeeName").val("");



                            $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                        }
                    });

                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $("#circularCause").val("");
                    $("#arrearType").val("");
                    $("#noOfMonth").val("");
                    $("#payableMonth").val("");
                    $("#year").val("");
                    $("#applicableFor").val("");
                    $("#employeeID").val("");



                    $("#error_UDForApproval").text("Sorry! Arrear No not match!!");
                }
            });
        }
        else {

            if ($('#arrearNo').val() == "") {
                $("#arrearNo").val("");
                $("#circularCause").val("");
                $("#arrearType").val("");
                $("#noOfMonth").val("");
                $("#payableMonth").val("");
                $("#year").val("");
                $("#applicableFor").val("");
                $("#employeeID").val("");



            }
            else {
                $("#error_UDForApproval").text("ID maximum 12 characters!");
            }
        }

    });

    /*-------------- datalaod End two------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
