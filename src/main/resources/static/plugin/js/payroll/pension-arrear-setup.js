/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    //$('#driverNo').select2();

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#pensionArrearSetupSave").click(function () {
        saveData();
    });
    $("#pensionArrearSetupReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var pensionArrearSetupMst = {};
        pensionArrearSetupMst.arrearNo = $("#arrearNo").val();

        pensionArrearSetupMst.payableMonth = $("#payableMonth").val();
        pensionArrearSetupMst.year = $("#year").val();
        pensionArrearSetupMst.employeeGid = $("#employeeGid").val();
        pensionArrearSetupMst.arrearType = $("#arrearType").val();
        pensionArrearSetupMst.noOfMonth = $("#noOfMonth").val();
        pensionArrearSetupMst.applicableFor = $("#applicableFor").val();
        pensionArrearSetupMst.circularCause = $("#circularCause").val();

        pensionArrearSetupMst.insertUser = $('#insertUser').val();

       /* var maintenanceDateConvert = $('#maintenanceDate').val();
        if (maintenanceDateConvert) {
            maintenanceDateConvert = maintenanceDateConvert.split("/").reverse().join("/");
            maintenanceDateConvert = getDate(maintenanceDateConvert);
        }
        maintenance.maintenanceDate = maintenanceDateConvert;*/



        return pensionArrearSetupMst;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/pension/save-pension-arrear-setup-post";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 1000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#arrearNo").val('');
        $("#circularCause").val('');
        $("#payableMonth").val('');
        $("#year").val('');
        $("#employeeID").val('');
        $("#employeeGid").val('');
        $("#arrearType").val('');
        $("#noOfMonth").val('');
        $("#applicableFor").val('');
        $("#employeeName").val('');

        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#employeeID").val())) {

            valdiationMsgContent = "employee ID is required!";
            $("#employeeID").focus();
            return false;
        }
        /*else if (isEmptyString($("#vehicleNo").val())) {

            valdiationMsgContent = "Vehicle Name is required!";
            $("#vehicleNo").focus();
            return false;
        }
        else if (isEmptyString($("#driverNo").val())) {

            valdiationMsgContent = "Driver Name is required!";
            $("#driverNo").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceAmount").val())) {

            valdiationMsgContent = "Maintenance Amount is required!";
            $("#maintenanceAmount").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceDate").val())) {

            valdiationMsgContent = "Maintenance Date is required!";
            $("#maintenanceDate").focus();
            return false;
        }

        else if (isEmptyString($("#fileNumber").val())) {
            valdiationMsgContent = "File Number is required!";
            $("#fileNumber").focus();
            return false;
        }
        else if ($("#fileNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "File Number maximum 50 characters!!";
            $("#fileNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptNumber").val())) {
            valdiationMsgContent = "Receipt Number is required!";
            $("#receiptNumber").focus();
            return false;
        }
        else if ($("#receiptNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Receipt Number maximum 50 characters!!";
            $("#receiptNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptDate").val())) {
            valdiationMsgContent = "Receipt Date is required!";
            $("#receiptDate").focus();
            return false;
        }
        else if (isEmptyString($("#serviceCenterAddress").val())) {

            valdiationMsgContent = "Service Center Address is required!";
            $("#serviceCenterAddress").focus();
            return false;
        }
        else if ($("#serviceCenterAddress").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Service Center Address maximum 50 characters!!";
            $("#serviceCenterAddress").focus();
            return false;
        }
        else if (isEmptyString($("#serviceCenterName").val())) {

            valdiationMsgContent = "Service Center Name is required!";
            $("#serviceCenterName").focus();
            return false;
        }
        else if ($("#serviceCenterName").val().length >30) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Service Center Name maximum 30 characters!!";
            $("#serviceCenterName").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceRemarks").val())) {

            valdiationMsgContent = "Maintenance Remarks is required!";
            $("#maintenanceRemarks").focus();
            return false;
        }
        else if ($("#maintenanceRemarks").val().length >100) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Maintenance Remarks maximum 100 characters!!";
            $("#maintenanceRemarks").focus();
            return false;
        }*/

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var arrearNo = curRow.find('td:eq(0)').text();
        var circularCause = curRow.find('td:eq(1)').text();
        var arrearType = curRow.find('td:eq(2)').text();
        var noOfMonth = curRow.find('td:eq(3)').text();
        var payableMonth = curRow.find('td:eq(4)').text();
        var year = curRow.find('td:eq(5)').text();
        var applicableFor = curRow.find('td:eq(6)').text();
        var employeeGid = curRow.find('td:eq(7)').text();
        var employeeID = curRow.find('td:eq(8)').text();

        var insertUser = curRow.find('td:eq(9)').text();

        $('#arrearNo').val(arrearNo);
        $('#circularCause').val(circularCause).trigger('change');
        $('#arrearType').val(arrearType).trigger('change');
        $('#payableMonth').val(payableMonth).trigger('change');
        $('#year').val(year).trigger('change');
        $('#applicableFor').val(applicableFor).trigger('change');
        $('#noOfMonth').val(noOfMonth);
        $('#employeeID').val(employeeID);

        $('#insertUser').val(insertUser);

        $.ajax({
            url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var employeeGid = "";
                var employeeName = "";



                $.each(response, function (i, l) {
                    employeeGid = l[0];
                    employeeName = l[1];


                });

                $("#employeeGid").val(employeeGid);
                $("#employeeName").val(employeeName);


                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $("#employeeGid").val("");
                $("#employeeName").val("");



                $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
            }
        });
        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/

    $('#dataTable tbody').on('click', '#details', function () {
        var curRow = $(this).closest('tr');
        var arrearNo = curRow.find('td:eq(0)').text();


        $('#arrearNo').val(arrearNo);
        $("#btnCookie").bind("click", function () {
            $.cookie("arrearNo", $("#arrearNo").val());
            window.location.href = "Page2.htm";
        });


    });


    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#employeeID", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeID = $('#employeeID').val();


        if (employeeID != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeID,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var employeeGid = "";
                    var employeeName = "";



                    $.each(response, function (i, l) {
                        employeeGid = l[0];
                        employeeName = l[1];


                    });

                    $("#employeeGid").val(employeeGid);
                    $("#employeeName").val(employeeName);


                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $("#employeeGid").val("");
                    $("#employeeName").val("");



                    $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeID').val() == "") {
                $("#employeeGid").val("");
                $("#employeeName").val("");
                $("#employeeID").val("");



            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });

    /*-------------- datalaod End two------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
