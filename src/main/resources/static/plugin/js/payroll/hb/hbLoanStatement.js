/**
 * Created by Saifuddin on 4/20/2020.
 */

$(document).ready(function () {

    $("#Report").click(function () {

        if (1) {

            var application = $('#application').val();
            var emp_id = $('#emp_id').val();

            if(!application){
                // application="null"
                showAlert("Insert Application No!");
            }
            else if(!emp_id){
                // emp_id = "null"
                showAlert("Insert Employee ID!");
            }

            else{
                window.open('/hb/genHBLoanStatement.pdf?statement='+application+','+emp_id, '_blank')
            }

        }
    });
});