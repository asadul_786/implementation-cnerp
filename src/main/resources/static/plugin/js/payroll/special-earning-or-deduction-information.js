/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    //$('#driverNo').select2();

    clearForm();

    $("#religionDependent").on("change",function(){

        var religionDependent = $('#religionDependent').val();
        if(religionDependent == 0){
            $("#religion").attr("disabled","disabled");

        }else {

            $("#religion").removeAttr("disabled");

        }
    });

    $("#amountType").on("change",function(){

        var amountType = $('#amountType').val();
        if(amountType == "F"){
            $("#paymentParcentage").attr("disabled","disabled");
            $("#basedOn").attr("disabled","disabled");
            $("#fixedAmount").removeAttr("disabled");
        }else {

            $("#fixedAmount").attr("disabled","disabled");
            $("#basedOn").removeAttr("disabled");
            $("#paymentParcentage").removeAttr("disabled");
        }
    });



    /*-------------- asset save function start ------------------------*/
    $("#specialEarningOrDeductionInformationSave").click(function () {
        saveData();
    });
    $("#specialEarningOrDeductionInformationReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var specialEarningOrDeductionInformation = {};
        specialEarningOrDeductionInformation.circularNo = $("#circularNo").val();

        specialEarningOrDeductionInformation.udCircularNo = $("#udCircularNo").val();
        var circularDateConvert = $('#circularDate').val();
        if (circularDateConvert) {
            circularDateConvert = circularDateConvert.split("/").reverse().join("/");
            circularDateConvert = getDate(circularDateConvert);
        }
        specialEarningOrDeductionInformation.circularDate = circularDateConvert;

        specialEarningOrDeductionInformation.circularSubject = $("#circularSubject").val();
        var payableDateConvert = $('#payableDate').val();
        if (payableDateConvert) {
            payableDateConvert = payableDateConvert.split("/").reverse().join("/");
            payableDateConvert = getDate(payableDateConvert);
        }
        specialEarningOrDeductionInformation.payableDate = payableDateConvert;

        specialEarningOrDeductionInformation.applicableFor = $("#applicableFor").val();
        specialEarningOrDeductionInformation.salaryProcess = $("#salaryProcess").val();
        specialEarningOrDeductionInformation.religionDependent = $("#religionDependent").val();
        specialEarningOrDeductionInformation.religion = $("#religion").val();
        specialEarningOrDeductionInformation.headCode = $("#headCode").val();
        specialEarningOrDeductionInformation.headType = $("#headType").val();
        specialEarningOrDeductionInformation.amountType = $("#amountType").val();
        specialEarningOrDeductionInformation.basedOn = $("#basedOn").val();
        specialEarningOrDeductionInformation.empolyeeType = $("#empolyeeType").val();
        specialEarningOrDeductionInformation.paymentParcentage = $("#paymentParcentage").val();
        specialEarningOrDeductionInformation.fixedAmount = $("#fixedAmount").val();

        specialEarningOrDeductionInformation.insertUser = $('#insertUser').val();

        return specialEarningOrDeductionInformation;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/pension/save-special-earning-or-deduction-information-post";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 1000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#circularNo").val('');
        $("#udCircularNo").val('');
        $("#circularDate").val('');
        $("#circularSubject").val('');
        $("#payableDate").val('');
        $("#applicableFor").val('');
        $("#salaryProcess").val('');
        $("#religionDependent").val('');
        $("#religion").val('');
        $("#headCode").val('');
        $("#headType").val('');
        $("#amountType").val('');
        $("#basedOn").val('');
        $("#empolyeeType").val('');
        $("#paymentParcentage").val('');
        $("#fixedAmount").val('');

        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#udCircularNo").val())) {

            valdiationMsgContent = "udCircularNo is required!";
            $("#udCircularNo").focus();
            return false;
        }
        /*else if (isEmptyString($("#vehicleNo").val())) {

            valdiationMsgContent = "Vehicle Name is required!";
            $("#vehicleNo").focus();
            return false;
        }
        else if (isEmptyString($("#driverNo").val())) {

            valdiationMsgContent = "Driver Name is required!";
            $("#driverNo").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceAmount").val())) {

            valdiationMsgContent = "Maintenance Amount is required!";
            $("#maintenanceAmount").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceDate").val())) {

            valdiationMsgContent = "Maintenance Date is required!";
            $("#maintenanceDate").focus();
            return false;
        }

        else if (isEmptyString($("#fileNumber").val())) {
            valdiationMsgContent = "File Number is required!";
            $("#fileNumber").focus();
            return false;
        }
        else if ($("#fileNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "File Number maximum 50 characters!!";
            $("#fileNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptNumber").val())) {
            valdiationMsgContent = "Receipt Number is required!";
            $("#receiptNumber").focus();
            return false;
        }
        else if ($("#receiptNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Receipt Number maximum 50 characters!!";
            $("#receiptNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptDate").val())) {
            valdiationMsgContent = "Receipt Date is required!";
            $("#receiptDate").focus();
            return false;
        }
        else if (isEmptyString($("#serviceCenterAddress").val())) {

            valdiationMsgContent = "Service Center Address is required!";
            $("#serviceCenterAddress").focus();
            return false;
        }
        else if ($("#serviceCenterAddress").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Service Center Address maximum 50 characters!!";
            $("#serviceCenterAddress").focus();
            return false;
        }
        else if (isEmptyString($("#serviceCenterName").val())) {

            valdiationMsgContent = "Service Center Name is required!";
            $("#serviceCenterName").focus();
            return false;
        }
        else if ($("#serviceCenterName").val().length >30) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Service Center Name maximum 30 characters!!";
            $("#serviceCenterName").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceRemarks").val())) {

            valdiationMsgContent = "Maintenance Remarks is required!";
            $("#maintenanceRemarks").focus();
            return false;
        }
        else if ($("#maintenanceRemarks").val().length >100) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Maintenance Remarks maximum 100 characters!!";
            $("#maintenanceRemarks").focus();
            return false;
        }*/

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var circularNo = curRow.find('td:eq(0)').text();
        var udCircularNo = curRow.find('td:eq(1)').text();
        var circularDate = curRow.find('td:eq(2)').text();
        var circularSubject = curRow.find('td:eq(3)').text();
        var payableDate = curRow.find('td:eq(4)').text();
        var applicableFor = curRow.find('td:eq(5)').text();
        var salaryProcess = curRow.find('td:eq(6)').text();
        var religionDependent = curRow.find('td:eq(7)').text();
        var religion = curRow.find('td:eq(8)').text();
        var headCode = curRow.find('td:eq(9)').text();
        var headType = curRow.find('td:eq(10)').text();
        var amountType = curRow.find('td:eq(11)').text();
        var basedOn = curRow.find('td:eq(12)').text();
        var empolyeeType = curRow.find('td:eq(13)').text();
        var paymentParcentage = curRow.find('td:eq(14)').text();
        var fixedAmount = curRow.find('td:eq(15)').text();

        var insertUser = curRow.find('td:eq(16)').text();

        $('#circularNo').val(circularNo);
        $('#udCircularNo').val(udCircularNo);
        $('#circularDate').val(circularDate);
        $('#circularSubject').val(circularSubject);
        $('#payableDate').val(payableDate);
        $('#applicableFor').val(applicableFor).trigger('change');
        $('#salaryProcess').val(salaryProcess).trigger('change');
        $('#religionDependent').val(religionDependent).trigger('change');

        $('#religion').val(religion).trigger('change');
        $('#headCode').val(headCode).trigger('change');
        $('#headType').val(headType).trigger('change');
        $('#amountType').val(amountType).trigger('change');
        $('#basedOn').val(basedOn).trigger('change');
        $('#empolyeeType').val(empolyeeType).trigger('change');
        $('#paymentParcentage').val(paymentParcentage);
        $('#fixedAmount').val(fixedAmount);

        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/



    /*-------------- datalaod Start ------------------------*/
    /*-------------- datalaod End two------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
