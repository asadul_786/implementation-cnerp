$(document).ready(function () {
    loadOrganizationId();
    var premiumCalculation = $('#tbl-premium-Calculation');

    $(document).on("change", "#organizationId", function (e) {
        var pgId = $("#organizationId option:selected").val();
        if (pgId == '-1') {
            clrFrom();
        } else {
            loadSearchSumData(pgId);
            loadSearchInfoData(pgId);
            loadPremiumCalculationTabel(pgId);
        }
    });

    $("#btnAssuredReset").click(function () {
        clrFrom();
    });


    $("#btnApprovalSave").click(function () {
        var flag = dataValidation();
        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to Approve?");
        }
    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var organizationId = $('#organizationId').val();
                        var approveDate = $('#approveDate').val();
                        var apprvReInsRate = $('#apprvReInsRate').val();
                        var averageRate = $('#averageRate').val();
                        var approvalLevel = $('#approvalLevel').val();
                        var approvalRate = $('#approvalRate').val();
                        var approvedBy = $('#approvedBy').val();
                        if (approveDate) {
                            approveDate = approveDate.split("/").reverse().join("/");
                            approveDate = getFormateDate(approveDate);
                        }
                        var approveList = {};
                        approveList.organizationId = organizationId;
                        approveList.approveDate = approveDate;
                        approveList.apprvReInsRate = apprvReInsRate;
                        approveList.averageRate = averageRate;
                        approveList.approvalLevel = approvalLevel;
                        approveList.approvalRate = approvalRate;
                        approveList.approvedBy = approvedBy;
                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/groupinsurance-ajax/approveInsurance",
                            data: JSON.stringify(approveList),
                            dataType: 'json',
                            success: function (data) {
                                if (data.getStatus == "1") {
                                    showAlertByType('Insurance Approved successfully!', "S");
                                    clrFrom();
                                    loadOrganizationId();
                                }
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", "F");
                            }
                        });
                    }
                },
                cancel: function () {
                }
            }
        });
    };

    function dataValidation() {

        var status = true;

        if ($('#organizationId').val() == "-1") {
            status = false;
            $("#error_organizationId").text("Empty field found!!");
            $("#organizationId").focus();
        } else $("#error_organizationId").text("");

        if ($('#approveDate').val() == "") {
            status = false;
            $("#error_approveDate").text("Empty field found!!");
            $("#approveDate").focus();
        } else if (isValidDate($("#approveDate").val()) == false) {
            status = false;
            $("#error_approveDate").text("Invalid date format!!");
            $("#approveDate").focus();
        } else $("#error_approveDate").text("");



        if ($('#apprvReInsRate').val() != "") {
            if ($('#apprvReInsRate').val().length > 5) {
                status = false;
                $("#error_apprvReInsRate").text("Allow maximum 5 character!!");
                $("#apprvReInsRate").focus();
            } else  $("#error_apprvReInsRate").text("");
        } else $("#error_apprvReInsRate").text("");

        if ($('#averageRate').val() != "") {
            if ($('#averageRate').val().length > 10) {
                status = false;
                $("#error_averageRate").text("Allow maximum 10 character!!");
                $("#averageRate").focus();
            } else  $("#error_averageRate").text("");
        } else $("#error_averageRate").text("");


        if ($('#approvalLevel').val() != "") {
            if ($('#approvalLevel').val().length > 50) {
                status = false;
                $("#error_approvalLevel").text("Allow maximum 50 character!!");
                $("#approvalLevel").focus();
            } else  $("#error_approvalLevel").text("");
        } else $("#error_approvalLevel").text("");


        if ($('#approvalRate').val() != "") {
            if ($('#approvalRate').val().length > 10) {
                status = false;
                $("#error_approvalRate").text("Allow maximum 10 character!!");
                $("#approvalRate").focus();
            } else  $("#error_approvalRate").text("");
        } else $("#error_approvalRate").text("");


        if ($('#approvedBy').val() != "") {
            if ($('#approvedBy').val().length > 20) {
                status = false;
                $("#error_approvedBy").text("Allow maximum 20 character!!");
                $("#approvedBy").focus();
            } else  $("#error_approvedBy").text("");
        } else $("#error_approvedBy").text("");

        return status;
    }


    function loadPremiumCalculationTabel(organizationId) {
        var organizationId = organizationId;
        var i = 0;
        var premiumCalculationList = {};
        premiumCalculationList.organizationId = organizationId;
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/groupinsurance-ajax/loadOrganizewiseData",
            data: JSON.stringify(premiumCalculationList),
            dataType: 'json',
            success: function (data) {
                premiumCalculation.dataTable({
                    paging: true,
                    searching: true,
                    destroy: true,
                    data: data,
                    columns: [{
                        "data": "",
                        "render": function (data,
                                            type,
                                            row) {
                            return i += 1;
                        }
                    }, {
                        "data": "employeeName"
                    }, {
                        "data": "categoryName"
                    }, {
                        "data": "entryAge"
                    }, {
                        "data": "sumAssured"
                    }, {
                        "data": "actualRate"
                    }, {
                        "data": "lifePremium"
                    }, {
                        "data": "extraPremium"
                    }, {
                        "data": "premiumAmt"
                    },]
                });
            },
            error: function (e) {
                showAlertByType('Something Wrong!', "F");
            }
        });

    }

    function loadSearchSumData(pgId) {
        $.ajax({
            url: "/groupinsurance-ajax/getGrpPremiumSumData/" + pgId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response != '') {
                    var total_Assured = "";
                    var total_lifePremium = "";
                    var total_ExtraPremium = "";
                    var total_SumAssured = "";
                    $.each(response, function (i, l) {
                        total_Assured = l[0];
                        total_lifePremium = l[1];
                        total_ExtraPremium = l[2];
                        total_SumAssured = l[3];
                    });
                    if (total_Assured == null || total_Assured == '') {
                        total_Assured = '0';
                    }
                    if (total_lifePremium == null || total_lifePremium == '') {
                        total_lifePremium = '';
                    }
                    if (total_ExtraPremium == null || total_ExtraPremium == '') {
                        total_ExtraPremium = '';
                    }
                    if (total_SumAssured == null || total_SumAssured == '') {
                        total_SumAssured = '';
                    }
                    $("#totalLifeAssured").val(total_Assured);
                    $("#sumAssured").val(total_SumAssured + '.00');
                    $("#lifePremium").val(total_lifePremium + '.00');
                    $("#extraPremium").val(total_ExtraPremium + '.00');
                    if (total_ExtraPremium == '') {
                        $('#tbl-supl-Calculation tbody').empty();
                    } else {
                        var decrementPremium = parseInt(total_ExtraPremium - 2);
                        firstTable(decrementPremium);
                    }
                } else {
                    clrFrom();
                }
            },
            error: function (xhr, status, error) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }


    function loadSearchInfoData(pgId) {
        $.ajax({
            url: "/groupinsurance-ajax/getGrpPremiumInfoData/" + pgId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response != '') {
                    var installmentPremium = "";
                    var hospitalBenefite = "";
                    var productName = "";
                    var strengthName = "";
                    var term = "";
                    var deathBenefiteRate = "";
                    var survcingBenefitRate = "";
                    var matrityAge = "";
                    var averageRate = "";

                    $.each(response, function (i, l) {
                        installmentPremium = l[0];
                        hospitalBenefite = l[1];
                        productName = l[2];
                        strengthName = l[3];
                        term = l[4];
                        deathBenefiteRate = l[5];
                        survcingBenefitRate = l[6];
                        matrityAge = l[7];
                        averageRate = l[8];
                    });
                    if (installmentPremium == null || installmentPremium == '') {
                        installmentPremium = '';
                    }
                    if (hospitalBenefite == null || hospitalBenefite == '') {
                        hospitalBenefite = '';
                    }
                    if (productName == null || productName == '') {
                        productName = '';
                    }
                    if (strengthName == null || strengthName == '') {
                        strengthName = '';
                    }
                    if (term == null || term == '') {
                        term = '';
                    }
                    if (deathBenefiteRate == null || deathBenefiteRate == '') {
                        deathBenefiteRate = '0';
                    }
                    if (survcingBenefitRate == null || survcingBenefitRate == '') {
                        survcingBenefitRate = '0';
                    }
                    if (matrityAge == null || matrityAge == '') {
                        matrityAge = '';
                    }
                    if (averageRate == null || averageRate == '') {
                        averageRate = '';
                    }
                    $("#instalmentPremium").val(installmentPremium + '.00');
                    $("#hospitalization").val(hospitalBenefite + '.00');
                    $("#productName").val(productName);
                    $("#strenght").val(strengthName);
                    $("#term").val(term);
                    $("#deathBenefit").val(deathBenefiteRate);
                    $("#survicingBenefit").val(survcingBenefitRate);
                    $("#maturityAge").val(matrityAge);
                    $("#averageRate").val(averageRate);

                } else {
                    clrFrom();
                }
            },
            error: function (xhr, status, error) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }

    function clrFrom() {
        $("#organizationId").val("-1");
        $("#totalLifeAssured").val("");
        $("#sumAssured").val("");
        $("#lifePremium").val("");
        $("#extraPremium").val("");
        //
        $("#instalmentPremium").val("");
        $("#hospitalization").val("");
        $("#productName").val("");
        $("#strenght").val("");
        $("#term").val("");
        $("#deathBenefit").val("");
        $("#survicingBenefit").val("");
        $("#maturityAge").val("");
        $('#tbl-supl-Calculation tbody').empty();
        loadPremiumCalculationTabel("");
        //
        $("#error_approvedBy").text("");
        $("#error_approvalRate").text("");
        $("#error_approvalLevel").text("");
        $("#error_averageRate").text("");
        $("#error_apprvReInsRate").text("");
        $("#error_approveDate").text("");
        $("#error_organizationId").text("");
//
        $('#approveDate').val("");
        $('#apprvReInsRate').val("");
        $('#averageRate').val("");
        $('#approvalLevel').val("");
        $('#approvedBy').val("");
        $('#approvalRate').val("");
        $("#averageRate").val("");
    }


    function firstTable(extraPremium) {
        var tableLangeuage = $('#tbl-supl-Calculation');
        var tableBody = "";
        $('#tbl-supl-Calculation tbody').empty();
        tableBody += "<tr'>";
        tableBody += "<td style='text-align:center;'>" + 'PDAB' + "</td>";
        tableBody += "<td style='text-align:center;'>" + extraPremium + '.00' + "</td>";
        tableBody += "<tr>";
        tableLangeuage.append(tableBody);
    }

    function loadOrganizationId() {
        $.get("/groupinsurance-ajax/getGrpInsuranceApprovalData",
            function (data, status) {
                var organizationId = $('#organizationId');
                organizationId.empty();
                organizationId.append($('<option/>', {
                    value: "-1",
                    text: "--Select Organization--"
                }));
                $.each(data, function (index, orgData) {
                    organizationId.append($('<option/>', {
                        value: orgData[1],
                        text: orgData[2]
                    }));
                });
            });
    }

    function getFormateDate(date) {
        var d = new Date(date);
        return d;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    };

});


