$(document).ready(function () {

    $("#btnDataSheetReport").click(function () {
        var flag = dataValidation();
        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to generate this report?");
        }
    });

    $("#btnReset").click(function () {
        clrFrom();
    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var organizationId = $('#organizationId').val();
                        var grpDataSheetList = {};
                        grpDataSheetList.organizationId = organizationId;
                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/groupinsurance-reportAjax/grpDataSheetPreparationReport",
                            data: JSON.stringify(grpDataSheetList),
                            dataType: 'json',
                            success: function (data) {
                                alert(data.organizationId);
                                clrFrom();
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", "F");
                            }
                        });
                    }
                },
                cancel: function () {

                }
            }
        });
    };

    function dataValidation() {
        var status = true;
        if ($('#organizationId').val() == "-1") {
            status = false;
            $("#err_organizationId").text("Empty field found!!");
            $("#organizationId").focus();
        } else $("#err_organizationId").text("");
        return status;
    }

    function clrFrom() {
        $("#organizationId").val("-1");
        $("#err_organizationId").text("");
    }

});