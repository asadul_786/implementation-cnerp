$(document).ready(function () {

    $("#btn_demand_notice_save").click(function () {

        var flag = dataValidation();

        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to create demand notice creation?");
        }
    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var organizationId = $('#organizationId').val();

                        var demandNoticList = {};
                        demandNoticList.organizationId = organizationId;
                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/agentcommission/saveCollectionAdjustment",
                            data: JSON.stringify(demandNoticList),
                            dataType: 'json',
                            success: function (data) {
                                var getStatus = "";
                                $.each(data, function (key, val) {
                                    getStatus = val;
                                });
                                if (getStatus == 'Success') {
                                    //showAlert('Adjust Collection processed ' + getStatus);
                                    confirmMessage('Adjust Collection processed ' + getStatus, "S");
                                } else {
                                    //showAlertByType(getStatus, 'F');
                                    confirmMessage(getStatus, "F");
                                }
                                clearform();
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", 'F');
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };

    $("#btn_demand_notice_reset").click(function () {
        clearform();
    });

    function clearform() {
        $("#organizationId").val("-1");
        $("#err_organizationId").text("");
    }

    function dataValidation() {
        var status = true;
        if ($('#organizationId').val() == "-1") {
            status = false;
            $("#err_organizationId").text("Empty field found!!");
            $("#organizationId").focus();
        } else $("#err_organizationId").text("");

        return status;
    }

});