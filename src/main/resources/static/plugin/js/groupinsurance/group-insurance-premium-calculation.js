$(document).ready(function () {

    var premiumCalculation = $('#tbl-premium-Calculation');

    $(document).on("change", "#organizationId", function (e) {
        var pgId = $("#organizationId option:selected").val();
        if (pgId == '-1') {
            clrFrom();
        } else {
            loadSearchSumData(pgId);
            loadSearchInfoData(pgId);
            loadPremiumCalculationTabel(pgId);
        }
    });

    $("#btnPremiumCalReset").click(function () {
        clrFrom();
    });

    function loadPremiumCalculationTabel(organizationId) {
        var organizationId = organizationId;
        var i = 0;
        var premiumCalculationList = {};
        premiumCalculationList.organizationId = organizationId;
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/groupinsurance-ajax/loadOrganizewiseData",
            data: JSON.stringify(premiumCalculationList),
            dataType: 'json',
            success: function (data) {
                premiumCalculation.dataTable({
                    paging: true,
                    searching: true,
                    destroy: true,
                    data: data,
                    columns: [{
                        "data": "",
                        "render": function (data,
                                            type,
                                            row) {
                            return i += 1;
                        }
                    }, {
                        "data": "employeeName"
                    }, {
                        "data": "categoryName"
                    }, {
                        "data": "entryAge"
                    }, {
                        "data": "sumAssured"
                    }, {
                        "data": "actualRate"
                    }, {
                        "data": "lifePremium"
                    }, {
                        "data": "extraPremium"
                    }, {
                        "data": "premiumAmt"
                    },]
                });
            },
            error: function (e) {
                showAlertByType('Something Wrong!', "F");
            }
        });

    }


    function loadSearchSumData(pgId) {
        $.ajax({
            url: "/groupinsurance-ajax/getGrpPremiumSumData/" + pgId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response != '') {
                    var total_Assured = "";
                    var total_lifePremium = "";
                    var total_ExtraPremium = "";
                    var total_SumAssured = "";
                    $.each(response, function (i, l) {
                        total_Assured = l[0];
                        total_lifePremium = l[1];
                        total_ExtraPremium = l[2];
                        total_SumAssured = l[3];
                    });
                    if (total_Assured == null || total_Assured == '') {
                        total_Assured = '0';
                    }
                    if (total_lifePremium == null || total_lifePremium == '') {
                        total_lifePremium = '';
                    }
                    if (total_ExtraPremium == null || total_ExtraPremium == '') {
                        total_ExtraPremium = '';
                    }
                    if (total_SumAssured == null || total_SumAssured == '') {
                        total_SumAssured = '';
                    }
                    $("#totalLifeAssured").val(total_Assured);
                    $("#sumAssured").val(total_SumAssured + '.00');
                    $("#lifePremium").val(total_lifePremium + '.00');
                    $("#extraPremium").val(total_ExtraPremium + '.00');
                    if (total_ExtraPremium == '') {
                        $('#tbl-supl-Calculation tbody').empty();
                    }else{
                        var decrementPremium = parseInt(total_ExtraPremium - 2);
                        firstTable(decrementPremium);
                    }
                } else {
                    clrFrom();
                }
            },
            error: function (xhr, status, error) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }


    function loadSearchInfoData(pgId) {
        $.ajax({
            url: "/groupinsurance-ajax/getGrpPremiumInfoData/" + pgId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response != '') {
                    var installmentPremium = "";
                    var hospitalBenefite = "";
                    var productName = "";
                    var strengthName = "";
                    var term = "";
                    var deathBenefiteRate = "";
                    var survcingBenefitRate = "";
                    var matrityAge = "";

                    $.each(response, function (i, l) {
                        installmentPremium = l[0];
                        hospitalBenefite = l[1];
                        productName = l[2];
                        strengthName = l[3];
                        term = l[4];
                        deathBenefiteRate = l[5];
                        survcingBenefitRate = l[6];
                        matrityAge = l[7];
                    });
                    if (installmentPremium == null || installmentPremium == '') {
                        installmentPremium = '';
                    }
                    if (hospitalBenefite == null || hospitalBenefite == '') {
                        hospitalBenefite = '';
                    }
                    if (productName == null || productName == '') {
                        productName = '';
                    }
                    if (strengthName == null || strengthName == '') {
                        strengthName = '';
                    }
                    if (term == null || term == '') {
                        term = '';
                    }
                    if (deathBenefiteRate == null || deathBenefiteRate == '') {
                        deathBenefiteRate = '0';
                    }
                    if (survcingBenefitRate == null || survcingBenefitRate == '') {
                        survcingBenefitRate = '0';
                    }
                    if (matrityAge == null || matrityAge == '') {
                        matrityAge = '';
                    }

                    $("#instalmentPremium").val(installmentPremium + '.00');
                    $("#hospitalization").val(hospitalBenefite + '.00');
                    $("#productName").val(productName);
                    $("#strenght").val(strengthName);
                    $("#term").val(term);
                    $("#deathBenefit").val(deathBenefiteRate);
                    $("#survicingBenefit").val(survcingBenefitRate);
                    $("#maturityAge").val(matrityAge);

                } else {
                    clrFrom();
                }
            },
            error: function (xhr, status, error) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }

    function clrFrom() {
        $("#organizationId").val("-1");
        $("#totalLifeAssured").val("");
        $("#sumAssured").val("");
        $("#lifePremium").val("");
        $("#extraPremium").val("");
        //
        $("#instalmentPremium").val("");
        $("#hospitalization").val("");
        $("#productName").val("");
        $("#strenght").val("");
        $("#term").val("");
        $("#deathBenefit").val("");
        $("#survicingBenefit").val("");
        $("#maturityAge").val("");
        $('#tbl-supl-Calculation tbody').empty();
        loadPremiumCalculationTabel("");
    }


    function firstTable(extraPremium) {
        var tableLangeuage = $('#tbl-supl-Calculation');
        var tableBody = "";
        $('#tbl-supl-Calculation tbody').empty();
        tableBody += "<tr'>";
        tableBody += "<td style='text-align:center;'>" + 'PDAB' + "</td>";
        tableBody += "<td style='text-align:center;'>" + extraPremium + '.00' + "</td>";
        tableBody += "<tr>";
        tableLangeuage.append(tableBody);
    }

});


