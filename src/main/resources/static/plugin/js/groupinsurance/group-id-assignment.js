$(document).ready(function () {

    loadSelectData();

    $(document).on("change", "#organizationId", function (e) {
        var pgId = $("#organizationId option:selected").val();
        loadSearchData(pgId);
    });

    $("#btnIdAssignmentSave").click(function () {
        var flag = dataValidation();
        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to save Id Assignment?");
        }
    });


    $("#btnIdAssignmentReset").click(function () {
        clrFrom();
    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var organizationId = $('#organizationId').val();
                        var proposalDate = $('#proposalDate').val();
                        var sumAssured = $('#sumAssured').val();
                        var officeName = $('#officeName').val();
                        var agentName = $('#agentName').val();
                        var lifePremium = $('#lifePremium').val();
                        var extraPremium = $('#extraPremium').val();
                        var installmentPremium = $('#installmentPremium').val();
                        var policyNo = $('#policyNo').val();
                        var commDate = $('#commDate').val();
                        var riskDate = $('#riskDate').val();

                        if (proposalDate) {
                            proposalDate = proposalDate.split("/").reverse().join("/");
                            proposalDate = getFormateDate(proposalDate);
                        }
                        if (commDate) {
                            commDate = commDate.split("/").reverse().join("/");
                            commDate = getFormateDate(commDate);
                        }
                        if (riskDate) {
                            riskDate = riskDate.split("/").reverse().join("/");
                            riskDate = getFormateDate(riskDate);
                        }

                        var idAssignmentList = {};
                        idAssignmentList.organizationId = organizationId;
                        idAssignmentList.proposalDate = proposalDate;
                        idAssignmentList.sumAssured = sumAssured;
                        idAssignmentList.officeName = officeName;
                        idAssignmentList.agentName = agentName;
                        idAssignmentList.lifePremium = lifePremium;
                        idAssignmentList.extraPremium = extraPremium;
                        idAssignmentList.installmentPremium = installmentPremium;
                        idAssignmentList.policyNo = policyNo;
                        idAssignmentList.commDate = commDate;
                        idAssignmentList.riskDate = riskDate;

                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/groupinsurance-ajax/updateGrpIdAssignmentInfo",
                            data: JSON.stringify(idAssignmentList),
                            dataType: 'json',
                            success: function (data) {
                                if (data.getStatus == "1") {
                                    showAlertByType('Save successfully!', "S");
                                    clrFrom();
                                    loadSelectData();
                                }
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", "F");
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };

    function dataValidation() {

        var status = true;

        if ($('#organizationId').val() == "-1") {
            status = false;
            $("#error_organizationId").text("Empty field found!!");
            $("#organizationId").focus();
        } else $("#error_organizationId").text("");

        if ($('#policyNo').val() == "") {
            status = false;
            $("#error_policyNo").text("Empty field found!!");
            $("#policyNo").focus();
        } else if ($('#policyNo').val().length > 10) {
            status = false;
            $("#error_policyNo").text("Allow maximum 10 character!!");
            $("#policyNo").focus();
        } else $("#error_policyNo").text("");

        if ($('#commDate').val() == "") {
            status = false;
            $("#error_commDate").text("Empty field found!!");
            $("#commDate").focus();
        } else if (isValidDate($("#commDate").val()) == false) {
            status = false;
            $("#error_commDate").text("Invalid date format!!");
            $("#commDate").focus();
        } else $("#error_commDate").text("");

        if ($('#riskDate').val() == "") {
            status = false;
            $("#error_riskDate").text("Empty field found!!");
            $("#riskDate").focus();
        } else if (isValidDate($("#riskDate").val()) == false) {
            status = false;
            $("#error_riskDate").text("Invalid date format!!");
            $("#riskDate").focus();
        } else $("#error_riskDate").text("");

        return status;
    }

    function loadSearchData(pgId) {
        $.ajax({
            url: "/groupinsurance-ajax/getGrpIdAssignmentInfo/" + pgId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response != '') {
                    var proposalDate = "";
                    var sumAssured = "";
                    var officeName = "";
                    var agentName = "";
                    var lifePremium = "";
                    var extraPremium = "";
                    var installmentPremium = "";
                    $.each(response, function (i, l) {
                        proposalDate = l[0];
                        sumAssured = l[1];
                        officeName = l[2];
                        agentName = l[3];
                        lifePremium = l[4];
                        extraPremium = l[5];
                        installmentPremium = l[6];
                    });
                    if (proposalDate == null || proposalDate == '') {
                        proposalDate = '';
                    }
                    if (sumAssured == null || sumAssured == '') {
                        sumAssured = '.00';
                    }
                    if (officeName == null || officeName == '') {
                        officeName = '';
                    }
                    if (agentName == null || agentName == '') {
                        agentName = '';
                    }
                    if (lifePremium == null || lifePremium == '') {
                        lifePremium = '.00';
                    }
                    if (extraPremium == null || extraPremium == '') {
                        extraPremium = '.00';
                    }
                    if (installmentPremium == null || installmentPremium == '') {
                        installmentPremium = '.00';
                    }

                    $("#sumAssured").val(sumAssured);
                    $("#officeName").val(officeName);
                    $("#agentName").val(agentName);
                    $("#lifePremium").val(lifePremium);
                    $("#extraPremium").val(extraPremium);
                    $("#installmentPremium").val(installmentPremium);

                    if (proposalDate == '') {
                        $("#proposalDate").val('');
                    } else {
                        $("#proposalDate").val(getDate(proposalDate));
                    }

                } else {
                    $("#sumAssured").val("");
                    $("#officeName").val("");
                    $("#agentName").val("");
                    $("#lifePremium").val("");
                    $("#extraPremium").val("");
                    $("#installmentPremium").val("");
                    $("#proposalDate").val("");
                }
            },
            error: function (xhr, status, error) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }

    function clrFrom() {
        $("#organizationId").val("-1");
        $("#proposalDate").val("");
        $("#sumAssured").val("");
        $("#officeName").val("");
        $("#agentName").val("");
        $("#lifePremium").val("");
        $("#extraPremium").val("");
        $("#installmentPremium").val("");
        $("#policyNo").val("");
        $("#commDate").val("");
        $("#riskDate").val("");
        //error
        $("#error_policyNo").text("");
        $("#error_commDate").text("");
        $("#error_riskDate").text("");
        $("#error_organizationId").text("");
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    };

    function getFormateDate(date) {
        var d = new Date(date);
        return d;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    function loadSelectData() {
        $.get("/groupinsurance-ajax/getOrganizationData",
            function (data, status) {
                var organizationId = $('#organizationId');
                organizationId.empty();
                organizationId.append($('<option/>', {
                    value: "-1",
                    text: "--Select Organization--"
                }));
                $.each(data, function (index, offices) {
                    organizationId.append($('<option/>', {
                        value: offices[2],
                        text: offices[1]
                    }));
                });
            });
    }

});