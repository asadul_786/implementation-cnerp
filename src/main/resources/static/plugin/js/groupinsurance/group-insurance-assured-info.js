$(document).ready(function () {
    $("#categoryName").select2();
    var i = 0;
    var table = $("#tblAssured").DataTable({
        "processing": true,
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>',
        },
        "pageLength": 10,
        ajax: {
            "url": "/groupinsurance-ajax/getAssuredList",
            "type": "GET",
            "dataType": "json"
        },
        "autoWidth": true,
        "columns": [{
            "data": "",
            "render": function (data,
                                type,
                                row) {
                return i += 1;
            }
        }, {
            "data": "categoryName",
            "name": "CATEGORY_NM"
        }, {
            "data": "employeeName",
            "name": "EMP_NAME"
        }, {
            "data": "dateOfBirth",
            "name": "DOB",
            "render": function (data,
                                type,
                                row) {
                return data == null ? '' : getDate(data);
            }
        }, {
            "data": "entryAge",
            "name": "ENTRY_AGE"
        }, {
            "data": "maturityAge",
            "name": "MATURITY_AGE"
        }, {
            "data": "basicSalary",
            "name": "BASIC_SALARY"
        }, {
            "data": "sumAssured",
            "name": "SUM_ASSURED"
        }, {
            "data": "incrementSumAssured",
            "name": "INCRE_SUM_ASSURED"
        }, {
            "data": "lastincrementBasic",
            "name": "LAST_INCRE_BASIC"
        }, {
            "data": "pgId",
            "name": "PGID",
            "visible": false
        }, {
            "data": "employeeId",
            "name": "EMP_ID",
            "visible": false
        }, {
            "data": "designationCode",
            "name": "DESIG_CD",
            "visible": false
        }, {
            "data": "fatherName",
            "name": "FATHER_NAME",
            "visible": false
        }, {
            "data": "categoryCd",
            "name": "CATEGORY_CD",
            "visible": false
        }, {
            "data": "entryDate",
            "name": "ENTRY_DT",
            "visible": false
        }, {
            "data": "status",
            "name": "STATUS",
            "visible": false
        }, {
            "data": "slNo",
            "name": "SL_NO",
            "visible": false
        }, {
            "className": "dt-btn",
            "render": function () {
                return '<button id="editBtnAssured" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></button>';
            }
        },
            {
                "className": "dt-btn",
                "render": function () {
                    return '<button id="deleteAssured" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                }
            }
        ],
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
            var values = Object.values(aData);
            $('td:eq(1)', nRow).attr("data-id", values[0]);
            $('td:eq(2)', nRow).attr("data-id", values[1]);
            $('td:eq(3)', nRow).attr("data-id", values[3]);
            $('td:eq(4)', nRow).attr("data-id", values[4]);
            $('td:eq(5)', nRow).attr("data-id", values[8]);
            $('td:eq(6)', nRow).attr("data-id", values[9]);
            $('td:eq(7)', nRow).attr("data-id", values[12]);
            $('td:eq(8)', nRow).attr("data-id", values[16]);
        }
    });

    $("#btnAssuredReset").click(function () {
        clrFrom();
    });

    $("#btnAssuredSave").click(function () {
        var flag = dataValidation();
        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to save assured information?");
        }
    });

    $('#tblAssured tbody').on('click', '#deleteAssured', function () {
        var curRow = $(this).closest('tr');
        var pgId = curRow.find('td:eq(1)').attr("data-id");
        var slNo = curRow.find('td:eq(8)').attr("data-id");
        if (pgId != '' && slNo != '') {
            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        $.ajax({
                            url: "/groupinsurance-ajax/deleteAssured/" + pgId + "/" + slNo,
                            type: 'GET',
                            dataType: 'json',
                            success: function (response) {
                                if (response.getStatus == '1') {
                                    table.row(curRow).remove().draw(false);
                                    showAlertByType('Deleted Successfully!!', "S");
                                    clrFrom();
                                }
                            },
                            error: function (xhr, status, error) {
                                showAlertByType('Something Wrong!', "F");
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }
    });

    //for update

    $('#tblAssured tbody').on('click', '#editBtnAssured', function () {
        errorResolve();
        $("#btnAssuredSave").html('Update' + ' ' + "<i class='fa fa-pencil-square-o'></i>");
        $('#organizationId').prop('disabled', true);
        var curRow = $(this).closest('tr');
        var pgId = curRow.find('td:eq(1)').attr("data-id");
        var employeeId = curRow.find('td:eq(2)').attr("data-id");
        var designationCode = curRow.find('td:eq(3)').attr("data-id");
        var fatherName = curRow.find('td:eq(4)').attr("data-id");
        var CategoryCode = curRow.find('td:eq(6)').attr("data-id");
        var Status = curRow.find('td:eq(5)').attr("data-id");
        var entryDate = curRow.find('td:eq(7)').attr("data-id");
        var slNo = curRow.find('td:eq(8)').attr("data-id");
        //text
        var entryAge = curRow.find('td:eq(4)').text();
        var basicSalary = curRow.find('td:eq(6)').text();
        var employeeName = curRow.find('td:eq(2)').text();
        var dateOfBirth = curRow.find('td:eq(3)').text();
        var lastIncrementBasic = curRow.find('td:eq(9)').text();
        var incrementSumAssured = curRow.find('td:eq(8)').text();
        var sumAssured = curRow.find('td:eq(7)').text();
        var categoryName = curRow.find('td:eq(1)').text();

        if (typeof(pgId) === "undefined" || pgId == '' || pgId == null) {
            pgId = '';
        }
        if (typeof(employeeId) === "undefined" || employeeId == '' || employeeId == null) {
            employeeId = '';
        }
        if (typeof(designationCode) === "undefined" || designationCode == '' || designationCode == null) {
            designationCode = '-1';
        }
        if (typeof(CategoryCode) === "undefined" || CategoryCode == '' || CategoryCode == null) {
            CategoryCode = '-1';
        }
        if (typeof(Status) === "undefined" || Status == '' || Status == null) {
            Status = '';
        }
        if (typeof(slNo) === "undefined" || slNo == '' || slNo == null) {
            slNo = '';
        }
        if (typeof(entryDate) === "undefined" || entryDate == '' || entryDate == null) {
            $("#entryDate").val("");
        } else {
            $("#entryDate").val(getDate(entryDate));
        }
        //text exception handle
        if (entryAge == '' || entryAge == null || typeof(entryAge) === "undefined") {
            entryAge = '';
        }
        if (basicSalary == '' || basicSalary == null || typeof(basicSalary) === "undefined") {
            basicSalary = '';
        }
        if (employeeName == '' || employeeName == null || typeof(employeeName) === "undefined") {
            employeeName = '';
        }
        if (dateOfBirth == '' || dateOfBirth == null || typeof(dateOfBirth) === "undefined") {
            dateOfBirth = '';
        }
        if (lastIncrementBasic == '' || lastIncrementBasic == null || typeof(lastIncrementBasic) === "undefined") {
            lastIncrementBasic = '';
        }
        if (incrementSumAssured == '' || incrementSumAssured == null || typeof(incrementSumAssured) === "undefined") {
            incrementSumAssured = '';
        }
        if (sumAssured == '' || sumAssured == null || typeof(sumAssured) === "undefined") {
            sumAssured = '';
        }
        if (Status == 1) {
            $("#status").prop('checked', true);
        } else {
            $("#status").prop('checked', false);
        }
        $("#employeeId").val(employeeId);
        $('#designationCode').val(designationCode);
        $('#designationCode').selectpicker('refresh');
        $("#fatherName").val(fatherName);
        $("#entryAge").val(entryAge);
        $("#basicSalary").val(basicSalary);
        $("#employeeName").val(employeeName);
        $("#categoryName").val(CategoryCode).select2();
        $("#dateOfBirth").val(dateOfBirth);
        $("#incrementedBasic").val(lastIncrementBasic);
        $("#incrementSumAssured").val(incrementSumAssured);
        $("#updatepgId").val(pgId);
        $("#organizationId").val(pgId);
        $("#sumAssured").val(sumAssured);

        var getData = $('#categoryName');
        getData.empty();
        if (CategoryCode == '-1') {
            getData.append($('<option/>', {
                value: "-1",
                text: "--Select Category--"
            }));
        } else {
            getData.append($('<option/>', {
                value: CategoryCode,
                text: categoryName
            }));
        }

        $("#slNo").val(slNo);
        if (pgId != '') {
            updateLoadData(pgId);
        }
        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);
    });

    $(document).on("change", "#organizationId", function (e) {
        var pgId = $("#organizationId option:selected").val();
        loadSearchData(pgId);
        loadSelectData(pgId);
    });

    $(document).on("input", "#basicSalary", function (e) {
        calSumAssured();
    });

    function calSumAssured() {
        var basicSalary = $("#basicSalary").val();
        var basicMultiplyBy = $("#basicMultiplyBy").val();
        var sum = parseFloat(basicSalary) * parseFloat(basicMultiplyBy);
        $("#sumAssured").val(sum);
    }

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var organizationId = $('#organizationId').val();
                        var proposalDate = $('#proposalDate').val();
                        var basicMultiplyBy = $('#basicMultiplyBy').val();
                        var slNo = $('#slNo').val();
                        var employeeName = $('#employeeName').val();
                        var fatherName = $('#fatherName').val();
                        var employeeId = $('#employeeId').val();
                        var incrementedBasic = $('#incrementedBasic').val();
                        var entryDate = $('#entryDate').val();
                        var designationCode = $('#designationCode').val();
                        var categoryName = $('#categoryName').val();
                        var maturityAge = $('#maturityAge').val();
                        var incrementSumAssured = $('#incrementSumAssured').val();
                        var dateOfBirth = $('#dateOfBirth').val();
                        var entryAge = $('#entryAge').val();
                        var basicSalary = $('#basicSalary').val();
                        var sumAssured = $('#sumAssured').val();
                        var updatepgId = $('#updatepgId').val();
                        var status = "";

                        if ($("#status").prop('checked') == true) {
                            status = '1';
                        } else {
                            status = '0';
                        }
                        if (categoryName == '-1') {
                            categoryName = '';
                        }
                        if (dateOfBirth) {
                            dateOfBirth = dateOfBirth.split("/").reverse().join("/");
                            dateOfBirth = getFormateDate(dateOfBirth);
                        }
                        if (entryDate) {
                            entryDate = entryDate.split("/").reverse().join("/");
                            entryDate = getFormateDate(entryDate);
                        }
                        if (proposalDate) {
                            proposalDate = proposalDate.split("/").reverse().join("/");
                            proposalDate = getFormateDate(proposalDate);
                        }

                        var assuredInfoList = {};
                        assuredInfoList.organizationId = organizationId;
                        assuredInfoList.proposalDate = proposalDate;
                        assuredInfoList.basicMultiplyBy = basicMultiplyBy;
                        assuredInfoList.slNo = slNo;
                        assuredInfoList.employeeName = employeeName;
                        assuredInfoList.fatherName = fatherName;
                        assuredInfoList.employeeId = employeeId;
                        assuredInfoList.incrementedBasic = incrementedBasic;
                        assuredInfoList.entryDate = entryDate;
                        assuredInfoList.designationCode = designationCode;
                        assuredInfoList.status = status;
                        assuredInfoList.categoryName = categoryName;
                        assuredInfoList.maturityAge = maturityAge;
                        assuredInfoList.incrementSumAssured = incrementSumAssured;
                        assuredInfoList.dateOfBirth = dateOfBirth;
                        assuredInfoList.entryAge = entryAge;
                        assuredInfoList.basicSalary = basicSalary;
                        assuredInfoList.sumAssured = sumAssured;
                        assuredInfoList.updatepgId = updatepgId;

                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/groupinsurance-ajax/saveAssuredInfo",
                            data: JSON.stringify(assuredInfoList),
                            dataType: 'json',
                            success: function (data) {
                                if (data.getStatus == "INSERT") {
                                    showAlertByType('Save successfully!', "S");
                                } else {
                                    showAlertByType('Update successfully!', "S");
                                }
                                i = 0;
                                table.ajax.reload();
                                clrFrom();

                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", "F");
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };


    function dataValidation() {

        var status = true;

        if ($('#fatherName').val() != "") {
            if ($('#fatherName').val().length > 100) {
                status = false;
                $("#error_fatherName").text("Allow maximum 100 character!!");
                $("#fatherName").focus();
            } else  $("#error_fatherName").text("");
        } else $("#error_fatherName").text("");

        if ($('#employeeId').val() != "") {
            if ($('#employeeId').val().length > 10) {
                status = false;
                $("#error_employeeId").text("Allow maximum 10 character!!");
                $("#employeeId").focus();
            } else  $("#error_employeeId").text("");
        } else $("#error_employeeId").text("");

        if ($('#incrementedBasic').val() != "") {
            if ($('#incrementedBasic').val().length > 12) {
                status = false;
                $("#error_incrementedBasic").text("Allow maximum 12 character!!");
                $("#incrementedBasic").focus();
            } else  $("#error_incrementedBasic").text("");
        } else $("#error_incrementedBasic").text("");

        if ($('#incrementSumAssured').val() != "") {
            if ($('#incrementSumAssured').val().length > 12) {
                status = false;
                $("#error_incrementSumAssured").text("Allow maximum 12 character!!");
                $("#incrementSumAssured").focus();
            } else  $("#error_incrementSumAssured").text("");
        } else $("#error_incrementSumAssured").text("");

        if ($('#organizationId').val() == "-1") {
            status = false;
            $("#error_organizationId").text("Empty field found!!");
            $("#organizationId").focus();
        } else $("#error_organizationId").text("");

        if ($('#employeeName').val() == "") {
            status = false;
            $("#error_employeeName").text("Empty field found!!");
            $("#employeeName").focus();
        } else if ($('#employeeName').val().length > 100) {
            status = false;
            $("#error_employeeName").text("Allow maximum 100 character!!");
            $("#employeeName").focus();
        } else $("#error_employeeName").text("");

        if ($('#dateOfBirth').val() == "") {
            status = false;
            $("#error_dateOfBirth").text("Empty field found!!");
            $("#dateOfBirth").focus();
        } else if (check_validDateOfBirth($("#dateOfBirth").val())) {
            status = false;
            $("#error_dateOfBirth").text("Birth date should not be greater than the current Date!!");
            $("#dateOfBirth").focus();
        } else if (isValidDate($("#dateOfBirth").val()) == false) {
            status = false;
            $("#error_dateOfBirth").text("Invalid date format!!");
            $("#dateOfBirth").focus();
        } else $("#error_dateOfBirth").text("");

        if ($('#entryAge').val() == "") {
            status = false;
            $("#error_entryAge").text("Empty field found!!");
            $("#entryAge").focus();
        } else if ($('#entryAge').val().length > 3) {
            status = false;
            $("#error_entryAge").text("Allow maximum 3 character!!");
            $("#entryAge").focus();
        } else $("#error_entryAge").text("");

        if ($('#basicSalary').val() == "") {
            status = false;
            $("#error_basicSalary").text("Empty field found!!");
            $("#basicSalary").focus();
        } else if ($('#basicSalary').val().length > 10) {
            status = false;
            $("#error_basicSalary").text("Allow maximum 10 character!!");
            $("#basicSalary").focus();
        } else $("#error_basicSalary").text("");

        if ($('#sumAssured').val() == "") {
            status = false;
            $("#error_sumAssured").text("Empty field found!!");
            $("#sumAssured").focus();
        } else if ($('#sumAssured').val().length > 12) {
            status = false;
            $("#error_sumAssured").text("Allow maximum 12 character!!");
            $("#sumAssured").focus();
        } else $("#error_sumAssured").text("");

        if ($('#entryDate').val() == "") {
            status = false;
            $("#error_entryDate").text("Empty field found!!");
            $("#entryDate").focus();
        } else if (isValidDate($("#entryDate").val()) == false) {
            status = false;
            $("#error_entryDate").text("Invalid date format!!");
            $("#entryDate").focus();
        } else $("#error_entryDate").text("");

        if ($('#designationCode').val() == "-1") {
            status = false;
            $("#error_designationCode").text("Empty field found!!");
            $("#designationCode").focus();
        } else $("#error_designationCode").text("");

        return status;
    }

    function loadSearchData(pgId) {
        $.ajax({
            url: "/groupinsurance-ajax/getGrpAssuredSearch/" + pgId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response != '') {
                    var pgId = "";
                    var dependsOnMultiplyBy = "";
                    var dependOn = "";
                    var proposalDate = "";
                    var maturityAge = "";
                    $.each(response, function (i, l) {
                        pgId = l[0];
                        dependsOnMultiplyBy = l[1];
                        dependOn = l[2];
                        proposalDate = l[3];
                        maturityAge = l[4];
                    });
                    if (proposalDate == null || proposalDate == '') {
                        proposalDate = '';
                    }
                    if (pgId == null || pgId == '') {
                        pgId = '';
                    }
                    if (dependsOnMultiplyBy == null || dependsOnMultiplyBy == '') {
                        dependsOnMultiplyBy = '0';
                    }
                    if (dependOn == null || dependOn == '') {
                        dependOn = '';
                    }
                    if (maturityAge == null || maturityAge == '') {
                        maturityAge = '0';
                    }
                    $("#pgId").val(pgId);
                    $("#maturityAge").val(maturityAge);
                    $("#basicMultiplyBy").val(dependsOnMultiplyBy);

                    var basicSalary1 = $("#basicSalary").val();
                    var basicMultiplyBy1 = $("#basicMultiplyBy").val();
                    var sum = parseFloat(basicSalary1) * parseFloat(basicMultiplyBy1);
                    $("#sumAssured").val(sum);

                    if (proposalDate == '') {
                        $("#proposalDate").val("");
                    } else {
                        $("#proposalDate").val(getDate(proposalDate));
                    }

                    if ($("#pgId").val() != '') {
                        loadMaxSlNoData($("#pgId").val());
                    }

                } else {
                    $("#maturityAge").val("");
                    $("#basicMultiplyBy").val("");
                    $("#proposalDate").val("");
                    $("#pgId").val("");
                    $("#slNo").val("");
                    $("#sumAssured").val("");
                    loadSelectData("");
                }
            },
            error: function (xhr, status, error) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }

    function updateLoadData(pgId) {
        $.ajax({
            url: "/groupinsurance-ajax/getGrpAssuredSearch/" + pgId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response != '') {
                    var pgId = "";
                    var dependsOnMultiplyBy = "";
                    var dependOn = "";
                    var proposalDate = "";
                    var maturityAge = "";
                    $.each(response, function (i, l) {
                        pgId = l[0];
                        dependsOnMultiplyBy = l[1];
                        dependOn = l[2];
                        proposalDate = l[3];
                        maturityAge = l[4];
                    });
                    if (proposalDate == null || proposalDate == '') {
                        proposalDate = '';
                    }
                    if (pgId == null || pgId == '') {
                        pgId = '';
                    }
                    if (dependsOnMultiplyBy == null || dependsOnMultiplyBy == '') {
                        dependsOnMultiplyBy = '0';
                    }
                    if (dependOn == null || dependOn == '') {
                        dependOn = '';
                    }
                    if (maturityAge == null || maturityAge == '') {
                        maturityAge = '0';
                    }
                    $("#maturityAge").val(maturityAge);
                    $("#basicMultiplyBy").val(dependsOnMultiplyBy);
                    if (proposalDate == '') {
                        $("#proposalDate").val("");
                    } else {
                        $("#proposalDate").val(getDate(proposalDate));
                    }
                } else {
                    $("#maturityAge").val("");
                    $("#basicMultiplyBy").val("");
                    $("#proposalDate").val("");
                    $("#pgId").val("");
                    $("#slNo").val("");
                    $("#sumAssured").val("");
                }
            },
            error: function (xhr, status, error) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }

    function loadMaxSlNoData(pgId) {
        $.ajax({
            url: "/groupinsurance-ajax/getGrpAssuredMaxSlno/" + pgId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var getSlNo = "";
                $("#slNo").val(response.getMax);
            },
            error: function (xhr, status, error) {
                showAlertByType('Something Wrong!', "F");
            }
        });
    }

    function clrFrom() {
        $("#updatepgId").val("");
        $("#organizationId").val("-1");
        $("#proposalDate").val("");
        $("#basicMultiplyBy").val("");
        $("#slNo").val("");
        $("#pgId").val("");
        $("#employeeName").val("");
        $("#fatherName").val("");
        $("#employeeId").val("");
        $("#incrementedBasic").val("");
        $("#entryDate").val("");
        $('#designationCode').val('-1');
        $('#designationCode').selectpicker('refresh');
        //checked
        $("#status").prop('checked', true);
        $("#maturityAge").val("");
        $("#incrementSumAssured").val("");
        $("#dateOfBirth").val("");
        $("#entryAge").val("");
        $("#basicSalary").val("");
        $("#sumAssured").val("");
//error
        $("#error_designationCode").text("");
        $("#error_entryDate").text("");
        $("#error_sumAssured").text("");
        $("#error_basicSalary").text("");
        $("#error_entryAge").text("");
        $("#error_dateOfBirth").text("");
        $("#error_employeeName").text("");
        $("#error_organizationId").text("");
        $("#error_incrementSumAssured").text("");
        $("#error_incrementedBasic").text("");
        $("#error_employeeId").text("");
        $("#error_fatherName").text("");
        var categoryName = $('#categoryName');
        categoryName.empty();
        categoryName.append($('<option/>', {
            value: "-1",
            text: "--Select Category--"
        }));
        $('#organizationId').prop('disabled', false);
        $("#btnAssuredSave").html('Save' + ' ' + "<i class='fa fa-floppy-o'></i>");
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;
        return date;
    };

    function check_validDateOfBirth(enteredDate) {
        var status = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate > today) {
            status = true;
        }
        return status;
    }

    function getFormateDate(date) {
        var d = new Date(date);
        return d;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    function loadSelectData(pgId) {
        $.get("/groupinsurance-ajax/getGrpCategoryWiseData/" + pgId,
            function (data, status) {
                var categoryName = $('#categoryName');
                categoryName.empty();
                categoryName.append($('<option/>', {
                    value: "-1",
                    text: "--Select Category--"
                }));
                $.each(data, function (index, offices) {
                    categoryName.append($('<option/>', {
                        value: offices[0],
                        text: offices[1]
                    }));
                });
            });
    }

    function errorResolve() {
        $("#error_designationCode").text("");
        $("#error_entryDate").text("");
        $("#error_sumAssured").text("");
        $("#error_basicSalary").text("");
        $("#error_entryAge").text("");
        $("#error_dateOfBirth").text("");
        $("#error_employeeName").text("");
        $("#error_organizationId").text("");
        $("#error_incrementSumAssured").text("");
        $("#error_incrementedBasic").text("");
        $("#error_employeeId").text("");
        $("#error_fatherName").text("");
    }
});