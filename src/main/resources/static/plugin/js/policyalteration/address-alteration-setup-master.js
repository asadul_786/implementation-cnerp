$(document).ready(function () {

    var pathname = window.location.pathname;
    var short_path_name = pathname.split('/')[2];

    if (short_path_name === "mailing-address-alteration-info") {
        $("#mailingAddressOne").addClass('active');
    }
    else if (short_path_name === "permanent-address-alteration-info") {
        $("#permanetAddressOne").addClass('active');
    }


    $(document).on("input", "#policyNo", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyNo = $('#policyNo').val();

        if (policyNo != "" && policyNo.length < 20) {
            $.ajax({
                url: "/policy-alteration/getPGID/" + policyNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var pgID = "";
                    $.each(response, function (i, l) {
                        pgID = l[0];
                    });
                    $("#pgid").val(pgID);
                    getPartyDetails(pgID);
                    $("#err_policyNo").text("");
                },
                error: function (xhr, status, error) {
                    $("#pgid").val("");
                    $("#err_policyNo").text("Sorry! Policy NO not match!!");
                    $("#partyCd").empty();
                    $("#partyCd").append($('<option/>', {
                        value: "-1",
                        text: "---Select Client Code---"
                    }));
                    $("#partyId").val("");
                    //
                    $("#t_address").val("");
                    $("#t_division_cd").val("-1");
                    $("#t_thana_cd").val("-1");
                    $("#t_phone_no").val("");
                    $("#t_district_cd").val("-1");
                    $("#t_po_cd").val("");
                    $("#t_country_cd").val("-1");
                    //
                    $("#p_address_1").val("");
                    $("#p_divisionCD").val("-1");
                    $("#pthanaCD").val("-1");
                    $("#p_phoneNo").val("");
                    $("#p_district_cd").val("-1");
                    $("#p_poCD").val("");
                    $("#p_country_cd").val("-1");
                }
            });
        }
        else {
            if ($('#policyNo').val() == "") {
                $("#err_policyNo").text("Policy NO required!");
                $("#pgid").val("");
                $("#partyCd").empty();
                $("#partyCd").append($('<option/>', {
                    value: "-1",
                    text: "---Select Client Code---"
                }));
                $("#partyId").val("");
                //
                $("#t_address").val("");
                $("#t_division_cd").val("-1");
                $("#t_thana_cd").val("-1");
                $("#t_phone_no").val("");
                $("#t_district_cd").val("-1");
                $("#t_po_cd").val("");
                $("#t_country_cd").val("-1");
                //
                $("#p_address_1").val("");
                $("#p_divisionCD").val("-1");
                $("#pthanaCD").val("-1");
                $("#p_phoneNo").val("");
                $("#p_district_cd").val("-1");
                $("#p_poCD").val("");
                $("#p_country_cd").val("-1");
            }
            else {
                $("#err_policyNo").text("Policy NO maximum 20 characters!");
            }
        }

    });

    function getPartyDetails(getPGID) {

        $.ajax({
            url: "/policy-alteration/getPartyDetails/" + getPGID,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var party_CD = "";
                var party_NM = "";
                var party_ID = "";
                $.each(response, function (i, l) {
                    party_CD = l[0];
                    party_NM = l[1];
                    party_ID = l[2];
                });
                var authorityCd = $('#partyCd');
                authorityCd.empty();
                authorityCd.append($('<option/>', {
                    value: "-1",
                    text: "---Select Client Code---"
                }));
                authorityCd.append($('<option/>', {
                    value: party_CD,
                    text: party_NM
                }));
                $("#partyId").val(party_ID);

            },
            error: function (xhr, status, error) {
                alert("get error");
            }
        });
    }

    //

//working

    $(document).on("change", "#partyCd", function (e) {

        var partyCd = $("#partyCd option:selected").val();
        var pgid = $("#pgid").val();

        if (partyCd != "" && pgid != "") {
            getClientName(partyCd, pgid);
            getAddressDetails(partyCd, pgid);
        } else {
            alert("field empty!!!")
        }
    });


    function getAddressDetails(partyCd, pgid) {

        if (partyCd != "" && pgid != "") {

            $.ajax({
                url: "/policy-alteration/getAddresDetails/" + partyCd + "/" + pgid,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var t_address = "";
                    var t_division = "";
                    var t_thana = "";
                    var t_phone = "";
                    var t_district = "";
                    var t_post_code = "";
                    var t_country = "";
                    //
                    var p_address = "";
                    var p_division = "";
                    var p_thana = "";
                    var p_phone = "";
                    var p_district = "";
                    var p_post_code = "";
                    var p_country = "";
                    //
                    var t_divisionName = "";
                    var t_districtName = "";
                    var t_thanaName = "";
                    var p_divisionName = "";
                    var p_districtName = "";
                    var p_thanaName = "";

                    $.each(response, function (i, l) {
                        t_address = l[0];
                        t_division = l[1];
                        t_thana = l[2];
                        t_phone = l[3];
                        t_district = l[4];
                        t_post_code = l[5];
                        t_country = l[6];
                        //
                        p_address = l[7];
                        p_division = l[8];
                        p_thana = l[9];
                        p_phone = l[10];
                        p_district = l[11];
                        p_post_code = l[12];
                        p_country = l[13];
                        //
                        t_divisionName = l[14];
                        t_districtName = l[15];
                        t_thanaName = l[16];

                        p_divisionName = l[17];
                        p_districtName = l[18];
                        p_thanaName = l[19];

                    });
                    $("#t_address").val(t_address);

                    $("#t_thana_cd").val(t_thana);
                    $("#t_phone_no").val(t_phone);
                    $("#t_district_cd").val(t_district);
                    $("#t_po_cd").val(t_post_code);
                    $("#t_country_cd").val(t_country);
                    //
                    $("#p_address_1").val(p_address);
                    $("#p_divisionCD").val(p_division);
                    $("#pthanaCD").val(p_thana);
                    $("#p_phoneNo").val(p_phone);
                    $("#p_district_cd").val(p_district);
                    $("#p_poCD").val(p_post_code);


                    var t_division_cd = $('#t_division_cd');
                    t_division_cd.append($('<option/>', {
                        value: t_division,
                        text: t_divisionName
                    }));

                    var t_district_cd = $('#t_district_cd');
                    t_district_cd.append($('<option/>', {
                        value: t_district,
                        text: t_districtName
                    }));

                    var t_thana_cd = $('#t_thana_cd');
                    t_thana_cd.append($('<option/>', {
                        value: t_thana,
                        text: t_thanaName
                    }));

                    var p_divisionCD = $('#p_divisionCD');
                    p_divisionCD.append($('<option/>', {
                        value: p_division,
                        text: p_divisionName
                    }));

                    var p_district_cd = $('#p_district_cd');
                    p_district_cd.append($('<option/>', {
                        value: p_district,
                        text: p_districtName
                    }));

                    var pthanaCD = $('#pthanaCD');
                    pthanaCD.append($('<option/>', {
                        value: p_thana,
                        text: p_thanaName
                    }));

                    var t_country = $('#t_country_cd');
                    t_country.append($('<option/>', {
                        value: t_country
                    }));

                    var p_country = $('#p_country_cd');
                    p_country.append($('<option/>', {
                        value: p_country
                    }));


                },
                error: function (xhr, status, error) {
                    alert("get error");
                }
            });

        }
    }

    function getClientName(partyCd, pgid) {

        if (partyCd != "" && pgid != "") {

            $.ajax({
                url: "/policy-alteration/getClintName/" + partyCd + "/" + pgid,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var party_Name = "";
                    $.each(response, function (i, l) {
                        party_Name = l[1];
                    });
                    $("#clientName").val(party_Name);
                },
                error: function (xhr, status, error) {
                    alert("get error");
                }
            });

        }
    }

    $(document).on("change", "#p_divisionCD", function (e) {

        var divisionCD = $("#p_divisionCD option:selected").val();

        $.get("/hrm-admin/get-districtInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#clientName');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Client Name---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });
            });
    });


    $(document).on("change", "#p_district_cd", function (e) {

        var divisionCD = $("#p_district_cd option:selected").val();

        $.get("/hrm-admin/get-thanaInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#pthanaCD');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Thana---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });


    $(document).on("change", "#t_division_cd", function (e) {

        var divisionCD = $("#t_division_cd option:selected").val();

        $.get("/hrm-admin/get-districtInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#t_district_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select District---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });

    $(document).on("change", "#t_district_cd", function (e) {

        var divisionCD = $("#t_district_cd option:selected").val();

        $.get("/hrm-admin/get-thanaInfo?divisionCD=" + divisionCD,

            function (data, status) {

                var office = $('#t_thana_cd');
                office.empty();
                office.append($('<option/>', {
                    value: "-1",
                    text: "---Select Thana---"
                }));
                $.each(data, function (index, branchbank) {
                    office.append($('<option/>', {
                        value: branchbank[0],
                        text: branchbank[1]
                    }));

                });

            });
    });


    $("#btnSave_persoanl_alternation").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var alterType = $('#alterType').val();
            var policyNo = $('#policyNo').val();
            var clientName = $('#clientName').val();
            var applicationDT = $('#applicationDT').val();
            var partyCd = $('#partyCd').val();
            var appslNo = $('#appslNo').val();

            var t_address = $('#t_address').val();
            var t_division_cd = $('#t_division_cd').val();
            var t_thana_cd = $('#t_thana_cd').val();
            var t_phone_no = $('#t_phone_no').val();
            var t_district_cd = $('#t_district_cd').val();
            var t_po_cd = $('#t_po_cd').val();
            var t_country_cd = $('#t_country_cd').val();

            var p_address_1 = $('#p_address_1').val();
            var p_divisionCD = $('#p_divisionCD').val();
            var pthanaCD = $('#pthanaCD').val();
            var p_phoneNo = $('#p_phoneNo').val();
            var p_district_cd = $('#p_district_cd').val();
            var p_poCD = $('#p_poCD').val();
            var p_country_cd = $('#p_country_cd').val();
            var pgid = $('#pgid').val();
            var partyId = $('#partyId').val();

            var changeAddress = {};

            changeAddress.alterType = alterType;
            changeAddress.policyNo = policyNo;
            changeAddress.clientName = clientName;
            changeAddress.applicationDT = applicationDT;
            changeAddress.partyCd = partyCd;
            changeAddress.appslNo = appslNo;

            changeAddress.t_address = t_address;
            changeAddress.t_division_cd = t_division_cd;
            changeAddress.t_thana_cd = t_thana_cd;
            changeAddress.t_phone_no = t_phone_no;
            changeAddress.t_district_cd = t_district_cd;
            changeAddress.t_po_cd = t_po_cd;
            changeAddress.t_country_cd = t_country_cd;

            changeAddress.p_address_1 = p_address_1;
            changeAddress.p_divisionCD = p_divisionCD;
            changeAddress.pthanaCD = pthanaCD;
            changeAddress.p_phoneNo = p_phoneNo;
            changeAddress.p_district_cd = p_district_cd;
            changeAddress.p_poCD = p_poCD;
            changeAddress.p_country_cd = p_country_cd;
            changeAddress.pgid = pgid;
            changeAddress.partyId = partyId;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/policy-alteration/addAlteration_address",
                data: JSON.stringify(changeAddress),
                dataType: 'json',
                success: function (data) {
                    if (data != "") {
                        showAlert("Successfully added.");
                        clearFrom();
                    }
                },
                error: function (e) {
                    showAlert("Sorry,Something Wrong!!");
                }
            });

        }

    });


    function dataValidation() {
        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($("#alterType").val() == "-1") {
            status = false;
            $("#err_alterType").text("Empty field found!!");
            $("#alterType").focus();
        } else $("#err_alterType").text("");

        if ($("#policyNo").val() == "") {
            status = false;
            $("#err_policyNo").text("Empty field found!!");
            $("#policyNo").focus();
        } else $("#err_policyNo").text("");

        if ($("#applicationDT").val() != "") {
            if (!dateCheck.test($("#applicationDT").val())) {
                status = false;
                $("#err_applicationDT").text("Invalid Date format!!");
                $("#applicationDT").focus();
            } else $("#err_applicationDT").text("");

        } else $("#err_applicationDT").text("");


        if ($("#partyCd").val() == "-1") {
            status = false;
            $("#err_partyCd").text("Empty field found!!");
            $("#partyCd").focus();
        } else $("#err_partyCd").text("");


        if ($("#appslNo").val() != "") {
            if ($("#appslNo").val().length > 2) {
                status = false;
                $("#err_appslNo").text("Maximum 2 digit allow!!!");
                $("#appslNo").focus();

            } else if (/[^0-9]/g.test($("#appslNo").val()) == true) {
                status = false;
                $("#err_appslNo").text("Invalid Character!!!");
                $("#appslNo").focus();

            } else  $("#err_appslNo").text("");
        } else  $("#err_appslNo").text("");


        if ($("#t_phone_no").val() != "") {
            if ($("#t_phone_no").val().length != 11) {
                status = false;
                $("#err_t_phone_no").text("Invalid Contact Number!!!");
                $("#t_phone_no").focus();
            } else  $("#err_t_phone_no").text("");
        } else  $("#err_t_phone_no").text("");


        if ($("#t_po_cd").val() != "") {
            if ($("#t_po_cd").val().length > 4) {
                status = false;
                $("#err_t_po_cd").text("Invalid Zip Code!!!");
                $("#t_po_cd").focus();
            } else  $("#err_t_po_cd").text("");
        } else  $("#err_t_po_cd").text("");


        if ($("#p_phoneNo").val() != "") {
            if ($("#p_phoneNo").val().length != 11) {
                status = false;
                $("#err_p_phoneNo").text("Invalid Contact Number!!!");
                $("#p_phoneNo").focus();
            } else  $("#err_p_phoneNo").text("");
        } else  $("#err_p_phoneNo").text("");


        if ($("#p_poCD").val() != "") {
            if ($("#p_poCD").val().length > 4) {
                status = false;
                $("#err_p_poCD").text("Invalid Zip Code!!!");
                $("#p_poCD").focus();
            } else  $("#err_p_poCD").text("");
        } else  $("#err_p_poCD").text("");

        return status;
    }

    function clearFrom() {
        $('#alterType').val("-1");
        $('#policyNo').val("");
        $('#clientName').val("");
        $('#applicationDT').val("");
        $('#partyCd').val("-1");
        $('#appslNo').val("");

        $('#t_address').val("");
        $('#t_division_cd').val("-1");

        $('#t_phone_no').val("");

        $('#t_po_cd').val("");
        $('#t_country_cd').val("-1");

        $('#p_address_1').val("");
        $('#p_divisionCD').val("-1");
        $('#p_phoneNo').val("");


        $('#p_district_cd').empty();
        $("#p_district_cd").append($('<option/>', {
            value: "-1",
            text: "--Select District--"
        }));

        $('#t_district_cd').empty();
        $("#t_district_cd").append($('<option/>', {
            value: "-1",
            text: "--Select District--"
        }));

        $('#pthanaCD').empty();
        $("#pthanaCD").append($('<option/>', {
            value: "-1",
            text: "--Select Thana--"
        }));

        $('#t_thana_cd').empty();
        $("#t_thana_cd").append($('<option/>', {
            value: "-1",
            text: "--Select Thana--"
        }));

        var authorityCd = $('#partyCd');
        authorityCd.empty();
        authorityCd.append($('<option/>', {
            value: "-1",
            text: "--Select Client Code--"
        }));


        $('#p_poCD').val("");
        $('#p_country_cd').val("-1");
        $('#pgid').val("");
        $('#partyId').val("");
        //error
        $("#err_p_poCD").text("");
        $("#err_p_phoneNo").text("");
        $("#err_t_po_cd").text("");
        $("#err_t_phone_no").text("");
        $("#err_appslNo").text("");
        $("#err_partyCd").text("");
        $("#err_applicationDT").text("");
        $("#err_policyNo").text("");
        $("#err_alterType").text("");

    }

    $("#btnSave_persoanl_alternationRefresh").click(function () {
        clearFrom();
    });

});