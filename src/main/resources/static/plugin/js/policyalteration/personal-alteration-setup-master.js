$(document).ready(function () {

    var pathname = window.location.pathname;
    $("#partyCd").attr('disabled', true);
    var short_path_name = pathname.split('/')[2];

    if (short_path_name === "personal-alteration-info") {
        $("#personalinfoOne").addClass('active');
    }
    else if (short_path_name === "personalDetails-alteration-info") {
        $("#personalInfoTwo").addClass('active');
    }


    $(document).on("input", "#policyNo", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyNo = $('#policyNo').val();

        if (policyNo != "" && policyNo.length < 20) {
            $.ajax({
                url: "/policy-alteration/getPGID/" + policyNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var pgID = "";
                    $.each(response, function (i, l) {
                        pgID = l[0];
                    });
                    $("#pgid").val(pgID);
                    $("#err_policyNo").text("");
                    $("#partyCd").attr('disabled', false);
                },
                error: function (xhr, status, error) {
                    $("#pgid").val("");
                    $("#err_policyNo").text("Sorry! Policy NO not match!!");
                    $("#partyCd").attr('disabled', true);
                    refresh();
                }
            });
        }
        else {
            if ($('#policyNo').val() == "") {
                $("#err_policyNo").text("Policy NO required!");
                $("#pgid").val("");
                $("#partyCd").attr('disabled', true);
            }
            else {
                $("#err_policyNo").text("Policy NO maximum 20 characters!");
                $("#partyCd").attr('disabled', true);
                refresh();
            }
        }

    });


    $(document).on("change", "#partyCd", function (e) {

        var partyCd = $("#partyCd option:selected").val();
        var pgid = $("#pgid").val();

        if (partyCd != "" && pgid != "") {
            getClientName(partyCd, pgid);
        }

    });

    function getClientName(partyCd, pgid) {

        if (partyCd != "" && pgid != "") {

            $.get("/policy-alteration/getPartyNamePerson_alteration/" + pgid + "/" + partyCd,

                function (data, status) {

                    var office = $('#slNo');
                    office.empty();
                    office.append($('<option/>', {
                        value: "-1",
                        text: "---Select Client Name---"
                    }));
                    $.each(data, function (index, branchbank) {
                        office.append($('<option/>', {
                            value: branchbank[2],
                            text: branchbank[1] + ' [' + branchbank[0] + ']'
                        }));

                    });
                });
        }
    }

    $(document).on("change", "#slNo", function (e) {
        var s = $("#slNo option:selected").text();
        s = s.substring(0, s.indexOf('['));
        $("#slNo  option:selected").text(s);
        var SlNO = $("#slNo option:selected").val();
        var PartyCD = $("#partyCd option:selected").val();
        var PGID = $("#pgid").val();

        getpersonalDetails(PGID, PartyCD, SlNO);

    });

    function getpersonalDetails(pgId, partyCd, slNo) {

        if (partyCd != "" && pgid != "" && slNo != "") {

            $.ajax({
                url: "/policy-alteration/getPartyNamePersonDetails/" + pgId + "/" + partyCd + "/" + slNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {

                    var clientName = "";
                    var partyId = "";
                    var fatherName = "";
                    var mothername = "";
                    var agedocuCd = "";
                    var eduqualCD = "";
                    var edudocuCd = "";
                    var martialsttus = "";
                    var childrenNo = "";
                    var announalIncome = "";
                    var incomeSource = "";
                    var tin = "";
                    var identificationMark = "";

                    $.each(response, function (i, l) {
                        clientName = l[0];
                        partyId = l[1];
                        fatherName = l[2];
                        mothername = l[3];
                        agedocuCd = l[4];
                        eduqualCD = l[5];
                        edudocuCd = l[6];
                        martialsttus = l[7];
                        childrenNo = l[8];
                        announalIncome = l[9];
                        incomeSource = l[10];
                        tin = l[11];
                        identificationMark = l[12];
                    });
                    $("#clientName").val(clientName);

                    $("#partyId").val(partyId);
                    $("#fatherName").val(fatherName);
                    $("#mothername").val(mothername);
                    $("#childrenNo").val(childrenNo);
                    $("#announalIncome").val(announalIncome);
                    $("#tin").val(tin);
                    $("#identificationMark").val(identificationMark);
                    //set condition

                    $("#agedocuCd").val(agedocuCd);
                    $("#eduqualCD").val(eduqualCD);
                    $("#edudocuCd").val(edudocuCd);
                    $("#martialsttus").val(martialsttus);
                    $("#incomeSource").val(incomeSource);


                    if (agedocuCd == null) {
                        $("#agedocuCd").val("-1");
                    } else {
                        $("#agedocuCd").val(agedocuCd);
                    }

                    if (eduqualCD == null) {
                        $("#eduqualCD").val("-1");
                    } else {
                        $("#eduqualCD").val(eduqualCD);
                    }

                    if (edudocuCd == null) {
                        $("#edudocuCd").val("-1");
                    } else {
                        $("#edudocuCd").val(edudocuCd);
                    }

                    if (martialsttus == null) {
                        $("#martialsttus").val("-1");
                    } else {
                        $("#martialsttus").val(martialsttus);
                    }

                    if (incomeSource == null) {
                        $("#incomeSource").val("-1");
                    } else {
                        $("#incomeSource").val(incomeSource);
                    }

                },
                error: function (xhr, status, error) {
                    alert("get error");
                }
            });
        }
    }


    $("#btn-personal-alteration-save").click(function () {

        var flag = dataValidation();

        if (flag == true) {

            var pgid = $('#pgid').val();
            var policyNo = $('#policyNo').val();
            var alterType = $('#alterType').val();
            var slNo = $('#slNo').val();
            var applicationDT = $('#applicationDT').val();
            var partyCd = $('#partyCd').val();
            var applicationSerrialNo = $('#applicationSerrialNo').val();
            var partyId = $('#partyId').val();
            var fatherName = $('#fatherName').val();
            var mothername = $('#mothername').val();
            var agedocuCd = $('#agedocuCd').val();
            var edudocuCd = $('#edudocuCd').val();
            var announalIncome = $('#announalIncome').val();
            var tin = $('#tin').val();
            var eduqualCD = $('#eduqualCD').val();
            var martialsttus = $('#martialsttus').val();
            var incomeSource = $('#incomeSource').val();
            var identificationMark = $('#identificationMark').val();
            var childrenNo = $('#childrenNo').val();
            var alternation_personal = {};

            alternation_personal.pgid = pgid;
            alternation_personal.policyNo = policyNo;
            alternation_personal.alterType = alterType;
            alternation_personal.slNo = slNo;
            alternation_personal.applicationDT = applicationDT;
            alternation_personal.partyCd = partyCd;

            alternation_personal.applicationSerrialNo = applicationSerrialNo;
            alternation_personal.partyId = partyId;
            alternation_personal.fatherName = fatherName;
            alternation_personal.mothername = mothername;
            alternation_personal.agedocuCd = agedocuCd;
            alternation_personal.edudocuCd = edudocuCd;

            alternation_personal.announalIncome = announalIncome;
            alternation_personal.tin = tin;
            alternation_personal.eduqualCD = eduqualCD;
            alternation_personal.martialsttus = martialsttus;
            alternation_personal.incomeSource = incomeSource;
            alternation_personal.identificationMark = identificationMark;
            alternation_personal.childrenNo = childrenNo;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/policy-alteration/addAlteration_personal",
                data: JSON.stringify(alternation_personal),
                dataType: 'json',
                success: function (data) {
                    alert('decided= ' + data);
                    if (data != "") {
                        showAlert("Successfully added.");
                        clearFrom();
                    }
                },
                error: function (e) {
                    showAlert("Sorry,Something Wrong!!");
                }
            });
        }
    });

    function dataValidation() {
        var status = true;
        var dateCheck = new RegExp("[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}");

        if ($("#alterType").val() == "-1") {
            status = false;
            $("#err_alterType").text("Empty field found!!");
            $("#alterType").focus();
        } else $("#err_alterType").text("");

        if ($("#policyNo").val() == "") {
            status = false;
            $("#err_policyNo").text("Empty field found!!");
            $("#policyNo").focus();
        } else $("#err_policyNo").text("");


        if ($("#applicationDT").val() != "") {
            if (!dateCheck.test($("#applicationDT").val())) {
                status = false;
                $("#err_applicationDT").text("Invalid Date format!!");
                $("#applicationDT").focus();
            } else $("#err_applicationDT").text("");

        } else $("#err_applicationDT").text("");


        if ($("#partyCd").val() == "-1") {
            status = false;
            $("#err_partyCd").text("Empty field found!!");
            $("#partyCd").focus();
        } else $("#err_partyCd").text("");


        if ($("#applicationSerrialNo").val() != "") {
            if ($("#applicationSerrialNo").val().length > 2) {
                status = false;
                $("#err_applicationSerrialNo").text("Maximum 2 digit allow!!!");
                $("#applicationSerrialNo").focus();

            } else if (/[^0-9]/g.test($("#applicationSerrialNo").val()) == true) {
                status = false;
                $("#err_applicationSerrialNo").text("Invalid Character!!!");
                $("#applicationSerrialNo").focus();

            } else  $("#err_applicationSerrialNo").text("");
        } else  $("#err_applicationSerrialNo").text("");


        if ($("#slNo").val() == "-1") {
            status = false;
            $("#err_serialNo").text("Empty field found!!");
            $("#slNo").focus();
        } else $("#err_serialNo").text("");

        if ($("#childrenNo").val() != "") {
            if ($("#childrenNo").val().length > 2) {
                status = false;
                $("#err_childrenNo").text("Maximum 2 digit!!");
                $("#childrenNo").focus();
            } else if (/[^0-9]/g.test($("#childrenNo").val()) == true) {
                status = false;
                $("#err_childrenNo").text("Invalid Character!!!");
                $("#childrenNo").focus();
            } else $("#err_childrenNo").text("");

        } else $("#err_childrenNo").text("");


        if ($("#announalIncome").val() != "") {
            if ($("#announalIncome").val().length > 10) {
                status = false;
                $("#err_announalIncome").text("Maximum 2 digit!!");
                $("#announalIncome").focus();
            } else if (/[^0-9]/g.test($("#announalIncome").val()) == true) {
                status = false;
                $("#err_announalIncome").text("Invalid Character!!!");
                $("#announalIncome").focus();
            } else $("#err_announalIncome").text("");

        } else $("#err_announalIncome").text("");


        if ($("#tin").val() != "") {
            if ($("#tin").val().length > 12) {
                status = false;
                $("#err_tin").text("Maximum 12 digit!!");
                $("#tin").focus();
            } else $("#err_tin").text("");

        } else $("#err_tin").text("");

        return status;
    }

    function clearFrom() {
        $('#pgid').val("");
        $('#policyNo').val("");
        $('#alterType').val("-1");
        $('#slNo').val("-1");
        $('#clientName').val("");
        $('#applicationDT').val("");
        $('#partyCd').val("-1");
        $('#applicationSerrialNo').val("");
        $('#partyId').val("");
        $('#fatherName').val("");
        $('#mothername').val("");
        $('#agedocuCd').val("-1");
        $('#edudocuCd').val("-1");
        $('#announalIncome').val("");
        $('#tin').val("");
        $('#eduqualCD').val("-1");
        $('#martialsttus').val("-1");
        $('#incomeSource').val("-1");
        $('#identificationMark').val("");
        $('#childrenNo').val("");
        //error resolve
        $("#err_alterType").text("");
        $("#err_policyNo").text("");
        $("#err_applicationDT").text("");
        $("#err_partyCd").text("");
        $("#err_applicationSerrialNo").text("");
        $("#err_serialNo").text("");
        $("#err_childrenNo").text("");
        $("#err_announalIncome").text("");
        $("#err_tin").text("");
    }

    function refresh() {
        $('#pgid').val("");
        $('#alterType').val("-1");
        $('#slNo').val("-1");
        $('#clientName').val("");
        $('#applicationDT').val("");
        $('#partyCd').val("-1");
        $('#applicationSerrialNo').val("");
        $('#partyId').val("");
        $('#fatherName').val("");
        $('#mothername').val("");
        $('#agedocuCd').val("-1");
        $('#edudocuCd').val("-1");
        $('#announalIncome').val("");
        $('#tin').val("");
        $('#eduqualCD').val("-1");
        $('#martialsttus').val("-1");
        $('#incomeSource').val("-1");
        $('#identificationMark').val("");
        $('#childrenNo').val("");
        //error resolve
        $("#err_alterType").text("");
        $("#err_applicationDT").text("");
        $("#err_partyCd").text("");
        $("#err_applicationSerrialNo").text("");
        $("#err_serialNo").text("");
        $("#err_childrenNo").text("");
        $("#err_announalIncome").text("");
        $("#err_tin").text("");

        var clientName = $('#slNo');
        clientName.empty();
        clientName.append($('<option/>', {
            value: "-1",
            text: "---Select Client Name---"
        }));

    }

    $("#btn-personal-alteration-refresh").click(function () {
        clearFrom();
    });

});