$(document).ready(function () {

    $("#btnbonusSave").click(function () {
        var flag = PolicyWiseBonusDataValidation();
        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to process Policy Wise Bonus?");
        }
    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var pgId = $('#pgId').val();
                        var policyStatus = $('#policyStatus').val();
                        var purposeOf = $('#purposeOf').val();

                        var policyBonusList = {};
                        policyBonusList.pgId = pgId;
                        policyBonusList.policyStatus = policyStatus;
                        policyBonusList.purposeOf = purposeOf;

                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/servicing/savePolicyBonusProcess",
                            data: JSON.stringify(policyBonusList),
                            dataType: 'json',
                            success: function (data) {
                                var getStatus = "";
                                $.each(data, function (key, val) {
                                    getStatus = val;
                                });
                                if (getStatus == 'Success') {
                                    confirmMessage('Policy Wise Bonus Data Successfully Created!!', "S");
                                } else {
                                    confirmMessage(getStatus, "F");
                                }
                                clearform();
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", 'F');
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };


    $("#btnbtnbonusRefresh").click(function () {
        clearform();
    });

    $(document).on("input", "#policyNo", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var policyNo = $('#policyNo').val();

        if (policyNo != "" && policyNo.length < 20) {

            $.ajax({
                url: "/servicing/getcollectionAdjustment/" + policyNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    if (response == '') {
                        $("#err_policyNo").text("Sorry! Policy No not match!!");
                        $("#pgId").val("");
                        $("#policyStatus").val("");
                        $("#lastPaidDate").val("");
                        $("#btnbonusSave").attr("disabled", true);
                        $("#btnbtnbonusRefresh").attr("disabled", true);
                    } else {
                        $("#err_policyNo").text("");
                        var getpgid = "";
                        $.each(response, function (i, l) {
                            getpgid = l[2];
                        });
                        $("#err_policyNo").text("");
                        loadbonusWiseInfo(getpgid);
                    }
                },
                error: function (xhr, status, error) {
                    showAlertByType("Sorry,Something Wrong!!", "F");
                }
            });
        }
        else {
            if ($('#policyNo').val() == '') {
                $("#err_policyNo").text("");
                $("#btnbonusSave").attr("disabled", false);
                $("#btnbtnbonusRefresh").attr("disabled", false);
            }
            else {
                $("#err_policyNo").text("Policy No maximum 20 characters!");
            }
        }
    });

    function loadbonusWiseInfo(getpgid) {
        $.ajax({
            url: "/servicing/getPolicyWisePGID/" + getpgid,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response == '') {
                    $("#pgId").val("");
                    $("#policyStatus").val("");
                    $("#lastPaidDate").val("");
                } else {
                    var pgid = "";
                    var lastPaidDate = "";
                    var policyStatus = "";
                    $.each(response, function (i, l) {
                        pgid = l[0];
                        policyStatus = l[1];
                        lastPaidDate = l[2];
                    });
                    $("#pgId").val(pgid);
                    $("#policyStatus").val(policyStatus);

                    if (lastPaidDate != "" || lastPaidDate != null) {
                        $("#lastPaidDate").val(getDate(lastPaidDate));
                    }
                }
            },
            error: function (xhr, status, error) {
                showAlertByType("Sorry,Something Wrong!!", "F");
            }
        });
    }

    function PolicyWiseBonusDataValidation() {

        var status = true;

        if ($('#policyNo').val() == "") {
            status = false;
            $("#err_policyNo").text("Empty field found!!");
            $("#policyNo").focus();

        } else $("#err_policyNo").text("");

        return status;
    }

    function getDate(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = month + "/" + day + "/" + year;
        return date;
    }

    function clearform() {
        $('#policyNo').val("");
        $('#lastPaidDate').val("");
        $('#pgId').val("");
        $('#policyStatus').val("");
        $('#purposeOf').val('3');
        $("#err_policyNo").text("");
    }

    var confirmMessage = function (text, messageType) {

        if (messageType == "S") {
            $.confirm({
                title: 'Success!',
                content: text,
                type: 'green',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        } else if (messageType == "F") {
            $.confirm({
                title: 'Error!',
                content: text,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        }
    };

});
