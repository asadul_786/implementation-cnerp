/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

     var table = $("#policy_info_lst_table").DataTable({

     "processing": true,
     "language": {
     "processing": "Processing... please wait"
     },
     "pageLength": 25,
     ajax: {
         "url": "dummyCall",
         "type": "GET",
         "data": function ( d ) {
             d.policyNo = $.trim($("#policy_no").val()),
             d.assuredNm = $.trim($("#assured").val()),
             d.mobile = $.trim($("#mb").val())
     },
         "dataType": "json",
         "dataSrc": ""
     },
     "initComplete": function(settings, json) {
        $("#policy_info_lst_table_processing").css({"top": "5px", "color": "green"});
     },
     "autoWidth": true,
     "columns": [
         { "data": "policyNo" },
         { "data": "commDt" },
         { "data": "assured" },
         { "data": "nominee" },
         { "data": "dob" },
         { "data": "mobile" },
         { "data": "fatherNm" },
         { "data": "address1" },
         { "data": "address2" },
         { "data": "insPrem" }
     ]
     });

    $(document).on("click", "#search_btn", function () {
         if(validate()){
             table.ajax.url("getSearchRes").load();
         }
     });

    $(document).on("click", "#clear_btn", function () {
        $('#polInfoSearchForm').trigger("reset");
    });

    function validate() {

        if($.trim($("#policy_no").val()) == "" && $.trim($("#assured").val()) == "" && $.trim($("#mb").val()) == "" ){
            $.alert({
                title: 'Alert!',
                content: 'Please fill up at least one field to filter !!'
            });

            return;
        }

        return true;
    }

});