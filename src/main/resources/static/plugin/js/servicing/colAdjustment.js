/**
 * Created by golam nobi.
 */

$(document).ready(
    function () {

        var table_PartialCollection = $('#table_PartialCollection');
        var table_UnAdjustedCollection = $('#table_UnAdjustedCollection');

        $(document).on("input", "#policyNo", function (e) {

            e.target.value = e.target.value.replace(/[^0-9]/g, '');

            var policyNo = $('#policyNo').val();

            if (policyNo != "" && policyNo.length < 20) {

                $.ajax({
                    url: "/servicing/getcollectionAdjustment/" + policyNo,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {

                        if (response == '') {
                            $("#err_policyNo").text("Sorry! Policy No not match!!");
                            $('#table_UnAdjustedCollection tbody').empty();
                            $('#table_PartialCollection tbody').empty();
                            clrfrom();
                        } else {
                            $("#err_policyNo").text("");
                            var getPolicyNo = "";
                            var getpgid = "";
                            var getServicingOffice = "";

                            $.each(response, function (i, l) {
                                getPolicyNo = l[0];
                                getServicingOffice = l[1];
                                getpgid = l[2];
                            });

                            if (getPolicyNo != '' && getServicingOffice != '' & getpgid != '') {
                                $("#servicingOffice").val(getServicingOffice);
                                loadFromData(getPolicyNo, getServicingOffice, getpgid);
                            }
                        }
                    },
                    error: function (xhr, status, error) {
                        showAlertByType("Sorry,Something Wrong!!", "F");
                    }
                });
            }
            else {
                if ($('#policyNo').val() == '') {
                    $("#err_policyNo").text("");
                    $('#table_UnAdjustedCollection tbody').empty();
                    $('#table_PartialCollection tbody').empty();
                    clrfrom();
                }
                else {
                    $("#err_policyNo").text("Policy No maximum 20 characters!");
                }
            }

        });


        $("#btn_adjustment").click(function () {

            var flag = dataValidation();
            if (flag == true) {
                confirmEmpJoinDialog("Are you sure to save Policy Adjustment?");
            }

        });


        var confirmEmpJoinDialog = function (text) {
            $.confirm({
                title: 'Confirm!',
                content: text,
                buttons: {
                    confirm: {
                        btnClass: 'btn-info',
                        keys: ['enter'],
                        action: function () {
                            var policyNo = $('#policyNo').val();
                            var policyStatus = $('#policyStatus').val();
                            var servicingOffice = $('#servicingOffice').val();
                            var adj_date = $('#adj_date').val();
                            var total_actual_req_amt = $('#total_actual_req_amt').val();
                            var total_premium_col = $('#total_premium_col').val();
                            var total_suspense_amt = $('#total_suspense_amt').val();
                            var productCd = $('#productCd').val();
                            if (adj_date) {
                                adj_date = adj_date.split("/").reverse().join("/");
                                adj_date = getFormateDate(adj_date);
                            }
                            function getFormateDate(date) {
                                var d = new Date(date);
                                return d;
                            }

                            $(".loader").show();
                            $("#btn_adjustment").prop('disabled', true);

                            var adjust = {};
                            adjust.policyNo = policyNo;
                            adjust.policyStatus = policyStatus;
                            adjust.servicingOffice = servicingOffice;
                            adjust.adj_date = adj_date;
                            adjust.total_actual_req_amt = total_actual_req_amt;
                            adjust.total_premium_col = total_premium_col;
                            adjust.total_suspense_amt = total_suspense_amt;
                            adjust.productCd = productCd;
                            $.ajax({
                                type: 'POST',
                                contentType: 'application/json',
                                url: "/servicing/savecolAdjmentAll",
                                data: JSON.stringify(adjust),
                                dataType: 'json',
                                success: function (data) {

                                    $(".loader").fadeOut("slow");
                                    $(".loader").hide();
                                    $("#btn_adjustment").prop('disabled', false);

                                    var getStatus = "";
                                    $.each(data, function (key, val) {
                                        getStatus = val;
                                    });

                                    if (getStatus == 'Policy Adjustment has been succesfully completed.') {
                                        showAlert(getStatus);
                                    } else {
                                        showAlertByType(getStatus, 'F');
                                    }
                                },
                                error: function (e) {
                                    showAlertByType("Sorry,Something Wrong!!", 'F');
                                    $(".loader").fadeOut("slow");
                                    $(".loader").hide();
                                    $("#btn_adjustment").prop('disabled', false);
                                }
                            });

                        }

                    },
                    cancel: function () {

                    }
                }
            });
        };


        function dataValidation() {

            var status = true;

            if ($('#policyNo').val() == "") {
                status = false;
                $("#err_policyNo").text("Empty field found!!");
                $("#policyNo").focus();
            } else $("#err_policyNo").text("");

            return status;
        }


        function loadFromData(getPolicyNo, getServicingOffice, getpgid) {

            $.ajax({
                url: "/servicing/getFromData/" + getPolicyNo + "/" + getServicingOffice,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var getStatus = "";
                    var adustmentDt = "";
                    getStatus = response.p_process_st;
                    if (getStatus == "Success") {

                        if ((response.adj_date == null)) {
                            adustmentDt = "";
                        } else {
                            adustmentDt = getDate(response.adj_date);
                        }
                        $('#total_premium_amt').val(response.total_premium_amt);
                        $('#total_premium_col').val(response.total_premium_col);
                        $('#total_premium_due').val(response.total_premium_due);
                        $('#total_late_fee').val(response.total_late_fee);
                        $('#total_free_waiver').val(response.total_free_waiver);
                        $('#late_fee_collected').val(response.late_fee_collected);
                        $('#total_late_fee_due').val(response.total_late_fee_due);
                        $('#total_suspense_amt').val(response.total_suspense_amt);
                        $('#adj_date').val(adustmentDt);
                        $('#policyStatus').val(response.policyStatus);
                        $('#policyholderNm').val(response.policyholderNm);
                        $('#total_actual_req_amt').val(response.total_actual_req_amt);
                        $('#productCd').val(response.productCd);
                        $("#err_empID").text("");
                    } else {
                        showAlertByType(getStatus, "W");
                        $("#err_empID").text("");
                    }
                    loadTableUnAdjusted(getpgid);
                    loadTablePartialCollection(getpgid);
                },
                error: function (xhr, status, error) {
                    showAlertByType("Sorry,Something Wrong loadFromData!!", "F");
                }
            });
        }


        function loadTableUnAdjusted(getpgid) {

            $.ajax({
                url: "/servicing/getUnadjustedCollection/" + getpgid,
                type: 'GET',
                dataType: 'json',
                success: function (data) {

                    var u_receive_dt = "";
                    var u_due_dt = "";
                    var u_to_dt = "";

                    var tableBody = "";
                    $('#table_UnAdjustedCollection tbody').empty();

                    $.each(data, function (idx, elem) {

                        if ((elem[2] == null)) {
                            u_receive_dt = "";
                        } else {
                            u_receive_dt = getDate(elem[2]);
                        }

                        if ((elem[5] == null)) {
                            u_due_dt = "";
                        } else {
                            u_due_dt = getDate(elem[5]);
                        }

                        if ((elem[6] == null)) {
                            u_to_dt = "";
                        } else {
                            u_to_dt = getDate(elem[6]);
                        }

                        tableBody += "<tr'>";
                        tableBody += "<td>" + elem[0] + "</td>";
                        tableBody += "<td>" + u_receive_dt + "</td>";
                        tableBody += "<td>" + u_due_dt + "</td>";
                        tableBody += "<td>" + u_to_dt + "</td>";
                        tableBody += "<td>" + elem[7] + "</td>";
                        tableBody += "<td>" + elem[8] + "</td>";
                        tableBody += "<td>" + elem[13] + "</td>";
                        tableBody += "<td>" + elem[9] + "</td>";
                        tableBody += "<td>" + elem[10] + "</td>";
                        tableBody += "<tr>";
                    });
                    table_UnAdjustedCollection.append(tableBody);
                },
                error: function (xhr, status, error) {
                    showAlertByType("Sorry,Something Wrong loadTableUnAdjusted!!", "F");
                }
            });

        }


        function loadTablePartialCollection(getpgid) {

            $.ajax({
                url: "/servicing/getAccPartialCollection/" + getpgid,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    var tableBody = "";
                    var receive_dt = "";
                    var media_dt = "";
                    $('#table_PartialCollection tbody').empty();

                    $.each(data, function (idx, elem) {

                        if ((elem[1] == null)) {
                            receive_dt = "";
                        } else {
                            receive_dt = getDate(elem[1]);
                        }

                        if ((elem[5] == null)) {
                            media_dt = "";
                        } else {
                            media_dt = getDate(elem[5]);
                        }

                        tableBody += "<tr'>";
                        tableBody += "<td>" + elem[0] + "</td>";
                        tableBody += "<td>" + receive_dt + "</td>";
                        tableBody += "<td>" + elem[2] + "</td>";
                        tableBody += "<td>" + elem[3] + "</td>";
                        tableBody += "<td>" + elem[4] + "</td>";
                        tableBody += "<td>" + media_dt + "</td>";
                        tableBody += "<td>" + elem[6] + "</td>";
                        tableBody += "<td>" + elem[7] + "</td>";
                        tableBody += "<tr>";
                    });
                    table_PartialCollection.append(tableBody);
                },
                error: function (xhr, status, error) {
                    showAlertByType("Sorry,Something Wrong loadTablePartialCollection!!", "F");
                }
            });
        }

        function getDate(dateObject) {
            var d = new Date(dateObject);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = day + "/" + month + "/" + year;

            return date;
        }

        function clrfrom() {
            $('#total_premium_amt').val("");
            $('#total_premium_col').val("");
            $('#total_premium_due').val("");
            $('#total_late_fee').val("");
            $('#total_free_waiver').val("");
            $('#late_fee_collected').val("");
            $('#total_late_fee_due').val("");
            $('#total_suspense_amt').val("");
            $('#adj_date').val("");
            $('#policyStatus').val("");
            $('#policyholderNm').val("");
            //
            $('#servicingOffice').val("");
            $('#productCd').val("");
            $('#total_actual_req_amt').val("");
        }

    });
