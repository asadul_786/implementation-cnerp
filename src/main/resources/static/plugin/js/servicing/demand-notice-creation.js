$(document).ready(function () {

    $("#btnDemandSave").click(function () {
        var flag = demandDataValidation();
        if (flag == true) {
            confirmEmpJoinDialog("Are you sure to process demand notice creation?");
        }
    });

    $("#btnDemandReset").click(function () {
        clearform();
    });

    var confirmEmpJoinDialog = function (text) {
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {
                        var demandNoticDate = $('#demandNoticDate').val();
                        if (demandNoticDate) {
                            demandNoticDate = demandNoticDate.split("/").reverse().join("/");
                            demandNoticDate = getFormateDate(demandNoticDate);
                        }

                        var demandNoticList = {};
                        demandNoticList.demandNoticDate = demandNoticDate;

                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "/servicing/saveDemandNoticeCreation",
                            data: JSON.stringify(demandNoticList),
                            dataType: 'json',
                            success: function (data) {
                                var getStatus = "";
                                $.each(data, function (key, val) {
                                    getStatus = val;
                                });
                                if (getStatus == 'Success') {
                                    confirmMessage('Demand Notice Data Successfully Created!!', "S");
                                } else {
                                    confirmMessage(getStatus, "F");
                                }
                                clearform();
                            },
                            error: function (e) {
                                showAlertByType("Sorry,Something Wrong!!", 'F');
                            }
                        });

                    }

                },
                cancel: function () {

                }
            }
        });
    };

    function demandDataValidation() {

        var status = true;

        if ($('#demandNoticDate').val() == "") {
            status = false;
            $("#err_demandNoticDate").text("Empty field found!!");
            $("#demandNoticDate").focus();

        } else if (isValidDate($("#demandNoticDate").val()) == false) {
            status = false;
            $("#err_demandNoticDate").text("Invalid Date!!");
            $("#demandNoticDate").focus();

        } else $("#err_demandNoticDate").text("");

        return status;
    }

    function clearform() {
        $('#demandNoticDate').val("");
        $("#err_demandNoticDate").text("");
    }

    function getFormateDate(date) {
        var d = new Date(date);
        return d;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
        return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
    }

    var confirmMessage = function (text, messageType) {

        if (messageType == "S") {
            $.confirm({
                title: 'Success!',
                content: text,
                type: 'green',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        } else if (messageType == "F") {
            $.confirm({
                title: 'Error!',
                content: text,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

        }
    };
});