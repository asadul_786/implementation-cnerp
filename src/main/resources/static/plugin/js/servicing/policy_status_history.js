/**
 * Created by Synesis on 2/2/2020.
 */
$(document).ready(function () {

    var table = $("#policy_history_table").DataTable({

        "processing": true,
        "language": {
            "processing": "Processing... please wait"
        },
        "pageLength": 25,
        ajax: {
            "url": "dummyCall",
            "type": "GET",
            "data": function ( d ) {
                d.policyNo = $.trim($("#policy_no").val())
            },
            "dataType": "json",
            "dataSrc": ""
        },
        "initComplete": function(settings, json) {
            $("#policy_history_table_processing").css({"top": "5px", "color": "green"});
        },
        "createdRow": function( row, data, dataIndex){
            if($.trim(data.recordTp) ==  "Present"){
                $(row).addClass('greenClass');
            }
            else if($.trim(data.recordTp) ==  "History")
                $(row).addClass('redClass');
        },
        "autoWidth": true,
        "columns": [
            { "data": "commDt" },
            { "data": "recordTp" },
            { "data": "policySt" },
            { "data": "statusDes" },
            { "data": "actionDt" },
            { "data": "actionUsr" }
        ]
    });

    $(document).on("click", "#search_history", function () {

        if(validate()){
            table.ajax.url("getPolicyHistory").load();
        }

    });

    $(document).on("input", "#policy_no", function (e) {

        if($("#policy_no").val().length == 10){

            $("#err_policy_no").text("");
            tmp = $.trim($("#policy_no").val());
            $("input").val("");
            $("#policy_no").val(tmp);

            $.ajax({
                url:  "getCurPolicyInfo",
                type: 'GET',
                data: {
                    policyNo: $.trim($("#policy_no").val())
                },
                dataType: 'json',
                success: function(response) {
                    response.ex != null ? showAlertByType(response.ex, "F") :
                    $("#comm_date").val(response.commDt);
                    $("#term").val(response.term);
                    $("#assured").val(response.assured);
                    $("#last_paid_dt").val(response.lastPaidDt);
                    $("#pay_md").val(response.payMdCd);
                    $("#pro_cd").val(response.proCd);
                    $("#ins_prem").val(response.insPrem);
                    $("#serv_off").val(response.servOff);
                    $("#pol_st").val(response.policySt);
                },
                error: function(xhr, status, error) {
                    showAlert("Something went wrong !!");
                },
                complete: function () {
                }
            });
        }
        else{
            $("#err_policy_no").text("Enter a valid Policy Number !!");
            tmp = $.trim($("#policy_no").val());
            $("input").val("");
            $("#policy_no").val(tmp);
        }
    });

    $(document).on("click", "#clear_btn", function () {
        $('#policyStHistoryForm').trigger("reset");
        $("#err_policy_no").text("");
    });

    $(document).on('input', '#policy_no', function(e){
        e.target.value = e.target.value.replace(/[^0-9]/g,'');
        $.trim($("#policy_no").val()).length == 10 ? $("#err_policy_no").text("") : "";
    });

    function validate() {

        if($.trim($("#policy_no").val()) == ""){
            $("#err_policy_no").text("Required !!");
            return;
        }

        if($.trim($("#policy_no").val()).length != 10){
            $("#err_policy_no").text("Not a valid policy no !!");
            return;
        }

        return true;

    }

});