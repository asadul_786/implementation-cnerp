$(document).ready(function () {

    $('#nominee_photo_view').attr("src", "/images/no-image.jpg");
    function readURLPhoto(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#nominee_photo_view').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#err_emp_photo_id").text("");
        }
    }
    $(document).on(
        "change",
        "#getImage",
        function (e) {
            if (this.files[0].size > 1048576) {//200KB
                $("#err_emp_photo_id").text(
                    "Photo size can't be greater than 1MB!");
            } else {

                $("#hasNewImage").val(true);
                readURLPhoto(this);
            }
        });
    $("#hasNewImage").val(false);

    function getPhotoExtension(photonm){
        if (isEmptyString(photonm)){
            return "jpg";
        }
        else{
            return photonm.split('.')[1];
        }
    }


    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#getImage + img').remove();
                $('#getImage').after('<img src="'+e.target.result+'" width="150" height="150" style="margin-top: 6px;"/>');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#getImage").change(function () {

        filePreview(this);
    });

    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field!";
        valdiationMsgContent = "";
        if ($("#vehicleNo").val()=='-1') {
            valdiationMsgContent = "Vehicle is required!";
            $("#vehicleNo").focus();
            return false;
        }else if (isEmptyString($("#name").val())) {
            valdiationMsgContent = "Name is required!";
            $("#name").focus();
            return false;
        }else if (isEmptyString($("#regDate").val())) {
            valdiationMsgContent = "Registration Date is required!";
            $("#regDate").focus();
            return false;
        }else if (isEmptyString($("#regNumber").val())) {
            valdiationMsgContent = "Registration Number is required!";
            $("#regNumber").focus();
            return false;
        }else if (isEmptyString($("#chassisNum").val())) {
            valdiationMsgContent = "Chassis Number is required!";
            $("#chassisNum").focus();
            return false;
        }else if (isEmptyString($("#engineNum").val())) {
            valdiationMsgContent = "Engine Number is required!";
            $("#engineNum").focus();
            return false;
        }else if (isEmptyString($("#empId").val())) {
            valdiationMsgContent = "Employee ID is required!";
            $("#empId").focus();
            return false;
        }else if (isEmptyString($("#billempId").val())) {
            valdiationMsgContent = "Billing Employee ID is required!";
            $("#billempId").focus();
            return false;
        }
        else {
            return true;
        }

    }

    $(document).on('input', '#capacity', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });
    // $("#regDate").datepicker({
    //     maxDate: 0
    // });
    //
    // $("#regRenualDate").datepicker({
    //     minDate: 0
    // });
    //DUPLICATE REG NO CHECK
    $(document).on("input", "#regNumber", function (e) {



        var regNumber = $('#regNumber').val();


        if (regNumber != "") {
            $.ajax({
                url: "/vehicle/regNumberCheck/" + regNumber,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    if (response == 1)
                        $("#err_policy_no").text("Registration Number Already Exists");
                    else
                        $("#err_policy_no").text("");




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

    });



    $(document).on("input", "#empId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var empId = $('#empId').val();


        if (empId != "" && empId.length < 16) {
            $.ajax({
                url: "/vehicle/employee-details/" + empId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];





                    });

                    $("#responsibleEmpNo").val(value_1);

                    $("#empName").val(value_2);
                    $("#desig").val(value_3);
                    $("#office").val(value_4);




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

    });

    $(document).on("input", "#billempId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var billempId = $('#billempId').val();


        if (billempId != "" && billempId.length < 16) {
            $.ajax({
                url: "/vehicle/employee-details/" + billempId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];





                    });

                    $("#billApprovalEmpNo").val(value_1);

                    $("#billempName").val(value_2);
                    $("#billdesig").val(value_3);
                    $("#billoffice").val(value_4);




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

    });


    var table = $('#dataTable').DataTable({


    });

    $(document).on("click", "#save", function () {

        if(isValidated()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var vehVehicle = {};
                        vehVehicle.vehicleNo = $("#key").val();
                        vehVehicle.vehicleTypeNo = $("#vehicleTypeNo").val();
                        vehVehicle.name = $("#name").val();
                        vehVehicle.regNumber = $("#regNumber").val();
                        vehVehicle.conditionNo = $("#conditionNo").val();
                        vehVehicle.regRenualDate = $("#regRenualDate").val();
                        vehVehicle.regDate = $("#regDate").val();
                        vehVehicle.chassisNum = $("#chassisNum").val();
                        vehVehicle.engineNum = $("#engineNum").val();
                        vehVehicle.brandName = $("#brandName").val();
                        vehVehicle.capacity = $("#capacity").val();
                        vehVehicle.remarks = $("#remarks").val();
                        vehVehicle.modelName = $("#modelName").val();
                        vehVehicle.responsibleEmpNo = $("#responsibleEmpNo").val();
                        vehVehicle.billApprovalEmpNo = $("#billApprovalEmpNo").val();
                        vehVehicle.color = $("#color").val();
                        vehVehicle.numberPlate = $("#numberPlate").val();

                        alert("1");
                        var form = $('#SetupForm')[0];
                        var data = new FormData(form);
                        alert("2");
                        data.append("vehicleData",JSON.stringify(vehVehicle));

                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url:  "saveVehVehicle",
                            data: data,
                            processData : false,
                            contentType: false,
                            cache : false,
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }else{
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }

    });



    $(document).on("click", "#edit", function () {

        //setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#key").val(curRow.find('td:eq(0)').text());
        $("#vehicleTypeNo").val(curRow.find('td:eq(1)').text());
        $("#name").val(curRow.find('td:eq(2)').text());
        $("#regNumber").val(curRow.find('td:eq(3)').text());
        $("#regDate").val(curRow.find('td:eq(4)').text());
        $("#regRenualDate").val(curRow.find('td:eq(5)').text());
        $("#engineNum").val(curRow.find('td:eq(6)').text());
        $("#chassisNum").val(curRow.find('td:eq(7)').text());
        $("#capacity").val(curRow.find('td:eq(8)').text());
        $("#brandName").val(curRow.find('td:eq(9)').text());
        $("#modelName").val(curRow.find('td:eq(10)').text());
        $("#remarks").val(curRow.find('td:eq(11)').text());
        $("#billApprovalEmpNo").val(curRow.find('td:eq(12)').text());
        $("#responsibleEmpNo").val(curRow.find('td:eq(13)').text());
        $("#conditionNo").val(curRow.find('td:eq(14)').text());
        $("#color").val(curRow.find('td:eq(15)').text());
        $("#numberPlate").val(curRow.find('td:eq(16)').text());

        $("#photoname").val(curRow.find('td:eq(17)').text());
        alert( $("#photoname").val());
        $("#photopath").val(curRow.find('td:eq(18)').text());

        $('#imagefiled').val(curRow.find('td:eq(17)').text());
        var photo64BitString = $(this).closest('tr').find('.nominee-photo').attr('nomineePhotoByte');
        console.log(photo64BitString);



        if (isEmptyString(photo64BitString)) {
            $("#hasNewImage").val(true);
            $("#nominee_photo_view").attr("src","/images/no-image.jpg");
        }
        else {
            $("#hasNewImage").val(false);
            $("#nominee_photo_view").attr("src", 'data:image/'+getPhotoExtension(curRow.find('td:eq(17)').text())+';base64,' + photo64BitString);
        }




        var billApprovalEmpNo = $('#billApprovalEmpNo').val();

        if (billApprovalEmpNo != "") {
            $.ajax({
                url: "/vehicle/employee-gid-details/" + billApprovalEmpNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];





                    });

                    $("#billempId").val(value_1);

                    $("#billempName").val(value_2);
                    $("#billdesig").val(value_3);
                    $("#billoffice").val(value_4);




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

        var responsibleEmpNo = $('#responsibleEmpNo').val();

        if (responsibleEmpNo != "") {
            $.ajax({
                url: "/vehicle/employee-gid-details/" + responsibleEmpNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];





                    });

                    $("#empId").val(value_1);

                    $("#empName").val(value_2);
                    $("#desig").val(value_3);
                    $("#office").val(value_4);




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }


        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    $(document).on("click", "#clear_btn", function () {
        $("select").prop("disabled", false);
        $("select").val('-1');
        clrErr();
        $("#save").text('Save');
    });

    function clrErr() {

        $("#err_p_cd").text("");
        $("#err_p_status").text("");
        $("#err_appl_party").text("");

    }

    $(document).on('input', '#surDiscFactForm', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    function validate() {

        clrErr();

        if($("#p_cd").val() == -1){
            $("#err_p_cd").text("Required !!");
            return;
        }

        if($("#p_status").val() == -1){
            $("#err_p_status").text("Required !!");
            return;
        }

        if($("#appl_party").val() == -1){
            $("#err_appl_party").text("Required !!");
            return;
        }

        return true;

    }

    $('#dataTable tbody').on('click', '#delete', function () {
        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Selected record will be deleted.',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/vehicle/vehVehicleDelete/" + col1,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            if (response === true) {
                                table.row(curRow).remove().draw(false);
                                showAlert("Deleted Successfully");
                            } else {
                                showAlert("Unknown error");
                            }

                        },
                        error: function (xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

});