/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    //$('#driverNo').select2();

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#maintenanceSave").click(function () {
        saveData();
    });
    $("#maintenanceReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var maintenance = {};
        maintenance.maintenanceNo = $("#maintenanceNo").val();
        maintenance.maintenanceTypeNo = $("#maintenanceTypeNo").val();
        maintenance.vehicleNo = $("#vehicleNo").val();

        var maintenanceDateConvert = $('#maintenanceDate').val();
        if (maintenanceDateConvert) {
            maintenanceDateConvert = maintenanceDateConvert.split("/").reverse().join("/");
            maintenanceDateConvert = getDate(maintenanceDateConvert);
        }
        maintenance.maintenanceDate = maintenanceDateConvert;

        maintenance.receiptNumber = $("#receiptNumber").val();
        var receiptDateConvert = $('#receiptDate').val();
        if (receiptDateConvert) {
            receiptDateConvert = receiptDateConvert.split("/").reverse().join("/");
            receiptDateConvert = getDate(receiptDateConvert);
        }
        maintenance.receiptDate = receiptDateConvert;
        maintenance.fileNumber = $("#fileNumber").val();

        maintenance.maintenanceAmount = $("#maintenanceAmount").val();
        maintenance.serviceCenterName = $("#serviceCenterName").val();
        maintenance.serviceCenterAddress = $("#serviceCenterAddress").val();
        maintenance.maintenanceRemarks = $("#maintenanceRemarks").val();
        maintenance.driverNo = $("#driverNo").val();
        maintenance.insertUser = $('#insertUser').val();
        return maintenance;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-maintenances";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 2000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#maintenanceNo").val('');
        $("#maintenanceTypeNo").val('');
        $("#vehicleNo").val('');
        $("#maintenanceDate").val('');
        $("#maintenanceAmount").val('');
        $("#serviceCenterName").val('');
        $("#serviceCenterAddress").val('');
        $("#maintenanceRemarks").val('');
        $("#driverNo").val('');

        $("#receiptNumber").val('');
        $("#receiptDate").val('');
        $("#fileNumber").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#maintenanceTypeNo").val())) {

            valdiationMsgContent = "Maintenance Type Name is required!";
            $("#maintenanceTypeNo").focus();
            return false;
        }
        else if (isEmptyString($("#vehicleNo").val())) {

            valdiationMsgContent = "Vehicle Name is required!";
            $("#vehicleNo").focus();
            return false;
        }
        else if (isEmptyString($("#driverNo").val())) {

            valdiationMsgContent = "Driver Name is required!";
            $("#driverNo").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceAmount").val())) {

            valdiationMsgContent = "Maintenance Amount is required!";
            $("#maintenanceAmount").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceDate").val())) {

            valdiationMsgContent = "Maintenance Date is required!";
            $("#maintenanceDate").focus();
            return false;
        }

        else if (isEmptyString($("#fileNumber").val())) {
            valdiationMsgContent = "File Number is required!";
            $("#fileNumber").focus();
            return false;
        }
        else if ($("#fileNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "File Number maximum 50 characters!!";
            $("#fileNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptNumber").val())) {
            valdiationMsgContent = "Receipt Number is required!";
            $("#receiptNumber").focus();
            return false;
        }
        else if ($("#receiptNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Receipt Number maximum 50 characters!!";
            $("#receiptNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptDate").val())) {
            valdiationMsgContent = "Receipt Date is required!";
            $("#receiptDate").focus();
            return false;
        }
        else if (isEmptyString($("#serviceCenterAddress").val())) {

            valdiationMsgContent = "Service Center Address is required!";
            $("#serviceCenterAddress").focus();
            return false;
        }
        else if ($("#serviceCenterAddress").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Service Center Address maximum 50 characters!!";
            $("#serviceCenterAddress").focus();
            return false;
        }
        else if (isEmptyString($("#serviceCenterName").val())) {

            valdiationMsgContent = "Service Center Name is required!";
            $("#serviceCenterName").focus();
            return false;
        }
        else if ($("#serviceCenterName").val().length >30) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Service Center Name maximum 30 characters!!";
            $("#serviceCenterName").focus();
            return false;
        }
        else if (isEmptyString($("#maintenanceRemarks").val())) {

            valdiationMsgContent = "Maintenance Remarks is required!";
            $("#maintenanceRemarks").focus();
            return false;
        }
        else if ($("#maintenanceRemarks").val().length >100) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Maintenance Remarks maximum 100 characters!!";
            $("#maintenanceRemarks").focus();
            return false;
        }

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var maintenanceNo = curRow.find('td:eq(0)').text();
        var maintenanceTypeNo = curRow.find('td:eq(1)').text();
        var vehicleNo = curRow.find('td:eq(2)').text();
        var maintenanceDate = curRow.find('td:eq(3)').text();
        var maintenanceAmount = curRow.find('td:eq(4)').text();
        var serviceCenterName = curRow.find('td:eq(5)').text();
        var serviceCenterAddress = curRow.find('td:eq(6)').text();
        var maintenanceRemarks = curRow.find('td:eq(7)').text();
        var driverNo = curRow.find('td:eq(8)').text();

        var receiptNumber = curRow.find('td:eq(9)').text();
        var receiptDate = curRow.find('td:eq(10)').text();
        var fileNumber = curRow.find('td:eq(11)').text();

        var insertUser = curRow.find('td:eq(12)').text();

        $('#maintenanceNo').val(maintenanceNo);
        $('#maintenanceTypeNo').val(maintenanceTypeNo).trigger('change');
        $('#vehicleNo').val(vehicleNo).trigger('change');
        $('#maintenanceDate').val(maintenanceDate);
        $('#maintenanceAmount').val(maintenanceAmount);
        $('#serviceCenterName').val(serviceCenterName);
        $('#serviceCenterAddress').val(serviceCenterAddress);
        $('#maintenanceRemarks').val(maintenanceRemarks);
        $('#driverNo').val(driverNo).trigger('change');

        $('#receiptNumber').val(receiptNumber);
        $('#receiptDate').val(receiptDate);
        $('#fileNumber').val(fileNumber);

        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    $("#vehicleNo").on("change", function () {

        var vehicleNo = $('#vehicleNo').val();
        if (vehicleNo != "") {
            $.ajax({

                url: "/vehicle-ajax/getDrivers/" + vehicleNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {

                    var driverNo = "";


                    $.each(response, function (i, l) {

                        driverNo = l[0];

                    });

                    $('#driverNo').val(driverNo);
                    //$('#driverNo').val('819');
                    $('#driverNo').selectpicker('refresh');

                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {

                    $('#driverNo').val("");


                    $("#error_UDForApproval").text("Sorry! maintenance Id not match!!");
                }
            });
        }
        else {

            if ($('#vehicleNo').val() == "") {

                $('#vehicleNo').val("");
                $('#driverNo').val("");


            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End two------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
