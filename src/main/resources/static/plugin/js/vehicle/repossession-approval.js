/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();

    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#repossessionApprovalSave").click(function () {
        saveData();
    });
    $("#repossessionApprovalReferesh").click(function () {
        clearForm();
    });

    function getFormData() {

        var repossessionApproval = {};
        repossessionApproval.purchaseRepossessionNo = $("#purchaseRepossessionNo").val();
        repossessionApproval.approvedBy = $("#approvedBy").val();
        repossessionApproval.isApproved = $("#isApproved").val();
        repossessionApproval.approvedNote = $("#approvedNote").val();

        var approvedOnConvert = $('#approvedOn').val();
        if (approvedOnConvert) {
            approvedOnConvert = approvedOnConvert.split("/").reverse().join("/");
            approvedOnConvert = getDate(approvedOnConvert);
        }
        repossessionApproval.repossessionDate = approvedOnConvert;

        repossessionApproval.receiptNumber = $("#receiptNumber").val();
        var receiptDateConvert = $('#receiptDate').val();
        if (receiptDateConvert) {
            receiptDateConvert = receiptDateConvert.split("/").reverse().join("/");
            receiptDateConvert = getDate(receiptDateConvert);
        }
        repossessionApproval.receiptDate = receiptDateConvert;
        repossessionApproval.fileNumber = $("#fileNumber").val();


        repossessionApproval.insertUser = $('#insertUser').val();
        var insertDateConvert = $('#insertDate').val();
        if (insertDateConvert) {
            insertDateConvert = insertDateConvert.split("/").reverse().join("/");
            insertDateConvert = getDate(insertDateConvert);
        }
        repossessionApproval.insertDate = insertDateConvert;

        return repossessionApproval;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-repossession-approval-post";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    showAlertByType("Successfully updated.", "F");
                    clearForm();
                    setInterval(function(){location.reload(true); }, 1500);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {

        $("#employeeId").val('');
        $("#repossessionEmpNo").val('');
        $("#employeeName").val('');
        $("#employeeDesignation").val('');
        $("#employeeOffice").val('');
        $("#officeNo").val('');
        $("#repossessionDate").val('');
        $("#repossessionDetails").val('');

        $("#purchaseRepossessionNo").val('');
        $("#isApproved").val('');
        $("#approvedNote").val('');
        $("#approvedOn").val('');

        $("#approvedBy").val("");
        $("#employeeNameForApproval").val("");
        $("#employeeDesignationForApproval").val("");
        $("#employeeOfficeForApproval").val("");
        $("#officeNoForApproval").val("");

        $("#receiptNumber").val('');
        $("#receiptDate").val('');
        $("#fileNumber").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#employeeId").val())) {

            valdiationMsgContent = "Employee Id is required!";
            $("#employeeId").focus();
            return false;
        }

        else if ($("#employeeId").val().length >8) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Employee Id maximum 8 characters!!";
            $("#employeeId").focus();
            return false;
        }
        else if (isEmptyString($("#isApproved").val())) {

            valdiationMsgContent = "Approved Status is required!";
            $("#isApproved").focus();
            return false;
        }

        else if (isEmptyString($("#receiptNumber").val())) {
            valdiationMsgContent = "Receipt Number is required!";
            $("#receiptNumber").focus();
            return false;
        }
        else if ($("#receiptNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Receipt Number maximum 50 characters!!";
            $("#receiptNumber").focus();
            return false;
        }

        else if (isEmptyString($("#approvedOn").val())) {

            valdiationMsgContent = "Approved date is required!";
            $("#approvedOn").focus();
            return false;
        }
        else if (isEmptyString($("#receiptDate").val())) {
            valdiationMsgContent = "Receipt Date is required!";
            $("#receiptDate").focus();
            return false;
        }
        else if (isEmptyString($("#approvedNote").val())) {

            valdiationMsgContent = "Approved Note is required!";
            $("#approvedNote").focus();
            return false;
        }
        else if ($("#approvedNote").val().length >300) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Approved Note maximum 8 characters!!";
            $("#approvedNote").focus();
            return false;
        }
        else if (isEmptyString($("#fileNumber").val())) {
            valdiationMsgContent = "File Number is required!";
            $("#fileNumber").focus();
            return false;
        }
        else if ($("#fileNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "File Number maximum 50 characters!!";
            $("#fileNumber").focus();
            return false;
        }
        else if (isEmptyString($("#employeeIdForApproval").val())) {

            valdiationMsgContent = "Approval Employee Id is required!";
            $("#employeeIdForApproval").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*var repossessionApprovalTable = $('#tbl-repossessionApproval-lst').DataTable({

        data: [],
        "columns": [
            {"data": "isApproved"},
            {"data": "approvedBy"},
            {"data": "approvedOn"},
            {"data": "approvedNote"},
            {"data": null, defaultContent: ''}

        ],
        'columnDefs': [
             {
                'targets': 4,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round repossessionApproval-edit"   encId="' + data.purchaseRepossessionNo + '"> <i class="fa fa-edit" aria-hidden="true" style="font-size: large"></i></a>';
                }
            }
        ],
        'order': [[1, 'asc']]
    });
    function loadDatableData() {

        var urlBuilder = "/vehicle-ajax/repossession-data-list";

        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (result) {
            repossessionApprovalTable.clear().draw();
            repossessionApprovalTable.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }*/
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/

    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var purchaseRepossessionNo = curRow.find('td:eq(0)').text();
        var employeeId = curRow.find('td:eq(9)').text();


        $('#purchaseRepossessionNo').val(purchaseRepossessionNo);
        $('#employeeId').val(employeeId);

        $.ajax({
            url: "/vehicle-ajax/getEmpolyeeDetailsForRepossessionApproval/" + employeeId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var repossessionEmpNo = "";
                var employeeName = "";
                var employeeDesignation = "";
                var employeeOffice = "";
                var officeNo = "";
                var repossessionDate = "";
                var repossessionDetails = "";
                var purchaseRepossessionNo = "";
                var insertUser = "";
                var insertDate = "";

                $.each(response, function (i, l) {
                    repossessionEmpNo = l[0];
                    employeeName = l[1];
                    employeeDesignation = l[2];
                    employeeOffice = l[3];
                    officeNo = l[4];
                    repossessionDate = l[5];
                    repossessionDetails = l[6];
                    purchaseRepossessionNo = l[7];
                    insertUser = l[8];
                    insertDate = l[9];

                });

                $("#repossessionEmpNo").val(repossessionEmpNo);
                $("#employeeName").val(employeeName);
                $("#employeeDesignation").val(employeeDesignation);
                $("#employeeOffice").val(employeeOffice);
                $("#officeNo").val(officeNo);
                var repossessionDate = getDateShow(repossessionDate);
                $('#repossessionDate').val(repossessionDate);

                $("#repossessionDetails").val(repossessionDetails);
                $("#purchaseRepossessionNo").val(purchaseRepossessionNo);
                $("#insertUser").val(insertUser);
                var insertDate = getDateShow(insertDate);
                $('#insertDate').val(insertDate);

                $("#error_UD").text("");

            },
            error: function (xhr, status, error) {
                $("#repossessionEmpNo").val("");
                $("#employeeName").val("");
                $("#employeeDesignation").val("");
                $("#employeeOffice").val("");
                $("#officeNo").val("");
                $("#repossessionDate").val("");
                $("#repossessionDetails").val("");
                $("#purchaseRepossessionNo").val("");
                $("#insertUser").val("");
                $("#insertDate").val("");

                $("#error_UD").text("Sorry! Employee Id not match!!");
            }
        });

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#employeeId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeId = $('#employeeId').val();


        if (employeeId != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetailsForRepossessionApproval/" + employeeId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var repossessionEmpNo = "";
                    var employeeName = "";
                    var employeeDesignation = "";
                    var employeeOffice = "";
                    var officeNo = "";
                    var repossessionDate = "";
                    var repossessionDetails = "";
                    var purchaseRepossessionNo = "";
                    var insertUser = "";
                    var insertDate = "";

                    $.each(response, function (i, l) {
                        repossessionEmpNo = l[0];
                        employeeName = l[1];
                        employeeDesignation = l[2];
                        employeeOffice = l[3];
                        officeNo = l[4];
                        repossessionDate = l[5];
                        repossessionDetails = l[6];
                        purchaseRepossessionNo = l[7];
                        insertUser = l[8];
                        insertDate = l[9];

                    });

                    $("#repossessionEmpNo").val(repossessionEmpNo);
                    $("#employeeName").val(employeeName);
                    $("#employeeDesignation").val(employeeDesignation);
                    $("#employeeOffice").val(employeeOffice);
                    $("#officeNo").val(officeNo);
                    var repossessionDate = getDateShow(repossessionDate);
                    $('#repossessionDate').val(repossessionDate);

                    $("#repossessionDetails").val(repossessionDetails);
                    $("#purchaseRepossessionNo").val(purchaseRepossessionNo);
                    $("#insertUser").val(insertUser);
                    var insertDate = getDateShow(insertDate);
                    $('#insertDate').val(insertDate);

                    $("#error_UD").text("");

                },
                error: function (xhr, status, error) {
                    $("#repossessionEmpNo").val("");
                    $("#employeeName").val("");
                    $("#employeeDesignation").val("");
                    $("#employeeOffice").val("");
                    $("#officeNo").val("");
                    $("#repossessionDate").val("");
                    $("#repossessionDetails").val("");
                    $("#purchaseRepossessionNo").val("");
                    $("#insertUser").val("");
                    $("#insertDate").val("");

                    $("#error_UD").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeId').val() == "") {
                $("#error_UD").text("Employee Id required!");
                $("#employeeId").val("");
                $("#repossessionEmpNo").val("");
                $("#employeeName").val("");
                $("#employeeDesignation").val("");
                $("#employeeOffice").val("");
                $("#officeNo").val("");
                $("#repossessionDate").val("");
                $("#repossessionDetails").val("");
                $("#purchaseRepossessionNo").val("");
                $("#insertUser").val("");
                $("#insertDate").val("");


            }
            else {
                $("#error_UD").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#employeeIdForApproval", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeIdForApproval = $('#employeeIdForApproval').val();


        if (employeeIdForApproval != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeIdForApproval,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var approvedBy = "";
                    var employeeNameForApproval = "";
                    var employeeDesignationForApproval = "";
                    var employeeOfficeForApproval = "";
                    var officeNoForApproval = "";


                    $.each(response, function (i, l) {
                        approvedBy = l[0];
                        employeeNameForApproval = l[1];
                        employeeDesignationForApproval = l[2];
                        employeeOfficeForApproval = l[3];
                        officeNoForApproval = l[4];

                    });

                    $("#approvedBy").val(approvedBy);
                    $("#employeeNameForApproval").val(employeeNameForApproval);
                    $("#employeeDesignationForApproval").val(employeeDesignationForApproval);
                    $("#employeeOfficeForApproval").val(employeeOfficeForApproval);
                    $("#officeNoForApproval").val(officeNoForApproval);

                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $("#approvedBy").val("");
                    $("#employeeNameForApproval").val("");
                    $("#employeeDesignationForApproval").val("");
                    $("#employeeOfficeForApproval").val("");
                    $("#officeNoForApproval").val("");


                    $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeIdForApproval').val() == "") {
                $("#approvedBy").val("");
                $("#employeeNameForApproval").val("");
                $("#employeeDesignationForApproval").val("");
                $("#employeeOfficeForApproval").val("");
                $("#officeNoForApproval").val("");


            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }

    function getDateShow(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;

        return date;
    };

});
