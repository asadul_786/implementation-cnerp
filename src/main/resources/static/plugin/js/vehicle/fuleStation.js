/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#fuelStationSave").click(function () {
        saveData();
    });
    $("#fuelStationReferesh").click(function () {
        clearForm();
    });

    function getFormData() {
        var fuelStation = {};
        fuelStation.fuelStationNo = $("#fuelStationNo").val();
        fuelStation.fuelStationName = $("#fuelStationName").val();
        fuelStation.fuelStationCode = $("#fuelStationCode").val();
        fuelStation.fuelStationAddress = $("#fuelStationAddress").val();
        fuelStation.insertUser = $('#insertUser').val();
        return fuelStation;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-fuel-stations";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 2000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#fuelStationName").val('');
        $("#fuelStationCode").val('');
        $("#fuelStationAddress").val('');
        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#fuelStationName").val())) {

            valdiationMsgContent = "Fuel Station Name Is Required!";
            $("#fuelStationName").focus();
            return false;
        }
        else if ($("#fuelStationName").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Fuel Station Name maximum 50 characters!!";
            $("#fuelStationName").focus();
            return false;
        }
        else if (isEmptyString($("#fuelStationCode").val())) {

            valdiationMsgContent = "Fuel Station Code Is Required!";
            $("#fuelStationCode").focus();
            return false;
        }
        else if ($("#fuelStationCode").val().length >10) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Fuel Station Code maximum 10 characters!!";
            $("#fuelStationCode").focus();
            return false;
        }
        else if (isEmptyString($("#fuelStationAddress").val())) {

            valdiationMsgContent = "Fuel Station Address Is Required!";
            $("#fuelStationAddress").focus();
            return false;
        }
        else if ($("#fuelStationAddress").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Fuel station address maximum 50 characters!!";
            $("#fuelStationAddress").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var fuelStationNo = curRow.find('td:eq(0)').text();
        var fuelStationName = curRow.find('td:eq(1)').text();
        var fuelStationCode = curRow.find('td:eq(2)').text();
        var fuelStationAddress = curRow.find('td:eq(3)').text();
        var insertUser = curRow.find('td:eq(4)').text();


        $('#fuelStationNo').val(fuelStationNo);
        $('#fuelStationName').val(fuelStationName);
        $('#fuelStationCode').val(fuelStationCode);
        $('#fuelStationAddress').val(fuelStationAddress);
        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
