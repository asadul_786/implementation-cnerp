/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#driverAttendancesSave").click(function () {
        saveData();
    });
    $("#driverAttendancesReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var driverAttendances = {};
        driverAttendances.attendancesNo = $("#attendancesNo").val();
        driverAttendances.driverNo = $("#driverNo").val();
        driverAttendances.billApprovalEmpNo = $("#billApprovalEmpNo").val();
        driverAttendances.overTimeHour = $("#overTimeHour").val();
        driverAttendances.overTimeAllowance = $("#overTimeAllowance").val();
        driverAttendances.otherAllowance = $("#otherAllowance").val();
        driverAttendances.remarks = $("#remarks").val();
        var attendancesDateConvert = $('#attendancesDate').val();
        if (attendancesDateConvert) {
            attendancesDateConvert = attendancesDateConvert.split("/").reverse().join("/");
            attendancesDateConvert = getDate(attendancesDateConvert);
        }
        driverAttendances.attendancesDate = attendancesDateConvert;
        driverAttendances.insertUser = $('#insertUser').val();


        return driverAttendances;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-driver-attendances";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 2000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#attendancesNo").val('');
        $("#driverNo").val('');
        $("#attendancesDate").val('');
        $("#overTimeHour").val('');
        $("#overTimeAllowance").val('');
        $("#otherAllowance").val('');
        $("#remarks").val('');
        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#driverNo").val())) {

            valdiationMsgContent = "Driver name is required!";
            $("#driverNo").focus();
            return false;
        }
        else if (isEmptyString($("#attendancesDate").val())) {

            valdiationMsgContent = "Attendances Date is required!";
            $("#attendancesDate").focus();
            return false;
        }
        else if (isEmptyString($("#overTimeHour").val())) {

            valdiationMsgContent = "Over Time Hour is required!";
            $("#overTimeHour").focus();
            return false;
        }
        else if (isEmptyString($("#overTimeAllowance").val())) {

            valdiationMsgContent = "Over Time Allowance is required!";
            $("#overTimeAllowance").focus();
            return false;
        }
        else if (isEmptyString($("#otherAllowance").val())) {

            valdiationMsgContent = "Other Allowance is required!";
            $("#otherAllowance").focus();
            return false;
        }
        else if (isEmptyString($("#remarks").val())) {

            valdiationMsgContent = "Remarks is required!";
            $("#remarks").focus();
            return false;
        }
        else if ($("#remarks").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Remarks maximum 50 characters!!";
            $("#remarks").focus();
            return false;
        }
        else if (isEmptyString($("#employeeIdForApproval").val())) {

            valdiationMsgContent = "Approval Employee Id is required!";
            $("#employeeIdForApproval").focus();
            return false;
        }
        else if (isEmptyString($("#billApprovalEmpNo").val())) {

            valdiationMsgContent = "Approval Employee Id Not Valid!!!!";
            $("#employeeIdForApproval").focus();
            return false;
        }

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var attendancesNo = curRow.find('td:eq(0)').text();
        var attendancesDate = curRow.find('td:eq(1)').text();
        var overTimeHour = curRow.find('td:eq(2)').text();
        var overTimeAllowance = curRow.find('td:eq(3)').text();
        var otherAllowance = curRow.find('td:eq(4)').text();
        var remarks = curRow.find('td:eq(5)').text();
        //var billApprovalEmpNo = curRow.find('td:eq(6)').text();
        var driverNo = curRow.find('td:eq(7)').text();
        var insertUser = curRow.find('td:eq(8)').text();
        var employeeIdForApproval = curRow.find('td:eq(12)').text();


        $('#attendancesNo').val(attendancesNo);
        $('#attendancesDate').val(attendancesDate);
        $('#overTimeHour').val(overTimeHour);
        $('#overTimeAllowance').val(overTimeAllowance);
        $('#otherAllowance').val(otherAllowance);
        $('#remarks').val(remarks);
        //$('#billApprovalEmpNo').val(billApprovalEmpNo);
        $('#driverNo').val(driverNo).trigger('change');
        $('#insertUser').val(insertUser);
        $('#employeeIdForApproval').val(employeeIdForApproval);

        $.ajax({
            url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeIdForApproval,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var billApprovalEmpNo = "";
                var employeeNameForApproval = "";
                var employeeDesignationForApproval = "";
                var employeeOfficeForApproval = "";
                var officeNoForApproval = "";


                $.each(response, function (i, l) {
                    billApprovalEmpNo = l[0];
                    employeeNameForApproval = l[1];
                    employeeDesignationForApproval = l[2];
                    employeeOfficeForApproval = l[3];
                    officeNoForApproval = l[4];

                });

                $("#billApprovalEmpNo").val(billApprovalEmpNo);
                $("#employeeNameForApproval").val(employeeNameForApproval);
                $("#employeeDesignationForApproval").val(employeeDesignationForApproval);
                $("#employeeOfficeForApproval").val(employeeOfficeForApproval);
                $("#officeNoForApproval").val(officeNoForApproval);

                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $("#billApprovalEmpNo").val("");
                $("#employeeNameForApproval").val("");
                $("#employeeDesignationForApproval").val("");
                $("#employeeOfficeForApproval").val("");
                $("#officeNoForApproval").val("");


                $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
            }
        });
        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#employeeIdForApproval", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeIdForApproval = $('#employeeIdForApproval').val();


        if (employeeIdForApproval != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeIdForApproval,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var billApprovalEmpNo = "";
                    var employeeNameForApproval = "";
                    var employeeDesignationForApproval = "";
                    var employeeOfficeForApproval = "";
                    var officeNoForApproval = "";


                    $.each(response, function (i, l) {
                        billApprovalEmpNo = l[0];
                        employeeNameForApproval = l[1];
                        employeeDesignationForApproval = l[2];
                        employeeOfficeForApproval = l[3];
                        officeNoForApproval = l[4];

                    });

                    $("#billApprovalEmpNo").val(billApprovalEmpNo);
                    $("#employeeNameForApproval").val(employeeNameForApproval);
                    $("#employeeDesignationForApproval").val(employeeDesignationForApproval);
                    $("#employeeOfficeForApproval").val(employeeOfficeForApproval);
                    $("#officeNoForApproval").val(officeNoForApproval);

                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $("#billApprovalEmpNo").val("");
                    $("#employeeNameForApproval").val("");
                    $("#employeeDesignationForApproval").val("");
                    $("#employeeOfficeForApproval").val("");
                    $("#officeNoForApproval").val("");


                    $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeIdForApproval').val() == "") {
                $("#billApprovalEmpNo").val("");
                $("#employeeNameForApproval").val("");
                $("#employeeDesignationForApproval").val("");
                $("#employeeOfficeForApproval").val("");
                $("#officeNoForApproval").val("");


            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
