$(document).ready(function () {

    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field!";
        valdiationMsgContent = "";
        if (isEmptyString($("#empId").val())) {
            valdiationMsgContent = "Employee ID is required!";
            $("#empId").focus();
            return false;
        }else if ($("#employeeRouteNo").val()=='-1') {

            valdiationMsgContent = "Route is required!";
            $("#employeeRouteNo").focus();
            return false;
        }else if ($("#shuttleYear").val()=='-1') {
            valdiationMsgContent = "Shuttle Year is required!";
            $("#shuttleYear").focus();
            return false;
        }else if ($("#shuttleMonth").val()=='-1') {
            valdiationMsgContent = "Shuttle Month is required!";
            $("#shuttleMonth").focus();
            return false;
        }else if (isEmptyString($("#shuttleDate").val())) {
            valdiationMsgContent = "Shuttle Date is required!";
            $("#shuttleDate").focus();
            return false;
        }else if (isEmptyString($("#shuttleAmount").val())) {
            valdiationMsgContent = "Shuttle Date is required!";
            $("#shuttleAmount").focus();
            return false;
        }
        else {
            return true;
        }

    }


    $(document).on('input', '#shuttleAmount', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });
    $(document).on('input', '#shuttleYear', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });
    // $("#regDate").datepicker({
    //     maxDate: 0
    // });
    //
    // $("#regRenualDate").datepicker({
    //     minDate: 0
    // });





    $(document).on("input", "#empId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var empId = $('#empId').val();


        if (empId != "" && empId.length < 16) {
            $.ajax({
                url: "/vehicle/employee-details/" + empId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];





                    });

                    $("#empGid").val(value_1);

                    $("#empName").val(value_2);
                    $("#desig").val(value_3);
                    $("#office").val(value_4);




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

    });




    var table = $('#dataTable').DataTable({


    });

    $(document).on("click", "#save", function () {

        if(isValidated()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var vehVehicle = {};
                        vehVehicle.shuttleServiceNo = $("#key").val();
                        vehVehicle.employeeNo = $("#empGid").val();
                        vehVehicle.employeeRouteNo = $("#employeeRouteNo").val();
                        vehVehicle.shuttleYear = $("#shuttleYear").val();
                        vehVehicle.shuttleMonth = $("#shuttleMonth").val();
                        vehVehicle.shuttleDate = $("#shuttleDate").val();
                        vehVehicle.shuttleAmount = $("#shuttleAmount").val();
                        vehVehicle.remarks = $("#remarks").val();
                        vehVehicle.receiptNumber = $("#receiptNumber").val();
                        vehVehicle.receiptDate = $("#receiptDate").val();
                        vehVehicle.fileNumber = $("#fileNumber").val();




                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveShuttleService",
                            type: 'POST',
                            data: JSON.stringify(vehVehicle),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }else{
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    });



    $(document).on("click", "#edit", function () {

        //setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#key").val(curRow.find('td:eq(0)').text());
        $("#empGid").val(curRow.find('td:eq(1)').text());
        $("#employeeRouteNo").val(curRow.find('td:eq(2)').text());
        $("#shuttleYear").val(curRow.find('td:eq(3)').text());
        $("#shuttleMonth").val(curRow.find('td:eq(4)').text());
        $("#shuttleDate").val(curRow.find('td:eq(5)').text());
        $("#shuttleAmount").val(curRow.find('td:eq(6)').text());
        $("#remarks").val(curRow.find('td:eq(7)').text());
        $("#receiptNumber").val(curRow.find('td:eq(8)').text());
        $("#receiptDate").val(curRow.find('td:eq(9)').text());
        $("#fileNumber").val(curRow.find('td:eq(10)').text());


        var empGid = $('#empGid').val();


        if (empGid != "") {
            $.ajax({
                url: "/vehicle/employee-gid-details/" + empGid,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];





                    });

                    $("#empId").val(value_1);

                    $("#empName").val(value_2);
                    $("#desig").val(value_3);
                    $("#office").val(value_4);




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }



        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    $(document).on("click", "#clear_btn", function () {
        $("select").prop("disabled", false);
        $("select").val('-1');
        clrErr();
        $("#save").text('Save');
    });

    function clrErr() {

        $("#err_p_cd").text("");
        $("#err_p_status").text("");
        $("#err_appl_party").text("");

    }


    function validate() {

        clrErr();


        return true;

    }

    // $('#dataTable tbody').on('click', '#delete', function () {
    //     var curRow = $(this).closest('tr');
    //     var col1 = curRow.find('td:eq(0)').text();
    //
    //     $.confirm({
    //         title: 'Confirm',
    //         content: 'Selected record will be deleted.',
    //         buttons: {
    //             ok: function () {
    //                 $.ajax({
    //                     contentType: 'application/json',
    //                     url: "/vehicle/vehVehicleDelete/" + col1,
    //                     type: 'POST',
    //                     //async: false,
    //                     //data: JSON.stringify(answerDto),
    //                     dataType: 'json',
    //                     success: function (response) {
    //
    //                         if (response === true) {
    //                             table.row(curRow).remove().draw(false);
    //                             showAlert("Deleted Successfully");
    //                         } else {
    //                             showAlert("Unknown error");
    //                         }
    //
    //                     },
    //                     error: function (xhr, status, error) {
    //                         showAlert("Unknown error");
    //                     }
    //                 });
    //             },
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    //
    // });

});