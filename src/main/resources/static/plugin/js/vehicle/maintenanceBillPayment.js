/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#maintenanceBillPaymentSave").click(function () {
        saveData();
    });
    $("#maintenanceBillPaymentReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var maintenanceBillPayment = {};
        maintenanceBillPayment.maintenanceBillPaymentNo = $("#maintenanceBillPaymentNo").val();
        maintenanceBillPayment.maintenanceNo = $("#maintenanceNo").val();
        maintenanceBillPayment.approvalEmpNo = $("#approvalEmpNo").val();
        maintenanceBillPayment.stationNo = $("#stationNo").val();
        maintenanceBillPayment.billingYear = $("#billingYear").val();
        maintenanceBillPayment.billingMonth = $("#billingMonth").val();
        maintenanceBillPayment.billingAmount = $("#billingAmount").val();
        maintenanceBillPayment.remarks = $("#remarks").val();
        var billingDateConvert = $('#billingDate').val();
        if (billingDateConvert) {
            billingDateConvert = billingDateConvert.split("/").reverse().join("/");
            billingDateConvert = getDate(billingDateConvert);
        }
        maintenanceBillPayment.billingDate = billingDateConvert;

        maintenanceBillPayment.receiptNumber = $("#receiptNumber").val();
        var receiptDateConvert = $('#receiptDate').val();
        if (receiptDateConvert) {
            receiptDateConvert = receiptDateConvert.split("/").reverse().join("/");
            receiptDateConvert = getDate(receiptDateConvert);
        }
        maintenanceBillPayment.receiptDate = receiptDateConvert;
        maintenanceBillPayment.fileNumber = $("#fileNumber").val();

        maintenanceBillPayment.insertUser = $('#insertUser').val();

        return maintenanceBillPayment;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-maintenance-bill-payment";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 1500);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#maintenanceBillPaymentNo").val('');
        $("#employeeIdForApproval").val('');
        $("#maintenanceNo").val('');
        $("#stationNo").val('');
        $("#billingYear").val('');
        $("#billingMonth").val('');
        $("#billingDate").val('');
        $("#billingAmount").val('');
        $("#remarks").val('');
        $("#approvalEmpNo").val('');

        $('#maintenanceNo').val('');
        $('#maintenanceTypeNo').val('');
        $('#vehicleNo').val('');
        $('#maintenanceDate').val('');
        $('#maintenanceAmount').val('');
        $('#serviceCenterName').val('');
        $('#serviceCenterAddress').val('');
        $('#maintenanceRemarks').val('');
        $('#driverNo').val('');

        $("#receiptNumber").val('');
        $("#receiptDate").val('');
        $("#fileNumber").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#maintenanceNo").val())) {

            valdiationMsgContent = "maintenance Id is required!";
            $("#maintenanceNo").focus();
            return false;
        }
        else if (isEmptyString($("#billingMonth").val())) {

            valdiationMsgContent = "Billing Month Id is required!";
            $("#billingMonth").focus();
            return false;
        }
        else if (isEmptyString($("#receiptNumber").val())) {
            valdiationMsgContent = "Receipt Number is required!";
            $("#receiptNumber").focus();
            return false;
        }
        else if ($("#receiptNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Receipt Number maximum 50 characters!!";
            $("#receiptNumber").focus();
            return false;
        }

        else if (isEmptyString($("#billingYear").val())) {

            valdiationMsgContent = "Billing Year is required!";
            $("#billingYear").focus();
            return false;
        }
        else if (isEmptyString($("#receiptDate").val())) {
            valdiationMsgContent = "Receipt Date is required!";
            $("#receiptDate").focus();
            return false;
        }
        else if (isEmptyString($("#billingDate").val())) {

            valdiationMsgContent = "Billing Date is required!";
            $("#billingDate").focus();
            return false;
        }
        else if (isEmptyString($("#fileNumber").val())) {
            valdiationMsgContent = "File Number is required!";
            $("#fileNumber").focus();
            return false;
        }
        else if ($("#fileNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "File Number maximum 50 characters!!";
            $("#fileNumber").focus();
            return false;
        }

        else if (isEmptyString($("#billingAmount").val())) {

            valdiationMsgContent = "Billing Amount is required!";
            $("#billingAmount").focus();
            return false;
        }
        else if (isEmptyString($("#remarks").val())) {

            valdiationMsgContent = "Remarks is required!";
            $("#remarks").focus();
            return false;
        }
        else if ($("#remarks").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Remarks maximum 50 characters!!";
            $("#remarks").focus();
            return false;
        }
        else if (isEmptyString($("#stationNo").val())) {

            valdiationMsgContent = "Station Name is required!";
            $("#stationNo").focus();
            return false;
        }
        else if (isEmptyString($("#employeeIdForApproval").val())) {

            valdiationMsgContent = "Approval Employee Id  is required!";
            $("#employeeIdForApproval").focus();
            return false;
        }
        else if (isEmptyString($("#approvalEmpNo").val())) {

            valdiationMsgContent = "Approval Employee Id  is not valid!!!";
            $("#employeeIdForApproval").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    /*$('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var maintenanceBillPaymentNo = curRow.find('td:eq(0)').text();
        var billingYear = curRow.find('td:eq(1)').text();
        var billingMonth = curRow.find('td:eq(2)').text();
        var billingDate = curRow.find('td:eq(3)').text();
        var billingAmount = curRow.find('td:eq(4)').text();
        var remarks = curRow.find('td:eq(5)').text();
        var maintenanceNo = curRow.find('td:eq(6)').text();
        var stationNo = curRow.find('td:eq(7)').text();
        var approvalEmpNo = curRow.find('td:eq(8)').text();
        var employeeIdForApproval = curRow.find('td:eq(9)').text();
        var insertUser = curRow.find('td:eq(10)').text();


        $('#maintenanceBillPaymentNo').val(maintenanceBillPaymentNo);
        $('#billingYear').val(billingYear).trigger('change');
        $('#billingMonth').val(billingMonth).trigger('change');
        $('#billingDate').val(billingDate);
        $('#billingAmount').val(billingAmount);
        $('#remarks').val(remarks);
        $('#maintenanceNo').val(maintenanceNo).trigger('change');
        $('#stationNo').val(stationNo).trigger('change');
        $('#approvalEmpNo').val(approvalEmpNo);
        $('#employeeIdForApproval').val(employeeIdForApproval);
        $('#insertUser').val(insertUser);

        $.ajax({
            url: "/vehicle-ajax/getEmpolyeeDetailsForRepossessionApproval/" + employeeIdForApproval,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var approvalEmpNo = "";
                var employeeNameForApproval = "";
                var employeeDesignationForApproval = "";
                var employeeOfficeForApproval = "";
                var officeNoForApproval = "";


                $.each(response, function (i, l) {
                    approvalEmpNo = l[0];
                    employeeNameForApproval = l[1];
                    employeeDesignationForApproval = l[2];
                    employeeOfficeForApproval = l[3];
                    officeNoForApproval = l[4];

                });

                $("#approvalEmpNo").val(approvalEmpNo);
                $("#employeeNameForApproval").val(employeeNameForApproval);
                $("#employeeDesignationForApproval").val(employeeDesignationForApproval);
                $("#employeeOfficeForApproval").val(employeeOfficeForApproval);
                $("#officeNoForApproval").val(officeNoForApproval);

                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $("#approvalEmpNo").val("");
                $("#employeeNameForApproval").val("");
                $("#employeeDesignationForApproval").val("");
                $("#employeeOfficeForApproval").val("");
                $("#officeNoForApproval").val("");


                $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
            }
        });
        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });*/

    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var maintenanceNo = curRow.find('td:eq(0)').text();

        $('#maintenanceNo').val(maintenanceNo);

        $.ajax({
            url: "/vehicle-ajax/getMaintenanceDetails/" + maintenanceNo,
            type: 'GET',
            dataType: 'json',
            success: function (response) {

                var maintenanceTypeNo = "";
                var vehicleNo = "";
                var maintenanceDate = "";
                var maintenanceAmount = "";
                var serviceCenterName = "";
                var serviceCenterAddress = "";
                var maintenanceRemarks = "";
                var driverNo = "";

                var receiptNumber1 = "";
                var receiptDate1 = "";
                var fileNumber1 = "";



                $.each(response, function (i, l) {
                    maintenanceTypeNo = l[0];
                    vehicleNo = l[1];
                    maintenanceDate = l[2];
                    maintenanceAmount = l[3];
                    serviceCenterName = l[4];
                    serviceCenterAddress = l[5];
                    maintenanceRemarks = l[6];
                    driverNo = l[7];

                    receiptNumber1 = l[8];
                    receiptDate1 = l[9];
                    fileNumber1 = l[10];

                });

                $('#maintenanceTypeNo').val(maintenanceTypeNo);
                $('#vehicleNo').val(vehicleNo);
                //$('#maintenanceDate').val(maintenanceDate);
                var maintenanceDate = getDateShow(maintenanceDate);
                $('#maintenanceDate').val(maintenanceDate);
                $('#maintenanceAmount').val(maintenanceAmount);
                $('#serviceCenterName').val(serviceCenterName);
                $('#serviceCenterAddress').val(serviceCenterAddress);
                $('#maintenanceRemarks').val(maintenanceRemarks);
                $('#driverNo').val(driverNo);

                $('#receiptNumber1').val(receiptNumber1);
                //$('#receiptDate1').val(receiptDate1);
                var receiptDate1 = getDateShow(receiptDate1);
                $('#receiptDate1').val(receiptDate1);
                $('#fileNumber1').val(fileNumber1);

                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $('#maintenanceNo').val("");
                $('#maintenanceTypeNo').val("");
                $('#vehicleNo').val("");
                $('#maintenanceDate').val("");
                $('#maintenanceAmount').val("");
                $('#serviceCenterName').val("");
                $('#serviceCenterAddress').val("");
                $('#maintenanceRemarks').val("");
                $('#driverNo').val("");
                $('#receiptNumber1').val("");
                $('#receiptDate1').val("");
                $('#fileNumber1').val("");

                $("#error_UDForApproval").text("Sorry! maintenance Id not match!!");
            }
        });
        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });

    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start two------------------------*/
    $(document).on("input", "#employeeIdForApproval", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeIdForApproval = $('#employeeIdForApproval').val();


        if (employeeIdForApproval != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeIdForApproval,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var approvalEmpNo = "";
                    var employeeNameForApproval = "";
                    var employeeDesignationForApproval = "";
                    var employeeOfficeForApproval = "";
                    var officeNoForApproval = "";


                    $.each(response, function (i, l) {
                        approvalEmpNo = l[0];
                        employeeNameForApproval = l[1];
                        employeeDesignationForApproval = l[2];
                        employeeOfficeForApproval = l[3];
                        officeNoForApproval = l[4];

                    });

                    $("#approvalEmpNo").val(approvalEmpNo);
                    $("#employeeNameForApproval").val(employeeNameForApproval);
                    $("#employeeDesignationForApproval").val(employeeDesignationForApproval);
                    $("#employeeOfficeForApproval").val(employeeOfficeForApproval);
                    $("#officeNoForApproval").val(officeNoForApproval);

                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $("#approvalEmpNo").val("");
                    $("#employeeNameForApproval").val("");
                    $("#employeeDesignationForApproval").val("");
                    $("#employeeOfficeForApproval").val("");
                    $("#officeNoForApproval").val("");

                    $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeIdForApproval').val() == "") {
                $("#approvalEmpNo").val("");
                $("#employeeNameForApproval").val("");
                $("#employeeDesignationForApproval").val("");
                $("#employeeOfficeForApproval").val("");
                $("#officeNoForApproval").val("");


            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End ------------------------*/

    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#maintenanceNo", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var maintenanceNo = $('#maintenanceNo').val();


        if (maintenanceNo != "") {
            $.ajax({
                url: "/vehicle-ajax/getMaintenanceDetails/" + maintenanceNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {

                    var maintenanceTypeNo = "";
                    var vehicleNo = "";
                    var maintenanceDate = "";
                    var maintenanceAmount = "";
                    var serviceCenterName = "";
                    var serviceCenterAddress = "";
                    var maintenanceRemarks = "";
                    var driverNo = "";

                    var receiptNumber1 = "";
                    var receiptDate1 = "";
                    var fileNumber1 = "";



                    $.each(response, function (i, l) {
                         maintenanceTypeNo = l[0];
                         vehicleNo = l[1];
                         maintenanceDate = l[2];
                         maintenanceAmount = l[3];
                         serviceCenterName = l[4];
                         serviceCenterAddress = l[5];
                         maintenanceRemarks = l[6];
                         driverNo = l[7];

                        receiptNumber1 = l[8];
                        receiptDate1 = l[9];
                        fileNumber1 = l[10];

                    });

                    $('#maintenanceTypeNo').val(maintenanceTypeNo);
                    $('#vehicleNo').val(vehicleNo);
                    //$('#maintenanceDate').val(maintenanceDate);
                    var maintenanceDate = getDateShow(maintenanceDate);
                    $('#maintenanceDate').val(maintenanceDate);
                    $('#maintenanceAmount').val(maintenanceAmount);
                    $('#serviceCenterName').val(serviceCenterName);
                    $('#serviceCenterAddress').val(serviceCenterAddress);
                    $('#maintenanceRemarks').val(maintenanceRemarks);
                    $('#driverNo').val(driverNo);

                    $('#receiptNumber1').val(receiptNumber1);
                    //$('#receiptDate1').val(receiptDate1);
                    var receiptDate1 = getDateShow(receiptDate1);
                    $('#receiptDate1').val(receiptDate1);
                    $('#fileNumber1').val(fileNumber1);

                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $('#maintenanceNo').val("");
                    $('#maintenanceTypeNo').val("");
                    $('#vehicleNo').val("");
                    $('#maintenanceDate').val("");
                    $('#maintenanceAmount').val("");
                    $('#serviceCenterName').val("");
                    $('#serviceCenterAddress').val("");
                    $('#maintenanceRemarks').val("");
                    $('#driverNo').val("");
                    $('#receiptNumber1').val("");
                    $('#receiptDate1').val("");
                    $('#fileNumber1').val("");

                    $("#error_UDForApproval").text("Sorry! maintenance Id not match!!");
                }
            });
        }
        else {

            if ($('#maintenanceNo').val() == "") {
                $('#maintenanceNo').val("");
                $('#maintenanceTypeNo').val("");
                $('#vehicleNo').val("");
                $('#maintenanceDate').val("");
                $('#maintenanceAmount').val("");
                $('#serviceCenterName').val("");
                $('#serviceCenterAddress').val("");
                $('#maintenanceRemarks').val("");
                $('#driverNo').val("");
                $('#receiptNumber1').val("");
                $('#receiptDate1').val("");
                $('#fileNumber1').val("");

            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End two------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


    function getDateShow(dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;

        return date;
    };


});
