/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();
    $("#employeeId").focus();


    /*-------------- asset save function start ------------------------*/
    $("#vehicleRentsSave").click(function () {
        saveData();
    });
    $("#vehicleRentsReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var vehicleRents = {};
        vehicleRents.vehicleRentsNo = $("#vehicleRentsNo").val();
        vehicleRents.vehicleNo = $("#vehicleNo").val();
        vehicleRents.rentFrom = $("#rentFrom").val();
        vehicleRents.providerDetails = $("#providerDetails").val();
        vehicleRents.contactNumber = $("#contactNumber").val();
        vehicleRents.address = $("#address").val();
        vehicleRents.monthlyRent = $("#monthlyRent").val();

        vehicleRents.receiptNumber = $("#receiptNumber").val();
        var receiptDateConvert = $('#receiptDate').val();
        if (receiptDateConvert) {
            receiptDateConvert = receiptDateConvert.split("/").reverse().join("/");
            receiptDateConvert = getDate(receiptDateConvert);
        }
        vehicleRents.receiptDate = receiptDateConvert;
        vehicleRents.fileNumber = $("#fileNumber").val();

        vehicleRents.insertUser = $('#insertUser').val();


        return vehicleRents;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-vehicle-rents";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 2000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#vehicleRentsNo").val('');
        $("#vehicleNo").val('');
        $("#rentFrom").val('');
        $("#providerDetails").val('');
        $("#contactNumber").val('');
        $("#address").val('');
        $("#monthlyRent").val('');

        $("#receiptNumber").val('');
        $("#receiptDate").val('');
        $("#fileNumber").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#vehicleNo").val())) {
            valdiationMsgContent = "Vehicle Name is required!";
            $("#vehicleNo").focus();
            return false;
        }
        else if (isEmptyString($("#rentFrom").val())) {
            valdiationMsgContent = "Rent From is required!";
            $("#rentFrom").focus();
            return false;
        }
        else if ($("#rentFrom").val().length >100) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Rent From maximum 100 characters!!";
            $("#rentFrom").focus();
            return false;
        }
        else if (isEmptyString($("#contactNumber").val())) {
            valdiationMsgContent = "Contact Number is required!";
            $("#contactNumber").focus();
            return false;
        }
        else if ($("#contactNumber").val().length >11) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Contact Number maximum 11 characters!!";
            $("#contactNumber").focus();
            return false;
        }
        else if (isEmptyString($("#monthlyRent").val())) {
            valdiationMsgContent = "Monthly Rent is required!";
            $("#monthlyRent").focus();
            return false;
        }

        else if (isEmptyString($("#receiptNumber").val())) {
            valdiationMsgContent = "Receipt Number is required!";
            $("#receiptNumber").focus();
            return false;
        }
        else if ($("#receiptNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Receipt Number maximum 50 characters!!";
            $("#receiptNumber").focus();
            return false;
        }
        else if (isEmptyString($("#receiptDate").val())) {
            valdiationMsgContent = "Receipt Date is required!";
            $("#receiptDate").focus();
            return false;
        }
        /*else if (checkDate($("#receiptDate").val())) {
            valdiationMsgContent = "Receipt Date is invalid!!!!";
            $("#receiptDate").focus();
            return false;
        }*/
        else if (isEmptyString($("#fileNumber").val())) {
            valdiationMsgContent = "File Number is required!";
            $("#fileNumber").focus();
            return false;
        }
        else if ($("#fileNumber").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "File Number maximum 50 characters!!";
            $("#fileNumber").focus();
            return false;
        }

        else if (isEmptyString($("#address").val())) {
            valdiationMsgContent = "Address is required!";
            $("#address").focus();
            return false;
        }
        else if ($("#address").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Address maximum 50 characters!!";
            $("#address").focus();
            return false;
        }
        else if (isEmptyString($("#providerDetails").val())) {
            valdiationMsgContent = "Provider Details is required!";
            $("#providerDetails").focus();
            return false;
        }
        else if ($("#providerDetails").val().length >200) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Provider Details maximum 200 characters!!";
            $("#providerDetails").focus();
            return false;
        }
        else {
            return true;
        }

    }


    function checkDate(fromDate) {
        var datevalidationStatus = false;
        var date = fromDate.substring(0, 2);
        var month = fromDate.substring(3, 5);
        var year = fromDate.substring(6, 10);
        var fromDate = new Date(year, month - 1, date);

        var precentDate = new Date();
        alert(precentDate);

        if (precentDate >= fromDate) {
            datevalidationStatus = true;
        }
        return datevalidationStatus;
    }


    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var vehicleRentsNo = curRow.find('td:eq(0)').text();
        var rentFrom = curRow.find('td:eq(1)').text();
        var providerDetails = curRow.find('td:eq(2)').text();
        var contactNumber = curRow.find('td:eq(3)').text();
        var address = curRow.find('td:eq(4)').text();
        var monthlyRent = curRow.find('td:eq(5)').text();
        var vehicleNo = curRow.find('td:eq(6)').text();

        var receiptNumber = curRow.find('td:eq(7)').text();
        var receiptDate = curRow.find('td:eq(8)').text();
        var fileNumber = curRow.find('td:eq(9)').text();


        var insertUser = curRow.find('td:eq(10)').text();


        $('#vehicleRentsNo').val(vehicleRentsNo);
        $('#rentFrom').val(rentFrom);
        $('#providerDetails').val(providerDetails);
        $('#contactNumber').val(contactNumber);
        $('#address').val(address);
        $('#monthlyRent').val(monthlyRent);
        $('#vehicleNo').val(vehicleNo).trigger('change');

        $('#receiptNumber').val(receiptNumber);
        $('#receiptDate').val(receiptDate);
        $('#fileNumber').val(fileNumber);

        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
