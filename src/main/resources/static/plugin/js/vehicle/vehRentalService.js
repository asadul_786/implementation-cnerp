$(document).ready(function () {

    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field!";
        valdiationMsgContent = "";
        if (isEmptyString($("#empId").val())) {
            valdiationMsgContent = "Employee ID is required!";
            $("#empId").focus();
            return false;
        }else if ($("#rentalMonth").val()=='-1') {
            valdiationMsgContent = "Rental Month is required!";
            $("#rentalMonth").focus();
            return false;
        }else if (isEmptyString($("#rentalDate").val())) {
            valdiationMsgContent = "Rental Date is required!";
            $("#rentalDate").focus();
            return false;
        }else if (isEmptyString($("#rentalAmount").val())) {
            valdiationMsgContent = "Rental Amount is required!";
            $("#rentalAmount").focus();
            return false;
        }else if (isEmptyString($("#fromLocation").val())) {
            valdiationMsgContent = "From Location is required!";
            $("#fromLocation").focus();
            return false;
        }else if (isEmptyString($("#toLocation").val())) {
            valdiationMsgContent = "To Location is required!";
            $("#toLocation").focus();
            return false;
        }else if (isEmptyString($("#fromDate").val())) {
            valdiationMsgContent = "From Date is required!";
            $("#fromDate").focus();
            return false;
        }else if (isEmptyString($("#toDate").val())) {
            valdiationMsgContent = "To Date is required!";
            $("#toDate").focus();
            return false;
        }
        else {
            return true;
        }

    }

    $(document).on('input', '#rentalAmount', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    // $("#regDate").datepicker({
    //     maxDate: 0
    // });
    //
    // $("#regRenualDate").datepicker({
    //     minDate: 0
    // });





    $(document).on("input", "#empId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var empId = $('#empId').val();


        if (empId != "" && empId.length < 16) {
            $.ajax({
                url: "/vehicle/employee-details/" + empId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];





                    });

                    $("#empGid").val(value_1);

                    $("#empName").val(value_2);
                    $("#desig").val(value_3);
                    $("#office").val(value_4);




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }

    });




    var table = $('#dataTable').DataTable({


    });

    $(document).on("click", "#save", function () {

        if(isValidated()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var vehVehicle = {};
                        vehVehicle.rentalServiceNo = $("#key").val();
                        vehVehicle.employeeNo = $("#empGid").val();
                        vehVehicle.rentalMonth = $("#rentalMonth").val();
                        vehVehicle.rentalDate = $("#rentalDate").val();
                        vehVehicle.rentalAmount = $("#rentalAmount").val();
                        vehVehicle.fromLocation = $("#fromLocation").val();
                        vehVehicle.toLocation = $("#toLocation").val();
                        vehVehicle.fromDate = $("#fromDate").val();
                        vehVehicle.toDate = $("#toDate").val();
                        vehVehicle.remarks = $("#remarks").val();
                        vehVehicle.receiptNumber = $("#receiptNumber").val();
                        vehVehicle.receiptDate = $("#receiptDate").val();
                        vehVehicle.fileNumber = $("#fileNumber").val();


                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveRentalService",
                            type: 'POST',
                            data: JSON.stringify(vehVehicle),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        } else{
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }

    });



    $(document).on("click", "#edit", function () {

        //setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#key").val(curRow.find('td:eq(0)').text());
        $("#empGid").val(curRow.find('td:eq(1)').text());
        $("#rentalMonth").val(curRow.find('td:eq(2)').text());
        $("#rentalDate").val(curRow.find('td:eq(3)').text());
        $("#rentalAmount").val(curRow.find('td:eq(4)').text());
        $("#fromLocation").val(curRow.find('td:eq(5)').text());
        $("#toLocation").val(curRow.find('td:eq(6)').text());
        $("#fromDate").val(curRow.find('td:eq(7)').text());
        $("#toDate").val(curRow.find('td:eq(8)').text());
        $("#remarks").val(curRow.find('td:eq(9)').text());
        $("#receiptNumber").val(curRow.find('td:eq(10)').text());
        $("#receiptDate").val(curRow.find('td:eq(11)').text());
        $("#fileNumber").val(curRow.find('td:eq(12)').text());


        var empGid = $('#empGid').val();


        if (empGid != "") {
            $.ajax({
                url: "/vehicle/employee-gid-details/" + empGid,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var value_1 = "";
                    var value_2 = "";
                    var value_3 = "";
                    var value_4 = "";

                    $.each(response, function (i, l) {



                        value_1 = l[0];
                        value_2 = l[1];
                        value_3 = l[2];
                        value_4 = l[3];





                    });

                    $("#empId").val(value_1);

                    $("#empName").val(value_2);
                    $("#desig").val(value_3);
                    $("#office").val(value_4);




                },
                error: function (xhr, status, error) {


                }
            });
        } else {


        }



        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    $(document).on("click", "#clear_btn", function () {
        $("select").prop("disabled", false);
        $("select").val('-1');
        clrErr();
        $("#save").text('Save');
    });

    function clrErr() {

        $("#err_p_cd").text("");
        $("#err_p_status").text("");
        $("#err_appl_party").text("");

    }


    function validate() {

        clrErr();


        return true;

    }

    // $('#dataTable tbody').on('click', '#delete', function () {
    //     var curRow = $(this).closest('tr');
    //     var col1 = curRow.find('td:eq(0)').text();
    //
    //     $.confirm({
    //         title: 'Confirm',
    //         content: 'Selected record will be deleted.',
    //         buttons: {
    //             ok: function () {
    //                 $.ajax({
    //                     contentType: 'application/json',
    //                     url: "/vehicle/vehVehicleDelete/" + col1,
    //                     type: 'POST',
    //                     //async: false,
    //                     //data: JSON.stringify(answerDto),
    //                     dataType: 'json',
    //                     success: function (response) {
    //
    //                         if (response === true) {
    //                             table.row(curRow).remove().draw(false);
    //                             showAlert("Deleted Successfully");
    //                         } else {
    //                             showAlert("Unknown error");
    //                         }
    //
    //                     },
    //                     error: function (xhr, status, error) {
    //                         showAlert("Unknown error");
    //                     }
    //                 });
    //             },
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    //
    // });

});