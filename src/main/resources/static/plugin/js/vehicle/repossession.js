/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#repossessionSave").click(function () {
        saveData();
    });
    $("#repossessionReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var repossession = {};
        repossession.purchaseRepossessionNo = $("#purchaseRepossessionNo").val();
        repossession.repossessionEmpNo = $("#repossessionEmpNo").val();
        repossession.officeNo = $("#officeNo").val();
        repossession.repossessionDetails = $("#repossessionDetails").val();
        var repossessionDateConvert = $('#repossessionDate').val();
        if (repossessionDateConvert) {
            repossessionDateConvert = repossessionDateConvert.split("/").reverse().join("/");
            repossessionDateConvert = getDate(repossessionDateConvert);
        }
        repossession.repossessionDate = repossessionDateConvert;


        repossession.insertUser = $('#insertUser').val();
        /*var insertDate = $('#insertDate').val();
        if (insertDate) {
            insertDate = insertDate.split("/").reverse().join("/");
            insertDate = getDate(insertDate);
        }*/

        return repossession;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-repossession-post";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();

                    }
                    setInterval(function(){location.reload(true); }, 2000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {

        $("#employeeId").val('');
        $("#repossessionEmpNo").val('');
        $("#employeeName").val('');
        $("#employeeDesignation").val('');
        $("#employeeOffice").val('');
        $("#officeNo").val('');
        $("#repossessionDate").val('');
        $("#repossessionDetails").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#employeeId").val())) {

            valdiationMsgContent = "Employee Id is required!";
            $("#employeeId").focus();
            return false;
        }
        else if ($("#employeeId").val().length >8) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Employee Id maximum 8 characters!!";
            $("#employeeId").focus();
            return false;
        }
        else if (isEmptyString($("#repossessionDate").val())) {

            valdiationMsgContent = "Repossession date is required!";
            $("#repossessionDate").focus();
            return false;
        }
        /*else if (checkvalid_JoinDate($("#repossessionDate").val())) {

            valdiationMsgContent = "mithu";
            $("#repossessionDate").focus();
            return false;
        }*/
        else if (isEmptyString($("#repossessionDetails").val())) {

            valdiationMsgContent = "Repossession details is required!";
            $("#repossessionDetails").focus();
            return false;
        }
        else if (isEmptyString($("#officeNo").val())) {

            valdiationMsgContent = "officeNo is required!";
            $("#officeNo").focus();
            return false;
        }
        else if ($("#repossessionDetails").val().length >300) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Repossession Details maximum 300 characters!!";
            $("#repossessionDetails").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*var repossessionTable = $('#tbl-repossession-lst').DataTable({

        data: [],
        "columns": [
            {"data": "repossessionDate"},
            {"data": "repossessionDetails"},
            {"data": "insertUser"},
            {"data": "insertDate"},
            {"data": null, defaultContent: ''}

        ],
        'columnDefs': [
             {
                'targets': 4,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '  <a class="btn btn-link  btn-round repossession-edit"   encId="' + data.purchaseRepossessionNo + '"> <i class="fa fa-edit" aria-hidden="true" style="font-size: large"></i></a>';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    function loadDatableData() {

        var urlBuilder = "/vehicle-ajax/repossession-data-list";

        $.ajax({
            url: urlBuilder,
            type: "GET"

        }).done(function (result) {
            repossessionTable.clear().draw();
            repossessionTable.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }*/
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var purchaseRepossessionNo = curRow.find('td:eq(0)').text();
        var repossessionDate  = curRow.find('td:eq(1)').text();
        var repossessionDetails  = curRow.find('td:eq(2)').text();
        var insertUser  = curRow.find('td:eq(5)').text();
        var employeeId = curRow.find('td:eq(9)').text();

        $('#purchaseRepossessionNo').val(purchaseRepossessionNo);
        $('#repossessionDetails').val(repossessionDetails);
        $('#repossessionDate').val(repossessionDate);
        $('#employeeId').val(employeeId);
        $('#insertUser').val(insertUser);

        $.ajax({
            url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var repossessionEmpNo = "";
                var employeeName = "";
                var employeeDesignation = "";
                var employeeOffice = "";
                var officeNo = "";

                $.each(response, function (i, l) {
                    repossessionEmpNo = l[0];
                    employeeName = l[1];
                    employeeDesignation = l[2];
                    employeeOffice = l[3];
                    officeNo = l[4];
                });

                $("#repossessionEmpNo").val(repossessionEmpNo);
                $("#employeeName").val(employeeName);
                $("#employeeDesignation").val(employeeDesignation);
                $("#employeeOffice").val(employeeOffice);
                $("#officeNo").val(officeNo);

                $("#error_UD").text("");

            },
            error: function (xhr, status, error) {
                $("#repossessionEmpNo").val("");
                $("#employeeName").val("");
                $("#employeeDesignation").val("");
                $("#employeeOffice").val("");
                $("#officeNo").val("");

                $("#error_UD").text("Sorry! Employee Id not match!!");
            }
        });

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#employeeId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeId = $('#employeeId').val();


        if (employeeId != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var repossessionEmpNo = "";
                    var employeeName = "";
                    var employeeDesignation = "";
                    var employeeOffice = "";
                    var officeNo = "";

                    $.each(response, function (i, l) {
                        repossessionEmpNo = l[0];
                        employeeName = l[1];
                        employeeDesignation = l[2];
                        employeeOffice = l[3];
                        officeNo = l[4];


                    });

                    $("#repossessionEmpNo").val(repossessionEmpNo);
                    $("#employeeName").val(employeeName);
                    $("#employeeDesignation").val(employeeDesignation);
                    $("#employeeOffice").val(employeeOffice);
                    $("#officeNo").val(officeNo);

                    $("#error_UD").text("");

                },
                error: function (xhr, status, error) {
                    $("#repossessionEmpNo").val("");
                    $("#employeeName").val("");
                    $("#employeeDesignation").val("");
                    $("#employeeOffice").val("");
                    $("#officeNo").val("");

                    $("#error_UD").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeId').val() == "") {
                $("#error_UD").text("Employee Id required!");
                $("#employeeId").val("");
                $("#repossessionEmpNo").val("");
                $("#employeeName").val("");
                $("#employeeDesignation").val("");
                $("#employeeOffice").val("");
                $("#officeNo").val("");


            }
            else {
                $("#error_UD").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }

    function checkvalid_JoinDate(enteredDate) {
        var status = false;
        var date = enteredDate.substring(0, 2);
        var month = enteredDate.substring(3, 5);
        var year = enteredDate.substring(6, 10);
        var myDate = new Date(year, month - 1, date);
        var today = new Date();
        if (myDate > today) {
            status = true;
        }
        return status;
    }


});
