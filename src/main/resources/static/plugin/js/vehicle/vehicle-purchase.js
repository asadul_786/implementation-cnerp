/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();



    /*-------------- asset save function start ------------------------*/
    $("#vehiclePurchaseSave").click(function () {
        saveData();
    });
    $("#vehiclePurchaseReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var vehiclePurchase = {};
        vehiclePurchase.purchaseNo = $("#purchaseNo").val();
        var purchaseDateConvert = $('#purchaseDate').val();
        if (purchaseDateConvert) {
            purchaseDateConvert = purchaseDateConvert.split("/").reverse().join("/");
            purchaseDateConvert = getDate(purchaseDateConvert);
        }
        vehiclePurchase.purchaseDate = purchaseDateConvert;


        vehiclePurchase.vehicleNo = $("#vehicleNo").val();
        vehiclePurchase.empNo = $("#empNo").val();
        vehiclePurchase.purchaseAmount = $("#purchaseAmount").val();
        vehiclePurchase.purchaseRemarks = $("#purchaseRemarks").val();


        vehiclePurchase.receiptNumber = $("#receiptNumber").val();
        var receiptDateConvert = $('#receiptDate').val();
        if (receiptDateConvert) {
            receiptDateConvert = receiptDateConvert.split("/").reverse().join("/");
            receiptDateConvert = getDate(receiptDateConvert);
        }
        vehiclePurchase.receiptDate = receiptDateConvert;
        vehiclePurchase.fileNumber = $("#fileNumber").val();

        vehiclePurchase.insertUser = $('#insertUser').val();
        return vehiclePurchase;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-vehicle-purchase";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 1500);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#purchaseNo").val('');
        $("#purchaseDate").val('');
        $("#vehicleNo").val('');
        $("#purchaseAmount").val('');
        $("#purchaseRemarks").val('');
        $("#empNo").val('');

        $("#receiptNumber").val('');
        $("#receiptDate").val('');
        $("#fileNumber").val('');

        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";

         if (isEmptyString($("#vehicleNo").val())) {
            valdiationMsgContent = "Vehicle Name is required!";
            $("#vehicleNo").focus();
            return false;
        }
         else if (isEmptyString($("#purchaseAmount").val())) {
             valdiationMsgContent = "Purchase Amount is required!";
             $("#purchaseAmount").focus();
             return false;
         }
         else if (isEmptyString($("#receiptNumber").val())) {
             valdiationMsgContent = "Receipt Number is required!";
             $("#receiptNumber").focus();
             return false;
         }
         else if ($("#receiptNumber").val().length >50) {
             valdiationMsgTitle = "Length validation!!!";
             valdiationMsgContent = "Receipt Number maximum 50 characters!!";
             $("#receiptNumber").focus();
             return false;
         }
        else if (isEmptyString($("#purchaseDate").val())) {
            valdiationMsgContent = "Purchase Date is required!";
            $("#purchaseDate").focus();
            return false;
        }
         else if (isEmptyString($("#receiptDate").val())) {
             valdiationMsgContent = "Receipt Date is required!";
             $("#receiptDate").focus();
             return false;
         }
        else if (isEmptyString($("#purchaseRemarks").val())) {
            valdiationMsgContent = "Purchase Remarks is required!";
            $("#purchaseRemarks").focus();
            return false;
        }
         else if ($("#purchaseRemarks").val().length >100) {
             valdiationMsgTitle = "Length validation!!!";
             valdiationMsgContent = "Purchase Remarks maximum 100 characters!!";
             $("#purchaseRemarks").focus();
             return false;
         }
         else if (isEmptyString($("#fileNumber").val())) {
             valdiationMsgContent = "File Number is required!";
             $("#fileNumber").focus();
             return false;
         }
         else if ($("#fileNumber").val().length >50) {
             valdiationMsgTitle = "Length validation!!!";
             valdiationMsgContent = "File Number maximum 50 characters!!";
             $("#fileNumber").focus();
             return false;
         }
         else if (isEmptyString($("#employeeIdForApproval").val())) {

             valdiationMsgContent = "Approval Employee Id is required!";
             $("#employeeIdForApproval").focus();
             return false;
         }
         else if (isEmptyString($("#empNo").val())) {

             valdiationMsgContent = "Approval Employee Id is Not Valid!!!!";
             $("#employeeIdForApproval").focus();
             return false;
         }

        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var purchaseNo = curRow.find('td:eq(0)').text();
        var purchaseDate = curRow.find('td:eq(1)').text();
        var vehicleNo = curRow.find('td:eq(2)').text();
        var purchaseAmount = curRow.find('td:eq(3)').text();
        var purchaseRemarks = curRow.find('td:eq(4)').text();
        var empNo = curRow.find('td:eq(5)').text();
        var employeeIdForApproval = curRow.find('td:eq(6)').text();

        var receiptNumber = curRow.find('td:eq(7)').text();
        var receiptDate = curRow.find('td:eq(8)').text();
        var fileNumber = curRow.find('td:eq(9)').text();

        var insertUser = curRow.find('td:eq(10)').text();



        $('#purchaseNo').val(purchaseNo);
        $('#purchaseDate').val(purchaseDate);
        $('#vehicleNo').val(vehicleNo).trigger('change');
        $('#purchaseAmount').val(purchaseAmount);
        $('#purchaseRemarks').val(purchaseRemarks);
        $('#empNo').val(empNo);
        $('#employeeIdForApproval').val(employeeIdForApproval);

        $('#receiptNumber').val(receiptNumber);
        $('#receiptDate').val(receiptDate);
        $('#fileNumber').val(fileNumber);

        $('#insertUser').val(insertUser);


        $.ajax({
            url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeIdForApproval,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                var empNo = "";
                var employeeNameForApproval = "";
                var employeeDesignationForApproval = "";
                var employeeOfficeForApproval = "";
                var officeNoForApproval = "";


                $.each(response, function (i, l) {
                    empNo = l[0];
                    employeeNameForApproval = l[1];
                    employeeDesignationForApproval = l[2];
                    employeeOfficeForApproval = l[3];
                    officeNoForApproval = l[4];

                });

                $("#empNo").val(empNo);
                $("#employeeNameForApproval").val(employeeNameForApproval);
                $("#employeeDesignationForApproval").val(employeeDesignationForApproval);
                $("#employeeOfficeForApproval").val(employeeOfficeForApproval);
                $("#officeNoForApproval").val(officeNoForApproval);

                $("#error_UDForApproval").text("");

            },
            error: function (xhr, status, error) {
                $("#empNo").val("");
                $("#employeeNameForApproval").val("");
                $("#employeeDesignationForApproval").val("");
                $("#employeeOfficeForApproval").val("");
                $("#officeNoForApproval").val("");


                $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
            }
        });
        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#employeeIdForApproval", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeIdForApproval = $('#employeeIdForApproval').val();


        if (employeeIdForApproval != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeIdForApproval,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var empNo = "";
                    var employeeNameForApproval = "";
                    var employeeDesignationForApproval = "";
                    var employeeOfficeForApproval = "";
                    var officeNoForApproval = "";


                    $.each(response, function (i, l) {
                        empNo = l[0];
                        employeeNameForApproval = l[1];
                        employeeDesignationForApproval = l[2];
                        employeeOfficeForApproval = l[3];
                        officeNoForApproval = l[4];

                    });

                    $("#empNo").val(empNo);
                    $("#employeeNameForApproval").val(employeeNameForApproval);
                    $("#employeeDesignationForApproval").val(employeeDesignationForApproval);
                    $("#employeeOfficeForApproval").val(employeeOfficeForApproval);
                    $("#officeNoForApproval").val(officeNoForApproval);

                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {
                    $("#empNo").val("");
                    $("#employeeNameForApproval").val("");
                    $("#employeeDesignationForApproval").val("");
                    $("#employeeOfficeForApproval").val("");
                    $("#officeNoForApproval").val("");


                    $("#error_UDForApproval").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeIdForApproval').val() == "") {
                $("#empNo").val("");
                $("#employeeNameForApproval").val("");
                $("#employeeDesignationForApproval").val("");
                $("#employeeOfficeForApproval").val("");
                $("#officeNoForApproval").val("");


            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
