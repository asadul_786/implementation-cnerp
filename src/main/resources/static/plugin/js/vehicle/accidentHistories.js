/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#accidentHistoriesSave").click(function () {
        saveData();
    });
    $("#accidentHistoriesReferesh").click(function () {
        clearForm();
    });

    function getFormData() {


        var accidentHistories = {};
        accidentHistories.accidentHistoriesNo = $("#accidentHistoriesNo").val();
        accidentHistories.driverNo = $("#driverNo").val();
        accidentHistories.vehicleNo = $("#vehicleNo").val();
        accidentHistories.accidentTypeNo = $("#accidentTypeNo").val();
        accidentHistories.accidentDetails = $("#accidentDetails").val();
        accidentHistories.transportConditionDetails = $("#transportConditionDetails").val();
        var accidentDateConvert = $('#accidentDate').val();
        if (accidentDateConvert) {
            accidentDateConvert = accidentDateConvert.split("/").reverse().join("/");
            accidentDateConvert = getDate(accidentDateConvert);
        }
        accidentHistories.accidentDate = accidentDateConvert;
        accidentHistories.insertUser = $('#insertUser').val();

        return accidentHistories;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-accident-histories";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 2000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#accidentHistoriesNo").val('');
        $("#driverNo").val('');
        $("#vehicleNo").val('');
        $("#accidentTypeNo").val('');
        $("#accidentDate").val('');
        $("#accidentDetails").val('');
        $("#transportConditionDetails").val('');
        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#driverNo").val())) {

            valdiationMsgContent = "Driver name is required!";
            $("#driverNo").focus();
            return false;
        }
        else if (isEmptyString($("#vehicleNo").val())) {

            valdiationMsgContent = "Vehicle name is required!";
            $("#vehicleNo").focus();
            return false;
        }
        else if (isEmptyString($("#accidentTypeNo").val())) {

            valdiationMsgContent = "Accident Type name is required!";
            $("#accidentTypeNo").focus();
            return false;
        }
        else if (isEmptyString($("#accidentDate").val())) {

            valdiationMsgContent = "Accident Date is required!";
            $("#accidentDate").focus();
            return false;
        }
        else if (isEmptyString($("#accidentDetails").val())) {

            valdiationMsgContent = "Accident Details is required!";
            $("#accidentDetails").focus();
            return false;
        }
        else if ($("#accidentDetails").val().length >300) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Accident Details maximum 300 characters!!";
            $("#accidentDetails").focus();
            return false;
        }
        else if (isEmptyString($("#transportConditionDetails").val())) {

            valdiationMsgContent = "Transport Condition Details is required!";
            $("#transportConditionDetails").focus();
            return false;
        }
        else if ($("#transportConditionDetails").val().length >300) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Transport Condition Details maximum 300 characters!!";
            $("#transportConditionDetails").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var accidentHistoriesNo = curRow.find('td:eq(0)').text();
        var accidentDate = curRow.find('td:eq(1)').text();
        var accidentDetails = curRow.find('td:eq(2)').text();
        var transportConditionDetails = curRow.find('td:eq(3)').text();
        var driverNo = curRow.find('td:eq(4)').text();
        var vehicleNo = curRow.find('td:eq(5)').text();
        var accidentTypeNo = curRow.find('td:eq(6)').text();
        var insertUser = curRow.find('td:eq(7)').text();


        $('#accidentHistoriesNo').val(accidentHistoriesNo);
        $('#accidentDate').val(accidentDate);
        $('#accidentDetails').val(accidentDetails);
        $('#transportConditionDetails').val(transportConditionDetails);
        $('#driverNo').val(driverNo).trigger('change');
        $('#vehicleNo').val(vehicleNo).trigger('change');
        $('#accidentTypeNo').val(accidentTypeNo).trigger('change');
        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    $("#vehicleNo").on("change", function () {

        var vehicleNo = $('#vehicleNo').val();
        if (vehicleNo != "") {
            $.ajax({

                url: "/vehicle-ajax/getDrivers/" + vehicleNo,
                type: 'GET',
                dataType: 'json',
                success: function (response) {

                    var driverNo = "";


                    $.each(response, function (i, l) {

                        driverNo = l[0];

                    });


                    $('#driverNo').val(driverNo);
                    $('#driverNo').selectpicker('refresh');

                    $("#error_UDForApproval").text("");

                },
                error: function (xhr, status, error) {

                    $('#driverNo').val("");


                    $("#error_UDForApproval").text("Sorry! maintenance Id not match!!");
                }
            });
        }
        else {

            if ($('#vehicleNo').val() == "") {

                $('#vehicleNo').val("");
                $('#driverNo').val("");


            }
            else {
                $("#error_UDForApproval").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End two------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
