$(document).ready(function () {


    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field!";
        valdiationMsgContent = "";
         if ($("#vehicleNo").val()=='-1') {
            valdiationMsgContent = "Vehicle is required!";
            $("#vehicleNo").focus();
            return false;
        }else if (isEmptyString($("#consumptionDate").val())) {
             valdiationMsgContent = "Consumption Date is required!";
             $("#consumptionDate").focus();
             return false;
         }else if ($("#fuelStationNo").val()=='-1') {
            valdiationMsgContent = "Fuel Station is required!";
            $("#fuelStationNo").focus();
            return false;
        }else if ($("#fuelTypeNo").val()=='-1') {
             valdiationMsgContent = "Fuel Type is required!";
             $("#fuelTypeNo").focus();
             return false;
         }else if (isEmptyString($("#quantity").val())) {
            valdiationMsgContent = "Quantity is required!";
            $("#quantity").focus();
            return false;
        }else if (isEmptyString($("#totalAmount").val())) {
            valdiationMsgContent = "Total Amount is required!";
            $("#totalAmount").focus();
            return false;
        }else if ($("#driverNo").val()=='-1') {
             valdiationMsgContent = "Driver is required!";
             $("#driverNo").focus();
             return false;
         }
        else {
            return true;
        }

    }

    $(document).on('input', '#quantity', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });
    $(document).on('input', '#totalAmount', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });







    var table = $('#dataTable').DataTable({


    });

    $(document).on("click", "#save", function () {

        if(isValidated()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var fuelConsumption = {};
                        fuelConsumption.fuelConsumptionNo = $("#key").val();
                        fuelConsumption.vehicleNo = $("#vehicleNo").val();
                        fuelConsumption.consumptionDate = $("#consumptionDate").val();
                        fuelConsumption.fuelStationNo = $("#fuelStationNo").val();
                        fuelConsumption.fuelTypeNo = $("#fuelTypeNo").val();
                        fuelConsumption.quantity = $("#quantity").val();
                        fuelConsumption.totalAmount = $("#totalAmount").val();
                        fuelConsumption.driverNo = $("#driverNo").val();

                        if($("#isPaid").val()=='on')
                            fuelConsumption.isPaid = 1;
                        else
                            fuelConsumption.isPaid = 0;
                        fuelConsumption.remarks = $("#remarks").val();



                        fuelConsumption.receiptNumber = $("#receiptNumber").val();
                        fuelConsumption.receiptDate = $("#receiptDate").val();
                        fuelConsumption.fileNumber = $("#fileNumber").val();




                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveFuelConsumption",
                            type: 'POST',
                            data: JSON.stringify(fuelConsumption),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }else{
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }

    });



    $(document).on("click", "#edit", function () {



        var curRow = $(this).closest('tr');
        $("#key").val(curRow.find('td:eq(0)').text());
        $("#vehicleNo").val(curRow.find('td:eq(1)').text());
        $('#vehicleNo').trigger('change')
        $("#consumptionDate").val(curRow.find('td:eq(2)').text());
        $("#fuelStationNo").val(curRow.find('td:eq(3)').text());
        $("#fuelTypeNo").val(curRow.find('td:eq(4)').text());
        $("#quantity").val(curRow.find('td:eq(5)').text());
        $("#totalAmount").val(curRow.find('td:eq(6)').text());
        $("#driverNo").val(curRow.find('td:eq(7)').text());
        $("#isPaid").val(curRow.find('td:eq(8)').text());
        $("#remarks").val(curRow.find('td:eq(9)').text());
        $("#receiptNumber").val(curRow.find('td:eq(10)').text());
        $("#receiptDate").val(curRow.find('td:eq(11)').text());
        $("#fileNumber").val(curRow.find('td:eq(12)').text());






        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });



});