/**
 * Created by md mithu sarker on 1/6/2020.
 */
function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}
function SelectRadioButton(name, value) {

    $("input[name='" + name + "'][value='" + value + "']").prop('checked', true);

    return false; // Returning false would not submit the form
}


$(document).ready(function () {

    clearForm();
    $("#employeeId").focus();



    /*-------------- asset save function start ------------------------*/
    $("#maintenanceTypeSave").click(function () {
        saveData();
    });
    $("#maintenanceTypeReferesh").click(function () {
        clearForm();
    });

    function getFormData() {
        var maintenanceType = {};
        maintenanceType.maintenanceTypeNO = $("#maintenanceTypeNO").val();
        maintenanceType.maintenanceTypeName = $("#maintenanceTypeName").val();
        maintenanceType.insertUser = $('#insertUser').val();
        return maintenanceType;
    }

    function saveData() {

        if (isValidated()) {
            const saveUnitUrl = "/vehicle-ajax/save-maintenance-type";
            $.ajax({
                url: saveUnitUrl,
                type: 'POST',
                data: JSON.stringify(getFormData()),
                contentType: 'application/json',
                async: false,
                success: function (data) {

                    if (data) {
                        //showAlert("Successfully added.");
                        showAlertByType("Successfully added.", 'S');
                        clearForm();
                    } else {
                        showAlertByType("Successfully updated.", "F");
                        clearForm();
                    }
                    setInterval(function(){location.reload(true); }, 2000);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    showAlertByType('Something went wrong[xhr], Contact with IT team.', 'F');
                }

            });
        }
        else {

            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }


    }

    function clearForm() {
        $("#maintenanceTypeName").val('');
        valdiationMsgTitle = "";
        valdiationMsgContent = "";
        $('.selectpicker').selectpicker('refresh');
    }

    /*-------------- asset save function End ------------------------*/



    /*-------------- validation function start ------------------------*/
    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field validation!!!";
        valdiationMsgContent = "";
        if (isEmptyString($("#maintenanceTypeName").val())) {

            valdiationMsgContent = "Maintenance Type Name is required!";
            $("#maintenanceTypeName").focus();
            return false;
        }
        else if ($("#maintenanceTypeName").val().length >50) {
            valdiationMsgTitle = "Length validation!!!";
            valdiationMsgContent = "Maintenance Type Name maximum 50 characters!!";
            $("#maintenanceTypeName").focus();
            return false;
        }
        else {
            return true;
        }

    }

    /*-------------- validation function End ------------------------*/


    /*-------------- Data Table Start ------------------------*/
    var table = $('#dataTable').DataTable({});
    /*-------------- Data Table End ------------------------*/



    /*-------------- Delete start ------------------------*/
    /*-------------- Delete End ------------------------*/



    /*-------------- update Start ------------------------*/
    $('#dataTable tbody').on('click', '#edit', function () {
        var curRow = $(this).closest('tr');
        var maintenanceTypeNO = curRow.find('td:eq(0)').text();
        var maintenanceTypeName = curRow.find('td:eq(1)').text();
        var insertUser = curRow.find('td:eq(2)').text();


        $('#maintenanceTypeNO').val(maintenanceTypeNO);
        $('#maintenanceTypeName').val(maintenanceTypeName);
        $('#insertUser').val(insertUser);


        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);


    });
    /*-------------- update End ------------------------*/


    /*-------------- datalaod Start ------------------------*/
    $(document).on("input", "#employeeId", function (e) {

        e.target.value = e.target.value.replace(/[^0-9]/g, '');

        var employeeId = $('#employeeId').val();


        if (employeeId != "") {
            $.ajax({
                url: "/vehicle-ajax/getEmpolyeeDetails/" + employeeId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var repossessionEmpNo = "";
                    var employeeName = "";
                    var employeeDesignation = "";
                    var employeeOffice = "";
                    var officeNo = "";

                    $.each(response, function (i, l) {
                        repossessionEmpNo = l[0];
                        employeeName = l[1];
                        employeeDesignation = l[2];
                        employeeOffice = l[3];
                        officeNo = l[4];


                    });

                    $("#repossessionEmpNo").val(repossessionEmpNo);
                    $("#employeeName").val(employeeName);
                    $("#employeeDesignation").val(employeeDesignation);
                    $("#employeeOffice").val(employeeOffice);
                    $("#officeNo").val(officeNo);

                    $("#error_UD").text("");

                },
                error: function (xhr, status, error) {
                    $("#repossessionEmpNo").val("");
                    $("#employeeName").val("");
                    $("#employeeDesignation").val("");
                    $("#employeeOffice").val("");
                    $("#officeNo").val("");

                    $("#error_UD").text("Sorry! Employee Id not match!!");
                }
            });
        }
        else {

            if ($('#employeeId').val() == "") {
                $("#error_UD").text("Employee Id required!");
                $("#employeeId").val("");
                $("#repossessionEmpNo").val("");
                $("#employeeName").val("");
                $("#employeeDesignation").val("");
                $("#employeeOffice").val("");
                $("#officeNo").val("");


            }
            else {
                $("#error_UD").text("ID maximum 8 characters!");
            }
        }

    });
    /*-------------- datalaod End ------------------------*/

    function getDate(date) {
        var d = new Date(date);
        return d;
    }


});
