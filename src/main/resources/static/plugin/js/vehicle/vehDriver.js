function getType(type)
{

    if(type=='1') {
           $('#agency1').show();
           $('#agency2').show();
       }else {
        $('#agency1').hide();
        $('#agency2').hide();
    }
}

$(document).ready(function () {

    $('#nominee_photo_view').attr("src", "/images/no-image.jpg");
    function readURLPhoto(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#nominee_photo_view').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#err_emp_photo_id").text("");
        }
    }
    $(document).on(
        "change",
        "#getImage",
        function (e) {
            if (this.files[0].size > 1048576) {//200KB
                $("#err_emp_photo_id").text(
                    "Photo size can't be greater than 1MB!");
            } else {

                $("#hasNewImage").val(true);
                readURLPhoto(this);
            }
        });
    $("#hasNewImage").val(false);

    function getPhotoExtension(photonm){
        if (isEmptyString(photonm)){
            return "jpg";
        }
        else{
            return photonm.split('.')[1];
        }
    }


 /*   function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#getImage + img').remove();
                $('#getImage').after('<img src="'+e.target.result+'" width="300" height="300" style="margin-top: 6px;"/>');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#getImage").change(function () {

        filePreview(this);
    });*/



    $('#agency1').hide();
    $('#agency2').hide();
    // $(document).on('input', '#capacity', function(e){
    //     e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    // });
    // $("#regDate").datepicker({
    //     maxDate: 0
    // });
    //
    // $("#regRenualDate").datepicker({
    //     minDate: 0
    // });
    //DUPLICATE REG NO CHECK
    // $(document).on("input", "#regNumber", function (e) {
    //
    //
    //
    //     var regNumber = $('#regNumber').val();
    //
    //
    //     if (regNumber != "") {
    //         $.ajax({
    //             url: "/vehicle/regNumberCheck/" + regNumber,
    //             type: 'GET',
    //             dataType: 'json',
    //             success: function (response) {
    //                 if (response == 1)
    //                     $("#err_policy_no").text("Registration Number Already Exists");
    //                 else
    //                     $("#err_policy_no").text("");
    //
    //
    //
    //
    //             },
    //             error: function (xhr, status, error) {
    //
    //
    //             }
    //         });
    //     } else {
    //
    //
    //     }
    //
    // });





    var table = $('#dataTable').DataTable({


    });

    $(document).on("click", "#save", function () {

        if(validate()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var driver = {};
                        driver.driverNo = $("#driverNo").val();
                        driver.vehicleNo = $("#vehicleNo").val();
                        driver.driverName = $("#name").val();
                        driver.driverMobile = $("#driverMobile").val();
                        driver.driverDob = $("#driverDob").val();
                        driver.joinDate = $("#joinDate").val();
                        driver.drivingLicenseNum = $("#drivingLicenseNum").val();
                        driver.expireDate = $("#expireDate").val();
                        driver.presentAddress = $("#presentAddress").val();
                        driver.permanentAddress = $("#permanentAddress").val();
                        driver.recruitmentTypeNo = $("#recruitmentTypeNo").val();
                        driver.agencyName = $("#agencyName").val();
                        driver.agencyDetails = $("#agencyDetails").val();


                        driver.photoname = $("#photoname").val();
                        driver.photopath = $("#photopath").val();
                        var form = $('#driverForm')[0];
                        var data = new FormData(form);
                        data.append("driverData", JSON.stringify(driver));

                        $.ajax({
                            type : "POST",
                            enctype : 'multipart/form-data',
                            url : "/vehicle/saveDrivers",
                            data : data,
                            processData : false,
                            contentType : false,
                            cache : false,
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }

    });



    $(document).on("click", "#edit", function () {

        //setDisOrEn(true);

        var curRow = $(this).closest('tr');
        $("#driverNo").val(curRow.find('td:eq(0)').text());
        $("#vehicleNo").val(curRow.find('td:eq(1)').text());
        $("#vehicleNo").trigger('change');
        $("#name").val(curRow.find('td:eq(2)').text());
        $("#driverMobile").val(curRow.find('td:eq(3)').text());
        $("#driverDob").val(curRow.find('td:eq(4)').text());
        $("#joinDate").val(curRow.find('td:eq(5)').text());
        $("#drivingLicenseNum").val(curRow.find('td:eq(6)').text());
        $("#expireDate").val(curRow.find('td:eq(7)').text());
        $("#presentAddress").val(curRow.find('td:eq(8)').text());
        $("#permanentAddress").val(curRow.find('td:eq(9)').text());
        $("#recruitmentTypeNo").val(curRow.find('td:eq(10)').text());
        $("#agencyName").val(curRow.find('td:eq(11)').text());
        $("#agencyDetails").val(curRow.find('td:eq(12)').text());

        $("#photoname").val(curRow.find('td:eq(17)').text());
        $("#photopath").val(curRow.find('td:eq(18)').text());

        $('#imagefiled').val(curRow.find('td:eq(17)').text());
        var photo64BitString = $(this).closest('tr').find('.nominee-photo').attr('nomineePhotoByte');
        console.log(photo64BitString);



        if (isEmptyString(photo64BitString)) {
            $("#hasNewImage").val(true);
            $("#nominee_photo_view").attr("src","/images/no-image.jpg");
        }
        else {
            $("#hasNewImage").val(false);
            $("#nominee_photo_view").attr("src", 'data:image/'+getPhotoExtension(curRow.find('td:eq(17)').text())+';base64,' + photo64BitString);
        }







        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    $(document).on("click", "#clear_btn", function () {
        $("select").prop("disabled", false);
        $("select").val('-1');
        clrErr();
        $("#save").text('Save');
    });

    function clrErr() {

        $("#err_p_cd").text("");
        $("#err_p_status").text("");
        $("#err_appl_party").text("");

    }

    $(document).on('input', '#surDiscFactForm', function(e){
        e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    });

    function validate() {

        clrErr();

        if($("#p_cd").val() == -1){
            $("#err_p_cd").text("Required !!");
            return;
        }

        if($("#p_status").val() == -1){
            $("#err_p_status").text("Required !!");
            return;
        }

        if($("#appl_party").val() == -1){
            $("#err_appl_party").text("Required !!");
            return;
        }

        return true;

    }

    $('#dataTable tbody').on('click', '#delete', function () {
        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();

        $.confirm({
            title: 'Confirm',
            content: 'Selected record will be deleted.',
            buttons: {
                ok: function () {
                    $.ajax({
                        contentType: 'application/json',
                        url: "/vehicle/vehVehicleDelete/" + col1,
                        type: 'POST',
                        //async: false,
                        //data: JSON.stringify(answerDto),
                        dataType: 'json',
                        success: function (response) {

                            if (response === true) {
                                table.row(curRow).remove().draw(false);
                                showAlert("Deleted Successfully");
                            } else {
                                showAlert("Unknown error");
                            }

                        },
                        error: function (xhr, status, error) {
                            showAlert("Unknown error");
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

    });

});