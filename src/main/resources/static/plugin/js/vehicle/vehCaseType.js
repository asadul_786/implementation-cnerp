$(document).ready(function () {

    var valdiationMsgTitle;
    var valdiationMsgContent;

    function isValidated() {
        valdiationMsgTitle = "Required field!";
        valdiationMsgContent = "";
        if (isEmptyString($("#caseTypeName").val())) {

            valdiationMsgContent = "Case Type Name is required!";
            $("#caseTypeName").focus();
            return false;
        } else {
            return true;
        }

    }

    var table = $('#dataTable').DataTable({});

    $(document).on("click", "#save", function () {

        if(isValidated()){

            $.confirm({
                title: 'Confirm',
                content: 'Are your sure?',
                buttons: {
                    ok: function () {
                        var driver = {};
                        driver.caseTypeNo = $("#key").val();
                        driver.caseTypeName = $("#caseTypeName").val();





                        $.ajax({
                            contentType: 'application/json',
                            url:  "saveCaseType",
                            type: 'POST',
                            data: JSON.stringify(driver),
                            dataType: 'json',
                            success: function(response) {
                                response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500);
                            },
                            error: function(xhr, status, error) {
                                showAlert("Something went wrong !!");
                            },
                            complete: function () {
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }else {
            customAlert(alertTypes.WARNING, valdiationMsgTitle, valdiationMsgContent);
        }

    });



    $(document).on("click", "#edit", function () {



        var curRow = $(this).closest('tr');
        $("#key").val(curRow.find('td:eq(0)').text());
        $("#caseTypeName").val(curRow.find('td:eq(1)').text());









        $("#save").text('Update');

        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

    // $(document).on("click", "#clear_btn", function () {
    //     $("select").prop("disabled", false);
    //     $("select").val('-1');
    //     // clrErr();
    //     $("#save").text('Save');
    // });

    // function clrErr() {
    //
    //     $("#err_p_cd").text("");
    //     $("#err_p_status").text("");
    //     $("#err_appl_party").text("");
    //
    // }

    // $(document).on('input', '#surDiscFactForm', function(e){
    //     e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    // });

    // function validate() {
    //
    //     clrErr();
    //
    //     if($("#p_cd").val() == -1){
    //         $("#err_p_cd").text("Required !!");
    //         return;
    //     }
    //
    //     if($("#p_status").val() == -1){
    //         $("#err_p_status").text("Required !!");
    //         return;
    //     }
    //
    //     if($("#appl_party").val() == -1){
    //         $("#err_appl_party").text("Required !!");
    //         return;
    //     }
    //
    //     return true;
    //
    // }

    // $('#dataTable tbody').on('click', '#delete', function () {
    //     var curRow = $(this).closest('tr');
    //     var col1 = curRow.find('td:eq(0)').text();
    //
    //     $.confirm({
    //         title: 'Confirm',
    //         content: 'Selected record will be deleted.',
    //         buttons: {
    //             ok: function () {
    //                 $.ajax({
    //                     contentType: 'application/json',
    //                     url: "/vehicle/vehVehicleDelete/" + col1,
    //                     type: 'POST',
    //                     //async: false,
    //                     //data: JSON.stringify(answerDto),
    //                     dataType: 'json',
    //                     success: function (response) {
    //
    //                         if (response === true) {
    //                             table.row(curRow).remove().draw(false);
    //                             showAlert("Deleted Successfully");
    //                         } else {
    //                             showAlert("Unknown error");
    //                         }
    //
    //                     },
    //                     error: function (xhr, status, error) {
    //                         showAlert("Unknown error");
    //                     }
    //                 });
    //             },
    //             cancel: function () {
    //
    //             }
    //         }
    //     });
    //
    // });

});