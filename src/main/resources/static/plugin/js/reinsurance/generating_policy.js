/**
 * Created by Asad on 2/2/2020.
 */
$(document).ready(function () {


    $("#save_setup_value").click(function (event) {

        event.preventDefault();
       // var isVaild = validationCalInfo();

        //if (isVaild) {
            confirmGeneratingPolicyInfoDialog("Are you sure to Generating Policy list ...?");
       // }
    });


    var confirmGeneratingPolicyInfoDialog = function (text) {
             //  alert("hi..");
        $.confirm({
            title: 'Confirm!',
            content: text,
            buttons: {
                confirm: {
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function () {

                        var inParameterVal = {};

                        $.ajax({
                            url: "/reinsurance/add-GeneratingPolicyInfoLst",
                            contentType: 'application/json',
                            dataType: 'json',
                            type: 'POST',
                            success: function (data) {
                               // alert("hi..");
                                 if(data== null) {
                                     showAlert("No Data Found");
                                 }else{
                                     showAlert("Process is done successfully..");
                                 }
                            },
                            error: function (e) {
                                showAlert("Something Wrong!!!");
                               // console.log(e);
                            }
                        });



                    }

                },
                cancel: function () {

                }
            }
        });
    };
});
    //
    //
    // var table = $("#ad_setup_value_table").DataTable({
    //
    //     "processing": true,
    //     "language": {
    //         "processing": "Processing... please wait"
    //     },
    //     "pageLength": 25,
    //     ajax: {
    //         "url": "add-ValueSetupList",
    //         "type": "GET",
    //         "dataType": "json"
    //     },
    //     "autoWidth": true,
    //     "columns": [
    //         { "data": "slNo", "name": "SL_NO",className: "dt-hd" },
    //         { "data": "off_order_no", "name": "OFF_ORDER_NO" },
    //         { "data": "off_order_dt", "name": "OFF_ORDER_DT"},
    //         { "data": "valid_dt_fr", "name": "VALID_DT_FR" },
    //         { "data": "valid_dt_to", "name": "VALID_DT_TO" },
    //         { "data": "max_insur_amount", "name": "MAX_INSUR_AMOUNT" },
    //         { "data": "coridor_amount", "name": "CORIDOR_AMOUNT" },
    //         {
    //             "data": "status", "name": "STATUS",
    //             "render": function (data) {
    //                 return data == 1 ? "Active" : "Inactive";
    //
    //             }
    //         },
    //         {
    //             "className": "dt-btn",
    //             "render": function () {
    //                 return '<button id="editBtn" class="btn btn-info btn-xs">Edit</button>';
    //
    //             }
    //
    //         }
    //     ]
    // });
    //
    // $(document).on("click", "#save_setup_value", function () {
    //
    //     if(validate()){
    //
    //         $.confirm({
    //             title: 'Confirm',
    //             content: 'Are your sure to save ?',
    //             buttons: {
    //                 ok: function () {
    //                     var reInvalueSetupVal = {};
    //                     reInvalueSetupVal.slNo = $("#key").val();
    //                     reInvalueSetupVal.off_order_no = $("#off_order_no").val();
    //                     reInvalueSetupVal.off_order_dt = $("#off_order_dt").val();
    //                     reInvalueSetupVal.valid_dt_fr = $("#valid_dt_fr").val();
    //                     reInvalueSetupVal.valid_dt_to = $("#valid_dt_to").val();
    //                     reInvalueSetupVal.max_insur_amount = $("#max_insur_amount").val();
    //                     reInvalueSetupVal.coridor_amount = $("#coridor_amount").val();
    //                     reInvalueSetupVal.status = $("#status").val();
    //
    //                     $.ajax({
    //                         contentType: 'application/json',
    //                         url:  "saveReInValueSetup",
    //                         type: 'POST',
    //                         data: JSON.stringify(reInvalueSetupVal),
    //                         dataType: 'json',
    //                         success: function(response) {
    //                             response == "0" ? showAlert("Inserted Successfully") : showAlert("Updated Successfully");
    //                             //$("#key").val('');
    //                             setTimeout(function () {
    //                                 window.location.reload();
    //                             }, 1500);
    //                         },
    //                         error: function(xhr, status, error) {
    //                             showAlert("Something went wrong !!");
    //                         },
    //                         complete: function () {
    //                         }
    //                     });
    //                 },
    //                 cancel: function () {
    //
    //                 }
    //             }
    //         });
    //     }
    //
    // });
    //
    // // function setDisOrEn(val) {
    // //     $("#dur_month").prop('disabled', val);
    // // }
    //
    // $(document).on("click", "#status", function () {
    //     $(this).val() == 1 ? $(this).val('0') : $(this).val('1');
    // });
    //
    // $( "#off_order_dt" ).datepicker({
    //     dateFormat: 'dd/mm/yy',
    //     changeMonth: true,
    //     changeYear: true,
    //     yearRange: '1970:2010'
    // });
    //
    // $( "#valid_dt_fr" ).datepicker({
    //     dateFormat: 'dd/mm/yy',
    //     changeMonth: true,
    //     changeYear: true,
    //     yearRange: '1970:2010'
    // });
    //
    // $( "#valid_dt_to" ).datepicker({
    //     dateFormat: 'dd/mm/yy',
    //     changeMonth: true,
    //     changeYear: true,
    //     yearRange: '1970:2010'
    // });
    //
    //
    // $(document).on("click", "#editBtn", function () {
    //     //setDisOrEn(true);
    //     var curRow = $(this).closest('tr');
    //     $("#sl_no").val(curRow.find('td:eq(0)').text());
    //     $("#off_order_no").val(curRow.find('td:eq(1)').text());
    //     $("#off_order_dt").val(curRow.find('td:eq(2)').text());
    //     $("#valid_dt_fr").val(curRow.find('td:eq(3)').text());
    //     $("#valid_dt_to").val(curRow.find('td:eq(4)').text());
    //     $("#max_insur_amount").val(curRow.find('td:eq(5)').text());
    //     $("#coridor_amount").val(curRow.find('td:eq(6)').text());
    //
    //
    //     if($.trim(curRow.find('td:eq(7)').text()) == "Active"){
    //         $("#status").prop("checked", true);
    //         $("#status").val('1');
    //     }
    //     else{
    //         $("#status").prop("checked", false);
    //         $("#status").val('0');
    //     }
    //
    //     $("#save_setup_value").text('Update');
    //
    //     $('html, body').animate({
    //         scrollTop: $('#scrlDiv').offset().top
    //     }, 500);
    //
    // });
    //
    //
    // $(document).on("click", "#clear_btn", function () {
    //
    //     $("#sl_no").val('');
    //     $("#off_order_no").val('');
    //     $("#off_order_dt").val('');
    //     $("#valid_dt_fr").val('');
    //     $("#valid_dt_to").val('');
    //     $("#max_insur_amount").val('');
    //     $("#coridor_amount").val('');
    //     $("#status").prop("checked", false);
    //     $("#status").val('0');
    //
    //    // setDisOrEn(false);
    //
    //    // clrErr();
    //
    //     $("#save_setup_value").text('Save');
    //
    // });
    //
    // function clrErr() {
    //
    //     $("#err_dur_month").text('');
    //     $("#err_disc_val").text('');
    //
    // }
    //
    // $(document).on('input', '#adPayFactForm', function(e){
    //     e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    // });
    //
    // $("input[type='search']").on("input", function (e) {
    //     e.target.value = e.target.value.replace(/[^0-9, .]/g,'');
    // });
    //
    // function validate() {
    //
    //     clrErr();
    //
    //     if($.trim($("#off_order_no").val()) == ""){
    //         $("#err_off_order_no").text("Required field !!");
    //         return;
    //     }
    //
    //     if($.trim($("#off_order_dt").val()) == ""){
    //         $("#err_off_order_dt").text("Required field !!");
    //         return;
    //     }
    //
    //     return true;
    //
    // }

