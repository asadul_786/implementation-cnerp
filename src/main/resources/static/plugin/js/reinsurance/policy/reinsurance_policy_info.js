/**
 * Created by Asad on 3/13/2020.
 */
$(document).ready(function () {

   $('#tbl-awaiting-apprval-lst').DataTable({

        // "sDom": "lfrti",
        "dom": '<"pull-left"f><"pull-right"l>tip',
        // paging:         true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: ""
        },
        "info":     false,
       "scrollY":        "600px",
       "scrollCollapse": true,
       "paging":         false

    }) ;

    $('.dataTables_filter').addClass('pull-left');
    $('.proposal-list-tr').css("cursor", "pointer");



    $(document).on("click", ".proposal-list-tr", function () {
        // alert( $('td', this).eq(1).find('span').html());
        $(this).addClass("selected").siblings().removeClass("selected");
        //var policy_no =$('td', this).eq(0).find('span').html();
        loadData($('td', this).eq(0).find('span').html());
        //alert(policy_no);

    });

    function  loadData(pgid) {

        const getListUrl = "/reinsurance/re-insuance-policy-info-lst/" + pgid;
        $.ajax({
            url: getListUrl,
            contentType: 'application/json',
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                //alert(pgid)
                clearFormData();
                $('.policy-no').val(pgid);
                //$('.pggid').val(applNo);
                fillFormData(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlertByType('Something went wrong ' + jqXhr,'F');
            }
        });

    }

    function clearFormData(){
        $('.policy-no').val('')
        $('.region_off').val('');
        $('.assured_name').val('');
        $('.comm_dt').val('');
        $('.dob').val('');
        $('.term').val('');
        $('.maturity_dt').val('');
        $('.sum_assured').val('');
        $('.age').val('');
        $('#policy-no').val('');
        $('#reInsCompany').val('-1');
        $('#reInsCompany').selectpicker('refresh');
        $('#cession_no').val('');
        $('#sum_assu').val('');
        $('#risk_dt_to').val('');
        $('#risk_dt_fr').val('');
        $('#remark').val('');

    };

    function  fillFormData(data){
       // alert(policy_no);

        //$('.policy-no').val(policy_no);
        $('.comm_dt').val(data[0][0]);
        $('.assured_name').val( data[0][1]);
        $('.dob').val( data[0][2]);
        $('.age').val( data[0][3]);
        $('.sum_assured').val( data[0][4]);
        $('.term').val( data[0][5]);
        $('#pggid').val( data[0][6]);
        $('.region_off').val( data[0][7]);
        $('.maturity_dt').val( data[0][8]);

    };


    $( "#btnReset" ).click(function() {
        clearFormData();

    });

    $("#btnSubmit").click(function () {

        var flag=isValidate();

        if(flag== true){

            var policy_no = $("#policy_no").val();
            var reInsCompany = $("#reInsCompany").val();
            var cession_no = $("#cession_no").val();
            var sum_assu = $("#sum_assu").val();
            var risk_dt_to = $("#risk_dt_to").val();
            var risk_dt_fr = $("#risk_dt_fr").val();
            var remark = $("#remark").val();
            var pggid = $("#pggid").val();


            var reInsuranceComp = {};

            reInsuranceComp.policy_no = policy_no;
            reInsuranceComp.reInsCompany = reInsCompany;
            reInsuranceComp.cession_no = cession_no;
            reInsuranceComp.sum_assu = sum_assu;
            reInsuranceComp.risk_dt_to = risk_dt_to;
            reInsuranceComp.risk_dt_fr = risk_dt_fr;
            reInsuranceComp.remark = remark;
            reInsuranceComp.pggid = pggid;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/reinsurance/add-reInsurance-policy",
                data: JSON.stringify(reInsuranceComp),
                dataType: 'json',
                success: function (data) {
                   // alert(data);
                    if (data == false) {
                        showAlertByType('Nominee Percentage cant be more 100%!!', "W");
                    } else {

                        showAlert("Successfully Saved!");
                        clearFormData();
                    }
                },
                error: function (e) {
                    showAlert("Something Wrong!!!");
                }
            });
        }
    });





    $("#btnSubmitss" ).click(function() {

        $.confirm({
            icon: 'ace-icon fa fa-lock',
            theme: 'material',
            animation: 'scale',
            type: 'orange',
            useBootstrap: true,
            title:  "Are you sure to proceed?",
            typeAnimated: true,

            buttons: {
                Yes: {

                    text: 'Save',
                    btnClass: 'btn-warning',
                    action: function () {

                        if(isValidate()){

                            const url = "/new-business/claim-payment" + "/" + "approve-adv-pay-app";
                            $.ajax({
                                url: url,
                                type: 'POST',
                                data: JSON.stringify( prepareData()),
                                contentType: 'application/json',
                                async: false,
                                success: function (commonMsg) {
                                    // console.log(commonMsg);
                                    if (commonMsg.isSucceed) {
                                        showAlertByType( commonMsg.remarks +'['+ commonMsg.msgCode+']', "S");
                                        // customAlert(alertTypes.SUCCESS, 'Success[' + commonMsg.msgCode + ']', 'Generated Successfully.');
                                        $("#btnReset").trigger("click");
                                    }
                                    else {
                                        showAlertByType( commonMsg.remarks +'['+ commonMsg.msgCode+']', "W");
                                        // customAlert(alertTypes.FAILED, 'Unsuccessful[' + commonMsg.msgCode + ']', commonMsg.msg + '[' + commonMsg.remarks + ']');
                                    }
                                },
                                error: function (jqXhr, textStatus, errorThrown) {
                                    showAlertByType('Something went wrong[xhr], Contact with IT team.', "F");
                                    // customAlert(alertTypes.ERROR, 'Error', 'Something went wrong[xhr], Contact with IT team.');
                                }

                            });
                        }
                        else
                        {
                            $.confirm({
                                icon: 'ace-icon fa fa-ban',
                                theme: 'material',
                                closeIcon: true,
                                animation: 'scale',
                                type: 'red',

                                title: 'Required data Validation',
                                content:  "Level, Approved by is required!",
                                typeAnimated: true,
                                buttons: {
                                    Yes: {
                                        text: 'Ok'
                                    }
                                }

                            });
                        }

                    }

                },
                Cancel: {
                    btnClass: 'btn-info',
                    text: 'Cancel',
                    action: function () {
                        return true;
                    }

                }
            }

        });


    });

    function  isValidate() {


        if($.trim($("#reInsCompany").val()) == "-1"){
            $("#err_reInstall_com").text("Required field !!");
            return;
        }else {
            $("#err_reInstall_com").text('');
            return true;
        }
        //
        // if(isEmptyString( $('.appl-no').val())||isEmptyString( $('.pggid').val())||isEmptyString( $('.policy-no').val())){
        //     return true;
        // }
        // else {
        //     return true;
        // }
    }

    function  prepareData() {

        var advancePaymentApprovalData = {};

        advancePaymentApprovalData.levelName = $('#level').val();
        advancePaymentApprovalData.approvedBy = $('.approved-by').children("option:selected").val()
        advancePaymentApprovalData.approvedDate = $('.date').val();

        advancePaymentApprovalData.applNO = $('.pggid').val();
        advancePaymentApprovalData.pggid = $('.appl-no').val();
        advancePaymentApprovalData.policyNo = $('.policy-no').val();

        advancePaymentApprovalData.discountFactor = $('.discount-factor').val( );
        advancePaymentApprovalData.paymentTypeCd = $('.payment-type-cd').val();
        advancePaymentApprovalData.actualPayableDate = $('.payable-date').val();
        advancePaymentApprovalData.reqPayableDate = $('.payable-date').val();
        advancePaymentApprovalData.netPayableAmount = $('.net-payable-amt').val();

        return advancePaymentApprovalData;

    }

});


function isEmptyString(strVal) {

    if (strVal == '' || strVal == null || strVal == 'undefined') {
        return true;
    }
    else {
        return false;
    }
}