function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
}

function exportTableToCSV(filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV file
    downloadCSV(csv.join("\n"), filename);
}

$(document).ready(function () {




    $('#tableData tbody').on('click', '#edit', function () {

        var curRow = $(this).closest('tr');
        var col1 = curRow.find('td:eq(0)').text();
        var col2 = curRow.find('td:eq(1)').text();
        var col3 = curRow.find('td:eq(2)').text();
        var col4 = curRow.find('td:eq(3)').text();

        var col5 = curRow.find('td:eq(4)').text();
        var col6 = curRow.find('td:eq(5)').text();
        var col7 = curRow.find('td:eq(6)').text();
        var sms= "Dear Sir, BDT "+col5+" has been received on "+col6+" for Policy No "+col1+" through Sales- Dhaka Regional Office. Thank for being with Jiban Bima Corporation.";
        $.ajax({
            type: "GET",
            url: "/policydocument/sendSms/" + sms + '/'+col4 ,
            success: function (data) {
                showAlert("SMS Sent Successfully.");


            },
            error: function (e) {
                showAlertByType('SMS Not Sent!', "F");

            }

        });



        $('html, body').animate({
            scrollTop: $('#scroll').offset().top
        }, 500);

    });

     $('#demandTable').hide();
     $('#csvbtn').hide();
    $("#drp-office-name").select2();

    //FILTERING DATA
    $("#btnapplyfiter").click(function () {

        // var flag = dataValidationEmployeeAll();

        var agentTable = $('#tableData');

        if (1) {

            var serviceCd = $('#drp-office-name').val();
            var pfcDedStat = $('#no-of-days').val();


            var empployeefilter = {};
            empployeefilter.serviceCd = serviceCd;
            empployeefilter.pfcDedStat = pfcDedStat;

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: "/policydocument/demandNoticeFiltering",
                data: JSON.stringify(empployeefilter),
                dataType: 'json',
                success: function (data) {
                    if (data == "") {
                        showAlertByType('Data not found', "W");
                        $('#demandTable').hide();
                        $('#csvbtn').hide();
                    }else {
                        agentTable.dataTable({
                            paging: true,
                            searching: true,
                            destroy: true,
                            data: data,
                            columns: [{
                                "data": "policyNo"
                            }, {
                                "data": "maturityDate"
                            }, {
                                "data": "partyName"
                            }, {
                                "data": "mobile"
                            }, {
                                "data": "installmentPremium"
                            }, {
                                "data": "lastPaidDate"
                            }, {
                                "data": "nextDueDate"
                            }, {
                                "data": "nextDueInstNo"
                            }, {
                                "data": "remainDays"
                            }, {
                                "data": "policyNo",
                                "render": function (data,
                                                    type,
                                                    row) {
                                    return "<a class='btn btn-success' id='edit'>"
                                        + '<i class="fas fa-comment-alt-dots"></i>'
                                        + 'Send SMS'
                                        + "</a>";
                                }


                            }]
                        });
                        $('#demandTable').show();
                        $('#csvbtn').show();


                    }

                },
                error: function (e) {
                    showAlertByType('Something Wrong!', "F");

                }

            });

        }

    });
})